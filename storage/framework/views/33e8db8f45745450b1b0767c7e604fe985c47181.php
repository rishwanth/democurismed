<!-- edit.blade.php -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Demo </title>
    <link rel="stylesheet" href="<?php echo e(asset('public/css/app.css')); ?>">
  </head>
  <body>
    <div class="container">
      <h2>Update User</h2><br  />
        <form method="post" action="<?php echo e(action('EmployeeController@update', $id)); ?>">
        <?php echo csrf_field(); ?>
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" value="<?php echo e($employee->name); ?>">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-12" style="margin-top:10px">
            <button type="submit" class="btn btn-success">Update</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>