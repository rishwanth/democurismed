<!-- index.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>List Employee</title>
    <link rel="stylesheet" href="<?php echo e(asset('public/css/app.css')); ?>">
  </head>
  <body>
    <div class="container">

<nav class="navbar">
  <div class="navbar-header">
  </div>
  <ul class="nav navbar-nav">
    <li>
      <a  href="<?php echo e(URL::to('users/create')); ?>">Add new</a> | 
      <a  href="<?php echo e(URL::to('users')); ?>">List</a> |
      <a  href="<?php echo e(URL::to('users/create')); ?>">Login</a> | 
      <a  href="<?php echo e(URL::to('users/create')); ?>">Register</a> | 
    <li>
  </ul>
</nav>

    <br />
    <?php if(\Session::has('success')): ?>
      <div class="alert alert-success">
        <p><?php echo e(\Session::get('success')); ?></p>
      </div><br />
     <?php endif; ?>
    <table class="table table-border" >
    <thead>
      <tr>
        <th align="center">User ID</th>
        <th align="center">User Name</th>
        <th align="center">Actions</th>
      </tr>
    </thead>
    <tbody>

      <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td><?php echo e($employee['id']); ?></td>
        <td><?php echo e($employee['name']); ?></td>

        <td align="right">
          <a  class="btn btn-success" href="<?php echo e(URL::to('users/create')); ?>">Add New</a>

          <a href="<?php echo e(action('EmployeeController@edit', $employee['id'])); ?>" class="btn btn-warning">Edit</a>
        </td>

        <td align="left">
          <form action="<?php echo e(action('EmployeeController@destroy', $employee['id'])); ?>" 
          method="post">
            <?php echo csrf_field(); ?>
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
            <a  class="btn btn-success" href="<?php echo e(URL::to('permission')); ?>">Permission <?php echo e(URL::to('permission')); ?></a>
          </form>
        </td>
      </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
  </table>
  </div>
  </body>
</html>