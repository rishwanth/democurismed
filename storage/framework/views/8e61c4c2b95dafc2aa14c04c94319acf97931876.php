<?php

 session_start();

 if (!isset($_SESSION['userID'])) {
  header("Location: index.html");
 }

 include("header.html");

 ?>
<div class="row">
    <h2 style="color: #251367; margin-left:20px;">ICD 10 / ICD 9 Library</h2>
    <div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">
            <a href="#" id="linkAddICD" class="btn btn-primary" style="text-decoration:none"><i class="icon icon-plus"></i>&nbsp;&nbsp; New Diagnosis Code</a><br/><br/>
                <table id="DX10" class="table display" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
                </table><br/><br/><br/>
        </div>
    </div>

</div>

    <div class="modal fade" id="DxModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Diagnosis Code</h4>
                </div>
                <div class="modal-body" style="min-height:150px;">
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">ICD Code </label>
                        <div class="col-md-12">
                            <input type="text" id="icdCode" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">DX Code </label>
                        <div class="col-md-12">
                            <input type="text" id="dxCode" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Long Description </label>
                        <div class="col-md-12">
                            <input type="text" id="longDesc" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Short Description </label>
                        <div class="col-md-12">
                            <input type="text" id="shortDesc" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Code Type </label>
                        <div class="col-md-12">
                            <select class="form-control"><option value="9">9</option><option value="10">10</option></select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveDx" class="btn btn-primary" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<?php
 include("footer.html");
 ?>

     <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    

