<?php $__env->startSection('content'); ?>

<?php
    $base_url = URL::to('/');
?>

<div class="row" style="margin: 5%; ">
    <a href="javascript:void(0)" class="btn btn-primary" id="linkNewIns"><i class="icon icon-plus"></i>&nbsp;Add New Insurance</a>
    <table id="insuranceList" class="table display" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
    </table>
</div>
<div class="modal fade" id="AddInsurance" tabindex="-1">

    <div class="modal-dialog" style="width:100%">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title" id="lblTitle">Add New Insurance</h4>

            </div>

            <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Payer Name </label>

                            <div class="col-md-12">

                                <input type="text" id="payerName" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Clearing House ID </label>

                            <div class="col-md-12">

                                <input type="text" id="clearingHousePayorID" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Address Street 1 </label>

                            <div class="col-md-12">

                                <input type="text" id="addrStrt1" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Address Street 2 </label>

                            <div class="col-md-12">

                                <input type="text" id="addrStrt2" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">City </label>

                            <div class="col-md-12">

                                <input type="text" id="addrCity" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">State </label>

                            <div class="col-md-12">

                                <input type="text" id="addrState" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Zipcode </label>

                            <div class="col-md-12">

                                <input type="text" id="addrZip" class="form-control" />

                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">

                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />

                <input type="button" id="btnSaveInsurance" class="btn btn-primary" data-dismiss="modal" value="Save" />

            </div>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div>
<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){

        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        //document.getElementById('practiceName1').innerHTML = sessionStorage.getItem('practiceName');
        //document.getElementById("lblUser").innerHTML = <?php echo e(Session::get('LOGGED_USER_NAME')); ?>;
        var root_url = $("#hidden_root_url").val();
        //alert('i am here layout=>app.blade.php' + root_url);
        $("#linkLogout").click(function(){
            sessionStorage.removeItem('practiceName');
            sessionStorage.removeItem('userID');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('loginName');
        })

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })

        var practiceId = "1";

        $('#linkNewIns').click(function(){

            $('#AddInsurance').modal('show');

            document.getElementById("hdnFlag").value = 0;

            document.getElementById('btnSaveInsurance').value = "Save Insurance";

            document.getElementById('lblTitle').innerHTML = "Add Insurance";

            // document.getElementById('payerName').value = "";

            // document.getElementById('clearingHousePayorID').value = "";

            // document.getElementById('addrStrt1').value = "";

            // document.getElementById('addrCity').value = "";

            // document.getElementById('addrState').value = "";

            // document.getElementById('addrZip').value = "";

        })

        $(document).on('click','.editServLoc',function() {

            var temp = $(this).attr('class').split(' ')[0];

            var insuranceID = temp.replace('edit','');

            document.getElementById("hdnFlag").value = insuranceID;

            $.ajax({

                type: "POST",

                url: root_url+"insurances/get",

                async : false,

                data:{

                    insuranceID : insuranceID

                },success:function(result){

                    $('#AddInsurance').modal('show');

                    var res = JSON.parse(result);

                    document.getElementById('btnSaveInsurance').value = "Update Insurance";

                    document.getElementById('lblTitle').innerHTML = "Edit Insurance";

                    document.getElementById('payerName').value = res[0].payerName;

                    document.getElementById('clearingHousePayorID').value = res[0].clearingHousePayorID;

                    document.getElementById('addrStrt1').value = res[0].addr;

                    document.getElementById('addrCity').value = res[0].city;

                    document.getElementById('addrState').value = res[0].state;

                    document.getElementById('addrZip').value = res[0].zip;

                }

            });

        });

        var ins_url = root_url + "insurances/list";
        $.post(ins_url,

            {

                practiceID: practiceId

            },

            function(data1, status){

                var dt = [];

                data1 = JSON.parse(data1).data;

                $.each(data1,function(i,v) {

                    dt.push([data1[i].id,data1[i].payer_name,data1[i].clearing_house_payor_id,data1[i].addr,data1[i].id]);

                });

                //alert(dt);

                var table = $('#insuranceList').DataTable({

                    "data": dt,

                    "bProcessing": true,

                    "aoColumns": [

                        {"mdata": "insuranceID","title":"ID", "visible": false},

                        {"title":"Payer Name","mdata": "payerName"},

                        {"title":"Clearing House ID","mdata": "clearingHousePayorID", "width":"15%"},

                        {"title":"Address","mdata": "addr"},

                        {"title":"Action",

                            "render": function ( data, type, full, meta ) {

                                return '<a href="javascript:void(0);" class="edit'+data+' editServLoc"><span class="ti-pencil"></span></a>';

                            }

                        }

                    ]

                });
                $("#insuranceList_wrapper").css({"width":"100%"})
                $("#insuranceList").css({"width":"100%"})


            });

        $('#btnSaveInsurance').click(function(){

            document.getElementById('btnSaveInsurance').value = "Add Insurance";

            document.getElementById('lblTitle').innerHTML = "Add New Insurance";

            var hdnFlag = document.getElementById("hdnFlag").value;
            alert(root_url);

            if(hdnFlag == 0){

                var payerName = $('#payerName').val();

                var clearingHousePayorID = $('#clearingHousePayorID').val();

                var addrStrt1 = $('#addrStrt1').val()+ ",";

                if($('#addrStrt2').val() == ""){

                    var addrStrt2 = "";

                }

                else{

                    var addrStrt2 = $('#addrStrt2').val()+ ",";

                }



                var addrCity = $('#addrCity').val()+ " ";

                var addrState = $('#addrState').val()+ " ";

                var addrZip = $('#addrZip').val();

                var addr = addrStrt1+addrStrt2+addrCity+addrState+addrZip;

                //var practiceID = sessionStorage.getItem("practiceId");

                $.ajax({

                    type: "POST",

                    url: root_url+"handler/insurances/add",

                    data:{

                        "payerName" : payerName,

                        "clearingHousePayorID" : clearingHousePayorID,

                        "addr" : addr,

                        "practiceID" : practiceId,

                    },success:function(result){
                        var res = JSON.parse(result);
                        // new PNotify({
                        //     type:'success',
                        //     text: json.message,
                        //     after_init: function(notice){
                        //         notice.attention('rubberBand');
                        //     }
                        // })
                        alert(res.data);

                    }

                });

            }

            else{

                var payerName = $('#payerName').val();

                var clearingHousePayorID = $('#clearingHousePayorID').val();

                var addrStrt1 = $('#addrStrt1').val()+ ",";

                if($('#addrStrt2').val() == ""){

                    var addrStrt2 = "";

                }

                else{

                    var addrStrt2 = $('#addrStrt2').val()+ ",";

                }



                var addrCity = $('#addrCity').val()+ " ";

                var addrState = $('#addrState').val()+ " ";

                var addrZip = $('#addrZip').val();

                var addr = addrStrt1+addrStrt2+addrCity+addrState+addrZip;

                var practiceID = sessionStorage.getItem("practiceId");

                var insuranceID = document.getElementById("hdnFlag").value;

                $.ajax({

                    type: "POST",

                    url: root_url+"insurances/update",

                    data:{

                        "insuranceID" : insuranceID,

                        "payerName" : payerName,

                        "clearingHousePayorID" : clearingHousePayorID,

                        "addr" : addr,

                        "practiceID" : practiceID,

                    },success:function(result){

                        var res = JSON.parse(result);
                        new PNotify({
                            type:'success',
                            text: json.message,
                            after_init: function(notice){
                                notice.attention('rubberBand');
                            }
                        })

                    }

                });

            }

        });

    });

</script>

</body>

</html>
<?php echo $__env->make('layout.handler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>