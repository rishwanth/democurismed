<?php
    $base_url = URL::to('/');
?>

<?php $__env->startSection('content'); ?>
<section class="content">
    <div class="notify_page">
	
		<div class="card ">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="ti-bell"></i> Notifications
                </h3>
                <span class="float-right hidden-xs">
                    <i class="fa fa-fw ti-angle-up clickable"></i>
                    <i class="fa fa-fw ti-close removecard"></i>
                </span>
            </div>
            <div class="card-body">
			
				<div class="row">
					
					<form id="myform" method="post">

						<div class="form-group">
							<label>Enter the file name: </label>
							<input class="form-control" type="text" id="filename" value="test" /> 
						</div>
						<div class="form-group">
							<label>Select file: </label>
							<input class="form-control" type="file" id="myfile" />
						</div>
						<div class="form-group">
							<div class="progress">
								<div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
							</div>

							<div class="msg"></div>
						</div>

						<input type="button" id="btn" class="btn-success" value="Upload" />
					</form>
				</div>
			</div>
		</div>
    </div>
    <div class="background-overlay"></div>
</section>
<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.animate.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.buttons.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.confirm.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.nonblock.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.mobile.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.desktop.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.history.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.callbacks.js"></script>
<script src="<?php echo $base_url; ?>/public/js/custom_js/notifications.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>


<script>
	$(function () {
		var root_url = $("#hidden_root_url").val();
		$('#btn').click(function () {
			$('.myprogress').css('width', '0');
			$('.msg').text('');
			var filename = $('#filename').val();
			var myfile = $('#myfile').val();
			if (filename == '' || myfile == '') {
				alert('Please enter file name and select file');
				return;
			}
			var formData = new FormData();
			formData.append('myfile', $('#myfile')[0].files[0]);
			formData.append('filename', filename);
			$('#btn').attr('disabled', 'disabled');
			 $('.msg').text('Uploading in progress...');
			 
			 
			 var percent = 0;
			var notice = new PNotify({
				text: "Please Wait",
				type: 'info',
				icon: 'fa fa-spinner fa-spin',
				hide: false,
				buttons: {
					closer: false,
					sticker: false
				},
				opacity: .75,
				shadow: false,
				width: "170px"
			});
			 
			 
			 
			$.ajax({
				url: root_url+'document/uploadfile',
				data: formData,
				processData: false,
				contentType: false,
				type: 'POST',
				// this part is progress bar
				xhr: function () {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", function (evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							percentComplete = parseInt(percentComplete * 100);
							
							$('.myprogress').text(percentComplete + '%');
							$('.myprogress').css('width', percentComplete + '%');
							
							var percent = percentComplete;
							
							var options = {
								text: percent + "% complete."
							};
							
							if (percent == 80) options.title = "Almost There";
							if (percent >= 100) {								
								options.title = "Done!";
								options.type = "success";
								options.hide = true;
								options.buttons = {
									closer: true,
									sticker: true
								};
								options.icon = 'fa fa-check';
								options.opacity = 1;
								options.shadow = true;
								options.width = PNotify.prototype.options.width;
							}
							notice.update(options);
									
							
						}
					}, false);
					return xhr;
				},
				success: function (data) {
					$('.msg').text(data);
					$('#btn').removeAttr('disabled');
				}
			});
		});
		
		
		$('.dynamic_notice_new').on('click', function () {
			var percent = 0;
			var notice = new PNotify({
				text: "Please Wait",
				type: 'info',
				icon: 'fa fa-spinner fa-spin',
				hide: false,
				buttons: {
					closer: false,
					sticker: false
				},
				opacity: .75,
				shadow: false,
				width: "170px"
			});

			setTimeout(function () {
				notice.update({
					title: false
				});
				var interval = setInterval(function () {
					percent += 2;
					var options = {
						text: percent + "% complete."
					};
					if (percent == 80) options.title = "Almost There";
					if (percent >= 100) {
						window.clearInterval(interval);
						options.title = "Done!";
						options.type = "success";
						options.hide = true;
						options.delay = 3000;
						options.buttons = {
							closer: true,
							sticker: true
						};
						options.icon = 'fa fa-check';
						options.opacity = 1;
						options.shadow = true;
						options.width = PNotify.prototype.options.width;
					}
					notice.update(options);
				}, 120);
			}, 1500);
		});
		
		
		
		
	});
</script>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>