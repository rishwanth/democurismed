<?php
    $base_url = URL::to('/');
?>

<?php $__env->startSection('content'); ?>
<div class="row">
	<h2 style="color: #251367; margin-left:20px;">Practice Info</h2>
	<div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">

			<div class="card-body">
                <div class="bs-example">
                    <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                        <li class="nav-item">
                            <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Setup Facility</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Set Up Zones</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_3" style="color:#000;" data-toggle="tab" class="nav-link">Setup Holiday/PTO</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_4" style="color:#000;" data-toggle="tab" class="nav-link">Setup Activities Subtypes</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_5" style="color:#000;" data-toggle="tab" class="nav-link"> Create Session Rules </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_role" style="color:#000;" data-toggle="tab" class="nav-link"> Manage Role </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_6" style="color:#000;" data-toggle="tab" class="nav-link"> Setup Employee </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_7" style="color:#000;" data-toggle="tab" class="nav-link"> Add/Edit Users </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_8" style="color:#000;" data-toggle="tab" class="nav-link"> Manage Payroll </a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="tab_3_1">
                        	<div id="accordion" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                               Facility Name &amp; Location
                                            </a>
                                        </h5>
                                    </div>

                                    <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
			                            	<div class="row">

        		<div class="col-sm-3">
                    <div class="form-group row" style="margin-bottom:1rem">
                        <div class="col-12">
                        	<?php
                        	$val = '';
                        	if (isset($data['practice']['name'])){
                        		$val = $data['practice']['name']; 
                        	}
                        	?>
                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Facility Name" value="<?php echo $val; ?>" />
                        </div>
                    </div>
                </div>

            <div class="col-sm-3">
                <div class="form-group row">
                    <div class="col-12">
                    	<?php
                    	$name = 'address';
                    	$val = 'address';
                    	if (isset($data['practice'][$name])){
                    		$val = $data['practice'][$name]; 
                    	}
                    	?>
                        <input type="text" class="form-control" id="<?php echo e($name); ?>" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>"/>
                    </div>
                </div>
            </div>

		<?php
        	$name = 'speciality';
        	$val = 'speciality';
        	if (isset($data['practice'][$val])){
        		$val = $data['practice'][$val]; 
        	}
        	?>                                 	

            <div class="col-sm-3">
                <div class="form-group row">
                    <div class="col-12">
                        <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                    </div>
                </div>
            </div>

        	<?php
	        	$name = 'dob';
	        	$val = 'dob';
	        	if (isset($data['practice'][$name])){
	        		$val = $data['practice'][$name]; 
	        	}
        	?>

            <div class="col-sm-3">
                <div class="form-group row">
                    <div class="col-12">
                        <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                    </div>
                </div>
            </div>

        	<?php
	        	$name = 'ssn';
	        	$val = 'ssn';
	        	if (isset($data['practice'][$name])){
	        		$val = $data['practice'][$name]; 
	        	}
        	?>
            <div class="col-sm-3">
                <div class="form-group row" style="margin-bottom:1rem">
                    <div class="col-12">
                        <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                    </div>
                </div>
            </div>

        	<?php
	        	$name = 'ein';
	        	$val = 'ein';
	        	if (isset($data['practice'][$name])){
	        		$val = $data['practice'][$name]; 
	        	}
        	?>
            <div class="col-sm-3">
                <div class="form-group row">
                    <div class="col-12">
                        <input type="text" class="form-control" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" 
                        id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                    </div>
                </div>
            </div>

        <div class="col-sm-3">
            <div class="form-group row">
                <div class="col-12">
                	<?php
                    $name = 'phone_home';
                    $val = 'phone_home';
                	if (isset($data['practice'][$val])){
                		$val = $data['practice'][$val]; 
                	}
                	?>

                    <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group row">
                <div class="col-12">
				<?php
                	$name = 'fax';
                	$val = 'fax';
                	if (isset($data['practice'][$val])){
                		$val = $data['practice'][$val]; 
                	}
                	?>                                 	
                    <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" value="<?php echo e($val); ?>" placeholder="<?php echo e($name); ?>" />
                </div>
            </div>
        </div>

		<?php
        	$name = 'phone_work';
        	$val = 'phone_work';
        	if (isset($data['practice'][$val])){
        		$val = $data['practice'][$val]; 
        	}
    	?>                                 	

        <div class="col-sm-3">
            <div class="form-group row" style="margin-bottom:1rem">
                <div class="col-12">

                    <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" 
                    id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>"  />
                </div>
            </div>
        </div>

		<?php
        	$name = 'phone_mobile';
        	$val = 'phone_mobile';
        	if (isset($data['practice'][$val])){
        		$val = $data['practice'][$val]; 
        	}
    	?>                                 	

        <div class="col-sm-3">
            <div class="form-group row">
                <div class="col-12">
                    <input type="text" class="form-control" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" 
                    id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                </div>
            </div>
        </div>

        <?php
            $name = 'dept';
            $val = 'dept';
            if (isset($data['practice'][$val])){
                $val = $data['practice'][$val]; 
            }
        ?>                                  


        <div class="col-sm-3">
            <div class="form-group row">
                <div class="col-12">
                    <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" 
                    id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                </div>
            </div>
        </div>

        <?php
            $name = 'npi';
            $val = 'npi';
            if (isset($data['practice'][$val])){
                $val = $data['practice'][$val]; 
            }
        ?>                                  

        <div class="col-sm-3">
            <div class="form-group row">
                <div class="col-12">
                    <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" 
                id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                </div>
            </div>
        </div>

        <?php
            $name = 'notes';
            $val = 'notes';
            if (isset($data['practice'][$val])){
                $val = $data['practice'][$val]; 
            }
        ?>                                  

        <div class="col-sm-3">
            <div class="form-group row" style="margin-bottom:1rem">
                <div class="col-12">
                    <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" 
                id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                </div>
            </div>
        </div>

        <?php
            $name = 'is_active';
            $val = 'is_active';
            if (isset($data['practice'][$val])){
                $val = $data['practice'][$val]; 
            }
        ?>                                  

        <div class="col-sm-3">
            <div class="form-group row">
                <div class="col-12">
                    <input type="text" class="form-control" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" 
                    id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>" value="<?php echo e($val); ?>" />
                </div>
            </div>
        </div>


        <div class="col-sm-3">
            <div class="form-group row">
                <div class="col-12">
                	<label class="checkbox-inline icheckbox">
                        <input type="checkbox" class="uniform"  value="1"
                    id="<?php echo e($name); ?>" name="<?php echo e($name); ?>" placeholder="<?php echo e($name); ?>"  > Is Default Facility
                    </label>
                </div>
            </div>
        </div>


        <div class="col-sm-12">
            <div class="form-group row">
                <div class="col-12">
                    <textarea class="form-control" rows="5" cols="5"></textarea>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group row">
                <div class="col-12">
                    <button type="button" class="btn btn-primary" id="savePractice" name="savePractice" >Save </button>
                </div>
            </div>
        </div>

            	</div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                   Add Payors to the Facility
                </a>
            </h5>
        </div>

        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion" style="padding:20px;">
            <div class="card-body">
            	<select multiple="multiple" size="10" name="ddl_payer_list[]">
			    </select>
            </div>

                <button id="listPayer" name="">Handler List</button>
                <button id="selectedPayer" class="selectedPayer" name="">Selected List</button>

                <button id="saveAddPayors" class="btn btn-primary"
                name="saveAddPayors">Save</button>

            </div>
        </div>


    <div class="card">
        <div class="card-header" role="tab" id="headingfour">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                   Payor Setup
                </a>
            </h5>
        </div>

        <div id="collapsefour" class="collapse" role="tabpanel" aria-labelledby="headingfour" data-parent="#accordion" style="padding:20px;">
            <div class="card-body">
                <button class="selectedPayer" name="">Load / Reload List</button>

                <table class="table table-bordered" style="width:100%;" id="selectedPayerTable" name="selectedPayerTable">
            	</table>
            </div>
        </div>
    </div>

    <div class="card" style="display: none;">
        <div class="card-header" role="tab" id="headingfive">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                   Payroll Integration
                </a>
            </h5>
        </div>

                                    <div id="collapsefive" class="collapse" role="tabpanel" aria-labelledby="headingfive" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                                        	<div class="row">
                                        		<div class="col-sm-3">
			                                        <div class="form-group row" style="margin-bottom:1rem">
			                                            <div class="col-12">
			                                                <select class="form-control">
			                                                	<option value="">--Payroll Process--</option>
			                                                	<option value="Manual">Manual</option>
			                                                	<option value="ADP">ADP</option>
			                                                </select>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-9">
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="ADP Company Code" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Mileage Rate Code" />
			                                            </div>
			                                        </div>
			                                    </div>
                                        	</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card" style="display: none;">
                                    <div class="card-header" role="tab" id="headingsix">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                               Company Logo Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapsesix" class="collapse" role="tabpanel" aria-labelledby="headingsix" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
<form id="formUploadFile" method="post" enctype="multipart/form-data" 
action="<?php echo e(URL::to('/')); ?>/practices/uploadlogo" >
                                    <input type="hidden" id="practice_id" name="practice_id" value="1">                                        		
                                            	<input type="file" id="logo" name="logo" class="form-control"/>

                                            	<button type="submit">Submit</button>
                                        	</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="tab_3_2">
    	<a href="javascript:void(0)" class="btn btn-primary" id="linkNewZone">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>
                            <table class="table table-bordered" style="width:100%;" id="zoneTable" name="zoneTable">
                        		
                        		
                        	</table>
                        </div>

                        <div class="tab-pane fade" id="tab_3_3">
    	<a href="javascript:void(0)" class="btn btn-primary linknew" id="linkNewHoliday" data-popup="AddHoliday">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>

                            <table class="table table-bordered" style="width:100%;"id="holidayTable" name="holidayTable" >
                        	</table>
                        </div>

                        <div class="tab-pane fade" id="tab_3_4">

                        	<div id="accordion1" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading7">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                               Service/Activity Sub Types
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse7" class="collapse show" role="tabpanel" aria-labelledby="heading7" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                            <div class="form-group col-md-6">
							                    <label class="control-label col-md-12">Type </label>
							                    <div class="col-md-12">
							                        <select class="form-control">
												      <option value="Billable">Billable</option>
												      <option value="Non-billable">Non-billable</option>
												    </select>
							                    </div>
							                </div>
							                <div class="form-group col-md-6">
							                    <label class="control-label col-md-12">Activity </label>
							                    <div class="col-md-12">
							                        <select class="form-control" id="ddlActivity" name="ddlActivity">
														<option selected="selected" value="--Select One--">--Select One--</option>
													</select>

    	<a href="javascript:void(0)" class="btn btn-primary linksave" id="saveActivitiesSubtypes" data-save="saveActivitiesSubtypes">
    	<i class="icon icon-plus"></i>&nbsp;Save</a>

							                    </div>
							                </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_5">
    	<a href="javascript:void(0)" class="btn btn-primary linknew" id="linkNewSessionRule" data-popup="AddSessionRule">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>

                        	<table class="table" cellspacing="0" border="1" style="width:100%;border-collapse:collapse;" id="sessionRuleTable" name="sessionRuleTable">
															</table>
                        </div>

                        <div class="tab-pane fade" id="tab_role">
        <a href="javascript:void(0)" class="btn btn-primary linknew" id="linkNewRole" data-popup="AddRole">
        <i class="icon icon-plus"></i>&nbsp;Add New</a>

                            <table class="table" cellspacing="0" border="1" style="width:100%;border-collapse:collapse;" id="roleTable" name="roleTable">
                            </table>
                        </div>

                        <div class="tab-pane fade" id="tab_3_6">
                        	<div id="accordion2" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading9">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                               Employee
                                            </a>
                                        </h5>

                                    </div>
                                    <div id="collapse9" class="collapse show" role="tabpanel" aria-labelledby="heading9" data-parent="#accordion" style="padding:20px;display: none1;">
    	<a href="javascript:void(0)" class="btn btn-primary linknew" id="linkNewEmployee" data-popup="AddEmployee">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>

                                        <div class="card-body">
                            <table class="table table-bordered" style="width:100%;" id="employeeTable" name="employeeTable">
                        		
                        		
                        	</table>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade" id="tab_3_7">
    	<a href="javascript:void(0)" class="btn btn-primary linknew" id="linkNewUser" data-popup="AddUser">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>

                        	<table class="table" cellspacing="0" border="1" style="width:100%;border-collapse:collapse;" id="userTable" name="userTable">
						</table>
                        	
                        </div>

                        <div class="tab-pane fade" id="tab_3_8">
                        	<div id="accordion3" role="tablist">
                        		<div class="card">
                                    <div class="card-header" role="tab" id="heading14">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                               Hide Timesheet Link
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse14" class="collapse show" role="tabpanel" aria-labelledby="heading14" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                        	<input type="button" value="Hide Timesheet Link" class="btn btn-primary float-right">
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading15">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                               Disallow Rendering Session
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse15" class="collapse" role="tabpanel" aria-labelledby="heading15" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                        	<div class="form-group col-md-12">
                                        		<input type="button" value="Save" class="btn btn-primary float-right col-md-1" />
                                        		<input type="text" class="form-control float-right col-md-3" style="line-height:28px; margin-right:15px;" placeholder="mm/dd/yyyy" />
							                    <label class="control-label float-right col-md-3">Disallow Rendering Session Before </label>
							                </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading16">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                               Pay Periods
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse16" class="collapse" role="tabpanel" aria-labelledby="heading16" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                        	<input type="button" value="Create Pay Periods" id="btnCreatePay" class="btn btn-primary float-right">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_9">
                        	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
	<div class="modal fade" id="myModal1" tabindex="-1">
		<div class="modal-dialog" style="width:70%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Practice</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:50px;">
					<div class="card-body">
  						<div class="row">
							<input type="hidden" id="hdnFlag"/>
							<div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Practice Name </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="practiceName1" name="PrimaryPhysiName" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6">
			                	<label class="control-label col-md-12">Speciality </label>
			                    <div class="col-md-12">
			                        <select class="form-control" id="phySpeciality">
			                        	<option></option>
			                        	<option>Psychologist</option>
			                        	<option>Counselor</option>
			                        </select>
			                    </div>
			            	</div>
			            	<div class="clearfix"></div>

			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Group NPI </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyNPI" name="txtPTNumber"  value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Date Of Birth </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyDOB" name="txtPTNumber"  value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Tax ID </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyEIN" name="txtPTNumber" value="" />
			                    </div>
			                </div>

			            	<div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Social Security # </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phySSN" name="txtPTNumber"  value="" />
			                    </div>
			                </div>
			                <div class="clearfix"></div>
			                <hr/>
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Address </label>
			                    <div class="col-md-12">
			                    	<textarea class="form-control" cols="5" rows="4" id="phyAddr"></textarea>
			                    </div>
			                </div>

			                <div class="form-group col-md-6" >
			                    <label class="control-label col-md-12">Home</label>
			                    <div class="col-md-12">
			                         <input type="text" class="form-control required" id="phyHomePhone" name="txtPTNumber"  value="" />
			                    </div>
			                </div>

			                 <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Work </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyWorkPhone" name="txtPTNumber" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6" >
			                    <label class="control-label col-md-12">Mobile </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyMobilePhone" name="txtPTNumber" value="" />
			                    </div>
			                </div>
			                <div class="clearfix"></div>
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Dept </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyDept" name="txtPTNumber" value="" />
			                    </div>
			                </div>
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Fax </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyFax" name="txtPTNumber" value="" />
			                    </div>
			                </div>
			                <div class="clearfix"></div>
			                <hr/>
			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-2">Notes </label>
			                    <div class="col-md-12">
			                    	<textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Add Practice" class="btn btn-primary" id="btnAddPractice"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div class="modal fade" id="mdlZone" tabindex="-1">
		<div class="modal-dialog" style="width:70%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Zone</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:50px;">
					<div class="card-body">
  						<div class="row">
							<input type="hidden" id="hdnFlag"/>
							<div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Name </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="practiceName1" name="PrimaryPhysiName" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-2">Description </label>
			                    <div class="col-md-12">
			                    	<textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Add Zone" class="btn btn-primary" id="btnSaveZone"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div class="modal fade" id="mdlHoliday" tabindex="-1">
		<div class="modal-dialog" style="width:70%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Holiday</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:50px;">
					<div class="card-body">
  						<div class="row">
							<input type="hidden" id="hdnFlag"/>
							<div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Date </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" placeholder="mm/dd/yyyy" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-2">Description </label>
			                    <div class="col-md-12">
			                    	<textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Add Holiday" class="btn btn-primary" id="btnSaveHoliday"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div class="modal fade" id="mdlPayPeriod" tabindex="-1">
		<div class="modal-dialog modal-lg" style="width:80%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Setup Payroll</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:50px;">
					<div class="card-body">
  						<div class="row">
							<div class="form-group col-md-12" style="padding-bottom:10px;">
			                    <label class="control-label col-md-12">How often do you pay your employees? </label>
			                    <div class="col-md-4">
			                        <select class="form-control">
			                        	<option selected="selected" value="1">Weekly</option>
										<option value="2">Bi-Weekly</option>
										<option value="3">From 1st &amp; 16th Every Month</option>
										<option value="4">Monthly</option>
										<option value="5">Custom</option>

			                        </select>
			                    </div>
			                </div><br/>
			                <div class="form-group col-md-4" style="padding-bottom:10px;">
			                    <label class="control-label col-md-12">Select Year </label>
			                    <div class="col-md-12">
			                        <select class="form-control">
			                        	<option value="2017">2017</option>
										<option selected="selected" value="2018">2018</option>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
			                        </select>
			                    </div>
			                </div>
			                <div class="form-group col-md-4">
			                    <label class="control-label col-md-12">What is the next pay period end date? </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" />
			                    </div>
			                </div>
			                <div class="form-group col-md-4">
			                    <label class="control-label col-md-12">What is the check date for next pay period? </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" />
			                    </div>
			                </div>

			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-12">After how many days employee can't submit time sheet? </label>
			                    <div class="col-md-4">
			                    	<input type="text" class="form-control" />
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Create Pay Period for this year" class="btn btn-primary" id="btnSavePay"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

<!-- AddZone -->
<div class="modal fade" id="AddZone" tabindex="-1">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New Zone</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Name </label>
                            <div class="col-md-12">
                                <input type="hidden" id="zone_id" name="zone_id" class="form-control" />
                                <input type="text" id="zone_name" name="zone_name" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Description </label>
                            <div class="col-md-12">
                                <input type="text" id="description" name="description"  class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Practice ID </label>
                            <div class="col-md-12">
                                <input type="text" id="practice_id" name="practice_id"  value="1" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Active (1/0) </label>
                            <div class="col-md-12">
                                <input type="text" id="is_active" name="is_active"  class="form-control" value="1" />
                            </div>
                        </div>
                    </div>


                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSave" class="btn btn-primary btnsave" data-dismiss1="modal" value="Save" data-btnname="zone" data-btnurl="zone" />
            </div>
            
        </div>
    </div>
</div>

<!-- AddRole -->
<div class="modal fade" id="AddRole" tabindex="-1">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New Role</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Name </label>
                            <div class="col-md-12">
                                <input type="hidden" id="role_id" name="role_id" class="form-control" />
                                <input type="text" id="role_name" name="role_name" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Active (1/0) </label>
                            <div class="col-md-12">
                                <input type="text" id="is_active" name="is_active"  class="form-control" value="1" />
                            </div>
                        </div>
                    </div>


                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSave" class="btn btn-primary btnsave" data-dismiss1="modal" value="Save" data-btnname="role" data-btnurl="role" />
            </div>
            
        </div>
    </div>
</div>


<!-- AddHoliday -->
<div class="modal fade" id="AddHoliday" tabindex="-1">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Date </label>
                            <div class="col-md-12">
                                <input type="hidden" id="holiday_id" name="holiday_id" class="form-control" />
                                <input type="text" id="date" name="date" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Description </label>
                            <div class="col-md-12">
                                <input type="text" id="description" name="description" class="date holiday_description form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Practice  </label>
                            <div class="col-md-12">
                                <input type="text" id="	practice_id" name="	practice_id" value="1" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Active </label>
                            <div class="col-md-12">
                                <input type="text" id="is_active" name="is_active" value="1" class="form-control" />
                            </div>
                        </div>
                    </div>

                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSave" class="btn btn-primary btnsave" data-dismiss1="modal" value="Save" data-btnname="holiday" data-btnurl="" />
            </div>
            
        </div>
    </div>
</div>

<!-- AddSessionRule -->
<div class="modal fade" id="AddSessionRule" tabindex="-1">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Name </label>
                            <div class="col-md-12">
                        <input type="hidden" id="sessionrule_id" name="sessionrule_id" class="form-control" value="0" />
                        <input type="text" id="sessionrule_name" name="sessionrule_name" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Description </label>
                            <div class="col-md-12">
                    <input type="text" id="sessionrule_description" name="sessionrule_description" class="sessionrule_description form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Practice  </label>
                            <div class="col-md-12">
                                <input type="text" id=" practice_id" name=" practice_id" value="1" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Active </label>
                            <div class="col-md-12">
                                <input type="text" id="is_active" name="is_active" value="1" class="form-control" />
                            </div>
                        </div>
                    </div>

                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSave" class="btn btn-primary btnsave" data-dismiss1="modal" value="Save" data-btnname="sessionrule" data-btnurl="" data-btnsave="" />
            </div>
            
        </div>
    </div>
</div>

<!-- AddUser -->
<div class="modal fade" id="AddUser" tabindex="-1">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New / Edit User</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">First Name </label>
                            <div class="col-md-12">
                                <input type="hidden" id="user_id" name="user_id" class="form-control" />
                                <input type="text" id="first_name" name="first_name" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Last Name </label>
                            <div class="col-md-12">
                                <input type="text" id="last_name" name="last_name" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Username </label>
                            <div class="col-md-12">
                                <input type="text" id="username" name="username" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Role </label>
                            <div class="col-md-12">
                                <input type="text" id="role" name="role" class="form-control" style="display:none;" />
                                <select id="user_role_name" name="screenName">
                                    <option value="0">Select Role</option>
                                </select>
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Email ID</label>
                            <div class="col-md-12">
                                <input type="text" id="email_id" name="email_id" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Practice</label>
                            <div class="col-md-12">
                                <input type="text" id="user_practice_id" name="user_practice_id" class="form-control" />
                            </div>
                        </div>
                    </div>
                    
                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Mobile</label>
                            <div class="col-md-12">
                                <input type="text" id="phone_num" name="phone_num" class="form-control" />
                            </div>
                        </div>
                    </div>


                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSave" name="btnSave" class="btn btn-primary btnsave" data-dismiss1="modal" value="Save" data-btnname="user" data-btnsave="register/update" />
            </div>
            
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	
	<script>
        $(document).ready(function(){
			var dualListContainer = $('[name="ddl_payer_list[]"]').bootstrapDualListbox({
                moveOnSelect: false
            });   
			listPayer();
            // dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
            // dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
            // dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
            // dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');
            $(document).on('click','.bootstrap-duallistbox-container .move',function(){
                ddlPayerListClick();
            })
            //ddlPayerListClick();

            $('#savePractice').click(function(){
                savePractice();
            });

            $('#saveAddPayors').click(function(){
                saveAddPayors('', 'add');
            });

            $(document).on('click','.zone-edit',function(){
                var id = $(this).data('id');
                editZone(id);
            })
            $(document).on('click','.role-edit',function(){
                var id = $(this).data('id');
                editRole(id);
            })
            $(document).on('click', '.role-permission',function(){
                var id = $(this).data('id');
                alert('.role-permission => ' + id);
            })

            $(document).on('click','.holiday-edit',function(){
                var id = $(this).data('id');
                editHoliday(id);
            })
            $(document).on('click','.session-edit',function(){
                var id = $(this).data('id');
                editSessionRule(id);
            })

            $(document).on('click', '.zone-delete',function(){
                var id = $(this).data('id');
                deleteZone(id);
            })
            $(document).on('click', '.holiday-delete',function(){
                var id = $(this).data('id');
                deleteHoliday(id);
            })
            $(document).on('click', '.session-delete',function(){
                var id = $(this).data('id');
                deleteSessionRule(id);
            })

            $(document).on('click', '.employee-delete',function(){
                var id = $(this).data('id');
                deleteEmployee(id);
            })
            $(document).on('click', '.user-delete',function(){
                var id = $(this).data('id');
                deleteUser(id);
            })
            $(document).on('click', '.user-permission',function(){
                var id = $(this).data('id');
                alert('.user-permission => ' + id);
            })
            $(document).on('click', '.payer-delete',function(){
                var id = $(this).data('id');
                var pid = $(this).data('pid');
                deletePayer(id, pid);
            })



            $(document).on('click','.user-edit',function(){
                var id = $(this).data('id');
                editUser(id);
            })


	        $('.selectedPayer').click(function(){
	        	selectedPayer();
            });

            $('.btnUpdatePayerSetup').click(function(){
                updatePayerSetup(this);
            });
            $('.payer-edit').click(function(){
                updatePayerSetup(this);
            });



			$(".ti-menu-alt").click();


             fillRole();
      	     fillSelectedPayer();
        	fillZone();
        	fillEmployee();
        	fillHoliday();
        	fillActivitiesSubtypes();
        	fillSessionRules();
        	fillUser();

	        $('#linkNewZone').click(function(){
            	$('#AddZone').modal('show');
                $('#zone_name').val('');
                $('#zone_id').val(0);
                $('#description').val('');
            });

            
	        $('.btnsave').click(function(){
	        	var name  =  $(this).data('btnname');
	        	var url  =  $(this).data('btnsave');
	        	//alert('btnSave ' + c);
	        	btnsave(name, url);
            });

	        $('.linknew').click(function(){
                var popupname = $(this).data('popup');
	        	var c  = '#' + popupname;
                alert(popupname);
            	$(c).modal('show');
                if (popupname == 'AddUser'){
                    $('#user_id').val(0);
                    fillRole('AddUser');

                } else if (popupname == 'AddHoliday'){
                    $('#holiday_id').val(0);
                    $("#date").val('');
                    $('#description').val('');
                    $('.holiday_description').val('');
                    $('#is_active').val(1);
                } else if (popupname == 'AddRole'){
                    $('#role_id').val(0);
                    $("#role_name").val('');
                    $('#is_active').val(1);
                    fillRole('role');
                } else if (popupname == 'AddSessionRule'){
                    $('#sessionrule_id').val(0);
                    $('#sessionrule_name').val('');
                    $('#sessionrule_description').val('');
                }

            });

	        $('.linksave').click(function(){
	        	var c  = $(this).data('save');
            	//$(c).modal('show');
	        	c  = '#' + c;
            });

            

            
            

            $(".sidebar-toggle").click();

            $('#masFacPhone').mask('(000) 000-0000');
            $('#masFacPhone2').mask('(000) 000-0000');
            $('#masFacFax').mask('(000) 000-0000');

            $(document).on('focusout','#masFacEmail',function() {
                if($(this).val()!=""){
                    if( !validateEmail($(this).val())) {
                        alertify.error("Please enter a valid email address");
                        $("#masFacEmail").focus();
                    }
                }
            });


        });
        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test( $email );
        }
	</script>

<script type="text/javascript">
function fillTable(tableName, screenName){
	var root_url = $("#hidden_root_url").val();
    var url = root_url+"practices/listholiday";
    var practice_id = 1;
    var response = [];
    var handler = '';
    if (tableName == 'zone'){
	    url = root_url + "practices/listzone";
    } else if (tableName == 'employee'){
	    url = root_url + "employee/list";
    } else if (tableName == 'holiday'){
	    url = root_url + "practices/listholiday";
    } else if (tableName == 'handler_activity'){
	    url = root_url + "handler/get";
	    handler = 'activity';
    } else if (tableName == 'handler_insurances'){
	    url = root_url + "handler/get";
	    handler = 'insurances';
    } else if (tableName == 'handler_insurances_right'){
        url = root_url + "handler/get";
        handler = 'insurances_right';
    } else if (tableName == 'session_rules'){
	    url = root_url + "handler/get";
	    handler = 'session_rule';
    } else if (tableName == 'user'){
	    url = root_url + "practices/getusers";
	    //handler = 'session_rule';
    } else if (tableName == 'insurances'){
	    url = root_url + "insurances/list";
    } else if (tableName == 'selectedpayer'){
	    url = root_url + "practices/listpayors";
    } else if (tableName == 'sessionrules'){
        url = root_url + "practices/listsessionrule";
    } else if (tableName == 'role'){
        url = root_url + "practices/listrole";
    }

    $.ajax({
        type: "POST",
        url: url,
        async: false,
        data:{
            practice_id : practice_id,
            handler: handler,
        },
        success:function(result){
            var res = JSON.parse(result);
            if(res.status == 0){
                //return res;
            }
		    if (tableName == 'zone'){
            	fillZoneSuccess(res);
            } else if (tableName == 'role'){
                fillRoleSuccess(res, screenName);
		    } else if (tableName == 'holiday'){
            	fillHolidaySuccess(res);
		    } else if (tableName == 'handler_activity'){
            	fillActivitiesSubtypesSuccess(res);
  			} else if (tableName == 'sessionrules'){
  				fillSessionRulesSuccess(res);
  			} else if (tableName == 'user'){
  				fillUserSuccess(res);
  			} else if (tableName == 'selectedpayer'){
  				fillSelectedPayerSuccess(res);
		    } else if (tableName == 'handler_insurances'){
            	fillHandlerInsuranceSuccess(res);
    		} else if (tableName == 'handler_insurances_right'){
                fillHandlerInsuranceSuccessRight(res);
            } else if (tableName == 'employee'){
            	fillEmployeeSuccess(res);
		    } else {
		    	response = res;
		    	return response;
		    }
		}
    });	
	
}

function fillRole(screen){
    fillTable('role', screen);
}   

function fillEmployee(){
	fillTable('employee');
}	

function fillZone(){
	fillTable('zone');
}	

function fillHoliday(){
	fillTable('holiday');
}

function fillActivitiesSubtypes(){
	fillTable('handler_activity');
}

function fillUser(){
	fillTable('user');
}	

function fillHandlerInsuranceSuccess (res){
    var data1 = res.data;
    var dt = [];
    var id, c;
    //$('#ddl_payer_list').empty();
    var data=[];
    $.each(data1,function(i,  v) {
        var demo1 = $('select[name="ddl_payer_list[]"]').bootstrapDualListbox();
        id = data1[i].id;
        c = data1[i].payer_name;
        //c = '<option value="' + id + '">' + c + '</option>';
        var c  = "<option value=\'" + data1[i].id + "\'>" + data1[i].payer_name + "</option>";
		demo1.append(c);
        $('select[name="ddl_payer_list[]"]').bootstrapDualListbox('refresh', true);
    });
    $('select[name="ddl_payer_list[]"]').bootstrapDualListbox({
            moveOnSelect: false,
            selectorMinimalHeight: 160
    });

}
function fillHandlerInsuranceSuccessRight (res){
    var data1 = res.data;
    var dt = [];
    var id, c;
    //$('#ddl_payer_list').empty();
    var data=[];
    $.each(data1,function(i,  v) {
        id = data1[i].id;
        c = data1[i].payer_name;
        //c = '<option value="' + id + '">' + c + '</option>';
        var c  = "<option value=\'" + data1[i].id + "\'>" + data1[i].payer_name + "</option>";
        $('bootstrap-duallistbox-nonselected-list_duallistbox_demo1[]').append(c);
        $('bootstrap-duallistbox-nonselected-list_duallistbox_demo1[]').bootstrapDualListbox('refresh', true);
    });
    $('bootstrap-duallistbox-nonselected-list_duallistbox_demo1[]').bootstrapDualListbox({
            moveOnSelect: false,
            selectorMinimalHeight: 160
    });

}

function fillUserSuccess (res){
	var c = 'fillUserSuccess => ' + res.data.length;
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id, 
            data1[i].first_name, 
            data1[i].last_name,
            data1[i].role,
            data1[i].id 
            ]);
    });

    $('#userTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "First Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Last Name", "width": "120px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Role", "width": "120px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    var root_url = $("#hidden_root_url").val();
                    var href = ' target="_new" href="' + root_url + 'acl?user_id=' + data + '"';
                    return '<a data-id="'+data+'" class="user-edit" ><span class="ti-pencil  "></span></a> | <a data-id="'+data+'" class="user-delete" ><span class="ti-trash "></span></a> | <a data-id="'+data+'" ' + href + ' class="_user-permission" >Permission</a>';
                }
            },
        ]
    });

}


function fillActivitiesSubtypesSuccess(res){
	var c = 'fillActivitiesSubtypesSuccess => ' + res.data.length;
}

function fillSessionRules(){
	fillTable('sessionrules');
}

function fillSessionRulesSuccess(res){
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id,
            data1[i].name,
            data1[i].description,
            data1[i].id]);
    });

    $('#sessionRuleTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "300px", "targets": 1 },
            { "width": "300px", "targets": 2 },
            { "width": "200px", "targets": 3 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Description", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a data-id="'+data+'" class="session-edit" ><span class="ti-pencil "></span></a> | <a data-id="'+data+'" class="session-delete" ><span class="ti-trash "></span></a>';
                }
            },
        ]
    });
}


function fillHolidaySuccess(res){
    var data1 = res.data;
    var dt = [];
    console.log(data1);

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id,
            data1[i].date,
            data1[i].description,
            data1[i].id]);
    });

    //alert('return =>fillHolidaySuccess => ' + dt.length);
    //return;

    $('#holidayTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Description", "width": "120px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a data-id="'+data+'" class="holiday-edit" ><span class="ti-pencil "></span></a> | <a data-id="'+data+'" class="holiday-delete" ><span class="ti-trash "></span></a>';
                }
            },
        ]
    });
}

function fillZoneSuccess(res){
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id,
            data1[i].name,
            data1[i].description,
            data1[i].id]
        );
    });

    $('#zoneTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Description", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a data-id="'+data+'" class=" zone-edit" ><span class="ti-pencil  "></span></a> | <a data-id="'+data+'" class="zone-delete" ><span class="ti-trash "></span></a>';
                }
            },
        ]
    });
}

function btnsave(name, url){
    alert(name);
	if (name == 'user'){
		saveUser(url);
	} else if (name == 'zone'){
		saveZone(url);
    } else if (name == 'role'){
        saveRole(url);
	} else if (name == 'holiday'){
		saveHoliday(url);
    } else if (name == 'sessionrule'){
        saveSessionRule(url);
	}
}

function saveUser(url) {
    var _token = $('#_token').val();
    var id = $("#user_id").val();
	var first_name = $("#first_name").val();
	var last_name = $('#last_name').val();
	var username = $('#username').val();
	var role = $('#user_role_name').val();
	var practice_id = $('#user_practice_id').val();
	var email_id = $('#email_id').val();
	var phone_num = $('#phone_num').val();
    var url = "<?php echo e(URL::to('practices/updateuser')); ?>";
    if (id == 0){
        url = "<?php echo e(URL::to('register/update')); ?>";
    }
    $(".preloader").show();
	$.ajax({
        type: "POST",
        url: url,
        data:{ 
            "_token" : _token,
            "id" : id,
        	"firstName" : first_name,
        	"lastName" : last_name,
        	"userName" : username,
        	"role" : role,
        	"emailID" : email_id,
        	"mobileNo" : phone_num,
        	"practiceID" : practice_id,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                    fillUser();
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    $("#user_id").val(json.data.id);
                    fillUser();
                }
             }
    });
}

function saveZone(url) {
    var _token = $('#_token').val();
    var zone_id = $("#zone_id").val();
    var name = $("#zone_name").val();
	var description = $('#description').val();
	var practice_id = $('#practice_id').val();
	var is_active = $('#is_active').val();
    $(".preloader").show();
	$.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/addzone')); ?>",
        data:{ 
            "_token" : _token,
            "id" : zone_id,
            "name" : name,
        	"description" : description,
        	"practice_id" : practice_id,
        	"is_active" : is_active,
        	"practiceID" : practice_id,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillZone();
                    //window.location.href = root_url;
                }
             }
    });
}

function saveRole(url) {
    var _token = $('#_token').val();
    var role_id = $("#role_id").val();
    var role_name = $("#role_name").val();
    var is_active = $('#is_active').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/saverole')); ?>",
        data:{ 
            "_token" : _token,
            "role_id" : role_id,
            "role_name" : role_name,
            "is_active" : is_active,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillRole();
                }
             }
    });
}

function saveHoliday(url) {
    var _token = $('#_token').val();
    var holiday_id = $('#holiday_id').val();
	var date = $("#date").val();
	var description = $('.holiday_description').val();
	var practice_id = $('#practice_id').val();
	var is_active = $('#is_active').val();
    $(".preloader").show();
	$.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/addholiday')); ?>",
        data:{ 
            "_token" : _token,
            "id" : holiday_id,
        	"date" : date,
        	"description" : description,
        	"practice_id" : practice_id,
        	"is_active" : is_active,
        	"practiceID" : practice_id,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillHoliday();
                    //window.location.href = root_url;
                }
             }
    });
}

function saveSessionRule(url) {
    var _token = $('#_token').val();
    var id = $("#sessionrule_id").val();
    var name = $("#sessionrule_name").val();
    var description = $('#sessionrule_description').val();
    var practice_id = $('#practice_id').val();
    var is_active = $('#is_active').val();
    var step = 'add';

    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/sessionrule')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : step,
            "name" : name,
            "description" : description,
            "practice_id" : practice_id,
            "is_active" : is_active,
            "practiceID" : practice_id,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillSessionRules();
                    //window.location.href = root_url;
                }
             }
    });
}

function listPayer(){
	fillTable('handler_insurances');
}

function selectedPayer(){
	var data = fillTable('selectedpayer');
}

function fillSelectedPayer(){
	//alert('fillSelectedPayer()');
	fillTable('selectedpayer');
}

function fillSelectedPayerSuccess(res){
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id, 
            data1[i].payer_name,
        	data1[i].cms1500_31, 
            data1[i].cms1500_32a,
            data1[i].cms1500_32b,
            data1[i].cms1500_33a, 
            data1[i].cms1500_33b,             
            data1[i].id,
            data1[i].pid,
            ]);
    });

    $('#selectedPayerTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "200px", "targets": 2 },
            { "width": "100px", "targets": 3 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "cms1500_31", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var id = full[0];
                    return '<input type="text" id="cms1500_31_' + id + '" value="'+data+'"/>';
                }
            },
            {"title": "cms1500_32a", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var id = full[0];
                    return '<input type="text" id="cms1500_32a_' + id + '" value="'+data+'"/>';
                }
            },
            {"title": "cms1500_32b", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var id = full[0];
                    return '<input type="text" id="cms1500_32b_' + id + '" value="'+data+'"/>';
                }
            },
            {"title": "cms1500_33a", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var id = full[0];
                    return '<input type="text" id="cms1500_33a_' + id + '" value="'+data+'"/>';
                }
            },
            {"title": "cms1500_33b", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var id = full[0];
                    return '<input type="text" id="cms1500_33b_' + id + '" value="'+data+'"/>';
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    var insurance_id = full[0];
                    var pid = full[8];
                    //alert(pid);
                    c = '<a data-id="' + data + '" class="payer-edit" onclick="updatePayerSetup(this)" ><span class="ti-save  "></span></a>';
                    //return '<input type="button" data-id="' + data +'" class="btnUpdatePayerSetup" onclick="updatePayerSetup(this)" value="Update"/>';
                    c += ' | <a data-pid="' + pid + '" data-id="'+data+'" data-insurance_id="' + insurance_id+ '" class="payer-delete" ><span class="ti-trash "></span></a>';
                    return c;
                }
            },
        ]
    });
}

function updatePayerSetup(_this){
    var _token = $('#_token').val();
	var uid = $(_this).data('id');
    var c;
    id = '#cms1500_31_' + uid;
    var cms1500_31 = $(id).val();

    id = '#cms1500_32a_' + uid;
    var cms1500_32a = $(id).val();

    id = '#cms1500_32b_' + uid;
    var cms1500_32b = $(id).val();

    id = '#cms1500_33a_' + uid;
    var cms1500_33a = $(id).val();

    id = '#cms1500_33b_' + uid;
    var cms1500_33b = $(id).val();

	var root_url = $("#hidden_root_url").val();
	var url = root_url + 'insurances/addsetup';
    $.ajax({
        type : "POST",
        url : url,
        data:{
            "_token" : _token,
            "id" : uid,
            "insurance_id" : id,
            "cms1500_31": cms1500_31,
            "cms1500_32a": cms1500_32a,
            "cms1500_32b": cms1500_32b,
            "cms1500_33a": cms1500_33a,
            "cms1500_33b": cms1500_33b,
        },success:function(result) {
            $('#loader').hide();
            console.log(result);
            var res = JSON.parse(result);
            if(res.status == 0){
                alert(res.message);
                return;
            }  else {
                alert(res.message);
            }
        }
    });
	
}

function fillEmployeeSuccess(res){
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id,
        	data1[i].name,
        	data1[i].individual_npi,
        	data1[i].phone_home,
        	data1[i].address,
        	data1[i].speciality,
        	data1[i].id]);
    });

    $('#employeeTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
            { "width": "200px", "targets": 3 },
            { "width": "200px", "targets": 4 },
            { "width": "100px", "targets": 5 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Npi", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Phone", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Address", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Speciality", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a data-id="'+data+'" class="employee-edit" ><span class="ti-pencil "></span></a> | <a data-id="'+data+'" class="employee-delete" ><span class="ti-trash "></span></a>';
                }
            },
        ]
    });
}

function savePractice(){
    var _token = $('#_token').val();
    var id = 1;
    var name1 = $("#name").val();
    var ein1 = $('#ein').val();
    var ssn1 = $('#ssn').val();
    var phone_mobile1 = $('#phone_mobile').val();
    var address1 = $('#address').val();
    var phone_home1 = $('#phone_home').val();
    var phone_work1 = $('#phone_work').val();
    var dept1 = $('#dept').val();
    var fax1 = $('#fax').val();
    var npi1 = $('#npi').val();
    var notes1 = $('#notes').val();
    var is_active1 = $('#is_active').val();
    var speciality1 = $('#speciality').val();
    var dob1 = $('#dob').val();
 

    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/save')); ?>",
        data:{ 
            "_token" : _token,
            "from" : "setup",
            "id" : id,
            "name" : name1,
            "practiceName" : name1,
            "phoneMobile" : phone_mobile1,
            "phone_mobile" : phone_mobile1,
            "speciality" : speciality1,
            "dob" : dob1,
            "ssn" : ssn1,
            "ein" : ein1,
            "phone_work" : phone_work1,
            "dept" : dept1,
            "fax" : fax1,
            "npi" : npi1,
            "notes" : notes1,
            "is_active" : is_active1,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    //window.location.href = root_url;
                }
             }
    });
}

function saveAddPayors(selectedId, action){
    //alert('saveAddPayors(){');
    //return;
    var ids = '';
    var c = '';
    
    if (selectedId.length > 0){
        ids = ' ' + selectedId + ' ,';
    } else {
        var ddl = '#bootstrap-duallistbox-selected-list_ddl_payer_list1 > option';
        $(ddl).each(function() {
            ids += this.value;
            ids += ', ';
        });   
    }
    if (ids.length < 1) {
        alert('please select the insurances');
        return;
    }
    c = 'selected insurance ids are' + ids;
    //alert(c);
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/saveaddpayors')); ?>",
        data:{ 
            "_token" : _token,
            "ids" : ids,
            "action" : action,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                }
                fillSelectedPayer()
             }
    });
}

function editZone(id) {
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/addzone')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'edit',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    res = json.data[0];
                    $('#AddZone').modal('show');
                    $('#zone_id').val(res.id);
                    $("#zone_name").val(res.name);
                    $('#description').val(res.description);
                    $('#is_active').val(res.is_active);
                }
             }
    });
}

function editHoliday(id) {
    alert(id);
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/addholiday')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'edit',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    res = json.data[0];
                    $('#AddHoliday').modal('show');
                    $('#holiday_id').val(res.id);
                    $("#date").val(res.date);
                    $('#description').val(res.description);
                    $('.holiday_description').val(res.description);
                    $('#is_active').val(res.is_active);
                }
             }
    });
}

function deleteZone(id) {
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/addzone')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'delete',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillZone();
                }
             }
    });
}

function deleteHoliday(id) {
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/addholiday')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'delete',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillHoliday();
                }
             }
    });
}

function editSessionRule(id) {
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/sessionrule')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'edit',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    res = json.data[0];
                    $('#AddSessionRule').modal('show');
                    $('#sessionrule_id').val(res.id);
                    $('#sessionrule_name').val(res.name);
                    $('#sessionrule_description').val(res.description);
                }
             }
    });
}

function deleteSessionRule(id) {
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/sessionrule')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'delete',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillSessionRules();
                }
             }
    });
}

function deleteEmployee(id) {
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('employee/save')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'delete',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillEmployee();
                }
             }
    });
}

function deleteUser(id) {
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/saveusers')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'delete',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillUser();
                }
             }
    });
}

function deletePayer(id, pid){
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/savepayors')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "pid" : pid,
            "step" : 'delete',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                    fillSelectedPayer();
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    fillSelectedPayer();
                }
             }
    });
}

function aclFieldsForUser(id) {
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('acl/listfields')); ?>",
        data:{ 
            "_token" : _token,
            "user_id" : 1,
            "step" : 'edit',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    res = json.data[0];
                    if (res.main_acl_key == 'USER_FIRST_NAME'){
                        if (res.is_display == 1){
                            $('#first_name').show();
                        } else {
                            $('#first_name').hide();
                        }
                        if (res.is_edit == 1){
                            //$('#first_name').attr('readonly', true);
                        } else {
                            $('#first_name').attr('readonly', true);
                        }
                    }
                    if (res.main_acl_key == 'USER_LAST_NAME'){
                        if (res.is_display == 1){
                            $('#last_name').show();
                        } else {
                            $('#last_name').hide();
                        }
                        if (res.is_edit == 1){
                            //$('#last_name').attr('readonly', true);
                        } else {
                            $('#last_name').attr('readonly', true);
                        }
                    }
                }
             }
    });
}

function editUser(id) {
    aclFieldsForUser(id);
    var _token = $('#_token').val();
    $(".preloader").show();
    $.ajax({
        type: "POST",
        url:"<?php echo e(URL::to('practices/saveusers')); ?>",
        data:{ 
            "_token" : _token,
            "id" : id,
            "step" : 'edit',
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    res = json.data[0];
                    $('#AddUser').modal('show');
                    $('#user_id').val(res.id);
                    $('#first_name').val(res.first_name);
                    $('#last_name').val(res.last_name);
                    $('#username').val(res.username);
                    $('#user_practice_id').val(res.practice_id);
                    $('#phone_num').val(res.phone_num);
                    $('#email_id').val(res.email_id);
                    $('#user_role_name').val(res.role);
                }
             }
    });
}

function fillRoleSuccess(res, screenName){
    var data1 = res.data;
    var dt = [];
        //alert(screenName);

    if (screenName == 'AddUser'){
        $.each(data1,function(i,  v) {
            var id = data1[i].id;
            var val = data1[i].name;
            var newOption = $('<option value="'+val+'">'+val+'</option>');
            $('#user_role_name').append(newOption);
            //alert(val);
        });

        return;
    }

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id,
            data1[i].name,
            data1[i].id]);
    });

    $('#roleTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "300px", "targets": 1 },
            { "width": "300px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Role", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    if (data <= 3){
                        return '';
                    }
                    var root_url = $("#hidden_root_url").val();
                    var href = ' target="_new" href="' + root_url + 'acl/role?role_id=' + data + '"';
                    return '<a data-id="'+data+'" class="role-edit" ><span class="ti-pencil "></span></a> | <a data-id="'+data+'" class="role-delete" ><span class="ti-trash "></span></a> | <a data-id="'+data+'" ' + href + ' class="_role-permission" >Permission</a>';
                }
            },
        ]
    });
}

function ddlPayerListClick(){
    var lastState = $('select[name="ddl_payer_list[]"]').val();
    $('[name="ddl_payer_list[]"]').on('change', function (e) {
        e.preventDefault();
        var newState = $(this).val();                     
        console.log(lastState);                        // selected before
        console.log(newState);                         // selected now
        var removedVal = $(lastState).not(newState).get(); // added elements
        var selectedVal = $(newState).not(lastState).get(); // removed elements
        lastState = newState;
        //alert(selectedVal);
        //alert(removedVal);
        if (selectedVal.length > 0) {
            saveAddPayors(selectedVal, 'add');
        }
        if (removedVal.length > 0) {
            saveAddPayors(removedVal, 'delete');
        }
    });
}


</script>	





<?php echo $__env->make('employee_add', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>