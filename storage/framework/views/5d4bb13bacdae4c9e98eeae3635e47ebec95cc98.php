<?php $__env->startSection('content'); ?>
    <div class="row" style="display: none;">
        <div class="col-lg-6 col-md-6">
            <h4>User Permission</h4>
        </div>
        <div class="col-lg-6 col-md-6 pull-right" >
            <a class="btn btn-success" href="<?php echo e(URL::to('acl/create')); ?>"> Create New Permission</a>
        </div>
    </div>


<?php $i=0; 
$user_id = 0;
$role_id =0;
if (isset($data['user_id'])){
    $user_id = $data['user_id'];
}
if (isset($data['role_id'])){
    $role_id = $data['role_id'];
}
?>
    <form action="<?php echo e(URL::to('acl/save')); ?>" method="POST">
        <input type="hidden" id="_token" name="_token"
               value="<?php echo csrf_token(); ?>">
        <input type="hidden" id="root_url" name="root_url"
               value="<?php echo e(URL::to('/')); ?>/">
        <input type="hidden" id="user_id" name="user_id"
               value="<?php echo e($data['user_id']); ?>/">
        <input type="hidden" id="role_id" name="role_id"
               value="<?php echo e($data['role_id']); ?>/">
    <?php if ($role_id > 0) { ?>
        <h3>ACL for role_id = <?php echo e($data['role_id']); ?></h3>
    <?php } ?>
    <?php if ($user_id > 0) { ?>
        <h3>ACL for user_id = <?php echo e($data['user_id']); ?></h3>
    <?php } ?>
    <table id="tableACL" class="table table-bordered dataTables_scrollHead">
    </table>


    </table>
    <button type="button" id="update_acl" class="btn btn-success">Update</button>
</form>

<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script type="text/javascript">
$(document).ready(function(){
    fillACL();

    $('#update_acl').click(function () {
        saveAcl();
    });


});

function fillACL(){
    var root_url = $("#hidden_root_url").val();
    var url = root_url + "acl/getlist";
    var _token = $('#_token').val();
    var user_id = $('#user_id').val();
    var role_id = $('#role_id').val();

    $.ajax({
        type: "POST",
        url: url,
        async: false,
        data:{
            "_token" : _token,
            user_id : user_id,
            role_id : role_id,
        },
        success:function(result){
            var res = JSON.parse(result);
            if(res.status == 0){
                alert(res.message);
                return ;
            }
            fillACLSuccess(res.data);
        }
    }); 
}


function fillACLSuccess(data1){
    var dt = [];
    if (data1 == ''){
        dt = [];
    } else {
        $.each(data1,function(i,  v) {
            dt.push([data1[i].main_acl_key,
                data1[i].acl_name,
                data1[i].is_display,
                data1[i].is_create,
                data1[i].is_edit,
                data1[i].is_view,
                data1[i].is_delete,
                data1[i].id]
            );
        });
    }

    $('#tableACL').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "300px", "targets": 1 },
            { "width": "300px", "targets": 2 },
            { "width": "200px", "targets": 3 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Screen", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Display", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var c ='';
                    var id = 'IS_DISPLAY_' + full[0];
                    c += '<input type="checkbox" id="' + id + '" ';
                    c += ' name="' + id + '" ';
                    c += ' value="' + data + '" ';
                    if (data==1){
                        c += ' checked ';
                    }
                    c += ' />';
                    return c;
                }
            },
            {"title": "Create", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var c ='';
                    var id = 'IS_CREATE_' + full[0];
                    c += '<input type="checkbox" id="' + id + '" ';
                    c += ' name="' + id + '" ';
                    c += ' value="' + data + '" ';
                    if (data==1){
                        c += ' checked ';
                    }
                    c += ' />';
                    return c;
                }
            },
            {"title": "Edit", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var c = '';
                    var id = 'IS_EDIT_' + full[0];
                    c += '<input type="checkbox" id="' + id + '" ';
                    c += ' name="' + id + '" ';
                    c += ' value="' + data + '" ';
                    if (data==1){
                        c += ' checked ';
                    }
                    c += ' />';
                    return c;
                }
            },
            {"title": "View", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var c = '';
                    var id = 'IS_VIEW_' + full[0];
                    c += '<input type="checkbox" id="' + id + '" ';
                    c += ' name="' + id + '" ';
                    c += ' value="' + data + '" ';
                    if (data==1){
                        c += ' checked ';
                    }
                    c += ' />';
                    return c;
                }
            },
            {"title": "Delete", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    var c = '';
                    var id = 'IS_DELETE_' + full[0];
                    c += '<input type="checkbox" id="' + id + '" ';
                    c += ' name="' + id + '" ';
                    c += ' value="' + data + '" ';
                    if (data==1){
                        c += ' checked ';
                    }
                    c += ' />';
                    return c;
                }
            }
            /*,
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a data-id="'+data+'" class="session-edit" ><span class="ti-pencil "></span></a> | <a data-id="'+data+'" class="session-delete" ><span class="ti-trash "></span></a>';
                }
            },*/
        ]
    });

}    

function saveAcl(){
    var menu = '';
    $('input[type=checkbox]').each(function () {
         c = (this.checked ? $(this).attr('id') : "");
         if (c != '' ){
            if (c != undefined){
                menu += c + ', ';
            }
         }
    });
    var root_url = $("#hidden_root_url").val();
    var url = root_url + "acl/save";
    var _token = $('#_token').val();
    var user_id = $('#user_id').val();
    var role_id = $('#role_id').val();

    $.ajax({
        type: "POST",
        url: url,
        async: false,
        data:{
            "_token" : _token,
            user_id : user_id,
            role_id : role_id,
            ids : menu,
        },
        success:function(result){
            var res = JSON.parse(result);
            if(res.status == 0){
                alert(res.message);
                return ;
            } else {
                alert(res.message);
                fillACL();
            }
        }
    }); 
}

</script>

<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>