<?php $__env->startSection('content'); ?>

<div class="row">
	<!-- <img src="assets/beat.gif" id="loader" style="position:absolute; left:50%; z-index:99999; top:35%;" /> -->
	<h2 style="color: #251367; margin-left:20px;">Ledger</h2>
	<div class="col-md-12" style="margin-top:20px;">
      <div class="widget box" >
          <div class="widget-content" style="min-height:100px">
            <div id="filter" class="col-md-12">
                <input type="hidden" id="_hdnClaimID"/>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">DOS</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="dos" placeholder="Date Of Service" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">From Date</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="fromDate" placeholder="From Date" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">To Date</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="toDate" placeholder="To Date" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">Client Name</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="clientName" placeholder="Client Name" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">Provider Name</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="providerName" placeholder="Provider Name" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">Insurance Name</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="insName" placeholder="Insurance Name" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">CPT</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="cptVal" placeholder="Procedure Code" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1"></label>
                            </div>
                            <div class="col-7">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-12">
                                <input type="button" class="btn btn-primary pull-right" id="btnGo" value="Get Details" />
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="col-12" style="padding-top:2%">
                </div>
                <div class="col-12" id="bulkDiv">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-2">
                          <div class="form-group row">
                              <div class="col-12">
                                <input type="text" class="form-control required" id="seqCPT" placeholder="Procedure Code" />
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-2">
                          <div class="form-group row">
                              <div class="col-12">
                                <input type="text" class="form-control required" id="seqProvider" placeholder="Provider Name" />
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-2">
                          <div class="form-group row">
                              <div class="col-12">
                                <input type="text" class="form-control required" id="seqMOD" placeholder="Modifier" />
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group row">
                              <div class="col-7">
                                <select id="seqStatus" class="form-control">
                                  <option value="">--Select Status--</option>
                                  <option value="Confirm" selected>Confirmed</option>
                                <option value="Rendered" selected>Ready to Bill</option>
                                <option value="Ready" selected>Ready for EDI</option>
                                </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-2">
                          <div class="form-group row">
                              <div class="col-12">
                                <input type="button" class="btn btn-primary" id="btnFin" value="Update" />
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
  	         <div class="clearfix"></div><br/><br/>
            <div class="widget-content">
                <div class="card-body">
                <div class="bs-example">
                    <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                        <li class="nav-item">
                            <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Still Confirmed</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Ready to Bill</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_3" style="color:#000;" data-toggle="tab" class="nav-link">Ready for EDI</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_4" style="color:#000;" data-toggle="tab" class="nav-link">Balance</a>
                        </li>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="tab_3_1">
                            <table id="test4" class="table table-bordered display"  cellspacing="0" width="100%">
                            </table>
                        </div>
                        <div class="tab-pane fade" id="tab_3_2">
                            <table id="test1" class="table table-bordered display"  cellspacing="0" width="100%">
                            </table>
                        </div>
                        <div class="tab-pane fade" id="tab_3_3">
                            <table id="test2" class="table table-bordered display"  cellspacing="0" width="100%">
                            </table>
                        </div>
                        <div class="tab-pane fade" id="tab_3_4">
                            <table id="test3" class="table table-bordered display"  cellspacing="0" width="100%">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
          	<!-- <div id="tableDiv">
          		<table id="testAll" class="table table-striped  table-hover table-checkable" data-horizontal-width="150%">
				</table>
                  <table id="testPatient" class="table table-striped  table-hover table-checkable" data-horizontal-width="150%">
				</table>
				<table id="testInsurance" class="table table-striped  table-hover table-checkable" data-horizontal-width="150%">
				</table>
			</div> -->
          </div>
      </div>

</div>
<div class="modal fade" id="swapModal" tabindex="-1">
    <div class="modal-dialog" style="width:50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Swap Claim</h4>
            </div>
            <div class="modal-body" style="min-height:80px;">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-8">
                      <div class="form-group row">
                          <div class="col-5">
                              <label for="input-text" class="control-label float-right txt_media1">Move to </label>
                          </div>
                          <div class="col-7">
                              <select class="form-control" id="moveTo">
                                <option value="Confirm" selected>Confirmed</option>
                                <option value="Rendered" selected>Ready to Bill</option>
                                <option value="Ready" selected>Ready for EDI</option>
                            </select>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="form-group row">
                          <div class="col-12">
                              <input type="button" class="btn btn-primary pull-right" id="btnSwap" value="Swap Claim">
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="claimDetails" tabindex="-1">
    <div class="modal-dialog" style="width:75%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Claim Details</h4>
            </div>
            <div class="modal-body" style="min-height:120px;">
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Client Name </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modClientName" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Therapist Name </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modTherapist" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Activity </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modActivity" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">DOS </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modDOS" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">CPT </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modCPT"  />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Modifier </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modMOD"  />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">POS </label>
                    <div class="col-md-6">
                        <select class="form-control" id="modPOS">
                            <option value="12">Home</option>
                            <option value="11">Office</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Units </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modUnits"  />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Charges </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modCharges"  />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Claim # </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modClaimNo" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Status </label>
                    <div class="col-md-6">
                        <select class="form-control" id="modStatus" disabled>
                            <option value=""></option>
                                <option value="Confirm">Confirmed</option>
                                <option value="UnConfirm">UnConfirmed</option>
                                <option value="Noshow">No Show</option>
                                <option value="Cancelled">Cancelled</option>
                                <option value="Hold">Hold</option>
                                <option value="Cancelled by Client">Cancelled by Client</option>
                                <option value="Cancelled by Provider">Cancelled by Provider</option>
                                <option value="Rendered">Rendered</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Move to </label>
                    <div class="col-md-6" id="ddlMove">
                        <select class="form-control">
                            <option value="Ready">Ready To Send</option>
                            <option value="Submit">Submitted</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
          var root_url = $("#hidden_root_url").val();

            $(".navigation li").parent().find('li').removeClass("active");

            $(".ti-menu-alt").click();

            document.getElementById("lblUser").innerHTML = sessionStorage.getItem('userName');

            $("#navv5").addClass("active");

            $('#divDDL').hide();
            getAllClaimsInfo();
            var val = "";
            $(document).on('click','.redPat',function() {
                var temp = $(this).attr('class').split(' ')[0];
                var newTemp = temp.replace('redirectPatient','');
                sessionStorage.setItem("patientId", newTemp);
                window.location.href ="EditPatient.php";
            });
            document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
            $("#bulkDiv").hide();
            var patientList = "";
            //alert(root_url + "patient/list1");
            $.ajax({
                type: "GET",
                url: root_url + "patient/list1",
                data: {
                    practiceID: "1"
                },
                success: function (result) {
                    patientList = result;
                }
            });
            var insurances = "";
            $.ajax({
                type: "POST",
                url: root_url + "insurances/get",
                data: {
                    practiceID: "1"
                },
                success: function (result) {
                    insurances = result;
                }
            });
            var PrimaryPhy = "";
            $.ajax({
                type: "POST",
                url: root_url + "physician/get",
                data: {
                    practiceID: "1"
                },
                success: function (result) {
                    PrimaryPhy = result;
                }
            });

            $("#insName").autocomplete({
                source: insurances,
                autoFocus:true
            });
            $('#fromDate').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'en',
                format: 'm-d-Y',
                timepicker: false,
                //minDate: '-2013/01/02',
                //maxDate: '+2014/12/31',
                formatDate: 'm-d-Y',
                closeOnDateSelect: true
            });
            $("#providerName").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#seqProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#clientName").autocomplete({
                source: patientList,
                autoFocus:true
            });
            $('#toDate').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'en',
                format: 'm-d-Y',
                timepicker: false,
                //minDate: '-2013/01/02',
                //maxDate: '+2014/12/31',
                formatDate: 'm-d-Y',
                closeOnDateSelect: true
            });
            $('#dos').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'en',
                format: 'm-d-Y',
                timepicker: false,
                //minDate: '-2013/01/02',
                //maxDate: '+2014/12/31',
                formatDate: 'm-d-Y',
                closeOnDateSelect: true
            });

            $("#btnFin").click(function(){
                var searchIDs = $('input:checked').map(function(){
                    return $(this).attr('id');
                });
                for(var i=0;i<searchIDs.length;i++){
                    var temp = searchIDs[i];
                    var newTemp = temp.replace('chk','');
                    document.getElementById('_hdnClaimID').value = newTemp;
                    var tempCPT = document.getElementById("seqCPT").value;
                    var temp_Mod = document.getElementById("seqMOD").value;
                    var tempMod = temp_Mod.split(" ");
                    var tempMod1 = tempMod[0];
                    var tempMod2 = tempMod[1];
                    var tempMod3 = tempMod[2];
                    var tempMod4 = tempMod[3];
                    var tempProvider = document.getElementById("seqProvider").value;
                    var tempStatus = document.getElementById("seqStatus").value;
                    var n1=tempProvider.indexOf("-");
                    tempProvider = tempProvider.substr(0,n1-1);
                    $.ajax({
                        type: "POST",
                        url: root_url+"claims/bulkupdateledger",
                        asyn:false,
                        data:{
                            claimID : newTemp,
                            tempCPT : tempCPT,
                            tempMod1 : tempMod1,
                            tempMod2 : tempMod2,
                            tempMod3 : tempMod3,
                            tempMod4 : tempMod4,
                            tempProvider : tempProvider,
                            tempStatus : tempStatus
                        },success:function(result){
                            window.location.reload();
                        }
                    });
                }
            });


            $(document).on('click','.xchange',function() {
                var temp = $(this).attr('class').split(' ')[0];
                var newTemp = temp.replace('xchange','');
                document.getElementById('_hdnClaimID').value = newTemp;
                $("#swapModal").modal("show");
            });
            $("#btnSwap").click(function(){
                var Status = document.getElementById("moveTo").value;
                var newTemp = document.getElementById('_hdnClaimID').value;
                $.ajax({
                    type: "POST",
                    url: root_url+"claims/updatestatus",
                    data:{
                        claimID : newTemp,
                        status : Status
                    },success:function(result){
                        $("#swapModal").modal("hide");
                        alertify.success("Successfully Updated");
                        window.location.reload();
                    }
                });
            })
            $(document).on('click','.editLedger',function() {
                var temp = $(this).attr('class').split(' ')[0];
                var newTemp = temp.replace('edit','');
                document.getElementById('_hdnClaimID').value = newTemp;
                var txtCPT = 'txtCPT'+newTemp;
                var txtMod = 'txtMod'+newTemp;
                var txtPOS = 'txtPOS'+newTemp;
                var txtUnits = 'txtUnits'+newTemp;
                var txtCharges = 'txtCharges'+newTemp;
                var tempCPT = document.getElementById(txtCPT).value;
                var temp_Mod = document.getElementById(txtMod).value;
                var tempMod = temp_Mod.split(" ");
                var tempMod1 = tempMod[0];
                var tempMod2 = tempMod[1];
                var tempMod3 = tempMod[2];
                var tempMod4 = tempMod[3];
                var tempPOS = document.getElementById(txtPOS).value;
                var tempUnits = document.getElementById(txtUnits).value;
                var tempCharges = document.getElementById(txtCharges).value;
                $.ajax({
                    type: "POST",
                    url: root_url+"claims/updateledger",
                    data:{
                        claimID : newTemp,
                        tempCPT : tempCPT,
                        tempMod1 : tempMod1,
                        tempMod2 : tempMod2,
                        tempMod3 : tempMod3,
                        tempMod4 : tempMod4,
                        tempPOS : tempPOS,
                        tempUnits : tempUnits,
                        tempCharges : tempCharges
                    },success:function(result){
                    }
                });
            });
            $("#btnGo").click(function(){
                var clientName = $("#clientName").val();
                var nos=clientName.indexOf("- ");
                clientName = clientName.substr(0,nos-1);
                var insName = $("#insName").val();
                var providerNa = $("#providerName").val();
                var n=providerNa.indexOf("-");
                var providerName = providerNa.substr(0,n-1);
                var n1=insName.indexOf("-");
                insName = insName.substr(0,n1-1);
                var dos = convertDate($("#dos").val());
                var cpt = $("#cptVal").val();
                var fromDate = convertDate($("#fromDate").val());
                var toDate = convertDate($("#toDate").val());
                $.ajax({
                    type: "POST",
                    url: root_url+"claims/getledgerinfo",
                    data:{
                        clientName : clientName,
                        providerName : providerName,
                        insName : insName,
                        cpt : cpt,
                        dos : dos,
                        fromDate : fromDate,
                        toDate : toDate,
                    },success:function(result){
                        var data1 = JSON.parse(result);
                        var dt = [];
                        $.each(data1,function(i,v) {
                            dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
                        });
                        $('#test1').DataTable({
                            "aaSorting": [[ 0, "desc" ]],
                            "destroy": true,
                            "data" : dt,

                            columns: [
                                {"title": "Claim ID","visible" : false},
                                {"title": "",
                                    "render": function ( data, type, full, meta ) {
                                        return '<input type="checkbox" class="chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                                    }
                                },
                                {"title": "Client Name",
                                    "render": function ( data, type, full, meta ) {
                                        return data;
                                    }
                                },
                                {"title": "Client Provider"},
                                {"title": "Activity"},
                                //          {"title": "Bill Date",
                                //           "render": function ( data, type, full, meta ) {
                                //       return changeDateFormat(data);
                                //     }
                                // },
                                {"title": "DOS",
                                    "render": function ( data, type, full, meta ) {
                                        return changeDateFormat(data);
                                    }
                                },
                                {"title": "CPT      ",
                                    "render": function ( data, type, full, meta ) {
                                        return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" style="padding-right:2px;" id="txtCPT'+full[0]+'" value="'+data+'" />';
                                    }
                                },
                                {"title": "Modifier",
                                    "render": function ( data, type, full, meta ) {
                                        return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" style="padding-right:2px;" id="txtMod'+full[0]+'" value="'+data+'" />';
                                    }
                                },
                                {"title": "POS",
                                    "render": function ( data, type, full, meta ) {
                                        return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" style="padding-right:2px;" id="txtPOS'+full[0]+'" value="'+data+'" />';
                                    }
                                },
                                {"title": "Units",
                                    "render": function ( data, type, full, meta ) {
                                        return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" style="padding-right:2px;" id="txtUnits'+full[0]+'" value="'+data+'" />';
                                    }
                                },
                                {"title": "Charges",
                                    "render": function ( data, type, full, meta ) {
                                        return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" style="padding-right:2px;" id="txtCharges'+full[0]+'" value="'+data+'" />';
                                    }
                                },
                                {"title": "Claim #"},
                                {"title": "Status"},
                                {"title": "Actions",
                                    "render": function ( data, type, full, meta ) {
                                        return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><span class="ti-arrows-horizontal" style="font-size:12px;"></span> </a>';
                                    }
                                }
                                // {"title": "Patient Name",
                                //     "render": function ( data, type, full, meta ) {
                                //         var tempData=data.indexOf("-");
                                //         var truncID = data.substr(0,tempData);
                                //         var tempName = data.replace(truncID,"");
                                //         var tempDataName = tempName.replace("- ","");
                                //       return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
                                //     }
                                // }
                            ]
                        });
                    }
                });
            });

            $(document).on('click','.chk',function() {
                if ($("input:checkbox:checked").length > 1) {
                    $("#bulkDiv").show();
                }
                else{
                    $("#bulkDiv").hide();
                }
            });

            $(document).on('click','.chkall1',function() {
                if($('.chkall1').is(':checked') == true){
                    $(".chkall11").prop("checked",true);
                    $("#bulkDiv").show();
                }
                else{
                    $(".chkall11").prop("checked",false);
                    $("#bulkDiv").hide();
                }
            });

            $(document).on('click','.chkall2',function() {
                if($('.chkall2').is(':checked') == true){
                    $(".chkall22").prop("checked",true);
                    $("#bulkDiv").show();
                }
                else{
                    $(".chkall22").prop("checked",false);
                    $("#bulkDiv").hide();
                }
            });


            $(document).on('click','.chkall3',function() {
                if($('.chkall3').is(':checked') == true){
                    $(".chkall33").prop("checked",true);
                    $("#bulkDiv").show();
                }
                else{
                    $(".chkall33").prop("checked",false);
                    $("#bulkDiv").hide();
                }
            });


            $(document).on('click','.chkall4',function() {
                if($('.chkall4').is(':checked') == true){
                    $(".chkall44").prop("checked",true);
                    $("#bulkDiv").show();
                }
                else{
                    $(".chkall44").prop("checked",false);
                    $("#bulkDiv").hide();
                }
            });

        });

        function Dec2(num) {
            num = String(num);
            if(num.indexOf('.') !== -1) {
                var numarr = num.split(".");
                if (numarr.length == 1) {
                    return Number(num);
                }
                else {
                    return Number(numarr[0]+"."+numarr[1].charAt(0)+numarr[1].charAt(1));
                }
            }
            else {
                return Number(num);
            }
        }

        function getAllClaimsInfo(){
          var root_url = $("#hidden_root_url").val();
            var tot = 0;
            var AllPattot = 0;
            $('#tableAll').show();
            $('#tablePat').hide();
            $('#tableIns').hide();
            // $.get("getZeroToThirtyAllByIns.php",function(data1){
            //  document.getElementById('spanZero').innerHTML = '$ '+data1+'.00';
            //  tot += parseInt(data1);
            // });

            // $.get("getThirtyToSixtyAllByIns.php",function(data1){
            //  document.getElementById('spanThirty').innerHTML = '$ '+data1+'.00';
            //  tot += parseInt(data1);
            // });

            // $.get("getSixtyToNinetyAllByIns.php",function(data1){
            //  document.getElementById('spanSixty').innerHTML = '$ '+data1+'.00';
            //  tot += parseInt(data1);
            // });

            // $.get("getNinetyToOnetwentyAllByIns.php",function(data1){
            //  document.getElementById('spanNinety').innerHTML = '$ '+data1+'.00';
            //  tot += parseInt(data1);
            // });

            // $.get("get120plusAllByIns.php",function(data1){
            //  document.getElementById('span120').innerHTML = '$ '+data1+'.00';
            //  tot += parseInt(data1);
            // });

            // $.get("getZeroToThirtyAllByPat.php",function(data1){
            //  document.getElementById('spanAllPatZero').innerHTML = '$ '+data1+'.00';
            //  AllPattot += parseInt(data1);
            // });

            // $.get("getThirtyToSixtyAllByPat.php",function(data1){
            //  document.getElementById('spanAllPatThirty').innerHTML = '$ '+data1+'.00';
            //  AllPattot += parseInt(data1);
            // });

            // $.get("getSixtyToNinetyAllByPat.php",function(data1){
            //  document.getElementById('spanAllPatSixty').innerHTML = '$ '+data1+'.00';
            //  AllPattot += parseInt(data1);
            // });

            // $.get("getNinetyToOnetwentyAllByPat.php",function(data1){
            //  document.getElementById('spanAllPatNinety').innerHTML = '$ '+data1+'.00';
            //  AllPattot += parseInt(data1);
            // });

            // $.get("get120plusAllByPat.php",function(data1){
            //  document.getElementById('spanAllPat120').innerHTML = '$ '+data1+'.00';
            //  AllPattot += parseInt(data1);
            // });

            var allclaims_url = root_url + 'claims/getall';

            $.get(allclaims_url,function(data1){
                $('#loader').show();
                var res = JSON.parse(data1);
                if (res.status ==0){
                  var dt = [];
                  $('#test1').DataTable({
                      "aaSorting": [[ 0, "desc" ]],
                      "destroy": true,
                      "data" : dt,

                      columns: [
                          {"title": "Claim ID","visible" : false},
                          {"title": "<input type='checkbox' class='chkall4'/>",
                              "render": function ( data, type, full, meta ) {
                                  return '<input type="checkbox" class="chkall44 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                              }
                          },
                          {"title": "Client Name",
                              "render": function ( data, type, full, meta ) {
                                  return data;
                              }
                          },
                          {"title": "Client Provider"},
                          {"title": "Rendering Provider"},
                          {"title": "Activity"},
                          //          {"title": "Bill Date",
                          //           "render": function ( data, type, full, meta ) {
                          //       return changeDateFormat(data);
                          //     }
                          // },
                          {"title": "DOS",
                              "render": function ( data, type, full, meta ) {
                                  return changeDateFormat(data);
                              }
                          },
                          {"title": "CPT"
                          },
                          {"title": "Modifier"
                          },
                          {"title": "POS"
                          },
                          {"title": "Units"
                          },
                          {"title": "Charges"
                          },
                          {"title": "Claim #"},
                          {"title": "Status"},
                          {"title": "Actions",
                              "render": function ( data, type, full, meta ) {
                                  return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>';
                              }
                          }
                      ]
                  });
                  $('#loader').hide();
                  return;
                }
                var data1 = res.data;
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].claimID,data1[i].claimID,data1[i].full_name,data1[i].phyName,data1[i].defaultRenderPhy,data1[i].activityName,data1[i].from_date,data1[i].proced,data1[i].mod1,data1[i].location,data1[i].units,data1[i].total,data1[i].claim_number,data1[i].claimStatus,data1[i].claimID]);
                });
                $('#test1').DataTable({
                    "aaSorting": [[ 0, "desc" ]],
                    "destroy": true,
                    "data" : dt,
                    columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "<input type='checkbox' class='chkall1'/> ",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="checkbox" class="chkall11 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Rendering Provider"},
                        {"title": "Activity"},
                        //          {"title": "Bill Date",
                        //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT      ",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" style="padding-right:2px;" id="txtCPT'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Modifier",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" style="padding-right:2px;" id="txtMod'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "POS",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" style="padding-right:2px;" id="txtPOS'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Units",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" style="padding-right:2px;" id="txtUnits'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Charges",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" style="padding-right:2px;" id="txtCharges'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><span class="ti-arrows-horizontal" style="font-size:12px;"></span> </a>';
                            }
                        }
                    ]
                });
                $('#loader').hide();
                //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
                //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
                $('#testAll').removeAttr('style');
            });

            var confClaims_url = root_url+'claims/getconfirmed';
            //alert(confClaims_url);
            $.get(confClaims_url,function(data1){
                $('#loader').show();
                var res = JSON.parse(data1);
                if (res.status ==0){
                  var dt = [];
                  $('#test4').DataTable({
                      "aaSorting": [[ 0, "desc" ]],
                      "destroy": true,
                      "data" : dt,

                      columns: [
                          {"title": "Claim ID","visible" : false},
                          {"title": "<input type='checkbox' class='chkall4'/>",
                              "render": function ( data, type, full, meta ) {
                                  return '<input type="checkbox" class="chkall44 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                              }
                          },
                          {"title": "Client Name",
                              "render": function ( data, type, full, meta ) {
                                  return data;
                              }
                          },
                          {"title": "Client Provider"},
                          {"title": "Rendering Provider"},
                          {"title": "Activity"},
                          //          {"title": "Bill Date",
                          //           "render": function ( data, type, full, meta ) {
                          //       return changeDateFormat(data);
                          //     }
                          // },
                          {"title": "DOS",
                              "render": function ( data, type, full, meta ) {
                                  return changeDateFormat(data);
                              }
                          },
                          {"title": "CPT"
                          },
                          {"title": "Modifier"
                          },
                          {"title": "POS"
                          },
                          {"title": "Units"
                          },
                          {"title": "Charges"
                          },
                          {"title": "Claim #"},
                          {"title": "Status"},
                          {"title": "Actions",
                              "render": function ( data, type, full, meta ) {
                                  return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>';
                              }
                          }
                      ]
                  });
                  $('#loader').hide();
                  return;
                }
                var data1 = res.data;
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].claimID,data1[i].claimID,data1[i].full_name,data1[i].phyName,data1[i].defaultRenderPhy,data1[i].activityName,data1[i].from_date,data1[i].proced,data1[i].mod1,data1[i].location,data1[i].units,data1[i].total,data1[i].claim_number,data1[i].claimStatus,data1[i].claimID]);
                });
                $('#test4').DataTable({
                    "aaSorting": [[ 0, "desc" ]],
                    "destroy": true,
                    "data" : dt,

                    columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "<input type='checkbox' class='chkall2'/>",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="checkbox" class="chkall22 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Rendering Provider"},
                        {"title": "Activity"},
                        //          {"title": "Bill Date",
                        //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT      ",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" style="padding-right:1px; width: 50px;" id="txtCPT'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Modifier",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" style="padding-right: 1px;width: 70px;" id="txtMod'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "POS",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" style="padding-right: 1px;width: 40px;" id="txtPOS'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Units",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" style="padding-right: 1px; width: 40px;" id="txtUnits'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Charges",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" style="padding-right: 1px;width: 40px;" id="txtCharges'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><span class="ti-arrows-horizontal" style="font-size:12px;"></span> </a>';
                            }
                        }
                    ]
                });
                $('#loader').hide();
                //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
                //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
                $('#testAll').removeAttr('style');
            });

            var readyClaims_url = root_url+'claims/getready';
            $.get(readyClaims_url,function(data1){
                $('#loader').show();
                var res = JSON.parse(data1);
                if (res.status ==0){
                  var dt = [];
                  $('#test2').DataTable({
                      "aaSorting": [[ 0, "desc" ]],
                      "destroy": true,
                      "data" : dt,

                      columns: [
                          {"title": "Claim ID","visible" : false},
                          {"title": "<input type='checkbox' class='chkall4'/>",
                              "render": function ( data, type, full, meta ) {
                                  return '<input type="checkbox" class="chkall44 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                              }
                          },
                          {"title": "Client Name",
                              "render": function ( data, type, full, meta ) {
                                  return data;
                              }
                          },
                          {"title": "Client Provider"},
                          {"title": "Rendering Provider"},
                          {"title": "Activity"},
                          //          {"title": "Bill Date",
                          //           "render": function ( data, type, full, meta ) {
                          //       return changeDateFormat(data);
                          //     }
                          // },
                          {"title": "DOS",
                              "render": function ( data, type, full, meta ) {
                                  return changeDateFormat(data);
                              }
                          },
                          {"title": "CPT"
                          },
                          {"title": "Modifier"
                          },
                          {"title": "POS"
                          },
                          {"title": "Units"
                          },
                          {"title": "Charges"
                          },
                          {"title": "Claim #"},
                          {"title": "Status"},
                          {"title": "Actions",
                              "render": function ( data, type, full, meta ) {
                                  return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>';
                              }
                          }
                      ]
                  });
                  $('#loader').hide();
                  return;
                }
                var data1 = res.data;
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].claimID,data1[i].claimID,data1[i].full_name,data1[i].phyName,data1[i].defaultRenderPhy,data1[i].activityName,data1[i].from_date,data1[i].proced,data1[i].mod1,data1[i].location,data1[i].units,data1[i].total,data1[i].claim_number,data1[i].claimStatus,data1[i].claimID]);
                });
                $('#test2').DataTable({
                    "aaSorting": [[ 0, "desc" ]],
                    "destroy": true,
                    "data" : dt,

                    columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "<input type='checkbox' class='chkall3'/>",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="checkbox" class="chkall33 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Rendering Provider"},
                        {"title": "Activity"},
                        //          {"title": "Bill Date",
                        //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT"
                        },
                        {"title": "Modifier"
                        },
                        {"title": "POS"
                        },
                        {"title": "Units"
                        },
                        {"title": "Charges"
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><span class="ti-arrows-horizontal" style="font-size:12px;"></span> </a>';
                            }
                        }
                    ]
                });
                $('#loader').hide();
                //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
                //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
                $('#testAll').removeAttr('style');
            });
            //thats what am gonna do
            var submitClaims_url = root_url+'claims/getsubmit';
            $.get(submitClaims_url,function(data1){
                $('#loader').show();
                var res = JSON.parse(data1);
                if (res.status ==0){
                  var dt = [];
                  $('#test3').DataTable({
                      "aaSorting": [[ 0, "desc" ]],
                      "destroy": true,
                      "data" : dt,

                      columns: [
                          {"title": "Claim ID","visible" : false},
                          {"title": "<input type='checkbox' class='chkall4'/>",
                              "render": function ( data, type, full, meta ) {
                                  return '<input type="checkbox" class="chkall44 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                              }
                          },
                          {"title": "Client Name",
                              "render": function ( data, type, full, meta ) {
                                  return data;
                              }
                          },
                          {"title": "Client Provider"},
                          {"title": "Rendering Provider"},
                          {"title": "Activity"},
                          //          {"title": "Bill Date",
                          //           "render": function ( data, type, full, meta ) {
                          //       return changeDateFormat(data);
                          //     }
                          // },
                          {"title": "DOS",
                              "render": function ( data, type, full, meta ) {
                                  return changeDateFormat(data);
                              }
                          },
                          {"title": "CPT"
                          },
                          {"title": "Modifier"
                          },
                          {"title": "POS"
                          },
                          {"title": "Units"
                          },
                          {"title": "Charges"
                          },
                          {"title": "Claim #"},
                          {"title": "Status"},
                          {"title": "Actions",
                              "render": function ( data, type, full, meta ) {
                                  return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>';
                              }
                          }
                      ]
                  });
                  $('#loader').hide();
                  return;
                }
                var data1 = res.data;
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].claimID,data1[i].claimID,data1[i].full_name,data1[i].phyName,data1[i].defaultRenderPhy,data1[i].activityName,data1[i].from_date,data1[i].proced,data1[i].mod1,data1[i].location,data1[i].units,data1[i].total,data1[i].claim_number,data1[i].claimStatus,data1[i].claimID]);
                });
                $('#test3').DataTable({
                    "aaSorting": [[ 0, "desc" ]],
                    "destroy": true,
                    "data" : dt,

                    columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "<input type='checkbox' class='chkall4'/>",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="checkbox" class="chkall44 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Rendering Provider"},
                        {"title": "Activity"},
                        //          {"title": "Bill Date",
                        //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT"
                        },
                        {"title": "Modifier"
                        },
                        {"title": "POS"
                        },
                        {"title": "Units"
                        },
                        {"title": "Charges"
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>';
                            }
                        }
                    ]
                });
                $('#loader').hide();
                //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
                //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
                $('#testAll').removeAttr('style');
            });

        }
        function changeDateFormat(inputDate){  // expects Y-m-d
            var splitDate = (inputDate.split(" ")[0]).split('-');

            if(splitDate.count == 0){
                return null;
            }

            var year = splitDate[0];
            var month = splitDate[1];
            var day = splitDate[2];

            var consolidated = month + '-' + day + '-' + year;
            var fake = year+ '-' + month+ '-' + day;
            var timing = inputDate.replace(fake,"")
            return consolidated+ ' ' + timing ;
        }
        var convertDate = function(usDate) {
            if(usDate !=""){
                var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
                return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
            }
        }
    </script>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>