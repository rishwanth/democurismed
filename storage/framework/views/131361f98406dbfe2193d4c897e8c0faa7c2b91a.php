<?php $__env->startSection('content'); ?>
<div class="row">
    <h2 style="color: #251367; margin-left:20px;">Practice Info</h2>
    <div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">

            <div class="card-body">
                <div class="bs-example">
                    <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                        <li class="nav-item">
                            <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Setup Facility</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Set Up Zones</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_3" style="color:#000;" data-toggle="tab" class="nav-link">Setup Holiday/PTO</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_4" style="color:#000;" data-toggle="tab" class="nav-link">Setup Activities Subtypes</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_5" style="color:#000;" data-toggle="tab" class="nav-link"> Create Session Rules </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_6" style="color:#000;" data-toggle="tab" class="nav-link"> Setup Employee </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_7" style="color:#000;" data-toggle="tab" class="nav-link"> Add/Edit Users </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_8" style="color:#000;" data-toggle="tab" class="nav-link"> Manage Payroll </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_9" style="color:#000;" data-toggle="tab" class="nav-link"> HIPAA </a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="tab_3_1">
                            <div id="accordion" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                               Facility Name &amp; Location
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                                            <div class="row">

                                                <div class="col-sm-3">
                                                    <div class="form-group row" style="margin-bottom:1rem">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Facility Name" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="Address" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Address 2" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Zipcode" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group row" style="margin-bottom:1rem">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="City" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="State" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Phone 1" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Fax" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group row" style="margin-bottom:1rem">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Phone 2" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="Email" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="EIN" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="NPI" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group row" style="margin-bottom:1rem">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Taxonomy Code" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="Contact Person with this Practice" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Start Time" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="End Time" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- <div class="col-sm-3">
                                                    <div class="form-group row" style="margin-bottom:1rem">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Service Area Miles" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="User Default Password" />
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <label class="checkbox-inline icheckbox">
                                                                <input type="checkbox" class="uniform" id="homeChk" value=""> Is Default Facility
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <textarea class="form-control" rows="5" cols="5"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                               Add Payors to the Facility
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                                            <select multiple="multiple" size="10" name="duallistbox_demo1[]">
                                              <option value="option1">AETNA</option>
                                              <option value="option2">MEDICARE</option>
                                              <option value="option3" selected="selected">MEDICAID</option>
                                              <option value="option4">CIGNA</option>
                                              <option value="option5">TRICARE</option>
                                              <option value="option6" selected="selected">UHC</option>
                                              <option value="option7">Optum</option>
                                              <option value="option8">Healthnet</option>
                                              <option value="option9">Anthem</option>
                                              <option value="option0">BCBS</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="headingfour">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                               Payor Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapsefour" class="collapse" role="tabpanel" aria-labelledby="headingfour" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                                            <table class="table table-bordered" style="width:100%;">
                                                <thead style="text-align:center">
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;"><input type="checkbox" /></th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Payor Name</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Payor Type</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Day Club by Provider</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Is capitated</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">CMS1500 31</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">CMS1500 32a</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">CMS1500 32b</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">CMS1500 33a</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">CMS1500 33b</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Active</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: center;"><input type="checkbox" /></td>
                                                        <td style="text-align: center;">Medicaid</td>
                                                        <td style="text-align: center;"><select class="form-control"><option>Electronic - Claim based</option><option>Office Ally SFTP</option><option>Non Claim based</option><option>Send to Amvik</option></select></td>
                                                        <td style="text-align: center;"><input type="checkbox" /></td>
                                                        <td style="text-align: center;"><input type="checkbox" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="checkbox" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;"><input type="checkbox" /></td>
                                                        <td style="text-align: center;">UHC</td>
                                                        <td style="text-align: center;"><select class="form-control"><option>Electronic - Claim based</option><option>Office Ally SFTP</option><option>Non Claim based</option><option>Send to Amvik</option></select></td>
                                                        <td style="text-align: center;"><input type="checkbox" /></td>
                                                        <td style="text-align: center;"><input type="checkbox" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="text" style="width:50px;" class="form-control" /></td>
                                                        <td style="text-align: center;"><input type="checkbox" /></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="headingfive">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                               Payroll Integration
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapsefive" class="collapse" role="tabpanel" aria-labelledby="headingfive" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group row" style="margin-bottom:1rem">
                                                        <div class="col-12">
                                                            <select class="form-control">
                                                                <option value="">--Payroll Process--</option>
                                                                <option value="Manual">Manual</option>
                                                                <option value="ADP">ADP</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9">
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="ADP Company Code" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Mileage Rate Code" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="headingsix">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                               Company Logo Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapsesix" class="collapse" role="tabpanel" aria-labelledby="headingsix" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                                            <input type="file" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_2">
                            <table class="table table-bordered" style="width:100%;">
                                <thead style="text-align:center">
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Zone ID</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Name</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Description</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Action</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: center;">111</td>
                                        <td style="text-align: center;">East</td>
                                        <td style="text-align: center;">East</td>
                                        <td style="text-align: center;"><i class="fa fa-close" style="font-size:22px"></i></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">222</td>
                                        <td style="text-align: center;">Main</td>
                                        <td style="text-align: center;">Main</td>
                                        <td style="text-align: center;"><i class="fa fa-close" style="font-size:22px"></i></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/>
                            <input type="button" id="btnAddZone" class="btn btn-primary" value="Add Zone"/>
                        </div>
                        <div class="tab-pane fade" id="tab_3_3">
                            <table class="table table-bordered" style="width:100%;">
                                <thead style="text-align:center">
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">ID</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Date of Holiday</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Description</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Action</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: center;">822</td>
                                        <td style="text-align: center;">September 7, 2015</td>
                                        <td style="text-align: center;">Labor Day</td>
                                        <td style="text-align: center;"><i class="fa fa-close" style="font-size:22px"></i></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">823</td>
                                        <td style="text-align: center;">November 26, 2015</td>
                                        <td style="text-align: center;">Thanksgiving Day</td>
                                        <td style="text-align: center;"><i class="fa fa-close" style="font-size:22px"></i></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/><br/><br/>
                            <input type="button" id="btnAddHoliday" class="btn btn-primary" value="Add Holiday"/>
                        </div>
                        <div class="tab-pane fade" id="tab_3_4">

                            <div id="accordion1" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading7">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                               Service/Activity Sub Types
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse7" class="collapse show" role="tabpanel" aria-labelledby="heading7" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-12">Type </label>
                                                <div class="col-md-12">
                                                    <select class="form-control">
                                                      <option value="Billable">Billable</option>
                                                      <option value="Non-billable">Non-billable</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-12">Activity </label>
                                                <div class="col-md-12">
                                                    <select class="form-control">
                                                        <option selected="selected" value="--Select One--">--Select One--</option>
                                                        <option value="40">Drive Time (Unbillable)</option>
                                                        <option value="41">Regular Time </option>
                                                        <option value="43">Training &amp; Admin</option>
                                                        <option value="44">Fill-In</option>
                                                        <option value="45">Other</option>
                                                        <option value="46">Public Holiday</option>
                                                        <option value="47">Paid Time Off</option>
                                                        <option value="50">Unpaid</option>
                                                        <option value="53">Preschool</option>
                                                        <option value="61">Cross Training</option>
                                                        <option value="62">Clinical Team Meeting </option>
                                                        <option value="63">General analyst training </option>
                                                        <option value="75">Sick Time Off</option>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading8">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                               ADP Codes
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse8" class="collapse show" role="tabpanel" aria-labelledby="heading8" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body" style="max-height:400px; overflow:scroll">
                                            <table cellspacing="0" rules="all" class="table" border="1" id="AdpCodes_grdADPCodes" style="width:90%;border-collapse:collapse;">
                                                <tr>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Select</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Activity</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">SubActivity</th>
                                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">PayCode</th>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="0"><input id="0" type="checkbox" name="02" /></span>
                                                                        </td><td>ABA CON</td><td>&nbsp;</td><td>
                                                                            <input name="02$txtHour3Code" type="text" id="0" maxlength="20" onchange="SelectRow(0)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="1"><input id="1" type="checkbox" name="03" /></span>
                                                                        </td><td>Assessment</td><td>&nbsp;</td><td>
                                                                            <input name="03$txtHour3Code" type="text" id="1" maxlength="20" onchange="SelectRow(1)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="2"><input id="2" type="checkbox" name="04" /></span>
                                                                        </td><td>Direct Behavior Therapy</td><td>&nbsp;</td><td>
                                                                            <input name="04$txtHour3Code" type="text" id="2" maxlength="20" onchange="SelectRow(2)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="3"><input id="3" type="checkbox" name="05" /></span>
                                                                        </td><td>Drive Time (Unbillable)</td><td>&nbsp;</td><td>
                                                                            <input name="05$txtHour3Code" type="text" id="3" maxlength="20" onchange="SelectRow(3)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="4"><input id="4" type="checkbox" name="06" /></span>
                                                                        </td><td>Fill-In</td><td>&nbsp;</td><td>
                                                                            <input name="06$txtHour3Code" type="text" id="4" maxlength="20" onchange="SelectRow(4)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="5"><input id="5" type="checkbox" name="07" /></span>
                                                                        </td><td>OA</td><td>&nbsp;</td><td>
                                                                            <input name="07$txtHour3Code" type="text" id="5" maxlength="20" onchange="SelectRow(5)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="6"><input id="6" type="checkbox" name="08" /></span>
                                                                        </td><td>Other</td><td>&nbsp;</td><td>
                                                                            <input name="08$txtHour3Code" type="text" id="6" maxlength="20" onchange="SelectRow(6)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="7"><input id="7" type="checkbox" name="09" /></span>
                                                                        </td><td>Paid Time Off</td><td>&nbsp;</td><td>
                                                                            <input name="09$txtHour3Code" type="text" id="7" maxlength="20" onchange="SelectRow(7)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="8"><input id="8" type="checkbox" name="10" /></span>
                                                                        </td><td>Public Holiday</td><td>&nbsp;</td><td>
                                                                            <input name="10$txtHour3Code" type="text" id="8" maxlength="20" onchange="SelectRow(8)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="9"><input id="9" type="checkbox" name="11" /></span>
                                                                        </td><td>Regular Time </td><td>&nbsp;</td><td>
                                                                            <input name="11$txtHour3Code" type="text" id="9" maxlength="20" onchange="SelectRow(9)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="10"><input id="10" type="checkbox" name="12" /></span>
                                                                        </td><td>Report Writing</td><td>&nbsp;</td><td>
                                                                            <input name="12$txtHour3Code" type="text" id="10" maxlength="20" onchange="SelectRow(10)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="11"><input id="11" type="checkbox" name="13" /></span>
                                                                        </td><td>Supervision</td><td>&nbsp;</td><td>
                                                                            <input name="13$txtHour3Code" type="text" id="11" maxlength="20" onchange="SelectRow(11)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="12"><input id="12" type="checkbox" name="14" /></span>
                                                                        </td><td>Team Meeting</td><td>&nbsp;</td><td>
                                                                            <input name="14$txtHour3Code" type="text" id="12" maxlength="20" onchange="SelectRow(12)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="13"><input id="13" type="checkbox" name="15" /></span>
                                                                        </td><td>Training &amp; Admin</td><td>&nbsp;</td><td>
                                                                            <input name="15$txtHour3Code" type="text" id="13" maxlength="20" onchange="SelectRow(13)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="14"><input id="14" type="checkbox" name="16" /></span>
                                                                        </td><td>Travel Time (Billable)</td><td>&nbsp;</td><td>
                                                                            <input name="16$txtHour3Code" type="text" id="14" maxlength="20" onchange="SelectRow(14)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="15"><input id="15" type="checkbox" name="17" /></span>
                                                                        </td><td>Unpaid</td><td>&nbsp;</td><td>
                                                                            <input name="17$txtHour3Code" type="text" id="15" maxlength="20" onchange="SelectRow(15)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="16"><input id="16" type="checkbox" name="18" /></span>
                                                                        </td><td>Updated Behavior Plan</td><td>&nbsp;</td><td>
                                                                            <input name="18$txtHour3Code" type="text" id="16" maxlength="20" onchange="SelectRow(16)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="17"><input id="17" type="checkbox" name="19" /></span>
                                                                        </td><td>Direct Behavior Therapy</td><td>BCBA</td><td>
                                                                            <input name="19$txtHour3Code" type="text" id="17" maxlength="20" onchange="SelectRow(17)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="18"><input id="18" type="checkbox" name="20" /></span>
                                                                        </td><td>Supervision</td><td>BCBA</td><td>
                                                                            <input name="20$txtHour3Code" type="text" id="18" maxlength="20" onchange="SelectRow(18)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="19"><input id="19" type="checkbox" name="21" /></span>
                                                                        </td><td>ABA CON</td><td>Family Behavior Treatment</td><td>
                                                                            <input name="21$txtHour3Code" type="text" id="19" maxlength="20" onchange="SelectRow(19)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="20"><input id="20" type="checkbox" name="22" /></span>
                                                                        </td><td>OA</td><td>Group Behavior Treatment By BCBA</td><td>
                                                                            <input name="22$txtHour3Code" type="text" id="20" maxlength="20" onchange="SelectRow(20)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="21"><input id="21" type="checkbox" name="23" /></span>
                                                                        </td><td>OA</td><td>Group Behavior Treatment By PARA</td><td>
                                                                            <input name="23$txtHour3Code" type="text" id="21" maxlength="20" onchange="SelectRow(21)" />
                                                                        </td>
                                                </tr><tr>
                                                    <td>
                                                                            <span data-rowid="22"><input id="22" type="checkbox" name="24" /></span>
                                                                        </td><td>Direct Behavior Therapy</td><td>PARA</td><td>
                                                                            <input name="24$txtHour3Code" type="text" id="22" maxlength="20" onchange="SelectRow(22)" />
                                                                        </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_5">
                            <table class="table" cellspacing="0" border="1" style="width:100%;border-collapse:collapse;">
                                <tr>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Rule</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Description/Message</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff; width:150px">Run Rule</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff; width:200px">Prevent Session Creation</th>
                                </tr><tr>
                                    <td>Auth Start Date</td><td>Session before authorization start date. </td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Auth End Date</td><td>Session after authorization end date. </td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Auth Frequency</td><td>Session is out of authorization frequency.</td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Therapist on Leave</td><td>Therapist is on leave on this day.  </td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Therapist double booked</td><td>Therapist already has session scheduled for this time.  </td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Payor Block Day</td><td>Payor has blocked this day for treatment</td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Same Activity Same Day</td><td>This session has be scheduled already on same day</td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Therapist non-availability hrs</td><td>Sessions scheduled in therapist non-availability hours</td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Client non-availability hrs</td><td>Sessions scheduled in client non-availability hours </td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Exceeds max days</td><td>Session puts the therapist over maximum hours for day</td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Exceeds max week</td><td>Session puts the therapist over maximum hours for week</td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Holiday</td><td>Session scheduled on holiday</td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr><tr>
                                    <td>Client double booked</td><td>Client already has session scheduled for this time.  </td><td><input type="checkbox"></td><td><input type="checkbox"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="tab_3_6">
                            <div id="accordion2" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading9">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                               Credential Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse9" class="collapse show" role="tabpanel" aria-labelledby="heading9" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-12">Select Credential Type </label>
                                                <div class="col-md-12">
                                                    <select class="form-control">
                                                      <option value="BA">Behaviour Analyst</option>
                                                      <option value="OS">Office Staff</option>
                                                      <option value="PP">ParaProfessional</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6"></div>
                                            <div class="form-group col-md-12">
                                                <label class="control-label col-md-12">All Credentials </label>
                                                <select multiple="multiple" size="10" name="duallistbox_demo1[]">
                                                  <option value="11">BCABA</option>
                                                    <option value="12">Drivers License</option>
                                                    <option value="13">Liability Insurance</option>
                                                    <option value="14">State Photo ID</option>
                                                    <option value="15">LMFT</option>
                                                    <option value="16">Clinical Psychologist</option>
                                                    <option value="17">LCSW</option>
                                                    <option value="18">LEP</option>
                                                    <option value="19">CPR Training</option>
                                                    <option value="24">Car Insurance</option>
                                                    <option value="27">Contract 1</option>
                                                    <option value="28">Contract 2</option>
                                                    <option value="33">Contracted Provider</option>
                                                    <option value="34">BAC</option>
                                                    <option value="37">CPI</option>
                                                    <option value="38">Annual Eval</option>
                                                    <option value="50">First Aid</option>
                                                    <option value="51">Crisis Prevention/Intervention</option>
                                                    <option value="54">Pro-Act training</option>
                                                    <option value="56">BCaBA</option>
                                                    <option value="59">Family Care Safety Registry</option>
                                                    <option value="67">BSL</option>
                                                    <option value="84">Evaluations</option>
                                                    <option value="85">Blood Born Pathogens</option>
                                                    <option value="86">Sexual Harassment</option>
                                                    <option value="87">HIPAA</option>
                                                    <option value="88">Mandated Reporting</option>
                                                    <option value="89">Incident Report Training</option>
                                                    <option value="90">Complaint and Grievances</option>
                                                    <option value="91">Sensitive Services</option>
                                                    <option value="93">8-Hour Supervisor Training</option>
                                                    <option value="94">Hawaii BCBA license</option>
                                                    <option value="96">LBA certification</option>
                                                    <option value="97">Short Term Disability </option>
                                                    <option value="101">IRA</option>
                                                    <option value="105">Health Insurance Eligibility</option>
                                                    <option value="110">HIV / Aids Certificate</option>
                                                    <option value="152">BCBA-D</option>

                                                </select>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="control-label col-md-12">All Clearances </label>
                                                <select multiple="multiple" size="10" name="duallistbox_demo1[]">
                                                  <option value="11">TB Clearance</option>
                                                    <option value="12">DOJ Clearance</option>
                                                    <option value="13">FBI Clearance</option>
                                                    <option value="14">Finger Printing</option>
                                                    <option value="48">DMV records report</option>
                                                    <option value="63">MMR</option>
                                                    <option value="64">Tdap</option>
                                                    <option value="65">Hep B</option>
                                                    <option value="66">Varicella</option>
                                                    <option value="69">90 Day Probation</option>
                                                    <option value="119">NSO Registry </option>
                                                    <option value="124">Abuser Registry</option>

                                                </select>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="control-label col-md-12">All Qualifications </label>
                                                <select multiple="multiple" size="10" name="duallistbox_demo1[]">
                                                  <option value="9">Bachelors Degree</option>
                                                    <option value="10">Masters Degree</option>
                                                    <option value="11">Ph.D</option>
                                                    <option value="12">Associate Degree</option>
                                                    <option value="13">Highschool Diploma</option>
                                                    <option value="14">Psy.D</option>
                                                    <option value="16">G.E.D</option>
                                                    <option value="84">COBA</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading10">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                               HR Note Type Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse10" class="collapse" role="tabpanel" aria-labelledby="heading10" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading11">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                               Employee Position
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse11" class="collapse" role="tabpanel" aria-labelledby="heading11" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading12">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                               Game Goal Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse12" class="collapse" role="tabpanel" aria-labelledby="heading12" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading13">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                               Game Goal Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse13" class="collapse" role="tabpanel" aria-labelledby="heading13" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_7">
                            <table class="table" cellspacing="0" rules="all" border="1" id="" style="width:100%;border-collapse:collapse;">
                                <tr>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">&nbsp;</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Internal Id</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Employee/Client Name</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Display Name</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Type</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Email</th>
                                    <th style="text-align: center; background-color:#2E576B; color:#fff;">Reset Password</th>
                                </tr><tr>
                                    <td class="table-check-cell">
                                                    <span class="editactivitycheckboxusers" data-usertypeid="2"><input id="0" type="checkbox" name="02" /></span>
                                                </td><td>
                                                    <span id="0">6524</span>
                                                </td><td>
                                                    <span id="0">Dywane, Bravo</span>
                                                </td><td>
                                                    <span id="0">Bravo</span>
                                                </td><td>
                                                    <span id="0">Staff</span>
                                                </td><td>
                                                    <span id="0">bravo@gmail.com</span>
                                                </td><td>
                                                    <i class="fa fa-edit" style="font-size:22px"></i>
                                                </td>
                                </tr><tr>
                                    <td class="table-check-cell">
                                                    <span class="editactivitycheckboxusers" data-usertypeid="2"><input id="1" type="checkbox" name="03" /></span>
                                                </td><td>
                                                    <span id="1">2046</span>
                                                </td><td>
                                                    <span id="1">Miller, Andrew</span>
                                                </td><td>
                                                    <span id="1">Andrew</span>
                                                </td><td>
                                                    <span id="1">Staff</span>
                                                </td><td>
                                                    <span id="1">andrew@gmail.com</span>
                                                </td><td>
                                                    <i class="fa fa-edit" style="font-size:22px"></i>
                                                </td>
                                </tr><tr>
                                    <td class="table-check-cell">
                                                    <span class="editactivitycheckboxusers" data-usertypeid="2"><input id="2" type="checkbox" name="04" /></span>
                                                </td><td>
                                                    <span id="2">2375</span>
                                                </td><td>
                                                    <span id="2">Smith, Don</span>
                                                </td><td>
                                                    <span id="2">Don</span>
                                                </td><td>
                                                    <span id="2">Staff</span>
                                                </td><td>
                                                    <span id="2">don@gmail.com</span>
                                                </td><td>
                                                    <i class="fa fa-edit" style="font-size:22px"></i>
                                                </td>
                                </tr><tr>
                                    <td class="table-check-cell">
                                                    <span class="editactivitycheckboxusers" data-usertypeid="3"><input id="3" type="checkbox" name="05" /></span>
                                                </td><td>
                                                    <span id="3">2090</span>
                                                </td><td>
                                                    <span id="3">Bullock, Caroline</span>
                                                </td><td>
                                                    <span id="3">Caroline Bullock</span>
                                                </td><td>
                                                    <span id="3">Provider</span>
                                                </td><td>
                                                    <span id="3">bullock.caroline@yahoo.com</span>
                                                </td><td>
                                                    <i class="fa fa-edit" style="font-size:22px"></i>
                                                </td>
                                </tr>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="tab_3_8">
                            <div id="accordion3" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading14">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                               Hide Timesheet Link
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse14" class="collapse show" role="tabpanel" aria-labelledby="heading14" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                            <input type="button" value="Hide Timesheet Link" class="btn btn-primary float-right">
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading15">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                               Disallow Rendering Session
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse15" class="collapse" role="tabpanel" aria-labelledby="heading15" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                            <div class="form-group col-md-12">
                                                <input type="button" value="Save" class="btn btn-primary float-right col-md-1" />
                                                <input type="text" class="form-control float-right col-md-3" style="line-height:28px; margin-right:15px;" placeholder="mm/dd/yyyy" />
                                                <label class="control-label float-right col-md-3">Disallow Rendering Session Before </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading16">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                               Pay Periods
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse16" class="collapse" role="tabpanel" aria-labelledby="heading16" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                            <input type="button" value="Create Pay Periods" id="btnCreatePay" class="btn btn-primary float-right">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_9">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
    <div class="modal fade" id="myModal1" tabindex="-1">
        <div class="modal-dialog" style="width:70%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Practice</h4>
                </div>
                <div class="modal-body" style="min-height:150px; padding-bottom:50px;">
                    <div class="card-body">
                        <div class="row">
                            <input type="hidden" id="hdnFlag"/>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Practice Name </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="practiceName1" name="PrimaryPhysiName" value="" />
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Speciality </label>
                                <div class="col-md-12">
                                    <select class="form-control" id="phySpeciality">
                                        <option></option>
                                        <option>Psychologist</option>
                                        <option>Counselor</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Group NPI </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="phyNPI" name="txtPTNumber"  value="" />
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Date Of Birth </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="phyDOB" name="txtPTNumber"  value="" />
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Tax ID </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="phyEIN" name="txtPTNumber" value="" />
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Social Security # </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="phySSN" name="txtPTNumber"  value="" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr/>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Address </label>
                                <div class="col-md-12">
                                    <textarea class="form-control" cols="5" rows="4" id="phyAddr"></textarea>
                                </div>
                            </div>

                            <div class="form-group col-md-6" >
                                <label class="control-label col-md-12">Home</label>
                                <div class="col-md-12">
                                     <input type="text" class="form-control required" id="phyHomePhone" name="txtPTNumber"  value="" />
                                </div>
                            </div>

                             <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Work </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="phyWorkPhone" name="txtPTNumber" value="" />
                                </div>
                            </div>

                            <div class="form-group col-md-6" >
                                <label class="control-label col-md-12">Mobile </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="phyMobilePhone" name="txtPTNumber" value="" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Dept </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="phyDept" name="txtPTNumber" value="" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Fax </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="phyFax" name="txtPTNumber" value="" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr/>
                            <div class="form-group col-md-12">
                                <label class="control-label col-md-2">Notes </label>
                                <div class="col-md-12">
                                    <textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="button" value="Add Practice" class="btn btn-primary" id="btnAddPractice"/>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="mdlZone" tabindex="-1">
        <div class="modal-dialog" style="width:70%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Zone</h4>
                </div>
                <div class="modal-body" style="min-height:150px; padding-bottom:50px;">
                    <div class="card-body">
                        <div class="row">
                            <input type="hidden" id="hdnFlag"/>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Name </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="practiceName1" name="PrimaryPhysiName" value="" />
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="control-label col-md-2">Description </label>
                                <div class="col-md-12">
                                    <textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="button" value="Add Zone" class="btn btn-primary" id="btnSaveZone"/>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="mdlHoliday" tabindex="-1">
        <div class="modal-dialog" style="width:70%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Holiday</h4>
                </div>
                <div class="modal-body" style="min-height:150px; padding-bottom:50px;">
                    <div class="card-body">
                        <div class="row">
                            <input type="hidden" id="hdnFlag"/>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Date </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" placeholder="mm/dd/yyyy" value="" />
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="control-label col-md-2">Description </label>
                                <div class="col-md-12">
                                    <textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="button" value="Add Holiday" class="btn btn-primary" id="btnSaveHoliday"/>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="mdlPayPeriod" tabindex="-1">
        <div class="modal-dialog modal-lg" style="width:80%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Setup Payroll</h4>
                </div>
                <div class="modal-body" style="min-height:150px; padding-bottom:50px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-12" style="padding-bottom:10px;">
                                <label class="control-label col-md-12">How often do you pay your employees? </label>
                                <div class="col-md-4">
                                    <select class="form-control">
                                        <option selected="selected" value="1">Weekly</option>
                                        <option value="2">Bi-Weekly</option>
                                        <option value="3">From 1st &amp; 16th Every Month</option>
                                        <option value="4">Monthly</option>
                                        <option value="5">Custom</option>

                                    </select>
                                </div>
                            </div><br/>
                            <div class="form-group col-md-4" style="padding-bottom:10px;">
                                <label class="control-label col-md-12">Select Year </label>
                                <div class="col-md-12">
                                    <select class="form-control">
                                        <option value="2017">2017</option>
                                        <option selected="selected" value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label col-md-12">What is the next pay period end date? </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" />
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label col-md-12">What is the check date for next pay period? </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" />
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="control-label col-md-12">After how many days employee can't submit time sheet? </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="button" value="Create Pay Period for this year" class="btn btn-primary" id="btnSavePay"/>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<?php $__env->stopSection(); ?>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <link href="css/bootstrap-duallistbox.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/jquery.bootstrap-duallistbox.js"></script>
    <script>
        $(document).ready(function(){

            var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
            $("#demoform").submit(function() {
                alert($('[name="duallistbox_demo1[]"]').val());
                return false;
            });

            $(".sidebar-toggle").click();

            $('#masFacPhone').mask('(000) 000-0000');
            $('#masFacPhone2').mask('(000) 000-0000');
            $('#masFacFax').mask('(000) 000-0000');

            $(document).on('focusout','#masFacEmail',function() {
                if($(this).val()!=""){
                    if( !validateEmail($(this).val())) {
                        alertify.error("Please enter a valid email address");
                        $("#masFacEmail").focus();
                    }
                }
            });

            $(document).on('focusout','#masFacZip',function() {
                var currValue = $(this).val();
                $.ajax({
                    type: "POST",
                    url: root_url+"zipinfo/get",
                    async: false,
                    data:{
                        zip : currValue
                    },
                    success:function(result){
                        var n = result.length;
                        result = result.substr(0,n-1);
                        if(result != "none"){
                            var res = JSON.parse(result);
                            document.getElementById("masFacState").value = res[0].state;
                            document.getElementById("masFacCity").value = res[0].city;
                        }
                        else{
                            alertify.error("Sorry! This Zip is not available in our Database.")
                        }
                    }
                });
            });

            $.get(root_url + "/practice", function(data1, status){
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].practiceID,data1[i].practiceName,data1[i].address,data1[i].npi,data1[i].EIN,data1[i].practiceID]);
                });
                //alert(dt);
                var table = $('#practiceList').DataTable({
                    "data": dt,
                    "bPaginate": false,
                    "bProcessing": true,
                    "aoColumns": [
                        {"mdata": "practiceID","title":"Practice ID", visible:false},
                        {"title":"Practice Name","mdata": "practiceName"},
                        {"title":"Address","mdata": "address"},
                        {"title":"Group ID","mdata": "npi"},
                        {"title":"Tax ID","mdata": "EIN"},
                        {"mdata": "practiceID","title":"Action",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0);" class="edit'+data+' editPractice"><span class="ti-pencil"></span></a>';
                            }
                        },
                    ]
                });
            });

            $(document).on('click','.editPractice',function() {
                var temp = $(this).attr('class').split(' ')[0];
                var phyID = temp.replace('edit','');
                document.getElementById("hdnFlag").value = phyID;
                sessionStorage.setItem("pracID",phyID);
                window.location.href="EditPractice.php";
                // $.ajax({
                //            type: "POST",
                //            url:"getPracticeInfo.php",
                //            async : false,
                //            data:{
                //              practiceID : phyID
                //            },success:function(result){
                //              $('#myModal1').modal('show');
                //              var res = JSON.parse(result);
                //              document.getElementById("practiceName1").value = res[0].practiceName;
                //  document.getElementById("phySpeciality").value = res[0].speciality;
                //  document.getElementById("phyDOB").value = res[0].dob;
                //  document.getElementById("phyNPI").value = res[0].npi;
                //  document.getElementById("phySSN").value = res[0].ssn;
                //  document.getElementById("phyEIN").value = res[0].EIN;
                //  document.getElementById("phyAddr").value = res[0].address;
                //  document.getElementById("phyHomePhone").value = res[0].phoneHome;
                //  document.getElementById("phyWorkPhone").value = res[0].phoneWork;
                //  document.getElementById("phyMobilePhone").value = res[0].phoneMobile;
                //  document.getElementById("phyDept").value = res[0].dept;
                //  document.getElementById("phyFax").value = res[0].fax;
                //  document.getElementById("phyNotes").value = res[0].notes;
                //  document.getElementById("hdnFlag").value = res[0].practiceID;
                //  document.getElementById("btnAddPractice").value = "Update Practice";
                //            }
                //          });
            });

            $("#btnAddPractice").click(function() {
                var hdnFlag = document.getElementById("hdnFlag").value;
                if(hdnFlag == 0){
                    var phyName = document.getElementById("practiceName1").value;
                    var speciality = document.getElementById("phySpeciality").value;
                    var dob = document.getElementById("phyDOB").value;
                    var individualNPI = document.getElementById("phyNPI").value;
                    var ssn = document.getElementById("phySSN").value;
                    var EIN = document.getElementById("phyEIN").value;
                    var address = document.getElementById("phyAddr").value;
                    var phoneHome = document.getElementById("phyHomePhone").value;
                    var phoneWork = document.getElementById("phyWorkPhone").value;
                    var phoneMobile = document.getElementById("phyMobilePhone").value;
                    var dept = document.getElementById("phyDept").value;
                    var fax = document.getElementById("phyFax").value;
                    var notes = document.getElementById("phyNotes").value;

                    $.ajax({
                        type: "POST",
                        url: root_url+"practices/add",
                        data:{
                            "practiceName" : phyName,
                            "phyDOB" : dob,
                            "phyNPI" : individualNPI,
                            "phySSN" : ssn,
                            "EIN" : EIN,
                            "phyAddr" : address,
                            "phyHomePhone" : phoneHome,
                            "phyWorkPhone" : phoneWork,
                            "phyMobilePhone" : phoneMobile,
                            "dept" : dept,
                            "phyFax" : fax,
                            "phyNotes" : notes,
                            "speciality" : speciality,
                        },success:function(result){
                            //alertify.success('PrimaryPhysiName updated successfully');
                            $('#myModal1').modal('hide');
                            //window.location.reload();
                        }
                    });
                }
                else
                {
                    var practiceID = document.getElementById("hdnFlag").value;
                    var phyName = document.getElementById("practiceName1").value;
                    var speciality = document.getElementById("phySpeciality").value;
                    var dob = document.getElementById("phyDOB").value;
                    var individualNPI = document.getElementById("phyNPI").value;
                    var ssn = document.getElementById("phySSN").value;
                    var EIN = document.getElementById("phyEIN").value;
                    var address = document.getElementById("phyAddr").value;
                    var phoneHome = document.getElementById("phyHomePhone").value;
                    var phoneWork = document.getElementById("phyWorkPhone").value;
                    var phoneMobile = document.getElementById("phyMobilePhone").value;
                    var dept = document.getElementById("phyDept").value;
                    var fax = document.getElementById("phyFax").value;
                    var notes = document.getElementById("phyNotes").value;

                    $.ajax({
                        type: "POST",
                        url: root_url+"practices/update",
                        data:{
                            "practiceID" : practiceID,
                            "practiceName" : phyName,
                            "phyDOB" : dob,
                            "phyNPI" : individualNPI,
                            "phySSN" : ssn,
                            "EIN" : EIN,
                            "phyAddr" : address,
                            "phyHomePhone" : phoneHome,
                            "phyWorkPhone" : phoneWork,
                            "phyMobilePhone" : phoneMobile,
                            "dept" : dept,
                            "phyFax" : fax,
                            "phyNotes" : notes,
                            "speciality" : speciality,
                        },success:function(result){
                            //alertify.success('PrimaryPhysiName updated successfully');
                            $('#myModal1').modal('hide');
                            //window.location.reload();
                        }
                    });
                }
            });
        });
        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test( $email );
        }
    </script>

<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>