<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-6" >
            <h2>Clients</h2>
        </div>
        <div class="col-6" >
            <a href="AddPatient.php" class="btn btn-primary float-right">Add New Client</a>
        </div>
        <div class="col-md-12" style="margin-top:20px;">
            <div class="widget box" >
                <div class="widget-content">
                    <form action="EditPatient.php" method="post">
                        <div class="table-responsive">
                            <table id="test" class="table table-bordered" style="cursor:pointer; border-radius:15px; border-color:#000;" cellspacing="0" width="100%">
                            </table>
                        </div>
                    </form>
                </div>
            </div>

        </div>
<?php $__env->stopSection(); ?>
        <style type="text/css">
            .dataTables_wrapper .dataTables_scroll {
                border: 1px solid #000;
                margin: 1.5rem 0;
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
                border-bottom-left-radius: 4px;
                border-bottom-right-radius: 4px;
            }




            div.dataTables_scrollHead table.dataTable {
                margin-bottom: 0 !important;
                margin-top: 0 !important;
                border: 0 !important;

            }

            .dataTables_scrollBody {
                overflow-x: hidden !important;
                border-top: 1px solid #000;
            }


            .table {
                border-left:0px !important;
                border-right:0px !important;

            }

            .table th {
                border-bottom: 0 !important;
            }
        </style>
        <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
        <script
                src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script>


            $(document).ready(function(){
                $(".preloader").show();

                var practiceId = sessionStorage.getItem('practiceId');
                var dbName = sessionStorage.getItem('dbName');

                document.getElementById("lblUser").innerHTML = sessionStorage.getItem('userName');

                $(".navigation li").parent().find('li').removeClass("active");

                $("#navv1").addClass("active");
                $(".left-side").css({"max-height":"800px"});

                //$("#test tr").css('cursor', 'pointer');
                $.post("https://curismed.com/medService/patients",
                    {
                        practiceID: '16'
                    },
                    function(data1, status){
                        var dt = [];
                        $.each(data1,function(i,v) {
                            var DOB = changeDateFormat(data1[i].dob);
                            var txt = "";
                            // $.ajax({
                            //     type: "POST",
                            //     url: root_url+"location/list",
                            //     async : false,
                            //     data:{
                            //         dbName : dbName,
                            //         patientID : data1[i].patientID
                            //     },success:function(result){
                            //         var res = JSON.parse(result);
                            //         if(res[0].home == "1"){
                            //             txt += "<span class='badge badge-success'>Home</span> ";
                            //         }
                            //         if(res[0].office == "1"){
                            //             txt += "<span class='badge badge-success'>Office</span> ";
                            //         }
                            //         if(res[0].school == "1"){
                            //             txt += "<span class='badge badge-success'>School</span> ";
                            //         }
                            //     }
                            // });

                            if(data1[i].gender == "M"){
                                var gen = '<i class="fa fa-fw fa-male" style="font-size:22px"></i> <span style="color:#33CC99; font-size:13px; font-weight:600;">'+data1[i].fullName+'</span>';
                            }
                            else{
                                var gen = '<i class="fa fa-fw fa-female" style="font-size:22px"></i> <span style="color:#33CC99; font-size:13px; font-weight:600;">'+data1[i].fullName+'</span>';
                            }

                            dt.push([data1[i].patientID,gen,DOB,data1[i].ssn,data1[i].chartNo,data1[i].phoneHome,data1[i].PrimaryCarePhysician]);
                        });
                        //alert(dt);
                        var table = $('#test').DataTable({
                            "scrollY":        "300px",
                            "scrollCollapse": true,
                            "data": dt,
                            "columnDefs": [
                                { "width": "10px", "targets": 0 },
                                { "width": "100px", "targets": 1 },
                                { "width": "50px", "targets": 2 },
                                { "width": "70px", "targets": 3 },
                                { "width": "70px", "targets": 4 },
                                { "width": "70px", "targets": 5 },
                                { "width": "70px", "targets": 6 },
                            ],
                            columns: [
                                {"title": "patientID", visible:false},
                                {"title": "Client Name"},
                                {"title": "DOB"},
                                {"title": "SSN"},
                                {"title": "Account #"},
                                {"title": "Cell #"},
                                {"title": "Rendering Provider"}
                            ]
                        });
                        $(".dataTables_filter").find("input").css({"border-radius":"15px"});
                        $(".dataTables_scrollBody table").css({"border":"none"});
                        // $("table.dataTable thead .sorting").css({"text-align":"center"})
                        // $("table.dataTable td ").css({"text-align":"center"})
                        $('.preloader').hide();
                        $('#test tbody').on('click', 'tr', function () {
                            $('#MedCoding').modal('show');
                            var data = table.row( this ).data();
                            var id = data[0];
                            sessionStorage.setItem("patientId", id);
                            window.location.href ="EditPatient.php";
                        });
                    });
                $("th:first").css({"border-top-left-radius":"5px"});
            });
            function changeDateFormat(inputDate){  // expects Y-m-d
                if(inputDate != "" && inputDate != undefined && inputDate != null){
                    var splitDate = inputDate.split('-');
                    if(splitDate.count == 0){
                        return null;
                    }

                    var year = splitDate[0];
                    var month = splitDate[1];
                    var day = splitDate[2];

                    return month + '-' + day + '-' + year;
                }
            }
</script>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>