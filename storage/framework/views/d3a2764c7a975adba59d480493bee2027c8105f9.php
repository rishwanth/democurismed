<?php $__env->startSection('content'); ?>
<?php
    $base_url = URL::to('/');
?>
<div class="row" style="margin: 5%;">
    <!-- <div class="card" style="width: 100%;">
        <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                   Add Sessions to the Facility
                </a>
            </h5>
        </div>
        <div id="collapseThree" class="collapse show" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion" style="padding:20px;">
            <div class="card-body">
                <select multiple="multiple" size="10" name="duallistbox_demo1[]">
                  <option value="option1">AETNA</option>
                  <option value="option2">MEDICARE</option>
                  <option value="option3" selected="selected">MEDICAID</option>
                  <option value="option4">CIGNA</option>
                  <option value="option5">TRICARE</option>
                  <option value="option6" selected="selected">UHC</option>
                  <option value="option7">Optum</option>
                  <option value="option8">Healthnet</option>
                  <option value="option9">Anthem</option>
                  <option value="option0">BCBS</option>
                </select>
            </div>
        </div>
    </div> -->


    <div class="card" style="width: 100%;">
        <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                   Add Sessions to the Facility
                </a>
            </h5>
        </div>
        <div id="collapseThree" class="collapse show" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion" style="padding:20px;">
            <div class="card-body">
                <div class="col-sm-12">
                    <div class="form-group row" style="margin-bottom: 1%">
                        <div class="col-4">
                            <label for="input-text" class="control-label float-right txt_media1">Session Rule Name</label>
                        </div>
                        <div class="col-8">
                            <input type="text" class="col-md-6 form-control" id="txtSessionName"/>
                        </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: 1%">
                        <div class="col-4">
                            <label for="input-text" class="control-label float-right txt_media1">Session Rule Desc</label>
                        </div>
                        <div class="col-8">
                            <input type="text" class="col-md-6 form-control" id="txtSessionDesc"/>
                        </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: 1%">
                        <div class="col-4">
                            <label for="input-text" class="control-label float-right txt_media1">Run Rule </label>
                        </div>
                        <div class="col-8">
                            <input type="checkbox" id="chkRunrule"/>
                        </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: 1%">
                        <div class="col-4">
                            <label for="input-text" class="control-label float-right txt_media1">Prevent Session Creation</label>
                        </div>
                        <div class="col-8">
                            <input type="checkbox" id="chkPreventSession"/>
                        </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: 1%">
                        <div class="col-4">
                        </div>
                        <div class="col-8">
                            <input type="button" id="btnAddSessions" value="Add Session" class="btn btn-primary" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <table id="getSessions" class="table table-bordered"></table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){
        //document.getElementById("lblUser").innerHTML = <?php echo e(Session::get('LOGGED_USER_NAME')); ?>;
        var root_url = $("#hidden_root_url").val();
        //alert('i am here layout=>app.blade.php' + root_url);
        $("#linkLogout").click(function(){
            sessionStorage.removeItem('practiceName');
            sessionStorage.removeItem('userID');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('loginName');
        })
        var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
        $("#demoform").submit(function() {
            alert($('[name="duallistbox_demo1[]"]').val());
            return false;
        });

        $("#btnAddSessions").click(function(){
            var session_name = $("#txtSessionName").val();
            var session_desc = $("#txtSessionDesc").val();
            var is_run_rule = 0;
            var is_prevent_session_creation = 0;
            if($('#chkRunrule').is(':checked') == true){
                is_run_rule = 1;
            }
            else{
                is_run_rule = 0;
            }
            if($('#chkPreventSession').is(':checked') == true){
                is_prevent_session_creation = 1;
            }
            else{
                is_prevent_session_creation = 0;
            }
            $.ajax({
                type: "POST",
                url: root_url+"/handler/sessionrule/add",
                data: {
                    name : session_name,
                    description : session_desc,
                    is_run_rule : is_run_rule,
                    is_prevent_session_creation : is_prevent_session_creation,
                }, success: function (result) {
                    window.location.reload();
                }
            });
        });

        $.ajax({
            type: "POST",
            url: root_url+"/handler/get",
            data: {
                handler : "session_rule"
            }, success: function (result) {
                var res = JSON.parse(result);
                if(res.status == 0){
                    return;
                }
                var data1 = res.data;
                var dt = [];
                $.each(data1,function(i,v) {
                    if(data1[i].is_run_rule == 0){
                        var c = "<input type='checkbox' id='chkRunRule"+data1[i].id+"'/>"
                    }
                    else{
                        var c = "<input type='checkbox' id='chkRunRule"+data1[i].id+"' checked/>"
                    }
                    if(data1[i].is_prevent_session_creation == 0){
                        var d = "<input type='checkbox' id='chkPreventSession"+data1[i].id+"'/>"
                    }
                    else{
                        var d = "<input type='checkbox' id='chkPreventSession"+data1[i].id+"' checked/>"
                    }
                    dt.push([data1[i].id,data1[i].name,data1[i].description, c, d]);
                });
                $('#getSessions').DataTable({
                    "data": dt,
                    "autoWidth": true,
                    
                    columns: [
                        {"title": "docID","visible" : false},
                        {"title": "Session Name",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Session Description", 
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Run Rule", 
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Prevent Session Creation", 
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                    ]
                });
            }
        });

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })
    });
</script>

<?php echo $__env->make('layout.handler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>