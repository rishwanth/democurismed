<?php $__env->startSection('content'); ?>
<?php
    $base_url = URL::to('/');
?>
<div class="row" style="margin: 5%; ">
    <div class="bs-example" style="width: 60%; ">
        <ul class="nav nav-tabs" style="margin-bottom: 15px;">
            <li class="nav-item">
                <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Add Activity</a>
            </li>
            <li class="nav-item">
                <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Add CPT codes</a>
            </li>
            <!-- <li class="nav-item">
        <a href="#tab_3_7" style="color:#000;" data-toggle="tab" class="nav-link">Log</a>
    </li> -->
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane active" id="tab_3_1">
                <div class="col-12">
                    <div class="card-body">
                        <div class="col-sm-12">
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Type of Service</label>
                                </div>
                                <div class="col-8">
                                    <select id="ddlService" class="col-md-10 form-control">
                                        <option value="">-- Select Service Type --</option>
                                        <option value="1">Behaviour Therapy</option>
                                        <option value="2">Physical Therapy</option>
                                        <option value="3">Occupational Therapy</option>
                                        <option value="4">Speech Therapy</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Activity Name</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="col-md-10 form-control" id="txtActivity"/>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                </div>
                                <div class="col-8">
                                    <input type="button" class="btn btn-primary" id="btnAddActivity" value="Add Activity"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <table id="getActivities" class="table table-bordered"></table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab_3_2">
                <div class="col-12">
                    <div class="card-body">
                        <div class="col-sm-12">
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-2">
                                    <label for="input-text" class="control-label float-right txt_media1">Activity Name</label>
                                </div>
                                <div class="col-8">
                                    <select id="ddlActivity" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <div class="col-2">
                                    <label for='dup' class="m-t-b-15">CPT Codes: </label>
                                </div>
                                <div class="col-8">
                                    <div class='input'>
                                        <input id='dup' value='' placeholder="type & hit space to create codes" style="min-width: 200px;">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <input type="button" class="btn btn-primary" value="Add Values" id="btnGetValues"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="codesList"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<link rel="stylesheet" href="<?php echo $base_url; ?>/public/vendors/insignia/css/insignia.css">
<link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>/public/css/custom_css/insignia_custom.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="<?php echo $base_url; ?>/public/vendors/insignia/js/insignia.js"></script>

<script>
    $(document).ready(function(){
        insignia(dup, {
            validate: function () {
                return true;
            }
        });
        //alert(sessionStorage.getItem("patientId"));
        //document.getElementById('hdntoggleDX').value = "false";
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        //document.getElementById('practiceName1').innerHTML = sessionStorage.getItem('practiceName');
        //document.getElementById("lblUser").innerHTML = <?php echo e(Session::get('LOGGED_USER_NAME')); ?>;
        var root_url = $("#hidden_root_url").val();
        //alert('i am here layout=>app.blade.php' + root_url);
        $("#linkLogout").click(function(){
            sessionStorage.removeItem('practiceName');
            sessionStorage.removeItem('userID');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('loginName');
        })
        $("#btnGetValues").click(function(){
            var getHtml = $(".nsg-editor span").html();
            getHtml = getHtml.replace(/<span class="nsg-tag">/g,'');
            var values = getHtml.replace(/[\/]/g, '');
            values = values.replace(/<span>/g,',');
            values = values.split(",");
            var val = $("#ddlActivity").val();
            for(var i=0; i<values.length-1;i++){
                $.ajax({
                    type: "POST",
                    url: root_url+"/handler/codes/add",
                    data: {
                        handler_activity_id : val,
                        code : values[i]
                    }, success: function (result) {
                    }
                });
            }
            for(var i=0; i<values.length-1;i++){
                    $('#codesList').append('<span style="font-size:140%;margin:1em" class="badge badge-success">' + values[i] + '</span>');
            }
            $(".nsg-tags").html("");

        });
        $("#ddlActivity").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: root_url+"/handler/get",
                data: {
                    handler_activity_id : val,
                    handler : "codes"
                }, success: function (result) {
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }
                    var data1 = res.data;
                    $.each(data1, function(key, val){
                        $('#codesList').append('<span style="font-size:140%; margin:1em" class="badge badge-success">' + val.code + '</span>');
                    });

                }
            });
        });
        $("#btnAddActivity").click(function(){
            var service_type_id = $("#ddlService").val();
            var activity = $("#txtActivity").val();
            $.ajax({
                type: "POST",
                url: root_url+"/handler/activity/add",
                data: {
                    service_type_id : service_type_id,
                    name : activity
                }, success: function (result) {
                    window.location.reload();
                }
            });
        });

        $.ajax({
            type: "POST",
            url: root_url+"/handler/get",
            data: {
                handler : "activity"
            }, success: function (result) {
                var res = JSON.parse(result);
                if(res.status == 0){
                    return;
                }
                var data1 = res.data;
                $('#ddlActivity').append('<option value="">-- Select Activity --</option>');
                $.each(data1, function(key, val){
                    $('#ddlActivity').append('<option value="' + val.id + '">' + val.name + '</option>');
                });
            }
        });

        $.ajax({
            type: "POST",
            url: root_url+"/handler/get",
            data: {
                handler : "activity"
            }, success: function (result) {
                var res = JSON.parse(result);
                if(res.status == 0){
                    return;
                }
                var data1 = res.data;
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].id,data1[i].service_name,data1[i].name]);
                });
                $('#getActivities').DataTable({
                    "data": dt,
                    "autoWidth": true,
                    
                    columns: [
                        {"title": "docID","visible" : false},
                        {"title": "Service Type",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Activity Name", 
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                    ]
                });
            }
        });

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })
    });
</script>

<?php echo $__env->make('layout.handler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>