<?php $__env->startSection('content'); ?>
 
<div class="row">
	<h2 style="color: #251367; margin-left:20px;">Statement List</h2>
	<div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">
        	<form action="EditPatient.php" method="post">
            <table id="test" class="table table-bordered" cellspacing="0" width="100%">
			</table>
			</form>
        </div>
    </div>

</div>

<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script>
        // var data1 = <?php
        //     $con = mysql_connect("localhost:3306","curis_user","Curis@123");

        //     if(!$con){
        //         die("Error : ".mysql_error());
        //     }
        //     mysql_select_db("curismed_aba",$con);

        //     $result = mysql_query("select * from m_statements");

        //     $edi = array();

        //     while($row = mysql_fetch_array($result)){
        //         $row_array['statementID'] = $row['statementID'];
        //         $row_array['statementName'] = $row['statementName'];
        //         $row_array['location'] = $row['location'];
        //         $row_array['createdOn'] = $row['createdOn'];
        //         array_push($edi,$row_array);
        //     }
        //     echo json_encode($edi);

        //     ?>;

        $(document).ready(function(){
            var root_url = $("#hidden_root_url").val();
            document.getElementById("practiceName").innerHTML = sessionStorage.getItem("practiceName");
            var dt = [];
            $.ajax({
                type: "POST",
                url: root_url+"statement_list",
                async : false,
                data:{
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }
                    var data1 = res.data;
                    $.each(data1,function(i,v) {
                        dt.push([data1[i].id,data1[i].name,data1[i].location+data1[i].name,data1[i].created_on]);
                    });
                }
            });
            $('#test').DataTable({
                "data": dt,
                columns: [
                    {"title": "ID",visible:false},
                    {"title": "Statement Name"},
                    {"title": "Location"},
                    {
                        "title":"Created On",
                        "mdata": "createdOn",
                        mRender: function (data, type, row) {
                            return changeDateFormat(data);
                        }
                    },
                    {
                        "title":"Actions",
                        "mdata": "Actions",
                        mRender: function (data, type, row) { return '<a href="public/docs/statements/'+row[1]+'" download><i class="icon icon-download-alt"></i></a>&nbsp;|&nbsp;<a href="public/docs/statements/'+row[1]+'" target="_blank">View</a>'; }
                    },
                ]
            });

        });
        function changeDateFormat(inputDate){  // expects Y-m-d
            var splitDate = inputDate.split('-');
            if(splitDate.count == 0){
                return null;
            }

            var year = splitDate[0];
            var month = splitDate[1];
            var day = splitDate[2].slice(0,2);
            var n = splitDate[2].length;
            var time = splitDate[2].slice(3,n);

            return month + '-' + day + '-' + year+ ' '+time;
        }
    </script>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>