<?php $__env->startSection('content'); ?>
<?php
    $base_url = URL::to('/');
?>
    <div class="row" style="margin: 5%; ">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-2">
            <a href="<?php echo $base_url; ?>/handler/insurances"><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/insurance.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Insurance</span>
            </div></a>
        </div>
        <div class="col-sm-2">
            <a href="<?php echo e(URL::to('/')); ?>/handler/activity"><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/activity.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Activity</span>
            </div></a>
        </div>
        <div class="col-sm-2">
            <a href="<?php echo e(URL::to('/')); ?>/handler/sessions"><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/session.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Session Rule</span>
            </div></a>
        </div>
        <!-- <div class="col-sm-2">
            <a href="<?php echo e(URL::to('/')); ?>/handler/cpts"><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/cpt.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>CPTs</span>
            </div></a>
        </div> -->
        <div class="col-sm-2">
            <a href="<?php echo e(URL::to('/')); ?>/handler/users"><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/users.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Users</span>
            </div></a>
        </div>
        <div class="col-sm-2">
            <a href="<?php echo e(URL::to('/')); ?>/handler/reports"><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/reports.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Reports</span>
            </div></a>
        </div>
        <div class="col-sm-1">
        </div>
    </div>
    <div class="row" style="margin: 5%; ">    
        <!-- <div class="bootstrap-admin-panel-content summer_noted">
            <div id="summernote"></div>
        </div> -->
    </div>
    
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout.handler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>