<br/>
Dear <b><?php echo e($mUser->first_name); ?> <?php echo e($mUser->last_name); ?> </b>,

<br/><br/>We are glad to have you a part with Curismed,<br/><br/> 

Here is the activation link to activate your account.

<a href="<?php echo e($mUser->link); ?>"><?php echo e($mUser->link); ?></a>

<br/><br/>

<br/><br/>Thanks,

<br/><?php echo e($mUser->sender); ?>

