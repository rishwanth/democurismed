<?php $__env->startSection('content'); ?>
<?php
    $base_url = URL::to('/');
?>
<div class="row">
	<h2 style="color: #251367; margin-left:20px;">EDI List</h2>
	<div class="col-md-12" style="margin-top:20px;">
        <div class="form-group col-md-12" style="margin-bottom:50px;">
            <input type="button" class="btn btn-primary" value="Submit EDI" id="btnGenerate"/>
        </div>
        <div class="widget box" >
            <div class="widget-content">
            	<form action="EditPatient.php" method="post">
                <table id="test" class="table table-bordered" cellspacing="0" width="100%">
                </table>
				</form>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<script
                src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/public/vendors/animate/animate.min.css"/>
<link href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.brighttheme.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.buttons.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.mobile.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.history.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/public/css/custom_css/toastr_notificatons.css">
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.animate.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.buttons.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.confirm.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.nonblock.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.mobile.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.desktop.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.history.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.callbacks.js"></script>
<script>
    

    $(document).ready(function(){
        var root_url = $("#hidden_root_url").val();
        
        $(".navigation li").parent().find('li').removeClass("active");
        getEDI();

        var inv = "";
        $.ajax({
            type: "GET",
            url: root_url+"edi/getlast",
            async : false,
            data:{
            },success:function(result){
                inv = parseInt(result)+1;
                inv = pad(inv, 5);
            }
        });
        $('#btnGenerate').click(function(){
            $.ajax({
                type: "POST",
                url: root_url+"patient/claimslist",
                data:{
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: "No Claims to be generated",
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                        return;
                    }
                    var res = res.data;
                    if(res.length != 0){
                        for(var i=0; i< res.length;i++){
                            var chNo = res[i].chart_num;
                            var ClNo = res[i].claim_number;
                            $.ajax({
                                type: "POST",
                                url:root_url+"edi/create",
                                async:false,
                                data:{
                                    ClaimNumber : ClNo,
                                    withBackgroundImage : 1,
                                    chartNumber : chNo
                                },success:function(result){
                                    // var percent = 0;
                                    // var notice = new PNotify({
                                    //     text: "Please Wait",
                                    //     type: 'info',
                                    //     icon: 'fa fa-spinner fa-spin',
                                    //     hide: false,
                                    //     buttons: {
                                    //         closer: false,
                                    //         sticker: false
                                    //     },
                                    //     opacity: .75,
                                    //     shadow: false,
                                    //     width: "170px"
                                    // });

                                    // setTimeout(function () {
                                    //     notice.update({
                                    //         title: false
                                    //     });
                                    //     var interval = setInterval(function () {
                                    //         percent += 2;
                                    //         var options = {
                                    //             text: percent + "% complete."
                                    //         };
                                    //         if (percent == 80) options.title = "Almost There";
                                    //         if (percent >= 100) {
                                    //             window.clearInterval(interval);
                                    //             options.title = "Done!";
                                    //             options.type = "success";
                                    //             options.hide = true;
                                    //             options.delay = 3000;
                                    //             options.buttons = {
                                    //                 closer: true,
                                    //                 sticker: true
                                    //             };
                                    //             options.icon = 'fa fa-check';
                                    //             options.opacity = 1;
                                    //             options.shadow = true;
                                    //             options.width = PNotify.prototype.options.width;
                                    //         }
                                    //         notice.update(options);
                                    //     }, 120);
                                    // }, 1500);
                                }
                            });
                        }
                        new PNotify({
                            type:'success',
                            text: "EDI successfully generated",
                            after_init: function(notice){
                                notice.attention('rubberBand');
                            }
                        })
                        getEDI();

                    }
                    else{
                        new PNotify({
                            title: 'Oh No!',
                            text: 'No Claims are to be generated.',
                            type: 'error'
                        });
                    }
                }
            });
        });
    });

    function getEDI(){
        var root_url = $("#hidden_root_url").val();
        $.ajax({
            type: "POST",
            url: root_url+"edi/list",
            async : false,
            data:{

            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    var dt = [];
                    $('#test').DataTable({
                        "data": dt,
                        destroy: true,
                        columns: [
                            {"title": "ID",visible:false},
                            {"title": "Invoice No"},
                            {"title": "Location"},
                            {
                                "title":"Created On",
                                "mdata": "createdOn",
                                mRender: function (data, type, row) { 
                                    return changeDateFormat1(data); 
                                }
                            },
                            {
                                "title":"Actions",
                                "mdata": "Actions",
                                mRender: function (data, type, row) { return '<a href="public/docs/edi/outbound/'+data+'" download><span class="ti-download"></span></a>&nbsp;|&nbsp;<a href="public/docs/edi/outbound/'+data+'" target="_blank">View</a>&nbsp;|&nbsp;<a href="public/docs/hcfa/'+data+'.pdf" target="_blank">HCFA 1500</a>'; }
                              //mRender: function (data, type, row) { return '<a href="EDI/'+data+'.txt" download><span class="ti-download"></span></a>&nbsp;|&nbsp;<a href="EDI/'+data+'.txt" target="_blank">View</a>&nbsp;|&nbsp;<a href="'+data+'.pdf" target="_blank">HCFA 1500</a>'; }
                            },
                        ]
                    });
                    return;
                }
                res = res.data;
                var dt = [];
                $.each(res, function (i, v) {
                    dt.push([res[i].id, res[i].invoice_num, 'public/docs/', res[i].created_on, res[i].invoice_num]);
                });

                $('#test').DataTable({
                    "data": dt,
                    columns: [
                        {"title": "ID",visible:false},
                        {"title": "Invoice No"},
                        {"title": "Location"},
                        {
                            "title":"Created On",
                            "mdata": "createdOn",
                            mRender: function (data, type, row) { 
                                return changeDateFormat1(data); 
                            }
                        },
                        {
                            "title":"Actions",
                            "mdata": "Actions",
                            mRender: function (data, type, row) { return '<a href="public/docs/edi/outbound/'+data+'" download><span class="ti-download"></span></a>&nbsp;|&nbsp;<a href="public/docs/edi/outbound/'+data+'" target="_blank">View</a>&nbsp;|&nbsp;<a href="public/docs/hcfa/'+data+'.pdf" target="_blank">HCFA 1500</a>'; }
                          //mRender: function (data, type, row) { return '<a href="EDI/'+data+'.txt" download><span class="ti-download"></span></a>&nbsp;|&nbsp;<a href="EDI/'+data+'.txt" target="_blank">View</a>&nbsp;|&nbsp;<a href="'+data+'.pdf" target="_blank">HCFA 1500</a>'; }
                        },
                    ]
                });

            }
        });
    }

    function changeDateFormat1(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2].slice(0,2);
        var n = splitDate[2].length;
        var time = splitDate[2].slice(3,n);

        return month + '-' + day + '-' + year+ ' '+time;
    }
    function pad(number, length) {

        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }

        return str;

    }
    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2];

        return month + '-' + day + '-' + year;
    }
</script>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>