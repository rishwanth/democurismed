<?php
    $base_url = URL::to('/');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>AMROMED LLC </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="<?php echo $base_url; ?>/public/img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>/public/css/app.css"/>
    <!-- end of global css -->
    <!--page level css -->
    <link href="<?php echo $base_url; ?>/public/vendors/swiper/css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/nvd3/css/nv.d3.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $base_url; ?>/public/vendors/lcswitch/css/lc_switch.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/select2/css/select2.min.css" type="text/css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/select2/css/select2-bootstrap.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/vendors/hover/css/hover-min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/laddabootstrap/css/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/weathericon/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/vendors/metrojs/css/MetroJs.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/css/custom.css" rel="stylesheet" type="text/css" >
    <link href="<?php echo $base_url; ?>/public/css/custom_css/wizard.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/css/advbuttons.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/css/custom_css/dashboard2.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/css/custom_css/dashboard1.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/css/custom_css/dashboard1_timeline.css" rel="stylesheet"/>
    <link href="<?php echo $base_url; ?>/public/css/jquery.datetimepicker.css" rel="stylesheet" />
    <link href="<?php echo $base_url; ?>/public/css/alertify.css" rel='stylesheet' type='text/css'>
    <link href="<?php echo $base_url; ?>/public/css/default.css" rel='stylesheet' type='text/css'>
    <link href="<?php echo $base_url; ?>/public/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $base_url; ?>/public/css/mini_sidebar.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo $base_url; ?>/public/js/jquery.min.js"></script>
    <style type="text/css">
        ::-webkit-input-placeholder {
            font-size: 11px;
        }
        :-moz-placeholder { /* Firefox 18- */
            font-size: 11px;
        }
        ::-moz-placeholder {  /* Firefox 19+ */
            font-size: 11px;
        }
        /* Overriding styles */
        ::-webkit-input-placeholder {
            font-size: 12px!important;
        }
        :-moz-placeholder { /* Firefox 18- */
            font-size: 12px!important;
        }
        ::-moz-placeholder {  /* Firefox 19+ */
            font-size: 12px!important;
        }
        .ui-autocomplete-input {
            border: none;
            font-size: 11px;
            z-index: 1000;
            height: 24px;
            margin-bottom: 5px;
            padding-top: 2px;
            border: 1px solid #DDD !important;
            padding-top: 0px !important;
            z-index: 1511;
            position: relative;
        }
        .ui-menu .ui-menu-item a {
            font-size: 11px;
            color:#fff;
        }
        .ui-autocomplete {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 99999999 !important;
            float: left;
            display: none;
            min-width: 160px;
            width: 160px;
            padding: 4px 0;
            margin: 2px 0 0 0;
            list-style: none;
            background-color: #ffffff;
            border-color: #ccc;
            border-color: rgba(0, 0, 0, 0.2);
            border-style: solid;
            border-width: 1px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;
            -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            -webkit-background-clip: padding-box;
            -moz-background-clip: padding;
            background-clip: padding-box;
            *border-right-width: 2px;
            *border-bottom-width: 2px;
        }
        .ui-menu-item > a.ui-corner-all {
            display: block;
            padding: 3px 15px;
            clear: both;
            font-weight: normal;
            line-height: 18px;
            color:#000;
            white-space: nowrap;
            text-decoration: none;
        }
        .ui-state-hover {
            color: #fff;
            text-decoration: none;
            background-color: #0088cc;
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            background-image: none;
        }
        .ui-state-active {
            color: #fff;
            text-decoration: none;
            background-color: #0088cc;
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            background-image: none;
        }
        .icon:hover{
            text-decoration: none;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
    <!--end of page level css-->
</head>
<body class="skin-default mini_sidebar">
<div class="preloader">
    <div class="loader_img"><img src="<?php echo $base_url; ?>/public/img/loader.gif" alt="loading..." height="64" width="64"></div>
</div>
<!-- header logo: style can be found in header-->
<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="<?php echo e(URL::to('index')); ?>" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the marginin -->
            <img src="<?php echo $base_url; ?>/public/img/logo_white.png" alt="logo"/>
        </a>
        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <div class="mr-auto">
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <i class="fa fa-fw ti-menu-alt"></i>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw ti-email black"></i>
                        <span class="badge badge-pill badge-success">2</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages table-striped">
                        <li class="dropdown-title">New Messages</li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar7.jpg" alt="avatar-image">
                                <div class="message-body"><strong>Ernest Kerry</strong>
                                    <br>
                                    Can we Meet?
                                    <br>
                                    <small>Just Now</small>
                                    <span class="badge badge-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar6.jpg" alt="avatar-image">
                                <div class="message-body"><strong>John</strong>
                                    <br>
                                    Dont forgot to call...
                                    <br>
                                    <small>5 minutes ago</small>
                                    <span class="badge badge-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar5.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Wilton Zeph</strong>
                                    <br>
                                    If there is anything else &hellip;
                                    <br>
                                    <small>14/10/2014 1:31 pm</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar1.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Jenny Kerry</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Tony</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-footer"><a href="#"> View All messages</a></li>
                    </ul>
                </li>
                <!--rightside toggle-->
                <li>
                    <a href="#" class="dropdown-toggle toggle-right" data-toggle="dropdown">
                        <i class="fa fa-fw ti-view-list black"></i>
                        <span class="badge badge-pill badge-danger">9</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown-->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle padding-user d-block" data-toggle="dropdown">
                        <img src="<?php echo $base_url; ?>/public/img/authors/avatar1.jpg" width="25" class="rounded-circle img-fluid float-left"
                             height="25" alt="User Image">
                        <div class="riot">
                            <div id="practiceName">
                                Addison
                                <span><i class="fa fa-sort-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo $base_url; ?>/public/img/authors/avatar1.jpg" class="rounded-circle" alt="User Image">
                            <p id="practiceName1"> Addison</p>
                        </li>
                        <!-- Menu Body -->
                        <li class="p-t-3"><a href="javascript:void(0)" class="dropdown-item"> <i class="fa fa-fw ti-user"></i> My Profile </a>
                        </li>
                        <li role="presentation"></li>
                        <li><a href="javascript:void(0)" class="dropdown-item"><i class="fa fa-fw ti-settings"></i> Account Settings </a></li>
                        <li role="presentation" class="dropdown-divider"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="float-left">
                                <a href="<?php echo e(URL::to('lockscreen')); ?>">
                                    <i class="fa fa-fw ti-lock"></i>
                                    Lock
                                </a>
                            </div>
                            <div class="float-right">
                                <a href="<?php echo e(URL::to('logout')); ?>?session_token=token" id="linkLogout">
                                    <i class="fa fa-fw ti-shift-right"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <input type="hidden" id="hidden_root_url" name="hidden_root_url"
           value="<?php echo e(URL::to('/')); ?>/">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="float-left profile-thumb" href="#">
                            <img src="<?php echo $base_url; ?>/public/img/authors/avatar1.jpg" class="rounded-circle" alt="User Image"></a>
                        <div class="content-profile">
                            <h4 class="media-heading" style="overflow:hidden" id="lblUser"></h4>
                            <ul class="icon-list">
                                <li>
                                    <a href="javascript:void(0)" title="user">
                                        <i class="fa fa-fw ti-user"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo e(URL::to('lockscreen')); ?>" title="lock">
                                        <i class="fa fa-fw ti-lock"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="settings">
                                        <i class="fa fa-fw ti-settings"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.html" title="Login">
                                        <i class="fa fa-fw ti-shift-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="navigation">
                    <li id="navv1" class="mini_bar">
                        <a href="<?php echo e(URL::to('/patient/view')); ?>">
                            <i class="menu-icon ti-user"></i>
                            <span class="mm-text">Clients</span>
                        </a>
                    </li>
                    <li id="navv10" class="mini_bar">
                        <a href="<?php echo e(URL::to('/list_physicians')); ?>">
                            <i class="menu-icon ti-id-badge"></i>
                            <span class="mm-text">Employees</span>
                        </a>
                    </li>
                    <li id="navv2" class="mini_bar">
                        <a href="<?php echo e(URL::to('/scheduler')); ?>">
                            <i class="menu-icon ti-calendar"></i>
                            <span class="mm-text">Scheduler</span>
                        </a>
                    </li>
                    <li class="menu-dropdown mini_bar"  id="navv3">
                        <a href="javascript:void(0)">
                            <i class="menu-icon ti-check-box"></i>
                            <span class="mm-text">EDI</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="<?php echo e(URL::to('/list_edi')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> Generate EDI
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/list_edi')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> EDI List
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown mini_bar"  id="navv4">
                        <a href="javascript:void(0)">
                            <i class="menu-icon ti-check-box"></i>
                            <span class="mm-text">Statements</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="<?php echo e(URL::to('/generate_statements')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> Generate Statements
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/list_statements')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> Statements List
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li id="navv5" class="mini_bar">
                        <a href="<?php echo e(URL::to('/ledger')); ?>">
                            <i class="menu-icon ti-receipt"></i>
                            <span class="mm-text">Ledger</span>
                        </a>
                    </li>
                    <li id="navv6" class="mini_bar">
                        <a href="<?php echo e(URL::to('/deposits')); ?>">
                            <i class="menu-icon ti-money"></i>
                            <span class="mm-text">Deposits</span>
                        </a>
                    </li>
                    <li id="navv7" class="mini_bar">
                        <a href="<?php echo e(URL::to('CRM/dashboard.html')); ?>">
                            <i class="menu-icon ti-desktop"></i>
                            <span class="mm-text">CRM</span>
                        </a>
                    </li>
                    <li id="navv8" class="mini_bar">
                        <a href="<?php echo e(URL::to('/reports')); ?>">
                            <i class="menu-icon ti-layout-list-large-image"></i>
                            <span class="mm-text">Reports</span>
                        </a>
                    </li>
                    <li class="menu-dropdown mini_bar" id="navv9">
                        <a href="javascript:void(0)">
                            <i class="menu-icon ti-check-box"></i>
                            <span class="mm-text">Settings</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="<?php echo e(URL::to('/list_practices')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> Practices
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/list_serviceLocations')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> Locations &amp; Facilities
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/CPTs')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> Procedure Codes
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/DXs')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> Diagnosis Codes
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/list_insurances')); ?>">
                                    <i class="fa fa-fw ti-receipt"></i> Payers List
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li id="navv10" class="mini_bar">
                        <a href="<?php echo e(URL::to('/acl')); ?>">
                            <i class="menu-icon ti-layout-list-large-image"></i>
                            <span class="mm-text">Permission</span>
                        </a>
                    </li>

                </ul>
                <!-- / .navigation -->
            </div>
            <!-- menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <aside class="right-side">
        <section class="content-header">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-8">
                    <div class="header-element">
                        <h3>Curis /
                            <small> Dashboard</small>
                        </h3>
                    </div>
                </div>
                <div class="col-lg-8 ml-auto col-md-8 col-sm-8 col-4">
                    <div class="header-mobile-object">
                        <nav class="navbar">
                            <div class="navbar-header">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobilenav" aria-controls="mobilenav" aria-expanded="false" aria-label="">
                                    <span class="fa fa-fw ti-menu"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="mobilenav">
                                <ul class="nav navbar-nav">
                                    <li><a href="dashboard.php">Home</a></li>
                                    <li><a href="<?php echo e(URL::to('/patient/add')); ?>">Add Patient</a></li>
                                    <li><a href="<?php echo e(URL::to('/patient/view')); ?>">Patient Search</a></li>
                                    <li><a href="<?php echo e(URL::to('/scheduler')); ?>">Appointments</a></li>
                                    <li><a href="<?php echo e(URL::to('/list_statements')); ?>">Generate EDI</a></li>
                                    <li><a href=<?php echo e(URL::to('/"ledger')); ?>">Ledger</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="header-object">
                        <ul>
                            <li>
                                <a href="dashboard.php">
                                    <i class="fa fa-fw ti-home" title="Home"></i>
                                    <span>Home</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/patient/add')); ?>">
                                    <i class="fa fa-fw ti-plus" title="Add Patient" ></i>
                                    <span>Add Patient</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/patient/view')); ?>">
                                    <i class="fa fa-fw ti-search"  title="Patient Search"></i>
                                    <span>Patient Search</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/scheduler')); ?>">
                                    <i class="fa fa-fw ti-calendar" title="Appointments"></i>
                                    <span>Appointments</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/list_edi')); ?>">
                                    <i class="fa fa-fw ti-printer" title="Generate EDI"></i>
                                    <span>Generate EDI</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(URL::to('/ledger')); ?>">
                                    <i class="fa fa-fw ti-receipt" title="Ledger" ></i>
                                    <span>Ledger</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <!--rightside bar -->
            <?php echo $__env->yieldContent('content'); ?>
            <div id="right">
                <div id="right-slim">
                    <div class="rightsidebar-right">
                        <div class="rightsidebar-right-content">
                            <div class="panel-tabs">
                                <ul class="nav nav-tabs nav-float" role="tablist">
                                    <li class="nav-item text-center">
                                        <a href="#r_tab1" role="tab" data-toggle="tab" class="nav-link active "><i
                                                    class="fa fa-fw ti-comments"></i></a>
                                    </li>
                                    <li class="text-center nav-item">
                                        <a href="#r_tab2" role="tab" data-toggle="tab" class="nav-link"><i class="fa fa-fw ti-bell"></i></a>
                                    </li>
                                    <li class="text-center nav-item">
                                        <a href="#r_tab3" role="tab" data-toggle="tab" class="nav-link"><i
                                                    class="fa fa-fw ti-settings"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="r_tab1">
                                    <div id="slim_t1">
                                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                            <i class="menu-icon  fa fa-fw ti-user"></i>
                                            Contacts
                                        </h5>
                                        <ul class="list-unstyled margin-none">
                                            <li class="rightsidebar-contact-wrapper">
                                                <a class="rightsidebar-contact" href="#">
                                                    <img src="<?php echo $base_url; ?>/public/img/authors/avatar6.jpg"
                                                         class="rounded-circle float-right" alt="avatar-image">
                                                    <i class="fa fa-circle text-xs text-primary"></i>
                                                    Annette
                                                </a>
                                            </li>
                                            <li class="rightsidebar-contact-wrapper">
                                                <a class="rightsidebar-contact" href="#">
                                                    <img src="<?php echo $base_url; ?>/public/img/authors/avatar.jpg"
                                                         class="rounded-circle float-right" alt="avatar-image">
                                                    <i class="fa fa-circle text-xs text-primary"></i>
                                                    Jordan
                                                </a>
                                            </li>
                                            <li class="rightsidebar-contact-wrapper">
                                                <a class="rightsidebar-contact" href="#">
                                                    <img src="<?php echo $base_url; ?>/public/img/authors/avatar2.jpg"
                                                         class="rounded-circle float-right" alt="avatar-image">
                                                    <i class="fa fa-circle text-xs text-primary"></i>
                                                    Stewart
                                                </a>
                                            </li>
                                            <li class="rightsidebar-contact-wrapper">
                                                <a class="rightsidebar-contact" href="#">
                                                    <img src="<?php echo $base_url; ?>/public/img/authors/avatar3.jpg"
                                                         class="rounded-circle float-right" alt="avatar-image">
                                                    <i class="fa fa-circle text-xs text-warning"></i>
                                                    Alfred
                                                </a>
                                            </li>
                                            <li class="rightsidebar-contact-wrapper">
                                                <a class="rightsidebar-contact" href="#">
                                                    <img src="<?php echo $base_url; ?>/public/img/authors/avatar4.jpg"
                                                         class="rounded-circle float-right" alt="avatar-image">
                                                    <i class="fa fa-circle text-xs text-danger"></i>
                                                    Eileen
                                                </a>
                                            </li>
                                            <li class="rightsidebar-contact-wrapper">
                                                <a class="rightsidebar-contact" href="#">
                                                    <img src="<?php echo $base_url; ?>/public/img/authors/avatar5.jpg"
                                                         class="rounded-circle float-right" alt="avatar-image">
                                                    <i class="fa fa-circle text-xs text-muted"></i>
                                                    Robert
                                                </a>
                                            </li>
                                            <li class="rightsidebar-contact-wrapper">
                                                <a class="rightsidebar-contact" href="#">
                                                    <img src="<?php echo $base_url; ?>/public/img/authors/avatar7.jpg"
                                                         class="rounded-circle float-right" alt="avatar-image">
                                                    <i class="fa fa-circle text-xs text-muted"></i>
                                                    Cassandra
                                                </a>
                                            </li>
                                        </ul>

                                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                            <i class="fa fa-fw ti-export"></i>
                                            Recent Updates
                                        </h5>
                                        <div>
                                            <ul class="list-unstyled">
                                                <li class="rightsidebar-notification">
                                                    <a href="#">
                                                        <i class="fa ti-comments-smiley fa-fw text-primary"></i>
                                                        New Comment
                                                    </a>
                                                </li>
                                                <li class="rightsidebar-notification">
                                                    <a href="#">
                                                        <i class="fa ti-twitter-alt fa-fw text-success"></i>
                                                        3 New Followers
                                                    </a>
                                                </li>
                                                <li class="rightsidebar-notification">
                                                    <a href="#">
                                                        <i class="fa ti-email fa-fw text-info"></i>
                                                        Message Sent
                                                    </a>
                                                </li>
                                                <li class="rightsidebar-notification">
                                                    <a href="#">
                                                        <i class="fa ti-write fa-fw text-warning"></i>
                                                        New Task
                                                    </a>
                                                </li>
                                                <li class="rightsidebar-notification">
                                                    <a href="#">
                                                        <i class="fa ti-export fa-fw text-danger"></i>
                                                        Server Rebooted
                                                    </a>
                                                </li>
                                                <li class="rightsidebar-notification">
                                                    <a href="#">
                                                        <i class="fa ti-info-alt fa-fw text-primary"></i>
                                                        Server Not Responding
                                                    </a>
                                                </li>
                                                <li class="rightsidebar-notification">
                                                    <a href="#">
                                                        <i class="fa ti-shopping-cart fa-fw text-success"></i>
                                                        New Order Placed
                                                    </a>
                                                </li>
                                                <li class="rightsidebar-notification">
                                                    <a href="#">
                                                        <i class="fa ti-money fa-fw text-info"></i>
                                                        Payment Received
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="r_tab2">
                                    <div id="slim_t2">
                                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                            <i class="fa fa-fw ti-bell"></i>
                                            Notifications
                                        </h5>
                                        <ul class="list-unstyled m-t-15 notifications">
                                            <li>
                                                <a href="" class="message icon-not striped-col">
                                                    <img class="message-image rounded-circle"
                                                         src="<?php echo $base_url; ?>/public/img/authors/avatar3.jpg" alt="avatar-image">

                                                    <div class="message-body">
                                                        <strong>John Doe</strong>
                                                        <br>
                                                        5 members joined today
                                                        <br>
                                                        <small class="noti-date">Just now</small>
                                                    </div>

                                                </a>
                                            </li>
                                            <li>
                                                <a href="" class="message icon-not">
                                                    <img class="message-image rounded-circle"
                                                         src="<?php echo $base_url; ?>/public/img/authors/avatar.jpg" alt="avatar-image">
                                                    <div class="message-body">
                                                        <strong>Tony</strong>
                                                        <br>
                                                        likes a photo of you
                                                        <br>
                                                        <small class="noti-date">5 min</small>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" class="message icon-not striped-col">
                                                    <img class="message-image rounded-circle"
                                                         src="<?php echo $base_url; ?>/public/img/authors/avatar6.jpg" alt="avatar-image">

                                                    <div class="message-body">
                                                        <strong>John</strong>
                                                        <br>
                                                        Dont forgot to call...
                                                        <br>
                                                        <small class="noti-date">11 min</small>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" class="message icon-not">
                                                    <img class="message-image rounded-circle"
                                                         src="<?php echo $base_url; ?>/public/img/authors/avatar1.jpg" alt="avatar-image">
                                                    <div class="message-body">
                                                        <strong>Jenny Kerry</strong>
                                                        <br>
                                                        Done with it...
                                                        <br>
                                                        <small class="noti-date">1 Hour</small>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" class="message icon-not striped-col">
                                                    <img class="message-image rounded-circle"
                                                         src="<?php echo $base_url; ?>/public/img/authors/avatar7.jpg" alt="avatar-image">

                                                    <div class="message-body">
                                                        <strong>Ernest Kerry</strong>
                                                        <br>
                                                        2 members joined today
                                                        <br>
                                                        <small class="noti-date">3 Days</small>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="text-right noti-footer"><a href="#">View All Notifications <i
                                                            class="ti-arrow-right"></i></a></li>
                                        </ul>
                                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                            <i class="fa fa-fw ti-check-box"></i>
                                            Tasks
                                        </h5>
                                        <ul class="list-unstyled m-t-15">
                                            <li>
                                                <div>
                                                    <p>
                                                        <span>Button Design</span>
                                                        <small class="float-right text-muted">40%</small>
                                                    </p>
                                                    <div class="progress progress-xs  active">
                                                        <div class="progress-bar bg-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                                             style="width: 40%">
                                                            <span class="sr-only">40% Complete (success)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div>
                                                    <p>
                                                        <span>Theme Creation</span>
                                                        <small class="float-right text-muted">20%</small>
                                                    </p>
                                                    <div class="progress progress-xs  active">
                                                        <div class="progress-bar bg-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                                             style="width: 20%">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div>
                                                    <p>
                                                        <span>XYZ Task To Do</span>
                                                        <small class="float-right text-muted">60%</small>
                                                    </p>
                                                    <div class="progress progress-xs  active">
                                                        <div class="progress-bar bg-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                                             style="width: 60%">
                                                            <span class="sr-only">60% Complete (warning)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div>
                                                    <p>
                                                        <span>Transitions Creation</span>
                                                        <small class="float-right text-muted">80%</small>
                                                    </p>
                                                    <div class="progress progress-xs active">
                                                        <div class="progress-bar bg-danger progress-bar-striped" role="progressbar"
                                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                                             style="width: 80%">
                                                            <span class="sr-only">80% Complete (danger)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="text-right"><a href="#">View All Tasks <i
                                                            class="ti-arrow-right"></i></a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="r_tab3">
                                    <div id="slim_t3">

                                        <h5 class="rightsidebar-right-heading text-uppercase gen-sett-m-t text-xs">
                                            <i class="fa fa-fw ti-settings"></i>
                                            General
                                        </h5>
                                        <ul class="list-unstyled settings-list m-t-10">
                                            <li>
                                                <label for="status">Available</label>
                                                <span class="float-right">
                                            <!-- <input type="checkbox" id="status" name="my-checkbox" checked> -->
                                        </span>
                                            </li>
                                            <li>
                                                <label for="email-auth">Login with Email</label>
                                                <span class="float-right">
                                            <!-- <input type="checkbox" id="email-auth" name="my-checkbox"> -->
                                        </span>
                                            </li>
                                            <li>
                                                <label for="update">Auto Update</label>
                                                <span class="float-right">
                                            <!-- <input type="checkbox" id="update" name="my-checkbox"> -->
                                        </span>
                                            </li>

                                        </ul>
                                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                            <i class="fa fa-fw ti-volume"></i>
                                            Sound & Notification
                                        </h5>
                                        <ul class="list-unstyled settings-list m-t-10">
                                            <li>
                                                <label for="chat-sound">Chat Sound</label>
                                                <span class="float-right">
                                            <!-- <input type="checkbox" id="chat-sound" name="my-checkbox" checked> -->
                                        </span>
                                            </li>
                                            <li>
                                                <label for="noti-sound">Notification Sound</label>
                                                <span class="float-right">
                                            <!-- <input type="checkbox" id="noti-sound" name="my-checkbox"> -->
                                        </span>
                                            </li>
                                            <li>
                                                <label for="remain">Remainder </label>
                                                <span class="float-right">
                                            <!-- <input type="checkbox" id="remain" name="my-checkbox" checked> -->
                                        </span>

                                            </li>
                                            <li>
                                                <label for="vol">Volume</label>
                                                <input type="range" id="vol" min="0" max="100" value="15">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#right -->
            <div class="background-overlay"></div>
        </section>
        <!-- /.content --> </aside>
    <!-- /.right-side --> </div>
<!-- ./wrapper -->
<!-- global js -->
<div id="qn"></div>
<script src="<?php echo $base_url; ?>/public/js/app.js" type="text/javascript"></script>
<!-- end of global js -->

<!-- begining of page level js -->

<!--swiper-->
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/alertify.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/swiper/js/swiper.min.js"></script>
<!-- <script src="<?php echo $base_url; ?>/public/js/dashboard2.js" type="text/javascript"></script> -->

<!--chartjs-->
<script src="<?php echo $base_url; ?>/public/vendors/chartjs/js/Chart.js"></script>
<!--nvd3 chart-->
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/nvd3/d3.v3.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/nvd3/js/nv.d3.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/moment/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/advanced_newsTicker/js/newsTicker.js"></script>
<script src="<?php echo $base_url; ?>/public/vendors/iCheck/js/icheck.js"></script>
<script src="<?php echo $base_url; ?>/public/vendors/moment/js/moment.min.js"></script>
<script src="<?php echo $base_url; ?>/public/vendors/select2/js/select2.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/js/custom_js/form_wizards.js" type="text/javascript"></script>
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end of page level js -->
<script src="<?php echo $base_url; ?>/public/vendors/moment/js/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/vendors/timedropper/js/timedropper.js" type="text/javascript"></script>
<!-- date-range-picker -->
<script src="<?php echo $base_url; ?>/public/vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/jquery.mask.min.js"></script>

<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/Buttons/js/buttons.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/laddabootstrap/js/spin.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/laddabootstrap/js/ladda.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/custom_js/button_main.js"></script>
<link rel="stylesheet" href="<?php echo $base_url; ?>/public/Scheduler/dhtmlxscheduler.css" type="text/css"  title="no title" charset="utf-8">
<script src="<?php echo $base_url; ?>/public/Scheduler/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>

<script>
    $(document).ready(function(){
        //alert(sessionStorage.getItem("patientId"));
        //document.getElementById('hdntoggleDX').value = "false";
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        document.getElementById('practiceName1').innerHTML = sessionStorage.getItem('practiceName');
        document.getElementById("lblUser").innerHTML = sessionStorage.getItem('userName');
        var root_url = $("#hidden_root_url").val();
        //alert('i am here layout=>app.blade.php' + root_url);
        $("#linkLogout").click(function(){
            sessionStorage.removeItem('practiceName');
            sessionStorage.removeItem('userID');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('loginName');
        })

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })
    });
</script>

</body>

</html>