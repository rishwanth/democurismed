<!DOCTYPE html>
<html>
<head>
    <title>Login | AMROMED LLC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../public/img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="../public/css/bootstrap.css" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="../public/vendors/themify/css/themify-icons.css" rel="stylesheet"/>
    <link href="../public/vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet"/>
    <link href="../public/css/login.css" rel="stylesheet">
    <style type="text/css">
        body{
            color:#000;
        }
    </style>
    <!--end page level css-->
</head>

<body id="sign-in">
<div class="preloader">
    <div class="loader_img"><img src="../public/img/loader.gif" alt="loading..." height="64" width="64"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-10 col-sm-8 m-auto login-form">

                <h2 class="text-center logo_h2">
                    <img src="../public/img/pages/clear_black.png" alt="Logo">
                </h2>

                <!-- <div class="alert alert-success">
                    <p>
                        User activation successfull. Set password for login
                    </p>
                </div> -->
                <div class="card-body">
                <div class="row">
                <div class="col-12">

                    <form action="<?php echo e(URL::to('/')); ?>/user/password/save" method="POST">
                        <input type="hidden" id="_token" name="_token"
                               value="<?php echo csrf_token(); ?>" />
                        <input type="hidden" id="root_url" name="root_url"
                               value="<?php echo e(URL::to('/')); ?>" />
                        <input type="hidden" id="token" name="token"
                               value="<?php echo e($data['token']); ?>"></input>

                        <?php if (isset($data['message'])) { ?>
                        <div class="form-group">
                            <h1>
                            <?php echo e($data['message']); ?>

                            </h1>
                        </div>
                        <?php } ?>

                        <div class="form-group">
                            Enter password
                            <input type="password" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="password1"
                                   name="password1" placeholder="Enter passowrd">
                        </div>

                        <div class="form-group">
                            Reenter password
                            <input type="password" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="password2"
                                   name="password2" placeholder="Rernter passowrd">
                        </div>

                    <button type="submit" class="btn btn-success">Update</button>

                </form>
        </div>
    </div>
</div>
<!-- global js -->
<script src="../public/js/jquery.min.js" type="text/javascript"></script>

<script src="../public/js/popper.min.js" type="text/javascript"></script>
<script src="../public/js/bootstrap.min.js" type="text/javascript"></script>

<!-- end of global js -->
<!-- page level js -->

<script src="../public/vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../public/js/custom_js/login.js"></script>

<script>

$(document).ready(function(){
});
</script>
</body>
</html>