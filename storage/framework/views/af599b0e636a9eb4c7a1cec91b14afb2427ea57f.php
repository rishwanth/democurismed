<!DOCTYPE html>
<html>
<head>
    <title>Login | AMROMED LLC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="public/img/favicon.ico"/>
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="public/img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="public/css/bootstrap.css" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="vendors/themify/public/css/themify-icons.css" rel="stylesheet"/>
    <link href="vendors/bootstrapvalidator/public/css/bootstrapValidator.min.css" rel="stylesheet"/>
    <link href="public/css/login.css" rel="stylesheet">
    <style type="text/css">
        body{
            color:#000;
        }
    </style>
    
    <!--end page level css-->
</head>

<body style="margin:0; padding:0; background-color:#F9F9F9">
    <div>
        <div>
            <img src="public/img/img3.jpg" style=" width:65%; height:100%; float:left">
        </div>
        <div class="col-lg-4 col-10 col-sm-8 login-form" style="margin:10% auto; float:left">

            <h2 class="text-center logo_h2">
                <img src="public/img/pages/clear_black.png" alt="Logo">
            </h2>

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form id="authentication" method="post" class="login_validator">
                            <div class="form-group">
                                <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtUser" name="username"
                                       placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtPswd"
                                       name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" style="border:1px solid #000">&nbsp; Remember Me
                                </label>
                                <a href="forgot_password.html" style="float:right" id="forgot" class="forgot"> Forgot Password ? </a>
                            </div>
                            <div class="form-group">
                                <!-- <input type="button" value="Sign In" id="btnSignIn" class="btn btn-primary btn-block"/> -->
                                <img src="public/img/pages/arrow-right.png" id="btnSignIn" style="margin:0 50%; background-color: chocolate;border-radius: 15px;" alt="Go" width="30" height="30">
                            </div>
                            

                            <!-- <span class="float-right sign-up">New ? <a href="register.html">Sign Up</a></span> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



<!-- global js -->
<script src="public/js/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
$('#txtPswd').keypress(function(e){

              if(e.keyCode==13)

              $('#btnSignIn').click();

        });

        $('#btnSignIn').click(function(){
            alert('a');

            var username = $('#txtUser').val();

            var password = $('#txtPswd').val();

            $.ajax({

                type: "POST",

                url:"checkLogin.php",

                data:{

                    username : username,

                    password : password,

                },success:function(result){

                    var res = JSON.parse(result);

                    if(res.length != 0){

                        if(res[0].status == "success"){

                            sessionStorage.setItem('userID',res[0].userID);

                            sessionStorage.setItem('loginName',res[0].username);

                            sessionStorage.setItem('userName',res[0].firstName + ' ' +res[0].lastName);

                            sessionStorage.setItem('practiceId', res[0].practiceID);
                            sessionStorage.setItem('practiceName', res[0].practiceName);
                            sessionStorage.setItem('dbName', res[0].dbName);

                            var ID = res[0].userID;

                            $.ajax({

                                type: "POST",

                                async:false,

                                url:"checkSession.php",

                                data:{

                                    ID : ID,

                                },success:function(result){

                                    if(res[0].IsLogged == "0"){

                                        window.location.href = "changePswd.html";

                                    }

                                    else{

                                        window.location.href = "dashboard.php";

                                    }

                                 }

                            });

                        }

                        else if(res[0].status == "failed"){

                            alert("Login Invalid");

                        }

                        else if(res[0].status == "exists"){

                            alert("Username not available");

                        }

                    }

                    else{

                        alert("Username not available");

                    }

                 }

            });

        });
</script>
<!-- end of page level js -->
</body>

</html>

