<!DOCTYPE html>
<html>
<head>
    <title>Login | AMROMED LLC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="public/img/favicon.ico"/>
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="public/img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="<?php echo e(URL::to('/')); ?>/public/css/bootstrap.css" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="<?php echo e(URL::to('/')); ?>/public/vendors/themify/css/themify-icons.css" rel="stylesheet"/>
    <link href="<?php echo e(URL::to('/')); ?>/public/vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet"/>
    <link href="<?php echo e(URL::to('/')); ?>/public/css/login.css" rel="stylesheet">
    <style type="text/css">
        body{
            color:#000;
        }
    </style>
    
    <!--end page level css-->
</head>
<?php 
$message = '';
if (isset($data['message'])) {
    $message = $data['message'];
}
?>
<body style="margin:0; padding:0; background-color:#F9F9F9">
    <div>
        <div>
            <img src="<?php echo e(URL::to('/')); ?>/public/img/img3.jpg" style=" width:65%; height:100%; float:left">
        </div>
        <div class="col-lg-4 col-10 col-sm-8 login-form" style="margin:10% auto; float:left">

            <h2 class="text-center logo_h2">
                <img src="<?php echo e(URL::to('/')); ?>/public/img/pages/clear_black.png" alt="Logo">
            </h2>

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form id="authentication" method="post" class="login_validator">
                            <?php if (!empty($message)) { ?>
                            <div class="form-group">
                                <h3><?php echo e($message); ?></h3>
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <input type="hidden" id="_token" name="_token"
                                       value="<?php echo csrf_token(); ?>">
                                <input type="hidden" id="root_url" name="root_url"
                                       value="<?php echo e(URL::to('/')); ?>/">

                                <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtUser" name="username"
                                       placeholder="Username">
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtPswd"
                                       name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" style="border:1px solid #000">&nbsp; Remember Me
                                </label>
<a href="<?php echo e(URL::to('/')); ?>/forgotpassword" style="float:right" id="forgot" class="forgot"> Forgot Password ? </a>
                            </div>
                            <div class="form-group">
                                <!-- <input type="button" value="Sign In" id="btnSignIn" class="btn btn-primary btn-block"/> -->
                                <img src="<?php echo e(URL::to('/')); ?>/public/img/pages/arrow-right.png" id="btnSignIn" style="margin:0 50%; background-color: chocolate;border-radius: 15px;" alt="Go" width="30" height="30">

            <a  class="btn btn-success" href="<?php echo e(URL::to('register')); ?>">Register</a>

                            </div>
                            

                            <!-- <span class="float-right sign-up">New ? <a href="register.html">Sign Up</a></span> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



<!-- global js -->
<script src="<?php echo e(URL::to('/')); ?>/public/js/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
$('#txtPswd').keypress(function(e){

              if(e.keyCode==13)

              $('#btnSignIn').click();

        });

        $('#btnSignIn').click(function(){
            var root_url = $('#root_url').val();
            var username = $('#txtUser').val();
            var password = $('#txtPswd').val();
            var _token = $('#_token').val();
            //var url = root_url + 'login';
            $.ajax({
                type: "POST",
                url: root_url + 'login',
                data:{
                    username : username,
                    password : password,
                    _token : _token,
                },success:function(result){
                    console.log(result);
                    var json = JSON.parse(result);
                    if (json.status == 1){
                        alert(json.message);
                        var url = root_url + 'patient/view';
                        window.location.href = url;
                    } else {
                        alert(json.message);
                    }
                }
            });
        });
</script>
</body>
</html>

