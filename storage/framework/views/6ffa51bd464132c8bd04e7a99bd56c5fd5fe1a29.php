<?php $__env->startSection('content'); ?>
<?php
    $base_url = URL::to('/');
?>

<div class="row" style="margin: 5%;">
    <div class="bs-example" style="width: 60%; ">
        <ul class="nav nav-tabs" style="margin-bottom: 15px;">
            <li class="nav-item">
                <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Add Users</a>
            </li>
            <li class="nav-item">
                <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Add Access to Users</a>
            </li>
            <!-- <li class="nav-item">
        <a href="#tab_3_7" style="color:#000;" data-toggle="tab" class="nav-link">Log</a>
    </li> -->
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane active" id="tab_3_1">
                <div class="col-12">
                    <div class="card-body">
                        <div class="col-12">
                        <form id="authentication" method="post" class="login_validator">
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">First Name</label>
                                </div>
                                <div class="col-8">
                                    <input type="hidden" id="_token" name="_token"
                                  value="<?php echo csrf_token(); ?>" />
                                    <input type="hidden" id="root_url" name="v"
                                  value="<?php echo e(URL::to('/')); ?>" />
                                  

                                     <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtFirstname" name="username" placeholder="First Name">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Last Name</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtLastname"
                                       name="password" placeholder="Last Name">
                                </div>
                            </div>


                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Username</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtUsername" name="username"
                                       placeholder="Username">
                                </div>
                            </div>


                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Role</label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" id="ddlRole" style="border-radius:15px; height:2.2rem">
                                        <option value="">--Select Role--</option>
                                        <option value="Admin">Admin</option>
                                        <option value="User">User</option>
                                        <option value="Manager">Manager</option>
                                        <option value="Provider">Provider</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Select Practice</label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" id="practiceList" style="border-radius:15px; height:2.2rem">
                                    </select>
                                </div>
                            </div>
                             



                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Email ID</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtEmailID" name="username"
                                       placeholder="Email ID">
                                </div>
                            </div>




                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Mobile</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtMob" name="username"
                                       placeholder="Mobile #">
                                </div>
                            </div>


                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-12">
                                    <img src="../public/img/pages/arrow-right.png" id="btnAddUser" style="margin:0 50%; background-color: chocolate;border-radius: 15px;" alt="Go" width="30" height="30">
                                </div>
                            </div>
                            

                            <!-- <span class="float-right sign-up">New ? <a href="register.html">Sign Up</a></span> -->
                        </form>
                    </div>
                        <div class="col-sm-12">
                            <table id="getActivities" class="table table-bordered"></table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab_3_2">
                <div class="col-12">
                    <div class="card-body">
                        <div class="col-sm-12">
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-2">
                                    <label for="input-text" class="control-label float-right txt_media1">Activity Name</label>
                                </div>
                                <div class="col-8">
                                    <select id="ddlActivity" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <div class="col-2">
                                    <label for='dup' class="m-t-b-15">CPT Codes: </label>
                                </div>
                                <div class="col-8">
                                    <div class='input'>
                                        <input id='dup' value='' placeholder="type & hit space to create codes" style="min-width: 200px;">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <input type="button" class="btn btn-primary" value="Add Values" id="btnGetValues"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="codesList"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){
        //document.getElementById("lblUser").innerHTML = <?php echo e(Session::get('LOGGED_USER_NAME')); ?>;
        var root_url = $("#hidden_root_url").val();
        //alert('i am here layout=>app.blade.php' + root_url);
        $("#linkLogout").click(function(){
            sessionStorage.removeItem('practiceName');
            sessionStorage.removeItem('userID');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('loginName');
        })

        $.get(root_url+"practices/listforregister", function(data, status){
        var data = JSON.parse(data).data;
        var list = [];
        for(var x in data){
          list.push(data[x]);
        }
        $.each(list,function(i,v) {
            $('#practiceList').html('<option value="">--Select Practice--</option>');
            list.forEach(function(t) { 
                $('#practiceList').append('<option value="'+t.id+'">'+t.name+'</option>');
            });
        });
    });

    $('#btnAddUser').click(function(){
        var _token = $('#_token').val();
        var firstName = $("#txtFirstname").val();
        var lastName = $('#txtLastname').val();
        var userName = $('#txtUsername').val();
        var role = $('#ddlRole').val();
        var practiceID = $('#practiceList').val();
        var emailID = $('#txtEmailID').val();
        var mobileNo = $('#txtMob').val();
        $(".preloader").show();
        $.ajax({
            type: "POST",
            url:"<?php echo e(URL::to('register/update')); ?>",
            data:{ 
                "_token" : _token,
                "firstName" : firstName,
                "lastName" : lastName,
                "userName" : userName,
                "role" : role,
                "emailID" : emailID,
                "mobileNo" : mobileNo,
                "practiceID" : practiceID,
                },success:function(result){
                    var json =JSON.parse(result);
                    var root_url = $('#root_url').val();
                    if (json.status == 0){
                        alert(json.message);
                    } else {
                        $(".preloader").hide();
                        alert(json.message);
                        window.location.href = root_url;
                    }
                 }
        });
    });

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })
    });
</script>

<?php echo $__env->make('layout.handler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>