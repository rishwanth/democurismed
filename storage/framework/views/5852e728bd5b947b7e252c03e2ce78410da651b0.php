<?php $__env->startSection('content'); ?>

        <div class="alert alert-warning fade in" style="margin-top:10px;" id="divAlert">
            <i class="icon-remove close" data-dismiss="alert"></i>
            <strong>Warning!</strong> <span id="alertTop">Your Payment Due is on 05/22/2016</span>
        </div>
        <!-- /Breadcrumbs line -->

        <div id="content">
            
            <h3 style="margin:0px 20px 20px 20px;">Payment</h3>
            <div class="card-body">
                <div class="row">
                    <input type="hidden" id="_hdnRetAllow"/>
                    <input type="hidden" id="_hdnRetPaid"/>
                    <input type="hidden" id="_hdnRetAdjust"/>
                    <input type="hidden" id="_hdnRetBal"/>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-12">Select Client</label>
                        <div class="col-md-10">
                            <select id="patient" class="form-control">
                                <option value="0"></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-12">From Date</label>
                        <div class="col-md-10">
                            <input type="text" id="fromDepDt" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-12">To Date</label>
                        <div class="col-md-10">
                            <input type="text" id="toDepDt" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-md-4" style="margin-bottom:20px;">
                        <label class="control-label col-md-2"></label>
                        <div class="col-md-6">
                            <input type="checkbox" class="checkbox"/> All Clients
                        </div>
                    </div>
                    <div class="form-group col-md-4" style="margin-bottom:20px;">
                        <label class="control-label col-md-2"></label>
                        <div class="col-md-6">
                            <input type="checkbox" class="checkbox" id="chkClosed"/> Include Closed
                        </div>
                    </div>
                    <div class="form-group col-md-4" style="margin-bottom:20px;margin-top:20px;">
                        <div class="col-md-12">
                            <input type="button" class="btn btn-primary" value="Get Claims" id="btnGetDeposit">
                            <input type="button" class="btn btn-primary" value="Import ERA" id="btnGetERA">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="form-group col-md-4">
                        <span class="col-md-6"><strong>Total Amount</strong></span>
                        <span class="col-md-6" id="spanTotAmt"></span>
                    </div>

                    <div class="form-group col-md-4">
                        <span class="col-md-6"><strong>Amount Applied</strong></span>
                        <span class="col-md-6" id="spanAmtApp"></span>
                    </div>

                    <div class="form-group col-md-4">
                        <span class="col-md-6"><strong>Amount Remaining</strong></span>
                        <span class="col-md-6" id="spanAmtRem"></span>
                    </div>
                </div>
            </div>
            
            <table class="table table-bordered table table-hover table-striped" style="margin-top:70px;">
                <thead id="depHead">
                </thead>
                <tbody id="depBody">
                </tbody>
            </table>
            <a href="javascript:void(0)" id="bulkDiv"><input type="button" value="Save all" id="hitBulk" class="btn btn-primary" />
            <a href="<?php echo e(URL::to('/deposits')); ?>"><button class="btn btn-primary pull-right">Back to Deposit</button></a>
        </div>
    </div>
<div class="modal fade" id="Status" tabindex="-1">
    <div class="modal-dialog" style="width:30%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Deposit</h4>
            </div>
            <div class="modal-body" style="min-height:180px;">
                <div class="form-group col-md-12">
                    <label class="control-label col-md-12">Copay :</label>
                    <div class="col-md-10" id="copay">
                        <input type="text" class="form-control copay" />
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="control-label col-md-12">Co-Ins :</label>
                    <div class="col-md-10" id="coins">
                        <input type="text" class="form-control coins" />
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="control-label col-md-12">Deductible :</label>
                    <div class="col-md-10" id="deduc">
                        <input type="text" class="form-control deduc" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" id="btnSaveStatus" class="btn btn-primary" data-dismiss="modal" value="Save" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalERA" tabindex="-1">
    <div class="modal-dialog" style="width:30%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Upload 835 File</h4>
            </div>
            <div class="modal-body" style="min-height:180px;">
                <form id="upload" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">835 File :</label>
                        <div class="col-md-10">
                            <input id="filename" type="file" name="filename" />
                        </div>
                    </div>
                    <input class="btn btn-success" type="submit" id="upload" value="Upload">
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSaveStatus" class="btn btn-primary" data-dismiss="modal" value="Save" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function(){
        var root_url = $("#hidden_root_url").val();
        $(".ti-menu-alt").click();
        var depID = sessionStorage.getItem("depID");
        var depAmt = sessionStorage.getItem("depAmt");
        var depPayor = sessionStorage.getItem("depPayor");
        var depPayorType = sessionStorage.getItem("depPayorType");
        //$("#bulkDiv").hide();
        $('#divAlert').hide();

        $('#fromDepDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#fromDepDt').mask('00-00-0000');

        $('#toDepDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#toDepDt').mask('00-00-0000');

        $("#btnGetERA").click(function(){
            $("#modalERA").modal("show");
        });
        $("#upload").submit(function (event) {
            event.preventDefault();
            $.ajax({
                 type: "post",
                 url: "http://localhost/democurismed/deposits/geterainfofromfile",
                 data: new FormData($(this)[0]),
                 processData: false,
                 contentType: false,
                 cache: false,
                 success: function (res) {
                    //console.log(res);
                    var data = JSON.parse(res).data;
                    $("#modalERA").modal("hide");
                    var arrDep = [];
                    $('#depBody').html('');
                    for(var x in data){
                        arrDep.push(data[x]);
                    }

                    $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:7%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:6%;">Code</th><th style="background-color:#2E576B; color:#fff; width:6%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:5%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:7%;">Allowed</th><th style="background-color:#2E576B; color:#fff; width:7%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Adjustment</th><th style="background-color:#2E576B; color:#fff; width:10%;">Insurance Balance</th><th style="background-color:#2E576B; color:#fff; width:10%;">Patient Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                    $('#depBody').html('');
                    arrDep.forEach(function(t) {
                        var clsName = "";
                        if(t.isPosted == '1'){
                            clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                        }
                        else{
                            clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                        }
                        if(t.claimBalance == "0"){
                            balanceValue = t.claimBalance;
                        }
                        else{
                            balanceValue = t.claimBalance;
                        }
                        if(t.claimBalancePat == "0.00"){
                            balanceValuePat = t.claimBalancePat;
                        }
                        else{
                            balanceValuePat = t.claimBalancePat;
                        }

                        var billed = t.total;


                        if((t.allowed == "" && t.adjustment == "" && t.paid == "") ||( t.allowed == "0.00" && t.adjustment == "0.00" && t.paid == "0.00")){
                            $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2"  id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3"  id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                        }
                    });

                }
            });
        });

        $("#chkClosed").change(function(){
            var currVal = $("#patient").val();
            if(depPayorType == "1"){
                $.post(root_url + "claims/ledger",
                    {
                        patientID: currVal,
                    },
                    function(data, status){
                        var arrDep = [];
                        $('#depBody').html('');
                        for(var x in data){
                            arrDep.push(data[x]);
                        }
                        //alert(arrCase);
                        //$.each(arrDep,function(i,v) {
                        $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Allowed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:9%;">Adjustment</th><th style="background-color:#2E576B; color:#fff; width:7%;">Insurance Balance</th><th style="background-color:#2E576B; color:#fff; width:7%;">Patient Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                        $('#depBody').html('');
                        arrDep.forEach(function(t) {
                            if($('#chkClosed').is(':checked') == false){
                                if(t.status != '3'){
                                    var clsName = "";
                                    if(t.isPosted == '1'){
                                        clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    else{
                                        clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    if(t.claimBalance == ""){
                                        balanceValue = t.total;
                                    }
                                    else{
                                        balanceValue = t.claimBalance;
                                    }

                                    var billed = t.total;


                                    if((t.allowed == "" && t.adjustment == "" && t.paid == "") ||( t.allowed == "0.00" && t.adjustment == "0.00" && t.paid == "0.00")){
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2"  id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3"  id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }
                                    else{
                                        if(balanceValue <=0){
                                            $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                        }
                                        else{
                                            $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                        }
                                    }

                                    var status = "ddlStatus"+t.claimID;
                                    if(t.status != ""){
                                        document.getElementById(status).value = t.status;
                                    }
                                    // $.ajax({
                                    //       type: "POST",
                                    //       url: root_url+"policies/check",
                                    //       async : false,
                                    //       data:{
                                    //         caseID : t.caseID
                                    //       },success:function(result){
                                    //         var status = "#ddlStatus"+t.claimID;
                                    //         if(result == "Secondary"){
                                    //             $(status).val("2");
                                    //         }
                                    //         else
                                    //         {
                                    //             if(balanceValue > 0){
                                    //                 $(status).val("1");
                                    //             }
                                    //             else{
                                    //                 $(status).val("3");
                                    //             }
                                    //         }
                                    //       }
                                    //   });
                                }
                            }
                            else{
                                var clsName = "";
                                if(t.isPosted == '1'){
                                    clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                }
                                else{
                                    clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                }
                                if(t.claimBalance == ""){
                                    balanceValue = t.total;
                                }
                                else{
                                    balanceValue = t.claimBalance;
                                }

                                var billed = t.total;


                                if((t.allowed == "" && t.adjustment == "" && t.paid == "") ||( t.allowed == "0.00" && t.adjustment == "0.00" && t.paid == "0.00")){
                                    $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2"  id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3"  id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                }
                                else{
                                    if(balanceValue <=0){
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }
                                    else{
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }
                                }

                                var status = "ddlStatus"+t.claimID;
                                if(t.status != ""){
                                    document.getElementById(status).value = t.status;
                                }
                                // $.ajax({
                                //       type: "POST",
                                //       url: root_url+"policies/check",
                                //       async : false,
                                //       data:{
                                //         caseID : t.caseID
                                //       },success:function(result){
                                //         var status = "#ddlStatus"+t.claimID;
                                //         if(result == "Secondary"){
                                //             $(status).val("2");
                                //         }
                                //         else
                                //         {
                                //             if(balanceValue > 0){
                                //                 $(status).val("1");
                                //             }
                                //             else{
                                //                 $(status).val("3");
                                //             }
                                //         }
                                //       }
                                //   });
                            }

                        });
                        //});
                    });
            }
            else{
                $.post(root_url + "claims/patientledger",
                    {
                        patientID: currVal,
                    },
                    function(data, status){
                        var arrDep = [];
                        $('#depBody').html('');
                        for(var x in data){
                            arrDep.push(data[x]);
                        }
                        //alert(arrCase);
                        //$.each(arrDep,function(i,v) {
                        $('#depHead').html('');
                        $('#depBody').html('');
                        arrDep.forEach(function(t) {
                            if($('#chkClosed').is(':checked') == false){
                                if(t.status != '3'){
                                    var clsName = "";
                                    if(t.isPosted == '1'){
                                        clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    else{
                                        clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    if(t.claimBalance == ""){
                                        balanceValue = t.total;
                                    }
                                    else{
                                        balanceValue = t.claimBalance;
                                    }

                                    var billed = t.total;

                                    if(balanceValue <=0){
                                        $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Patient Pay</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" disabled style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><input type="text" class="form-control textbox5" id="txtDepPatPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value=""/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }
                                    else{
                                        $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Patient Pay</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" disabled style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><input type="text" class="form-control textbox5" id="txtDepPatPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value=""/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }

                                    var status = "ddlStatus"+t.claimID;
                                    if(t.status != ""){
                                        document.getElementById(status).value = t.status;
                                    }
                                    // $.ajax({
                                    //       type: "POST",
                                    //       url: root_url+"policies/check",
                                    //       async : false,
                                    //       data:{
                                    //         caseID : t.caseID
                                    //       },success:function(result){
                                    //         var status = "#ddlStatus"+t.claimID;
                                    //         if(result == "Secondary"){
                                    //             $(status).val("2");
                                    //         }
                                    //         else
                                    //         {
                                    //             if(balanceValue > 0){
                                    //                 $(status).val("1");
                                    //             }
                                    //             else{
                                    //                 $(status).val("3");
                                    //             }
                                    //         }
                                    //       }
                                    // });
                                }
                                else{
                                    var clsName = "";
                                    if(t.isPosted == '1'){
                                        clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    else{
                                        clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    if(t.claimBalance == ""){
                                        balanceValue = t.total;
                                    }
                                    else{
                                        balanceValue = t.claimBalance;
                                    }

                                    var billed = t.total;

                                    if(balanceValue <=0){
                                        $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Patient Pay</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" disabled style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><input type="text" class="form-control textbox5" id="txtDepPatPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value=""/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }
                                    else{
                                        $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Patient Pay</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" disabled style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><input type="text" class="form-control textbox5" id="txtDepPatPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value=""/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }

                                    var status = "ddlStatus"+t.claimID;
                                    if(t.status != ""){
                                        document.getElementById(status).value = t.status;
                                    }
                                    // $.ajax({
                                    //       type: "POST",
                                    //       url: root_url+"policies/check",
                                    //       async : false,
                                    //       data:{
                                    //         caseID : t.caseID
                                    //       },success:function(result){
                                    //         var status = "#ddlStatus"+t.claimID;
                                    //         if(result == "Secondary"){
                                    //             $(status).val("2");
                                    //         }
                                    //         else
                                    //         {
                                    //             if(balanceValue > 0){
                                    //                 $(status).val("1");
                                    //             }
                                    //             else{
                                    //                 $(status).val("3");
                                    //             }
                                    //         }
                                    //       }
                                    // });
                                }
                            }
                        });
                        //});
                    });
            }
        })

        sessionStorage.removeItem("txtboxPaid");
        if(depPayorType == '1'){
            $.post(root_url + "insurances/listpatients",
                {
                    insuranceID: depPayor,
                },
                function(data, status){
                    var res = JSON.parse(data);
                    if(res.status == 0){
                        new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                        return;
                    }
                    var data = res.data;
                    var arrDep = [];
                    $('#patient').html('');
                    for(var x in data){
                        arrDep.push(data[x]);
                    }
                    //alert(arrCase);
                    $.each(arrDep,function(i,v) {
                        $('#patient').html('');
                        $('#patient').append('<option value=""></option>');
                        arrDep.forEach(function(t) {
                            $('#patient').append('<option value="'+t.patientID+'">'+t.fullName+'</option>');
                        });
                    });
                });
        }
        else{
            var payerName = "";
            $.ajax({
                type: "POST",
                url: root_url+"patient/getpatname",
                async : false,
                data:{
                    patientID : depPayor
                },success:function(result){
                    var res = JSON.parse(result);
                    payerName = res[0].fullName;
                }
            });
            $('#patient').html('');
            $('#patient').append('<option value=""></option>');
            $('#patient').append('<option value="'+depPayor+'">'+payerName+'</option>');
        }

        var temp = sessionStorage.getItem("patientId");
        document.getElementById('spanTotAmt').innerHTML = toFixed(depAmt,2);
        $.post(root_url + "deposits/findbalance",
            {
                depositID: depID,
            },
            function(data, status){
                var res = JSON.parse(data);
                if(res.status == 0){
                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                    return;
                }
                var data = res.data.balance;
                document.getElementById('spanAmtRem').innerHTML = toFixed(data,2);
                var tempSpanTotAmt = document.getElementById('spanTotAmt').innerHTML - document.getElementById('spanAmtRem').innerHTML;
                document.getElementById('spanAmtApp').innerHTML = toFixed(tempSpanTotAmt,2);
            });
        $('#btnGetDeposit').click(function(){
            var currVal = $('#patient').val();
            var fromDepDt = changeDate($('#fromDepDt').val());
            var toDepDt = changeDate($('#toDepDt').val());
            // $.ajax({
            //     type: "POST",
            //     url:"getAlerts.php",
            //     async : false,
            //     data:{
            //         alert_patientID : currVal
            //     },success:function(result){
            //         var res = JSON.parse(result);
            //         if(res!=""){

            //             if(res[0].alerts_Deposits == "1"){
            //                 $('#divAlert').show();
            //                 document.getElementById("alertTop").innerHTML = res[0].alerts_Deposits1;
            //             }
            //             else{
            //                 $('#divAlert').hide();
            //             }
            //         }
            //     }
            // });
            if(depPayorType == "1"){
                $.post(root_url + "claims/listdeposit",
                    {
                        patientID: currVal,
                        fromDt:fromDepDt,
                        toDt:toDepDt
                    },
                    function(data, status){
                        var res = JSON.parse(data);
                        if(res.status == 0){
                            new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                            return;
                        }
                        var data = res.data;
                        var arrDep = [];
                        $('#depBody').html('');
                        for(var x in data){
                            arrDep.push(data[x]);
                        }
                        //alert(arrCase);
                        //$.each(arrDep,function(i,v) {
                        $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:7%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:6%;">Code</th><th style="background-color:#2E576B; color:#fff; width:6%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:5%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:7%;">Allowed</th><th style="background-color:#2E576B; color:#fff; width:7%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Adjustment</th><th style="background-color:#2E576B; color:#fff; width:10%;">Insurance Balance</th><th style="background-color:#2E576B; color:#fff; width:10%;">Patient Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                        $('#depBody').html('');
                        arrDep.forEach(function(t) {
                            if($('#chkClosed').is(':checked') == false){
                                if(t.status != '3'){
                                    var clsName = "";
                                    if(t.isPosted == '1'){
                                        clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    else{
                                        clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    if(t.claimBalance == "0"){
                                        balanceValue = t.claimBalance;
                                    }
                                    else{
                                        balanceValue = t.claimBalance;
                                    }
                                    if(t.claimBalancePat == "0.00"){
                                        balanceValuePat = t.claimBalancePat;
                                    }
                                    else{
                                        balanceValuePat = t.claimBalancePat;
                                    }

                                    var billed = t.total;


                                    if((t.allowed == "" && t.adjustment == "" && t.paid == "") ||( t.allowed == "0.00" && t.adjustment == "0.00" && t.paid == "0.00")){
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2"  id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3"  id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }
                                    else{
                                        if(balanceValue <=0){
                                            $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                        }
                                        else{
                                            $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                        }
                                    }

                                    var status = "ddlStatus"+t.claimID;
                                    if(t.status != ""){
                                        document.getElementById(status).value = t.status;
                                    }
                                }
                            }
                            // else{
                            //     var clsName = "";
                            //          if(t.isPosted == '1'){
                            //             clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                            //          }
                            //          else{
                            //             clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                            //          }
                            //          if(t.claimBalance == ""){
                            //             balanceValue = t.total;
                            //          }
                            //          else{
                            //             balanceValue = t.claimBalance;
                            //          }

                            //          var billed = t.total;


                            //          if((t.allowed == "" && t.adjustment == "" && t.paid == "") ||( t.allowed == "0.00" && t.adjustment == "0.00" && t.paid == "0.00")){
                            //             $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2"  id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3"  id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                            //          }
                            //          else{
                            //             if(balanceValue <=0){
                            //                 $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                            //             }
                            //             else{
                            //                 $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                            //             }
                            //         }

                            //         var status = "ddlStatus"+t.claimID;
                            //         if(t.status != ""){
                            //             document.getElementById(status).value = t.status;
                            //         }
                            //         $.ajax({
                            //               type: "POST",
                            //               url: root_url+"policies/check",
                            //               async : false,
                            //               data:{
                            //                 caseID : t.caseID
                            //               },success:function(result){
                            //                 var status = "#ddlStatus"+t.claimID;
                            //                 if(result == "Secondary"){
                            //                     $(status).val("2");
                            //                 }
                            //                 else
                            //                 {
                            //                     if(balanceValue > 0){
                            //                         $(status).val("1");
                            //                     }
                            //                     else{
                            //                         $(status).val("3");
                            //                     }
                            //                 }
                            //               }
                            //           });
                            // }

                        });
                        //});
                    });
            }
            else{
                $.post(root_url + "claims/patientledger",
                    {
                        patientID: currVal,
                    },
                    function(data, status){
                        var arrDep = [];
                        $('#depBody').html('');
                        for(var x in data){
                            arrDep.push(data[x]);
                        }
                        //alert(arrCase);
                        //$.each(arrDep,function(i,v) {
                        $('#depHead').html('');
                        $('#depBody').html('');
                        arrDep.forEach(function(t) {
                            if($('#chkClosed').is(':checked') == false){
                                if(t.status != '3'){
                                    var clsName = "";
                                    if(t.isPosted == '1'){
                                        clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    else{
                                        clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                    }
                                    if(t.claimBalance == "0.00"){
                                        balanceValue = t.claimBalance;
                                    }
                                    else{
                                        balanceValue = t.claimBalance;
                                    }

                                    if(t.claimBalancePat == "0.00"){
                                        balanceValuePat = t.claimBalancePat;
                                    }
                                    else{
                                        balanceValuePat = t.claimBalancePat;
                                    }

                                    var billed = t.total;


                                    // if((t.allowed == "" && t.adjustment == "" && t.paid == "") ||( t.allowed == "0.00" && t.adjustment == "0.00" && t.paid == "0.00")){
                                    //    $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                    //    $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    // }
                                    // else{
                                    if(balanceValue <=0){
                                        // $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Insurance Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Patient Pay</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                        // $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" disabled style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td id="txtBalancePat'+t.claimID+'">'+balanceValue+'</td><td><input type="text" class="form-control textbox5" id="txtDepPatPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value=""/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                        $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:11%;">Copay</th><th style="background-color:#2E576B; color:#fff; width:11%;">Coins</th><th style="background-color:#2E576B; color:#fff; width:11%;">Deduc</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:10%;">Status</th>');
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control copaytext" id="txtDepPatcopay'+t.claimID+'" style="width:60px; border-radius:3px; float:left;" value=""/><span id="txtoldPatcopay'+t.claimID+'" style="float:left; margin-left:10px;">'+t.copay+'</span></td><td><input type="text" class="form-control coinstext" id="txtDepPatcoins'+t.claimID+'" style="width:60px; border-radius:3px; float:left;" value=""/><span id="txtoldPatcoins'+t.claimID+'" style="float:left; margin-left:10px;">'+t.coins+'</span></td><td><input type="text" class="form-control deductext" id="txtDepPatdeduc'+t.claimID+'" style="width:60px; border-radius:3px; float:left;" value=""/><span id="txtoldPatdeduc'+t.claimID+'" style="float:left; margin-left:10px;">'+t.deductible+'</span></td><td id="txtBalancePat'+t.claimID+'">'+balanceValuePat+'</td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }
                                    else{
                                        $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:11%;">Copay</th><th style="background-color:#2E576B; color:#fff; width:11%;">Coins</th><th style="background-color:#2E576B; color:#fff; width:11%;">Deduc</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:10%;">Status</th>');
                                        $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control copaytext" id="txtDepPatcopay'+t.claimID+'" style="width:60px; border-radius:3px; float:left;" value=""/><span id="txtoldPatcopay'+t.claimID+'" style="float:left; margin-left:10px;">'+t.copay+'</span></td><td><input type="text" class="form-control coinstext" id="txtDepPatcoins'+t.claimID+'" style="width:60px; border-radius:3px; float:left;" value=""/><span id="txtoldPatcoins'+t.claimID+'" style="float:left; margin-left:10px;">'+t.coins+'</span></td><td><input type="text" class="form-control deductext" id="txtDepPatdeduc'+t.claimID+'" style="width:60px; border-radius:3px; float:left;" value=""/><span id="txtoldPatdeduc'+t.claimID+'" style="float:left; margin-left:10px;">'+t.deductible+'</span></td><td id="txtBalancePat'+t.claimID+'">'+balanceValuePat+'</td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                    }

                                    //}

                                    var status = "ddlStatus"+t.claimID;
                                    if(t.status != ""){
                                        document.getElementById(status).value = t.status;
                                    }
                                    // $.ajax({
                                    //       type: "POST",
                                    //       url: root_url+"policies/check",
                                    //       async : false,
                                    //       data:{
                                    //         caseID : t.caseID
                                    //       },success:function(result){
                                    //         var status = "#ddlStatus"+t.claimID;
                                    //         if(result == "Secondary"){
                                    //             $(status).val("2");
                                    //         }
                                    //         else
                                    //         {
                                    //             if(balanceValue > 0){
                                    //                 $(status).val("1");
                                    //             }
                                    //             else{
                                    //                 $(status).val("3");
                                    //             }
                                    //         }
                                    //       }
                                    // });
                                }
                                // else{
                                //     var clsName = "";
                                //      if(t.isPosted == '1'){
                                //         clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                //      }
                                //      else{
                                //         clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t.claimID+'" /></td>';
                                //      }
                                //      if(t.claimBalance == ""){
                                //         balanceValue = t.total;
                                //      }
                                //      else{
                                //         balanceValue = t.claimBalance;
                                //      }

                                //      var billed = t.total;


                                //      // if((t.allowed == "" && t.adjustment == "" && t.paid == "") ||( t.allowed == "0.00" && t.adjustment == "0.00" && t.paid == "0.00")){
                                //      //    $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                //      //    $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                //      // }
                                //      // else{
                                //         if(balanceValue <=0){
                                //             $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Patient Pay</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                //             $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" disabled style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><input type="text" class="form-control textbox5" id="txtDepPatPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value=""/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                //         }
                                //         else{
                                //             $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:9%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:7%;">Code</th><th style="background-color:#2E576B; color:#fff; width:7%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:7%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:9%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Patient Pay</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                                //             $('#depBody').append('<tr>'+clsName+'<td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t.claimID+'" disabled style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><input type="text" class="form-control textbox5" id="txtDepPatPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value=""/></td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                //         }

                                //     //}

                                //     var status = "ddlStatus"+t.claimID;
                                //     if(t.status != ""){
                                //         document.getElementById(status).value = t.status;
                                //     }
                                //     $.ajax({
                                //           type: "POST",
                                //           url: root_url+"policies/check",
                                //           async : false,
                                //           data:{
                                //             caseID : t.caseID
                                //           },success:function(result){
                                //             var status = "#ddlStatus"+t.claimID;
                                //             if(result == "Secondary"){
                                //                 $(status).val("2");
                                //             }
                                //             else
                                //             {
                                //                 if(balanceValue > 0){
                                //                     $(status).val("1");
                                //                 }
                                //                 else{
                                //                     $(status).val("3");
                                //                 }
                                //             }
                                //           }
                                //     });
                                // }
                            }

                        });
                        //});
                    });
            }
        });

        $(document).on('click','#hitBulk',function() {
            var searchIDs = $('input:checked').map(function(){
                return $(this).attr('class').split(" ")[1];
            });
            for(var i=0;i<searchIDs.length;i++){
                if(depPayorType == "1"){
                    var depID = sessionStorage.getItem("depID");
                    var id = searchIDs[i];
                    var final = id.replace("chkBulk","");
                    var copayVal = "txtcopay"+final;
                    var deducVal = "txtdeduc"+final;
                    var coinsVal = "txtcoins"+final;
                    var allowed = "txtDepAllowed"+final;
                    var paid = "txtDepPayment"+final;
                    var adjust = "txtDepAdjust"+final;
                    var bal = "txtBalancePat"+final;
                    var balIns = "txtBalanceIns"+final;
                    var reason = "ddlreason"+final;
                    var status = "ddlStatus"+final;
                    var copay = sessionStorage.getItem(copayVal);
                    var deduc = sessionStorage.getItem(deducVal);
                    var coins = sessionStorage.getItem(coinsVal);
                    var allowedVal = document.getElementById(allowed).value;
                    var paidVal = document.getElementById(paid).value;
                    var adjustVal = document.getElementById(adjust).value;
                    var balValPat = document.getElementById(bal).value;
                    var balValIns = document.getElementById(balIns).value;
                    var reasonVal = document.getElementById(reason).value;
                    var statusVal = document.getElementById(status).value;
                    //if(statusVal =="1"){
                    $.ajax({
                        type: "POST",
                        url:root_url + "deposits/adjustdeposit",
                        data:{
                            "isPosted" : "1",
                            "depositID" : depID,
                            "claimID" : final,
                            "patientID" : "-1",
                            "insuranceID" : "-100",
                            "amountAdjusted" : paidVal,
                            "copay" : copay,
                            "deductible" : deduc,
                            "coins" : coins,
                            "allowed" : allowedVal,
                            "adjustmentNotes" : statusVal,
                            "adjustmentCode" : reasonVal,
                            "paid" : paidVal,
                            "adjustment" : adjustVal,
                            "claimBalancePat" : balValPat,
                            "claimBalanceIns" : balValIns,
                            "reason" : reasonVal,
                            "status" : statusVal
                        },success:function(result){
                            //window.location.href = "Deposit.php";
                            var json = JSON.parse(result);
                            new PNotify({
                                type:'success',
                                text: "Deposit Posted successfully",
                                after_init: function(notice){
                                    notice.attention('rubberBand');
                                }
                            })
                            sessionStorage.removeItem("txtboxPaid");
                        }

                    });
                    //}
                }
                else{
                    var depID = sessionStorage.getItem("depID");
                    var id = searchIDs[i];
                    var final = id.replace("chkBulk","");
                    var reason = "ddlreason"+final;
                    var status = "ddlStatus"+final;
                    var bal = "txtBalancePat"+final;
                    //var balIns = "txtBalanceIns"+final;
                    var paid = "txtDepPatPayment"+final;

                    var copayVal = "txtDepPatcopay"+final;
                    var deducVal = "txtDepPatdeduc"+final;
                    var coinsVal = "txtDepPatcoins"+final;

                    var reasonVal = document.getElementById(reason).value;
                    var statusVal = document.getElementById(status).value;
                    var paidVal = document.getElementById(paid).value;
                    var patRem = document.getElementById("spanAmtRem").innerHTML;
                    var balVal = document.getElementById(bal).value;

                    var copay = document.getElementById(copayVal).value;
                    var deduc = document.getElementById(deducVal).value;
                    var coins = document.getElementById(coinsVal).value;

                    //var balValIns = document.getElementById(balIns).value;
                    if(parseInt(balVal) <= parseInt(patRem)){
                        patRem = parseInt(patRem) - parseInt(balVal);
                        //if(statusVal == "3"){
                        $.ajax({
                            type: "POST",
                            url:root_url + "deposits/adjustPat",
                            data:{
                                "isPosted" : "1",
                                "depositID" : depID,
                                "claimID" : final,
                                "patientID" : "-1",
                                "adjustment" : "0",
                                "insuranceID" : "-100",
                                "copay" : copay,
                                "deductible" : deduc,
                                "coins" : coins,
                                "claimBalancePat" : balVal,
                                //"claimBalanceIns" : balVal,
                                "adjustmentNotes" : statusVal,
                                "adjustmentCode" : reasonVal,
                                "reason" : reasonVal,
                                "paid" : paidVal,
                                "amountAdjusted" : paidVal,
                                "status" : statusVal
                            },success:function(result){
                                //window.location.href = "Deposit.php";
                                new PNotify({
                                    type:'success',
                                    text: json.message,
                                    after_init: function(notice){
                                        notice.attention('rubberBand');
                                    }
                                })
                                window.location.reload();
                                sessionStorage.removeItem("txtboxPaid");
                            }

                        });
                        // }
                        // else{
                        //     alert("Its a Patient Balance. So it can be only closed by paying the whole Claim Balance")
                        // }
                    }
                    else{
                        alert("Patient's cheque is not enough to close this claim!")
                    }
                }
            }
            //alertify.success("Deposited successfully");
            $("#btnGetDeposit").click();
        });


        $(document).on('click','.btnSave',function() {
            if(depPayorType == "1"){
                var depID = sessionStorage.getItem("depID");
                var id = $(this).attr('id');
                var final = id.replace("btnSaveDep","");
                var copayVal = "txtcopay"+final;
                var deducVal = "txtdeduc"+final;
                var coinsVal = "txtcoins"+final;
                var allowed = "txtDepAllowed"+final;
                var paid = "txtDepPayment"+final;
                var adjust = "txtDepAdjust"+final;
                var bal = "txtBalance"+final;
                var reason = "ddlreason"+final;
                var status = "ddlStatus"+final;
                var copay = document.getElementById(copayVal).value;
                var deduc = document.getElementById(deducVal).value;
                var coins = document.getElementById(coinsVal).value;
                var allowedVal = document.getElementById(allowed).value;
                var paidVal = document.getElementById(paid).value;
                var adjustVal = document.getElementById(adjust).value;
                var balVal = document.getElementById(bal).innerHTML;
                var reasonVal = document.getElementById(reason).value;
                var statusVal = document.getElementById(status).value;
                //if(statusVal =="1"){
                $.ajax({
                    type: "POST",
                    url:root_url + "deposits/adjust",
                    data:{
                        "isPosted" : "1",
                        "depositID" : depID,
                        "claimID" : final,
                        "patientID" : "-1",
                        "insuranceID" : "-100",
                        "amountAdjusted" : paidVal,
                        "copay" : copay,
                        "deductible" : deduc,
                        "coins" : coins,
                        "allowed" : allowedVal,
                        "adjustmentNotes" : statusVal,
                        "adjustmentCode" : reasonVal,
                        "paid" : paidVal,
                        "adjustment" : adjustVal,
                        "claimBalance" : balVal,
                        "reason" : reasonVal,
                        "status" : statusVal
                    },success:function(result){
                        //window.location.href = "Deposit.php";
                        new PNotify({
                            type:'success',
                            text: json.message,
                            after_init: function(notice){
                                notice.attention('rubberBand');
                            }
                        })
                        window.location.reload();
                        sessionStorage.removeItem("txtboxPaid");
                    }

                });
                //}
            }
            else{
                var depID = sessionStorage.getItem("depID");
                var id = $(this).attr('id');
                var final = id.replace("btnSaveDep","");
                var reason = "ddlreason"+final;
                var status = "ddlStatus"+final;
                var bal = "txtBalance"+final;
                var paid = "txtDepPatPayment"+final;
                var reasonVal = document.getElementById(reason).value;
                var statusVal = document.getElementById(status).value;
                var paidVal = document.getElementById(paid).value;
                var patRem = document.getElementById("spanAmtRem").innerHTML;
                var balVal = document.getElementById(bal).innerHTML;
                if(balVal <= patRem){
                    patRem = patRem - balVal;
                    if(statusVal == "3"){
                        $.ajax({
                            type: "POST",
                            url:root_url + "deposits/adjust",
                            data:{
                                "isPosted" : "1",
                                "depositID" : depID,
                                "claimID" : final,
                                "patientID" : "-1",
                                "adjustment" : "0",
                                "insuranceID" : "-100",
                                "claimBalance" : balVal,
                                "adjustmentNotes" : statusVal,
                                "adjustmentCode" : reasonVal,
                                "reason" : reasonVal,
                                "amountAdjusted" : paidVal,
                                "status" : statusVal
                            },success:function(result){
                                //window.location.href = "Deposit.php";
                                new PNotify({
                                    type:'success',
                                    text: json.message,
                                    after_init: function(notice){
                                        notice.attention('rubberBand');
                                    }
                                })
                                window.location.reload();
                                sessionStorage.removeItem("txtboxPaid");
                            }

                        });
                    }
                    else{
                        alert("Its a Patient Balance. So it can be only closed by paying the whole Claim Balance")
                    }
                }
                else{
                    alert("Patient's cheque is not enough to close this claim!")
                }
            }

        });

        $(document).on('click','.statusLink',function() {
            $("#Status").modal('show');
            var ID = $(this).attr('id');
            StatusModal(ID);
        });
        $(document).on('focusout','.textbox1',function() {
            if($(this).val()!="0.00" && $(this).val()!=""){
                var tempValue1 = $(this).val();
                var newVal = tempValue1;
                $(this).val(newVal);
                var tempID = $(this).attr('id');
                sessionStorage.setItem("txtboxPaid",tempValue1);
                var tempChk = tempID.replace('txtDepPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",true);
                FocOut(tempValue1,tempID);
                // if ($("input:checkbox:checked").length > 1) {
                //   $("#bulkDiv").show();
                // }
                // else{
                //   $("#bulkDiv").hide();
                // }
            }
            else{
                var tempID = $(this).attr('id');
                var tempChk = tempID.replace('txtDepPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",false);
                // if ($("input:checkbox:checked").length > 1) {
                //   $("#bulkDiv").show();
                // }
                // else{
                //   $("#bulkDiv").hide();
                // }
            }
        });

        $(document).on('focusout','.copaytext',function() {
            if($(this).val()!="0.00" && $(this).val()!=""){
                var tempValue1 = $(this).val();
                var newVal = tempValue1;
                $(this).val(newVal);
                var tempID = $(this).attr('id');
                //sessionStorage.setItem("txtboxPaid",tempValue1);
                var tempChk = tempID.replace('txtDepPatcopay','');
                var ID = tempChk;
                tempChk = ".chkBulk"+tempChk;
                var copay = "txtDepPatcopay"+ID;
                var copayHtml = "txtoldPatcopay"+ID;
                var patBal = "txtBalancePat"+ID;
                var copayVal = document.getElementById(copayHtml).innerHTML;
                $(tempChk).prop("checked",true);
                var copayBal = toFixed(copayVal,2) - document.getElementById(copay).value;
                var patBalance = document.getElementById(patBal).innerHTML;
                patBalance = toFixed(patBalance,2) - document.getElementById(copay).value;
                document.getElementById(copayHtml).innerHTML = toFixed(copayBal,2);
                document.getElementById(patBal).innerHTML = toFixed(patBalance,2);
            }
            else{
                var tempID = $(this).attr('id');
                var tempChk = tempID.replace('txtDepPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",false);
            }
        });

        $(document).on('focusout','.coinstext',function() {
            if($(this).val()!="0.00" && $(this).val()!=""){
                var tempValue1 = $(this).val();
                var newVal = tempValue1;
                $(this).val(newVal);
                var tempID = $(this).attr('id');
                //sessionStorage.setItem("txtboxPaid",tempValue1);
                var tempChk = tempID.replace('txtDepPatcoins','');
                var ID = tempChk;
                tempChk = ".chkBulk"+tempChk;
                var copay = "txtDepPatcoins"+ID;
                var copayHtml = "txtoldPatcoins"+ID;
                var patBal = "txtBalancePat"+ID;
                var copayVal = document.getElementById(copayHtml).innerHTML;
                $(tempChk).prop("checked",true);
                var copayBal = toFixed(copayVal,2) - document.getElementById(copay).value;
                var patBalance = document.getElementById(patBal).innerHTML;
                patBalance = toFixed(patBalance,2) - document.getElementById(copay).value;
                document.getElementById(copayHtml).innerHTML = toFixed(copayBal,2);
                document.getElementById(patBal).innerHTML = toFixed(patBalance,2);
            }
            else{
                var tempID = $(this).attr('id');
                var tempChk = tempID.replace('txtDepPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",false);
            }
        });

        $(document).on('focusout','.deductext',function() {
            if($(this).val()!="0.00" && $(this).val()!=""){
                var tempValue1 = $(this).val();
                var newVal = tempValue1;
                $(this).val(newVal);
                var tempID = $(this).attr('id');
                //sessionStorage.setItem("txtboxPaid",tempValue1);
                var tempChk = tempID.replace('txtDepPatdeduc','');
                var ID = tempChk;
                tempChk = ".chkBulk"+tempChk;
                var copay = "txtDepPatdeduc"+ID;
                var copayHtml = "txtoldPatdeduc"+ID;
                var patBal = "txtBalancePat"+ID;
                var copayVal = document.getElementById(copayHtml).innerHTML;
                $(tempChk).prop("checked",true);
                var copayBal = toFixed(copayVal,2) - document.getElementById(copay).value;
                var patBalance = document.getElementById(patBal).innerHTML;
                patBalance = toFixed(patBalance,2) - document.getElementById(copay).value;
                document.getElementById(copayHtml).innerHTML = toFixed(copayBal,2);
                document.getElementById(patBal).innerHTML = toFixed(patBalance,2);
            }
            else{
                var tempID = $(this).attr('id');
                var tempChk = tempID.replace('txtDepPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",false);
            }
        });
        $(document).on('click','#btnSaveStatus',function() {
            var tempID = sessionStorage.getItem("currClaim");
            var temp = "#txtBalanceIns"+tempID;

            var icopay = "txtcopay"+tempID;
            var icoins = "txtcoins"+tempID;
            var ideduc = "txtdeduc"+tempID;
            var copay = document.getElementById(icopay).value;
            var coins = document.getElementById(icoins).value;
            var deduc = document.getElementById(ideduc).value;

            sessionStorage.setItem(icopay,copay);
            sessionStorage.setItem(icoins,coins);
            sessionStorage.setItem(ideduc,deduc);

            $(temp).focus();
            sessionStorage.removeItem("currClaim");
        });
        // $('#Status').on('keypress', function (event) {
        //   //alert("BEFORE ENTER clicked");
        //   var keycode = (event.keyCode ? event.keyCode : event.which);
        //   if(keycode == '13'){
        //     //alert("AFTER ENTER clicked");
        //     $('#btnSaveStatus').click();
        //   }
        //   // var tempID = $(".textbox7").attr("id");
        //   // var tempChk = tempID.replace('txtBalanceIns','');
        //   var tempID = sessionStorage.getItem("currClaim");
        //   var temp = "#txtBalanceIns"+tempID;
        //   $(temp).focus();
        //   sessionStorage.removeItem("currClaim");
        // });
        $(document).on('focusout','.textbox5',function() {
            if($(this).val()!="0.00" && $(this).val()!=""){
                var tempValue1 = $(this).val();
                var newVal = tempValue1;
                $(this).val(newVal);
                var tempID = $(this).attr('id');
                var tempChk = tempID.replace('txtDepPatPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",true);
                sessionStorage.setItem("txtboxPatPaid",tempValue1)
                FocOutPat(tempValue1,tempID);
                // if ($("input:checkbox:checked").length > 1) {
                //   $("#bulkDiv").show();
                // }
                // else{
                //   $("#bulkDiv").hide();
                // }
            }
            else{
                var tempID = $(this).attr('id');
                var tempChk = tempID.replace('txtDepPatPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",false);
                // if ($("input:checkbox:checked").length > 1) {
                //   $("#bulkDiv").show();
                // }
                // else{
                //   $("#bulkDiv").hide();
                // }
            }
        });
        $(document).on('focus','.textbox3',function() {
            var tempValue1 = $(this).val();
            var newVal = tempValue1;
            //var t = $(this).val(newVal);
            sessionStorage.setItem("txtboxAdjust",newVal);
        });
        $(document).on('focusout','.textbox3',function() {
            var tempValue1 = $(this).val();
            var newVal = parseFloat(tempValue1);
            $(this).val(newVal);
            var tempID = $(this).attr('id');
            var resStr = tempID.replace('txtDepAdjust','');
            tempID = 'txtBalancePat'+resStr;
            var tempID1 = 'txtBalanceIns'+resStr;
            var tempPaid = 'txtDepPayment'+resStr;
            var tempPaidVal = document.getElementById(tempPaid).value;
            if(tempPaidVal !=""){
                var tempValue1 = $(this).val();
                var newVal = parseFloat(tempValue1);
                $(this).val(newVal);
                var tempID = $(this).attr('id');
                var resStr = tempID.replace('txtDepAdjust','');
                tempID = 'txtBalancePat'+resStr;
                var tempID1 = 'txtBalanceIns'+resStr;
                var tempPaid = 'txtDepPayment'+resStr;
                var tempTot =  'txtTot'+resStr;
                var tempCopay =  'txtcopay'+resStr;
                var tempDeduc =  'txtdeduc'+resStr;
                var tempCoIns =  'txtcoins'+resStr;
                var tempBal = document.getElementById(tempTot).innerHTML;
                var tempPaidVal = document.getElementById(tempPaid).value;

                $.ajax({
                    type: "POST",
                    url: root_url+"deposits/total",
                    async : false,
                    data:{
                        claimID : resStr,
                        paid : tempPaidVal,
                        adjust : newVal
                    },success:function(result){
                        var res = JSON.parse(result);
                        if(res.status == 0){
                            return;
                        }
                        var Balance = res.data.total;
                        Balance = tempBal - Balance;
                        document.getElementById(tempID).value = Balance;
                        document.getElementById(tempID1).value = "0.00"
                        sessionStorage.setItem("patientBal",Balance);
                        $("#Status").modal('show');
                        StatusModal(resStr);
                        document.getElementById(tempCopay).value = Balance.toFixed(2);
                        var sesCopay = "txtcopay"+resStr;
                        var sesCoins = "txtcoins"+resStr;
                        var sesDeduc = "txtdeduc"+resStr;
                        sessionStorage.setItem(sesCopay,Balance);
                        document.getElementById(tempDeduc).value = "0.00"; 
                        document.getElementById(tempCoIns).value = "0.00"; 
                        var copayvalue = document.getElementById(tempCopay).value;
                        var coinsvalue = document.getElementById(tempCoIns).value;
                        var deducvalue = document.getElementById(tempDeduc).value;
                        sessionStorage.setItem("tempCopay",Balance); 
                        sessionStorage.setItem("tempDeduc",deducvalue); 
                        sessionStorage.setItem("tempCoins",coinsvalue); 
                        sessionStorage.setItem(sesCoins,coinsvalue);
                        sessionStorage.setItem(sesDeduc,deducvalue);
                    }
                });
            }
        });
        $(document).on('focusout','.copay',function() {
            var isTer = sessionStorage.getItem("txtboxPaid");
            var copayvalue = 0;
            var coinsvalue = 0;
            var deducvalue = 0;
            if(isTer != "" && isTer != null){
                var temp = $(this).attr('id');
                var tempValue1 = $(this).val();
                var resStr = temp.replace('txtcopay','');
                var tempID = "txtBalancePat"+resStr;
                var tempID1 = "txtBalanceIns"+resStr;
                var tempCopay =  'txtcopay'+resStr;
                var tempDeduc =  'txtdeduc'+resStr;
                var tempCoIns =  'txtcoins'+resStr;
                //document.getElementById(tempCopay).value = document.getElementById(tempCopay).value;
                copayvalue = document.getElementById(tempCopay).value;
                coinsvalue = document.getElementById(tempCoIns).value;
                deducvalue = document.getElementById(tempDeduc).value;
                tempValue1 = eval(copayvalue) + eval(coinsvalue) + eval(deducvalue);
                document.getElementById(tempID).value = tempValue1.toFixed(2);
                sessionStorage.setItem("tempCopay",copayvalue);
                sessionStorage.setItem("tempDeduc",deducvalue);
                sessionStorage.setItem("tempCoins",coinsvalue);
                var balFinal = sessionStorage.getItem(tempCopay);
                balFinal = balFinal - tempValue1;
                document.getElementById(tempID1).value = toFixed(balFinal,2);
            }
        });
        $(document).on('focusout','.coins',function() {
            var isTer = sessionStorage.getItem("txtboxPaid")
            if(isTer != "" && isTer != null){
                var temp = $(this).attr('id');
                var tempValue1 = $(this).val();
                var resStr = temp.replace('txtcoins','');
                var tempID = "txtBalancePat"+resStr;
                var tempID1 = "txtBalanceIns"+resStr;
                var tempCopay =  'txtcopay'+resStr;
                var tempDeduc =  'txtdeduc'+resStr;
                var tempCoIns =  'txtcoins'+resStr;
                //document.getElementById(tempCoIns).value = document.getElementById(tempCoIns).value;
                var copayvalue = document.getElementById(tempCopay).value;
                var coinsvalue = document.getElementById(tempCoIns).value;
                var deducvalue = document.getElementById(tempDeduc).value;
                tempValue1 = eval(copayvalue) + eval(coinsvalue) + eval(deducvalue);
                document.getElementById(tempID).value = tempValue1.toFixed(2);
                sessionStorage.setItem("tempCopay",copayvalue);
                sessionStorage.setItem("tempDeduc",deducvalue);
                sessionStorage.setItem("tempCoins",coinsvalue);
                var balFinal = sessionStorage.getItem(tempCopay);
                balFinal = balFinal - tempValue1;
                document.getElementById(tempID1).value = toFixed(balFinal,2);
            }
        });
        $(document).on('focusout','.deduc',function() {
            var isTer = sessionStorage.getItem("txtboxPaid")
            if(isTer != "" && isTer != null){
                var temp = $(this).attr('id');
                var tempValue1 = $(this).val();
                var resStr = temp.replace('txtdeduc','');
                var tempID = "txtBalancePat"+resStr;
                var tempID1 = "txtBalanceIns"+resStr;
                var tempCopay =  'txtcopay'+resStr;
                var tempDeduc =  'txtdeduc'+resStr;
                var tempCoIns =  'txtcoins'+resStr;
                //document.getElementById(tempDeduc).value = document.getElementById(tempDeduc).value;
                var copayvalue = document.getElementById(tempCopay).value;
                var coinsvalue = document.getElementById(tempCoIns).value;
                var deducvalue = document.getElementById(tempDeduc).value;

                tempValue1 = eval(copayvalue) + eval(coinsvalue) + eval(deducvalue);
                document.getElementById(tempID).value = tempValue1.toFixed(2);
                sessionStorage.setItem("tempCopay",copayvalue);
                sessionStorage.setItem("tempDeduc",deducvalue);
                sessionStorage.setItem("tempCoins",coinsvalue);
                var balFinal = sessionStorage.getItem(tempCopay);
                balFinal = balFinal - tempValue1;
                document.getElementById(tempID1).value = toFixed(balFinal,2);
            }
        });
        // $("#Status").on("shown.bs.modal", function () {
        //     $(this).find('input[type="text"]').focus();
        // });
        $(document).on('focusout','.textbox2',function() {
            var tempValue1 = $(this).val();
            var newVal = tempValue1;
            $(this).val(newVal);
        });
        $(document).on('focusin','.textbox1',function() {
            var isTer = sessionStorage.getItem("txtboxPaid")
            if(isTer != "" && isTer != null){
                var tempValue1 = $(this).val();
                var tempID = $(this).attr('id');
                var resStr = tempID.replace('txtDepPayment','');
                var tempid = 'txtBalancePat'+resStr;
                var tempID1 = 'txtBalanceIns'+resStr;
                var tempTot = 'txtTot'+resStr;
                if(tempValue1 != ""){
                    var tempBal = document.getElementById(tempid).value;
                    var tempTotal = document.getElementById(tempTot).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    // var interApplied = AmtApplied - tempValue1;
                    // var interRemain = AmtRemain + tempValue1;
                    var FinalBal = tempTotal ;
                    var FinalApp = AmtApplied - parseFloat(tempValue1);
                    var FinalRem = AmtRemain + parseFloat(tempValue1);
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                        document.getElementById(tempid).value = "";
                    }
                }
            }
        });
        $(document).on('focusin','.textbox5',function() {
            var isTer = sessionStorage.getItem("txtboxPatPaid")
            if(isTer != "" && isTer != null){
                var tempValue1 = $(this).val();
                var tempID = $(this).attr('id');
                var resStr = tempID.replace('txtDepPatPayment','');
                var tempid = 'txtBalancePat'+resStr;
                var tempID1 = 'txtBalanceIns'+resStr;
                var tempTot = 'txtTot'+resStr;
                if(tempValue1 != ""){
                    var tempBal = document.getElementById(tempid).value;
                    var tempTotal = document.getElementById(tempTot).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    // var interApplied = AmtApplied - tempValue1;
                    // var interRemain = AmtRemain + tempValue1;
                    var FinalBal = tempTotal ;
                    var FinalApp = AmtApplied - parseFloat(tempValue1);
                    var FinalRem = AmtRemain + parseFloat(tempValue1);
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                        document.getElementById(tempid).value = "";
                    }
                }
            }
        });
    });



    function StatusModal(ID){
        var root_url = $("#hidden_root_url").val();
        var fin = ID.replace("statusID","");
        $("#copay").html('<input type="text" id="txtcopay'+fin+'" class="form-control copay" value="" />');
        $("#coins").html('<input type="text" id="txtcoins'+fin+'" class="form-control coins" value="" />');
        $("#deduc").html('<input type="text" id="txtdeduc'+fin+'" class="form-control deduc" value="" />');
        $.ajax({
            type: "POST",
            url: root_url+"claims/status",
            async : false,
            data:{
                claimID : fin
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                    return;
                }
                var t = res.data[0];

                sessionStorage.setItem("currClaim",fin);
                var copayVal = "#txtcopay"+t.claimID;
                var deducVal = "#txtdeduc"+t.claimID;
                var coinsVal = "#txtcoins"+t.claimID;
                if(t.copay != ""){
                    $(copayVal).val(t.copay);
                    var sesCopay = "txtcopay"+fin;
                    var sesCopay1 = "txtcopay"+fin;
                    sessionStorage.setItem(sesCopay,t.copay);
                    document.getElementById(sesCopay1).focus();
                }
                else
                {
                    $(copayVal).val(t.copay);
                    var sesCopay = "txtcopay"+fin;
                    var sesCopay1 = "txtcopay"+fin;
                    sessionStorage.setItem(sesCopay,t.copay);
                    document.getElementById(sesCopay1).focus();
                }
                if(t.deductible != ""){
                    $(deducVal).val(t.deductible);
                    var sesDeduc = "txtdeduc"+fin;
                    sessionStorage.setItem(sesDeduc,t.deductible);
                }
                else{
                    $(deducVal).val(t.deductible);
                    var sesDeduc = "txtdeduc"+fin;
                    sessionStorage.setItem(sesDeduc,t.deductible);
                }
                if(t.coins != ""){
                    $(coinsVal).val(t.coins);
                    var sesCoins = "txtcoins"+fin;
                    sessionStorage.setItem(sesCoins,t.coins);
                }
                else{
                    $(coinsVal).val(t.coins);
                    var sesCoins = "txtcoins"+fin;
                    sessionStorage.setItem(sesCoins,t.coins);
                }
            }
        });
    }
    function FocOut(a,b){
        var root_url = $("#hidden_root_url").val();
        var tempVal = a;
        var resStr = b.replace('txtDepPayment','');
        var tempid = 'txtBalancePat'+resStr;
        var tempID1 = 'txtBalanceIns'+resStr;
        var tempStatus = '#ddlStatus'+resStr;
        var tempTot = 'txtTot'+resStr;
        var tempAll = 'txtDepAllowed'+resStr;
        var tempAdj = 'txtDepAdjust'+resStr;
        var tempCopay = 'txtcopay'+resStr;
        var tempCoins = 'txtcoins'+resStr;
        var tempCaseID = "";
        var temp = "";
        var Ispost = "";
        var tempSessiom = sessionStorage.getItem("txtboxPaid");
        $.ajax({
            type: "POST",
            url: root_url+"claims/isposted",
            async : false,
            data:{
                claimID : resStr
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                    return;
                }
                result = res.data;
                //result = JSON.parse(result);
                tempCaseID = result[0].caseID;
                Ispost = result[0].isPosted;
            }
        });
        if(Ispost == "0"){
            if(a != tempSessiom){
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var tempAll = document.getElementById(tempAll).value;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var interApplied = AmtApplied - a;
                        var interRemain = AmtRemain - a;
                        var FinalApp = interApplied + parseFloat(decVal);
                        var FinalRem = interRemain - parseFloat(decVal);
                        //alert(tempBal);
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                    }
                    else{
                        $(this).val(tempVal);
                    }
                }
            }
            else{
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(decVal);
                        var FinalRem = AmtRemain - parseFloat(decVal);
                        //alert(Balance);
                        if(Balance <= 0){
                            $(tempStatus).val("3");
                            $("#Status").modal('hide');
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"policies/check",
                                async : false,
                                data:{
                                    caseID : tempCaseID
                                },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        $("#Status").modal('show');
                                        //StatusModal(resStr);
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                        $("#Status").modal('show');
                                        //StatusModal(resStr);
                                    }
                                }
                            });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        // $("#Status").modal('show');
                        // StatusModal(resStr);
                        // if(Balance % 1 != 0){
                        //     document.getElementById(tempCoins).value = Balance;
                        // }
                        // else{
                        //     console.log(tempCopay+".value = "+Balance+"");
                        //     document.getElementById(tempCopay).value = parseFloat(Balance);
                        // }
                    }
                    else{
                        $(this).val(tempVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(tempVal);
                        var FinalRem = AmtRemain - parseFloat(tempVal);
                        //alert(Balance);
                        if(Balance <= 0){
                            $(tempStatus).val("3");
                            $("#Status").modal('hide');
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"policies/check",
                                async : false,
                                data:{
                                    caseID : tempCaseID
                                },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        // $.ajax({
                                        //       type: "POST",
                                        //       url:"updateInsurance.php",
                                        //       async : false,
                                        //       data:{
                                        //         caseID : tempCaseID
                                        //       },success:function(result){
                                        //       }
                                        //   });
                                        $("#Status").modal('show');
                                        //StatusModal(resStr);
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                        $("#Status").modal('show');
                                        //StatusModal(resStr);
                                    }
                                }
                            });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        // $("#Status").modal('show');
                        // StatusModal(resStr);
                        // if(Balance % 1 != 0){
                        //     document.getElementById(tempCoins).value = Balance;
                        // }
                        // else{
                        //     console.log(tempCopay+".value = "+Balance+"");
                        //     document.getElementById(tempCopay).value = Balance;
                        // }
                    }
                }
            }
        }
        else{
            if(a != tempSessiom){
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var tempAll = document.getElementById(tempAll).value;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var interApplied = AmtApplied - a;
                        var interRemain = AmtRemain - a;
                        var FinalApp = interApplied + parseFloat(decVal);
                        var FinalRem = interRemain - parseFloat(decVal);
                        //alert(tempBal);
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                    }
                    else{
                        $(this).val(tempVal);
                    }
                }
            }
            else{
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = 0;
                        var Balance = "";
                        $.ajax({
                            type: "POST",
                            url: root_url+"deposits/total",
                            async : false,
                            data:{
                                claimID : resStr,
                                paid : a,
                                adjust : Adjust
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                                    return;
                                }
                                //var data1 = res.data;
                                var Balance = res.data.total;
                                Balance = tempBal - Balance;
                            }
                        });
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(decVal);
                        var FinalRem = AmtRemain - parseFloat(decVal);
                        //alert(Balance);
                        if(Balance <= 0){
                            $(tempStatus).val("3");
                            $("#Status").modal('hide');
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"policies/check",
                                async : false,
                                data:{
                                    caseID : tempCaseID
                                },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        // $("#Status").modal('show');
                                        // StatusModal(resStr);
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                        // $("#Status").modal('show');
                                        // StatusModal(resStr);
                                    }
                                }
                            });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        // $("#Status").modal('show');
                        // StatusModal(resStr);
                        // if(Balance % 1 != 0){
                        //     document.getElementById(tempCoins).value = Balance;
                        // }
                        // else{
                        //     console.log(tempCopay+".value = "+Balance+"");
                        //     document.getElementById(tempCopay).value = parseFloat(Balance);
                        // }
                    }
                    else{
                        $(this).val(tempVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = 0;
                        var Balance = "";
                        $.ajax({
                            type: "POST",
                            url: root_url+"deposits/total",
                            async : false,
                            data:{
                                claimID : resStr,
                                paid : a,
                                adjust : Adjust
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                                    return;
                                }
                                //var data1 = res.data;
                                var Balance = res.data.total;
                                Balance = tempBal - Balance;
                            }
                        });
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(tempVal);
                        var FinalRem = AmtRemain - parseFloat(tempVal);
                        //alert(Balance);
                        if(Balance <= 0){
                            $(tempStatus).val("3");
                            $("#Status").modal('hide');
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"policies/check",
                                async : false,
                                data:{
                                    caseID : tempCaseID
                                },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        // $.ajax({
                                        //       type: "POST",
                                        //       url:"updateInsurance.php",
                                        //       async : false,
                                        //       data:{
                                        //         caseID : tempCaseID
                                        //       },success:function(result){
                                        //       }
                                        //   });
                                        // $("#Status").modal('show');
                                        // StatusModal(resStr);
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                        // $("#Status").modal('show');
                                        // StatusModal(resStr);
                                    }
                                }
                            });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        // $("#Status").modal('show');
                        // StatusModal(resStr);
                        // if(Balance % 1 != 0){
                        //     document.getElementById(tempCoins).value = Balance;
                        // }
                        // else{
                        //     console.log(tempCopay+".value = "+Balance+"");
                        //     document.getElementById(tempCopay).value = Balance;
                        // }
                    }
                }
            }
        }

    }
    function FocOutPat(a,b){
        var root_url = $("#hidden_root_url").val();
        var tempVal = a;
        var resStr = b.replace('txtDepPatPayment','');
        var tempid = 'txtBalancePat'+resStr;
        var tempID1 = 'txtBalanceIns'+resStr;
        var tempStatus = '#ddlStatus'+resStr;
        var tempTot = 'txtTot'+resStr;
        var tempAll = 'txtDepAllowed'+resStr;
        var tempAdj = 'txtDepAdjust'+resStr;
        var tempCopay = 'txtcopay'+resStr;
        var tempCoins = 'txtcoins'+resStr;
        var tempCaseID = "";
        var tempSessiom = sessionStorage.getItem("txtboxPatPaid");
        $.ajax({
            type: "POST",
            url: root_url+"claims/get",
            async : false,
            data:{
                claimID : resStr
            },success:function(result){
                tempCaseID = result;
            }
        });
        if(a != tempSessiom){
            var IsDec = tempVal.indexOf(".");
            if(tempVal != ""){
                if(IsDec == '0' || IsDec == '-1'){
                    var decVal = tempVal+'.00';
                    $(this).val(decVal);
                    var tempBal = document.getElementById(tempTot).innerHTML;
                    var tempAll = document.getElementById(tempAll).value;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtBal = parseFloat(tempBal);
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    var Adjust = AmtBal - tempAll;
                    var Balance = tempAll - tempVal;
                    var interApplied = AmtApplied - a;
                    var interRemain = AmtRemain - a;
                    var FinalApp = interApplied + parseFloat(decVal);
                    var FinalRem = interRemain - parseFloat(decVal);
                    //alert(tempBal);
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                        document.getElementById(tempAdj).value = Adjust;
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                        document.getElementById(tempAdj).value = Adjust;
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = Balance;
                        document.getElementById(tempAdj).value = Adjust;
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                        document.getElementById(tempid).value = Balance;
                        document.getElementById(tempAdj).value = Adjust;
                    }
                }
                else{
                    $(this).val(tempVal);
                }
            }
        }
        else{
            var IsDec = tempVal.indexOf(".");
            if(tempVal != ""){
                if(IsDec == '0' || IsDec == '-1'){
                    var decVal = tempVal+'.00';
                    $(this).val(decVal);
                    var tempBal = document.getElementById(tempTot).innerHTML;
                    var patBal = document.getElementById(tempid).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtBal = parseFloat(tempBal);
                    var Balance = patBal - tempVal;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    var FinalApp = AmtApplied + parseFloat(decVal);
                    var FinalRem = AmtRemain - parseFloat(decVal);
                    //alert(Balance);
                    if(Balance == 0 || Balance <= 0){
                        $(tempStatus).val("3");
                        $("#Status").modal('hide');
                    }
                    else{
                        $.ajax({
                            type: "POST",
                            url: root_url+"policies/check",
                            async : false,
                            data:{
                                caseID : tempCaseID
                            },success:function(result){
                                if(result == "Secondary"){
                                    $(tempStatus).val("2");
                                    //$("#Status").modal('show');
                                    //StatusModal(resStr);
                                }
                                else
                                {
                                    $(tempStatus).val("1");
                                    //$("#Status").modal('show');
                                    //StatusModal(resStr);
                                }
                            }
                        });
                    }
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                        //document.getElementById(tempAdj).value = Adjust;
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                        //document.getElementById(tempAdj).value = Adjust;
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = Balance;
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                        document.getElementById(tempid).value = Balance;
                    }
                    // $("#Status").modal('show');
                    // StatusModal(resStr);
                }
                else{
                    var decVal = tempVal;
                    $(this).val(tempVal);
                    var tempBal = document.getElementById(tempTot).innerHTML;
                    var patBal = document.getElementById(tempid).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtBal = parseFloat(tempBal);
                    var Balance = patBal - tempVal;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    var FinalApp = AmtApplied + parseFloat(decVal);
                    var FinalRem = AmtRemain - parseFloat(decVal);
                    //alert(Balance);
                    if(Balance == 0 || Balance <= 0){
                        $(tempStatus).val("3");
                        $("#Status").modal('hide');
                    }
                    else{
                        $.ajax({
                            type: "POST",
                            url: root_url+"policies/check",
                            async : false,
                            data:{
                                caseID : tempCaseID
                            },success:function(result){
                                if(result == "Secondary"){
                                    $(tempStatus).val("2");
                                    // $.ajax({
                                    //       type: "POST",
                                    //       url:"updateInsurance.php",
                                    //       async : false,
                                    //       data:{
                                    //         caseID : tempCaseID
                                    //       },success:function(result){
                                    //       }
                                    //   });
                                    //$("#Status").modal('show');
                                    //StatusModal(resStr);
                                }
                                else
                                {
                                    $(tempStatus).val("1");
                                    //$("#Status").modal('show');
                                    //StatusModal(resStr);
                                }
                            }
                        });
                    }
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = Balance;
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                        document.getElementById(tempid).value = Balance;
                    }
                    //$("#Status").modal('show');
                    //StatusModal(resStr);
                }
            }
        }

    }

    function floorFigure(figure, decimals){
        if (!decimals) decimals = 2;
        var d = Math.pow(10,decimals);
        return (parseInt(figure*d)/d).toFixed(decimals);
    };

    function toFixed(value, n) {
        const f = Math.pow(10, n);
        return (Math.trunc(value*f)/f).toFixed(n);
    }


    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2];

        return month + '-' + day + '-' + year;
    }
    function changeDate(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var month = splitDate[0];
        var date1 = splitDate[1];
        var year = splitDate[2];

        return year + '-' + month + '-' + date1;
    }
</script>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>