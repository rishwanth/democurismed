<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-6" >
            <h2>Clients</h2>
        </div>
        <div class="col-6" >
            <a href="<?php echo e(URL::to('patient/add')); ?>" class="btn btn-primary float-right">Add New Client</a>
        </div>
        <div class="col-md-12" style="margin-top:20px;">
            <div class="widget box" >
                <div class="widget-content">
                    
                        <div class="table-responsive">
                            <table id="test" class="table table-bordered" style="cursor:pointer; border-radius:15px; border-color:#000;" cellspacing="0" width="100%">
                            </table>
                        </div>
                </div>
            </div>

        </div>
<?php $__env->stopSection(); ?>
        <style type="text/css">
            .dataTables_wrapper .dataTables_scroll {
                border: 1px solid #000;
                margin: 1.5rem 0;
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
                border-bottom-left-radius: 4px;
                border-bottom-right-radius: 4px;
            }




            div.dataTables_scrollHead table.dataTable {
                margin-bottom: 0 !important;
                margin-top: 0 !important;
                border: 0 !important;

            }

            .dataTables_scrollBody {
                overflow-x: hidden !important;
                border-top: 1px solid #000;
            }


            .table {
                border-left:0px !important;
                border-right:0px !important;

            }

            .table th {
                border-bottom: 0 !important;
            }
        </style>
        <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
        <script
                src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script>


            $(document).ready(function(){
                $(".preloader").show();

                var practiceId = sessionStorage.getItem('practiceId');
                var dbName = sessionStorage.getItem('dbName');
                var root_url = $("#hidden_root_url").val();
                //document.getElementById("lblUser").innerHTML = sessionStorage.getItem('userName');

                $(".navigation li").parent().find('li').removeClass("active");

                $("#navv1").addClass("active");
                $(".left-side").css({"max-height":"800px"});
                $.ajax({
                    type: "POST",
                    url: "http://localhost/democurismed/patient/list2",
                    data: {
                    }, success: function (result) {
                        var data = JSON.parse(result);
                        if (data.status != 0) {
                            var data1 = data.data;
                            //alert(data1.length);
                            var dt = [];
                            $.each(data1, function (i, v) {
                                var DOB = changeDateFormat(data1[i].dob);
                                var txt = "";
                                $.ajax({
                                    type: "POST",
                                    url: "http://localhost/democurismed/location/list",
                                    async: false,
                                    data: {
                                        patientID: data1[i].patientID
                                    }, success: function (result) {
                                        var res = JSON.parse(result);
                                        if (res.status != 0) {
                                            res = res.data;
                                            if (res[0].home == "1") {
                                                txt += "<span class='badge badge-success'>Home</span> ";
                                            }
                                            if (res[0].office == "1") {
                                                txt += "<span class='badge badge-success'>Office</span> ";
                                            }
                                            if (res[0].school == "1") {
                                                txt += "<span class='badge badge-success'>School</span> ";
                                            }
                                        }
                                        else {
                                            txt += "<span class='badge badge-danger'>No Locations selected</span> ";
                                        }
                                    }
                                });

                                if (data1[i].gender == "Male") {
                                    var gen = '<i class="fa fa-fw fa-male" style="font-size:22px"></i> <span style="color:#33CC99; font-size:13px; font-weight:600;">' + data1[i].full_name + '</span>';
                                }
                                else {
                                    var gen = '<i class="fa fa-fw fa-female" style="font-size:22px"></i> <span style="color:#33CC99; font-size:13px; font-weight:600;">' + data1[i].full_name + '</span>';
                                }

                                dt.push([data1[i].id, gen, DOB, data1[i].ssn, data1[i].chart_num, data1[i].phone_home, data1[i].primarycarephysician, txt]);
                            });
                            //alert(dt);
                            var table = $('#test').DataTable({
                                "scrollY": "300px",
                                "scrollCollapse": true,
                                "data": dt,
                                columns: [
                                    {"title": "patientID", visible: false},
                                    {"title": "Client Name"},
                                    {"title": "DOB"},
                                    {"title": "SSN"},
                                    {"title": "Account #"},
                                    {"title": "Cell #"},
                                    {"title": "Rendering Provider"},
                                    {"title": "Locations"}
                                ]
                            });
                            $(".dataTables_filter").find("input").css({"border-radius": "15px"});
                            $(".dataTables_scrollBody table").css({"border": "none"});
                            // $("table.dataTable thead .sorting").css({"text-align":"center"})
                            // $("table.dataTable td ").css({"text-align":"center"})
                            $('.preloader').hide();
                            $('#test tbody').on('click', 'tr', function () {
                                $('#MedCoding').modal('show');
                                var data = table.row(this).data();
                                var id = data[0];
                                sessionStorage.setItem("patientId", id);
                                window.location.href = "edit";
                            });
                            $("th:first").css({"border-top-left-radius": "5px"});
                        }
                        else{
                            var dt = [];
                            var table = $('#test').DataTable({
                                "scrollY": "300px",
                                "scrollCollapse": true,
                                "data": dt,
                                columns: [
                                    {"title": "patientID", visible: false},
                                    {"title": "Client Name"},
                                    {"title": "DOB"},
                                    {"title": "SSN"},
                                    {"title": "Account #"},
                                    {"title": "Cell #"},
                                    {"title": "Rendering Provider"},
                                    {"title": "Locations"}
                                ]
                            });
                            $(".dataTables_filter").find("input").css({"border-radius": "15px"});
                            $(".dataTables_scrollBody table").css({"border": "none"});
                            // $("table.dataTable thead .sorting").css({"text-align":"center"})
                            // $("table.dataTable td ").css({"text-align":"center"})
                            $('.preloader').hide();
                            $('#test tbody').on('click', 'tr', function () {
                                $('#MedCoding').modal('show');
                                var data = table.row(this).data();
                                var id = data[0];
                                sessionStorage.setItem("patientId", id);
                                window.location.href = "edit";
                            });
                            $("th:first").css({"border-top-left-radius": "5px"});
                        }
                    }
                });
                
                $("th:first").css({"border-top-left-radius":"5px"});
            });
            function changeDateFormat(inputDate){  // expects Y-m-d
                if(inputDate != "" && inputDate != undefined && inputDate != null){
                    var splitDate = inputDate.split('-');
                    if(splitDate.count == 0){
                        return null;
                    }

                    var year = splitDate[0];
                    var month = splitDate[1];
                    var day = splitDate[2];

                    return month + '-' + day + '-' + year;
                }
            }
</script>
<?php echo $__env->make('layout.app',['data' => $data], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>