<?php $__env->startSection('content'); ?>

	<div class="row">
		<h2 style="color: #251367; margin-left:20px;">CPTs LIST</h2>
		<div class="col-md-12" style="margin-top:20px;">
        <div class="widget box" >
            <div class="widget-content">
                <a href="#" id="linkAddCPT" class="btn btn-primary" style="text-decoration:none"><i class="icon icon-plus"></i>&nbsp;&nbsp; New Procedure Code</a><br/><br/>
					<table id="CPTList" class="table display" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
					</table>
            </div>
        </div>

	</div>

				
    <div class="modal fade" id="CPTModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Procedure Code</h4>
                </div>
                <div class="modal-body" style="min-height:150px;">
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">CPT Code </label>
                        <div class="col-md-12">
                            <input type="text" id="cptCode" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Description </label>
                        <div class="col-md-12">
                            <input type="text" id="desc" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Charges </label>
                        <div class="col-md-12">
                            <input type="text" id="charges" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveCPT" class="btn btn-primary" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
	<div class="modal fade" id="EditCPT" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit CPT</h4>
				</div>
				<div class="modal-body" style="min-height:150px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label col-md-12">CPT Code </label>
                                <div class="col-md-12">
                                    <input type="text" id="cptCd" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label col-md-12">Description </label>
                                <div class="col-md-12">
                                    <input type="text" id="desc1" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label col-md-12">Charges </label>
                                <div class="col-md-12">
                                    <input type="text" id="charge" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnEditCPT" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
<?php $__env->stopSection(); ?>

    
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>