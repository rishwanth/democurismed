<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="row col-md-9" style="border-left:1px solid #000; border-top-left-radius:5px; border-top:3px solid #000;border-bottom:1px solid #000;border-right:1px solid #000; margin-left:3px; color:#000; font-weight:600;">
            <div class="col-md-4" style="margin-top:10px;">
                <span style="font-size:24px; color: orangered;" id="patientName_top"></span><br/>
                <span>DOB  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="DOBspan"></span></span> <br/>
                <span>Insurance : <span id="dashboardIns"></span></span>
            </div>
            <div class="col-md-4" style="margin-top:15px;">
                <span>Account # &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="accNo"></span></span> <br/>
                <span>Supervisor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="rendered"> </span></span> <br/>
            </div>
            <div class="col-md-4" style="margin-top:15px;">
                <span style="color:green">Last Insurance Payment &nbsp;&nbsp; : <span id="lastPayment"></span></span> <br/>
                <span style="color:green">Last Billed &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="lastCharge"></span></span> <br/>
                <span style="color:maroon">Last Statement &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="lastStatement"></span></span> <br/>
            </div>
        </div>
        <div class="row col-md-3" style="border-top:3px solid #000; border-top-right-radius:5px; border-bottom:1px solid #000;border-right:1px solid #000; margin-left:3px; padding-top:5px; color:#000; font-weight:600;">
            <div class="col-md-12">
                <h3 style="margin-top:10px;padding:0">Total Outstanding</h3>
                <span style="color:green">Insurance : $<span id="outstandIns"></span></span> <br/>
                <span style="color:maroon">Patient&nbsp;&nbsp; &nbsp;&nbsp; : $<span id="outstandPat"></span></span> <br/>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:20px;">
            <div class="widget box" >
                <div class="widget-content">
                    <div class="card-body">
                        <div class="bs-example">
                            <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                                <li class="nav-item">
                                    <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Demographic</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Insurance</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#tab_3_3" style="color:#000;" data-toggle="tab" class="nav-link">Activity</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#tab_3_4" style="color:#000;" data-toggle="tab" class="nav-link">Accounts</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#tab_3_5" style="color:#000;" data-toggle="tab" class="nav-link">Documents</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#tab_3_6" style="color:#000;" data-toggle="tab" class="nav-link">Alerts</a>
                                </li>
                                <!-- <li class="nav-item">
                            <a href="#tab_3_7" style="color:#000;" data-toggle="tab" class="nav-link">Log</a>
                        </li> -->
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div class="tab-pane active" id="tab_3_1">
                                    <div class="col-12">
                                        <div class="card-body">
                                            <div class="row">
                                                <input type="hidden" id="_hdnID" />
                                                <input type="hidden" id="hdnPatFullName" />
                                                <input type="hidden" id="hdnPatDOB" />
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Full Name</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="fullName" name="txtPTNumber" placeholder="Enter the Name of the Patient" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Gender</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <select class="form-control" id="gender">
                                                                <option value=""></option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-sm-6">
                                            <div class="form-group row">
                                                <div class="col-5">
                                                    <label for="input-text" class="control-label float-right txt_media1"></label>
                                                </div>
                                                <div class="col-7">
                                                </div>
                                            </div>
                                        </div> -->
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Date Of Birth</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="dob" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Social Security # </label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control" id="ssn" name="txtPTNumber" placeholder="Enter the Social Security #" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-sm-6">
                                            <div class="form-group row">
                                                <div class="col-5">
                                                    <label for="input-text" class="control-label float-right txt_media1">Employment Status</label>
                                                </div>
                                                <div class="col-7">
                                                    <select class="form-control" id="employment">
                                                        <option></option>
                                                        <option>Employed</option>
                                                        <option>Self-Employed</option>
                                                        <option>Retired</option>
                                                        <option>Student. Full-time</option>
                                                        <option>Student. Part-time</option>
                                                        <option>Unknown</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> -->
                                                <div class="col-12">
                                                    <label class="control-label col-6" style="text-align:left; font-size:12px; font-weight:600;"><span style="color:orange"><u>Contact Information</u></span></label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Street 1</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="addrStreet1" placeholder="Enter the Street address" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Street 2</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="addrStreet2" placeholder="Enter the Street address" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">City</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="addrCity" disabled name="txtPTNumber" placeholder="City" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">State</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="addrState" disabled name="txtPTNumber" placeholder="State" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Zipcode</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="addrZip" name="txtPTNumber" placeholder="Enter the Zipcode" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Home #</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="phoneHome" name="txtPTNumber" placeholder="Enter the Home #" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            <!-- <div class="col-sm-6">
                                            <div class="form-group row">
                                                <div class="col-5">
                                                    <label for="input-text" class="control-label float-right txt_media1">Work #</label>
                                                </div>
                                                <div class="col-7">
                                                    <input type="text" class="form-control required" id="phoneWork" name="txtPTNumber" placeholder="Enter the Work #" value="" />
                                                </div>
                                            </div>
                                        </div> -->
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Cell #</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="phoneMobile" name="txtPTNumber" placeholder="Enter the Cell #" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Email</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control required" id="email" placeholder="Enter the Email Address" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-sm-6">
                                            <div class="form-group row">
                                                <div class="col-5">
                                                    <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="notifyEmail"> &nbsp;&nbsp;Send Email Notification
                                                </div>
                                                <div class="col-7">
                                                    <input type="checkbox" class="uniform" value="" id="notifyPhone"> Enable Auto Phone call Remainders
                                                </div>
                                            </div>
                                        </div>-->
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1"></label>
                                                        </div>
                                                        <div class="col-7">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1"></label>
                                                        </div>
                                                        <div class="col-7">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1"></label>
                                                        </div>
                                                        <div class="col-7">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Supervisor (CMS 1500-31)</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" class="form-control validateGeneral" id="primaryCarePhy" name="DRenderProvider" placeholder="Enter the Rendering Provider" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-6">
                                                            <label for="input-text" class="control-label float-right txt_media1">Service Locations</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="col-12">
                                                                <label class="checkbox-inline icheckbox">
                                                                    <input type="checkbox" class="uniform" id="homeChk" value="11"> Home
                                                                </label>
                                                                <label class="checkbox-inline icheckbox">
                                                                    <input type="checkbox" class="uniform" id="officeChk" value="12"> Office
                                                                </label>
                                                                <label class="checkbox-inline icheckbox">
                                                                    <input type="checkbox" class="uniform" id="schoolChk" value="3"> School
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="form-group row">
                                                        <div class="col-7">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group row">
                                                        <div class="col-7">
                                                            <input class="btn btn-primary pull-right" value="Update Patient" id="btnUpdatePatient" type="button">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-sm-6">
                                            <div class="form-group row">
                                                <div class="col-5">
                                                    <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                </div>
                                                <div class="col-7">
                                                    <input type="text" id="policyInsurance1" class="form-control validateGeneral1"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group row">
                                                <div class="col-5">
                                                    <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                </div>
                                                <div class="col-7">
                                                    <input type="text" id="policyInsurance1" class="form-control validateGeneral1"/>
                                                </div>
                                            </div>
                                        </div> -->




                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_3_2">
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-8">
                                                <select id="ddlcaseList" style="height:32px;" class="col-md-10 form-control"></select>
                                            </div>
                                            <div class="col-4">
                                                <input type="button" id="btnInsurance" class="btn btn-primary" value="Create New Case" />
                                            </div>
                                        </div>
                                    </div>
                                    <div id="accordion" role="tablist">
                                        <input type="hidden" id="lblCaseNo" />
                                        <input type="hidden" id="hdnPatientID" />
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <a id="acc1" data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                        Primary Insurance
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row" style="padding:0.5rem">
                                                        <div class="col-sm-12">
                                                            <label class="control-label col-6" style="text-align:left; font-size:13px; font-weight:600;"><span style="color:orange"><u>Insurance Info</u></span></label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyInsurance1" class="form-control validateGeneral1"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-12">
                                                                    <span id="spanAddress1" style="font-weight:600;"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <select class="form-control validateGeneral1" style="font-size:12px;" style="font-size:12px;" id="EditpolicyRelationInsured1">
                                                                        <option>Self</option>
                                                                        <option>Spouse</option>
                                                                        <option>Other</option>
                                                                        <option>Child</option>
                                                                        <option>Grandfather Or Grandmother</option>
                                                                        <option>Grandson Or Granddaughter</option>
                                                                        <option>Nephew Or Niece</option>
                                                                        <option>Adopted Child</option>
                                                                        <option>Foster Child</option>
                                                                        <option>Stepson</option>
                                                                        <option>Ward</option>
                                                                        <option>Stepdaughter</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyHolder1" class="form-control validateGeneral1" disabled/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyDOB1" class="form-control validateGeneral1" disabled/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyNo1" class="form-control validateGeneral1"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyStartDt1" class="form-control validateGeneral1"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyEndDt1" class="form-control validateGeneral1"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyGroupNo1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyGroupName1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Copay</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyCopay1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-12">
                                                                    <div class="card-body" style="padding:0">
                                                                        <div class="row">
                                                                            <div class="col-6">
                                                                                <input type="text" id="EditprincipalDiag" class="form-control validateGeneral1"  placeholder="Principle Diag"/>
                                                                            </div>
                                                                            <div class="col-6">
                                                                                <input type="text" id="EditdefDiag2" class="form-control" placeholder="Diagnosis 2"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Deductible</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyDeduc1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-12">
                                                                    <div class="card-body" style="padding:0">
                                                                        <div class="row">
                                                                            <div class="col-6">
                                                                                <input type="text" id="EditdefDiag3" class="form-control"  placeholder="Diagnosis 3"/>
                                                                            </div>
                                                                            <div class="col-6">
                                                                                <input type="text" id="EditdefDiag4" class="form-control" placeholder="Diagnosis 4"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label class="control-label col-6" style="text-align:left; font-size:13px; font-weight:600;"><span style="color:orange"><u>Authorization Info</u></span></label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Auth No</label>
                                                                </div>
                                                                <div class="col-5">
                                                                    <select id="EditauthNo1" class="form-control"></select>
                                                                </div>
                                                                <div class="col-1">
                                                                    <a href="javascript:void(0)" id="linkEditAuth1"><span class="ti-pencil"></span></a>
                                                                </div>
                                                                <div class="col-1">
                                                                    <a href="javascript:void(0)" id="linkAddAuth1"><span class="ti-plus"></span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Type of Service</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <select id="Edittos1" class="form-control validateGeneral1">
                                                                        <option></option>
                                                                        <option value="Behaviour Therapy">Behaviour Therapy</option>
                                                                        <option value="Physical Therapy">Physical Therapy</option>
                                                                        <option value="Occupational Therapy">Occupational Therapy</option>
                                                                        <option value="Speech Therapy">Speech Therapy</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Eff. Start Date</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditAuthstartDt1" class="form-control validateGeneral1"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Eff. End Date</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditAuthendDt1" class="form-control validateGeneral1"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-6">
                                                            <div class="card-body" style="padding:0">
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <label for="input-text" class="control-label float-right txt_media1">Copay</label>
                                                                    </div>
                                                                    <div class="col-8">
                                                                        <input type="text" id="EditpolicyCopay1" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="card-body" style="padding:0">
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <label for="input-text" class="control-label float-right txt_media1">Deductible</label>
                                                                    </div>
                                                                    <div class="col-8">
                                                                        <input type="text" id="EditpolicyDeduc1" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingThree">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" id="acc2" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                        Secondary Insurance
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row" style="padding:0.5rem">
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyInsurance2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-12">
                                                                    <span id="spanAddress2" style="font-weight:600;"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <select class="form-control" style="font-size:12px;" style="font-size:12px;" id="EditpolicyRelationInsured2">
                                                                        <option>Self</option>
                                                                        <option>Spouse</option>
                                                                        <option>Other</option>
                                                                        <option>Child</option>
                                                                        <option>Grandfather Or Grandmother</option>
                                                                        <option>Grandson Or Granddaughter</option>
                                                                        <option>Nephew Or Niece</option>
                                                                        <option>Adopted Child</option>
                                                                        <option>Foster Child</option>
                                                                        <option>Stepson</option>
                                                                        <option>Ward</option>
                                                                        <option>Stepdaughter</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyHolder2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyDOB2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyNo2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyStartDt2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyEndDt2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyGroupNo2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyGroupName2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Copay </label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyCopay2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Deduc </label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyDeduc2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label class="control-label col-6" style="text-align:left; font-size:13px; font-weight:600;"><span style="color:orange"><u>Authorization Info</u></span></label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Auth No</label>
                                                                </div>
                                                                <div class="col-5">
                                                                    <select id="EditauthNo2" class="form-control"></select>
                                                                </div>
                                                                <div class="col-1">
                                                                    <a href="javascript:void(0)" id="linkEditAuth2"><span class="ti-pencil"></span></a>
                                                                </div>
                                                                <div class="col-1">
                                                                    <a href="javascript:void(0)" id="linkAddAuth2"><span class="ti-plus"></span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Type of Service</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <select id="Edittos2" class="form-control ">
                                                                        <option></option>
                                                                        <option value="Behaviour Therapy">Behaviour Therapy</option>
                                                                        <option value="Physical Therapy">Physical Therapy</option>
                                                                        <option value="Occupational Therapy">Occupational Therapy</option>
                                                                        <option value="Speech Therapy">Speech Therapy</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Eff. Start Date</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditAuthstartDt2" class="form-control "/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Eff. End Date</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditAuthendDt2" class="form-control "/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingFour">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" id="acc3" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                        Teritary Insurance
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row" style="padding:0.5rem">
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyInsurance3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-12">
                                                                    <span id="spanAddress3" style="font-weight:600;"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <select class="form-control" style="font-size:12px;" style="font-size:12px;" id="EditpolicyRelationInsured3">
                                                                        <option>Self</option>
                                                                        <option>Spouse</option>
                                                                        <option>Other</option>
                                                                        <option>Child</option>
                                                                        <option>Grandfather Or Grandmother</option>
                                                                        <option>Grandson Or Granddaughter</option>
                                                                        <option>Nephew Or Niece</option>
                                                                        <option>Adopted Child</option>
                                                                        <option>Foster Child</option>
                                                                        <option>Stepson</option>
                                                                        <option>Ward</option>
                                                                        <option>Stepdaughter</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyHolder3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyDOB3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyNo3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyStartDt3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyEndDt3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyGroupNo3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyGroupName3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Copay </label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyCopay3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-5">
                                                                    <label for="input-text" class="control-label float-right txt_media1">Deduc </label>
                                                                </div>
                                                                <div class="col-7">
                                                                    <input type="text" id="EditpolicyDeduc3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group row">
                                                <div class="col-10">
                                                </div>
                                                <div class="col-2">
                                                    <input type="button" class="btn btn-primary" id="btnUpdateIns" value="Update Insurance">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_3_3">
                                    <input type="hidden" id="hdnAuth" />
                                    <input type="hidden" id="hdncaseID" />
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Insurance </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select id="ddlActcaseList" class="form-control"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <input type="button" id="btnOpenAct" value="Create New Activity" class="btn btn-primary">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Authorization </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select class="form-control" id="ddlAuth">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <table id="activityTab" class="table-bordered" style="width:100%">
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_3_4">
                                    <table id="accountTable" class="table-bordered"></table><br/><br/>
                                </div>
                                <div class="tab-pane fade" id="tab_3_5">
                                    <div class="form-group col-md-3">
                                        <a href="#" id="linkUpload" class="btn btn-primary" style="text-decoration:none"><i class="icon icon-upload-alt"></i>&nbsp;&nbsp;Upload Document</a>
                                    </div>
                                    <table id="documentTable" class="table table-striped table-bordered table-hover table-checkable">
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="tab_3_6">

                                </div>
                                <!-- <div class="tab-pane fade" id="tab_3_7">
                        </div> -->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="modal fade" id="insurance" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:85%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add New Case</h4>
                    </div>
                    <div class="modal-body">
                        <div id="accordion" role="tablist">
                            <input type="hidden" id="lblCaseNo" />
                            <input type="hidden" id="hdnPatientID" />
                            <div class="card">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a id="acc1" data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                            Primary Insurance
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row" style="padding:0.5rem">
                                            <div class="col-12">
                                                <label class="control-label col-6" style="text-align:left;"><span style="color:blue; font-size:14px; font-weight:600;"><ul>Insurance Info</u></span></label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyInsurance1" class="form-control validateGeneral1"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <span id="NewspanAddress1" style="font-weight:600;"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <select class="form-control validateGeneral1" style="font-size:12px;" style="font-size:12px;" id="policyRelationInsured1">
                                                            <option>Self</option>
                                                            <option>Spouse</option>
                                                            <option>Other</option>
                                                            <option>Child</option>
                                                            <option>Grandfather Or Grandmother</option>
                                                            <option>Grandson Or Granddaughter</option>
                                                            <option>Nephew Or Niece</option>
                                                            <option>Adopted Child</option>
                                                            <option>Foster Child</option>
                                                            <option>Stepson</option>
                                                            <option>Ward</option>
                                                            <option>Stepdaughter</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyHolder1" class="form-control validateGeneral1" disabled/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyDOB1" class="form-control validateGeneral1" disabled/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyNo1" class="form-control validateGeneral1"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyStartDt1" class="form-control validateGeneral1"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyEndDt1" class="form-control validateGeneral1"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Total Auth Visits</label>
                                            </div>
                                            <div class="col-4">
                                                <select class="form-control" style="font-size:12px;" style="font-size:12px;" id="totAuthType">
                                                    <option value=""></option>
                                                    <option value="Week">Week</option>
                                                    <option value="Total Auth">Total Auth</option>
                                                    <option value="Session">Session</option>
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <input type="text" id="AuthtotalVisits" class="form-control validateGeneral1"/>
                                            </div>
                                        </div>
                                    </div> -->
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyGroupNo1" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyGroupName1" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <div class="card-body" style="padding:0">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <input type="text" id="principalDiag" class="form-control validateGeneral1"  placeholder="Diagnosis 1"/>
                                                                </div>
                                                                <div class="col-6">
                                                                    <input type="text" id="defDiag2" class="form-control" placeholder="Diagnosis 2"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Copay</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyCopay1" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <div class="card-body" style="padding:0">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <input type="text" id="defDiag3" class="form-control"  placeholder="Diagnosis 3"/>
                                                                </div>
                                                                <div class="col-6">
                                                                    <input type="text" id="defDiag4" class="form-control" placeholder="Diagnosis 4"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Deductible</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyDeduc1" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <label class="control-label col-6" style="text-align:left;"><span style="color:blue; font-size:14px; font-weight:600;"><ul>Auth Info</u></span></label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Auth No</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="authNo" class="form-control validateGeneral1"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Type of Service</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <select id="tos" class="form-control validateGeneral1">
                                                            <option></option>
                                                            <option value="Behaviour Therapy">Behaviour Therapy</option>
                                                            <option value="Physical Therapy">Physical Therapy</option>
                                                            <option value="Occupational Therapy">Occupational Therapy</option>
                                                            <option value="Speech Therapy">Speech Therapy</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Eff. Start Date</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="AuthstartDt" class="form-control validateGeneral1"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Eff. End Date</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="AuthendDt" class="form-control validateGeneral1"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div
                            <div class="card">
                                <div class="card-header" role="tab" id="headingThree">
                                    <h5 class="mb-0">
                                        <a class="collapsed" id="acc2" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Secondary Insurance
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row" style="padding:0.5rem">
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyInsurance2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <span id="NewspanAddress2" style="font-weight:600;"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <select class="form-control" style="font-size:12px;" style="font-size:12px;" id="policyRelationInsured2">
                                                            <option>Self</option>
                                                            <option>Spouse</option>
                                                            <option>Other</option>
                                                            <option>Child</option>
                                                            <option>Grandfather Or Grandmother</option>
                                                            <option>Grandson Or Granddaughter</option>
                                                            <option>Nephew Or Niece</option>
                                                            <option>Adopted Child</option>
                                                            <option>Foster Child</option>
                                                            <option>Stepson</option>
                                                            <option>Ward</option>
                                                            <option>Stepdaughter</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyHolder2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyDOB2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyNo2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyStartDt2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyEndDt2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyGroupNo2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyGroupName2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Copay </label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyCopay2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Deductible </label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyDeduc2" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingFour">
                                    <h5 class="mb-0">
                                        <a class="collapsed" id="acc3" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            Teritary Insurance
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row" style="padding:0.5rem">
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyInsurance3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <span id="NewspanAddress3" style="font-weight:600;"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <select class="form-control" style="font-size:12px;" id="policyRelationInsured3">
                                                            <option>Self</option>
                                                            <option>Spouse</option>
                                                            <option>Other</option>
                                                            <option>Child</option>
                                                            <option>Grandfather Or Grandmother</option>
                                                            <option>Grandson Or Granddaughter</option>
                                                            <option>Nephew Or Niece</option>
                                                            <option>Adopted Child</option>
                                                            <option>Foster Child</option>
                                                            <option>Stepson</option>
                                                            <option>Ward</option>
                                                            <option>Stepdaughter</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyHolder3"  class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyDOB3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyNo3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyStartDt3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyEndDt3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyGroupNo3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyGroupName3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Copay </label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyCopay3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-5">
                                                        <label for="input-text" class="control-label float-right txt_media1">Deductible </label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="text" id="policyDeduc3" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                        <input type="button" id="btnSaveCase" class="btn btn-primary" data-dismiss="modal" value="Save" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="EditClaim" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:90%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Claim</h4>
                    </div>
                    <div class="modal-body" style="min-height:400px;">
                        <input type="hidden" id="hdnClaimID" />
                        <input type="hidden" id="hdnClaimNo" />
                        <input type="hidden" id="hdnDX" />
                        <input type="hidden" id="hdnBal" />
                        <input type="hidden" id="hdnCaseChart" />
                        <input type="hidden" id="hdnCurrInsurance" />
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-4" style="padding:0;">
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">From</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="fromDt" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">To</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="toDt" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">CPT</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="proced" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Units</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="units" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Bill Date</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="billDt" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Billed Amount</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="total" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Allowed Amonut</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="allowed" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Paid</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="paid" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Adjustment</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="adjustment" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Balance</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="claimBalance" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Claim Status</label>
                                            </div>
                                            <div class="col-7">
                                                <select class="form-control" id="claimStatus" disabled>
                                                    <option value="DRAFT">DRAFT</option>
                                                    <option value="REVIEW">REVIEW</option>
                                                    <option value="COMPLETE">COMPLETE</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="form-group col-md-12" style="float:left;">
                                <label class="control-label col-md-6">Diag 1 </label>
                                <div class="col-md-6">
                                    <input type="text" id="diag1" class="form-control" disabled />
                                </div>
                            </div>
                            <div class="form-group col-md-12" style="float:left;">
                                <label class="control-label col-md-6">Diag 2 </label>
                                <div class="col-md-6">
                                    <input type="text" id="diag2" class="form-control" disabled />
                                </div>
                            </div>
                            <div class="form-group col-md-12" style="float:left;">
                                <label class="control-label col-md-6">Diag 3 </label>
                                <div class="col-md-6">
                                    <input type="text" id="diag3" class="form-control" disabled />
                                </div>
                            </div>
                            <div class="form-group col-md-12" style="float:left;">
                                <label class="control-label col-md-6">Diag 4 </label>
                                <div class="col-md-6">
                                    <input type="text" id="diag4" class="form-control" disabled />
                                </div>
                            </div> -->
                                </div>
                                <div class="form-group col-md-2" style="padding:0;">
                                    <input type="button" id="btnPaperClaim" style="margin:30px 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Print Paper Claim" />
                                    <input type="button" id="btnRebill" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary" value="Rebill" />
                                    <input type="button" id="btnTransBal" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Transfer Balance" />
                                    <input type="button" id="btnNote" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Note" />
                                    <input type="button" id="btnSettle" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Settle" />
                                    <input type="button" id="btnVoid" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Void" />
                                    <input type="button" id="btnAdjust1" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Adjustment" />
                                </div>
                                <div class="form-group col-md-5" style="padding:0 0 0 20px;">
                                    <div class="bs-example">
                                        <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                                            <li class="nav-item">
                                                <a href="#home" data-toggle="tab" class="nav-link active">History</a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content">
                                            <div class="tab-pane active" id="tab_1_1">
                                                <p id="historyClaims" style="width: 100%;overflow: scroll;max-height: 300px; min-height:300px">

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"><label class="control-label col-md-12 lblTitle" style="padding-top:20px;"></label></div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12" style="float:left;">
                                        <label class="control-label col-md-4 lblSub1"> </label>
                                        <div class="col-md-6 txtSub1">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12" style="float:left;">
                                        <label class="control-label col-md-4 lblSub2"> </label>
                                        <div class="col-md-6 txtSub2">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12" style="float:left;">
                                        <label class="control-label col-md-4 lblSub3"> </label>
                                        <div class="col-md-6 txtSub3">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-9 btnSection">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                        <input type="button" id="btnUpdateClaim" class="btn btn-primary" data-dismiss="modal" value="Save" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="upload" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Upload File</h4>
                    </div>
                    <div class="modal-body" style="min-height:150px;">
                        <form action="UploadFile.php" method="post" enctype="multipart/form-data">
                            <h2>Upload File</h2>
                            <label for="fileSelect">Filename:</label>
                            <input type="file" name="photo" id="fileSelect"><br>
                            <input type="submit" name="submit" value="Upload">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="paperClaimModal" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Select Paper Claim Style</h4>
                    </div>
                    <div class="modal-body" style="min-height:120px;">
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <div class="col-md-6">
                                <input type="radio" class="btn btn-primary" id="withBG" name="BG" value="With BG" checked /> With Background
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <div class="col-md-6">
                                <input type="radio" class="btn btn-primary" id="withoutBG" name="BG" value="Without BG" /> Without Background
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                        <input type="button" id="btnPrint" class="btn btn-primary" data-dismiss="modal" value="Save" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="DocumentNotes" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Notes</h4>
                    </div>
                    <div class="modal-body" style="min-height:120px;">
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <input type="hidden" id="hdnDocID"/>
                            <label>Notes : </label><br/><textarea class="form-control" rows="7" cols="10" id="Documentnotes"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                        <input type="button" id="btnDocNotes" class="btn btn-primary" data-dismiss="modal" value="Save" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="AddAuthModal" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Auth #</h4>
                    </div>
                    <div class="modal-body" style="min-height:120px;">
                        <input type="hidden" id="__hdnAuthNum">
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label>Auth # : </label><br/><input type="text" id="newtempAuth" class="form-control"/>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label>Type Of Service : </label><br/>
                            <select id="newtos" class="form-control">
                                <option></option>
                                <option value="Behaviour Therapy">Behaviour Therapy</option>
                                <option value="Physical Therapy">Physical Therapy</option>
                                <option value="Occupational Therapy">Occupational Therapy</option>
                                <option value="Speech Therapy">Speech Therapy</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label>Effective Start Date : </label><br/><input type="text" id="newAuthstartDt" class="form-control"/>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label>Effective End Date : </label><br/><input type="text" id="newAuthendDt" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                        <input type="button" id="btntempAuth" class="btn btn-primary" data-dismiss="modal" value="Save" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="EditAuthModal" tabindex="-1" style="z-index:1700">
            <div class="modal-dialog modal-lg" style="width:40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Auth #</h4>
                    </div>
                    <div class="modal-body" style="min-height:120px;">
                        <input type="hidden" id="_hdnAuthID" />
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label>Auth # : </label><br/><input type="text" id="EdittempAuth" class="form-control"/>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label>Type Of Service : </label><br/>
                            <select id="Edittemptos" class="form-control">
                                <option></option>
                                <option value="Behaviour Therapy">Behaviour Therapy</option>
                                <option value="Physical Therapy">Physical Therapy</option>
                                <option value="Occupational Therapy">Occupational Therapy</option>
                                <option value="Speech Therapy">Speech Therapy</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label>Effective Start Date : </label><br/><input type="text" id="EdittempAuthstartDt" class="form-control"/>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label>Effective End Date : </label><br/><input type="text" id="EdittempAuthendDt" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                        <input type="button" id="btntempAuth" class="btn btn-primary" data-dismiss="modal" value="Save" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="guarantorModal" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Guarantor</h4>
                    </div>
                    <div class="modal-body" style="min-height:150px;">
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label class="control-label col-md-6">Guarantor Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="guarantorName" name="txtFirstName" placeholder="Enter the First Name" />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label class="control-label col-md-6">Street</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="guarantorAddrStreet" name="txtFirstName" placeholder="Enter the First Name" />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label class="control-label col-md-6">City</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="guarantorAddrCity" name="txtFirstName" placeholder="Enter the First Name" />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label class="control-label col-md-6">State </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="guarantorAddrState" name="txtMiddleName" placeholder="Enter the Middle Name" />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label class="control-label col-md-6">Zip code </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="guarantorAddrZip" placeholder="Enter the Last Name" />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="padding-bottom:5%;">
                            <label class="control-label col-md-6">Relationship to Guarantor </label>
                            <div class="col-md-6">
                                <select class="form-control" id="guarantorRelation">
                                    <option></option>
                                    <option>Child</option>
                                    <option>Other</option>
                                    <option>Spouse</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                        <input type="button" id="btnSaveFullname" class="btn btn-primary" data-dismiss="modal" value="Save" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="claimDetails" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:75%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Claim Details</h4>
                    </div>
                    <div class="modal-body" style="min-height:120px;">
                        <table id="details" class="table table-striped table-bordered table-hover table-checkable">
                        </table>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modalActivity" tabindex="-1" style="z-index:99999">
            <div class="modal-dialog modal-lg" style="width:75%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="ActTitleMod"></h4>
                    </div>
                    <div class="modal-body" style="min-height:350px;">
                        <input type="hidden" id="hdnActID" />
                        <input type="hidden" id="hdnFlagAct" />
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1">Authorization </label>
                                        </div>
                                        <div class="col-7">
                                            <select class="form-control validateGeneral1" style="font-size:12px;" id="modalddlAuth">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1">Activity </label>
                                        </div>
                                        <div class="col-7">
                                            <select class="form-control validateGeneral1" style="font-size:12px;" style="font-size:12px;" id="Activity">
                                                <option value=""></option>
                                                <option value="Assessment">Assessment</option>
                                                <option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option>
                                                <option value="Supervision">Supervision</option>
                                                <option value="Supervision Parent Training">Supervision Parent Training</option>
                                                <option value="Report Writing Send to Editor">Report Writing Send to Editor</option>
                                                <option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option>
                                                <option value="Report Writing Progress Reports">Report Writing Progress Reports</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1">CPT Code </label>
                                        </div>
                                        <div class="col-7">
                                            <select class="form-control validateGeneral1" style="font-size:12px;" style="font-size:12px;" id="ActCPT">
                                                <option value=""></option>
                                                <option value="0359T">0359T</option>
                                                <option value="0360T">0360T</option>
                                                <option value="0361T">0361T</option>
                                                <option value="0364T">0364T</option>
                                                <option value="0365T">0365T</option>
                                                <option value="0368T">0368T</option>
                                                <option value="0369T">0369T</option>
                                                <option value="0370T">0370T</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1">Modifiers </label>
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body" style="padding:10px;">
                                                <div class="row">
                                                    <div class="col-3" style="padding-left:10px; padding-right:10px;">
                                                        <input type="text" id="ActMod1" class="form-control" maxlength="2"/>
                                                    </div>
                                                    <div class="col-3" style="padding-left:10px; padding-right:10px;">
                                                        <input type="text" id="ActMod2" class="form-control" maxlength="2"/>
                                                    </div>
                                                    <div class="col-3" style="padding-left:10px; padding-right:10px;">
                                                        <input type="text" id="ActMod3" class="form-control" maxlength="2"/>
                                                    </div>
                                                    <div class="col-3" style="padding-left:10px; padding-right:10px;">
                                                        <input type="text" id="ActMod4" class="form-control" maxlength="2"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1">Billed per </label>
                                        </div>
                                        <div class="col-7">
                                            <select class="form-control validateGeneral1" style="font-size:12px;" id="ActPer">
                                                <option value="15 mins">15 mins</option>
                                                <option value="30 mins">30 mins</option>
                                                <option value="Hour">Hour</option>
                                                <option value="Session">Session</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1">Units or Hours? </label>
                                        </div>
                                        <div class="col-7">
                                            <select class="form-control validateGeneral1" id="ddlApprove">
                                                <option value="Approved Units">Approved Units</option>
                                                <option value="Approved Hours">Approved Hours</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1" id="lblUnits"> </label>
                                        </div>
                                        <div class="col-7">
                                            <input type="text" id="calculate" class="form-control validateGeneral1"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group row">
                                        <div class="col-12" id="divHours">
                                            <select class="form-control" id="ddlApproveHours">
                                                <option value="Weekly">Weekly</option>
                                                <option value="Monthly">Monthly</option>
                                                <option value="Total Auth">Total Auth</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <div class="col-4">
                                            <label for="input-text" class="control-label float-right txt_media1"> Total Units</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="totAuth" class="form-control validateGeneral1"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1"> </label>
                                        </div>
                                        <div class="col-7">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1">Rate (s) </label>
                                        </div>
                                        <div class="col-7">
                                            <input type="text" id="ActRate" class="form-control validateGeneral1"/>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="col-12">
                            <label class="control-label col-6" style="text-align:left;"><span style="color:black">Maximum Frequency Allowed</span></label>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActMaxi1">
                                        <option value="Unit">Unit</option>
                                        <option value="Amount">Amount</option>
                                        <option value="Sessions">Sessions</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">per </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActPer1">
                                        <option value="Day">Day</option>
                                        <option value="Week">Week</option>
                                        <option value="Month">Month</option>
                                        <option value="Total Auth">Total Auth</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="ActIs1" class="form-control"/> And
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActMaxi2">
                                        <option value="Unit">Unit</option>
                                        <option value="Amount">Amount</option>
                                        <option value="Sessions">Sessions</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">per </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActPer2">
                                        <option value="Day">Day</option>
                                        <option value="Week">Week</option>
                                        <option value="Month">Month</option>
                                        <option value="Total Auth">Total Auth</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="ActIs2" class="form-control"/> And
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActMaxi3">
                                        <option value="Unit">Unit</option>
                                        <option value="Amount">Amount</option>
                                        <option value="Sessions">Sessions</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">per </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActPer3">
                                        <option value="Day">Day</option>
                                        <option value="Week">Week</option>
                                        <option value="Month">Month</option>
                                        <option value="Total Auth">Total Auth</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="ActIs3" class="form-control"/>
                                </div>
                            </div>
                        </div> -->
                                <div class="col-12">
                                    <table id="activityTab" class="table-bordered" style="width:100%">
                                    </table>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-5">
                                            <label for="input-text" class="control-label float-right txt_media1"> </label>
                                        </div>
                                        <div class="col-7">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-primary" id="btnAddActi" value="Update Activity" />
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>





<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>
    
    $(document).ready(function(){
        //alert(sessionStorage.getItem("patientId"));
        //document.getElementById('hdntoggleDX').value = "false";
        //$(".ti-menu").click();
        var root_url = $("#hidden_root_url").val();
        $(".ti-menu-alt").click();
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        var temp = sessionStorage.getItem("patientId");
        $(".navigation li").parent().find('li').removeClass("active");

        $("#navv1").addClass("active");

        // $('#icdtoggle').on('change', function(event, state) {
        //   document.getElementById('hdntoggleDX').value = event.target.checked; // jQuery event
        // });
        $("#divPerTime").hide();

        $("#linkUpload").click(function(){
            window.location.href="upload.php";
        });

        $.ajax({
            type: "POST",
            url : root_url + 'patient/lastpayment',
            data:{
                "patientID" : temp
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    return;
                }
                var data = res.data[0].created_At;
                if(data!="" && data != null){
                    document.getElementById("lastPayment").innerHTML = changeDateFormat(data.split(" ")[0]);

                }
                else{
                    document.getElementById("lastPayment").innerHTML = "Not yet generated";
                }
            }
        })

        $.ajax({
            type: "POST",
            url : root_url + 'patient/insurance',
            data:{
                "patientID" : temp
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    return;
                }
                var data = res.data[0].payer_name;
                if(result!=""){
                    document.getElementById("dashboardIns").innerHTML = data;

                }
                else{
                    document.getElementById("dashboardIns").innerHTML = data;
                }
            }
        })

        $.ajax({
            type: "POST",
            url : root_url + 'patient/lastcharge',
            data:{
                "patientID" : temp
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    document.getElementById("lastCharge").innerHTML = "Not yet generated";
                    return;
                }
                var data = res.data[0].created_at;
                if(data!="" && data != null){
                    document.getElementById("lastCharge").innerHTML = changeDateFormat(data.split(" ")[0]);

                }
                else{
                    document.getElementById("lastCharge").innerHTML = "Not yet generated";
                }
            }
        })
        $.ajax({
            type: "POST",
            url : root_url + 'patient/laststatement',
            data:{
                "patientID" : temp
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    document.getElementById("lastStatement").innerHTML = "Not yet generated";
                    return;
                }
                var data = res.data[0].created_on;
                if(data!="" && data != null){
                    document.getElementById("lastStatement").innerHTML = changeDateFormat(data.split(" ")[0]);

                }
                else{
                    document.getElementById("lastStatement").innerHTML = "Not yet generated";
                }
            }
        })

        $.ajax({
            type: "POST",
            url : root_url + 'patient/outstandinginsurance',
            data:{
                "patientID" : temp
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    return;
                }
                var result = res.data[0].total;
                if(result==""){
                    result = "0";
                }
                else{
                    result = result;
                }
                document.getElementById("outstandIns").innerHTML = result;
            }
        })
        $.ajax({
            type: "POST",
            url : root_url + 'patient/outstanding',
            data:{
                "patientID" : temp
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    return;
                }
                var result = res.data[0].total;
                if(result==""){
                    result = "0";
                }
                else{
                    result = result;
                }
                document.getElementById("outstandPat").innerHTML = result;
            }
        })

        $.ajax({
            type: "POST",
            url:root_url + "patient/listbypatientid",
            async: false,
            data:{
                patientID: temp
            },
            success:function(data){
                var res = JSON.parse(data);
                if(res.status == 0){
                    return;
                }
                var data = res.data
                var newDate = changeDateFormat(data[0].dob);
                document.title = data[0].full_name + ' | AMROMED Billing Service';
                document.getElementById('fullName').value = data[0].full_name;
                patFullName = data[0].full_name;
                document.getElementById("hdnPatFullName").value = patFullName;
                document.getElementById('accNo').innerHTML = data[0].chart_num;
                document.getElementById('DOBspan').innerHTML = changeDateFormat(data[0].dob);
                patDOB = changeDateFormat(data[0].dob);
                document.getElementById("hdnPatDOB").value = patDOB;
                if(data[0].primarycarephysician !=null){document.getElementById('rendered').innerHTML = data[0].primarycarephysician;}else{document.getElementById('rendered').innerHTML = "Not yet Selected";}
                // if(data[0].ReferringPhysician != null){document.getElementById('referred').innerHTML = data[0].ReferringPhysician;}else{document.getElementById('referred').innerHTML = "Not yet Selected";}
                //$("#martialStatus").val(data[0].martialStatus);
                $("#employment").val(data[0].employment);
                //$("#reference").val(data[0].reference);
                //$("#employer").val(data[0].employer);
                document.getElementById('dob').value = newDate;
                //document.getElementById('medicalRecord').value = data[0].medicalRecord;
                if(data[0].gender =='M'){
                    $("#gender").val("Male");
                }
                else{
                    $("#gender").val("Female");
                }
                // if(data[0].notifyEmail=='1'){
                // $('#notifyEmail').prop('checked',true);
                // }
                // else{
                // $('#notifyEmail').prop('checked',false);
                // }
                // if(data[0].notifyPhone=='1'){
                // $('#notifyPhone').prop('checked',true);
                // }
                // else{
                // $('#notifyPhone').prop('checked',false);
                // }
                document.getElementById('ssn').value = data[0].ssn;
                document.getElementById('patientName_top').innerHTML = data[0].full_name;
                document.getElementById('addrStreet1').value = data[0].addr_street1;
                document.getElementById('addrStreet2').value = data[0].addr_street2;
                document.getElementById('phoneHome').value = data[0].phone_home;
                document.getElementById('addrCity').value = data[0].addr_city;
                //document.getElementById('phoneWork').value = data[0].phoneWork;
                document.getElementById('addrState').value = data[0].addr_state;
                document.getElementById('email').value = data[0].email;
                document.getElementById('phoneMobile').value = data[0].phone_mobile;
                document.getElementById('addrZip').value = data[0].addr_zip;
                if(data[0].primary_care_phy == undefined || data[0].primary_care_phy == 0){
                    document.getElementById('primaryCarePhy').value = "";
                }
                else{
                    document.getElementById('primaryCarePhy').value = data[0].primary_care_phy + ' - ' +data[0].primarycarephysician;
                }
                //document.getElementById('defaultServiceLoc').value = data[0].defaultServiceLoc;
                if(data[0].is_financial_resp == "1"){
                    $('#IsFinancialResp').prop('checked',true);
                    $.ajax({
                        type: "POST",
                        url: root_url+"guarantor/get",
                        async: false,
                        data:{
                            patientID: temp
                        },
                        success:function(data){
                            data = JSON.parse(data);
                            document.getElementById('guarantorName').value = data[0].guarantorName;
                            document.getElementById('guarantorAddrStreet').value = data[0].guarantorStreet;
                            document.getElementById('guarantorAddrCity').value = data[0].guarantorCity;
                            document.getElementById('guarantorAddrState').value = data[0].guarantorState;
                            document.getElementById('guarantorAddrZip').value = data[0].guarantorZip;
                            $('#guarantorRelation').val(data[0].guarantorRelationship);
                        }
                    });
                }
            }
        });

        $.post(root_url + "cases/list",
            {
                patientID: temp,
            },
            function(data, status){
                var res = JSON.parse(data);
                if(res.status == 0){
                    return;
                }
                var data = res.data
                $('#ddlcaseList').html('');
                //iterate over the data and append a select option
                $('#ddlcaseList').append('<option>-- Select Cases --</option>');
                $.each(data, function(key, val){
                    $.ajax({
                        async : false,
                        type: "POST",
                        url: root_url+"cases/getgroup",
                        data:{
                            "caseID" : val.id
                        },success:function(result){
                            var res = JSON.parse(result);
                            if(res.status == 0){
                                return;
                            }
                            res = res.data
                            $('#ddlcaseList').append('<option id="' + val.chart_num + '" value="' + val.chart_num + '">' + res[0].chart_num + ' - ('+res[0].payer_name+') - '+res[0].description+' - (Dt : '+changeDateFormat(res[0].start_date)+' & '+changeDateFormat(res[0].end_date)+')</option>');
                        }
                    });
                });
                $('#ddlActcaseList').html('');
                $('#ddlActcaseList').append('<option>-- Select Cases --</option>');
                $.each(data, function(key, val){
                    $.ajax({
                        async : false,
                        type: "POST",
                        url: root_url+"cases/getgroup",
                        data:{
                            "caseID" : val.id
                        },success:function(result){
                            var res = JSON.parse(result);
                            if(res.status == 0){
                                return;
                            }
                            res = res.data
                            $('#ddlActcaseList').append('<option id="' + val.chart_num + '" value="' + val.chart_num + '">' + res[0].chart_num + ' - ('+res[0].payer_name+') - '+res[0].description+' - (Dt : '+changeDateFormat(res[0].start_date)+' & '+changeDateFormat(res[0].end_date)+')</option>');
                        }
                    });
                });
                var ddlVal = $("#ddlActcaseList").val();
                $.ajax({
                    async : false,
                    type: "POST",
                    url:root_url+"cases/getcaseid",
                    data:{
                        "caseChartNo" : ddlVal
                    },success:function(result){
                        var caseID = result;
                        $.ajax({
                            async: false,
                            type: "POST",
                            url : root_url + 'authinfo/list',
                            data: {
                                caseID: caseID,
                            }, success: function (data) {
                                $('#modalddlAuth').html('');
                                //iterate over the data and append a select option
                                data = JSON.parse(data);
                                $('#modalddlAuth').append('<option>-- Select Auth --</option>');
                                $.each(data, function(key, val){
                                    $('#modalddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                                });
                            }
                        });
                    }
                });
            });

        $("#linkEditAuth1").click(function(){
            $('#EditAuthModal').modal('show');
            var tempAuth = $("#EditauthNo1").val();
            document.getElementById("_hdnAuthID").value = tempAuth;
            $.ajax({
                async : false,
                type: "POST",
                url : root_url+"authinfo/getinfobyauthnum",
                data:{
                    "authNo" : tempAuth
                },success:function(data){
                    var n = data.length;
                    data = data.substr(0,n-1);
                    var res = JSON.parse(data);
                    document.getElementById("EdittempAuth").value = res[0].authNo;
                    document.getElementById("Edittemptos").value = res[0].tos;
                    document.getElementById("EdittempAuthstartDt").value = res[0].start_date;
                    document.getElementById("EdittempAuthendDt").value = res[0].end_date;
                }
            });
        });
        $("#linkEditAuth2").click(function(){
            $('#EditAuthModal').modal('show');
            var tempAuth = $("#EditauthNo2").val();
            document.getElementById("_hdnAuthID").value = tempAuth;
            $.ajax({
                async : false,
                type: "POST",
                url : root_url+"authinfo/getinfobyauthnum",
                data:{
                    "authNo" : tempAuth
                },success:function(data){
                    var n = data.length;
                    data = data.substr(0,n-1);
                    var res = JSON.parse(data);
                    document.getElementById("EdittempAuth").value = res[0].authNo;
                    document.getElementById("Edittemptos").value = res[0].tos;
                    document.getElementById("EdittempAuthstartDt").value = res[0].start_date;
                    document.getElementById("EdittempAuthendDt").value = res[0].end_date;
                }
            });
        });

        $('#EditauthNo1').change(function(){
            var val = $(this).val();
            $.ajax({
                async : false,
                type: "POST",
                url : root_url+"authinfo/getinfobyauthnum",
                data:{
                    "authNo" : val
                },success:function(data){
                    var n = data.length;
                    data = data.substr(0,n-1);
                    var res = JSON.parse(data);
                    document.getElementById("Edittos1").value = res[0].tos;
                    document.getElementById("EditAuthstartDt1").value = res[0].start_date;
                    document.getElementById("EditAuthendDt1").value = res[0].end_date;
                }
            });
        });

        $('#EditauthNo2').change(function(){
            var val = $(this).val();
            $.ajax({
                async : false,
                type: "POST",
                url : root_url+"authinfo/getinfobyauthnum",
                data:{
                    "authNo" : val
                },success:function(data){
                    var n = data.length;
                    data = data.substr(0,n-1);
                    var res = JSON.parse(data);
                    document.getElementById("Edittos2").value = res[0].tos;
                    document.getElementById("EditAuthstartDt2").value = res[0].start_date;
                    document.getElementById("EditAuthendDt2").value = res[0].end_date;
                }
            });
        });

        $("#linkAddAuth").click(function(){
            $('#AddAuthModal').modal('show');
        });

        $('#ActPer').change(function(){
            var val = $(this).val();
            if(val == "Unit"){
                $("#divPerTime").show();
            }
            else{
                $("#divPerTime").hide();
            }
        });

        $('#ActMaxi1').change(function(){
            if($(this).val() == "Amount"){
                $('#ActPer1').html("");
                $('#ActPer1').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
            }
            else if($(this).val() == "Sessions"){
                $('#ActPer1').html("");
                $('#ActPer1').append('<option value="Total Auth">Total Auth</option>');
            }
            else{
                $('#ActPer1').html("");
                $('#ActPer1').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
            }
        });
        $('#ActMaxi2').change(function(){
            if($(this).val() == "Amount"){
                $('#ActPer2').html("");
                $('#ActPer2').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
            }
            else if($(this).val() == "Sessions"){
                $('#ActPer2').html("");
                $('#ActPer2').append('<option value="Total Auth">Total Auth</option>');
            }
            else{
                $('#ActPer2').html("");
                $('#ActPer2').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
            }
        });
        $('#ActMaxi3').change(function(){
            if($(this).val() == "Amount"){
                $('#ActPer3').html("");
                $('#ActPer3').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
            }
            else if($(this).val() == "Sessions"){
                $('#ActPer3').html("");
                $('#ActPer3').append('<option value="Total Auth">Total Auth</option>');
            }
            else{
                $('#ActPer3').html("");
                $('#ActPer3').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
            }
        });


        $("#btntempAuth").click(function(){
            var caseChartNo = document.getElementById('ddlcaseList').value;
            $.ajax({
                async : false,
                type: "POST",
                url:root_url+"cases/getcaseid",
                data:{
                    "caseChartNo" : caseChartNo
                },success:function(result){
                    var caseID = result;
                    var patientID = temp;
                    var authNo = document.getElementById('newtempAuth').value;
                    var newtos = document.getElementById('newtos').value;
                    var startDt = convertDate(document.getElementById('newAuthstartDt').value);
                    var endDt = convertDate(document.getElementById('newAuthendDt').value);
                    var temp1 = document.getElementById("__hdnAuthNum").value;
                    if(temp1 == "1"){
                        $.ajax({
                            async : false,
                            type: "POST",
                            url : root_url + 'authinfo/add',
                            data:{
                                "caseID" : caseID,
                                "patientID" : patientID,
                                "authNo" : authNo,
                                "tos" : newtos,
                                "startDt" : startDt,
                                "endDt" : endDt,
                                "policyEntity" : "1"
                            },success:function(result){
                                alertify.success("New Auth # added successfully");
                                window.location.reload();
                            }
                        });
                    }
                    else{
                        $.ajax({
                            async : false,
                            type: "POST",
                            url : root_url + 'authinfo/add',
                            data:{
                                "caseID" : caseID,
                                "patientID" : patientID,
                                "authNo" : authNo,
                                "tos" : newtos,
                                "startDt" : startDt,
                                "endDt" : endDt,
                                "policyEntity" : "2"
                            },success:function(result){
                                alertify.success("New Auth # added successfully");
                                window.location.reload();
                            }
                        });
                    }
                }
            });
        });

        $("#divHours").hide();
        $("#lblUnits").html("Units");
        $("#ddlApprove").change(function(){
            $("#calculate").val();
            if($(this).val()=="Approved Units"){
                $("#lblUnits").html("Units");
                $("#divUnits").show();
                $("#divHours").hide();
            }
            else{
                $("#lblUnits").html("Hours");
                $("#divHours").show();
            }
        })
        $("#ActPer").change(function(){
            if($(this).val() == "Session"){
                $("#lblUnits").hide();
                $("#divHours").show();
            }
            else{
                if($("#calculate").val() != ""){
                    var hours = document.getElementById("ddlApproveHours").value;
                    if(hours == "Weekly"){
                        var date1 = "";
                        var date2 = "";
                        var authAuth = $("#ddlAuth").val();
                        $.ajax({
                            type: "POST",
                            url : root_url+"authinfo/getinfobyauthnum",
                            async: false,
                            data:{
                                authNo : authAuth
                            },
                            success:function(data){
                                var n = data.length;
                                data = data.substr(0,n-1);
                                var res = JSON.parse(data);
                                if(res[0].start_date != ""){date1 = res[0].start_date;}
                                if(res[0].end_date != ""){date2 = res[0].end_date;}
                            }
                        });
                        var splitDate = date1.split('-');
                        var splitDate1 = date2.split('-');

                        date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                        date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                        var tem = diffDays / 7;
                        tem = document.getElementById("calculate").value * tem;
                        if(document.getElementById("ActPer").value == "15 mins"){
                            tem = tem*4;
                        }
                        else if(document.getElementById("ActPer").value == "30 mins"){
                            tem = tem*2;
                        }
                        else {
                            tem = tem*1;
                        }
                        document.getElementById("totAuth").value = Math.floor(tem);
                    }
                    else if(hours=="Monthly"){
                        var date1 = "";
                        var date2 = "";
                        var authAuth = $("#ddlAuth").val();
                        $.ajax({
                            type: "POST",
                            url : root_url+"authinfo/getinfobyauthnum",
                            async: false,
                            data:{
                                authNo : authAuth
                            },
                            success:function(data){
                                var n = data.length;
                                data = data.substr(0,n-1);
                                var res = JSON.parse(data);
                                if(res[0].start_date != ""){date1 = res[0].start_date;}
                                if(res[0].end_date != ""){date2 = res[0].end_date;}
                            }
                        });
                        var splitDate = date1.split('-');
                        var splitDate1 = date2.split('-');

                        date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                        date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                        var tem = diffDays / 30;
                        tem = document.getElementById("calculate").value * tem;
                        if(document.getElementById("ActPer").value == "15 mins"){
                            tem = tem*4;
                        }
                        else if(document.getElementById("ActPer").value == "30 mins"){
                            tem = tem*2;
                        }
                        else {
                            tem = tem*1;
                        }
                        document.getElementById("totAuth").value = Math.floor(tem);
                    }
                    else{
                        var date1 = "";
                        var date2 = "";
                        var authAuth = $("#ddlAuth").val();
                        $.ajax({
                            type: "POST",
                            url : root_url+"authinfo/getinfobyauthnum",
                            async: false,
                            data:{
                                authNo : authAuth
                            },
                            success:function(data){
                                var n = data.length;
                                data = data.substr(0,n-1);
                                var res = JSON.parse(data);
                                if(res[0].start_date != ""){date1 = res[0].start_date;}
                                if(res[0].end_date != ""){date2 = res[0].end_date;}
                            }
                        });
                        var splitDate = date1.split('-');
                        var splitDate1 = date2.split('-');

                        date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                        date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                        //var tem = diffDays / 1;
                        var tem = document.getElementById("calculate").value / 1
                        if(document.getElementById("ActPer").value == "15 mins"){
                            tem = tem*4;
                        }
                        else if(document.getElementById("ActPer").value == "30 mins"){
                            tem = tem*2;
                        }
                        else {
                            tem = tem*1;
                        }
                        document.getElementById("totAuth").value = Math.floor(tem);

                    }
                }
            }
        })
        $("#ddlApproveHours").change(function(){
            var hours = document.getElementById("ddlApproveHours").value;
            if(hours == "Weekly"){
                var date1 = "";
                var date2 = "";
                var authAuth = $("#ddlAuth").val();
                var url = root_url+"authinfo/getinfobyauthnum";
                $.ajax({
                    type: "POST",
                    url:url,
                    async: false,
                    data:{
                        authNo : authAuth
                    },
                    success:function(data){
                        var res = JSON.parse(data);
                        if(res.status == 0){
                            return;
                        }
                        res = res.data;
                        if(res[0].start_date != ""){date1 = res[0].start_date.split(' ')[0];}
                        if(res[0].end_date != ""){date2 = res[0].end_date.split(' ')[0];}
                    }
                });
                var splitDate = date1.split('-');
                var splitDate1 = date2.split('-');

                date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                var tem = diffDays / 7;
                tem = document.getElementById("calculate").value * tem;
                if(document.getElementById("ActPer").value == "15 mins"){
                    tem = tem*4;
                }
                else if(document.getElementById("ActPer").value == "30 mins"){
                    tem = tem*2;
                }
                else {
                    tem = tem*1;
                }
                document.getElementById("totAuth").value = Math.floor(tem);
            }
            else if(hours=="Monthly"){
                var date1 = "";
                var date2 = "";
                var authAuth = $("#ddlAuth").val();
                var url = root_url+"authinfo/getinfobyauthnum";
                $.ajax({
                    type: "POST",
                    url:url,
                    async: false,
                    data:{
                        authNo : authAuth
                    },
                    success:function(data){
                        var res = JSON.parse(data);
                        if(res.status == 0){
                            return;
                        }
                        res = res.data;
                        if(res[0].start_date != ""){date1 = res[0].start_date.split(' ')[0];}
                        if(res[0].end_date != ""){date2 = res[0].end_date.split(' ')[0];}
                    }
                });
                var splitDate = date1.split('-');
                var splitDate1 = date2.split('-');

                date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                var tem = diffDays / 30;
                tem = document.getElementById("calculate").value * tem;
                if(document.getElementById("ActPer").value == "15 mins"){
                    tem = tem*4;
                }
                else if(document.getElementById("ActPer").value == "30 mins"){
                    tem = tem*2;
                }
                else {
                    tem = tem*1;
                }
                document.getElementById("totAuth").value = Math.floor(tem);
            }
            else{
                var date1 = "";
                var date2 = "";
                var authAuth = $("#ddlAuth").val();
                var url = root_url+"authinfo/getinfobyauthnum";
                $.ajax({
                    type: "POST",
                    url:url,
                    async: false,
                    data:{
                        authNo : authAuth
                    },
                    success:function(data){
                        var res = JSON.parse(data);
                        if(res.status == 0){
                            return;
                        }
                        res = res.data;
                        if(res[0].start_date != ""){date1 = res[0].start_date.split(' ')[0];}
                        if(res[0].end_date != ""){date2 = res[0].end_date.split(' ')[0];}
                    }
                });
                var splitDate = date1.split('-');
                var splitDate1 = date2.split('-');

                date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                //var tem = diffDays / 1;
                var tem = document.getElementById("calculate").value / 1
                if(document.getElementById("ActPer").value == "15 mins"){
                    tem = tem*4;
                }
                else if(document.getElementById("ActPer").value == "30 mins"){
                    tem = tem*2;
                }
                else {
                    tem = tem*1;
                }
                document.getElementById("totAuth").value = Math.floor(tem);

            }
        })


        $(document).on('focusout','#calculate',function() {
            if($("#ddlApprove").val()=="Approved Units"){
                var temp = $(this).val();
                document.getElementById("totAuth").value = temp;
            }
            else{
                var hours = document.getElementById("ddlApproveHours").value;
                if(hours == "Weekly"){
                    var date1 = "";
                    var date2 = "";
                    var authAuth = $("#ddlAuth").val();
                    $.ajax({
                        type: "POST",
                        url : root_url+"authinfo/getinfobyauthnum",
                        async: false,
                        data:{
                            authNo : authAuth
                        },
                        success:function(data){
                            var res = JSON.parse(data);
                            if(res.status == 0){
                                return;
                            }
                            res = res.data;
                            if(res[0].start_date != ""){date1 = res[0].start_date.split(' ')[0];}
                            if(res[0].end_date != ""){date2 = res[0].end_date.split(' ')[0];}
                        }
                    });
                    var splitDate = date1.split('-');
                    var splitDate1 = date2.split('-');

                    date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                    date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    var tem = diffDays / 7;
                    tem = document.getElementById("calculate").value * tem;
                    if(document.getElementById("ActPer").value == "15 mins"){
                        tem = tem*4;
                    }
                    else if(document.getElementById("ActPer").value == "30 mins"){
                        tem = tem*2;
                    }
                    else {
                        tem = tem*1;
                    }
                    document.getElementById("totAuth").value = Math.floor(tem);
                }
                else if(hours=="Monthly"){
                    var date1 = "";
                    var date2 = "";
                    var authAuth = $("#ddlAuth").val();
                    $.ajax({
                        type: "POST",
                        url : root_url+"authinfo/getinfobyauthnum",
                        async: false,
                        data:{
                            authNo : authAuth
                        },
                        success:function(data){
                            var res = JSON.parse(data);
                            if(res.status == 0){
                                return;
                            }
                            res = res.data;
                            if(res[0].start_date != ""){date1 = res[0].start_date.split(' ')[0];}
                            if(res[0].end_date != ""){date2 = res[0].end_date.split(' ')[0];}
                        }
                    });
                    var splitDate = date1.split('-');
                    var splitDate1 = date2.split('-');

                    date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                    date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    var tem = diffDays / 30;
                    tem = document.getElementById("calculate").value * tem;
                    if(document.getElementById("ActPer").value == "15 mins"){
                        tem = tem*4;
                    }
                    else if(document.getElementById("ActPer").value == "30 mins"){
                        tem = tem*2;
                    }
                    else {
                        tem = tem*1;
                    }
                    document.getElementById("totAuth").value = Math.floor(tem);
                }
                else{
                    var date1 = "";
                    var date2 = "";
                    var authAuth = $("#ddlAuth").val();
                    $.ajax({
                        type: "POST",
                        url : root_url+"authinfo/getinfobyauthnum",
                        async: false,
                        data:{
                            authNo : authAuth
                        },
                        success:function(data){
                            var res = JSON.parse(data);
                            if(res.status == 0){
                                return;
                            }
                            res = res.data;
                            if(res[0].start_date != ""){date1 = res[0].start_date.split(' ')[0];}
                            if(res[0].end_date != ""){date2 = res[0].end_date.split(' ')[0];}
                        }
                    });
                    var splitDate = date1.split('-');
                    var splitDate1 = date2.split('-');

                    date1 = new Date(splitDate[0],splitDate[1],splitDate[2]);
                    date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    //var tem = diffDays / 1;
                    var tem = document.getElementById("calculate").value / 1;
                    if(document.getElementById("ActPer").value == "15 mins"){
                        tem = tem*4;
                    }
                    else if(document.getElementById("ActPer").value == "30 mins"){
                        tem = tem*2;
                    }
                    else {
                        tem = tem*1;
                    }
                    document.getElementById("totAuth").value = Math.floor(tem);

                }
            }
        });

        $('#IsFinancialResp').change(function(){
            if($('#IsFinancialResp').is(':checked') == true){
                $('#guarantorModal').modal('show');
            }
            else{
                $('#guarantorModal').modal('hide');
            }
        })

        $('#EditauthNo').change(function(){
            authDetails();
        });

        $('#ddlAuth').change(function(){
            getActivityData();
        });

        $('#btnOpenAct').hide();

        $('#btnOpenAct').click(function(){
            $("#modalActivity").modal("show");
            var ddlVal = $("#ddlActcaseList").val();
            $.ajax({
                async : false,
                type: "POST",
                url:root_url+"cases/getcaseid",
                data:{
                    "caseChartNo" : ddlVal
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }
                    var caseID = res.data.case_id;
                    $.ajax({
                        async: false,
                        type: "POST",
                        url : root_url + 'authinfo/getauths',
                        data: {
                            caseID: caseID,
                        }, success: function (data) {
                            $('#modalddlAuth').html('');
                            //iterate over the data and append a select option
                            data = JSON.parse(data).data;
                            $('#modalddlAuth').append('<option>-- Select Auth --</option>');
                            $.each(data, function(key, val){
                                $('#modalddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                            });
                        }
                    });
                }
            });
            document.getElementById("hdnFlagAct").value = "0";
            $("#ActTitleMod").html("Create Activity");
            $("#btnAddActi").val("Add Activity");
            $('#Activity').prop('selectedIndex', -1);
            var auth = $('#ddlAuth').val();
            alert(root_url+"authinfo/gettos");
            $.ajax({
                type: "POST",
                url:root_url+"authinfo/gettos",
                async : false,
                data:{
                    auth : auth
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }
                    var res = res.data;
                    if(res[0].tos =="Behaviour Therapy"){
                        $("#Activity").html('');
                        $("#Activity").append('<option value=""></option><option value="Initial Assessment By BCBA">Initial Assessment By BCBA</option><option value="Follow up Assessment By BCBA">Follow up Assessment By BCBA</option><option value="Direct Behavior Therapy By BCBA">Direct Behavior Therapy By BCBA</option><option value="Direct Behavior Therapy By BcABA">Direct Behavior Therapy By BcABA</option><option value="Direct Behavior Therapy By Para">Direct Behavior Therapy By Para</option><option value="Supervision By BCBA">Supervision By BCBA</option><option value="Social Skills Group By Para">Social Skills Group By Para</option><option value="Social Skills Group By BCBA">Social Skills Group By BCBA</option><option value="Parent Training By BCBA">Parent Training By BCBA</option><option value="Family Group Training By BCBA">Family Group Training By BCBA</option>');
                        $("#ActCPT").html('');
                        $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0360T-0361T">0360T-0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0364T-0365T">0364T-0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0368T-0369T">0368T-0369T</option><option value="0366T">0366T</option><option value="0367T">0367T</option><option value="0366T-0367T">0366T-0367T</option><option value="0370T">0370T</option><option value="0371T">0371T</option><option value="0372T">0372T</option>      <option value="0373T">0373T</option><option value="0374T">0374T</option><option value="0373T-0374T">0373T-0374T</option><option value="H0004">H0004</option><option value="H0031">H0031</option><option value="H0032">H0032</option><option value="H0046">H0046</option><option value="H2000">H2000</option><option value="H2012">H2012</option><option value="H2014">H2014</option><option value="H2016">H2016</option><option value="H2019">H2019</option><option value="H2020">H2020</option><option value="H2021">H2021</option><option value="H2027">H2027</option><option value="H2030">H2030</option><option value="H2031">H2031</option><option value="H2033">H2033</option><option value="S5102">S5102</option><option value="S5108">S5108</option><option value="S5110">S5110</option><option value="S5111">S5111</option><option value="S5130">S5130</option><option value="S5135">S5135</option><option value="S5151">S5151</option>')
                    }
                    else if(res.tos =="Speech Therapy"){
                        $("#Activity").html('');
                        $("#Activity").append('<option value=""></option><option value="Evaluation By Speech Therapist">Evaluation By Speech Therapist</option><option value="Continued Services">Continued Services</option>');
                        $("#ActCPT").html('');
                        $("#ActCPT").append('<option value=""></option><option value="92513">92513</option><option value="92507">92507</option>')
                    }
                    else{
                        $("#Activity").html('');
                        $("#Activity").append('<option value=""></option><option value="Assessment">Assessment</option><option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option><option value="Supervision">Supervision</option><option value="Supervision Parent Training">Supervision Parent Training</option><option value="Report Writing Send to Editor">Report Writing Send to Editor</option><option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option><option value="Report Writing Progress Reports">Report Writing Progress Reports</option>');
                        $("#ActCPT").html('');
                        $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0370T">0370T</option>')
                    }
                }
            });
            $("#ActCPT").val("");
            $("#ActMod1").val("");
            $("#ActMod2").val("");
            $("#ActMod3").val("");
            $("#ActMod4").val("");
            $("#ActPer").val("");
            $("#ActPerTime").val("");
            $("#calculate").val("");
            $("#totAuth").val("");
            $("#ActRate").val("");
            $("#ActMaxi1").val("");
            $("#ActMaxi2").val("");
            $("#ActMaxi3").val("");
            $("#ActPer1").val("");
            $("#ActPer2").val("");
            $("#ActPer3").val("");
            $("#ActIs1").val("");
            $("#ActIs2").val("");
            $("#ActIs3").val("");
        });


        $(document).on('click','.docSave',function() {
            var temp = $(this).attr('class').split(' ')[0];
            var newTemp = temp.replace('saveDoc','');
            var DocID = newTemp;
            var statusVal = "ddlDocStatus"+newTemp;
            var status = document.getElementById(statusVal).value;
            $.ajax({
                type: "POST",
                url : root_url + 'document/update',
                async : false,
                data:{
                    docID : DocID,
                    status : status
                },success:function(result){
                    window.location.reload();
                }
            });
        });

        $('#btnAddActi').click(function(){
            if ($('#modalActivity .validateGeneral1').length > 0) {
                $("#modalActivity .validateGeneral1").each(function () {
                    //alert($(this).val());
                    if ($(this).val() === "") {
                        var changeBorder = $(this).attr('id');
                        document.getElementById(changeBorder).style.border = "1px solid maroon";
                        document.getElementById(changeBorder).focus();
                        alertify.error("Please fill the required fields");
                        return false;
                    }
                    else {
                        $(".alert").remove();
                        $(this).removeClass('validateGeneral1')
                        var changeBorder = $(this).attr('id');
                        document.getElementById(changeBorder).style.border = "1px solid #ccc";
                        if ($('#modalActivity .validateGeneral1').length <= 0) {
                            var flag = document.getElementById("hdnFlagAct").value;
                            if(flag == "0"){
                                var authNo = $("#ddlAuth").val();
                                var caseVal = $("#ddlActcaseList").val();
                                $.ajax({
                                    type: "POST",
                                    url:root_url+"cases/getcaseid",
                                    async: false,
                                    data:{
                                        "caseChartNo" : caseVal,
                                    },success:function(result){
                                        var res = JSON.parse(result);
                                        if(res.status == 0){
                                            return;
                                        }
                                        res = res.data;
                                        document.getElementById("hdncaseID").value = res.case_id;
                                    }
                                });
                                var caseID = document.getElementById("hdncaseID").value;
                                var authNo = $("#modalddlAuth").val();
                                var caseID = document.getElementById("hdncaseID").value;
                                var activityName = $("#Activity").val();
                                var activityCPT = $("#ActCPT").val();
                                var activityMod1 = $("#ActMod1").val();
                                var activityMod2 = $("#ActMod2").val();
                                var activityMod3 = $("#ActMod3").val();
                                var activityMod4 = $("#ActMod4").val();
                                var activityBilledPer = $("#ActPer").val();
                                var activityBilledPerTime = $("#ActPerTime").val();
                                var activityRates = $("#ActRate").val();
                                // var activityMax1 = $("#ActMaxi1").val();
                                // var activityMax2 = $("#ActMaxi2").val();
                                // var activityMax3 = $("#ActMaxi3").val();
                                // var activityPer1 = $("#ActPer1").val();
                                // var activityPer2 = $("#ActPer2").val();
                                // var activityPer3 = $("#ActPer3").val();
                                // var activityIs1 = $("#ActIs1").val();
                                // var activityIs2 = $("#ActIs2").val();
                                // var activityIs3 = $("#ActIs3").val();
                                var totAuth = $("#totAuth").val();
                                var aType = $("#ddlApprove").val();
                                var authType="";
                                if(aType=="Approved Units"){
                                    authType = "Total Auth";
                                }
                                else{
                                    authType = document.getElementById("ddlApproveHours").value;
                                }
                                $.ajax({
                                    type: "POST",
                                    url : root_url + 'activity/add',
                                    async: false,
                                    data:{
                                        "authNo" : authNo,
                                        "caseID" : caseID,
                                        "activityName" : activityName,
                                        "activityCPT" : activityCPT,
                                        "activityMod1" : activityMod1,
                                        "activityMod2" : activityMod2,
                                        "activityMod3" : activityMod3,
                                        "activityMod4" : activityMod4,
                                        "activityBilledPer" : activityBilledPer,
                                        "activityBilledPerTime" : activityBilledPerTime,
                                        "authType" : authType,
                                        "totAuth" : totAuth,
                                        "remainAuth" : totAuth,
                                        "activityRates" : activityRates,
                                        // "activityMax1" : activityMax1,
                                        // "activityMax2" : activityMax2,
                                        // "activityMax3" : activityMax3,
                                        // "activityPer1" : activityPer1,
                                        // "activityPer2" : activityPer2,
                                        // "activityPer3" : activityPer3,
                                        // "activityIs1" : activityIs1,
                                        // "activityIs2" : activityIs2,
                                        // "activityIs3" : activityIs3,
                                    },success:function(result){
                                        var res = JSON.parse(result);
                                        if(res.status == 0){
                                            return;
                                        }
                                        res = res.data;
                                        alertify.success(res.message);
                                        $('#Activity').prop('selectedIndex', -1);
                                        $("#ActCPT").val("");
                                        $("#ActMod1").val("");
                                        $("#ActMod2").val("");
                                        $("#ActMod3").val("");
                                        $("#ActMod4").val("");
                                        $("#ActPer").val("");
                                        $("#ActPerTime").val("");
                                        $("#calculate").val("");
                                        $("#totAuth").val("");
                                        $("#ActRate").val("");
                                        $("#ActMaxi1").val("");
                                        $("#ActMaxi2").val("");
                                        $("#ActMaxi3").val("");
                                        $("#ActPer1").val("");
                                        $("#ActPer2").val("");
                                        $("#ActPer3").val("");
                                        $("#ActIs1").val("");
                                        $("#ActIs2").val("");
                                        $("#ActIs3").val("");
                                        getActivityData();
                                        $("#modalActivity").modal("hide");
                                        //window.location.href = "ViewPatient.php";
                                    }
                                });
                            }
                            else{

                            }
                        }
                    }
                });
            }
            else{
                var flag = document.getElementById("hdnFlagAct").value;
                if(flag == "0"){
                    var authNo = $("#ddlAuth").val();
                    var caseVal = $("#ddlActcaseList").val();
                    $.ajax({
                        type: "POST",
                        url:root_url+"cases/getcaseid",
                        async: false,
                        data:{
                            "caseChartNo" : caseVal,
                        },success:function(result){
                            document.getElementById("hdncaseID").value = result;
                        }
                    });
                    var caseID = document.getElementById("hdncaseID").value;
                    var authNo = $("#modalddlAuth").val();
                    var caseID = document.getElementById("hdncaseID").value;
                    var activityName = $("#Activity").val();
                    var activityCPT = $("#ActCPT").val();
                    var activityMod1 = $("#ActMod1").val();
                    var activityMod2 = $("#ActMod2").val();
                    var activityMod3 = $("#ActMod3").val();
                    var activityMod4 = $("#ActMod4").val();
                    var activityBilledPer = $("#ActPer").val();
                    var activityBilledPerTime = $("#ActPerTime").val();
                    var activityRates = $("#ActRate").val();
                    // var activityMax1 = $("#ActMaxi1").val();
                    // var activityMax2 = $("#ActMaxi2").val();
                    // var activityMax3 = $("#ActMaxi3").val();
                    // var activityPer1 = $("#ActPer1").val();
                    // var activityPer2 = $("#ActPer2").val();
                    // var activityPer3 = $("#ActPer3").val();
                    // var activityIs1 = $("#ActIs1").val();
                    // var activityIs2 = $("#ActIs2").val();
                    // var activityIs3 = $("#ActIs3").val();
                    var totAuth = $("#totAuth").val();
                    var aType = $("#ddlApprove").val();
                    var authType="";
                    if(aType=="Approved Units"){
                        authType = "Total Auth";
                    }
                    else{
                        authType = document.getElementById("ddlApproveHours").value;
                    }
                    $.ajax({
                        type: "POST",
                        url : root_url + 'activity/add',
                        async: false,
                        data:{
                            "authNo" : authNo,
                            "caseID" : caseID,
                            "activityName" : activityName,
                            "activityCPT" : activityCPT,
                            "activityMod1" : activityMod1,
                            "activityMod2" : activityMod2,
                            "activityMod3" : activityMod3,
                            "activityMod4" : activityMod4,
                            "activityBilledPer" : activityBilledPer,
                            "activityBilledPerTime" : activityBilledPerTime,
                            "authType" : authType,
                            "totAuth" : totAuth,
                            "remainAuth" : totAuth,
                            "activityRates" : activityRates,
                            // "activityMax1" : activityMax1,
                            // "activityMax2" : activityMax2,
                            // "activityMax3" : activityMax3,
                            // "activityPer1" : activityPer1,
                            // "activityPer2" : activityPer2,
                            // "activityPer3" : activityPer3,
                            // "activityIs1" : activityIs1,
                            // "activityIs2" : activityIs2,
                            // "activityIs3" : activityIs3,
                        },success:function(result){
                            if(result =="added"){
                                alertify.success("Activity added successfully");
                                $('#Activity').prop('selectedIndex', -1);
                                $("#ActCPT").val("");
                                $("#ActMod1").val("");
                                $("#ActMod2").val("");
                                $("#ActMod3").val("");
                                $("#ActMod4").val("");
                                $("#ActPer").val("");
                                $("#ActPerTime").val("");
                                $("#calculate").val("");
                                $("#totAuth").val("");
                                $("#ActRate").val("");
                                $("#ActMaxi1").val("");
                                $("#ActMaxi2").val("");
                                $("#ActMaxi3").val("");
                                $("#ActPer1").val("");
                                $("#ActPer2").val("");
                                $("#ActPer3").val("");
                                $("#ActIs1").val("");
                                $("#ActIs2").val("");
                                $("#ActIs3").val("");
                                getActivityData();
                                $("#modalActivity").modal("hide");
                            }
                            else{
                                alertify.error(result);
                            }
                            //window.location.href = "ViewPatient.php";
                        }
                    });
                }
            }

        });

        $(document).on('click','.del',function() {
            var temp = $(this).attr('class').split(' ')[1];
            var newTemp = temp.replace('del','');
            var ActID = newTemp;
            var url = root_url + 'activity/list';
            $.ajax({
                type: "POST",
                url:url,
                async : false,
                data:{
                    actID : ActID,
                },success:function(result){
                    var res = JSON.parse(result);
                    var totalAuth = res[0].totalAuth;
                    var remainAuth = res[0].remainAuth;
                    if(totalAuth == remainAuth){
                        var url = root_url + 'activity/delete';
                        $.ajax({
                            type: "POST",
                            url:url,
                            async : false,
                            data:{
                                actID : ActID,
                            },success:function(result){
                                alertify.success(result);
                                getActivityData();
                            }
                        });
                    }
                    else{
                        alertify.error("Sorry, This Activity has been used. So unable to delete it!!")
                    }
                }
            });
        });

        $(document).on('click','.act',function() {
            var temp = $(this).attr('class').split(' ')[1];
            var newTemp = temp.replace('act','');
            var ActID = newTemp;
            $("#modalActivity").modal('show');
            var url = root_url + 'activity/list';
            $.ajax({
                type: "POST",
                url:url,
                async : false,
                data:{
                    actID : ActID,
                },success:function(result){
                    var res = JSON.parse(result);
                    var auth = $('#ddlAuth').val();
                    $.ajax({
                        type: "POST",
                        url:root_url+"authinfo/gettos",
                        data:{
                            auth : auth
                        },success:function(result){
                            var res = JSON.parse(result);
                            if(res.status == 0){
                                return;
                            }
                            var res = res.data;
                            if(res.tos =="Behaviour Therapy"){
                                $("#Activity").html('');
                                $("#Activity").append('<option value=""></option><option value="Initial Assessment By BCBA">Initial Assessment By BCBA</option><option value="Follow up Assessment By BCBA">Follow up Assessment By BCBA</option><option value="Direct Behavior Therapy By BCBA">Direct Behavior Therapy By BCBA</option><option value="Direct Behavior Therapy By BcABA">Direct Behavior Therapy By BcABA</option><option value="Direct Behavior Therapy By Para">Direct Behavior Therapy By Para</option><option value="Supervision By BCBA">Supervision By BCBA</option><option value="Social Skills Group By Para">Social Skills Group By Para</option><option value="Social Skills Group By BCBA">Social Skills Group By BCBA</option><option value="Parent Training By BCBA">Parent Training By BCBA</option><option value="Family Group Training By BCBA">Family Group Training By BCBA</option>');
                                $("#ActCPT").html('');
                                $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0360T-0361T">0360T-0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0364T-0365T">0364T-0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0368T-0369T">0368T-0369T</option><option value="0366T">0366T</option><option value="0367T">0367T</option><option value="0366T-0367T">0366T-0367T</option><option value="0370T">0370T</option><option value="0371T">0371T</option><option value="0372T">0372T</option>      <option value="0373T">0373T</option><option value="0374T">0374T</option><option value="0373T-0374T">0373T-0374T</option><option value="H0004">H0004</option><option value="H0031">H0031</option><option value="H0032">H0032</option><option value="H0046">H0046</option><option value="H2000">H2000</option><option value="H2012">H2012</option><option value="H2014">H2014</option><option value="H2016">H2016</option><option value="H2019">H2019</option><option value="H2020">H2020</option><option value="H2021">H2021</option><option value="H2027">H2027</option><option value="H2030">H2030</option><option value="H2031">H2031</option><option value="H2033">H2033</option><option value="S5102">S5102</option><option value="S5108">S5108</option><option value="S5110">S5110</option><option value="S5111">S5111</option><option value="S5130">S5130</option><option value="S5135">S5135</option><option value="S5151">S5151</option>')
                            }
                            else if(res.tos =="Speech Therapy"){
                                $("#Activity").html('');
                                $("#Activity").append('<option value=""></option><option value="Evaluation By Speech Therapist">Evaluation By Speech Therapist</option><option value="Continued Services">Continued Services</option>');
                                $("#ActCPT").html('');
                                $("#ActCPT").append('<option value=""></option><option value="92513">92513</option><option value="92507">92507</option>')
                            }
                            else{
                                $("#Activity").html('');
                                $("#Activity").append('<option value=""></option><option value="Assessment">Assessment</option><option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option><option value="Supervision">Supervision</option><option value="Supervision Parent Training">Supervision Parent Training</option><option value="Report Writing Send to Editor">Report Writing Send to Editor</option><option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option><option value="Report Writing Progress Reports">Report Writing Progress Reports</option>');
                                $("#ActCPT").html('');
                                $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0370T">0370T</option>')
                            }
                        }
                    });
                    var ddlVal = $("#ddlActcaseList").val();
                    $.ajax({
                        type: "POST",
                        url:root_url+"cases/getcaseid",
                        data:{
                            "caseChartNo" : ddlVal
                        },success:function(result){
                            var caseID = result;
                            $.ajax({
                                async: false,
                                type: "POST",
                                url : root_url + 'authinfo/list',
                                data: {
                                    caseID: caseID,
                                }, success: function (data) {
                                    $('#modalddlAuth').html('');
                                    //iterate over the data and append a select option
                                    data = JSON.parse(data);
                                    $('#modalddlAuth').append('<option>-- Select Auth --</option>');
                                    $.each(data, function(key, val){
                                        $('#modalddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                                    });
                                }
                            });
                        }
                    });
                    $("#ActTitleMod").html("Update Activity");
                    $("#btnAddActi").val("Update Activity");
                    document.getElementById("hdnFlagAct").value = "1";
                    $("#Activity").val(res[0].activityName);
                    //document.getElementById("Activity").value = res[0].activityName;
                    document.getElementById("ActCPT").value = res[0].activityCPT;
                    document.getElementById("ActMod1").value = res[0].activityMod1;
                    document.getElementById("ActMod2").value = res[0].activityMod2;
                    document.getElementById("ActMod3").value = res[0].activityMod3;
                    document.getElementById("ActMod4").value = res[0].activityMod4;
                    document.getElementById("ActPer").value = res[0].activityBilledPer;
                    document.getElementById("ActRate").value = res[0].activityRates;
                    // document.getElementById("ActMaxi1").value = res[0].activityMax1;
                    // document.getElementById("ActMaxi2").value = res[0].activityMax2;
                    // document.getElementById("ActMaxi3").value = res[0].activityMax3;
                    // document.getElementById("ActPer1").value = res[0].activityPer1;
                    // document.getElementById("ActPer2").value = res[0].activityPer2;
                    // document.getElementById("ActPer3").value = res[0].activityPer3;
                    // document.getElementById("ActIs1").value = res[0].activityIs1;
                    // document.getElementById("ActIs2").value = res[0].activityIs2;
                    // document.getElementById("ActIs3").value = res[0].activityIs3;
                }
            });
        });

        $(document).on('click','.noteEdit',function() {
            var temp = $(this).attr('class').split(' ')[0];
            var newTemp = temp.replace('editNote','');
            document.getElementById('hdnDocID').value = newTemp;
            $('#DocumentNotes').modal("show");
        });

        $('#btnDocNotes').click(function(){
            var notes = document.getElementById("Documentnotes").value;
            var DocID = document.getElementById("hdnDocID").value;
            $.ajax({
                type: "POST",
                url:root_url + "document/update",
                async : false,
                data:{
                    docID : DocID,
                    notes : notes
                },success:function(result){
                    $('#DocumentNotes').modal("hide");
                    window.location.reload();
                }
            });
        })

        $("#formUpload").submit(function(){
            alert("Submitted");
        });

        $("#btnSub").submit(function(){
            window.location.href = "UploadFile.php";
        });

        $("#addAuthModal").click(function(){
            $('#authModal').modal('show');
        });

        $('#divAlert').hide();
        $.ajax({
            type: "POST",
            url: root_url+"document/get",
            async : false,
            data:{
                patientID : temp
            },success:function(data){
                var dt = [];
                var res = JSON.parse(data);
                if(res.status == 0){
                    return;
                }
                var data1 = res.data;
                $.each(data1,function(i,v) {
                    dt.push([data1[i].docID,data1[i].docName,data1[i].CreatedOn,data1[i].docPath]);
                });
                $('#documentTable').DataTable({
                    "data": dt,
                    "autoWidth": false,
                    "columnDefs": [
                        { "width": "10px", "targets": 0 },
                        { "width": "200px", "targets": 1 },
                        { "width": "100px", "targets": 2 },
                        { "width": "70px", "targets": 3 },
                    ],
                    columns: [
                        {"title": "docID","visible" : false},
                        {"title": "Name", "width": "60px",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Uploaded On",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="'+data+'" download><span class="ti-download"></span></a>';
                            }
                        },
                    ]
                });
                $("#documentTable").css({"border":"1px solid #333"});
            }

        });


        // $.ajax({
        //       type: "POST",
        //       url:"getAlerts.php",
        //       async : false,
        //       data:{
        //         alert_patientID : temp
        //       },success:function(result){
        //         var res = JSON.parse(result);
        //         if(res!=""){
        //           if(res[0].alerts_EditPatient == "1"){
        //               $('#divAlert').show();
        //               document.getElementById("alertTop").innerHTML = res[0].alert_desc;
        //           }
        //           else{
        //             $('#divAlert').hide();
        //           }
        //         }
        //     }
        //   });

        $("#saveAlert").click(function(){
            var alert_patientID = temp;
            var alert_desc = $('#alertMsg').val();
            var alerts_Charge = 0;
            var alerts_Deposits = 0;
            var alerts_Statements = 0;
            var alerts_EditPatient = 0;
            var alerts_ScheduleApp = 0;
            var alerts_ViewClaims = 0;
            if($('#chkalerts_Charge').is(':checked') == true){
                alerts_Charge = "1";
            }
            else{
                alerts_Charge = "0";
            }
            if($('#chkalerts_Deposits').is(':checked') == true){
                alerts_Deposits = "1";
            }
            else{
                alerts_Deposits = "0";
            }
            if($('#chkalerts_Statements').is(':checked') == true){
                alerts_Statements = "1";
            }
            else{
                alerts_Statements = "0";
            }
            if($('#chkalerts_EditPatient').is(':checked') == true){
                alerts_EditPatient = "1";
            }
            else{
                alerts_EditPatient = "0";
            }
            if($('#chkalerts_ScheduleApp').is(':checked') == true){
                alerts_ScheduleApp = "1";
            }
            else{
                alerts_ScheduleApp = "0";
            }
            if($('#chkalerts_ViewClaims').is(':checked') == true){
                alerts_ViewClaims = "1";
            }
            else{
                alerts_ViewClaims = "0";
            }
            $.ajax({
                type: "POST",
                url: root_url+"alerts/save",
                async : false,
                data:{
                    alert_patientID : alert_patientID,
                    alert_desc : alert_desc,
                    alerts_Statements : alerts_Statements,
                    alerts_Deposits : alerts_Deposits,
                    alerts_Charge : alerts_Charge,
                    alerts_ViewClaims : alerts_ViewClaims,
                    alerts_EditPatient : alerts_EditPatient,
                    alerts_ScheduleApp : alerts_ScheduleApp
                },success:function(result){
                    if(result == "add"){
                        alertify.success("Alert added successfully");
                        window.location.reload();
                    }
                    else{
                        alertify.success("Alert updated successfully");
                        window.location.reload();
                    }
                }
            });
        });

        $(document).on('focusout','#addrZip',function() {
            var currValue = $(this).val();
            $.ajax({
                type: "POST",
                url: root_url+"zipinfo/get",
                async: false,
                data:{
                    zip : currValue
                },
                success:function(result){
                    var n = result.length;
                    result = result.substr(0,n-1);
                    if(result != "none"){
                        var res = JSON.parse(result);
                        document.getElementById("addrState").value = res[0].state;
                        document.getElementById("addrCity").value = res[0].city;
                    }
                    else{
                        alertify.error("Sorry! This Zip is not available in our Database.")
                    }
                }
            });
        });

        var inv = "";
        $.ajax({
            type: "GET",
            url: root_url+"edi/getlast",
            async : false,
            data:{
            },success:function(result){
                inv = parseInt(result)+1;
                inv = pad(inv, 5);
            }
        });

        $("#btnPrint").click(function(){
            var claimID = document.getElementById('hdnClaimID').value;
            var ClaimNumber = document.getElementById('hdnClaimNo').value;
            if($("input[name='BG']:checked").val() == "Without BG"){
                $.ajax({
                    type: "POST",
                    url:"HCFA-500.php",
                    async : false,
                    data:{
                        ClaimNumber : ClaimNumber
                    },success:function(result){
                        alertify.success("EDI generated successfully");
                        window.open('HCFA-500.pdf','_blank');
                    }
                });
            }
            else{
                $.ajax({
                    type: "POST",
                    url:"HCFA-500-BG.php",
                    async : false,
                    data:{
                        ClaimNumber : ClaimNumber
                    },success:function(result){
                        alertify.success("EDI generated successfully");
                        window.open('HCFA-500-BG.pdf','_blank');
                    }
                });
            }
        });

        $(document).on('click','.Editclaim',function() {
            var temp = $(this).attr('class').split(' ')[0];
            var newTemp = temp.replace('edit','');
            $.ajax({
                type: "POST",
                url: root_url+"claims/list",
                async : false,
                data:{
                    claimID : newTemp
                },success:function(result){
                    $("#claimDetails").modal("hide");
                    $('#EditClaim').modal('show');
                    $.ajax({
                        type: "POST",
                        url: root_url+"claims/history",
                        data:{
                            claimID : newTemp
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#historyClaims").html("");
                            for(var i=0;i < res.length;i++){
                                $("#historyClaims").append("<span>"+changeDateFormat(res[i].created_At.split(" ")[0])+" "+ res[i].created_At.split(" ")[1] +" : "+res[i].notes+"</span><br>");
                            }
                        }
                    });
                    var n = result.length;
                    result = result.substr(0,n-1);
                    var res = JSON.parse(result);
                    document.getElementById('fromDt').value = changeDateFormat(res[0].fromDt);
                    document.getElementById('hdnClaimID').value = res[0].claimID;
                    document.getElementById('hdnClaimNo').value = res[0].ClaimNumber;
                    document.getElementById('toDt').value = changeDateFormat(res[0].toDt);
                    document.getElementById('proced').value = res[0].proced;
                    document.getElementById('billDt').value = changeDateFormat(res[0].toDt);
                    document.getElementById('units').value = res[0].units;
                    document.getElementById('total').value = res[0].total;
                    document.getElementById('allowed').value = res[0].allowed;
                    document.getElementById('paid').value = res[0].paid;
                    document.getElementById('adjustment').value = res[0].adjustment;
                    document.getElementById('claimBalance').value = res[0].claimBalance;
                    // document.getElementById('diag1').value = res[0].diag1;
                    // document.getElementById('diag2').value = res[0].diag2;
                    // document.getElementById('diag3').value = res[0].diag3;
                    // document.getElementById('diag4').value = res[0].diag4;
                    document.getElementById('hdnCaseChart').value = res[0].caseChartNo;
                    document.getElementById('hdnBal').value = res[0].claimBalance;
                    document.getElementById('hdnCurrInsurance').value = res[0].insuranceID;
                    $("#claimStatus").val(res[0].claimStatus);
                }
            });
        });

        $(document).on('click','.actionbtn',function() {
            if($(this).val() == "Transfer Balance"){
                $('.lblTitle').html('Transfer Balance :');
                $('.lblSub1').html('Policy :');
                $('.lblSub2').html('Amount :');
                var claimBal = document.getElementById('hdnBal').value;
                var currInsurance = document.getElementById('hdnCurrInsurance').value;
                $('.txtSub1').html('<select class="form-control" id="ddlPolicy"><option value="-100">SELF</option></select>');
                var tempcaseText = document.getElementById('hdnCaseChart').value;
                for(var i=1; i<4;i++){
                    $.post(root_url + "policies/load",
                        {
                            policyEntity: i,
                            caseChartNo : tempcaseText
                        },
                        function(data1, status){
                            if(data1.length != 0){
                                var insName1 = data1[0].InsuranceName;
                                var insuranceID1 = data1[0].insuranceID;
                                if(insuranceID1 != currInsurance){
                                    $('#ddlPolicy').append('<option value="'+insuranceID1+'">'+insName1+'</option>');
                                }
                            }
                        });
                }
                $('.txtSub2').html('<input type="text" id="txtAmount" value="'+claimBal+'" disabled class="form-control"/>');
                $('.lblSub3').html('');
                $('.txtSub3').html('');
                $('.btnSection').html('<input type="button" id="btnTransfer" value="Transfer" class="btn btn-primary pull-right"/>');
            }
            else if($(this).val() == "Adjustment"){
                $('.lblTitle').html('Adjustment :');
                $('.lblSub1').html('Adjust Amount :');
                $('.lblSub2').html('Adjustment Code :');
                $('.txtSub1').html('<input type="text" id="txtAdjust" class="form-control"/>');
                $('.txtSub2').html('<select class="form-control" id="ddlAdjustmentCd"><option value="0">Default</option><option value="0">General Adjustment</option><option value="02">Contractural Adjustment 1</option><option value="03">Contractural Adjustment 2</option><option value="04">Contractural Adjustment 3</option><option value="05">Capitation Adjustment</option><option value="06">Out Of Network Adjustment</option><option value="07">Small Balance Adjustment</option><option value="08">Courtesy/Charity Adjustment</option><option value="09">Workers Comp Adjustment</option><option value="10">Payment Correction Adjustment</option><option value="11">IRS Tax Deduction</option><option value="12">Patient Refund</option><option value="13">Insurance Refund</option><option value="14">Other Refund</option><option value="15">Small Balance Write-Off</option><option value="16">Collection Account Write-Off</option><option value="17">Settlement Write-Off</option><option value="18">Bad Debt Write-Off</option><option value="19">Debit Adjustment</option><option value="20">Credit Adjustment</option></select>');
                $('.btnSection').html('<input type="button" id="btnAdjust" value="Adjust" class="btn btn-primary pull-right"/>');
            }
            else if($(this).val() == "Note"){
                $('.lblTitle').html('Notes :');
                $('.txtSub1').html('');
                $('.txtSub2').html('');
                $('.lblSub1').html('<textarea class="form-control" style="width:400px; height:80px;" rows="3" cols="6" id="areaNotes"></textarea>');
                $('.lblSub2').html('');
                $('.btnSection').html('<input type="button" id="btnAddNotes" value="Add Note" class="btn btn-primary pull-right"/>');
            }
            else if($(this).val() == "Void"){
                $('.lblTitle').html('');
                $('.lblSub1').html('');
                $('.lblSub2').html('');
                $('.txtSub1').html('');
                $('.txtSub2').html('');
                $('.lblSub3').html('');
                $('.txtSub3').html('');
                $('.btnSection').html('');
            }
            else if($(this).val() == "Settle"){
                $('.lblTitle').html('');
                $('.lblSub1').html('');
                $('.lblSub2').html('');
                $('.txtSub1').html('');
                $('.txtSub2').html('');
                $('.lblSub3').html('');
                $('.txtSub3').html('');
                $('.btnSection').html('');
            }
        });

        $(document).on('click','#btnTransfer',function() {
            var claimID = document.getElementById('hdnClaimID').value;
            var policyID = $('#ddlPolicy').val();
            var amount = $('#txtAmount').val();
            $.ajax({
                type: "POST",
                url: root_url+"claims/updatetransfer",
                async : false,
                data:{
                    claimID : claimID,
                    insuranceID : policyID,
                    claimBalance : amount
                },success:function(result){
                    //console.log(result);
                    //$('#EditClaim').modal('hide');
                    //window.location.reload();
                    alertify.success("Transferred Successfully");
                    $.ajax({
                        type: "POST",
                        url: root_url+"claims/history",
                        data:{
                            claimID : claimID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#historyClaims").html("");
                            for(var i=0;i < res.length;i++){
                                $("#historyClaims").append("<span>"+changeDateFormat(res[i].created_At.split(" ")[0])+" "+ res[i].created_At.split(" ")[1] +" : "+res[i].notes+"</span><br>");
                            }
                        }
                    });
                }
            });
            //$('#EditClaim').modal('hide');
        });

        $(document).on('click','#btnAdjust',function() {
            var claimID = document.getElementById('hdnClaimID').value;
            var amount = $('#txtAdjust').val();
            var adjustCode = $('#ddlAdjustmentCd option:selected').text();
            $.ajax({
                type: "POST",
                url: root_url+"claims/updateadjustment",
                async : false,
                data:{
                    claimID : claimID,
                    claimAdjust : amount,
                    adjustCode : adjustCode
                },success:function(result){
                    alertify.success("Adjusted Successfully");
                    $.ajax({
                        type: "POST",
                        url: root_url+"claims/history",
                        data:{
                            claimID : claimID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#historyClaims").html("");
                            for(var i=0;i < res.length;i++){
                                $("#historyClaims").append("<span>"+changeDateFormat(res[i].created_At.split(" ")[0])+" "+ res[i].created_At.split(" ")[1] +" : "+res[i].notes+"</span><br>");
                            }
                        }
                    });
                }
            });
            //$('#EditClaim').modal('hide');
        });

        $(document).on('click','#btnSettle',function() {
            if (confirm("Do you want to Settle the Charges?")) {
                var claimID = document.getElementById('hdnClaimID').value;
                $.ajax({
                    type: "POST",
                    url: root_url+"claims/updatesettle",
                    async : false,
                    data:{
                        claimID : claimID,
                    },success:function(result){
                        alertify.success("Claim Balance reset and added with adjustment successfully.");
                        $.ajax({
                            type: "POST",
                            url: root_url+"claims/history",
                            data:{
                                claimID : claimID
                            },success:function(result){
                                var res = JSON.parse(result);
                                $("#historyClaims").html("");
                                for(var i=0;i < res.length;i++){
                                    $("#historyClaims").append("<span>"+changeDateFormat(res[i].created_At.split(" ")[0])+" "+ res[i].created_At.split(" ")[1] +" : "+res[i].notes+"</span><br>");
                                }
                            }
                        });
                    }
                });
            }
            return false;
        });

        $(document).on('click','#btnVoid',function() {
            if (confirm("Do you want to Void the Charges?")) {
                var claimID = document.getElementById('hdnClaimID').value;
                $.ajax({
                    type: "POST",
                    url: root_url+"claims/delete",
                    async : false,
                    data:{
                        claimID : claimID,
                    },success:function(result){
                        alertify.success("Claim has been deleted.");
                        $.ajax({
                            type: "POST",
                            url: root_url+"claims/history",
                            data:{
                                claimID : claimID
                            },success:function(result){
                                var res = JSON.parse(result);
                                $("#historyClaims").html("");
                                for(var i=0;i < res.length;i++){
                                    $("#historyClaims").append("<span>"+changeDateFormat(res[i].created_At.split(" ")[0])+" "+ res[i].created_At.split(" ")[1] +" : "+res[i].notes+"</span><br>");
                                }
                            }
                        });
                    }
                });
            }
            return false;
        });

        $(document).on('click','#btnAddNotes',function() {
            var claimID = document.getElementById('hdnClaimID').value;
            var notes = $('#areaNotes').val();
            $.ajax({
                type: "POST",
                url: root_url+"claims/updatehistory",
                data:{
                    claimID : claimID,
                    pou : "Accounts",
                    purpose : "Add Notes for a claim",
                    notes : notes
                },success:function(result){
                    $("#areaNotes").val("")
                    alertify.success("Notes updated successfully");
                    $.ajax({
                        type: "POST",
                        url: root_url+"claims/history",
                        data:{
                            claimID : claimID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#historyClaims").html("");
                            for(var i=0;i < res.length;i++){
                                $("#historyClaims").append("<span>"+changeDateFormat(res[i].created_At.split(" ")[0])+" "+ res[i].created_At.split(" ")[1] +" : "+res[i].notes+"</span><br>");
                            }
                        }
                    });
                }
            });
        });

        $('#btnPaperClaim').click(function(){
            $('#paperClaimModal').modal('show');
        })

        // $.ajax({
        //     type: "POST",
        //     url: root_url + "claims/getaccount",
        //     data: {
        //         patientID: temp
        //     }, success: function (data) {
        //         var data1 = JSON.parse(data);

        //         var dt = [];
        //         $.each(data1,function(i,v) {
        //             dt.push([data1[i].ClaimNumber,data1[i].fromDt,data1[i].TotalCharges,data1[i].Balance,data1[i].payerName,data1[i].fullName]);
        //         });
        //         $('#accountTable').DataTable({
        //             "aaSorting": [[ 0, "desc" ]],
        //             "data": dt,
        //             columns: [
        //                 {"title": "Claim #",
        //                     "render": function ( data, type, full, meta ) {
        //                         return '<a href="javascript:void(0);" class="cNo cNo'+data+'">'+data+'</a>';
        //                     }
        //                 },
        //                 {"title": "DOS",
        //                     "render": function ( data, type, full, meta ) {
        //                         return changeDateFormat(data);
        //                     }
        //                 },
        //                 {"title": "Total Charges",
        //                     "render": function ( data, type, full, meta ) {
        //                         return '$'+data;
        //                     }
        //                 },
        //                 {"title": "Balance",
        //                     "render": function ( data, type, full, meta ) {
        //                         return '$'+data;
        //                     }
        //                 },
        //                 {"title": "Payer Name"},
        //                 {"title": "Patient Name"}
        //             ]
        //         });
        //         $("#accountTable").removeAttr("style");
        //     }
        // });

        $(document).on('click','.cNo',function() {
            //var value = $(this).val();
            var temp = $(this).attr('class').split(' ')[1];
            var newTemp = temp.replace('cNo','');
            sessionStorage.setItem("claimNo", newTemp);
            $("#claimDetails").modal("show");
            $.ajax({
                type: "POST",
                url:root_url + "claims/ledgerdetail",
                data:{
                    ClaimNumber : newTemp
                },success:function(data1){

                    var dt = [];
                    $.each(data1,function(i,v) {
                        if(data1[i].allowed == ""){
                            data1[i].allowed = "0.00";
                        }
                        if(data1[i].paid == ""){
                            data1[i].paid = "0.00";
                        }
                        if(data1[i].adjustment == ""){
                            data1[i].adjustment = "0.00";
                        }

                        if(data1[i].insuranceName == null){
                            data1[i].insuranceName = "Self";
                        }
                        dt.push([data1[i].fromDt,data1[i].toDt,data1[i].proced,data1[i].units,data1[i].total,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,data1[i].insuranceName,data1[i].fullName,data1[i].claimID]);
                    });


                    $('#details').DataTable({
                        destroy: true,
                        searching: false,
                        "aaSorting": [[ 4, "desc" ]],
                        "data": dt,
                        columns: [
                            {"title": "From",
                                "render": function ( data, type, full, meta ) {
                                    return changeDateFormat(data);
                                }
                            },
                            {"title": "To",
                                "render": function ( data, type, full, meta ) {
                                    return changeDateFormat(data);
                                }
                            },
                            {"title": "CPT"},
                            {"title": "Units"},
                            {"title":"Total Charges","mdata": "total",
                                "render": function ( data, type, full, meta ) {
                                    return '$'+data;
                                }
                            },
                            {"title":"Allowed","mdata": "allowed",
                                "render": function ( data, type, full, meta ) {
                                    return '$'+data;
                                }
                            },
                            {"title":"Paid","mdata": "paid",
                                "render": function ( data, type, full, meta ) {
                                    return '$'+data;
                                }
                            },
                            {"title":"Adjustment","mdata": "adjustment",
                                "render": function ( data, type, full, meta ) {
                                    return '$'+data;
                                }
                            },
                            {"title":"Balance","mdata": "claimBalance",
                                "render": function ( data, type, full, meta ) {
                                    return '$'+data;
                                }
                            },
                            {"title":"Payer Name",
                                "render": function ( data, type, full, meta ) {
                                    return data;
                                }
                            },
                            {"title":"Patient Name",
                                "render": function ( data, type, full, meta ) {
                                    return data;
                                }
                            },
                            {"title": "Action",
                                "render": function ( data, type, full, meta ) {
                                    return '<a href="javascript:void(0);" class="edit'+data+' Editclaim"><span class="ti-pencil"></span></a>';
                                }
                            }
                        ]
                    });
                }
            });
        });
        $('#btnUpdateClaim').click(function(){
            var claimID = document.getElementById('hdnClaimID').value;
            var fromDt = convertDate(document.getElementById('fromDt').value);
            var toDt = convertDate(document.getElementById('toDt').value);
            var proced = document.getElementById('proced').value;
            var billDt = convertDate(document.getElementById('billDt').value);
            var units = document.getElementById('units').value;
            var total = document.getElementById('total').value;
            var allowed = document.getElementById('allowed').value;
            var paid = document.getElementById('paid').value;
            var adjustment = document.getElementById('adjustment').value;
            var claimBalance = document.getElementById('claimBalance').value;
            var claimStatus = document.getElementById('claimStatus').value;
            var isPosted = document.getElementById('IsPosted').value;
            var isSent = document.getElementById('IsSent').value;
            var diag1 = document.getElementById('diag1').value;
            var diag2 = document.getElementById('diag2').value;
            var diag3 = document.getElementById('diag3').value;
            var diag4 = document.getElementById('diag4').value;

            $.ajax({
                type: "POST",
                url: root_url+"claims/update",
                data:{
                    claimID : claimID,
                    fromDt : fromDt,
                    toDt : toDt,
                    proced : proced,
                    billDt : billDt,
                    units : units,
                    total : total,
                    allowed : allowed,
                    paid : paid,
                    adjustment : adjustment,
                    claimBalance : claimBalance,
                    claimStatus : claimStatus,
                    isPosted : isPosted,
                    isSent : isSent,
                    diag1 : diag1,
                    diag2 : diag2,
                    diag3 : diag3,
                    diag4 : diag4
                },success:function(result){
                    $('#EditClaim').modal('hide');
                }
            });
        });
        $('#btnRebill').click(function(){
            var claimID = document.getElementById("hdnClaimID").value;
            //var rebillVal = document.getElementById("IsSent").value;
            //if(rebillVal != 0){
            $.ajax({
                type: "POST",
                url: root_url+"claims/refill",
                data:{
                    claimID : claimID
                },
                success:function(result){
                    $.ajax({
                        type: "POST",
                        url: root_url+"claims/history",
                        data:{
                            claimID : claimID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#historyClaims").html("");
                            for(var i=0;i < res.length;i++){
                                $("#historyClaims").append("<span>"+changeDateFormat(res[i].created_At.split(" ")[0])+" "+ res[i].created_At.split(" ")[1] +" : "+res[i].notes+"</span><br>");
                            }
                        }
                    });
                    alertify.success("This claim is added to EDI Queue again");
                }
            });
            //}
            // else{
            //     alertify.error("This claims is not sent already.");
            // }
        });

        var patientList = "";
        patientList = root_url + "patient/list1";
        $("#policyHolder1").autocomplete({
            source: patientList,
            autoFocus:true
        });
        $("#policyHolder2").autocomplete({
            source: patientList,
            autoFocus:true
        });
        $("#policyHolder3").autocomplete({
            source: patientList,
            autoFocus:true
        });
        $("#EditpolicyHolder2").autocomplete({
            source: patientList,
            autoFocus:true
        });
        $("#EditpolicyHolder1").autocomplete({
            source: patientList,
            autoFocus:true
        });
        $("#EditpolicyHolder3").autocomplete({
            source: patientList,
            autoFocus:true
        });

        $('#chkAuth').change(function(){
            if($('#chkAuth').is(':checked') == true){
                $('#authNo').removeAttr('disabled');
                //$('#noOfVisits').removeAttr('disabled');
                $('#totalVisits').removeAttr('disabled');
                $('#lastVisit').removeAttr('disabled');
                $('#principalDiag').removeAttr('disabled');
                $('#defDiag2').removeAttr('disabled');
                $('#defDiag3').removeAttr('disabled');
                $('#defDiag4').removeAttr('disabled');
            }
            else{
                $('#authNo').attr('disabled',true);
                $('#noOfVisits').attr('disabled',true);
                $('#totalVisits').attr('disabled',true);
                $('#lastVisit').attr('disabled',true);
                $('#principalDiag').attr('disabled',true);
                $('#defDiag2').attr('disabled',true);
                $('#defDiag3').attr('disabled',true);
                $('#defDiag4').attr('disabled',true);
            }
        });

        $('#EditchkAuth').change(function(){
            checkAuth();
        });

        var insurances = "";
        insurances = root_url + "insurances/get";
        $("#policyInsurance1").autocomplete({
            source: insurances,
            autoFocus:true
        });
        $("#policyInsurance2").autocomplete({
            source: insurances,
            autoFocus:true
        });
        $("#policyInsurance3").autocomplete({
            source: insurances,
            autoFocus:true
        });
        $("#EditpolicyInsurance1").autocomplete({
            source: insurances,
            autoFocus:true
        });
        $("#EditpolicyInsurance2").autocomplete({
            source: insurances,
            autoFocus:true
        });
        $("#EditpolicyInsurance3").autocomplete({
            source: insurances,
            autoFocus:true
        });


        $(document).on('focus','#principalDiag',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            if(toggleDX != "false"){
                $("#principalDiag").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#principalDiag").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: root_url+"codeicd/list",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#defDiag2',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            if(toggleDX != "false"){
                $("#defDiag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#defDiag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: root_url+"codeicd/list",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#defDiag3',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            if(toggleDX != "false"){
                $("#defDiag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#defDiag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: root_url+"codeicd/list",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#defDiag4',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            if(toggleDX != "false"){
                $("#defDiag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#defDiag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: root_url+"codeicd/list",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        //document.getElementById('hdnDX').value = "true";

        $('#icd').on('change', function(event, state) {
            document.getElementById('hdnDX').value = event.target.checked; // jQuery event
        });


        $(document).on('focus','#diag1',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag1").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag1").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: root_url+"codeicd/list",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag2',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: root_url+"codeicd/list",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag3',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) {
                        $.ajax({
                            url: root_url+"codeicd/list",
                            data: { term: request.term },
                            type: "POST",
                            success: function(data) {
                                response(JSON.parse(data));
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $("#diag4").autocomplete({
            minlength : 3,
            source: function(request, response) {
                $.ajax({
                    url: root_url+"codeicd/list",
                    data: { term: request.term },
                    type: "POST",
                    success: function(data) {
                        response(JSON.parse(data));
                    }
                });
            },
            autoFocus:true
        });

        // $('#Editicdtoggle').on('change', function(event, state) {
        //   document.getElementById('hdnEdittoggleDX').value = event.target.checked; // jQuery event
        // });

        // $(document).on('focus','#EditprincipalDiag',function() {
        //     var toggleDX = document.getElementById('hdnEdittoggleDX').value;
        //     if(toggleDX != "false"){
        //         $("#EditprincipalDiag").autocomplete({
        //             minlength : 3,
        //             source: function(request, response) {
        //                 $.ajax({
        //                     url: "dxSearch9.php",
        //                     data: { term: request.term },
        //                     type: "POST",
        //                     success: function(data) {
        //                         response(JSON.parse(data));
        //                     }
        //                 });
        //             },
        //             autoFocus:true
        //         });
        //     }
        //     else{
        //         $("#EditprincipalDiag").autocomplete({
        //             minlength : 3,
        //             source: function(request, response) {
        //                 $.ajax({
        //                     url: root_url+"codeicd/list",
        //                     data: { term: request.term },
        //                     type: "POST",
        //                     success: function(data) {
        //                         response(JSON.parse(data));
        //                     }
        //                 });
        //             },
        //             autoFocus:true
        //         });
        //     }
        // });

        // $(document).on('focus','#EditdefDiag2',function() {
        //     var toggleDX = document.getElementById('hdnEdittoggleDX').value;
        //     if(toggleDX != "false"){
        //         $("#EditdefDiag2").autocomplete({
        //             minlength : 3,
        //             source: function(request, response) {
        //                 $.ajax({
        //                     url: "dxSearch9.php",
        //                     data: { term: request.term },
        //                     type: "POST",
        //                     success: function(data) {
        //                         response(JSON.parse(data));
        //                     }
        //                 });
        //             },
        //             autoFocus:true
        //         });
        //     }
        //     else{
        //         $("#EditdefDiag2").autocomplete({
        //             minlength : 3,
        //             source: function(request, response) {
        //                 $.ajax({
        //                     url: root_url+"codeicd/list",
        //                     data: { term: request.term },
        //                     type: "POST",
        //                     success: function(data) {
        //                         response(JSON.parse(data));
        //                     }
        //                 });
        //             },
        //             autoFocus:true
        //         });
        //     }
        // });

        // $(document).on('focus','#EditdefDiag3',function() {
        //     var toggleDX = document.getElementById('hdnEdittoggleDX').value;
        //     if(toggleDX != "false"){
        //         $("#EditdefDiag3").autocomplete({
        //             minlength : 3,
        //             source: function(request, response) {
        //                 $.ajax({
        //                     url: "dxSearch9.php",
        //                     data: { term: request.term },
        //                     type: "POST",
        //                     success: function(data) {
        //                         response(JSON.parse(data));
        //                     }
        //                 });
        //             },
        //             autoFocus:true
        //         });
        //     }
        //     else{
        //         $("#EditdefDiag3").autocomplete({
        //             minlength : 3,
        //             source: function(request, response) {
        //                 $.ajax({
        //                     url: root_url+"codeicd/list",
        //                     data: { term: request.term },
        //                     type: "POST",
        //                     success: function(data) {
        //                         response(JSON.parse(data));
        //                     }
        //                 });
        //             },
        //             autoFocus:true
        //         });
        //     }
        // });

        // $(document).on('focus','#EditdefDiag4',function() {
        //     var toggleDX = document.getElementById('hdnEdittoggleDX').value;
        //     if(toggleDX != "false"){
        //         $("#EditdefDiag4").autocomplete({
        //             minlength : 3,
        //             source: function(request, response) {
        //                 $.ajax({
        //                     url: "dxSearch9.php",
        //                     data: { term: request.term },
        //                     type: "POST",
        //                     success: function(data) {
        //                         response(JSON.parse(data));
        //                     }
        //                 });
        //             },
        //             autoFocus:true
        //         });
        //     }
        //     else{
        //         $("#EditdefDiag4").autocomplete({
        //             minlength : 3,
        //             source: function(request, response) {
        //                 $.ajax({
        //                     url: root_url+"codeicd/list",
        //                     data: { term: request.term },
        //                     type: "POST",
        //                     success: function(data) {
        //                         response(JSON.parse(data));
        //                     }
        //                 });
        //             },
        //             autoFocus:true
        //         });
        //     }
        // });


        $('#ssn').mask('000-00-0000');
        $('#phoneHome').mask('(000)000-0000');

        $('#phoneWork').mask('(000)000-0000');

        $('#phoneMobile').mask('(000)000-0000');
        $('#dob').mask('00/00/0000');
        $('#firstConsultDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#firstConsultDate').mask('00-00-0000');

        $('#EditAuthstartDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditAuthstartDt1').mask('00-00-0000');
        $('#EditAuthstartDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditAuthstartDt2').mask('00-00-0000');
        $('#EditAuthendDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditAuthendDt1').mask('00-00-0000');
        $('#EditAuthendDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditAuthendDt2').mask('00-00-0000');
        $('#newAuthstartDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#newAuthstartDt').mask('00-00-0000');

        $('#newAuthendDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#newAuthendDt').mask('00-00-0000');
        $('#EdittempAuthstartDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EdittempAuthstartDt').mask('00-00-0000');
        $('#EdittempAuthendDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EdittempAuthendDt').mask('00-00-0000');
        $('#dateLastSeen').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateLastSeen').mask('00-00-0000');
        $('#refDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#refDate').mask('00-00-0000');
        $('#prescripDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#prescripDate').mask('00-00-0000');
        $('#lastXRay').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastXRay').mask('00-00-0000');
        $('#estimatedDOB').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#estimatedDOB').mask('00-00-0000');
        $('#dateAssumedCare').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateAssumedCare').mask('00-00-0000');
        $('#dateRelinquishedCare').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateRelinquishedCare').mask('00-00-0000');
        $('#lastWorkDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastWorkDate').mask('000-00-0000');
        $('#dob').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#globalCoverageUntil').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#globalCoverageUntil').mask('000-00-0000');
        $('#fromDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#fromDt').mask('00-00-0000');
        $('#billDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#billDt').mask('00-00-0000');
        $('#toDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#toDt').mask('00-00-0000');
        $('#policyStartDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt1').mask('00-00-0000');
        $('#policyEndDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt1').mask('00-00-0000');
        $('#policyStartDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt2').mask('00-00-0000');
        $('#policyEndDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt2').mask('00-00-0000');
        $('#policyStartDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt3').mask('00-00-0000');
        $('#policyEndDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt3').mask('00-00-0000');
        $('#EditpolicyStartDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyStartDt1').mask('00-00-0000');
        $('#EditpolicyEndDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyEndDt1').mask('00-00-0000');
        $('#EditpolicyStartDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyStartDt2').mask('00-00-0000');
        $('#EditpolicyEndDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyEndDt2').mask('00-00-0000');
        $('#EditpolicyStartDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyStartDt3').mask('00-00-0000');
        $('#EditpolicyEndDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyEndDt3').mask('00-00-0000');
        $('#retirementDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#retirementDate').mask('00-00-0000');
        $('#lastVisit').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastVisit').mask('00-00-0000');
        $('#authorizedThrough').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#authorizedThrough').mask('00-00-0000');
        $('#phoneHome').mask('(000)000-0000');
        //$('#phoneWork').mask('(000)000-0000');
        $('#phoneMobile').mask('(000)000-0000');

        $("#btnAddPriPhy").click(function() {
            var primaryPhysiName = $('#PrimaryPhysiName').val();
            var phyDOB = convertDate($('#phyDOB').val());
            var phyDegree = $('#phyDegree').val();
            var phyNPI= $('#phyNPI').val();
            var phyType = $('#phyType').val();
            var phySSN = $('#phySSN').val();
            var phyAddr = $('#phyAddr').val();
            var phyHomePhone = $('#phyHomePhone').val();
            var phyWorkPhone = $('#phyWorkPhone').val();
            var phyMobilePhone = $('#phyMobilePhone').val();
            var phyEmail = $('#phyEmail').val();
            var phyFax = $('#phyFax').val();
            var phyNotes = $('#phyNotes').val();

            $.ajax({
                type: "POST",
                url: root_url+"physician/add",
                data:{
                    "PrimaryPhysiName" : primaryPhysiName,
                    "phyDOB" : phyDOB,
                    "phyDegree" : phyDegree,
                    "phyNPI" : phyNPI,
                    "phyType" : phyType,
                    "phySSN" : phySSN,
                    "phyAddr" : phyAddr,
                    "phyHomePhone" : phyHomePhone,
                    "phyWorkPhone" : phyWorkPhone,
                    "phyMobilePhone" : phyMobilePhone,
                    "phyEmail" : phyEmail,
                    "phyFax" : phyFax,
                    "phyNotes" : phyNotes
                },success:function(result){
                    alertify.success('PrimaryPhysiName updated successfully');
                    $('#PrimaryPhysi').modal('hide');
                    //window.location.reload();
                }

            });
        });
        var fac_url = root_url + 'servicelocation/list';
        $.get(fac_url, function(data, status){
            var data = JSON.parse(data);
            var arrFacility = [];
            $('#Editfacility').html('');
            for(var x in data){
                arrFacility.push(data[x]);
            }
            //alert(arrCase);
            $.each(arrFacility,function(i,v) {
                $('#Editfacility').html('');
                arrFacility.forEach(function(t) {
                    $('#Editfacility').append('<option value="'+t.serviceLocID+'">'+t.internalName+'</option>');
                });
            });
        });

        var fac_url = root_url + 'servicelocation/list';
        $.get(fac_url, function(data, status){
            var data = JSON.parse(data);
            var arrFacility = [];
            $('#facility').html('');
            for(var x in data){
                arrFacility.push(data[x]);
            }
            //alert(arrCase);
            $.each(arrFacility,function(i,v) {
                $('#facility').html('');
                arrFacility.forEach(function(t) {
                    $('#facility').append('<option value="'+t.serviceLocID+'">'+t.internalName+'</option>');
                });
            });
        });

        var loc = "";
        $.ajax({
            type: "POST",
            url: root_url + "servicelocation/list",
            data: {
                practiceID: "1"
            },
            success: function (result) {
                loc = result;
            }
        });

        $("#defaultServiceLoc").autocomplete({
            source: loc,
            autoFocus:true
        });

        var PrimaryPhy = "";
        PrimaryPhy = root_url + "physician/get";

        $("#primaryCarePhy").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#defaultRenderPhy").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#assignedProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#supervisingProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#operatingProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#otherProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#outsideProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });

        $("#EditprimaryCarePhy").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#EditassignedProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#EditsupervisingProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#EditoperatingProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#EditotherProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#EditoutsideProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });


        $("#btnUpdateIns").click(function(){
            updatePolicy();
        });

        var patFullName = "";
        var patDOB = "";
        $.ajax({
            type: "POST",
            url: root_url+"location/list",
            async: false,
            data:{
                patientID: temp
            },
            success:function(data){
                var res = JSON.parse(data);
                if(res.status == 0){
                    return;
                }
                res = res.data
                if(res[0].home == "1"){
                    document.getElementById('homeChk').checked = true;
                }
                else{
                    document.getElementById('homeChk').checked = false;
                }
                if(res[0].office == "1"){
                    document.getElementById('officeChk').checked = true;
                }
                else{
                    document.getElementById('officeChk').checked = false;
                }
                if(res[0].school == "1"){
                    document.getElementById('schoolChk').checked = true;
                }
                else{
                    document.getElementById('schoolChk').checked = false;
                }
            }
        });
        
        $("#divInsured").hide();
        $("#IsInsured").click(function(){
            if($('#IsInsured').prop('checked')){
                $("#divInsured").show();
            }
            else{
                $("#divInsured").hide();
            }
        });

        $("#fullName").click(function(){
            var data = $("#fullName").val();
            var arr = data.split(' ');
            $('#NameModal').modal('show');
            document.getElementById('FirstName').value = arr[0];
            if(arr[1] == undefined){
                document.getElementById('MiddleName').value = '';
            }
            else{
                document.getElementById('MiddleName').value = arr[1];
            }

            if(arr[2] == undefined){
                document.getElementById('LastName').value = '';
            }
            else{
                document.getElementById('LastName').value = arr[2];
            }

        });
        $("#Address").click(function(){
            $('#AddrModal').modal('show');
        });
        $("#linkattachatt").click(function(){
            $('#attach1').modal('show');
        });

        $("#linkPrimaryPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });
        $("#linkRenderPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });
        $("#linkReferPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });

        $("#btnSaveCase").click(function() {
            addCase();
        });

        $("#btnInsurance").click(function(){
            $('#insurance').modal('show');
        });

        $('#ddlcaseList').change(function(){
            var patFullName = document.getElementById("hdnPatFullName").value;
            var patDOB = document.getElementById("hdnPatDOB").value;
            editCase($(this).val(),temp,patFullName,patDOB);
            var ddlVal = $(this).val();
            $.ajax({
                type: "POST",
                url:root_url+"cases/getcaseid",
                data:{
                    "caseChartNo" : ddlVal
                },success:function(result){
                    var caseID = result;
                    $.ajax({
                        async: false,
                        type: "POST",
                        url : root_url + 'cases/getauth1',
                        data: {
                            caseID: caseID,
                        }, success: function (data) {
                            //iterate over the data and append a select option
                            data = JSON.parse(data);
                            //$('#modalddlAuth').append('<option>-- Select Auth --</option>');
                            $.each(data, function(key, val){
                                $('#EditauthNo').append('<option value="' + val.key + '">' + val.label + '</option>');
                            });
                        }
                    });
                }
            });
        })

        $('#btnAddAuth').click(function(){

        });

        $('#linkAddAuth1').click(function(){
            document.getElementById("__hdnAuthNum").value = "1";
            $('#AddAuthModal').modal('show');
        });
        $('#linkAddAuth2').click(function(){
            document.getElementById("__hdnAuthNum").value = "2";
            $('#AddAuthModal').modal('show');
        });

        $('#ddlActcaseList').change(function(){
            var ddlVal = $(this).val();
            $.ajax({
                async : false,
                type: "POST",
                url:root_url+"cases/getcaseid",
                data:{
                    "caseChartNo" : ddlVal
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }
                    var caseID = res.data.case_id;
                    $.ajax({
                        async: false,
                        type: "POST",
                        url : root_url + 'authinfo/getauths',
                        data: {
                            caseID: caseID,
                        }, success: function (data) {
                            $('#ddlAuth').html('');
                            //iterate over the data and append a select option
                            data = JSON.parse(data).data;
                            $('#ddlAuth').append('<option>-- Select Auth --</option>');
                            $.each(data, function(key, val){
                                $('#ddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                            });
                        }
                    });
                }
            });
            //getActivityData();
        });

        // $("#btnEditinsurance").click(function(){
        //     var ddlValue = $('#ddlcaseList').val();
        //     $.post(root_url + "cases/load",
        //     {
        //         caseChartNo: ddlValue,
        //     },
        //     function(data, status){
        //         if(data[0].description != ""){document.getElementById('Editdescription').value = data[0].description;}
        //         if(data[0].auth_num != ""){document.getElementById('EditauthNo').value = data[0].auth_num;}
        //         if(data[0].lastVisit != ""){if(data[0].lastVisit != '0000-00-00 00:00:00'){document.getElementById('EditlastVisit').value = changeDateFormat(data[0].lastVisit);} else{document.getElementById('EditlastVisit').value = '';}}
        //         if(data[0].noOfVisits != ""){document.getElementById('EditnoOfVisits').value = data[0].noOfVisits;}
        //         if(data[0].principalDiag != ""){document.getElementById('EditprincipalDiag').value = data[0].principalDiag;}
        //         if(data[0].defDiag2 != ""){document.getElementById('EditdefDiag2').value = data[0].defDiag2;}
        //         if(data[0].defDiag3 != ""){document.getElementById('EditdefDiag3').value = data[0].defDiag3;}
        //         if(data[0].defDiag4 != ""){document.getElementById('EditdefDiag4').value = data[0].defDiag4;}
        //     });
        //     $.post(root_url + "policies/load",
        //     {
        //         caseChartNo: ddlValue,
        //         policyEntity : "1"
        //     },
        //     function(data, status){
        //         if(data.length!=0){
        //             var patVal1 = data[0].policyHolder;
        //             $.ajax({
        //                 type: "POST",
        //                 url: root_url+"patient/list",
        //                 async: false,
        //                 data:{
        //                     "patientID" : patVal1
        //                     },success:function(result){
        //                         var res = JSON.parse(result);
        //                         sessionStorage.setItem("patName1",res[0].full_name);
        //                 }
        //             });
        //             var patName1 = sessionStorage.getItem("patName1");
        //             if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance1').value = data[0].InsuranceName;}
        //             if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder1').value = temp + ' - ' +patName1;}
        //             if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured1').value = data[0].relationshipToInsured;}
        //             if(data[0].policyNo != ""){document.getElementById('EditpolicyNo1').value = data[0].policyNo;}
        //             if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt1').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt1').value = '';}}
        //             if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo1').value = data[0].groupNo;}
        //             if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt1').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt1').value = '';}}
        //             if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay1').value = data[0].coPayment;}
        //             if(patDOB != ""){document.getElementById('EditpolicyDOB1').value = patDOB;}

        //         }
        //     });

        //     $.post(root_url + "policies/load",
        //     {
        //         caseChartNo: ddlValue,
        //         policyEntity : "2"
        //     },
        //     function(data, status){
        //         if(data.length!=0){
        //             var patVal2 = data[0].policyHolder;
        //             $.ajax({
        //                 type: "POST",
        //                 url: root_url+"patient/list",
        //                 async: false,
        //                 data:{
        //                     "patientID" : patVal2
        //                     },success:function(result){
        //                         var res = JSON.parse(result);
        //                         sessionStorage.setItem("patName2",res[0].full_name);
        //                 }
        //             });
        //             var patName2 = sessionStorage.getItem("patName1");
        //             if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance2').value = data[0].InsuranceName;}
        //             if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder2').value = temp + ' - ' +patName2;}
        //             if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured2').value = data[0].relationshipToInsured;}
        //             if(data[0].policyNo != ""){document.getElementById('EditpolicyNo2').value = data[0].policyNo;}
        //             if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt2').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt2').value = '';}}
        //             if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo2').value = data[0].groupNo;}
        //             if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt2').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt2').value = '';}}
        //             if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay2').value = data[0].coPayment;}
        //             if(patDOB != ""){document.getElementById('EditpolicyDOB2').value = patDOB;}
        //         }
        //     });
        //     $.post(root_url + "policies/load",
        //     {
        //         caseChartNo: ddlValue,
        //         policyEntity : "3"
        //     },
        //     function(data, status){
        //         if(data.length!=0){
        //             var patVal3 = data[0].policyHolder;
        //             $.ajax({
        //                 type: "POST",
        //                 url: root_url+"patient/list",
        //                 async: false,
        //                 data:{
        //                     "patientID" : patVal3
        //                     },success:function(result){
        //                         var res = JSON.parse(result);
        //                         sessionStorage.setItem("patName3",res[0].full_name);
        //                 }
        //             });
        //             var patName3 = sessionStorage.getItem("patName3");
        //             if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance3').value = data[0].InsuranceName;}
        //             if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder3').value = temp + ' - ' +patName3;}
        //             if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured3').value = data[0].relationshipToInsured;}
        //             if(data[0].policyNo != ""){document.getElementById('EditpolicyNo3').value = data[0].policyNo;}
        //             if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt3').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt2').value = '';}}
        //             if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo3').value = data[0].groupNo;}
        //             if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt3').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt2').value = '';}}
        //             if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay3').value = data[0].coPayment;}
        //             if(patDOB != ""){document.getElementById('EditpolicyDOB3').value = patDOB;}
        //         }
        //     });
        //     $('#policyHolder1').val(temp + ' - ' +patFullName);
        //     $('#policyDOB1').val(patDOB);
        //     $('#policyHolder2').val(temp + ' - ' +patFullName);
        //     $('#policyDOB2').val(patDOB);
        //     $('#policyHolder3').val(temp + ' - ' +patFullName);
        //     $('#policyDOB3').val(patDOB);
        // });

        $('#EditpolicyRelationInsured1').change(function(){
            if($(this).val() != 'Self'){
                $.ajax({
                    type: "POST",
                    url: root_url+"guarantor/get",
                    async : false,
                    data:{
                        patientID : temp
                    },success:function(data){
                        var res = JSON.parse(data);
                        document.getElementById("EditpolicyHolder1").value = res[0].guarantorID +' - ' +res[0].guarantorName;
                        document.getElementById("EditpolicyDOB1").value = changeDateFormat(res[0].guarantorDOB);
                    }
                });
            }
            else{
                $('#EditpolicyHolder1').val(temp + ' - ' +patFullName);
                $('#EditpolicyDOB1').val(patDOB);
            }
        });
        $('#EditpolicyRelationInsured2').change(function(){
            if($(this).val() != 'Self'){
                $('#EditpolicyHolder2').val('');
                $('#EditpolicyDOB2').val('');
            }
            else{
                $('#EditpolicyHolder2').val(temp + ' - ' +patFullName);
                $('#EditpolicyDOB2').val(patDOB);
            }
        });
        $('#EditpolicyRelationInsured3').change(function(){
            if($(this).val() != 'Self'){
                $('#EditpolicyHolder3').val('');
                $('#EditpolicyDOB3').val('');
            }
            else{
                $('#EditpolicyHolder3').val(temp + ' - ' +patFullName);
                $('#EditpolicyDOB3').val(patDOB);
            }
        });

        $('#policyHolder1').val(temp + ' - ' +patFullName);
        $('#policyDOB1').val(patDOB);
        $('#policyHolder2').val(temp + ' - ' +patFullName);
        $('#policyDOB2').val(patDOB);
        $('#policyHolder3').val(temp + ' - ' +patFullName);
        $('#policyDOB3').val(patDOB);


        $('#policyRelationInsured1').change(function(){
            if($(this).val() != 'Self'){
                $.ajax({
                    type: "POST",
                    url: root_url+"guarantor/get",
                    async : false,
                    data:{
                        patientID : document.getElementById('hdnPatientID').value
                    },success:function(data){
                        var res = JSON.parse(data);
                        document.getElementById("policyHolder1").value = res[0].guarantorID +' - ' +res[0].guarantorName;
                        document.getElementById("policyDOB1").value = changeDateFormat(res[0].guarantorDOB);
                    }
                });
                // $('#policyHolder1').val('');
                // $('#policyDOB1').val('');
            }
            else{
                $('#policyHolder1').val(patFullName);
                $('#policyDOB1').val(patDOB);
            }
        });
        $('#policyRelationInsured2').change(function(){
            if($(this).val() != 'Self'){
                $.ajax({
                    type: "POST",
                    url: root_url+"guarantor/get",
                    async : false,
                    data:{
                        patientID : document.getElementById('hdnPatientID').value
                    },success:function(data){
                        var res = JSON.parse(data);
                        document.getElementById("policyHolder2").value = res[0].guarantorID +' - ' +res[0].guarantorName;
                        document.getElementById("policyDOB2").value = changeDateFormat(res[0].guarantorDOB);
                    }
                });
                // $('#policyHolder1').val('');
                // $('#policyDOB1').val('');
            }
            else{
                $('#policyHolder2').val(patFullName);
                $('#policyDOB2').val(patDOB);
            }
        });
        $('#policyRelationInsured3').change(function(){
            if($(this).val() != 'Self'){
                $.ajax({
                    type: "POST",
                    url: root_url+"guarantor/get",
                    async : false,
                    data:{
                        patientID : document.getElementById('hdnPatientID').value
                    },success:function(data){
                        var res = JSON.parse(data);
                        document.getElementById("policyHolder3").value = res[0].guarantorID +' - ' +res[0].guarantorName;
                        document.getElementById("policyDOB3").value = changeDateFormat(res[0].guarantorDOB);
                    }
                });
                // $('#policyHolder1').val('');
                // $('#policyDOB1').val('');
            }
            else{
                $('#policyHolder3').val(patFullName);
                $('#policyDOB3').val(patDOB);
            }
        });


        $("#btnUpdatePatient").click(function() {
            var fullName = $.trim($("#fullName").val());
            //var martialStatus = $.trim($('#martialStatus').val());
            var ssn = $.trim($('#ssn').val());
            var employment = $.trim($('#employment').val());
            var dob =  convertDate($.trim($('#dob').val()));
            //var employer = $.trim($('#employer').val());
            var gender = $.trim($('#gender').val());
            if(gender == "Male"){
                gender="M";
            }
            else{
                gender = "F";
            }
            //var reference = $.trim($('#reference').val());
            //var medicalRecord = $.trim($('#medicalRecord').val());
            var addrStreet1 = $.trim($('#addrStreet1').val());
            var addrStreet2 = $.trim($('#addrStreet2').val());
            var addrCity = $.trim($('#addrCity').val());
            var addrState = $.trim($('#addrState').val());
            var addrCountry = $.trim($('#addrCountry').val());
            var addrZip = $.trim($('#addrZip').val());
            var phoneHome =  $.trim($('#phoneHome').val());
            //var phoneWork = $.trim($('#phoneWork').val());
            var phoneMobile = $.trim($('#phoneMobile').val());
            var email = $.trim($('#email').val());
            //var notifyEmail = $.trim($('#notifyEmail').val());
            //var notifyPhone = $.trim($('#notifyPhone').val());
            var primaryCarePhy =  $.trim($('#primaryCarePhy').val());
            var n1=primaryCarePhy.indexOf("-");
            if(n1 != -1){
                var PrimaryPhyID = primaryCarePhy.substr(0,n1-1);
            }
            else{
                var PrimaryPhyID = primaryCarePhy;
            }
            //var defaultServiceLoc = $.trim($('#defaultServiceLoc').val());
            var ID = temp;

            var homeChk = document.getElementById('homeChk');
            if (homeChk.checked){
                var home="1";
            }else{
                var home="0";
            }

            var officeChk = document.getElementById('officeChk');
            if (officeChk.checked){
                var office="1";
            }else{
                var office="0";
            }

            var schoolChk = document.getElementById('schoolChk');
            if (schoolChk.checked){
                var school="1";
            }else{
                var school="0";
            }

            $.ajax({
                type: "POST",
                url: root_url+"location/update",
                data:{
                    "patientID" : ID,
                    "home" : home,
                    "office" : office,
                    "school" : school
                },success:function(result){
                }

            });


            $.ajax({
                type: "POST",
                url: root_url+"patient/update",
                data:{
                    "patientID" : ID,
                    "fullName" : fullName,
                    //"martialStatus" : martialStatus,
                    "ssn" : ssn,
                    //"employment" : employment,
                    "dob" : dob,
                    //"employer" : employer,
                    "gender" : gender,
                    //"reference" : reference,
                    //"medicalRecord" : medicalRecord,
                    "addrStreet1" : addrStreet1,
                    "addrStreet2" : addrStreet2,
                    "addrCity" : addrCity,
                    "addrState" : addrState,
                    "addrCountry" : addrCountry,
                    "addrZip" : addrZip,
                    "phoneHome" : phoneHome,
                    //"phoneWork" : phoneWork,
                    "phoneMobile" : phoneMobile,
                    "email" : email,
                    //"notifyEmail" : notifyEmail,
                    //"notifyPhone" : notifyPhone,
                    "primaryCarePhy" : PrimaryPhyID,
                    //"defaultServiceLoc" : defaultServiceLoc,
                },success:function(result){
                    alertify.success('Patient updated successfully');
                    window.location.reload();
                }

            });
        });
    });

    function getActivityData(){
        var root_url = $("#hidden_root_url").val();
        var ddlVal = $("#ddlAuth").val();
        $('#btnOpenAct').show();
        $.ajax({
            async : false,
            type: "POST",
            url: root_url+"activity/getbyauth",
            data:{
                authNo: ddlVal
            },success:function(data1){
                var res = JSON.parse(data1);
                if(res.status == 0){
                    return;
                }
                var data1 = res.data;
                var dt = [];
                $.each(data1,function(i,v) {
                    var startDt = changeDateFormat(data1[i].start_date);
                    var endDt = changeDateFormat(data1[i].end_date);
                    dt.push([data1[i].name,startDt,endDt,data1[i].total_auth,data1[i].remain_auth,data1[i].id]);
                });
                //alert(dt);
                var table = $('#activityTab').DataTable({
                    "bPaginate": false,
                    "bFilter": false,
                    "bInfo": false,
                    destroy: true,
                    "data": dt,
                    columns: [
                        {"title": "Activity Name"},
                        {"title": "Start Date"},
                        {"title": "End Date"},
                        {"title": "Total Auth"},
                        {"title": "Remaining Auth"},
                        {"title": "Action",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0);" class="act act'+data+'"><span class="ti-pencil"></span></a> | <a href="javascript:void(0);" class="del del'+data+'"><span class="ti-trash"></span></a>';
                            }
                        }
                    ]
                });
            }
        });
    }

    function generateActivity(){
        var root_url = $("#hidden_root_url").val();
        var ddlVal = $("#ddlActcaseList").val();
        $.ajax({
            async : false,
            type: "POST",
            url:root_url+"cases/getcaseid",
            data:{
                "caseChartNo" : ddlVal
            },success:function(result){
                var caseID = result;
                $.ajax({
                    async: false,
                    type: "POST",
                    url : root_url + 'authinfo/list',
                    data: {
                        caseID: caseID,
                    }, success: function (data) {
                        $('#ddlAuth').html('');
                        //iterate over the data and append a select option
                        data = JSON.parse(data);
                        $('#ddlAuth').append('<option>-- Select Auth --</option>');
                        $.each(data, function(key, val){
                            $('#ddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                        });
                    }
                });
            }
        });
        getActivityData();
    }

    function editCase(temp1,patientID,patFullName,patDOB){
        var root_url = $("#hidden_root_url").val();
        var ddlValue = temp1;
        $.post(root_url + "cases/loadbychartnum",
            {
                caseChartNo: ddlValue,
            },
            function(data, status){
                var res = JSON.parse(data);
                if(res.status == 0){
                    return;
                }
                var data = res.data;
                if(data[0].auth_num != ""){
                    document.getElementById('EditauthNo1').value = data[0].auth_num;
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: root_url + 'authinfo/getauthoinfo1',
                        data: {
                            caseID: data[0].id,
                            policyEntity : "1"
                        }, success: function (data) {
                            var res = JSON.parse(data);
                            if(res.status == 0){
                                
                                return;
                            }
                            var res = res.data;
                            //if(res[0].visitsRemaining != ""){document.getElementById('EditnoOfVisits').value = res[0].visitsRemaining;}
                            //if(res[0].totalAuthVisits != ""){document.getElementById('EdittotalVisits').value = res[0].totalAuthVisits;}
                            $.each(res, function(i,v){
                                if(res[i].auth_num != ""){
                                    $('#EditauthNo1').append('<option value="' + res[i].auth_num + '">' + res[i].auth_num + '</option>');
                                    //document.getElementById('EditauthNo1').value = res[0].authNo;
                                }
                            });
                            if(res[0].tos != ""){document.getElementById('Edittos1').value = res[0].tos;}
                            //if(res[0].totAuthType != ""){document.getElementById('EdittotAuthType').value = res[0].totAuthType;}
                            if(res[0].start_date != ""){document.getElementById('EditAuthstartDt1').value = changeDateFormat(res[0].start_date);}
                            if(res[0].end_date != ""){document.getElementById('EditAuthendDt1').value = changeDateFormat(res[0].end_date);}
                        }
                    });
                    //$('#EditnoOfVisits').removeAttr('disabled');
                }
                $.ajax({
                    async: false,
                    type: "POST",
                    url: root_url + 'authinfo/getauthoinfo2',
                    data: {
                        caseID: data[0].caseID,
                        policyEntity : "2"
                    }, success: function (data) {
                        var res = JSON.parse(data);
                        if(res.status == 0){
                            
                            return;
                        }
                        var res = res.data;
                        //if(res[0].visitsRemaining != ""){document.getElementById('EditnoOfVisits').value = res[0].visitsRemaining;}
                        //if(res[0].totalAuthVisits != ""){document.getElementById('EdittotalVisits').value = res[0].totalAuthVisits;}
                        $.each(res, function(i,v){
                            if(res[i].auth_num != ""){
                                $('#EditauthNo2').append('<option value="' + res[i].auth_num + '">' + res[i].auth_num + '</option>');
                                //document.getElementById('EditauthNo1').value = res[0].authNo;
                            }
                        });
                        if(res[0].tos != ""){document.getElementById('Edittos2').value = res[0].tos;}
                        //if(res[0].totAuthType != ""){document.getElementById('EdittotAuthType').value = res[0].totAuthType;}
                        if(res[0].start_date != ""){document.getElementById('EditAuthstartDt2').value = changeDateFormat(res[0].start_date);}
                        if(res[0].end_date != ""){document.getElementById('EditAuthendDt2').value = changeDateFormat(res[0].end_date);}
                    }
                });

                //if(data[0].lastVisit != ""){if(data[0].lastVisit != '0000-00-00 00:00:00'){document.getElementById('EditlastVisit').value = changeDateFormat(data[0].lastVisit);} else{document.getElementById('EditlastVisit').value = '';}}
                //if(data[0].noOfVisits != ""){document.getElementById('EditnoOfVisits').value = data[0].noOfVisits;}
                //if(data[0].totalVisits != ""){document.getElementById('EdittotalVisits').value = data[0].totalVisits;}else if(data[0].totalVisits == -1){document.getElementById('EdittotalVisits').value = "-";}
                if(data[0].principal_diag != ""){
                    document.getElementById('EditprincipalDiag').value = data[0].principal_diag;
                }
                if(data[0].def_diag_2 != ""){document.getElementById('EditdefDiag2').value = data[0].def_diag_2;}
                if(data[0].def_diag_3 != ""){document.getElementById('EditdefDiag3').value = data[0].def_diag_3;}
                if(data[0].def_diag_4 != ""){document.getElementById('EditdefDiag4').value = data[0].def_diag_4;}
            });

            $.ajax({
                async: false,
                type: "POST",
                url: root_url + "policies/loadpolicy",
                data: {
                    caseChartNo: ddlValue,
                    policyEntity : "1"
                }, success: function (data) {
                    var res = JSON.parse(data);
                    if(res.status == 0){
                        
                        return;
                    }
                    var data = res.data;
                    if(data.length!=0){
                        var patVal1 = data[0].policy_holder;
                        $.ajax({
                            type: "POST",
                            url: root_url+"patient/list",
                            async: false,
                            data:{
                                "patientID" : patVal1
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    
                                    return;
                                }
                                var res = res.data;
                                sessionStorage.setItem("patName1",res[0].full_name);
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: root_url+"insurances/getAddress",
                            data:{
                                "insuranceID" : data[0].insurance_id
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    
                                    return;
                                }
                                var res = res.data;
                                document.getElementById('spanAddress1').innerHTML = res.address;
                            }
                        });
                        var patName1 = sessionStorage.getItem("patName1");
                        if(data[0].insurance_name != ""){document.getElementById('EditpolicyInsurance1').value = data[0].insurance_name;}
                        if(data[0].relationship_to_insured != ""){document.getElementById('EditpolicyRelationInsured1').value = data[0].relationship_to_insured;}
                        if(data[0].policy_num != ""){document.getElementById('EditpolicyNo1').value = data[0].policy_num;}
                        if(data[0].start_date != ""){if(data[0].start_date != '0000-00-00'){document.getElementById('EditpolicyStartDt1').value = changeDateFormat(data[0].start_date);} else{document.getElementById('EditpolicyStartDt1').value = '';}}
                        if(data[0].group_num != ""){document.getElementById('EditpolicyGroupNo1').value = data[0].group_num;}
                        if(data[0].group_name != ""){document.getElementById('EditpolicyGroupName1').value = data[0].group_name;}
                        if(data[0].end_date != ""){if(data[0].end_date != '0000-00-00'){document.getElementById('EditpolicyEndDt1').value = changeDateFormat(data[0].end_date);} else{document.getElementById('EditpolicyEndDt1').value = '';}}
                        if(data[0].co_payment != ""){document.getElementById('EditpolicyCopay1').value = data[0].co_payment;}
                        if(data[0].deductible_met != ""){document.getElementById('EditpolicyDeduc1').value = data[0].deductible_met;}
                        if(patDOB != ""){document.getElementById('EditpolicyDOB1').value = patDOB;}
                        if(data[0].relationship_to_insured == "Self"){
                            if(data[0].policy_holder != ""){document.getElementById('EditpolicyHolder1').value = patientID + ' - ' +patName1;}
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"guarantor/get",
                                async: false,
                                data:{
                                    patientID: patientID
                                },
                                success:function(data){
                                    data = JSON.parse(data);
                                    if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder1').value = data[0].guarantorID + ' - ' +data[0].guarantorName;}
                                }
                            });
                        }

                    }
                }
            });

            $.ajax({
                async: false,
                type: "POST",
                url: root_url + "policies/loadpolicy",
                data: {
                    caseChartNo: ddlValue,
                    policyEntity : "1"
                }, success: function (data) {
                    if(data.length!=0){
                        var patVal2 = data[0].policy_holder;
                        $.ajax({
                            type: "POST",
                            url: root_url+"patient/list",
                            async: false,
                            data:{
                                "patientID" : patVal2
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    
                                    return;
                                }
                                var res = res.data;
                                sessionStorage.setItem("patName2",res[0].full_name);
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: root_url+"insurances/getAddress",
                            data:{
                                "insuranceID" : data[0].insurance_id
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    
                                    return;
                                }
                                var res = res.data;
                                document.getElementById('spanAddress2').innerHTML = res.address;
                            }
                        });
                        var patName2 = sessionStorage.getItem("patName1");
                        if(data[0].insurance_name != ""){document.getElementById('EditpolicyInsurance2').value = data[0].insurance_name;}
                        if(data[0].relationship_to_insured != ""){document.getElementById('EditpolicyRelationInsured2').value = data[0].relationship_to_insured;}
                        if(data[0].policy_num != ""){document.getElementById('EditpolicyNo2').value = data[0].policy_num;}
                        if(data[0].start_date != ""){if(data[0].start_date != '0000-00-00'){document.getElementById('EditpolicyStartDt2').value = changeDateFormat(data[0].start_date);} else{document.getElementById('EditpolicyStartDt2').value = '';}}
                        if(data[0].group_num != ""){document.getElementById('EditpolicyGroupNo2').value = data[0].group_num;}
                        if(data[0].group_name != ""){document.getElementById('EditpolicyGroupName2').value = data[0].group_name;}
                        if(data[0].end_date != ""){if(data[0].end_date != '0000-00-00'){document.getElementById('EditpolicyEndDt2').value = changeDateFormat(data[0].end_date);} else{document.getElementById('EditpolicyEndDt2').value = '';}}
                        if(data[0].co_payment != ""){document.getElementById('EditpolicyCopay2').value = data[0].co_payment;}
                        if(data[0].deductible_met != ""){document.getElementById('EditpolicyDeduc2').value = data[0].deductible_met;}
                        if(patDOB != ""){document.getElementById('EditpolicyDOB2').value = patDOB;}
                    }
                }
            });
            $.ajax({
                async: false,
                type: "POST",
                url: root_url + "policies/loadpolicy",
                data: {
                    caseChartNo: ddlValue,
                    policyEntity : "1"
                }, success: function (data) {
                    if(data.length!=0){
                        var patVal3 = data[0].policy_holder;
                        $.ajax({
                            type: "POST",
                            url: root_url+"patient/list",
                            async: false,
                            data:{
                                "patientID" : patVal3
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    
                                    return;
                                }
                                var res = res.data;
                                sessionStorage.setItem("patName3",res[0].full_name);
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: root_url+"insurances/getAddress",
                            data:{
                                "insuranceID" : data[0].insurance_id
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    
                                    return;
                                }
                                var res = res.data;
                                document.getElementById('spanAddress3').innerHTML = res.address;
                            }
                        });
                        var patName3 = sessionStorage.getItem("patName3");
                        if(data[0].insurance_name != ""){document.getElementById('EditpolicyInsurance3').value = data[0].insurance_name;}
                        if(data[0].relationship_to_insured != ""){document.getElementById('EditpolicyRelationInsured3').value = data[0].relationship_to_insured;}
                        if(data[0].policy_num != ""){document.getElementById('EditpolicyNo3').value = data[0].policy_num;}
                        if(data[0].start_date != ""){if(data[0].start_date != '0000-00-00'){document.getElementById('EditpolicyStartDt3').value = changeDateFormat(data[0].start_date);} else{document.getElementById('EditpolicyStartDt3').value = '';}}
                        if(data[0].group_num != ""){document.getElementById('EditpolicyGroupNo3').value = data[0].group_num;}
                        if(data[0].group_name != ""){document.getElementById('EditpolicyGroupName3').value = data[0].group_name;}
                        if(data[0].end_date != ""){if(data[0].end_date != '0000-00-00'){document.getElementById('EditpolicyEndDt3').value = changeDateFormat(data[0].end_date);} else{document.getElementById('EditpolicyEndDt3').value = '';}}
                        if(data[0].co_payment != ""){document.getElementById('EditpolicyCopay3').value = data[0].co_payment;}
                        if(data[0].deductible_met != ""){document.getElementById('EditpolicyDeduc3').value = data[0].deductible_met;}
                        if(patDOB != ""){document.getElementById('EditpolicyDOB3').value = patDOB;}
                    }
                }
            });
        $('#policyHolder1').val(patientID + ' - ' +patFullName);
        $('#policyDOB1').val(patDOB);
        $('#policyHolder2').val(patientID + ' - ' +patFullName);
        $('#policyDOB2').val(patDOB);
        $('#policyHolder3').val(patientID + ' - ' +patFullName);
        $('#policyDOB3').val(patDOB);
    }

    function reloadCases(temp){
        var root_url = $("#hidden_root_url").val();
        $.post(root_url + "cases",
            {
                patientID: temp,
            },
            function(data, status){
                $('#ddlcaseList').html('');
                //iterate over the data and append a select option
                $('#ddlcaseList').append('<option>-- Select Cases --</option>');
                $.each(data, function(key, val){
                    $.ajax({
                        async : false,
                        type: "POST",
                        url: root_url+"cases/getgroup",
                        data:{
                            "caseID" : val.caseID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $('#ddlcaseList').append('<option id="' + val.caseChartNo + '" value="' + val.caseChartNo + '">' + res[0].caseChartNo + ' - (Insurance : '+res[0].payerName+') - '+res[0].description+' - (Auth : '+res[0].authNo+') - (Dated : '+res[0].start_date+' & '+res[0].end_date+')</option>');
                        }
                    });
                });
                $('#ddlActcaseList').html('');
                //iterate over the data and append a select option
                //$('#ddlActcaseList').append('<option>-- Select Cases --</option>');
                $.each(data, function(key, val){
                    $('#ddlActcaseList').append('<option value="' + val.caseChartNo + '" id="' + val.caseChartNo + '">' + val.caseChartNo + '</option>');
                });
                var ddlVal = $("#ddlActcaseList").val();
                $.ajax({
                    async : false,
                    type: "POST",
                    url:root_url+"cases/getcaseid",
                    data:{
                        "caseChartNo" : ddlVal
                    },success:function(result){
                        var caseID = result;
                        $.ajax({
                            async: false,
                            type: "POST",
                            url : root_url + 'authinfo/list',
                            data: {
                                caseID: caseID,
                            }, success: function (data) {
                                $('#ddlAuth').html('');
                                //iterate over the data and append a select option
                                data = JSON.parse(data);
                                $('#ddlAuth').append('<option>-- Select Auth --</option>');
                                $.each(data, function(key, val){
                                    $('#ddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                                });
                            }
                        });
                    }
                });
            });
    }

    var convertDate = function(usDate) {
        var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
        return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }

    function authDetails(){
        var root_url = $("#hidden_root_url").val();
        var authNo = document.getElementById('EditauthNo').value;
        $.ajax({
            async: false,
            type: "POST",
            url: root_url + 'authinfo/get',
            data: {
                authNo: authNo,
            }, success: function (data) {
                var n = data.length;
                data = data.substr(0, n - 1);
                var res = JSON.parse(data);
                //if(res[0].visitsRemaining != ""){document.getElementById('EditnoOfVisits').value = res[0].visitsRemaining;}
                //if(res[0].totalAuthVisits != ""){document.getElementById('EdittotalVisits').value = res[0].totalAuthVisits;}
                if (res[0].authNo != "") {
                    document.getElementById('EditauthNo').value = res[0].authNo;
                }
                if (res[0].tos != "") {
                    document.getElementById('Edittos').value = res[0].tos;
                }
                //if(res[0].totAuthType != ""){document.getElementById('EdittotAuthType').value = res[0].totAuthType;}
                if (res[0].start_date != "") {
                    document.getElementById('EditstartDt').value = changeDateFormat(res[0].start_date);
                }
                if (res[0].end_date != "") {
                    document.getElementById('EditendDt').value = changeDateFormat(res[0].end_date);
                }
            }
        });
    }

    function addCase(){
        var root_url = $("#hidden_root_url").val();
        var PatID = sessionStorage.getItem("patientId");
        var description = "Primary";
        var authNo = $('#authNo').val();
        var tos = $('#tos').val();
        var AuthstartDt = convertDate($('#AuthstartDt').val());
        var AuthendDt = convertDate($('#AuthendDt').val());
        var AuthtotalVisits = $('#AuthtotalVisits').val();
        var totAuthType = $('#totAuthType').val();

        var lastVisit = convertDate($('#lastVisit').val());
        // if($('#totalVisits').val() == ""){
        //     var totalVisits = -1;
        // }
        // else{
        //     var totalVisits = $('#totalVisits').val();
        // }
        var principalDiag = $('#principalDiag').val();
        var defDiag2 = $('#defDiag2').val();
        var defDiag3 = $('#defDiag3').val();
        var defDiag4 = $('#defDiag4').val();

        var policyInsurance1 = $('#policyInsurance1').val();
        var policyHolder1 = getHolderID($('#policyHolder1').val());
        var policyRelationInsured1 = $('#policyRelationInsured1').val();
        var policyNo1 = $('#policyNo1').val();
        var policyStartDt1 = convertDate($('#policyStartDt1').val());
        var policyGroupNo1 = $('#policyGroupNo1').val();
        var policyGroupName1 = $('#policyGroupName1').val();
        var policyEndDt1 = convertDate($('#policyEndDt1').val());
        var policyClaimNo1 = $('#policyClaimNo1').val();
        var policyCopay1 = $('#policyCopay1').val();
        var policyDeduc1 = $('#policyDeduc1').val();

        var policyInsurance2 = $('#policyInsurance2').val();
        var policyHolder2 = getHolderID($('#policyHolder2').val());
        var policyRelationInsured2 = $('#policyRelationInsured2').val();
        var policyNo2 = $('#policyNo2').val();
        var policyStartDt2 = convertDate($('#policyStartDt2').val());
        var policyGroupNo2 = $('#policyGroupNo2').val();
        var policyGroupName2 = $('#policyGroupName2').val();
        var policyEndDt2 = convertDate($('#policyEndDt2').val());
        var policyClaimNo2 = $('#policyClaimNo2').val();
        var policyCopay2 = $('#policyCopay2').val();
        var policyDeduc2 = $('#policyDeduc2').val();

        var policyInsurance3 = $('#policyInsurance3').val();
        var policyHolder3 = getHolderID($('#policyHolder3').val());
        var policyRelationInsured3 = $('#policyRelationInsured3').val();
        var policyNo3 = $('#policyNo3').val();
        var policyStartDt3 = convertDate($('#policyStartDt3').val());
        var policyGroupNo3 = $('#policyGroupNo3').val();
        var policyGroupName3 = $('#policyGroupName3').val();
        var policyEndDt3 = convertDate($('#policyEndDt3').val());
        var policyClaimNo3 = $('#policyClaimNo3').val();
        var policyCopay3 = $('#policyCopay3').val();
        var policyDeduc3 = $('#policyDeduc3').val();

        if(policyInsurance1 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"insurances/getidbypayername",
                async: false,
                data:{
                    "payerName" : policyInsurance1
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        
                        return;
                    }
                    else{
                        sessionStorage.setItem("insurance1",res.data.id);
                    }
                }
            });
        }
        if(policyInsurance2 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"insurances/getidbypayername",
                async: false,
                data:{
                    "payerName" : policyInsurance2
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        
                        return;
                    }
                    else{
                        sessionStorage.setItem("insurance2",res.data.id);
                    }
                }
            });
        }
        if(policyInsurance3 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"insurances/getidbypayername",
                async: false,
                data:{
                    "payerName" : policyInsurance3
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        
                        return;
                    }
                    else{
                        sessionStorage.setItem("insurance3",res.data.id);
                    }
                }
            });
        }

        var tempInsID = sessionStorage.getItem("insurance1");
        $.ajax({
            async : false,
            type: "POST",
            url:root_url + "cases/add",
            data:{
                "patientID" : PatID,
                "description" : description,
                "insuranceID" : tempInsID,
                "authNo" : authNo1,
                "lastVisit" : lastVisit,
                //"noOfVisits" : totalVisits,
                //"totalVisits" : totalVisits,
                "principalDiag" : principalDiag,
                "defDiag2" : defDiag2,
                "defDiag3" : defDiag3,
                "defDiag4" : defDiag4,
            },success:function(result){
                result = JSON.parse(result);
                document.getElementById("hdncaseID").value = result.data.caseID;
                if(authNo1 != "") {
                    $.ajax({
                        async: false,
                        type: "POST",
                        url : root_url + 'authinfo/add',
                        data: {
                            "caseID": result.data.caseID,
                            "patientID": PatID,
                            "authNo": authNo1,
                            "tos": tos1,
                            "startDt": AuthstartDt1,
                            "endDt": AuthendDt1,
                            "policyEntity": "1"
                            // "totAuthType" : totAuthType,
                            // "totalVisits" : AuthtotalVisits,
                            // "totalRemaining" : AuthtotalVisits
                        }, success: function (result) {
                            console.log(result);
                        }
                    });
                }
                if(authNo2 != ""){
                    $.ajax({
                        async : false,
                        type: "POST",
                        url : root_url + 'authinfo/add',
                        data:{
                            "caseID" : result.CaseID,
                            "patientID" : PatID,
                            "authNo" : authNo2,
                            "tos" : tos2,
                            "startDt" : AuthstartDt2,
                            "endDt" : AuthendDt2,
                            "policyEntity" : "2"
                            // "totAuthType" : totAuthType,
                            // "totalVisits" : AuthtotalVisits,
                            // "totalRemaining" : AuthtotalVisits
                        },success:function(result){
                            console.log(result);
                        }
                    });
                }
                alertify.success("Case successfully Created");
                //document.getElementById('hdnChkCase').value = "1";
            }
        });

        $.ajax({
            type: "POST",
            url: root_url+"cases/getchartnum",
            async: false,
            data:{
                "patientID" : PatID
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    return;
                }else{
                    var hdnCaseChartNo = res.data.val;
                    resultNew = hdnCaseChartNo.split(',');
                    sessionStorage.setItem("tempCaseChartNo", resultNew[0]);
                    sessionStorage.setItem("tempClaimID", resultNew[1]);
                }
            }
        });
        var tempCaseChartNo = sessionStorage.getItem("tempCaseChartNo");
        var tempClaimID = sessionStorage.getItem("tempClaimID");
        var insurance1 = sessionStorage.getItem("insurance1");
        var insurance2 = sessionStorage.getItem("insurance2");
        var insurance3 = sessionStorage.getItem("insurance3");
        if(policyInsurance1 != "" && policyNo1 != ""){
            $.ajax({
                type: "POST",
                url:root_url + "policies/createpolicy",
                async: false,
                data:{
                    "policyEntity" : "1",
                    "caseID" : tempClaimID,
                    "authNo" : authNo1,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance1,
                    "policyHolder" : policyHolder1,
                    "relationshipToInsured" : policyRelationInsured1,
                    "policyNo" : policyNo1,
                    "groupNo" : policyGroupNo1,
                    "groupName" : policyGroupName1,
                    "claimNo" : policyClaimNo1,
                    "startDt" : policyStartDt1,
                    "endDt" : policyEndDt1,
                    "coPayment" : policyCopay1,
                    "deductibleMet" : policyDeduc1,
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }else{
                        alertify.success("Primary Policy added successfully");
                        document.getElementById('hdnChkCase').value = "1";
                        // $("#step-2").css({"display":"none"});
                        // $("#step-3").css({"display":"flex"});
                        // $("#up2").removeClass("btn-primary");
                        // $("#up3").addClass("btn-primary");
                        // $("#up2").addClass("btn-default");
                        // $("#up3").removeClass("btn-default");
                        reloadCases(PatID);
                    }
                    //window.location.href = "ViewPatient.php";
                }
            });
        }
        if(policyInsurance2 != "" && policyNo2 != ""){
            $.ajax({
                type: "POST",
                url:root_url + "policies/createpolicy",
                async: false,
                data:{
                    "policyEntity" : "2",
                    "caseID" : tempClaimID,
                    "authNo" : authNo2,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance2,
                    "policyHolder" : policyHolder2,
                    "relationshipToInsured" : policyRelationInsured2,
                    "policyNo" : policyNo2,
                    "groupNo" : policyGroupNo2,
                    "groupName" : policyGroupName2,
                    "claimNo" : policyClaimNo2,
                    "startDt" : policyStartDt2,
                    "endDt" : policyEndDt2,
                    "coPayment" : policyCopay2,
                    "deductibleMet" : policyDeduc2,
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }else{
                        alertify.success("Secondary Policy added successfully");
                        //window.location.href = "ViewPatient.php";
                        document.getElementById('hdnChkCase').value = "1";
                        reloadCases(PatID);
                    }
                }
            });
        }
        if(policyInsurance3 != "" && policyNo3 != ""){
            $.ajax({
                type: "POST",
                url:root_url + "policies/createpolicy",
                async: false,
                data:{
                    "policyEntity" : "3",
                    "caseID" : tempClaimID,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance3,
                    "policyHolder" : policyHolder3,
                    "relationshipToInsured" : policyRelationInsured3,
                    "policyNo" : policyNo3,
                    "groupNo" : policyGroupNo3,
                    "groupName" : policyGroupName3,
                    "claimNo" : policyClaimNo3,
                    "startDt" : policyStartDt3,
                    "endDt" : policyEndDt3,
                    "coPayment" : policyCopay3,
                    "deductibleMet" : policyDeduc3,
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }else{
                        alertify.success("Teritiary Policy added successfully");
                        document.getElementById('hdnChkCase').value = "1";
                        //window.location.href = "ViewPatient.php";
                        reloadCases(PatID);
                    }
                }
            });
        }
    }

    function updatePolicy(){
        var root_url = $("#hidden_root_url").val();
        var PatID = sessionStorage.getItem("patientId");
        var tempCaseChartNo = $("#ddlcaseList").val();
        var policyInsurance1 = $('#EditpolicyInsurance1').val();
        var policyHolder1 = getHolderID($('#EditpolicyHolder1').val());
        var policyRelationInsured1 = $('#EditpolicyRelationInsured1').val();
        var policyNo1 = $('#EditpolicyNo1').val();
        var policyStartDt1 = convertDate($('#EditpolicyStartDt1').val());
        var policyGroupNo1 = $('#EditpolicyGroupNo1').val();
        var policyEndDt1 = convertDate($('#EditpolicyEndDt1').val());
        var policyClaimNo1 = $('#EditpolicyClaimNo1').val();
        var policyCopay1 = $('#EditpolicyCopay1').val();

        var principalDiag = $('#EditprincipalDiag').val();
        var defDiag2 = $('#EditdefDiag2').val();
        var defDiag3 = $('#EditdefDiag3').val();
        var defDiag4 = $('#EditdefDiag4').val();

        var policyInsurance2 = $('#EditpolicyInsurance2').val();
        var policyHolder2 = getHolderID($('#EditpolicyHolder2').val());
        var policyRelationInsured2 = $('#EditpolicyRelationInsured2').val();
        var policyNo2 = $('#EditpolicyNo2').val();
        var policyStartDt2 = convertDate($('#EditpolicyStartDt2').val());
        var policyGroupNo2 = $('#EditpolicyGroupNo2').val();
        var policyEndDt2 = convertDate($('#EditpolicyEndDt2').val());
        var policyClaimNo2 = $('#EditpolicyClaimNo2').val();
        var policyCopay2 = $('#EditpolicyCopay2').val();

        var policyInsurance3 = $('#EditpolicyInsurance3').val();
        var policyHolder3 = getHolderID($('#EditpolicyHolder3').val());
        var policyRelationInsured3 = $('#EditpolicyRelationInsured3').val();
        var policyNo3 = $('#EditpolicyNo3').val();
        var policyStartDt3 = convertDate($('#EditpolicyStartDt3').val());
        var policyGroupNo3 = $('#EditpolicyGroupNo3').val();
        var policyEndDt3 = convertDate($('#EditpolicyEndDt3').val());
        var policyClaimNo3 = $('#EditpolicyClaimNo3').val();
        var policyCopay3 = $('#EditpolicyCopay3').val();
        if(policyInsurance1 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"insurances/get",
                async: false,
                data:{
                    "payerName" : policyInsurance1
                },success:function(result){
                    sessionStorage.setItem("insurance1",result);
                }
            });
        }
        if(policyInsurance2 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"insurances/get",
                async: false,
                data:{
                    "payerName" : policyInsurance2
                },success:function(result){
                    sessionStorage.setItem("insurance2",result);
                }
            });
        }
        if(policyInsurance3 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"insurances/get",
                async: false,
                data:{
                    "payerName" : policyInsurance3
                },success:function(result){
                    sessionStorage.setItem("insurance3",result);
                }
            });
        }
        var insurance1 = sessionStorage.getItem("insurance1");
        var insurance2 = sessionStorage.getItem("insurance2");
        var insurance3 = sessionStorage.getItem("insurance3");
        if(policyInsurance1 != "" && policyNo1 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"policies/update",
                async: false,
                data:{
                    "policyEntity" : "1",
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance1,
                    "policyHolder" : policyHolder1,
                    "relationshipToInsured" : policyRelationInsured1,
                    "policyNo" : policyNo1,
                    "groupNo" : policyGroupNo1,
                    "claimNo" : policyClaimNo1,
                    "startDt" : policyStartDt1,
                    "endDt" : policyEndDt1,
                    "coPayment" : policyCopay1,
                    "principalDiag" : principalDiag,
                    "defDiag2" : defDiag2,
                    "defDiag3" : defDiag3,
                    "defDiag4" : defDiag4,
                },success:function(result){
                    alertify.success("Primary Policy Updated successfully");
                    reloadCases(PatID);
                }
            });
        }
        if(policyInsurance2 != "" && policyNo2 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"policies/update",
                async: false,
                data:{
                    "policyEntity" : "2",
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance2,
                    "policyHolder" : policyHolder2,
                    "relationshipToInsured" : policyRelationInsured2,
                    "policyNo" : policyNo2,
                    "groupNo" : policyGroupNo2,
                    "claimNo" : policyClaimNo2,
                    "startDt" : policyStartDt2,
                    "endDt" : policyEndDt2,
                    "coPayment" : policyCopay2,
                },success:function(result){
                    alertify.success("Secondary Policy Updated successfully");
                    reloadCases(PatID);
                }
            });
        }
        if(policyInsurance3 != "" && policyNo3 != ""){
            $.ajax({
                type: "POST",
                url: root_url+"policies/update",
                async: false,
                data:{
                    "policyEntity" : "3",
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance3,
                    "policyHolder" : policyHolder3,
                    "relationshipToInsured" : policyRelationInsured3,
                    "policyNo" : policyNo3,
                    "groupNo" : policyGroupNo3,
                    "claimNo" : policyClaimNo3,
                    "startDt" : policyStartDt3,
                    "endDt" : policyEndDt3,
                    "coPayment" : policyCopay3,
                },success:function(result){
                    alertify.success("Teritiary Policy Updated successfully");
                    reloadCases(PatID);
                }
            });
        }
    }

    function pad(number, length) {

        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }

        return str;

    }

    function checkAuth(){
        if($('#EditchkAuth').is(':checked') == true){
            $('#EditauthNo').removeAttr('disabled');
            //$('#EditnoOfVisits').removeAttr('disabled');
            //$('#EdittotalVisits').removeAttr('disabled');
            $('#EditlastVisit').removeAttr('disabled');
            $('#EditprincipalDiag').removeAttr('disabled');
            $('#EditdefDiag2').removeAttr('disabled');
            $('#EditdefDiag3').removeAttr('disabled');
            $('#EditdefDiag4').removeAttr('disabled');
        }
        else{
            $('#EditauthNo').attr('disabled',true);
            $('#EditnoOfVisits').attr('disabled',true);
            $('#EdittotalVisits').attr('disabled',true);
            $('#EditlastVisit').attr('disabled',true);
            $('#EditprincipalDiag').attr('disabled',true);
            $('#EditdefDiag2').attr('disabled',true);
            $('#EditdefDiag3').attr('disabled',true);
            $('#EditdefDiag4').attr('disabled',true);
        }
    }

    function getHolderID(name1){
        var name = name1;
        if(name != ""){
            var n=name.indexOf("-");
            var truncID = name.substr(0,n);
        }
        return truncID;
    }

    var convertDate = function(usDate) {
        if(usDate != "" && usDate != null && usDate != undefined){
            var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
            return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
        }
    }
    function changeDateFormat(inputDate){  // expects Y-m-d
        if(inputDate != "" && inputDate != null && inputDate != undefined){
            var splitDate = inputDate.split('-');
            if(splitDate.count == 0){
                return null;
            }

            var year = splitDate[0];
            var month = splitDate[1];
            var day = splitDate[2].substr(0,2);

            return month + '-' + day + '-' + year;
        }
        else{
            return "";
        }
    }
</script>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>