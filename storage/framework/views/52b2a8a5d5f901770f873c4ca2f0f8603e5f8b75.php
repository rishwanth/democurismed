<?php $__env->startSection('content'); ?>
 
    <div class="row">
    	<h2 style="color: #251367; margin-left:20px;">Deposit</h2>
    	<div class="col-md-12" style="margin-top:20px;">
        <div class="widget box" >
            <div class="widget-content">
            	<a href="javascript:void(0)" id="linkAddDep" class="btn btn-primary" style="text-decoration:none"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Deposit</a>
            	<a href="javascript:void(0)" id="uploadERA" class="btn btn-primary" style="text-decoration:none"><i class="fa fa-plus"></i>&nbsp;&nbsp;Upload ERA</a>
            	<form action="EditPatient.php" method="post">
    				<table id="deposit" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
    				</table>
    			</form>
                <table id="depositDetails" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
                </table>
            </div>
        </div>

    </div>
	<div class="modal fade" id="AddDeposit" tabindex="-1">
		<div class="modal-dialog modal-lg" style="width:75%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Deposit</h4>
				</div>
				<div class="modal-body" style="min-height:380px;">
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-6">
		                        <label class="control-label col-md-12">Select Payor Type </label>
		                        <div class="col-md-12">
		                            <select class="form-control" id="payorType">
		                            	<option value="0" selected></option>
		                            	<option value="1">Payor</option>
		                            	<option value="2">Client</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6" id="divPayorType">
		                        <label class="control-label col-md-12" id="lblType">Payor </label>
		                        <div class="col-md-12">
		                            <input type="text" id="payor" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6">
		                        <label class="control-label col-md-12">Deposit Date </label>
		                        <div class="col-md-12">
		                            <input type="text" id="depositDate" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6">
		                        <label class="control-label col-md-12" id="lblType1">Payment Method </label>
		                        <div class="col-md-12">
		                            <select class="form-control" id="paymentType">
		                            	<option>Check</option>
		                            	<option>EFT</option>
		                            	<option>Credit Card</option>
		                            	<option>Cash</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12">Check # </label>
		                        <div class="col-md-12">
		                            <input type="text" id="instrumentNo" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12">Check Date </label>
		                        <div class="col-md-12">
		                            <input type="text" id="instrumentDate" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12" id="lblType">Amount </label>
		                        <div class="col-md-12">
		                            <input type="text" id="amount" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-8">
		                        <label class="control-label col-md-12" id="lblType">Notes </label>
		                        <div class="col-md-12">
		                            <textarea class="form-control" id="notes" cols="5" rows="10"></textarea>
		                        </div>
		                    </div>
		                </div>
		            </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnSaveDep" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="EditDeposit" tabindex="-1">
		<div class="modal-dialog" style="width:75%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Deposit</h4>
				</div>
				<div class="modal-body" style="min-height:280px;">
					<input type="hidden" id="_hdnDepID"/>
					<div class="form-group col-md-6">
                        <label class="control-label col-md-12">Select Payor Type </label>
                        <div class="col-md-6">
                            <select class="form-control" id="EditpayorType">
                            	<option value="0"></option>
                            	<option value="1">Payor</option>
                            	<option value="2">Client</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6" id="divPayorType">
                        <label class="control-label col-md-12" id="lblType">Payor </label>
                        <div class="col-md-6">
                            <select class="form-control" id="Editpayor">
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Deposit Date </label>
                        <div class="col-md-6">
                            <input type="text" id="EditdepositDate" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12" id="lblType">Payment Method </label>
                        <div class="col-md-6">
                            <select class="form-control" id="EditpaymentType">
                            	<option>Check</option>
                            	<option>EFT</option>
                            	<option>Credit Card</option>
                            	<option>Cash</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12">Check # </label>
                        <div class="col-md-6">
                            <input type="text" id="EditinstrumentNo" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12">Check Date </label>
                        <div class="col-md-6">
                            <input type="text" id="EditinstrumentDate" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12" id="lblType">Amount </label>
                        <div class="col-md-6">
                            <input type="text" id="Editamount" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label class="control-label col-md-12" id="lblType">Notes </label>
                        <div class="col-md-10">
                            <textarea class="form-control" id="Editnotes" col="5" row="6"></textarea>
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnEditDep" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>