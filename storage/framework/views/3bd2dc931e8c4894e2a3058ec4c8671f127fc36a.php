<?php $__env->startSection('content'); ?>


<div class="row">
    <h2 style="color: #251367; margin-left:20px;">Insurances List</h2>
    <div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">
            <a href="javascript:void(0)" class="btn btn-primary" id="linkNewIns"><i class="icon icon-plus"></i>&nbsp;Add New Insurance</a><br/><br/>
                <table id="insuranceList" class="table display" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
                </table>
        </div>
    </div>
</div>


<div class="modal fade" id="AddInsurance" tabindex="-1">

    <div class="modal-dialog" style="width:60%">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title" id="lblTitle">Add New Insurance</h4>

            </div>

            <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Payer Name </label>

                            <div class="col-md-12">

                                <input type="text" id="payerName" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Clearing House ID </label>

                            <div class="col-md-12">

                                <input type="text" id="clearingHousePayorID" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Address Street 1 </label>

                            <div class="col-md-12">

                                <input type="text" id="addrStrt1" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Address Street 2 </label>

                            <div class="col-md-12">

                                <input type="text" id="addrStrt2" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">City </label>

                            <div class="col-md-12">

                                <input type="text" id="addrCity" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">State </label>

                            <div class="col-md-12">

                                <input type="text" id="addrState" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Zipcode </label>

                            <div class="col-md-12">

                                <input type="text" id="addrZip" class="form-control" />

                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">

                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />

                <input type="button" id="btnSaveInsurance" class="btn btn-primary" data-dismiss="modal" value="Save" />

            </div>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>