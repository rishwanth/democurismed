<?php
    $base_url = URL::to('/');
?>
@extends('layout.app')
@section('content')
<div class="row">
	<h2 style="color: #251367; margin-left:20px;">Practice Info</h2>
	<div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">

			<div class="card-body">
                <div class="bs-example">
                    <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                        <li class="nav-item">
                            <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Setup Facility</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Set Up Zones</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_3" style="color:#000;" data-toggle="tab" class="nav-link">Setup Holiday/PTO</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_4" style="color:#000;" data-toggle="tab" class="nav-link">Setup Activities Subtypes</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_5" style="color:#000;" data-toggle="tab" class="nav-link"> Create Session Rules </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_6" style="color:#000;" data-toggle="tab" class="nav-link"> Setup Employee </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_7" style="color:#000;" data-toggle="tab" class="nav-link"> Add/Edit Users </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_8" style="color:#000;" data-toggle="tab" class="nav-link"> Manage Payroll </a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="tab_3_1">
                        	<div id="accordion" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                               Facility Name &amp; Location
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
			                            	<div class="row">

			                            		<div class="col-sm-3">
			                                        <div class="form-group row" style="margin-bottom:1rem">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Facility Name" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="Address" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Address 2" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Zipcode" />
			                                            </div>
			                                        </div>
			                                    </div>

			                                    <div class="col-sm-3">
			                                        <div class="form-group row" style="margin-bottom:1rem">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="City" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="State" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Phone 1" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Fax" />
			                                            </div>
			                                        </div>
			                                    </div>

			                                    <div class="col-sm-3">
			                                        <div class="form-group row" style="margin-bottom:1rem">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Phone 2" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="Email" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="EIN" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="NPI" />
			                                            </div>
			                                        </div>
			                                    </div>

			                                    <div class="col-sm-3">
			                                        <div class="form-group row" style="margin-bottom:1rem">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Taxonomy Code" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="Contact Person with Amvik" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Start Time" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="End Time" />
			                                            </div>
			                                        </div>
			                                    </div>

			                                    <!-- <div class="col-sm-3">
			                                        <div class="form-group row" style="margin-bottom:1rem">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patFirstName" name="txtFirstName" placeholder="Service Area Miles" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="User Default Password" />
			                                            </div>
			                                        </div>
			                                    </div> -->
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                            	<label class="checkbox-inline icheckbox">
			                                                    <input type="checkbox" class="uniform" id="homeChk" value=""> Is Default Facility
			                                                </label>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-9">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <textarea class="form-control" rows="5" cols="5"></textarea>
			                                            </div>
			                                        </div>
			                                    </div>

			                            	</div>
			                            </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                               Add Payors to the Facility
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                                        	<select multiple="multiple" size="10" name="duallistbox_demo1[]">
										      <option value="option1">AETNA</option>
										      <option value="option2">MEDICARE</option>
										      <option value="option3" selected="selected">MEDICAID</option>
										      <option value="option4">CIGNA</option>
										      <option value="option5">TRICARE</option>
										      <option value="option6" selected="selected">UHC</option>
										      <option value="option7">Optum</option>
										      <option value="option8">Healthnet</option>
										      <option value="option9">Anthem</option>
										      <option value="option0">BCBS</option>
										    </select>
                                        </div>
                                        <button id="listPayer" name="">Handler List</button>
                                        <button id="selectedPayer" name="">Selected List</button>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="headingfour">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                               Payor Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapsefour" class="collapse" role="tabpanel" aria-labelledby="headingfour" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                            <table class="table table-bordered" style="width:100%;" id="selectedPayerTable" name="selectedPayerTable">
                        		
                        		
                        	</table>

                                        </div>
                                    </div>
                                </div>

                                <div class="card" style="display: none;">
                                    <div class="card-header" role="tab" id="headingfive">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                               Payroll Integration
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapsefive" class="collapse" role="tabpanel" aria-labelledby="headingfive" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
                                        	<div class="row">
                                        		<div class="col-sm-3">
			                                        <div class="form-group row" style="margin-bottom:1rem">
			                                            <div class="col-12">
			                                                <select class="form-control">
			                                                	<option value="">--Payroll Process--</option>
			                                                	<option value="Manual">Manual</option>
			                                                	<option value="ADP">ADP</option>
			                                                </select>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-9">
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control" id="patMiddleName" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" name="txtMiddleName" placeholder="ADP Company Code" />
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-3">
			                                        <div class="form-group row">
			                                            <div class="col-12">
			                                                <input type="text" class="form-control validateGeneral" style="padding:0.5rem 0.75rem; line-height:1.25; font-size:1em;" id="patLastName" name="txtLastName" placeholder="Mileage Rate Code" />
			                                            </div>
			                                        </div>
			                                    </div>
                                        	</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card" style="display: none;">
                                    <div class="card-header" role="tab" id="headingsix">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                               Company Logo Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapsesix" class="collapse" role="tabpanel" aria-labelledby="headingsix" data-parent="#accordion" style="padding:20px;">
                                        <div class="card-body">
<form id="formUploadFile" method="post" enctype="multipart/form-data" 
action="{{ URL::to('/') }}/practices/uploadlogo" >
                                    <input type="hidden" id="practice_id" name="practice_id" value="1">                                        		
                                            	<input type="file" id="logo" name="logo" class="form-control"/>

                                            	<button type="submit">Submit</button>
                                        	</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="tab_3_2">
    	<a href="javascript:void(0)" class="btn btn-primary" id="linkNewZone">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>
                            <table class="table table-bordered" style="width:100%;" id="zoneTable" name="zoneTable">
                        		
                        		
                        	</table>
                        </div>

                        <div class="tab-pane fade" id="tab_3_3">
    	<a href="javascript:void(0)" class="btn btn-primary linknew" id="linkNewHoliday" data-popup="AddHoliday">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>

                            <table class="table table-bordered" style="width:100%;"id="holidayTable" name="holidayTable" >
                        	</table>
                        </div>

                        <div class="tab-pane fade" id="tab_3_4">

                        	<div id="accordion1" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading7">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                               Service/Activity Sub Types
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse7" class="collapse show" role="tabpanel" aria-labelledby="heading7" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                            <div class="form-group col-md-6">
							                    <label class="control-label col-md-12">Type </label>
							                    <div class="col-md-12">
							                        <select class="form-control">
												      <option value="Billable">Billable</option>
												      <option value="Non-billable">Non-billable</option>
												    </select>
							                    </div>
							                </div>
							                <div class="form-group col-md-6">
							                    <label class="control-label col-md-12">Activity </label>
							                    <div class="col-md-12">
							                        <select class="form-control" id="ddlActivity" name="ddlActivity">
														<option selected="selected" value="--Select One--">--Select One--</option>
													</select>

    	<a href="javascript:void(0)" class="btn btn-primary linksave" id="saveActivitiesSubtypes" data-save="saveActivitiesSubtypes">
    	<i class="icon icon-plus"></i>&nbsp;Save</a>

							                    </div>
							                </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_5">
    	<a href="javascript:void(0)" class="btn btn-primary linknew" id="linkNewSessionRule" data-popup="AddSessionRule">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>

                        	<table class="table" cellspacing="0" border="1" style="width:100%;border-collapse:collapse;" id="sessionRuleTable" name="sessionRuleTable">
															</table>
                        </div>

                        <div class="tab-pane fade" id="tab_3_6">
                        	<div id="accordion2" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading9">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                               Credential Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse9" class="collapse show" role="tabpanel" aria-labelledby="heading9" data-parent="#accordion" style="padding:20px;display: none1;">
                                        <div class="card-body">
                                            <div class="form-group col-md-6">
							                    <label class="control-label col-md-12">Select Credential Type </label>
							                    <div class="col-md-12">
							                        <select class="form-control">
												      <option value="BA">Behaviour Analyst</option>
												      <option value="OS">Office Staff</option>
												      <option value="PP">ParaProfessional</option>
												    </select>
							                    </div>
							                </div>
							                <div class="form-group col-md-6"></div>
							                <div class="form-group col-md-12" style="display: none;">
							                    <label class="control-label col-md-12">All Credentials </label>
							                    <select multiple="multiple" size="10" name="duallistbox_demo1[]">
											      <option value="11">BCABA</option>
													<option value="12">Drivers License</option>
													<option value="13">Liability Insurance</option>
													<option value="14">State Photo ID</option>
													<option value="15">LMFT</option>
													<option value="16">Clinical Psychologist</option>
													<option value="17">LCSW</option>
													<option value="18">LEP</option>
													<option value="19">CPR Training</option>
													<option value="24">Car Insurance</option>
													<option value="27">Contract 1</option>
													<option value="28">Contract 2</option>
													<option value="33">Contracted Provider</option>
													<option value="34">BAC</option>
													<option value="37">CPI</option>
													<option value="38">Annual Eval</option>
													<option value="50">First Aid</option>
													<option value="51">Crisis Prevention/Intervention</option>
													<option value="54">Pro-Act training</option>
													<option value="56">BCaBA</option>
													<option value="59">Family Care Safety Registry</option>
													<option value="67">BSL</option>
													<option value="84">Evaluations</option>
													<option value="85">Blood Born Pathogens</option>
													<option value="86">Sexual Harassment</option>
													<option value="87">HIPAA</option>
													<option value="88">Mandated Reporting</option>
													<option value="89">Incident Report Training</option>
													<option value="90">Complaint and Grievances</option>
													<option value="91">Sensitive Services</option>
													<option value="93">8-Hour Supervisor Training</option>
													<option value="94">Hawaii BCBA license</option>
													<option value="96">LBA certification</option>
													<option value="97">Short Term Disability </option>
													<option value="101">IRA</option>
													<option value="105">Health Insurance Eligibility</option>
													<option value="110">HIV / Aids Certificate</option>
													<option value="152">BCBA-D</option>

											    </select>
							                </div>
							                <div class="form-group col-md-12"  style="display: none;">
							                    <label class="control-label col-md-12">All Clearances </label>
							                    <select multiple="multiple" size="10" name="duallistbox_demo1[]">
											      <option value="11">TB Clearance</option>
													<option value="12">DOJ Clearance</option>
													<option value="13">FBI Clearance</option>
													<option value="14">Finger Printing</option>
													<option value="48">DMV records report</option>
													<option value="63">MMR</option>
													<option value="64">Tdap</option>
													<option value="65">Hep B</option>
													<option value="66">Varicella</option>
													<option value="69">90 Day Probation</option>
													<option value="119">NSO Registry </option>
													<option value="124">Abuser Registry</option>

											    </select>
							                </div>
							                <div class="form-group col-md-12"  style="display: none;">
							                    <label class="control-label col-md-12">All Qualifications </label>
							                    <select multiple="multiple" size="10" name="duallistbox_demo1[]">
											      <option value="9">Bachelors Degree</option>
													<option value="10">Masters Degree</option>
													<option value="11">Ph.D</option>
													<option value="12">Associate Degree</option>
													<option value="13">Highschool Diploma</option>
													<option value="14">Psy.D</option>
													<option value="16">G.E.D</option>
													<option value="84">COBA</option>

											    </select>
							                </div>
							                *****

							                <a href="{{ $base_url }}/physician/add"target="_new" >Add Employee</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading10">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                               HR Note Type Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse10" class="collapse" role="tabpanel" aria-labelledby="heading10" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading11">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                               Employee Position
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse11" class="collapse" role="tabpanel" aria-labelledby="heading11" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading12">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                               Game Goal Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse12" class="collapse" role="tabpanel" aria-labelledby="heading12" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading13">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                               Game Goal Setup
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse13" class="collapse" role="tabpanel" aria-labelledby="heading13" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_7">
    	<a href="javascript:void(0)" class="btn btn-primary linknew" id="linkNewUser" data-popup="AddUser">
    	<i class="icon icon-plus"></i>&nbsp;Add New</a>

                        	<table class="table" cellspacing="0" border="1" style="width:100%;border-collapse:collapse;" id="userTable" name="userTable">
															</table>
                        	
                        </div>

                        <div class="tab-pane fade" id="tab_3_8">
                        	<div id="accordion3" role="tablist">
                        		<div class="card">
                                    <div class="card-header" role="tab" id="heading14">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                               Hide Timesheet Link
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse14" class="collapse show" role="tabpanel" aria-labelledby="heading14" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                        	<input type="button" value="Hide Timesheet Link" class="btn btn-primary float-right">
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading15">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                               Disallow Rendering Session
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse15" class="collapse" role="tabpanel" aria-labelledby="heading15" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                        	<div class="form-group col-md-12">
                                        		<input type="button" value="Save" class="btn btn-primary float-right col-md-1" />
                                        		<input type="text" class="form-control float-right col-md-3" style="line-height:28px; margin-right:15px;" placeholder="mm/dd/yyyy" />
							                    <label class="control-label float-right col-md-3">Disallow Rendering Session Before </label>
							                </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" role="tab" id="heading16">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                               Pay Periods
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse16" class="collapse" role="tabpanel" aria-labelledby="heading16" data-parent="#accordion" style="padding:20px">
                                        <div class="card-body">
                                        	<input type="button" value="Create Pay Periods" id="btnCreatePay" class="btn btn-primary float-right">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_9">
                        	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
	<div class="modal fade" id="myModal1" tabindex="-1">
		<div class="modal-dialog" style="width:70%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Practice</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:50px;">
					<div class="card-body">
  						<div class="row">
							<input type="hidden" id="hdnFlag"/>
							<div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Practice Name </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="practiceName1" name="PrimaryPhysiName" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6">
			                	<label class="control-label col-md-12">Speciality </label>
			                    <div class="col-md-12">
			                        <select class="form-control" id="phySpeciality">
			                        	<option></option>
			                        	<option>Psychologist</option>
			                        	<option>Counselor</option>
			                        </select>
			                    </div>
			            	</div>
			            	<div class="clearfix"></div>

			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Group NPI </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyNPI" name="txtPTNumber"  value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Date Of Birth </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyDOB" name="txtPTNumber"  value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Tax ID </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyEIN" name="txtPTNumber" value="" />
			                    </div>
			                </div>

			            	<div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Social Security # </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phySSN" name="txtPTNumber"  value="" />
			                    </div>
			                </div>
			                <div class="clearfix"></div>
			                <hr/>
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Address </label>
			                    <div class="col-md-12">
			                    	<textarea class="form-control" cols="5" rows="4" id="phyAddr"></textarea>
			                    </div>
			                </div>

			                <div class="form-group col-md-6" >
			                    <label class="control-label col-md-12">Home</label>
			                    <div class="col-md-12">
			                         <input type="text" class="form-control required" id="phyHomePhone" name="txtPTNumber"  value="" />
			                    </div>
			                </div>

			                 <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Work </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyWorkPhone" name="txtPTNumber" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6" >
			                    <label class="control-label col-md-12">Mobile </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyMobilePhone" name="txtPTNumber" value="" />
			                    </div>
			                </div>
			                <div class="clearfix"></div>
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Dept </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyDept" name="txtPTNumber" value="" />
			                    </div>
			                </div>
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Fax </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="phyFax" name="txtPTNumber" value="" />
			                    </div>
			                </div>
			                <div class="clearfix"></div>
			                <hr/>
			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-2">Notes </label>
			                    <div class="col-md-12">
			                    	<textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Add Practice" class="btn btn-primary" id="btnAddPractice"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div class="modal fade" id="mdlZone" tabindex="-1">
		<div class="modal-dialog" style="width:70%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Zone</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:50px;">
					<div class="card-body">
  						<div class="row">
							<input type="hidden" id="hdnFlag"/>
							<div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Name </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" id="practiceName1" name="PrimaryPhysiName" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-2">Description </label>
			                    <div class="col-md-12">
			                    	<textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Add Zone" class="btn btn-primary" id="btnSaveZone"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div class="modal fade" id="mdlHoliday" tabindex="-1">
		<div class="modal-dialog" style="width:70%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Holiday</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:50px;">
					<div class="card-body">
  						<div class="row">
							<input type="hidden" id="hdnFlag"/>
							<div class="form-group col-md-6">
			                    <label class="control-label col-md-12">Date </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control required" placeholder="mm/dd/yyyy" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-2">Description </label>
			                    <div class="col-md-12">
			                    	<textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Add Holiday" class="btn btn-primary" id="btnSaveHoliday"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div class="modal fade" id="mdlPayPeriod" tabindex="-1">
		<div class="modal-dialog modal-lg" style="width:80%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Setup Payroll</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:50px;">
					<div class="card-body">
  						<div class="row">
							<div class="form-group col-md-12" style="padding-bottom:10px;">
			                    <label class="control-label col-md-12">How often do you pay your employees? </label>
			                    <div class="col-md-4">
			                        <select class="form-control">
			                        	<option selected="selected" value="1">Weekly</option>
										<option value="2">Bi-Weekly</option>
										<option value="3">From 1st &amp; 16th Every Month</option>
										<option value="4">Monthly</option>
										<option value="5">Custom</option>

			                        </select>
			                    </div>
			                </div><br/>
			                <div class="form-group col-md-4" style="padding-bottom:10px;">
			                    <label class="control-label col-md-12">Select Year </label>
			                    <div class="col-md-12">
			                        <select class="form-control">
			                        	<option value="2017">2017</option>
										<option selected="selected" value="2018">2018</option>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
			                        </select>
			                    </div>
			                </div>
			                <div class="form-group col-md-4">
			                    <label class="control-label col-md-12">What is the next pay period end date? </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" />
			                    </div>
			                </div>
			                <div class="form-group col-md-4">
			                    <label class="control-label col-md-12">What is the check date for next pay period? </label>
			                    <div class="col-md-12">
			                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" />
			                    </div>
			                </div>

			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-12">After how many days employee can't submit time sheet? </label>
			                    <div class="col-md-4">
			                    	<input type="text" class="form-control" />
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Create Pay Period for this year" class="btn btn-primary" id="btnSavePay"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

<!-- AddZone -->
<div class="modal fade" id="AddZone" tabindex="-1">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New Zone</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Name </label>
                            <div class="col-md-12">
                                <input type="text" id="name" name="name" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Description </label>
                            <div class="col-md-12">
                                <input type="text" id="description" name="description"  class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Practice ID </label>
                            <div class="col-md-12">
                                <input type="text" id="practice_id" name="practice_id"  value="1" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Active (1/0) </label>
                            <div class="col-md-12">
                                <input type="text" id="is_active" name="is_active"  class="form-control" value="1" />
                            </div>
                        </div>
                    </div>


                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSave" class="btn btn-primary btnsave" data-dismiss1="modal" value="Save" data-btnname="zone" data-btnurl="zone" />
            </div>
            
        </div>
    </div>
</div>

<!-- AddHoliday -->
<div class="modal fade" id="AddHoliday" tabindex="-1">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Date </label>
                            <div class="col-md-12">
                                <input type="text" id="date" name="date" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Description </label>
                            <div class="col-md-12">
                                <input type="text" id="description" name="description" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Practice  </label>
                            <div class="col-md-12">
                                <input type="text" id="	practice_id" name="	practice_id" value="1" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Active </label>
                            <div class="col-md-12">
                                <input type="text" id="is_active" name="is_active" value="1" class="form-control" />
                            </div>
                        </div>
                    </div>

                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSave" class="btn btn-primary btnsave" data-dismiss1="modal" value="Save" data-btnname="holiday" data-btnurl="" />
            </div>
            
        </div>
    </div>
</div>

<!-- AddHoliday -->
<div class="modal fade" id="AddUser" tabindex="-1">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New User</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">First Name </label>
                            <div class="col-md-12">
                                <input type="text" id="first_name" name="first_name" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Last Name </label>
                            <div class="col-md-12">
                                <input type="text" id="last_name" name="last_name" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Username </label>
                            <div class="col-md-12">
                                <input type="text" id="username" name="username" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Role </label>
                            <div class="col-md-12">
                                <input type="text" id="role" name="role" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Email ID</label>
                            <div class="col-md-12">
                                <input type="text" id="email_id" name="email_id" class="form-control" />
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Practice</label>
                            <div class="col-md-12">
                                <input type="text" id="practice_id" name="practice_id" class="form-control" />
                            </div>
                        </div>
                    </div>
                    
                   <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Mobile</label>
                            <div class="col-md-12">
                                <input type="text" id="phone_num" name="phone_num" class="form-control" />
                            </div>
                        </div>
                    </div>


                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSave" name="btnSave" class="btn btn-primary btnsave" data-dismiss1="modal" value="Save" data-btnname="user" data-btnsave="register/update" />
            </div>
            
        </div>
    </div>
</div>


@stop
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<link href="css/bootstrap-duallistbox.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="js/jquery.bootstrap-duallistbox.js"></script>
	<script>
        $(document).ready(function(){
	        $('#listPayer').click(function(){
	        	listPayer();
            });

	        $('#selectedPayer').click(function(){
	        	selectedPayer();
            });

	        $('.btnUpdatePayerSetup').click(function(){
	        	alert('btnUpdatePayerSetup');
	        	updatePayerSetup(this);
            });



			$(".ti-menu-alt").click();

        	fillSelectedPayer();
/*        	fillZone();
        	fillHoliday();
        	fillActivitiesSubtypes();
        	fillSessionRules();
        	fillUser();
*/
	        $('#linkNewZone').click(function(){
            	$('#AddZone').modal('show');
            });

            
	        $('.btnsave').click(function(){
	        	var name  =  $(this).data('btnname');
	        	var url  =  $(this).data('btnsave');
	        	//alert('btnSave ' + c);
	        	btnsave(name, url);
            });

	        $('.linknew').click(function(){
	        	var c  = '#' + $(this).data('popup');
            	$(c).modal('show');
            });

	        $('.linksave').click(function(){
	        	var c  = $(this).data('save');
            	//$(c).modal('show');
	        	c  = '#' + c;
            });

            

            
            

            $(".sidebar-toggle").click();

            $('#masFacPhone').mask('(000) 000-0000');
            $('#masFacPhone2').mask('(000) 000-0000');
            $('#masFacFax').mask('(000) 000-0000');

            $(document).on('focusout','#masFacEmail',function() {
                if($(this).val()!=""){
                    if( !validateEmail($(this).val())) {
                        alertify.error("Please enter a valid email address");
                        $("#masFacEmail").focus();
                    }
                }
            });

/*            $(document).on('focusout','#masFacZip',function() {
                var currValue = $(this).val();
                $.ajax({
                    type: "POST",
                    url: root_url+"zipinfo/get",
                    async: false,
                    data:{
                        zip : currValue
                    },
                    success:function(result){
                        var n = result.length;
                        result = result.substr(0,n-1);
                        if(result != "none"){
                            var res = JSON.parse(result);
                            document.getElementById("masFacState").value = res[0].state;
                            document.getElementById("masFacCity").value = res[0].city;
                        }
                        else{
                            alertify.error("Sorry! This Zip is not available in our Database.")
                        }
                    }
                });
            });
*/
/*            $.get(root_url + "/practice", function(data1, status){
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].practiceID,data1[i].practiceName,data1[i].address,data1[i].npi,data1[i].EIN,data1[i].practiceID]);
                });
                //alert(dt);
                var table = $('#practiceList').DataTable({
                    "data": dt,
                    "bPaginate": false,
                    "bProcessing": true,
                    "aoColumns": [
                        {"mdata": "practiceID","title":"Practice ID", visible:false},
                        {"title":"Practice Name","mdata": "practiceName"},
                        {"title":"Address","mdata": "address"},
                        {"title":"Group ID","mdata": "npi"},
                        {"title":"Tax ID","mdata": "EIN"},
                        {"mdata": "practiceID","title":"Action",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0);" class="edit'+data+' editPractice"><span class="ti-pencil"></span></a>';
                            }
                        },
                    ]
                });
            });
*/
/*            $(document).on('click','.editPractice',function() {
                var temp = $(this).attr('class').split(' ')[0];
                var phyID = temp.replace('edit','');
                document.getElementById("hdnFlag").value = phyID;
                sessionStorage.setItem("pracID",phyID);
                window.location.href="EditPractice.php";
                // $.ajax({
                //            type: "POST",
                //            url:"getPracticeInfo.php",
                //            async : false,
                //            data:{
                //              practiceID : phyID
                //            },success:function(result){
                //            	$('#myModal1').modal('show');
                //            	var res = JSON.parse(result);
                //            	document.getElementById("practiceName1").value = res[0].practiceName;
                // 	document.getElementById("phySpeciality").value = res[0].speciality;
                // 	document.getElementById("phyDOB").value = res[0].dob;
                // 	document.getElementById("phyNPI").value = res[0].npi;
                // 	document.getElementById("phySSN").value = res[0].ssn;
                // 	document.getElementById("phyEIN").value = res[0].EIN;
                // 	document.getElementById("phyAddr").value = res[0].address;
                // 	document.getElementById("phyHomePhone").value = res[0].phoneHome;
                // 	document.getElementById("phyWorkPhone").value = res[0].phoneWork;
                // 	document.getElementById("phyMobilePhone").value = res[0].phoneMobile;
                // 	document.getElementById("phyDept").value = res[0].dept;
                // 	document.getElementById("phyFax").value = res[0].fax;
                // 	document.getElementById("phyNotes").value = res[0].notes;
                // 	document.getElementById("hdnFlag").value = res[0].practiceID;
                // 	document.getElementById("btnAddPractice").value = "Update Practice";
                //            }
                //        	});
            });
*/
/*            $("#btnAddPractice").click(function() {
                var hdnFlag = document.getElementById("hdnFlag").value;
                if(hdnFlag == 0){
                    var phyName = document.getElementById("practiceName1").value;
                    var speciality = document.getElementById("phySpeciality").value;
                    var dob = document.getElementById("phyDOB").value;
                    var individualNPI = document.getElementById("phyNPI").value;
                    var ssn = document.getElementById("phySSN").value;
                    var EIN = document.getElementById("phyEIN").value;
                    var address = document.getElementById("phyAddr").value;
                    var phoneHome = document.getElementById("phyHomePhone").value;
                    var phoneWork = document.getElementById("phyWorkPhone").value;
                    var phoneMobile = document.getElementById("phyMobilePhone").value;
                    var dept = document.getElementById("phyDept").value;
                    var fax = document.getElementById("phyFax").value;
                    var notes = document.getElementById("phyNotes").value;

                    $.ajax({
                        type: "POST",
                        url: root_url+"practices/add",
                        data:{
                            "practiceName" : phyName,
                            "phyDOB" : dob,
                            "phyNPI" : individualNPI,
                            "phySSN" : ssn,
                            "EIN" : EIN,
                            "phyAddr" : address,
                            "phyHomePhone" : phoneHome,
                            "phyWorkPhone" : phoneWork,
                            "phyMobilePhone" : phoneMobile,
                            "dept" : dept,
                            "phyFax" : fax,
                            "phyNotes" : notes,
                            "speciality" : speciality,
                        },success:function(result){
                            //alertify.success('PrimaryPhysiName updated successfully');
                            $('#myModal1').modal('hide');
                            //window.location.reload();
                        }
                    });
                }
                else
                {
                    var practiceID = document.getElementById("hdnFlag").value;
                    var phyName = document.getElementById("practiceName1").value;
                    var speciality = document.getElementById("phySpeciality").value;
                    var dob = document.getElementById("phyDOB").value;
                    var individualNPI = document.getElementById("phyNPI").value;
                    var ssn = document.getElementById("phySSN").value;
                    var EIN = document.getElementById("phyEIN").value;
                    var address = document.getElementById("phyAddr").value;
                    var phoneHome = document.getElementById("phyHomePhone").value;
                    var phoneWork = document.getElementById("phyWorkPhone").value;
                    var phoneMobile = document.getElementById("phyMobilePhone").value;
                    var dept = document.getElementById("phyDept").value;
                    var fax = document.getElementById("phyFax").value;
                    var notes = document.getElementById("phyNotes").value;

                    $.ajax({
                        type: "POST",
                        url: root_url+"practices/update",
                        data:{
                            "practiceID" : practiceID,
                            "practiceName" : phyName,
                            "phyDOB" : dob,
                            "phyNPI" : individualNPI,
                            "phySSN" : ssn,
                            "EIN" : EIN,
                            "phyAddr" : address,
                            "phyHomePhone" : phoneHome,
                            "phyWorkPhone" : phoneWork,
                            "phyMobilePhone" : phoneMobile,
                            "dept" : dept,
                            "phyFax" : fax,
                            "phyNotes" : notes,
                            "speciality" : speciality,
                        },success:function(result){
                            //alertify.success('PrimaryPhysiName updated successfully');
                            $('#myModal1').modal('hide');
                            //window.location.reload();
                        }
                    });
                }
            });
*/        });
        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test( $email );
        }
	</script>

<script type="text/javascript">
function fillTable(tableName){
	var root_url = $("#hidden_root_url").val();
    var url = root_url+"practices/listholiday";
    var practice_id = 1;
    var response = [];
    var handler = '';
    if (tableName == 'zone'){
	    url = root_url + "practices/listzone";
    } else if (tableName == 'holiday'){
	    url = root_url + "practices/listholiday";
    } else if (tableName == 'handler_activity'){
	    url = root_url + "handler/get";
	    handler = 'activity';
    } else if (tableName == 'handler_insurances'){
    	//alert(tableName);
	    url = root_url + "handler/get";
	    handler = 'insurances';
    } else if (tableName == 'session_rules'){
	    url = root_url + "handler/get";
	    handler = 'session_rule';
    } else if (tableName == 'user'){
	    url = root_url + "practices/getusers";
	    //handler = 'session_rule';
    } else if (tableName == 'insurances'){
	    url = root_url + "insurances/list";
    } else if (tableName == 'selectedpayer'){
	    url = root_url + "insurances/list";
    }


    $.ajax({
        type: "POST",
        url: url,
        async: false,
        data:{
            practice_id : practice_id,
            handler: handler,
        },
        success:function(result){
            var res = JSON.parse(result);
            if(res.status == 0){
                return res;
            }
		    if (tableName == 'zone'){
            	fillZoneSuccess(res);
		    } else if (tableName == 'holiday'){
            	fillHolidaySuccess(res);
		    } else if (tableName == 'handler_activity'){
            	fillActivitiesSubtypesSuccess(res);
  			} else if (tableName == 'session_rules'){
  				fillSessionRulesSuccess(res);
  			} else if (tableName == 'user'){
  				fillUserSuccess(res);
  			} else if (tableName == 'selectedpayer'){
  				fillSelectedPayerSuccess(res);
		    } else {
		    	response = res;
		    	return response;
		    }
		}
    });	
	
}

function fillZone(){
	fillTable('zone');
}	

function fillHoliday(){
	fillTable('holiday');
}

function fillActivitiesSubtypes(){
	fillTable('handler_activity');
}

function fillUser(){
	fillTable('user');
}	

function fillUserSuccess (res){
	var c = 'fillUserSuccess => ' + res.data.length;
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id, data1[i].first_name, data1[i].last_name]);
    });

    $('#userTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "First Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Last Name", "width": "120px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+data+'"><span class="ti-cancel"></span></a>';
                }
            },
        ]
    });

}


function fillActivitiesSubtypesSuccess(res){
	var c = 'fillActivitiesSubtypesSuccess => ' + res.data.length;
	console.log(c);
}

function fillSessionRules(){
	fillTable('session_rules');
}

function fillSessionRulesSuccess(res){
	var c = 'fillSessionRulesSuccess => ' + res.data.length;
	console.log(c);
	//alert(c);
}

function fillHolidaySuccess(res){
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id,data1[i].date,data1[i].description]);
    });

    $('#holidayTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Description", "width": "120px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+data+'"><span class="ti-cancel"></span></a>';
                }
            },
        ]
    });
}

function fillZoneSuccess(res){
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].id,data1[i].name,data1[i].id]);
    });

    $('#zoneTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+data+'"><span class="ti-cancel"></span></a>';
                }
            },
        ]
    });
}

function btnsave(name, url){
	if (name == 'user'){
		saveUser(url);
	} else if (name == 'zone'){
		saveZone(url);
	} else if (name == 'holiday'){
		saveHoliday(url);
	}
}

function saveUser(url) {
    var _token = $('#_token').val();
	var first_name = $("#first_name").val();
	var last_name = $('#last_name').val();
	var username = $('#username').val();
	var role = $('#role').val();
	var practice_id = $('#practice_id').val();
	var email_id = $('#email_id').val();
	var phone_num = $('#phone_num').val();
    $(".preloader").show();
	$.ajax({
        type: "POST",
        url:"{{ URL::to('register/update') }}",
        data:{ 
            "_token" : _token,
        	"firstName" : first_name,
        	"lastName" : last_name,
        	"userName" : username,
        	"role" : role,
        	"emailID" : email_id,
        	"mobileNo" : phone_num,
        	"practiceID" : practice_id,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    //window.location.href = root_url;
                }
             }
    });
}

function saveZone(url) {
    var _token = $('#_token').val();
	var name = $("#name").val();
	var description = $('#description').val();
	var practice_id = $('#practice_id').val();
	var is_active = $('#is_active').val();
    $(".preloader").show();
	$.ajax({
        type: "POST",
        url:"{{ URL::to('practices/addzone') }}",
        data:{ 
            "_token" : _token,
        	"name" : name,
        	"description" : description,
        	"practice_id" : practice_id,
        	"is_active" : is_active,
        	"practiceID" : practice_id,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    //window.location.href = root_url;
                }
             }
    });
}

function saveHoliday(url) {
    var _token = $('#_token').val();
	var date = $("#date").val();
	var description = $('#description').val();
	var practice_id = $('#practice_id').val();
	var is_active = $('#is_active').val();
    $(".preloader").show();
	$.ajax({
        type: "POST",
        url:"{{ URL::to('practices/addholiday') }}",
        data:{ 
            "_token" : _token,
        	"date" : date,
        	"description" : description,
        	"practice_id" : practice_id,
        	"is_active" : is_active,
        	"practiceID" : practice_id,
            },success:function(result){
                var json =JSON.parse(result);
                var root_url = $('#root_url').val();
                if (json.status == 0){
                    $(".preloader").hide();
                    alert(json.message);
                } else {
                    $(".preloader").hide();
                    alert(json.message);
                    //window.location.href = root_url;
                }
             }
    });
}

function listPayer(){
	var data = fillTable('handler_insurances');
	alert(data);
}

function selectedPayer(){
	var data = fillTable('insurances');
}

function fillSelectedPayer(){
	alert('fillSelectedPayer()');
	fillTable('selectedpayer');
}

function fillSelectedPayerSuccess(res){
    var data1 = res.data;
    var dt = [];

    $.each(data1,function(i,  v) {
        dt.push([data1[i].insurance_id, data1[i].payer_name,
        	data1[i].cms1500_31, 
        	data1[i].id]);
    });

    //console.log(data1);

    $('#selectedPayerTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "200px", "targets": 2 },
            { "width": "100px", "targets": 3 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Name", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "cms1500_31", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return '<input type="text" id="cms1500_31_1" value="'+data+'"/>';
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<input type="button" data-id="' + data +'" class="btnUpdatePayerSetup" onclick="updatePayerSetup(this)" value="Update"/>';
                }
            },
        ]
    });
}

function updatePayerSetup(_this){
    var _token = $('#_token').val();
	var id = $(_this).data('id');
	var cms1500_31 = $('#cms1500_31_1').val();
	var root_url = $("#hidden_root_url").val();
	var url = root_url + 'insurances/addsetup';

    $.ajax({
        type : "POST",
        url : url,
        data:{
            "_token" : _token,
            "id" : id,
            "insurance_id" : id,
            "cms1500_31": cms1500_31,
        },success:function(result) {
            $('#loader').hide();
            console.log(result);
            var res = JSON.parse(result);
            if(res.status == 0){
                alert(res.message);
                return;
            }  else {
                alert(res.message);
                //window.location.href = root_url + "physician/list"; 
            }
        }
    });
	
}

</script>	
