CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0359f544699d9e93c3c3c05c67bcf896b27b04f91edb2dc5fa7df7af778605b92995f61fe9f82da7', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:49:00', '2018-08-04 14:49:00', '2019-08-04 20:19:00'),
('0d0f0bb65e9c8c4a939b3bdd8cdda30a832b04b72fa6f598deec321bf8e8f3225bdc188811245e5d', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:20:24', '2018-08-05 11:20:24', '2019-08-05 16:50:24'),
('0d2d0248d7f12962b33b5a8baa0652dc4c0288624a460e09eaa13e11593f0acebf7ded7623b76417', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:13:39', '2018-08-05 11:13:39', '2019-08-05 16:43:39'),
('10439690d383bf5cd8fd1d650032bf301f6f920cd4da19bb4facee9628923e5070e8744c44d91ff6', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:17:26', '2018-08-05 08:17:26', '2019-08-05 13:47:26'),
('113a47366b580a94e379c107c5980864da5ff0447e30674c5c09f1fb8a016b3649dd106d6ac1fb50', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:13:26', '2018-08-05 11:13:26', '2019-08-05 16:43:26'),
('1ad6a9729008166c0a07c34759b8d0bbecbec3a4d95d223b8322eb32854df381df19fe508dcdfe80', 3, 1, 'MyApp', '[]', 0, '2018-08-06 03:14:22', '2018-08-06 03:14:22', '2019-08-06 08:44:22'),
('2339dc8f7795ef2998bb4fa43869433293f6f4fd8464b9e041545316a8e01e44b08a8edcb9f94c6c', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:09:26', '2018-08-05 08:09:26', '2019-08-05 13:39:26'),
('31275d788581b04ad1487ef76530e289617ff3d5084de966b2a230e24d67269b1be741b5c8a1c8f5', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:13:46', '2018-08-05 08:13:46', '2019-08-05 13:43:46'),
('3713935e4f2c6054e29ea28d94e556bdeec5b368855c6e410937c1fc4415207ecb561f1938938d8c', 1, 1, 'MyApp', '[]', 0, '2018-08-04 13:23:35', '2018-08-04 13:23:35', '2019-08-04 18:53:35'),
('3c123c5260a880cc043d0a352ad98beccca570ae206e9b6234f93f53a577a22f6d45ce3e0a10a0ef', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:09:38', '2018-08-05 11:09:38', '2019-08-05 16:39:38'),
('40adf24890b23c4c0b064552e87f1644ff7c3ef85d3f889519a565e5f5cab6b6becb7755133ab7e0', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:17:48', '2018-08-05 08:17:48', '2019-08-05 13:47:48'),
('41af0d2bbf101b2cf2774f0a1196703ae0f9c24414e0bd921e098198289feaf40ecbecf93a868946', 3, 1, 'MyApp', '[]', 0, '2018-08-06 09:29:12', '2018-08-06 09:29:12', '2019-08-06 14:59:12'),
('4471cf35cfe568b0938da86b1da092bb8cc14df2d3418c79baaaa613bf52fcf7f42328ac4eb35b4c', 3, 1, 'MyApp', '[]', 0, '2018-08-06 21:37:44', '2018-08-06 21:37:44', '2019-08-07 03:07:44'),
('511e7aef299e8d4d868c129e4df0d0bfd936497317d5fb2f55df3d0c333c41a2d81ac7cf1b4ae4c6', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:45:51', '2018-08-04 14:45:51', '2019-08-04 20:15:51'),
('53f0ab16d53e6e68d0a6d9d4a796ad7a053139762fbf366c16b789c8a5b0721157c856fb953c1a71', 3, 1, 'MyApp', '[]', 0, '2018-08-06 09:29:25', '2018-08-06 09:29:25', '2019-08-06 14:59:25'),
('543ab803a44833a531fc4af73012acd9231d7e42f52831fc053eb39605b043defe7e5891c06649a2', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:19:41', '2018-08-05 11:19:41', '2019-08-05 16:49:41'),
('547b4b332703b995bd4e51dd8d9308f340e510e94b7850cf23f4d737c94e5ce30e0cb2b8ae651be6', 3, 1, 'MyApp', '[]', 0, '2018-08-06 21:37:17', '2018-08-06 21:37:17', '2019-08-07 03:07:17'),
('5993d7264d65365042017beb02bea4dbad8afdb41455677619793e12593c41d02f5c9f26acd7ffbf', 1, 1, 'MyApp', '[]', 0, '2018-08-04 13:24:16', '2018-08-04 13:24:16', '2019-08-04 18:54:16'),
('655565a6a64df484742f32820264cbb25f3314d7a31e34cfe5f1dd6cc4e4dd994de0091fa79f0392', 2, 1, 'MyApp', '[]', 0, '2018-08-04 13:59:22', '2018-08-04 13:59:22', '2019-08-04 19:29:22'),
('691ec2eb23ed6ca2106c75ed2ad367081a9c32f8526bc659d9b86f702aed9552e11c3f4af5ffd6e7', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:14:31', '2018-08-05 11:14:31', '2019-08-05 16:44:31'),
('6975875a8920a5eb7d6f4aea402674514a5d492f859ef57e9b9efaff632fdac96e0ab0a1f2ef5b88', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:14:59', '2018-08-05 11:14:59', '2019-08-05 16:44:59'),
('6d6eef2ac97cc9fa89b75fd7c1749bf421e8ddcd4593ebbad441d9f275565480981f576cb33ea1ed', 1, 1, 'MyApp', '[]', 0, '2018-08-04 13:23:29', '2018-08-04 13:23:29', '2019-08-04 18:53:29'),
('6d93bc6ca687c9631c46c0de35b8fc0b8d1e0f3db24f0b4aa9bef53520c3e725f9c287dc2e32b827', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:49:10', '2018-08-04 14:49:10', '2019-08-04 20:19:10'),
('7515695fa003aa78221e93a67f3025a7748e5cb12a51a56b52cb2815a515fcf44a6b702e22c59d94', 4, 1, 'MyApp', '[]', 0, '2018-08-04 16:58:46', '2018-08-04 16:58:46', '2019-08-04 22:28:46'),
('7629be28bdd1d6f508e35be998b96ffa69453c340d62bb4536c02857c80cbf2b7f28baf37b7704f2', 3, 1, 'MyApp', '[]', 0, '2018-08-06 06:37:11', '2018-08-06 06:37:11', '2019-08-06 12:07:11'),
('77322b696a368328a62ac042bb3e46bf5254b7fa800e1d0b5d48a1af9023f20f9bebd3f189606115', 3, 1, 'MyApp', '[]', 0, '2018-08-06 09:31:05', '2018-08-06 09:31:05', '2019-08-06 15:01:05'),
('7fb66bc9e7f7d8d41b7c9c085557b1cd77a1ae5b778fd7a09040ebb5c264558a9063665478dc89d1', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:11:46', '2018-08-05 08:11:46', '2019-08-05 13:41:46'),
('8086e06d85e79e77a84aa2a21baa07dd7c4b352bbc0cb5fb09b345c385fbb0db025838fec4caa57e', 1, 1, 'MyApp', '[]', 0, '2018-08-04 12:58:56', '2018-08-04 12:58:56', '2019-08-04 18:28:56'),
('83b4f059e23e828f088622d8ae2a04afa564db67b4330be1038dcbcb077bb81f7aa8333df9da5f51', 1, 1, 'MyApp', '[]', 0, '2018-08-04 13:26:52', '2018-08-04 13:26:52', '2019-08-04 18:56:52'),
('8f2e120012de49a5f8ff7bf1281d02f886d93f3f8abdf25af42b81e4d774e7504759f156dbe9dda2', 3, 1, 'MyApp', '[]', 0, '2018-08-06 06:39:05', '2018-08-06 06:39:05', '2019-08-06 12:09:05'),
('969d6dbb6ad1136b9943f35a610a741ba8c11a284fd530480378ae2e78d8e1eeac8e96c374f00b13', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:17:04', '2018-08-05 11:17:04', '2019-08-05 16:47:04'),
('a7efc2f0df524ff4c733bbeca4ca1eacc48252360bf82e2d5e4db84a45dc8d53ae12691d0e77474b', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:16:55', '2018-08-05 08:16:55', '2019-08-05 13:46:55'),
('aa49215f2bbc8f2f8d90e0102fa2bdec5a60af1e48a3f2d8c3f9d6a067d098e550498fda456a3d9d', 3, 1, 'MyApp', '[]', 0, '2018-08-05 20:15:51', '2018-08-05 20:15:51', '2019-08-06 01:45:51'),
('ac53db770d1f85151097180c2a060ccbadffe29935fa4128994422c4196aa2b30308f2670284f4f4', 3, 1, 'MyApp', '[]', 0, '2018-08-04 17:12:25', '2018-08-04 17:12:25', '2019-08-04 22:42:25'),
('ae4eb2c920491e2772374d196e9d47e84d80d5b90681f8afafca4363c75a482da85ecc8aea0ebde4', 3, 1, 'MyApp', '[]', 0, '2018-08-06 09:27:36', '2018-08-06 09:27:36', '2019-08-06 14:57:36'),
('ae58c6883f985064cd2203a2e3f5e8a89353693ae00b89fecdbd9ba7302762ecd043e0287bcfc53a', 3, 1, 'MyApp', '[]', 0, '2018-08-06 08:20:14', '2018-08-06 08:20:14', '2019-08-06 13:50:14'),
('b43ebade5c23b1cb67b5364db8d94ef58bcf819a53d57034932921541bee67fb45ece3a2703d30e7', 3, 1, 'MyApp', '[]', 0, '2018-08-06 08:57:36', '2018-08-06 08:57:36', '2019-08-06 14:27:36'),
('b682c8fac8f9a28b88907b6ef9827c308a0d17ab9981948cbb78b68d1ff9ad9e923a449e1cc74828', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:18:28', '2018-08-05 11:18:28', '2019-08-05 16:48:28'),
('b715eb16aeda52fa58d7d2ff5f6174c202f4494b6543242ece51e4c2c9d6615f05b88f842cb078f5', 2, 1, 'MyApp', '[]', 0, '2018-08-04 13:56:50', '2018-08-04 13:56:50', '2019-08-04 19:26:50'),
('c3f5687304a347e3f094efa4c336fa73d94e7988c7a1965eb2f242a314c38e78045fe82bce2e7e99', 2, 1, 'MyApp', '[]', 0, '2018-08-04 13:59:16', '2018-08-04 13:59:16', '2019-08-04 19:29:16'),
('c64a7ff3b17fe210f0201e0002e60d0fa87c9f38738d67375b64eaade0cab4894fed9defa94366d1', 3, 1, 'MyApp', '[]', 0, '2018-08-06 07:49:57', '2018-08-06 07:49:57', '2019-08-06 13:19:57'),
('d7a10a29542e8134ec6c469cba7de22bcb1be7d71c2b8332ca5c529937ab2a3613d260d62970e5ad', 3, 1, 'MyApp', '[]', 0, '2018-08-04 16:58:21', '2018-08-04 16:58:21', '2019-08-04 22:28:21'),
('db909d37ddc1f6ab18b74d02e85e7b8da93b1f55800dde6d58ca138597d714d7e3b26cd093eff32f', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:48:35', '2018-08-04 14:48:35', '2019-08-04 20:18:35'),
('e536ef943ffed3043821f41eafeb5f3ebb192f74af5c400c1055b0cfe6bf47d730a98c26d35a73fc', 3, 1, 'MyApp', '[]', 0, '2018-08-06 08:10:58', '2018-08-06 08:10:58', '2019-08-06 13:40:58'),
('e77ad9acaa67c6004a68174a8f4054b5704104fe5b4b254d4874f13af84d0a4ef8e7beacc668649d', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:10:17', '2018-08-05 11:10:17', '2019-08-05 16:40:17'),
('ebb7b7d372c5715e53d1f1ea5a2f0327fbed6339be9737225b9293f8393acc504f05f5e5b57e23bf', 3, 1, 'MyApp', '[]', 0, '2018-08-06 21:24:25', '2018-08-06 21:24:25', '2019-08-07 02:54:25'),
('f592087a19bb6c02a6658c72a0eece503b1361daedccc80298276ff4602e27b7fdcfdafbea2edb02', 3, 1, 'MyApp', '[]', 0, '2018-08-05 11:17:48', '2018-08-05 11:17:48', '2019-08-05 16:47:48'),
('f79753b6a356650ef4aeba5a80922940eb5e7f4856a9e98f1dda9ef1eefa26185fcb50118123b335', 5, 1, 'MyApp', '[]', 0, '2018-08-05 08:28:07', '2018-08-05 08:28:07', '2019-08-05 13:58:07'),
('f81a0dcb369742208084a40c85b6bb5245c1563c92b22a46699e2071248c15a10394bf7ec830c8c9', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:45:16', '2018-08-04 14:45:16', '2019-08-04 20:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '7X61XonRmaLjqaue2Y8O6hKLOHEmeuhVygqS6WHk', 'http://localhost', 1, 0, 0, '2018-08-04 11:58:00', '2018-08-04 11:58:00'),
(2, NULL, 'Laravel Password Grant Client', '3PF0nBejsgERULceggQZQuvQQEHCpLA1Gn2vqxTt', 'http://localhost', 0, 1, 0, '2018-08-04 11:58:00', '2018-08-04 11:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-08-04 11:58:00', '2018-08-04 11:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_acl_list`
--

CREATE TABLE `tb_acl_list` (
  `id` bigint(20) NOT NULL,
  `acl_key` varchar(128) NOT NULL,
  `acl_name` varchar(64) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_acl_list`
--

INSERT INTO `tb_acl_list` (`id`, `acl_key`, `acl_name`, `created_at`, `updated_at`) VALUES
(1, 'ACL_CLIENTS', 'Clients', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(2, 'ACL_EMPLOYEES', 'Employees', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(3, 'ACL_SCHEDULER', 'Scheduler', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(4, 'ACL_EDI', 'EDI', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(5, 'ACL_STATEMENTS', 'Statements', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(6, 'ACL_LEDGER', 'Ledger', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(7, 'ACL_DEPOSITS', 'Deposits', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(8, 'ACL_CRM', 'CRM', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(9, 'ACL_REPORTS', 'Reports', '2018-08-06 02:11:02', '2018-08-06 02:11:02'),
(10, 'ACL_SETTINGS', 'Settings', '2018-08-06 02:11:02', '2018-08-06 02:11:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `role` varchar(128) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email_id` varchar(128) NOT NULL,
  `phone_num` varchar(16) NOT NULL,
  `is_logged` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`id`, `first_name`, `last_name`, `username`, `role`, `password`, `email_id`, `phone_num`, `is_logged`, `created_at`, `updated_at`) VALUES
(7, 'a', 'a', 'a', 'Admin', '76af7efae0d034d1e3335ed1b90f24b6cadf2bf1', 'a@a.com', '1234567890', 0, '2018-08-05 08:36:53', '2018-08-05 08:36:53'),
(8, 'b', 'b', 'b', 'Admin', '76af7efae0d034d1e3335ed1b90f24b6cadf2bf1', 'b@b1.com', '1234567890', 0, '2018-08-05 08:37:49', '2018-08-05 08:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_acl_list`
--

CREATE TABLE `tb_user_acl_list` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `acl_key` varchar(128) NOT NULL,
  `is_create` int(1) NOT NULL DEFAULT '0',
  `is_edit` int(1) NOT NULL DEFAULT '0',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `is_view` int(1) NOT NULL DEFAULT '0',
  `is_display` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user_acl_list`
--

INSERT INTO `tb_user_acl_list` (`id`, `user_id`, `acl_key`, `is_create`, `is_edit`, `is_delete`, `is_view`, `is_display`, `created_at`, `updated_at`) VALUES
(59, 7, 'ACL_SETTINGS', 0, 0, 0, 0, 1, '2018-08-06 09:55:06', '2018-08-06 09:55:06'),
(58, 7, 'ACL_REPORTS', 0, 0, 0, 0, 1, '2018-08-06 09:55:06', '2018-08-06 09:55:06'),
(57, 7, 'ACL_CRM', 1, 0, 0, 0, 0, '2018-08-06 09:55:06', '2018-08-06 10:08:14'),
(56, 7, 'ACL_DEPOSITS', 1, 0, 0, 0, 0, '2018-08-06 09:55:06', '2018-08-06 10:08:14'),
(55, 7, 'ACL_LEDGER', 0, 1, 0, 0, 0, '2018-08-06 09:55:06', '2018-08-06 10:10:30'),
(54, 7, 'ACL_STATEMENTS', 0, 1, 0, 0, 0, '2018-08-06 09:55:06', '2018-08-06 10:10:30'),
(53, 7, 'ACL_EDI', 0, 0, 1, 0, 0, '2018-08-06 09:55:06', '2018-08-06 10:10:30'),
(52, 7, 'ACL_SCHEDULER', 0, 0, 1, 0, 0, '2018-08-06 09:55:06', '2018-08-06 10:10:30'),
(51, 7, 'ACL_EMPLOYEES', 0, 0, 0, 0, 0, '2018-08-06 09:55:06', '2018-08-06 09:55:06'),
(50, 7, 'ACL_CLIENTS', 0, 0, 0, 0, 0, '2018-08-06 09:55:06', '2018-08-06 09:55:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'rish', 'rish@avr.com', '$2y$10$1WN0cW0Q0.wxhCkRpOeLR.TKo9HXDHSL5.ZRS1XgCAq6/YEP4SxPe', NULL, '2018-08-04 13:56:50', '2018-08-04 13:56:50'),
(3, 'a', 'a@a.com', '$2y$10$mEXRcBYhi0VAtXNGZgoV2uCF9cY84VRguqzVrXOPX1i4sCOr4FiIG', NULL, '2018-08-04 16:58:21', '2018-08-04 16:58:21'),
(4, 'b', 'b@b.com', '$2y$10$PfbNMARkySTirI/Ezqwe6eKFG3F2lrT9y6xSqSa9g2IegiYaFtzgq', NULL, '2018-08-04 16:58:46', '2018-08-04 16:58:46'),
(5, 'a', 'a.a@a.com', '$2y$10$PyPnpWrJaAoSJJLoiNyPjeZF0pVxJbWvst0N1kQTX5TR3QR2iIYq2', NULL, '2018-08-05 08:28:06', '2018-08-05 08:28:06');




--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tb_acl_list`
--
ALTER TABLE `tb_acl_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user_acl_list`
--
ALTER TABLE `tb_user_acl_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);


ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_acl_list`
--
ALTER TABLE `tb_acl_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_user_acl_list`
--
ALTER TABLE `tb_user_acl_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

