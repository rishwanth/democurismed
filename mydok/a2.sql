auto_increment,
  `created_at` datetime not null,
  `updated_at` datetime not null,
    primary key(id)  
) engine=innodb;





-- --------------------------------------------------------

--
-- Table structure for table `m_crm`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_depositdetails`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_deposits`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_details`
--


--
-- Dumping data for table `m_details`
--

INSERT INTO `m_details` (`detail`, `detailvalue1`, `detailvalue2`, `created_at`, `updated_at`) VALUES
('ChartNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00'),
('BatchNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00'),
('ClaimNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_documents`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_edi`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_employer`
--

CREATE TABLE `m_employer` (
  `employerID` int(11) NOT NULL,
  `employerName` varchar(100) NOT NULL,
  `employerAddr` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_guarantor`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_insurances`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_insured`
--

CREATE TABLE `m_insured` (
  `insuredID` int(11) NOT NULL,
  `insured_fullName` varchar(50) NOT NULL,
  `insured_dob` date NOT NULL,
  `insured_gender` varchar(1) NOT NULL,
  `insured_relationship` varchar(10) NOT NULL,
  `insured_ssn` varchar(20) NOT NULL,
  `insured_martialStatus` varchar(20) NOT NULL,
  `insured_medicalRecord` varchar(20) NOT NULL,
  `insured_employment` varchar(20) NOT NULL,
  `insured_employer` varchar(50) NOT NULL,
  `insured_reference` varchar(50) NOT NULL,
  `insured_isActive` int(11) NOT NULL,
  `insured_isDeleted` int(11) NOT NULL,
  `insured_addrStreet1` varchar(50) NOT NULL,
  `insured_addrStreet2` varchar(50) NOT NULL,
  `insured_addrCity` varchar(20) NOT NULL,
  `insured_addrState` varchar(20) NOT NULL,
  `insured_addrCountry` varchar(20) NOT NULL,
  `insured_addrZip` varchar(10) NOT NULL,
  `insured_email` varchar(50) NOT NULL,
  `insured_phoneHome` varchar(50) NOT NULL,
  `insured_phoneWork` varchar(50) NOT NULL,
  `insured_phoneMobile` varchar(50) NOT NULL,
  `insured_emergencyContact` varchar(50) NOT NULL,
  `insured_emergencyPhone` varchar(50) NOT NULL,
  `patientID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_locations`
--

CREATE TABLE `tb_locations` (
  `id` int(11) NOT NULL auto_increment,
  `patient_id` int(3) NOT NULL,
  `home` int(2) NOT NULL,
  `office` int(2) NOT NULL,
  `school` int(2) NOT NULL

-- --------------------------------------------------------

--
-- Table structure for table `m_logs`
--

CREATE TABLE `m_logs` (
  `logID` int(11) NOT NULL,
  `logModule` varchar(50) NOT NULL,
  `logDetail` varchar(1024) NOT NULL,
  `logAction` varchar(50) NOT NULL,
  `logRefID` varchar(20) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_modifiers`
--

CREATE TABLE `m_modifiers` (
  `ID` int(11) NOT NULL,
  `modifierCode` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_patients`
--

create table `tb_patients` (
  `id` bigint not null,
  `full_name` varchar(50) not null,
  `dob` date not null,
  `gender` varchar(1) not null,
  `relationship` varchar(10) not null,
  `ssn` varchar(20) not null,
  `martial_status` varchar(20) not null,
  `medical_record` varchar(20) not null,
  `employment` varchar(20) not null,
  `employer` varchar(50) not null,
  `reference` varchar(50) not null,
  `is_active` int(11) not null,
  `is_deleted` int(11) not null,
  `addr_street1` varchar(50) not null,
  `addr_street2` varchar(50) not null,
  `addr_city` varchar(20) not null,
  `addr_state` varchar(20) not null,
  `addr_country` varchar(20) not null,
  `addr_zip` varchar(10) not null,
  `email` varchar(50) not null,
  `phone_home` varchar(50) not null,
  `phone_work` varchar(50) not null,
  `phone_mobile` varchar(50) not null,
  `notifyemail` int(11) not null,
  `notify_phone` int(11) not null,
  `is_financial_resp` int(1) not null,
  `primary_care_phy` int(11) not null,
  `referring_phy` int(11) not null,
  `default_render_phy` int(11) not null,
  `default_service_loc` varchar(20) not null,
  `chart_num` varchar(20) not null,
  `emergency_contact` varchar(50) not null,
  `emergency_phone` varchar(50) not null,
  `created_by` int(11) not null,
  `last_edited_by` int(11) not null,
  `conduct` varchar(10) not null,
  `practice_id` int(11) not null,
  `created_at` datetime not null,
  `updated_at` datetime not null,
   primary key(id)  
) engine=innodb;

-- --------------------------------------------------------


-- --------------------------------------------------------

--
-- Table structure for table `m_placeofservices`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_policies`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_practices`
--

  

-- --------------------------------------------------------

--
-- Table structure for table `m_procedure`
--

CREATE TABLE `m_procedure` (
  `procedureID` int(11) NOT NULL,
  `procedureCd` varchar(50) NOT NULL,
  `procedureDesc` varchar(500) DEFAULT NULL,
  `procedureCharge` float(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_relationships`
--

create table `tb_relation_ships` (
  `id` bigint not null default 0,
  `full_name` varchar(50) not null,
  `dob` date not null,
  `gender` varchar(10) null default '',
  `relationship` varchar(10) null default '',
  `ssn` varchar(20) null  default '',
  `martial_status` varchar(20) null  default '',
  `medical_record` varchar(20) null  default '',
  `employment` varchar(20) not null,
  `employer` varchar(50) not null,
  `reference` varchar(50) not null,
  `is_active` int(1) null default 1,
  `is_deleted` int(1) null default 0,
  `addr_street1` varchar(50) null default '',
  `addr_street2` varchar(50) null default '',
  `addr_city` varchar(20) null default '',
  `addr_state` varchar(20) null default '',
  `addr_country` varchar(20) null default '',
  `addr_zip` varchar(10) null default '',
  `email` varchar(125) null default '',
  `phone_home` varchar(50) null default '',
  `phone_work` varchar(50) null default '',
  `phone_mobile` varchar(50) null default '',
  `notify_email` int(1) null default 0,
  `notify_phone` int(11) null default 0,
  `primary_care_phy` bigint null default 0,
  `referring_phy` bigint null default 0,
  `default_render_phy` bigint null default 0,
  `default_service_loc` varchar(20) null default '',
  `chart_num` varchar(20) null default '',
  `emergency_contact` varchar(50) null default '',
  `emergency_phone` varchar(50) null default '',
  `created_by` bigint null default 0,
  `last_edited_by` bigint null default 0,
  `conduct` varchar(10) null default '',
  `referral_source` varchar(50) null default '',
  `practice_id` bigint null default 0,
  `created_at` timestamp null,
  `updated_at` timestamp null,
   primary key(id)  
) engine=innodb;

-- --------------------------------------------------------

--
-- Table structure for table `m_serviceLocations`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_statements`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_taxonomycodes`
--

CREATE TABLE `m_taxonomycodes` (
  `ID` int(11) NOT NULL,
  `medicareCode` varchar(50) NOT NULL,
  `medicareProvider` varchar(200) NOT NULL,
  `taxonomy` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_zipcodes`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_zones`
--

CREATE TABLE `m_zones` (
  `zoneID` int(11) NOT NULL,
  `zoneName` varchar(100) NOT NULL,
  `zoneDesc` varchar(100) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO `tb_service_locations` (`id`, `internal_name`, `npi`, `override_ein`, `time_zone`, `legacy_num_type`, `legacy_num`, `billing_name`, `address`, `city`, `state`, `zip`, `phone`, `fax`, `place_of_service`, `clia_num`, `type_of_bill`, `pay_to_name`, `inst_address`, `inst_phone`, `inst_fax`, `is_active`, `practice_id`, `created_by`, `updated_at`, `created_at`) VALUES (NULL, 'Forming Friendships', '414551250', '0', '', '', '', 'Forming Friendships LLC', '123, Valley', 'Vienna', 'MO', '06851', '', '', '11', '', '', '', NULL, '', '', '1', '1', '0', NULL, NULL);