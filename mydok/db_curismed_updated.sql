drop database if exists db_curismed_v1;
create database db_curismed_v1;
use db_curismed_v1;

create table `tb_users` (
  `id` bigint not null auto_increment,
  `first_name` varchar(64) not null,
  `last_name` varchar(64) not null,
  `username` varchar(64) not null,
  `role` varchar(128) not null,
  `password` varchar(64) not null,
  `email_id` varchar(128) not null,
  `phone_num` varchar(16) null default '',
  `is_activated` int(1) null default 0,
  `is_logged` int(1) null default 0,
  `token` varchar(128) null default '',
  `reset_token` varchar(128) null default '',
  `oauth_token` text,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;

drop table if exists `tb_users_session`;

create table `tb_users_session` (
  `id` bigint not null auto_increment,
  `session_token` varchar(128) null default '',
  `user_id` bigint null default 0,
  `date_login` datetime null,
  `date_last_accessed` datetime null,
  `is_active` int(1) null default 0,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;

create table `tb_acl_list` (
  `id` bigint not null auto_increment,
  `acl_key` varchar(128) null default '',
  `acl_name` varchar(64) null default '',
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=myisam;

create table `tb_user_acl_list` (
  `id` bigint not null auto_increment,
  `user_id` bigint null default 0,
  `acl_key` varchar(128) null default '',
  `is_create` int(1) null default 0,
  `is_edit` int(1) null default 0,
  `is_delete` int(1) null default 0,
  `is_view` int(1) null default 0,
  `is_display` int(1) null default 0,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=myisam;

create table `tb_place_of_services` (
  `id` bigint not null auto_increment,
  `code` varchar(20) null default '',
  `name` varchar(50) null default '',
  `description` text null,
  `created_at` datetime,
  `updated_at` datetime,
   primary key(id)  
) engine=innodb;

create table `tb_disposition` (
  `id` bigint not null auto_increment,
  `name` varchar(100) null default '',
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;

create table `tb_history` (
  `id` bigint not null auto_increment,
  `claim_id` bigint null default 0,
  `disposition_id` bigint null default 0,
  `comments` text null,
  `worked_date` datetime null,
  `user_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;


create table `tb_activity` (
  `id` bigint not null auto_increment,
  `case_id` bigint null default 0,
  `auth_num` varchar(32) null default '',
  `name` varchar(64) null default '',
  `cpt` varchar(32) null default '',
  `mod1` varchar(16) null default '',
  `mod2` varchar(10) null default '',
  `mod3` varchar(10) null default '',
  `mod4` varchar(10) null default '',
  `billed_per` varchar(10) null default '',
  `billed_per_time` varchar(10) null default '',
  `total_auth` int(10) null default 0,
  `auth_aype` varchar(50) null default '',
  `remain_auth` int(10) null default 0,
  `rates` decimal(10,2) null default 0,
  `max1` varchar(10) null default '',
  `max2` varchar(10) null default '',
  `max3` varchar(10) null default '',
  `per1` varchar(10) null default '',
  `per2` varchar(10) null default '',
  `per3` varchar(10) null default '',
  `is_1` varchar(10) null default '',
  `is_2` varchar(10) null default '',
  `is_3` varchar(10) null default '',
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;


create table `tb_locations` (
  `id` bigint not null auto_increment,
  `patient_id` bigint null default 0,
  `home` int(2) null default 0,
  `office` int(2) null default 0,
  `school` int(2) null default 0,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;

create table `tb_alerts` (
  `id` bigint not null auto_increment,
  `desc` text null,
  `patient_id` bigint null default 0,
  `is_edit_patient` int(1) null default 0,
  `is_schedule_app` int(1) null default 0,
  `is_charge` int(1) null default 0,
  `is_view_claims` int(1) null default 0,
  `is_deposits` int(1) null default 0,
  `is_statements` int(1) null default 0,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;

drop table if exists `tb_edi`;

create table `tb_edi` (
  `id` bigint not null auto_increment,
  `invoice_num` varchar(50) null default '',
  `location` varchar(100) null default '',
  `practice_id` bigint null default 0,
  `created_on` datetime null,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;

create table `tb_appointments` (
  `id` bigint not null auto_increment,
  `start_date` datetime not null,
  `end_date` datetime not null,
  `patient_name` varchar(50) null default '',
  `auth` varchar(20) null default '',
  `activity_id` bigint null default 0,
  `location` varchar(20) null default '',
  `provider` varchar(50) null default '',
  `desc` text null,
  `status` varchar(20) null default '',
  `patient_id` bigint  null default 0,
  `provider_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;


create table `tb_auth` (
  `id` bigint not null auto_increment,
  `patient_id` bigint null default 0,
  `case_id` bigint null default 0,
  `policy_entity` int(5) null default 0,
  `tos` varchar(50) null default '',
  `auth_num` varchar(100) null default '',
  `start_date` date null,
  `end_date` date null,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;


create table `tb_billing_code` (
  `id` bigint not null auto_increment,
  `cd` varchar(16) null default '',
  `desc` varchar(100) null default '',
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;


create table `tb_cases` (
  `id` bigint not null auto_increment,
  `patient_id` bigint not null,
  `is_closed` int(1) not null default 0,
  `description` varchar(64) not null,
  `allow_print` int(1) not null default 1,
  `is_deleted` int(1) not null default 0,
  `date` date null,
  `chart_num` varchar(32) not null,
  `global_coverage_until` varchar(64) null,
  `employer` varchar(50) null,
  `emp_status` varchar(50) null,
  `retirement_date` date null,
  `location` varchar(50) null,
  `work_phone` varchar(50) null,
  `extension` varchar(50) null,
  `stud_status` varchar(50) null,
  `guarantor` varchar(50) null,
  `marital_status` varchar(50) null,
  `assigned_provider` bigint not null default 0,
  `referring_provider` bigint not null default 0,
  `supervising_provider` bigint not null default 0,
  `operating_provider` bigint not null default 0,
  `other_provider` bigint not null default 0,
  `referral_source` varchar(50) null,
  `attorney` varchar(50) null,
  `facility` varchar(50) null,
  `billing_code` varchar(50) null,
  `price_code` varchar(50) null,
  `other_arrangements` varchar(50) null,
  `authorized_through` varchar(50) null,
  `auth_num` varchar(50) null,
  `last_visit` date null,
  `no_of_visits` bigint null,
  `total_visits` int(10) null,
  `auth_id` varchar(50) null,
  `last_visit_num` varchar(50) null,
  `principal_diag` varchar(100) null,
  `def_diag_2` varchar(100) null,
  `def_diag_3` varchar(100) null,
  `def_diag_4` varchar(100) null,
  `poa1` varchar(100) null,
  `poa2` varchar(100) null,
  `poa3` varchar(100) null,
  `poa4` varchar(100) null,
  `allergies_notes` varchar(1000) null,
  `edi_notes` varchar(1000) null,
  `rep_type_code` varchar(50) null,
  `attach_ctrl_num` varchar(50) null,
  `rep_trans_code` varchar(50) null,
  `injury_date` date null,
  `illness_indicator` varchar(50) null,
  `first_consult_date` date null,
  `similar_symptoms` varchar(100) null,
  `same_symptom` bigint null,
  `emp_related` bigint null,
  `emergency` bigint null,
  `related_to` varchar(50) null,
  `related_state` varchar(50) null,
  `nature` varchar(50) null,
  `last_xray` date null,
  `death_status` varchar(50) null,
  `unable_to_work_from` date null,
  `unable_to_work_to` date null,
  `total_disab_from` date null,
  `tot_disab_to` date null,
  `part_disab_from` date null,
  `part_disab_to` date null,
  `hosp_form` date null,
  `hosp_to` date null,
  `return_work_indicator` varchar(50) null,
  `disability_percent` varchar(50) null,
  `last_work_date` date null,
  `pregnant` bigint null,
  `estimated_dob` date null,
  `date_assumed_care` date null,
  `date_relinquished_care` date null,
  `outside_lab` bigint null,
  `lab_charges` bigint null,
  `local_use_a` varchar(20) null,
  `local_use_b` varchar(20) null,
  `indicator` varchar(50) null,
  `ref_date` date null,
  `prescrip_date` date null,
  `prior_auth_num` varchar(50) null,
  `extra1` varchar(50) null,
  `extra2` varchar(50) null,
  `extra3` varchar(50) null,
  `extra4` varchar(50) null,
  `outside_provider` varchar(50) null,
  `date_last_seen` date null,
  `epsdt` bigint null,
  `resubmission_date` date null,
  `family_planning` bigint null,
  `original_ref_no` varchar(50) null,
  `service_auth` varchar(50) null,
  `non_avail_indicator` varchar(50) null,
  `branch_service` varchar(50) null,
  `sponsor_stat` varchar(50) null,
  `spl_program` varchar(50) null,
  `sponsor_guide` varchar(50) null,
  `eff_start_date` date null,
  `eff_end_date` date null,
  `care_plan_over_sight` varchar(50) null,
  `hospice_num` varchar(50) null,
  `cliano` varchar(50) null,
  `mamo_certification` varchar(50) null,
  `ref_access_num` varchar(50) null,
  `demo_code` varchar(50) null,
  `assign_indicator` varchar(50) null,
  `insurance_type_code` varchar(50) null,
  `timely_filling_indicator` varchar(50) null,
  `epsdt_ref_code1` varchar(50) null,
  `epsdt_ref_code2` varchar(50) null,
  `epsdt_ref_code3` varchar(50) null,
  `homebound` bigint null,
  `ideno` varchar(50) null,
  `condition_indicator` varchar(50) null,
  `cert_code_applies` bigint null,
  `code_category` varchar(50) null,
  `total_visit_rendered` varchar(50) null,
  `total_visit_projected` varchar(50) null,
  `num_of_visits_home_health` bigint null,
  `num_of_units` bigint null,
  `duration` bigint null,
  `discipline_type_code` varchar(50) null,
  `delivery_pattern_code` varchar(50) null,
  `delivery_time_code` varchar(50) null,
  `freq_period` bigint null,
  `freq_count` bigint null,
  `create_dby` bigint null,
  `is_cash` bigint null,
  `updated_at` datetime,
  `created_at` datetime,
  primary key(id)
) engine=innodb;

create table `tb_documents` (
  `id` bigint not null auto_increment,
  `name` text null,
  `type` varchar(10) null default '',
  `path` text null,
  `status` varchar(50) null default '',
  `notes` text null,
  `patient_id` bigint null default 0,
  `user_id` bigint null default 0,
  `practice_id` bigint null default 0,
  `created_on` varchar(30) null default '',
  `updated_at` datetime,
  `created_at` datetime,
  primary key(id)
) engine=innodb;
  
create table `tb_statements` (
  `id` bigint not null auto_increment,
  `name` varchar(50) null default '',
  `patient_id` bigint null default 0,
  `location` text null,
  `created_on` datetime null,
  `updated_at` datetime,
  `created_at` datetime,
  primary key(id)
) engine=innodb;

create table `tb_claims` (
  `id` bigint not null auto_increment,
  `case_id` bigint null default 0,
  `app_id` bigint null default 0,
  `auth_num` varchar(40) null default '',
  `from_date` date null,
  `to_date` date null,
  `proced` varchar(50) null default '',
  `mod1` varchar(50) null default '',
  `units` int(5) null default 0,
  `charge` decimal(10,2) null default 0,
  `total` decimal(10,2) null default 0,
  `diag1` varchar(50) null default '',
  `diag2` varchar(50) null default '',
  `diag3` varchar(50) null default '',
  `diag4` varchar(50) null default '',
  `applied` bigint null default 0,
  `notes` text null,
  `ref_code` varchar(15) null default '',
  `place_of_service` varchar(50) null default '',
  `is_posted` bigint null default 0,
  `is_sent` int(1) null default 0,
  `is_sec_claim` int(1) null default 0,
  `allowed` decimal(10,2) null default 0,
  `paid` decimal(10,2) null default 0,
  `adjustment` decimal(10,2) null default 0,
  `adjust_code` varchar(100) null default '',
  `claim_balance` decimal(10,2) null default 0,
  `claim_balance_pat` decimal(10,2) null default '0.00',
  `reason` text null,
  `status` varchar(50) null default '',
  `copay` decimal(10,2) null default 0,
  `deductible` decimal(10,2) null default 0,
  `coins` decimal(10,2) null default 0,
  `primary_care_phy` bigint null default 0,
  `service_loc_id` bigint null default 0,
  `claim_status` varchar(50) null default '',
  `insurance_id` bigint null default 0,
  `created_by` bigint null default 0,
  `is_deleted` int(1) null default 0,
  `sent_batch_number` bigint null default 0,
  `claim_number` varchar(20) null default '',
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;

create table `tb_details` (
  `id` bigint not null auto_increment,
  `detail` varchar(20) null default '',
  `detailvalue1` bigint null default 0,
  `detailvalue2` text null,
  `created_at` datetime,
  `updated_at` datetime,
   primary key(id)  
) engine=innodb;

create table `tb_claims_history` (
  `id` bigint not null auto_increment,
  `claim_id` bigint null default 0,
  `pou` varchar(50) null default '',
  `purpose` varchar(50) null default '',
  `notes` text null,
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;

create table `tb_service_locations` (
  `id` bigint not null auto_increment,
  `internal_name` varchar(100) null default '',
  `npi` varchar(100) null default '',
  `override_ein` bigint null default 0,
  `time_zone` varchar(100) null default '',
  `legacy_num_type` varchar(100) null default '',
  `legacy_num` varchar(100) null default '',
  `billing_name` varchar(100) null default '',
  `address` text null,
  `city` varchar(30) null default '',
  `state` varchar(30) null default '',
  `zip` varchar(30) null default '',
  `phone` varchar(100) null default '',
  `fax` varchar(100) null default '',
  `place_of_service` varchar(100) null default '',
  `clia_num` varchar(100) null default '',
  `type_of_bill` varchar(100) null default '',
  `pay_to_name` varchar(100) null default '',
  `inst_address` text null,
  `inst_phone` varchar(100) null default '',
  `inst_fax` varchar(100) null default '',
  `is_active` int(1) null default 1,
  `practice_id` bigint null default 0,
  `created_by` bigint null default 0,
  `updated_at` datetime,
  `created_at` datetime,
    primary key(id)  
) engine=innodb;


create table `tb_claims_submission` (
  `id` bigint not null auto_increment,
  `patient_id` bigint null default 0,
  `claim_id` bigint null default 0,
  `claim_number` varchar(11) null default '',
  `submitted_at` datetime null,
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;


create table `tb_code_cpt` (
  `id` bigint not null auto_increment,
  `cpt_code` varchar(20) null default '',
  `description` text null,
  `charge` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
   primary key(id)  
) engine=innodb;


create table `tb_code_icd` (
  `id` bigint not null auto_increment,
  `icd_code` varchar(20) null default '',
  `dx_code` varchar(50) null default '',
  `long_description` text null,
  `short_description` text null,
  `code_type` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;

create table `tb_comments` (
    `id` bigint not null auto_increment,
    `patient_id` bigint null default 0,
    `comment_by` bigint null default 0,
    `comment_text` text null,
    `is_deleted` int(1) null default 0,
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;

create table `tb_zipcodes` (
  `id` bigint not null auto_increment,
  `state` varchar(10) null default '',
  `city` varchar(100) null default '',
  `zip` varchar(10) null default '',
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;


create table `tb_insurances` (
  `id` bigint not null auto_increment,
  `payer_name` varchar(50) null default '',
  `enr` bigint null default 0,
  `typ` varchar(50) null default '',
  `st` varchar(50) null default '',
  `lob` varchar(50) null default '',
  `rte` bigint null default 0,
  `rts` bigint null default 0,
  `era` bigint null default 0,
  `sec` bigint null default 0,
  `note` text null,
  `clearing_house_payor_id` varchar(50) null default '',
  `addr` text null,
  `city` varchar(30) null default '',
  `state` varchar(10) null default '',
  `zip` varchar(20) null default '',
  `phone` varchar(20) null default '',
  `fax` varchar(20) null default '',
  `abforbilling` varchar(20) null default '',
  `practice_id` bigint null default 0,
  `is_active` int(1) null default 1,
  `created_by` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;

create table `tb_policies` (
  `id` bigint not null auto_increment,
  `case_id` bigint null default 0,
  `auth_num` varchar(50) null default '',
  `policy_entity` bigint null default 0,
  `insurance_id` bigint null default 0,
  `policy_holder` bigint null default 0,
  `relationship_to_insured` varchar(20) null default '',
  `relation_id` bigint null default 0,
  `policy_num` varchar(50) null default '',
  `group_num` varchar(50) null default '',
  `group_name` varchar(50) null default '',
  `claim_num` varchar(50) null default '',
  `start_date` date null,
  `end_date` date null,
  `assigned` bigint null default 0,
  `crossover_claim` bigint null default 0,
  `deductible_met` bigint null default 0,
  `annual_deductible` bigint null default 0,
  `co_payment` bigint null default 0,
  `treatment_auth` varchar(50) null default '',
  `doc_ctrl_num` varchar(50) null default '',
  `ins_class_a` bigint null default 0,
  `ins_class_b` bigint null default 0,
  `ins_class_c` bigint null default 0,
  `ins_class_d` bigint null default 0,
  `ins_class_e` bigint null default 0,
  `ins_class_f` bigint null default 0,
  `ins_class_g` bigint null default 0,
  `ins_class_h` bigint null default 0,
  `updated_at` datetime,
  `created_at` datetime,
    primary key(id)  
) engine=innodb;

create table `tb_guarantor` (
  `id` bigint not null auto_increment,
  `name` varchar(100) null default '',
  `dob` date null,
  `street` text null,
  `city` varchar(50) null default '',
  `state` varchar(50) null default '',
  `zip` varchar(20) null default '',
  `phone_home` varchar(15) null default '',
  `relationship` varchar(100) null default '',
  `patient_id` bigint null default 0,
  `updated_at` datetime,
  `created_at` datetime,
    primary key(id)  
) engine=innodb;

create table `tb_deposits` (
  `id` bigint not null auto_increment,
  `date` date null,
  `cheque_num` varchar(50) null default '',
  `cheque_date` date null,
  `payor_id` bigint null default 0,
  `patient_id` bigint null default 0,
  `payor_type` varchar(10) null default '',
  `amount` decimal(10,2) null default 0,
  `payment_type` varchar(10) null default '',
  `notes` text null,
  `created_by` bigint null default 0,
  `practice_id` bigint null default 0,
  `is_deleted` int(1) null default 0,
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;

create table `tb_deposit_details` (
  `id` bigint not null auto_increment,
  `deposit_id` bigint null default 0,
  `claim_id` bigint null default 0,
  `patient_id` bigint null default 0,
  `amount_adjusted` decimal(10,2) null default 0,
  `adjustment` decimal(10,2) null default 0,
  `claim_balance_ins` decimal(10,2) null default 0,
  `claim_balance_pat` decimal(10,2) null default 0,
  `is_deleted` int(1) null default 0,
  `created_by` bigint null default 0,
  `adjustment_details` varchar(100) null default '',
  `adjustment_notes` varchar(50) null default '',
  `adjustment_code` varchar(50) null default '',
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;
  

create table `tb_physicians` (
  `id` bigint not null auto_increment,
  `name` varchar(50) null default '',
  `phone_home` varchar(20) null default '',
  `phone_work` varchar(20) null default '',
  `phone_mobile` varchar(20) null default '',
  `phy_email` varchar(100) null default '',
  `dob` date null,
  `individual_npi` varchar(50) null default '',
  `ssno` varchar(50) null default '',
  `speciality` varchar(50) null default '',
  `address` text null,
  `fax` varchar(20) null default '',
  `notes` text null,
  `created_by` bigint null default 0,
  `practice_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;

drop table if exists `tb_practices`;

create table `tb_practices` (
  `id` bigint not null auto_increment,
  `name` text null,
  `speciality` varchar(100) null default '',
  `dob` varchar(20) null default '',
  `ssn` varchar(20) null default '',
  `ein` varchar(15) null default '',
  `address` text null,
  `phone_home` varchar(20) null default '',
  `phone_work` varchar(20) null default '',
  `phone_mobile` varchar(20) null default '',
  `dept` text null,
  `fax` varchar(20) null default '',
  `npi` varchar(20) null default '',
  `notes` text null,
  `is_active` int(1) null default 1,
  `image_name` varchar(124) null default '',
  `image_name_saved` varchar(124) null default '',
  `updated_at` datetime,
  `created_at` datetime,
  primary key(id)
) engine=innodb;

create table `tb_patients` (
  `id` bigint not null auto_increment,
  `full_name` varchar(50) null default '',
  `dob` date null,
  `gender` varchar(10) null default '',
  `relationship` varchar(10) null default '',
  `ssn` varchar(20) null default '',
  `martial_status` varchar(20) null default '',
  `medical_record` varchar(20) null default '',
  `employment` varchar(20) null default '',
  `employer` varchar(50) null default '',
  `reference` varchar(50) null default '',
  `is_active` int(1) null default 0,
  `is_deleted` int(1) null default 0,
  `addr_street1` varchar(50) null default '',
  `addr_street2` varchar(50) null default '',
  `addr_city` varchar(20) null default '',
  `addr_state` varchar(20) null default '',
  `addr_country` varchar(20) null default '',
  `addr_zip` varchar(10) null default '',
  `email` varchar(50) null default '',
  `phone_home` varchar(50) null default '',
  `phone_work` varchar(50) null default '',
  `phone_mobile` varchar(50) null default '',
  `notifyemail` int(1) null default 0,
  `notify_phone` int(1) null default 0,
  `is_financial_resp` int(1) null default 0,
  `primary_care_phy` bigint null default 0,
  `referring_phy` bigint null default 0,
  `default_render_phy` bigint null default 0,
  `default_service_loc` bigint null default 0,
  `chart_num` varchar(20) null default '',
  `emergency_contact` varchar(50) null default '',
  `emergency_phone` varchar(50) null default '',
  `created_by` bigint null default 0,
  `last_edited_by` bigint null default 0,
  `conduct` varchar(10) null default '',
  `practice_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
   primary key(id)  
) engine=innodb;

drop table if exists tb_testing_pagination;
create table `tb_testing_pagination` (
  `id` bigint not null auto_increment,
  `first_name` varchar(50) null default '',
  `last_name` varchar(50) null default '',
  `created_at` datetime,
  `updated_at` datetime,
   primary key(id)  
) engine=innodb;

create table `tb_crm` (
  `id` bigint not null auto_increment,
  `claim_id` bigint null default 0,
  `comments` text null,
  `dispo` varchar(20) null default '',
  `followup_date` date null,
  `worked_date` date null,
  `tyre_id` bigint null default 0,
  `user_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
   primary key(id)  
) engine=innodb;

create table `tb_relation_ships` (
  `id` bigint not null auto_increment,
  `full_name` varchar(50) null default '',
  `dob` date null,
  `gender` varchar(10) null default '',
  `relationship` varchar(10) null default '',
  `ssn` varchar(20) null  default '',
  `martial_status` varchar(20) null  default '',
  `medical_record` varchar(20) null  default '',
  `employment` varchar(20) not null,
  `employer` varchar(50) not null,
  `reference` varchar(50) not null,
  `is_active` int(1) null default 1,
  `is_deleted` int(1) null default 0,
  `addr_street1` varchar(50) null default '',
  `addr_street2` varchar(50) null default '',
  `addr_city` varchar(20) null default '',
  `addr_state` varchar(20) null default '',
  `addr_country` varchar(20) null default '',
  `addr_zip` varchar(10) null default '',
  `email` varchar(125) null default '',
  `phone_home` varchar(50) null default '',
  `phone_work` varchar(50) null default '',
  `phone_mobile` varchar(50) null default '',
  `notify_email` int(1) null default 0,
  `notify_phone` int(11) null default 0,
  `primary_care_phy` bigint null default 0,
  `referring_phy` bigint null default 0,
  `default_render_phy` bigint null default 0,
  `default_service_loc` varchar(20) null default '',
  `chart_num` varchar(20) null default '',
  `emergency_contact` varchar(50) null default '',
  `emergency_phone` varchar(50) null default '',
  `created_by` bigint null default 0,
  `last_edited_by` bigint null default 0,
  `conduct` varchar(10) null default '',
  `referral_source` varchar(50) null default '',
  `practice_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
   primary key(id)  
) engine=innodb;

drop table if exists `tb_portal_logs`;

create table `tb_portal_logs` (
  `id` bigint not null auto_increment,
  `user_name` varchar(125) null default '',
  `user_id` bigint null default 0,
  `ip` varchar(20) null default '',
  `date` datetime null,
  `file` text null,
  `function` text null,
  `action` text null,
  `comments` text null,
  `session_token` varchar(40) null default '',
  `created_at` datetime,
  `updated_at` datetime,
   primary key(id)  
) engine=innodb;


/*truncate table tb_acl_list;*/

insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('Clients', 'ACL_CLIENTS', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('Employees', 'ACL_EMPLOYEES', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('Scheduler', 'ACL_SCHEDULER', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('EDI', 'ACL_EDI', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('Statements', 'ACL_STATEMENTS', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('Ledger', 'ACL_LEDGER', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('Deposits', 'ACL_DEPOSITS', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('CRM', 'ACL_CRM', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('Reports', 'ACL_REPORTS', now(), now());
insert into tb_acl_list (acl_name, acl_key, created_at, updated_at)
 values('Settings', 'ACL_SETTINGS', now(), now());

 
 create table `oauth_access_tokens` (
  `id` varchar(100) not null,
  `user_id` bigint default null,
  `client_id` bigint not null,
  `name` varchar(255) default null,
  `scopes` text,
  `revoked` tinyint(1) not null,
  `created_at` timestamp null ,
  `updated_at` timestamp null ,
  `expires_at` datetime default null
) engine=innodb; 

create table `oauth_auth_codes` (
  `id` varchar(100) not null,
  `user_id` bigint not null,
  `client_id` bigint not null,
  `scopes` text,
  `revoked` tinyint(1) not null,
  `expires_at` datetime default null
) engine=innodb;


create table `oauth_clients` (
  `id` int(10) unsigned not null,
  `user_id` bigint default null,
  `name` varchar(255) not null,
  `secret` varchar(100) not null,
  `redirect` text null,
  `personal_access_client` tinyint(1) not null,
  `password_client` tinyint(1) not null,
  `revoked` tinyint(1) not null,
  `created_at` datetime,
  `updated_at` timestamp null
) engine=innodb;

INSERT INTO `oauth_clients` VALUES(1, NULL, 'Laravel Personal Access Client', '7X61XonRmaLjqaue2Y8O6hKLOHEmeuhVygqS6WHk', 'http://localhost', 1, 0, 0, '2018-08-04 11:58:00', '2018-08-04 11:58:00');
INSERT INTO `oauth_clients` VALUES(2, NULL, 'Laravel Password Grant Client', '3PF0nBejsgERULceggQZQuvQQEHCpLA1Gn2vqxTt', 'http://localhost', 0, 1, 0, '2018-08-04 11:58:00', '2018-08-04 11:58:00');


create table `oauth_personal_access_clients` (
  `id` int(10) unsigned not null,
  `client_id` bigint not null,
  `created_at` datetime,
  `updated_at` timestamp null
) engine=innodb;

truncate table `oauth_personal_access_clients`;
insert into `oauth_personal_access_clients` values(1, 1, '2018-08-04 11:58:00', '2018-08-04 11:58:00');


create table `oauth_refresh_tokens` (
  `id` varchar(100) not null,
  `access_token_id` varchar(100) not null,
  `revoked` tinyint(1) not null,
  `expires_at` datetime default null
) engine=innodb;


create table `password_resets` (
  `email` varchar(255) not null,
  `token` varchar(255) not null,
  `created_at` timestamp null
) engine=innodb;

truncate table `password_resets`;

create table `users` (
    `id` bigint not null auto_increment,
    `name` varchar(255) null default '',
    `email` varchar(255) null default '',
    `password` varchar(255) null default '',
    `remember_token` varchar(100) null default '',
    `created_at` datetime,
    `updated_at` datetime,
    primary key(id),
    unique key `users_email_unique` (`email`)  
) engine=innodb;

create table `tb_practices_insurance` (
`id` bigint not null auto_increment,
`practice_id` bigint null default 0,
`insurance_id` bigint null default 0,
`updated_at` timestamp null,
`created_at` timestamp null,
primary key(id)
) engine=innodb;



/*
truncate table `users`;

insert into `users` values(2, 'rish', 'rish@avr.com', '$2y$10$1wn0cw0q0.wxhckrpoelr.tko9hxdhsl5.zrs1xgcaq6/yep4sxpe', null, '2018-08-04 13:56:50', '2018-08-04 13:56:50');
insert into `users` values(3, 'a', 'rishwanth@curismed.com', '$2y$10$mexrcbyhi0vatxngzgov2ucf9cy84vrguqzvrxopx1i4scor4fiig', null, '2018-08-04 16:58:21', '2018-08-04 16:58:21');
insert into `users` values(4, 'b', 'anand@curismed.com', '$2y$10$pfbnmarkystiri/ezqwe6ekfg3f2lrt9y6xsqsa9g2iegiyaftzgq', null, '2018-08-04 16:58:46', '2018-08-04 16:58:46');

insert into `users` values(1, 'a', 'rishwanth@curismed.com', '$2y$10$mexrcbyhi0vatxngzgov2ucf9cy84vrguqzvrxopx1i4scor4fiig', null, '2018-08-04 16:58:21', '2018-08-04 16:58:21');
insert into `users` values(2, 'b', 'anand@curismed.com', '$2y$10$pfbnmarkystiri/ezqwe6ekfg3f2lrt9y6xsqsa9g2iegiyaftzgq', null, '2018-08-04 16:58:46', '2018-08-04 16:58:46');

*/

alter table `oauth_access_tokens`
  add primary key (`id`),
  add key `oauth_access_tokens_user_id_index` (`user_id`);

alter table `oauth_auth_codes`
  add primary key (`id`);

alter table `oauth_clients`
  add primary key (`id`),
  add key `oauth_clients_user_id_index` (`user_id`);

alter table `oauth_personal_access_clients`
  add primary key (`id`),
  add key `oauth_personal_access_clients_client_id_index` (`client_id`);

alter table `oauth_refresh_tokens`
  add primary key (`id`),
  add key `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

alter table `password_resets`
  add key `password_resets_email_index` (`email`);

/*  
alter table `users`
  add primary key (`id`),
  add unique key `users_email_unique` (`email`);
*/

/*  
truncate table tb_users;
truncate table users;

insert into `tb_users` (`id`, `first_name`, `last_name`, `username`, `role`, `password`, `email_id`, `phone_num`, `is_logged`, `created_at`, `updated_at`, `token`, `is_activated`) values
(1, 'rishwanth', 'avr', 'a', 'admin', '76af7efae0d034d1e3335ed1b90f24b6cadf2bf1', 'rishwanth@curismed.com', '1234567890', 0, '2018-08-05 08:36:53', '2018-08-05 08:36:53', 'a4b5f2ff-9a56-11e8-ae94-844bf58051a3', 0)

insert into `tb_users` (`id`, `first_name`, `last_name`, `username`, `role`, `password`, `email_id`, `phone_num`, `is_logged`, `created_at`, `updated_at`, `token`, `is_activated`) values
(2, 'anand', 'anand', 'b', 'admin', '76af7efae0d034d1e3335ed1b90f24b6cadf2bf1', 'anand@curismed.com', '1234567890', 0, '2018-08-05 08:37:49', '2018-08-05 08:37:49', 'a4b5f2ff-9a56-11e8-ae94-844bf58051a3a', 0);


truncate table `users`;

insert into `users` values(1, 'a', 'rishwanth@curismed.com', '$2y$10$mexrcbyhi0vatxngzgov2ucf9cy84vrguqzvrxopx1i4scor4fiig', null, '2018-08-04 16:58:21', '2018-08-04 16:58:21');
insert into `users` values(2, 'b', 'anand@curismed.com', '$2y$10$pfbnmarkystiri/ezqwe6ekfg3f2lrt9y6xsqsa9g2iegiyaftzgq', null, '2018-08-04 16:58:46', '2018-08-04 16:58:46');


insert into tb_testing_pagination(first_name, last_name) SELECT first_name, last_name FROM `tb_testing_pagination` ;

*/

/*

truncate table tb_users;
truncate table users;


*/

/*
INSERT INTO `tb_patients` (`id`, `full_name`, `dob`, `gender`, `relationship`, `ssn`, `martial_status`, `medical_record`, `employment`, `employer`, `reference`, `is_active`, `is_deleted`, `addr_street1`, `addr_street2`, `addr_city`, `addr_state`, `addr_country`, `addr_zip`, `email`, `phone_home`, `phone_work`, `phone_mobile`, `notifyemail`, `notify_phone`, `is_financial_resp`, `primary_care_phy`, `referring_phy`, `default_render_phy`, `default_service_loc`, `chart_num`, `emergency_contact`, `emergency_phone`, `created_by`, `last_edited_by`, `conduct`, `practice_id`, `created_at`, `updated_at`) VALUES ('1', 'AAA', '2018-08-01', 'M', 'SSS', 'SSS', 'M', 'YES', 'aaaa', '', '', '1', '0', '', '', '', '', '', '', '', '', '', '', '1', '1', '1', '1', '1', '1', '', '', '', '', '1', '1', '', '1', '2018-07-18 11:45:54', '2018-08-05 14:07:49');

*/

INSERT INTO `tb_zipcodes` (`id`, `state`, `city`, `zip`, `created_at`, `updated_at`) VALUES ('1', 'MA', 'Mussourie', '06851', NULL, NULL);

INSERT INTO `tb_details` (`id`, `detail`, `detailvalue1`, `detailvalue2`, `created_at`, `updated_at`) VALUES ('1', 'ChartNumber', '1', '', NULL, NULL);
INSERT INTO `tb_details` (`id`, `detail`, `detailvalue1`, `detailvalue2`, `created_at`, `updated_at`) VALUES (2, 'ClaimNumber', '1', '', NULL, NULL);
INSERT INTO `tb_details` (`id`, `detail`, `detailvalue1`, `detailvalue2`, `created_at`, `updated_at`) VALUES (3, 'BatchNumber', '1', '', NULL, NULL);

INSERT INTO `tb_insurances` (`id`, `payer_name`, `enr`, `typ`, `st`, `lob`, `rte`, `rts`, `era`, `sec`, `note`, `clearing_house_payor_id`, `addr`, `city`, `state`, `zip`, `phone`, `fax`, `abforbilling`, `practice_id`, `is_active`, `created_by`, `created_at`, `updated_at`) VALUES (NULL, 'AETNA', '0', '0', '0', '0', '0', '0', '0', '0', '', '00240', '123, Valley Ave', 'Vienna', 'VA', '06851', '', '', '', '0', '1', '0', NULL, NULL);

INSERT INTO `tb_physicians` (`id`, `name`, `phone_home`, `phone_work`, `phone_mobile`, `phy_email`, `dob`, `individual_npi`, `ssno`, `speciality`, `address`, `fax`, `notes`, `created_by`, `practice_id`, `created_at`, `updated_at`) VALUES (NULL, 'Sriram', '', '', '', '', NULL, '4564613218', '', '', '123 Valley Ave,\r\nVienna MO 06851', '', NULL, '0', '1', NULL, NULL);

INSERT INTO `tb_practices` (`id`, `name`, `speciality`, `dob`, `ssn`, `ein`, `address`, `phone_home`, `phone_work`, `phone_mobile`, `dept`, `fax`, `npi`, `notes`, `is_active`, `updated_at`, `created_at`) VALUES (NULL, 'Forming Friendships LLC', 'ABA', '2018-08-01', '84951654', '54651219', NULL, '', '', '', NULL, '', '115599589', NULL, '1', NULL, NULL);

INSERT INTO `tb_service_locations` (`id`, `internal_name`, `npi`, `override_ein`, `time_zone`, `legacy_num_type`, `legacy_num`, `billing_name`, `address`, `city`, `state`, `zip`, `phone`, `fax`, `place_of_service`, `clia_num`, `type_of_bill`, `pay_to_name`, `inst_address`, `inst_phone`, `inst_fax`, `is_active`, `practice_id`, `created_by`, `updated_at`, `created_at`) VALUES
(3, 'Forming Friendships', '414551250', 0, '', '', '', 'Forming Friendships LLC', '123, Valley', 'Vienna', 'MO', '06851', '', '', '11', '', '', '', NULL, '', '', 1, 1, 0, NULL, NULL);

ALTER TABLE `tb_users` ADD `practice_id` INT(3) NOT NULL AFTER `oauth_token`;

drop table if exists `tb_physicians`;

create table `tb_physicians` (
`id` bigint not null auto_increment,
`name` varchar(50) null default '',
`phone_home` varchar(20) null default '',
`phone_work` varchar(20) null default '',
`phone_mobile` varchar(20) null default '',
`phy_email` varchar(100) null default '',
`dob` date null,
`individual_npi` varchar(50) null default '',
`ssno` varchar(50) null default '',
`speciality` varchar(50) null default '',
`address` text null,
`fax` varchar(20) null default '',
`notes` text null,
`created_by` bigint null default 0,
`practice_id` bigint null default 0,
`employee_position` varchar(32) null default '',
`employee_type` varchar(15) null default 'Full Time',
`date_of_hire` datetime null,
`has_benifits` varchar(3) null default 'No',
`payroll_company` varchar(128) null default '',
`payroll_employee_num` varchar(16) null default '',
`workers_comp_num` varchar(16) null default '',
`department_id` bigint null default 0,
`department_name` varchar(64) null default '',
`pay_code` varchar(16) null default '',
`address_1` text null,
`address_2` text null,
`city` varchar(32) null default '',
`region` varchar(32) null default '',
`country_id` bigint null default 0,
`country_name` varchar(32) null default '',
`zip` varchar(8) null default '',
`driver_license_number` varchar(32) null default '',
`driver_license_expiration` datetime null,
`auto_insurance_number` varchar(32) null default '',
`auto_insurance_expiration` datetime null,
`educational_credential` varchar(32) null default '',
`tb_clearance_expiration` datetime null,
`medical_clearance_expiration` datetime null,
`background_check_completion` varchar(32) null default '',
`background_check_expiration` datetime null,
`job_title` varchar(64) null default '',
`created_at` datetime,
`updated_at` datetime,
primary key(id)
) engine=innodb;

drop table if exists `tb_physicians_paycodes`;

create table `tb_physicians_paycodes` (
`id` bigint not null auto_increment,
`physician_id` bigint null default 0,
`paycode_id` bigint null default 0,
`billable` varchar(3) null default 'Yes',
`mileage` float(12,2) null default 0.0,
`rate` float(12,2) null default 0.0,
`created_at` datetime,
`updated_at` datetime,
primary key(id)
) engine=innodb;



drop table if exists `tb_physicians_workhistory`;

create table `tb_physicians_workhistory` (
`id` bigint not null auto_increment,
`physician_id` bigint null default 0,
`start_date` datetime null,
`end_date` datetime null,
`notes` text null,
`created_at` datetime,
`updated_at` datetime,
primary key(id)
) engine=innodb;


drop table if exists `tb_paycodes`;

create table `tb_paycodes` (
`id` bigint not null auto_increment,
`name` varchar(128) null default '',
`rate` float(15,2) null default 0.0,
`created_at` datetime,
`updated_at` datetime,
primary key(id)
) engine=innodb;

truncate tb_paycodes;

insert into tb_paycodes (id, name, created_at, updated_at) values(-1, '--Add New Pay Code--', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Assistant Behavioral Staff Trainer', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Training', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Material Development Coordinator', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Business and Technology intem', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Behavioral Staff Trainer', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Behavior Technician', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Behavior Consultant', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Behavior Analyst', now(), now());
insert into tb_paycodes (name, created_at, updated_at) values('Administrative Coordinator', now(), now());

  ALTER TABLE `tb_appointments` ADD `appointment_type` VARCHAR(20) NOT NULL AFTER `id`;
/* For Payroll table records insertion, this flag has been created */
  ALTER TABLE `tb_claims` ADD `is_payroll` INT(2) NOT NULL DEFAULT '0' AFTER `is_sent`;

  create table `tb_insurance_setup` (
`id` bigint not null auto_increment,
`insurance_id` bigint not null,
`practice_id` bigint not null,
`edi_type` varchar(64) null default '',
`is_day_club` int(1) null default 0,
`is_capitated` int(1) null default 0,
`cms1500_31` varchar(64) null default '',
`cms1500_32a` varchar(64) null default '',
`cms1500_32b` varchar(64) null default '',
`cms1500_33a` varchar(64) null default '',
`cms1500_33b` varchar(64) null default '',
`is_active` int(1) null default 1,
`created_at` timestamp null,
`updated_at` timestamp null,
primary key(id)
) engine=innodb;

  create table `tb_zone` (
`id` bigint not null auto_increment,
`name` varchar(64) null default '',
`practice_id` bigint not null default 0,
`is_active` int(1) null default 1,
`created_at` timestamp null,
`updated_at` timestamp null,
primary key(id)
) engine=innodb;

  create table `tb_holiday` (
`id` bigint not null auto_increment,
`practice_id` bigint not null default 0,
`date` date not null,
`description` varchar(124) null default '',
`is_active` int(1) null default 1,
`created_at` timestamp null,
`updated_at` timestamp null,
primary key(id)
) engine=innodb;

  create table `tb_practice_paycodes` (
`id` bigint not null auto_increment,
`practice_id` bigint not null default 0,
`paycode_id` bigint not null default 0,
`is_active` int(1) null default 1,
`created_at` timestamp null,
`updated_at` timestamp null,
primary key(id)
) engine=innodb;

  create table `tb_lookup` (
  `id` bigint not null auto_increment,
  `parent_key` text null,
  `key` text null,
  `name` text null,
  `type` varchar(64) not null default '',
  `is_active` int(1) null default 1,
  `updated_at` timestamp null,
  `created_at` timestamp null,
  primary key(id)
) engine=innodb;

insert into tb_lookup values (null, '', 'ECB', 'Electronic - Claim based', 'EDI_TYPE', 1, now(), now());
insert into tb_lookup values (null, '', 'OASFTP', 'Office ally SFTP', 'EDI_TYPE', 1, now(), now());
insert into tb_lookup values (null, '', 'NCB', 'Non Claim based', 'EDI_TYPE', 1, now(), now());
insert into tb_lookup values (null, '', 'STA', 'Send to Amvik', 'EDI_TYPE', 1, now(), now());

drop table if exists `tb_handler_insurances`;

create table `tb_handler_insurances` (
  `id` bigint not null auto_increment,
  `payer_name` varchar(50) null default '',
  `enr` bigint null default 0,
  `typ` varchar(50) null default '',
  `st` varchar(50) null default '',
  `lob` varchar(50) null default '',
  `rte` bigint null default 0,
  `rts` bigint null default 0,
  `era` bigint null default 0,
  `sec` bigint null default 0,
  `note` text null,
  `clearing_house_payor_id` varchar(50) null default '',
  `addr` text null,
  `city` varchar(30) null default '',
  `state` varchar(10) null default '',
  `zip` varchar(20) null default '',
  `phone` varchar(20) null default '',
  `fax` varchar(20) null default '',
  `abforbilling` varchar(20) null default '',
  `practice_id` bigint null default 0,
  `is_active` int(1) null default 1,
    `is_deleted` int(1) null default 0,
  `created_by_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;


drop table if exists `tb_handler_activity`;

create table `tb_handler_activity` (
  `id` bigint not null auto_increment,
  `name` varchar(128) null default '',
    `is_active` int(1) null default 1,
    `is_deleted` int(1) null default 0,
  `created_by_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
    primary key(id)  
) engine=innodb;


drop table if exists `tb_handler_session_rule`;

create table `tb_handler_session_rule` (
`id` bigint not null auto_increment,
`name` varchar(128) null default '',
`description` text null,
`practice_id` bigint not null default 0,
`is_run_rule` int(1) null default 1,
`is_prevent_session_creation` int(1) null default 1,
`is_active` int(1) null default 1,
`is_deleted` int(1) null default 0,
`created_by_id` bigint null default 0,
`created_at` timestamp null,
`updated_at` timestamp null,
primary key(id)
) engine=innodb;

drop table if exists `tb_handler_users`;

create table `tb_handler_users` (
  `id` bigint not null auto_increment,
  `first_name` varchar(64) not null,
  `last_name` varchar(64) not null,
  `username` varchar(64) not null,
  `role` varchar(128) not null,
  `password` varchar(64) not null,
  `email_id` varchar(128) not null,
  `phone_num` varchar(16) null default '',
  `is_activated` int(1) null default 0,
  `is_logged` int(1) null default 0,
  `token` varchar(128) null default '',
  `reset_token` varchar(128) null default '',
  `oauth_token` text,
    `is_active` int(1) null default 1,
`is_deleted` int(1) null default 0,
`created_by_id` bigint null default 0,
  `created_at` datetime,
  `updated_at` datetime,
  primary key(id)
) engine=innodb;