-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 31, 2018 at 12:57 PM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `curismed_aba1`
--

-- --------------------------------------------------------

--
-- Table structure for table `disposition`
--

CREATE TABLE `disposition` (
  `dispoID` int(11) NOT NULL,
  `DispoName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `historyID` int(11) NOT NULL,
  `claimID` int(10) NOT NULL,
  `dispo` int(10) NOT NULL,
  `comments` varchar(10000) NOT NULL,
  `workedDate` datetime NOT NULL,
  `user` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mapdispo`
--

CREATE TABLE `mapdispo` (
  `mapID` int(11) NOT NULL,
  `dispoID` int(10) NOT NULL,
  `tyreID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_activity`
--

CREATE TABLE `m_activity` (
  `activityID` int(11) NOT NULL,
  `caseID` int(10) NOT NULL,
  `authNo` varchar(20) NOT NULL,
  `activityName` varchar(50) NOT NULL,
  `activityCPT` varchar(20) NOT NULL,
  `activityMod1` varchar(10) NOT NULL,
  `activityMod2` varchar(10) NOT NULL,
  `activityMod3` varchar(10) NOT NULL,
  `activityMod4` varchar(10) NOT NULL,
  `activityBilledPer` varchar(10) NOT NULL,
  `activityBilledPerTime` varchar(10) NOT NULL,
  `totalAuth` int(10) NOT NULL,
  `authType` varchar(50) NOT NULL,
  `remainAuth` int(10) NOT NULL,
  `activityRates` decimal(10,2) NOT NULL,
  `activityMax1` varchar(10) NOT NULL,
  `activityMax2` varchar(10) NOT NULL,
  `activityMax3` varchar(10) NOT NULL,
  `activityPer1` varchar(10) NOT NULL,
  `activityPer2` varchar(10) NOT NULL,
  `activityPer3` varchar(10) NOT NULL,
  `activityIs1` varchar(10) NOT NULL,
  `activityIs2` varchar(10) NOT NULL,
  `activityIs3` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_alerts`
--

CREATE TABLE `m_alerts` (
  `alert_ID` int(11) NOT NULL,
  `alert_desc` varchar(1000) NOT NULL,
  `alert_patientID` int(10) NOT NULL,
  `alerts_EditPatient` int(1) NOT NULL,
  `alerts_ScheduleApp` int(1) NOT NULL,
  `alerts_Charge` int(1) NOT NULL,
  `alerts_ViewClaims` int(1) NOT NULL,
  `alerts_Deposits` int(1) NOT NULL,
  `alerts_Statements` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_appointments`
--

CREATE TABLE `m_appointments` (
  `appID` int(11) NOT NULL,
  `appStartDt` datetime NOT NULL,
  `appEndDt` datetime NOT NULL,
  `appPatientName` varchar(50) NOT NULL,
  `appAuth` varchar(20) NOT NULL,
  `appActivity` varchar(50) NOT NULL,
  `appLocation` varchar(20) NOT NULL,
  `appProvider` varchar(50) NOT NULL,
  `appDesc` varchar(200) NOT NULL,
  `appStatus` varchar(20) NOT NULL,
  `appPatientID` varchar(10) NOT NULL,
  `appProviderID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_auth`
--

CREATE TABLE `m_auth` (
  `authID` int(11) NOT NULL,
  `patientID` int(10) NOT NULL,
  `caseID` int(10) NOT NULL,
  `policyEntity` int(5) NOT NULL,
  `tos` varchar(50) NOT NULL,
  `authNo` varchar(100) NOT NULL,
  `startDt` date NOT NULL,
  `endDt` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_billingCode`
--

CREATE TABLE `m_billingCode` (
  `billingCdID` int(11) NOT NULL,
  `billingCd` varchar(10) NOT NULL,
  `billingCdDesc` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_cases`
--

CREATE TABLE `m_cases` (
  `caseID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `isClosed` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `allowPrint` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `caseDt` datetime NOT NULL,
  `caseChartNo` varchar(20) NOT NULL,
  `globalCoverageUntil` varchar(50) NOT NULL,
  `employer` varchar(50) NOT NULL,
  `empstatus` varchar(50) NOT NULL,
  `retirementDate` date NOT NULL,
  `location` varchar(50) NOT NULL,
  `workPhone` varchar(50) NOT NULL,
  `extension` varchar(50) NOT NULL,
  `studStatus` varchar(50) NOT NULL,
  `guarantor` varchar(50) NOT NULL,
  `maritalStatus` varchar(50) NOT NULL,
  `assignedProvider` int(11) NOT NULL,
  `referringProvider` int(11) NOT NULL,
  `supervisingProvider` int(11) NOT NULL,
  `operatingProvider` int(11) NOT NULL,
  `otherProvider` int(11) NOT NULL,
  `referralSource` varchar(50) NOT NULL,
  `attorney` varchar(50) NOT NULL,
  `facility` varchar(50) NOT NULL,
  `caseBillingCode` varchar(50) NOT NULL,
  `priceCode` varchar(50) NOT NULL,
  `otherArrangements` varchar(50) NOT NULL,
  `authorizedThrough` varchar(50) NOT NULL,
  `authNo` varchar(50) NOT NULL,
  `lastVisit` datetime NOT NULL,
  `noOfVisits` int(11) NOT NULL,
  `totalVisits` int(10) NOT NULL,
  `authID` varchar(50) NOT NULL,
  `lastVisitNo` varchar(50) NOT NULL,
  `principalDiag` varchar(100) NOT NULL,
  `defDiag2` varchar(100) NOT NULL,
  `defDiag3` varchar(100) NOT NULL,
  `defDiag4` varchar(100) NOT NULL,
  `poa1` varchar(100) NOT NULL,
  `poa2` varchar(100) NOT NULL,
  `poa3` varchar(100) NOT NULL,
  `poa4` varchar(100) NOT NULL,
  `allergiesNotes` varchar(1000) NOT NULL,
  `ediNotes` varchar(1000) NOT NULL,
  `repTypeCode` varchar(50) NOT NULL,
  `attachCtrlNo` varchar(50) NOT NULL,
  `repTransCode` varchar(50) NOT NULL,
  `injuryDt` date NOT NULL,
  `illnessIndicator` varchar(50) NOT NULL,
  `firstConsultDate` date NOT NULL,
  `similarSymptoms` varchar(100) NOT NULL,
  `sameSymptom` int(11) NOT NULL,
  `empRelated` int(11) NOT NULL,
  `emergency` int(11) NOT NULL,
  `relatedTo` varchar(50) NOT NULL,
  `relatedState` varchar(50) NOT NULL,
  `nature` varchar(50) NOT NULL,
  `lastXRay` date NOT NULL,
  `deathStat` varchar(50) NOT NULL,
  `unableToWorkFrm` date NOT NULL,
  `unableToWorkTo` date NOT NULL,
  `totalDisabFrm` date NOT NULL,
  `totDisabTo` date NOT NULL,
  `partDisabFrm` date NOT NULL,
  `partDisabTo` date NOT NULL,
  `hospFrm` date NOT NULL,
  `hospTo` date NOT NULL,
  `returnWorkIndicator` varchar(50) NOT NULL,
  `disabilityPercent` varchar(50) NOT NULL,
  `lastWorkDate` date NOT NULL,
  `pregnant` int(11) NOT NULL,
  `estimatedDOB` date NOT NULL,
  `dateAssumedCare` date NOT NULL,
  `dateRelinquishedCare` date NOT NULL,
  `outsideLab` int(11) NOT NULL,
  `labCharges` int(11) NOT NULL,
  `localUseA` varchar(20) NOT NULL,
  `localUseB` varchar(20) NOT NULL,
  `indicator` varchar(50) NOT NULL,
  `refDate` date NOT NULL,
  `prescripDate` date NOT NULL,
  `priorAuthNo` varchar(50) NOT NULL,
  `extra1` varchar(50) NOT NULL,
  `extra2` varchar(50) NOT NULL,
  `extra3` varchar(50) NOT NULL,
  `extra4` varchar(50) NOT NULL,
  `outsideProvider` varchar(50) NOT NULL,
  `dateLastSeen` date NOT NULL,
  `epsdt` int(11) NOT NULL,
  `resubmissionDt` date NOT NULL,
  `familyPlanning` int(11) NOT NULL,
  `originalRefNo` varchar(50) NOT NULL,
  `serviceAuth` varchar(50) NOT NULL,
  `nonAvailIndicator` varchar(50) NOT NULL,
  `branchService` varchar(50) NOT NULL,
  `sponsorStat` varchar(50) NOT NULL,
  `splProgram` varchar(50) NOT NULL,
  `sponsorGuide` varchar(50) NOT NULL,
  `effStartDt` date NOT NULL,
  `effEndDt` date NOT NULL,
  `carePlanOversight` varchar(50) NOT NULL,
  `hospiceNo` varchar(50) NOT NULL,
  `CLIANo` varchar(50) NOT NULL,
  `mamoCertification` varchar(50) NOT NULL,
  `refAccessNo` varchar(50) NOT NULL,
  `demoCode` varchar(50) NOT NULL,
  `assignIndicator` varchar(50) NOT NULL,
  `insuranceTypeCode` varchar(50) NOT NULL,
  `timelyFillingIndicator` varchar(50) NOT NULL,
  `EPSDTRefCode1` varchar(50) NOT NULL,
  `EPSDTRefCode2` varchar(50) NOT NULL,
  `EPSDTRefCode3` varchar(50) NOT NULL,
  `homebound` int(11) NOT NULL,
  `IDENo` varchar(50) NOT NULL,
  `conditionIndicator` varchar(50) NOT NULL,
  `certCodeApplies` int(11) NOT NULL,
  `codeCategory` varchar(50) NOT NULL,
  `totalVisitRendered` varchar(50) NOT NULL,
  `totalVisitProjected` varchar(50) NOT NULL,
  `noOfVisitsHomeHealth` int(11) NOT NULL,
  `noOfUnits` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `disciplineTypeCode` varchar(50) NOT NULL,
  `deliveryPatternCode` varchar(50) NOT NULL,
  `deliveryTimeCode` varchar(50) NOT NULL,
  `freqPeriod` int(11) NOT NULL,
  `freqCount` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `isCash` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_claims`
--

CREATE TABLE `m_claims` (
  `claimID` int(11) NOT NULL,
  `caseID` int(11) NOT NULL,
  `appID` int(11) NOT NULL,
  `authNo` varchar(40) NOT NULL,
  `fromDt` date NOT NULL,
  `toDt` date NOT NULL,
  `proced` varchar(50) NOT NULL,
  `mod1` varchar(50) NOT NULL,
  `units` int(11) NOT NULL,
  `charge` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `diag1` varchar(50) NOT NULL,
  `diag2` varchar(50) NOT NULL,
  `diag3` varchar(50) NOT NULL,
  `diag4` varchar(50) NOT NULL,
  `applied` int(11) DEFAULT NULL,
  `notes` varchar(200) DEFAULT NULL,
  `refCode` varchar(15) DEFAULT NULL,
  `placeOfService` varchar(50) NOT NULL,
  `isPosted` int(11) NOT NULL,
  `isSent` int(11) NOT NULL,
  `isSecClaim` int(2) NOT NULL DEFAULT '0',
  `allowed` decimal(10,2) NOT NULL,
  `paid` decimal(10,2) NOT NULL,
  `adjustment` decimal(10,2) NOT NULL,
  `adjustCode` varchar(100) DEFAULT NULL,
  `claimBalance` decimal(10,2) NOT NULL,
  `claimBalancePat` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reason` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `copay` decimal(10,2) DEFAULT NULL,
  `deductible` decimal(10,2) DEFAULT NULL,
  `coins` decimal(10,2) DEFAULT NULL,
  `primaryCarePhy` int(11) DEFAULT NULL,
  `serviceLocID` int(11) DEFAULT NULL,
  `claimStatus` varchar(50) DEFAULT NULL,
  `insuranceID` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `SentBatchNumber` int(11) DEFAULT NULL,
  `ClaimNumber` varchar(20) NOT NULL,
  `created_At` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_claimshistory`
--

CREATE TABLE `m_claimshistory` (
  `historyID` int(11) NOT NULL,
  `claimID` int(10) NOT NULL,
  `pou` varchar(50) NOT NULL,
  `purpose` varchar(50) NOT NULL,
  `notes` varchar(500) NOT NULL,
  `created_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_claimssubmission`
--

CREATE TABLE `m_claimssubmission` (
  `id` int(11) NOT NULL,
  `patientID` int(10) NOT NULL,
  `claimID` int(11) NOT NULL,
  `ClaimNumber` varchar(11) NOT NULL,
  `submitted_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_codecpt`
--

CREATE TABLE `m_codecpt` (
  `id` int(11) NOT NULL,
  `cptCode` varchar(20) NOT NULL,
  `description` varchar(500) NOT NULL,
  `charge` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_codeicd`
--

CREATE TABLE `m_codeicd` (
  `id` int(11) NOT NULL,
  `icdCode` varchar(20) NOT NULL,
  `dxCode` varchar(50) NOT NULL,
  `longDescription` varchar(500) NOT NULL,
  `shortDescription` varchar(300) NOT NULL,
  `codeType` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_comments`
--

CREATE TABLE `m_comments` (
  `commentID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `commentBy` int(11) NOT NULL,
  `commentText` varchar(500) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_crm`
--

CREATE TABLE `m_crm` (
  `id` int(11) NOT NULL,
  `claimID` int(50) NOT NULL,
  `comments` varchar(5000) NOT NULL,
  `dispo` varchar(20) NOT NULL,
  `followupDate` date NOT NULL,
  `workedDate` date NOT NULL,
  `tyreID` int(10) NOT NULL,
  `userID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_depositdetails`
--

CREATE TABLE `m_depositdetails` (
  `depositDetailID` int(11) NOT NULL,
  `depositID` int(11) NOT NULL,
  `claimID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `amountAdjusted` decimal(10,2) NOT NULL,
  `adjustment` decimal(10,2) NOT NULL,
  `claimBalanceIns` decimal(10,2) NOT NULL,
  `claimBalancePat` decimal(10,2) NOT NULL,
  `isdeleted` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `adjustmentDetails` varchar(100) NOT NULL,
  `adjustmentNotes` varchar(50) NOT NULL,
  `adjustmentCode` varchar(50) NOT NULL,
  `created_At` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_deposits`
--

CREATE TABLE `m_deposits` (
  `depositID` int(11) NOT NULL,
  `depositDate` date NOT NULL,
  `chequeNo` varchar(50) NOT NULL,
  `chequeDate` date NOT NULL,
  `payorID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `payorType` varchar(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `paymentType` varchar(10) NOT NULL,
  `notes` varchar(250) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `created_At` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_details`
--

CREATE TABLE `m_details` (
  `detail` varchar(20) NOT NULL,
  `detailvalue1` int(11) NOT NULL,
  `detailvalue2` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_details`
--

INSERT INTO `m_details` (`detail`, `detailvalue1`, `detailvalue2`, `created_at`, `updated_at`) VALUES
('ChartNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00'),
('BatchNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00'),
('ClaimNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_documents`
--

CREATE TABLE `m_documents` (
  `docID` int(11) NOT NULL,
  `docName` varchar(1000) NOT NULL,
  `docType` varchar(10) NOT NULL,
  `docPath` varchar(1000) NOT NULL,
  `status` varchar(50) NOT NULL,
  `notes` varchar(1000) NOT NULL,
  `patientID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `CreatedOn` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_edi`
--

CREATE TABLE `m_edi` (
  `ediID` int(11) NOT NULL,
  `ediInvoiceNo` varchar(50) NOT NULL,
  `ediLocation` varchar(100) NOT NULL,
  `ediCreatedOn` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_employer`
--

CREATE TABLE `m_employer` (
  `employerID` int(11) NOT NULL,
  `employerName` varchar(100) NOT NULL,
  `employerAddr` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_guarantor`
--

CREATE TABLE `m_guarantor` (
  `guarantorID` int(11) NOT NULL,
  `guarantorName` varchar(100) NOT NULL,
  `guarantorDOB` date NOT NULL,
  `guarantorStreet` varchar(500) NOT NULL,
  `guarantorCity` varchar(50) NOT NULL,
  `guarantorState` varchar(50) NOT NULL,
  `guarantorZip` varchar(20) NOT NULL,
  `guarantorPhoneHome` varchar(15) NOT NULL,
  `guarantorRelationship` varchar(100) NOT NULL,
  `patientID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_insurances`
--

CREATE TABLE `m_insurances` (
  `insuranceID` int(11) NOT NULL,
  `payerName` varchar(50) NOT NULL,
  `enr` int(11) NOT NULL,
  `typ` varchar(50) NOT NULL,
  `st` varchar(50) NOT NULL,
  `lob` varchar(50) NOT NULL,
  `rte` int(11) NOT NULL,
  `rts` int(11) NOT NULL,
  `era` int(11) NOT NULL,
  `sec` int(11) NOT NULL,
  `note` varchar(500) NOT NULL,
  `clearingHousePayorID` varchar(50) NOT NULL,
  `addr` varchar(500) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(10) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `abforbilling` varchar(20) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `isActive` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `created_At` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_insured`
--

CREATE TABLE `m_insured` (
  `insuredID` int(11) NOT NULL,
  `insured_fullName` varchar(50) NOT NULL,
  `insured_dob` date NOT NULL,
  `insured_gender` varchar(1) NOT NULL,
  `insured_relationship` varchar(10) NOT NULL,
  `insured_ssn` varchar(20) NOT NULL,
  `insured_martialStatus` varchar(20) NOT NULL,
  `insured_medicalRecord` varchar(20) NOT NULL,
  `insured_employment` varchar(20) NOT NULL,
  `insured_employer` varchar(50) NOT NULL,
  `insured_reference` varchar(50) NOT NULL,
  `insured_isActive` int(11) NOT NULL,
  `insured_isDeleted` int(11) NOT NULL,
  `insured_addrStreet1` varchar(50) NOT NULL,
  `insured_addrStreet2` varchar(50) NOT NULL,
  `insured_addrCity` varchar(20) NOT NULL,
  `insured_addrState` varchar(20) NOT NULL,
  `insured_addrCountry` varchar(20) NOT NULL,
  `insured_addrZip` varchar(10) NOT NULL,
  `insured_email` varchar(50) NOT NULL,
  `insured_phoneHome` varchar(50) NOT NULL,
  `insured_phoneWork` varchar(50) NOT NULL,
  `insured_phoneMobile` varchar(50) NOT NULL,
  `insured_emergencyContact` varchar(50) NOT NULL,
  `insured_emergencyPhone` varchar(50) NOT NULL,
  `patientID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_locations`
--

CREATE TABLE `m_locations` (
  `locID` int(11) NOT NULL,
  `patientID` int(3) NOT NULL,
  `home` int(2) NOT NULL,
  `office` int(2) NOT NULL,
  `school` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_logs`
--

CREATE TABLE `m_logs` (
  `logID` int(11) NOT NULL,
  `logModule` varchar(50) NOT NULL,
  `logDetail` varchar(1024) NOT NULL,
  `logAction` varchar(50) NOT NULL,
  `logRefID` varchar(20) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_modifiers`
--

CREATE TABLE `m_modifiers` (
  `ID` int(11) NOT NULL,
  `modifierCode` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_patients`
--

CREATE TABLE `m_patients` (
  `patientID` int(11) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `relationship` varchar(10) NOT NULL,
  `ssn` varchar(20) NOT NULL,
  `martialStatus` varchar(20) NOT NULL,
  `medicalRecord` varchar(20) NOT NULL,
  `employment` varchar(20) NOT NULL,
  `employer` varchar(50) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `isActive` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `addrStreet1` varchar(50) NOT NULL,
  `addrStreet2` varchar(50) NOT NULL,
  `addrCity` varchar(20) NOT NULL,
  `addrState` varchar(20) NOT NULL,
  `addrCountry` varchar(20) NOT NULL,
  `addrZip` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phoneHome` varchar(50) NOT NULL,
  `phoneWork` varchar(50) NOT NULL,
  `phoneMobile` varchar(50) NOT NULL,
  `notifyEmail` int(11) NOT NULL,
  `notifyPhone` int(11) NOT NULL,
  `IsFinancialResp` int(1) NOT NULL,
  `primaryCarePhy` int(11) NOT NULL,
  `referringPhy` int(11) NOT NULL,
  `defaultRenderPhy` int(11) NOT NULL,
  `defaultServiceLoc` varchar(20) NOT NULL,
  `chartNo` varchar(20) NOT NULL,
  `emergencyContact` varchar(50) NOT NULL,
  `emergencyPhone` varchar(50) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `lastEditedBy` int(11) NOT NULL,
  `conduct` varchar(10) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_physicians`
--

CREATE TABLE `m_physicians` (
  `physicianID` int(11) NOT NULL,
  `phyName` varchar(50) NOT NULL,
  `phoneHome` varchar(20) NOT NULL,
  `phoneWork` varchar(20) NOT NULL,
  `phoneMobile` varchar(20) NOT NULL,
  `phyEmail` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `individualNPI` varchar(50) NOT NULL,
  `ssno` varchar(50) NOT NULL,
  `speciality` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `notes` varchar(1000) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_placeofservices`
--

CREATE TABLE `m_placeofservices` (
  `placeID` int(11) NOT NULL,
  `placeCode` varchar(20) NOT NULL,
  `placeName` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_policies`
--

CREATE TABLE `m_policies` (
  `policyID` int(11) NOT NULL,
  `caseID` int(11) NOT NULL,
  `authNo` varchar(50) NOT NULL,
  `policyEntity` int(11) NOT NULL,
  `insuranceID` int(11) NOT NULL,
  `policyHolder` int(11) NOT NULL,
  `relationshipToInsured` varchar(20) NOT NULL,
  `relationID` int(11) NOT NULL,
  `policyNo` varchar(50) NOT NULL,
  `groupNo` varchar(50) NOT NULL,
  `groupName` varchar(50) NOT NULL,
  `claimNo` varchar(50) NOT NULL,
  `startDt` date NOT NULL,
  `endDt` date NOT NULL,
  `assigned` int(11) NOT NULL,
  `crossoverClaim` int(11) NOT NULL,
  `deductibleMet` int(11) NOT NULL,
  `annualDeductible` int(11) NOT NULL,
  `coPayment` int(11) NOT NULL,
  `treatmentAuth` varchar(50) NOT NULL,
  `docCtrlNo` varchar(50) NOT NULL,
  `insClassA` int(11) NOT NULL,
  `insClassB` int(11) NOT NULL,
  `insClassC` int(11) NOT NULL,
  `insClassD` int(11) NOT NULL,
  `insClassE` int(11) NOT NULL,
  `insClassF` int(11) NOT NULL,
  `insClassG` int(11) NOT NULL,
  `insClassH` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_practices`
--

CREATE TABLE `m_practices` (
  `practiceID` int(11) NOT NULL,
  `practiceName` varchar(1000) NOT NULL,
  `speciality` varchar(100) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `ssn` varchar(20) NOT NULL,
  `EIN` varchar(15) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `phoneHome` varchar(20) NOT NULL,
  `phoneWork` varchar(20) NOT NULL,
  `phoneMobile` varchar(20) NOT NULL,
  `dept` varchar(200) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `npi` varchar(20) NOT NULL,
  `notes` varchar(2000) NOT NULL,
  `isActive` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_procedure`
--

CREATE TABLE `m_procedure` (
  `procedureID` int(11) NOT NULL,
  `procedureCd` varchar(50) NOT NULL,
  `procedureDesc` varchar(500) DEFAULT NULL,
  `procedureCharge` float(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_relationships`
--

CREATE TABLE `m_relationships` (
  `relationID` int(11) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `relationship` varchar(10) NOT NULL,
  `ssn` varchar(20) NOT NULL,
  `martialStatus` varchar(20) NOT NULL,
  `medicalRecord` varchar(20) NOT NULL,
  `employment` varchar(20) NOT NULL,
  `employer` varchar(50) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `isActive` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `addrStreet1` varchar(50) NOT NULL,
  `addrStreet2` varchar(50) NOT NULL,
  `addrCity` varchar(20) NOT NULL,
  `addrState` varchar(20) NOT NULL,
  `addrCountry` varchar(20) NOT NULL,
  `addrZip` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phoneHome` varchar(50) NOT NULL,
  `phoneWork` varchar(50) NOT NULL,
  `phoneMobile` varchar(50) NOT NULL,
  `notifyEmail` int(11) NOT NULL,
  `notifyPhone` int(11) NOT NULL,
  `primaryCarePhy` int(11) NOT NULL,
  `referringPhy` int(11) NOT NULL,
  `defaultRenderPhy` int(11) NOT NULL,
  `defaultServiceLoc` varchar(20) NOT NULL,
  `chartNo` varchar(20) NOT NULL,
  `emergencyContact` varchar(50) NOT NULL,
  `emergencyPhone` varchar(50) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `lastEditedBy` int(11) NOT NULL,
  `conduct` varchar(10) NOT NULL,
  `referralSource` varchar(50) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_serviceLocations`
--

CREATE TABLE `m_serviceLocations` (
  `serviceLocID` int(11) NOT NULL,
  `internalName` varchar(100) NOT NULL,
  `NPI` varchar(100) NOT NULL,
  `overrideEIN` int(11) NOT NULL,
  `timeZone` varchar(100) NOT NULL,
  `legacyNoType` varchar(100) NOT NULL,
  `legacyNo` varchar(100) NOT NULL,
  `billingName` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `placeOfService` varchar(100) NOT NULL,
  `CLIANo` varchar(100) NOT NULL,
  `typeOfBill` varchar(100) NOT NULL,
  `payToName` varchar(100) NOT NULL,
  `instAddress` varchar(1000) NOT NULL,
  `instPhone` varchar(100) NOT NULL,
  `instFax` varchar(100) NOT NULL,
  `isActive` int(11) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_statements`
--

CREATE TABLE `m_statements` (
  `statementID` int(11) NOT NULL,
  `statementName` varchar(50) NOT NULL,
  `patientID` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_taxonomycodes`
--

CREATE TABLE `m_taxonomycodes` (
  `ID` int(11) NOT NULL,
  `medicareCode` varchar(50) NOT NULL,
  `medicareProvider` varchar(200) NOT NULL,
  `taxonomy` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_users`
--

CREATE TABLE `m_users` (
  `userID` int(100) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `emailID` varchar(150) NOT NULL,
  `phoneNo` varchar(15) NOT NULL,
  `IsLogged` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_zipcodes`
--

CREATE TABLE `m_zipcodes` (
  `zipID` int(11) NOT NULL,
  `state` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_zones`
--

CREATE TABLE `m_zones` (
  `zoneID` int(11) NOT NULL,
  `zoneName` varchar(100) NOT NULL,
  `zoneDesc` varchar(100) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `disposition`
--
ALTER TABLE `disposition`
  ADD PRIMARY KEY (`dispoID`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`historyID`);

--
-- Indexes for table `mapdispo`
--
ALTER TABLE `mapdispo`
  ADD PRIMARY KEY (`mapID`);

--
-- Indexes for table `m_activity`
--
ALTER TABLE `m_activity`
  ADD PRIMARY KEY (`activityID`);

--
-- Indexes for table `m_alerts`
--
ALTER TABLE `m_alerts`
  ADD PRIMARY KEY (`alert_ID`);

--
-- Indexes for table `m_appointments`
--
ALTER TABLE `m_appointments`
  ADD PRIMARY KEY (`appID`);

--
-- Indexes for table `m_auth`
--
ALTER TABLE `m_auth`
  ADD PRIMARY KEY (`authID`),
  ADD UNIQUE KEY `authNo` (`authNo`);

--
-- Indexes for table `m_billingCode`
--
ALTER TABLE `m_billingCode`
  ADD PRIMARY KEY (`billingCdID`);

--
-- Indexes for table `m_cases`
--
ALTER TABLE `m_cases`
  ADD PRIMARY KEY (`caseID`);

--
-- Indexes for table `m_claims`
--
ALTER TABLE `m_claims`
  ADD PRIMARY KEY (`claimID`);

--
-- Indexes for table `m_claimshistory`
--
ALTER TABLE `m_claimshistory`
  ADD PRIMARY KEY (`historyID`);

--
-- Indexes for table `m_claimssubmission`
--
ALTER TABLE `m_claimssubmission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_codecpt`
--
ALTER TABLE `m_codecpt`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `m_codeicd`
--
ALTER TABLE `m_codeicd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `dxCode` (`dxCode`);

--
-- Indexes for table `m_comments`
--
ALTER TABLE `m_comments`
  ADD PRIMARY KEY (`commentID`);

--
-- Indexes for table `m_crm`
--
ALTER TABLE `m_crm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_depositdetails`
--
ALTER TABLE `m_depositdetails`
  ADD PRIMARY KEY (`depositDetailID`);

--
-- Indexes for table `m_deposits`
--
ALTER TABLE `m_deposits`
  ADD PRIMARY KEY (`depositID`);

--
-- Indexes for table `m_details`
--
ALTER TABLE `m_details`
  ADD PRIMARY KEY (`detail`);

--
-- Indexes for table `m_documents`
--
ALTER TABLE `m_documents`
  ADD PRIMARY KEY (`docID`);

--
-- Indexes for table `m_edi`
--
ALTER TABLE `m_edi`
  ADD PRIMARY KEY (`ediID`);

--
-- Indexes for table `m_employer`
--
ALTER TABLE `m_employer`
  ADD PRIMARY KEY (`employerID`);

--
-- Indexes for table `m_guarantor`
--
ALTER TABLE `m_guarantor`
  ADD PRIMARY KEY (`guarantorID`);

--
-- Indexes for table `m_insurances`
--
ALTER TABLE `m_insurances`
  ADD PRIMARY KEY (`insuranceID`);

--
-- Indexes for table `m_insured`
--
ALTER TABLE `m_insured`
  ADD PRIMARY KEY (`insuredID`);

--
-- Indexes for table `m_locations`
--
ALTER TABLE `m_locations`
  ADD PRIMARY KEY (`locID`);

--
-- Indexes for table `m_logs`
--
ALTER TABLE `m_logs`
  ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `m_modifiers`
--
ALTER TABLE `m_modifiers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `m_patients`
--
ALTER TABLE `m_patients`
  ADD PRIMARY KEY (`patientID`);

--
-- Indexes for table `m_physicians`
--
ALTER TABLE `m_physicians`
  ADD PRIMARY KEY (`physicianID`);

--
-- Indexes for table `m_placeofservices`
--
ALTER TABLE `m_placeofservices`
  ADD PRIMARY KEY (`placeID`);

--
-- Indexes for table `m_policies`
--
ALTER TABLE `m_policies`
  ADD PRIMARY KEY (`policyID`);

--
-- Indexes for table `m_practices`
--
ALTER TABLE `m_practices`
  ADD PRIMARY KEY (`practiceID`);

--
-- Indexes for table `m_procedure`
--
ALTER TABLE `m_procedure`
  ADD PRIMARY KEY (`procedureID`);

--
-- Indexes for table `m_relationships`
--
ALTER TABLE `m_relationships`
  ADD PRIMARY KEY (`relationID`);

--
-- Indexes for table `m_serviceLocations`
--
ALTER TABLE `m_serviceLocations`
  ADD PRIMARY KEY (`serviceLocID`);

--
-- Indexes for table `m_statements`
--
ALTER TABLE `m_statements`
  ADD PRIMARY KEY (`statementID`);

--
-- Indexes for table `m_taxonomycodes`
--
ALTER TABLE `m_taxonomycodes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `m_zipcodes`
--
ALTER TABLE `m_zipcodes`
  ADD PRIMARY KEY (`zipID`);

--
-- Indexes for table `m_zones`
--
ALTER TABLE `m_zones`
  ADD PRIMARY KEY (`zoneID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `disposition`
--
ALTER TABLE `disposition`
  MODIFY `dispoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `historyID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mapdispo`
--
ALTER TABLE `mapdispo`
  MODIFY `mapID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `m_activity`
--
ALTER TABLE `m_activity`
  MODIFY `activityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `m_alerts`
--
ALTER TABLE `m_alerts`
  MODIFY `alert_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_appointments`
--
ALTER TABLE `m_appointments`
  MODIFY `appID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_auth`
--
ALTER TABLE `m_auth`
  MODIFY `authID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `m_billingCode`
--
ALTER TABLE `m_billingCode`
  MODIFY `billingCdID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_cases`
--
ALTER TABLE `m_cases`
  MODIFY `caseID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `m_claims`
--
ALTER TABLE `m_claims`
  MODIFY `claimID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_claimshistory`
--
ALTER TABLE `m_claimshistory`
  MODIFY `historyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_claimssubmission`
--
ALTER TABLE `m_claimssubmission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `m_comments`
--
ALTER TABLE `m_comments`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_crm`
--
ALTER TABLE `m_crm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_depositdetails`
--
ALTER TABLE `m_depositdetails`
  MODIFY `depositDetailID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_deposits`
--
ALTER TABLE `m_deposits`
  MODIFY `depositID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_documents`
--
ALTER TABLE `m_documents`
  MODIFY `docID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `m_edi`
--
ALTER TABLE `m_edi`
  MODIFY `ediID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_employer`
--
ALTER TABLE `m_employer`
  MODIFY `employerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_guarantor`
--
ALTER TABLE `m_guarantor`
  MODIFY `guarantorID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_insurances`
--
ALTER TABLE `m_insurances`
  MODIFY `insuranceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT for table `m_insured`
--
ALTER TABLE `m_insured`
  MODIFY `insuredID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_locations`
--
ALTER TABLE `m_locations`
  MODIFY `locID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `m_logs`
--
ALTER TABLE `m_logs`
  MODIFY `logID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `m_modifiers`
--
ALTER TABLE `m_modifiers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `m_patients`
--
ALTER TABLE `m_patients`
  MODIFY `patientID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `m_physicians`
--
ALTER TABLE `m_physicians`
  MODIFY `physicianID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `m_placeofservices`
--
ALTER TABLE `m_placeofservices`
  MODIFY `placeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `m_policies`
--
ALTER TABLE `m_policies`
  MODIFY `policyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `m_practices`
--
ALTER TABLE `m_practices`
  MODIFY `practiceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `m_procedure`
--
ALTER TABLE `m_procedure`
  MODIFY `procedureID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_relationships`
--
ALTER TABLE `m_relationships`
  MODIFY `relationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_serviceLocations`
--
ALTER TABLE `m_serviceLocations`
  MODIFY `serviceLocID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_statements`
--
ALTER TABLE `m_statements`
  MODIFY `statementID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_taxonomycodes`
--
ALTER TABLE `m_taxonomycodes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=486;

--
-- AUTO_INCREMENT for table `m_users`
--
ALTER TABLE `m_users`
  MODIFY `userID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `m_zipcodes`
--
ALTER TABLE `m_zipcodes`
  MODIFY `zipID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29385;

--
-- AUTO_INCREMENT for table `m_zones`
--
ALTER TABLE `m_zones`
  MODIFY `zoneID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
