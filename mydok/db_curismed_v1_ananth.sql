-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2018 at 01:19 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_curismed_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `scopes` text,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('00669edc1a0f4db7cb60ddf5d86e9b5e32ec914d4d188d044ffe3d27d24467fd707eca229768b400', 1, 1, 'MyApp', '[]', 0, '2018-08-23 01:56:52', '2018-08-23 01:56:52', '2019-08-23 07:26:52'),
('1e3dd53b78079b35fc4e5484e2a4c8ba7f02924ba583c317bed24b599959074b93435ec4cba34d25', 1, 1, 'MyApp', '[]', 0, '2018-08-23 01:56:31', '2018-08-23 01:56:31', '2019-08-23 07:26:31'),
('414a1434e13f338537e8be04e87106b9638405b7d5559bb50253d645753d66c505352ed751c32d4d', 1, 1, 'MyApp', '[]', 0, '2018-08-23 01:53:29', '2018-08-23 01:53:29', '2019-08-23 07:23:29'),
('4b7a8505b4cf7bc7bbb7f970d322160b007024fc395b48fafa28f1ad675b2e42ecb1e18fb155a78c', 1, 1, '2018-08-23 06:19:49', '[]', 0, '2018-08-23 00:49:50', '2018-08-23 00:49:50', '2019-08-23 06:19:50'),
('69ba131152feaec7afd5c3a0b6c44bc3bf123c19b2648c269cb6bba7054ee05350a03c019065256f', 1, 1, 'MyApp', '[]', 0, '2018-08-23 01:57:07', '2018-08-23 01:57:07', '2019-08-23 07:27:07'),
('8da210fe3330e31f18e28d3b7c090dfe4d46ed414a44352f2cf65997b4da8e5ba23649ee0cce19a7', 1, 1, 'MyApp', '[]', 0, '2018-08-23 01:55:55', '2018-08-23 01:55:55', '2019-08-23 07:25:55'),
('a5edf5ecac4952c453b41525f298479f44682600110088130641d83f9b8a5ce9155509de3fd8edc8', 1, 1, 'MyApp', '[]', 0, '2018-08-23 01:55:42', '2018-08-23 01:55:42', '2019-08-23 07:25:42'),
('b7fa75757f08f6859f56566e95b3aa552a7907e793f9d59cc181adcc27159dd79fc0d5914fd07a5c', 1, 1, 'MyApp', '[]', 0, '2018-08-23 01:56:47', '2018-08-23 01:56:47', '2019-08-23 07:26:47'),
('f53b294adf9ff2f61c28fc14c75d08128ae1f5db1817daece815f0b5720464891f4257b9deb9f424', 1, 1, 'MyApp', '[]', 0, '2018-08-23 01:56:40', '2018-08-23 01:56:40', '2019-08-23 07:26:40'),
('fb2cbd773a48b340f0b558616b8e385e945a87f58be2351838cba20f48a4a674bd86c34ff8805758', 1, 1, 'MyApp', '[]', 0, '2018-08-23 00:50:05', '2018-08-23 00:50:05', '2019-08-23 06:20:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `scopes` text,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `secret` varchar(100) NOT NULL,
  `redirect` text,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '7X61XonRmaLjqaue2Y8O6hKLOHEmeuhVygqS6WHk', 'http://localhost', 1, 0, 0, '2018-08-04 06:28:00', '2018-08-04 06:28:00'),
(2, NULL, 'Laravel Password Grant Client', '3PF0nBejsgERULceggQZQuvQQEHCpLA1Gn2vqxTt', 'http://localhost', 0, 1, 0, '2018-08-04 06:28:00', '2018-08-04 06:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-08-04 06:28:00', '2018-08-04 06:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) NOT NULL,
  `access_token_id` varchar(100) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_acl_list`
--

CREATE TABLE `tb_acl_list` (
  `id` bigint(20) NOT NULL,
  `acl_key` varchar(128) DEFAULT '',
  `acl_name` varchar(64) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_acl_list`
--

INSERT INTO `tb_acl_list` (`id`, `acl_key`, `acl_name`, `created_at`, `updated_at`) VALUES
(1, 'ACL_CLIENTS', 'Clients', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(2, 'ACL_EMPLOYEES', 'Employees', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(3, 'ACL_SCHEDULER', 'Scheduler', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(4, 'ACL_EDI', 'EDI', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(5, 'ACL_STATEMENTS', 'Statements', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(6, 'ACL_LEDGER', 'Ledger', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(7, 'ACL_DEPOSITS', 'Deposits', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(8, 'ACL_CRM', 'CRM', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(9, 'ACL_REPORTS', 'Reports', '2018-08-19 10:05:14', '2018-08-19 10:05:14'),
(10, 'ACL_SETTINGS', 'Settings', '2018-08-19 10:05:14', '2018-08-19 10:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_activity`
--

CREATE TABLE `tb_activity` (
  `id` bigint(20) NOT NULL,
  `case_id` bigint(20) DEFAULT '0',
  `auth_num` varchar(32) DEFAULT '',
  `name` varchar(64) DEFAULT '',
  `cpt` varchar(32) DEFAULT '',
  `mod1` varchar(16) DEFAULT '',
  `mod2` varchar(10) DEFAULT '',
  `mod3` varchar(10) DEFAULT '',
  `mod4` varchar(10) DEFAULT '',
  `billed_per` varchar(10) DEFAULT '',
  `billed_per_time` varchar(10) DEFAULT '',
  `total_auth` int(10) DEFAULT '0',
  `auth_aype` varchar(50) DEFAULT '',
  `remain_auth` int(10) DEFAULT '0',
  `rates` decimal(10,2) DEFAULT '0.00',
  `max1` varchar(10) DEFAULT '',
  `max2` varchar(10) DEFAULT '',
  `max3` varchar(10) DEFAULT '',
  `per1` varchar(10) DEFAULT '',
  `per2` varchar(10) DEFAULT '',
  `per3` varchar(10) DEFAULT '',
  `is_1` varchar(10) DEFAULT '',
  `is_2` varchar(10) DEFAULT '',
  `is_3` varchar(10) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_activity`
--

INSERT INTO `tb_activity` (`id`, `case_id`, `auth_num`, `name`, `cpt`, `mod1`, `mod2`, `mod3`, `mod4`, `billed_per`, `billed_per_time`, `total_auth`, `auth_aype`, `remain_auth`, `rates`, `max1`, `max2`, `max3`, `per1`, `per2`, `per3`, `is_1`, `is_2`, `is_3`, `created_at`, `updated_at`) VALUES
(1, 1, '45645646', 'Direct Behavior Theraphy', '0359T', '11', NULL, NULL, NULL, '30 mins', NULL, 1000, 'Total Auth', 986, '25.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-20 04:51:20', '2018-08-20 04:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_alerts`
--

CREATE TABLE `tb_alerts` (
  `id` bigint(20) NOT NULL,
  `desc` text,
  `patient_id` bigint(20) DEFAULT '0',
  `is_edit_patient` int(1) DEFAULT '0',
  `is_schedule_app` int(1) DEFAULT '0',
  `is_charge` int(1) DEFAULT '0',
  `is_view_claims` int(1) DEFAULT '0',
  `is_deposits` int(1) DEFAULT '0',
  `is_statements` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_appointments`
--

CREATE TABLE `tb_appointments` (
  `id` bigint(20) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `patient_name` varchar(50) DEFAULT '',
  `auth` varchar(20) DEFAULT '',
  `activity` varchar(50) DEFAULT '',
  `location` varchar(20) DEFAULT '',
  `provider` varchar(50) DEFAULT '',
  `desc` text,
  `status` varchar(20) DEFAULT '',
  `patient_id` bigint(20) DEFAULT '0',
  `provider_id` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_appointments`
--

INSERT INTO `tb_appointments` (`id`, `start_date`, `end_date`, `patient_name`, `auth`, `activity`, `location`, `provider`, `desc`, `status`, `patient_id`, `provider_id`, `created_at`, `updated_at`) VALUES
(2, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Rendered', 1, 1, NULL, NULL),
(3, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(4, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(5, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(6, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(7, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(8, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(9, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(10, '2018-08-20 09:00:00', '2018-08-20 09:30:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Ananth B Sriram', '45645646', '1', '03', '', '', 'Confirm', 1, 0, NULL, NULL),
(12, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(13, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(14, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(15, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(16, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(17, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(18, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(19, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(20, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(21, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(22, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(23, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(24, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Confirm', 1, 1, NULL, NULL),
(25, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Rendered', 1, 1, NULL, NULL),
(26, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Rendered', 1, 1, NULL, NULL),
(27, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Rendered', 1, 1, NULL, NULL),
(28, '2018-08-20 09:00:00', '2018-08-20 09:15:00', 'Ananth B Sriram', '45645646', '1', '03', 'Sriram', '', 'Rendered', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_auth`
--

CREATE TABLE `tb_auth` (
  `id` bigint(20) NOT NULL,
  `patient_id` bigint(20) DEFAULT '0',
  `case_id` bigint(20) DEFAULT '0',
  `policy_entity` int(5) DEFAULT '0',
  `tos` varchar(50) DEFAULT '',
  `auth_num` varchar(100) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_auth`
--

INSERT INTO `tb_auth` (`id`, `patient_id`, `case_id`, `policy_entity`, `tos`, `auth_num`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'Behaviour Therapy', '45645646', '2018-08-01 00:00:00', '2018-08-31 00:00:00', '2018-08-20 04:46:30', '2018-08-20 04:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `tb_billing_code`
--

CREATE TABLE `tb_billing_code` (
  `id` bigint(20) NOT NULL,
  `cd` varchar(16) DEFAULT '',
  `desc` varchar(100) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cases`
--

CREATE TABLE `tb_cases` (
  `id` bigint(20) NOT NULL,
  `patient_id` bigint(20) NOT NULL,
  `is_closed` int(1) NOT NULL DEFAULT '0',
  `description` varchar(64) NOT NULL,
  `allow_print` int(1) NOT NULL DEFAULT '1',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `chart_num` varchar(32) NOT NULL,
  `global_coverage_until` varchar(64) DEFAULT NULL,
  `employer` varchar(50) DEFAULT NULL,
  `emp_status` varchar(50) DEFAULT NULL,
  `retirement_date` date DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `work_phone` varchar(50) DEFAULT NULL,
  `extension` varchar(50) DEFAULT NULL,
  `stud_status` varchar(50) DEFAULT NULL,
  `guarantor` varchar(50) DEFAULT NULL,
  `marital_status` varchar(50) DEFAULT NULL,
  `assigned_provider` bigint(20) NOT NULL DEFAULT '0',
  `referring_provider` bigint(20) NOT NULL DEFAULT '0',
  `supervising_provider` bigint(20) NOT NULL DEFAULT '0',
  `operating_provider` bigint(20) NOT NULL DEFAULT '0',
  `other_provider` bigint(20) NOT NULL DEFAULT '0',
  `referral_source` varchar(50) DEFAULT NULL,
  `attorney` varchar(50) DEFAULT NULL,
  `facility` varchar(50) DEFAULT NULL,
  `billing_code` varchar(50) DEFAULT NULL,
  `price_code` varchar(50) DEFAULT NULL,
  `other_arrangements` varchar(50) DEFAULT NULL,
  `authorized_through` varchar(50) DEFAULT NULL,
  `auth_num` varchar(50) DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `no_of_visits` bigint(20) DEFAULT NULL,
  `total_visits` int(10) DEFAULT NULL,
  `auth_id` varchar(50) DEFAULT NULL,
  `last_visit_num` varchar(50) DEFAULT NULL,
  `principal_diag` varchar(100) DEFAULT NULL,
  `def_diag_2` varchar(100) DEFAULT NULL,
  `def_diag_3` varchar(100) DEFAULT NULL,
  `def_diag_4` varchar(100) DEFAULT NULL,
  `poa1` varchar(100) DEFAULT NULL,
  `poa2` varchar(100) DEFAULT NULL,
  `poa3` varchar(100) DEFAULT NULL,
  `poa4` varchar(100) DEFAULT NULL,
  `allergies_notes` varchar(1000) DEFAULT NULL,
  `edi_notes` varchar(1000) DEFAULT NULL,
  `rep_type_code` varchar(50) DEFAULT NULL,
  `attach_ctrl_num` varchar(50) DEFAULT NULL,
  `rep_trans_code` varchar(50) DEFAULT NULL,
  `injury_date` date DEFAULT NULL,
  `illness_indicator` varchar(50) DEFAULT NULL,
  `first_consult_date` date DEFAULT NULL,
  `similar_symptoms` varchar(100) DEFAULT NULL,
  `same_symptom` bigint(20) DEFAULT NULL,
  `emp_related` bigint(20) DEFAULT NULL,
  `emergency` bigint(20) DEFAULT NULL,
  `related_to` varchar(50) DEFAULT NULL,
  `related_state` varchar(50) DEFAULT NULL,
  `nature` varchar(50) DEFAULT NULL,
  `last_xray` datetime DEFAULT NULL,
  `death_status` varchar(50) DEFAULT NULL,
  `unable_to_work_from` datetime DEFAULT NULL,
  `unable_to_work_to` datetime DEFAULT NULL,
  `total_disab_from` datetime DEFAULT NULL,
  `tot_disab_to` datetime DEFAULT NULL,
  `part_disab_from` datetime DEFAULT NULL,
  `part_disab_to` datetime DEFAULT NULL,
  `hosp_form` date DEFAULT NULL,
  `hosp_to` date DEFAULT NULL,
  `return_work_indicator` varchar(50) DEFAULT NULL,
  `disability_percent` varchar(50) DEFAULT NULL,
  `last_work_date` date DEFAULT NULL,
  `pregnant` bigint(20) DEFAULT NULL,
  `estimated_dob` datetime DEFAULT NULL,
  `date_assumed_care` datetime DEFAULT NULL,
  `date_relinquished_care` date DEFAULT NULL,
  `outside_lab` bigint(20) DEFAULT NULL,
  `lab_charges` bigint(20) DEFAULT NULL,
  `local_use_a` varchar(20) DEFAULT NULL,
  `local_use_b` varchar(20) DEFAULT NULL,
  `indicator` varchar(50) DEFAULT NULL,
  `ref_date` date DEFAULT NULL,
  `prescrip_date` date DEFAULT NULL,
  `prior_auth_num` varchar(50) DEFAULT NULL,
  `extra1` varchar(50) DEFAULT NULL,
  `extra2` varchar(50) DEFAULT NULL,
  `extra3` varchar(50) DEFAULT NULL,
  `extra4` varchar(50) DEFAULT NULL,
  `outside_provider` varchar(50) DEFAULT NULL,
  `date_last_seen` date DEFAULT NULL,
  `epsdt` bigint(20) DEFAULT NULL,
  `resubmission_date` date DEFAULT NULL,
  `family_planning` bigint(20) DEFAULT NULL,
  `original_ref_no` varchar(50) DEFAULT NULL,
  `service_auth` varchar(50) DEFAULT NULL,
  `non_avail_indicator` varchar(50) DEFAULT NULL,
  `branch_service` varchar(50) DEFAULT NULL,
  `sponsor_stat` varchar(50) DEFAULT NULL,
  `spl_program` varchar(50) DEFAULT NULL,
  `sponsor_guide` varchar(50) DEFAULT NULL,
  `eff_start_date` date DEFAULT NULL,
  `eff_end_date` date DEFAULT NULL,
  `care_plan_over_sight` varchar(50) DEFAULT NULL,
  `hospice_num` varchar(50) DEFAULT NULL,
  `cliano` varchar(50) DEFAULT NULL,
  `mamo_certification` varchar(50) DEFAULT NULL,
  `ref_access_num` varchar(50) DEFAULT NULL,
  `demo_code` varchar(50) DEFAULT NULL,
  `assign_indicator` varchar(50) DEFAULT NULL,
  `insurance_type_code` varchar(50) DEFAULT NULL,
  `timely_filling_indicator` varchar(50) DEFAULT NULL,
  `epsdt_ref_code1` varchar(50) DEFAULT NULL,
  `epsdt_ref_code2` varchar(50) DEFAULT NULL,
  `epsdt_ref_code3` varchar(50) DEFAULT NULL,
  `homebound` bigint(20) DEFAULT NULL,
  `ideno` varchar(50) DEFAULT NULL,
  `condition_indicator` varchar(50) DEFAULT NULL,
  `cert_code_applies` bigint(20) DEFAULT NULL,
  `code_category` varchar(50) DEFAULT NULL,
  `total_visit_rendered` varchar(50) DEFAULT NULL,
  `total_visit_projected` varchar(50) DEFAULT NULL,
  `num_of_visits_home_health` bigint(20) DEFAULT NULL,
  `num_of_units` bigint(20) DEFAULT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `discipline_type_code` varchar(50) DEFAULT NULL,
  `delivery_pattern_code` varchar(50) DEFAULT NULL,
  `delivery_time_code` varchar(50) DEFAULT NULL,
  `freq_period` bigint(20) DEFAULT NULL,
  `freq_count` bigint(20) DEFAULT NULL,
  `create_dby` bigint(20) DEFAULT NULL,
  `is_cash` bigint(20) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cases`
--

INSERT INTO `tb_cases` (`id`, `patient_id`, `is_closed`, `description`, `allow_print`, `is_deleted`, `date`, `chart_num`, `global_coverage_until`, `employer`, `emp_status`, `retirement_date`, `location`, `work_phone`, `extension`, `stud_status`, `guarantor`, `marital_status`, `assigned_provider`, `referring_provider`, `supervising_provider`, `operating_provider`, `other_provider`, `referral_source`, `attorney`, `facility`, `billing_code`, `price_code`, `other_arrangements`, `authorized_through`, `auth_num`, `last_visit`, `no_of_visits`, `total_visits`, `auth_id`, `last_visit_num`, `principal_diag`, `def_diag_2`, `def_diag_3`, `def_diag_4`, `poa1`, `poa2`, `poa3`, `poa4`, `allergies_notes`, `edi_notes`, `rep_type_code`, `attach_ctrl_num`, `rep_trans_code`, `injury_date`, `illness_indicator`, `first_consult_date`, `similar_symptoms`, `same_symptom`, `emp_related`, `emergency`, `related_to`, `related_state`, `nature`, `last_xray`, `death_status`, `unable_to_work_from`, `unable_to_work_to`, `total_disab_from`, `tot_disab_to`, `part_disab_from`, `part_disab_to`, `hosp_form`, `hosp_to`, `return_work_indicator`, `disability_percent`, `last_work_date`, `pregnant`, `estimated_dob`, `date_assumed_care`, `date_relinquished_care`, `outside_lab`, `lab_charges`, `local_use_a`, `local_use_b`, `indicator`, `ref_date`, `prescrip_date`, `prior_auth_num`, `extra1`, `extra2`, `extra3`, `extra4`, `outside_provider`, `date_last_seen`, `epsdt`, `resubmission_date`, `family_planning`, `original_ref_no`, `service_auth`, `non_avail_indicator`, `branch_service`, `sponsor_stat`, `spl_program`, `sponsor_guide`, `eff_start_date`, `eff_end_date`, `care_plan_over_sight`, `hospice_num`, `cliano`, `mamo_certification`, `ref_access_num`, `demo_code`, `assign_indicator`, `insurance_type_code`, `timely_filling_indicator`, `epsdt_ref_code1`, `epsdt_ref_code2`, `epsdt_ref_code3`, `homebound`, `ideno`, `condition_indicator`, `cert_code_applies`, `code_category`, `total_visit_rendered`, `total_visit_projected`, `num_of_visits_home_health`, `num_of_units`, `duration`, `discipline_type_code`, `delivery_pattern_code`, `delivery_time_code`, `freq_period`, `freq_count`, `create_dby`, `is_cash`, `updated_at`, `created_at`) VALUES
(1, 1, 0, 'Primary', 1, 0, NULL, 'ANASRI00029_1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '45645646', NULL, NULL, NULL, NULL, NULL, 'F45.0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-20 04:46:28', '2018-08-20 04:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `tb_claims`
--

CREATE TABLE `tb_claims` (
  `id` bigint(20) NOT NULL,
  `case_id` bigint(20) DEFAULT '0',
  `app_id` bigint(20) DEFAULT '0',
  `auth_num` varchar(40) DEFAULT '',
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `proced` varchar(50) DEFAULT '',
  `mod1` varchar(50) DEFAULT '',
  `units` int(5) DEFAULT '0',
  `charge` decimal(10,2) DEFAULT '0.00',
  `total` decimal(10,2) DEFAULT '0.00',
  `diag1` varchar(50) DEFAULT '',
  `diag2` varchar(50) DEFAULT '',
  `diag3` varchar(50) DEFAULT '',
  `diag4` varchar(50) DEFAULT '',
  `applied` bigint(20) DEFAULT '0',
  `notes` text,
  `ref_code` varchar(15) DEFAULT '',
  `place_of_service` varchar(50) DEFAULT '',
  `is_posted` bigint(20) DEFAULT '0',
  `is_sent` int(1) DEFAULT '0',
  `is_sec_claim` int(1) DEFAULT '0',
  `allowed` decimal(10,2) DEFAULT '0.00',
  `paid` decimal(10,2) DEFAULT '0.00',
  `adjustment` decimal(10,2) DEFAULT '0.00',
  `adjust_code` varchar(100) DEFAULT '',
  `claim_balance` decimal(10,2) DEFAULT '0.00',
  `claim_balance_pat` decimal(10,2) DEFAULT '0.00',
  `reason` text,
  `status` varchar(50) DEFAULT '',
  `copay` decimal(10,2) DEFAULT '0.00',
  `deductible` decimal(10,2) DEFAULT '0.00',
  `coins` decimal(10,2) DEFAULT '0.00',
  `primary_care_phy` bigint(20) DEFAULT '0',
  `service_loc_id` bigint(20) DEFAULT '0',
  `claim_status` varchar(50) DEFAULT '',
  `insurance_id` bigint(20) DEFAULT '0',
  `created_by` bigint(20) DEFAULT '0',
  `is_deleted` int(1) DEFAULT '0',
  `sent_batch_number` bigint(20) DEFAULT '0',
  `claim_number` varchar(20) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_claims`
--

INSERT INTO `tb_claims` (`id`, `case_id`, `app_id`, `auth_num`, `from_date`, `to_date`, `proced`, `mod1`, `units`, `charge`, `total`, `diag1`, `diag2`, `diag3`, `diag4`, `applied`, `notes`, `ref_code`, `place_of_service`, `is_posted`, `is_sent`, `is_sec_claim`, `allowed`, `paid`, `adjustment`, `adjust_code`, `claim_balance`, `claim_balance_pat`, `reason`, `status`, `copay`, `deductible`, `coins`, `primary_care_phy`, `service_loc_id`, `claim_status`, `insurance_id`, `created_by`, `is_deleted`, `sent_batch_number`, `claim_number`, `created_at`, `updated_at`) VALUES
(1, 1, 18, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 1, 1, 0, '25.00', '20.00', '0.00', '', '0.00', '5.00', 'Contractual Adj', '1', '0.00', '0.00', '0.00', 1, 3, 'Submit', -100, 0, 0, 0, '00002', NULL, '2018-08-23 05:48:16'),
(2, 1, 19, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 1, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', '', '1', '0.00', '0.00', '0.00', 1, 3, 'SUbmit', 1, 0, 0, 0, '00003', NULL, '2018-08-23 05:27:02'),
(3, 1, 20, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 1, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', '', '1', '0.00', '0.00', '0.00', 1, 3, 'Submit', 1, 0, 0, 0, '00004', NULL, '2018-08-23 05:26:11'),
(4, 1, 21, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 0, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', NULL, '', '0.00', '0.00', '0.00', 1, 3, 'Confirm', 1, 0, 0, 0, '00005', NULL, NULL),
(5, 1, 22, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 0, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', NULL, '', '0.00', '0.00', '0.00', 1, 3, 'Confirm', 1, 0, 0, 0, '00006', NULL, NULL),
(6, 1, 23, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 0, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', NULL, '', '0.00', '0.00', '0.00', 1, 3, 'Confirm', 1, 0, 0, 0, '00007', NULL, NULL),
(7, 1, 24, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 0, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', NULL, '', '0.00', '0.00', '0.00', 1, 3, 'Confirm', 1, 0, 0, 0, '00008', NULL, NULL),
(8, 1, 25, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 0, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', NULL, '', '0.00', '0.00', '0.00', 1, 3, 'Rendered', 1, 0, 0, 0, '00009', NULL, NULL),
(9, 1, 26, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 0, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', NULL, '', '0.00', '0.00', '0.00', 1, 3, 'Rendered', 1, 0, 0, 0, '00010', NULL, NULL),
(10, 1, 27, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 0, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', NULL, 'Rendered', '0.00', '0.00', '0.00', 1, 3, 'Rendered', 1, 0, 0, 0, '00011', NULL, NULL),
(11, 1, 28, '45645646', '2018-08-20 09:00:00', '2018-08-20 09:00:00', '0359T', '11   ', 1, '25.00', '25.00', 'F45.0', '', '', '', 0, NULL, '', '03', 0, 0, 0, '0.00', '0.00', '0.00', '', '25.00', '0.00', NULL, 'Rendered', '0.00', '0.00', '0.00', 1, 3, 'Rendered', 1, 0, 0, 0, '00012', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_claims_history`
--

CREATE TABLE `tb_claims_history` (
  `id` bigint(20) NOT NULL,
  `claim_id` bigint(20) DEFAULT '0',
  `pou` varchar(50) DEFAULT '',
  `purpose` varchar(50) DEFAULT '',
  `notes` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_claims_history`
--

INSERT INTO `tb_claims_history` (`id`, `claim_id`, `pou`, `purpose`, `notes`, `created_at`, `updated_at`) VALUES
(1, 10, 'Claims', 'Created', 'Claim has been created', NULL, NULL),
(2, 11, 'Claims', 'Created', 'Claim has been created', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_claims_submission`
--

CREATE TABLE `tb_claims_submission` (
  `id` bigint(20) NOT NULL,
  `patient_id` bigint(20) DEFAULT '0',
  `claim_id` bigint(20) DEFAULT '0',
  `claim_number` varchar(11) DEFAULT '',
  `submitted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_code_cpt`
--

CREATE TABLE `tb_code_cpt` (
  `id` bigint(20) NOT NULL,
  `cpt_code` varchar(20) DEFAULT '',
  `description` text,
  `charge` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_code_icd`
--

CREATE TABLE `tb_code_icd` (
  `id` bigint(20) NOT NULL,
  `icd_code` varchar(20) DEFAULT '',
  `dx_code` varchar(50) DEFAULT '',
  `long_description` text,
  `short_description` text,
  `code_type` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_comments`
--

CREATE TABLE `tb_comments` (
  `id` bigint(20) NOT NULL,
  `patient_id` bigint(20) DEFAULT '0',
  `comment_by` bigint(20) DEFAULT '0',
  `comment_text` text,
  `is_deleted` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_crm`
--

CREATE TABLE `tb_crm` (
  `id` bigint(20) NOT NULL,
  `claim_id` bigint(20) DEFAULT '0',
  `comments` text,
  `dispo` varchar(20) DEFAULT '',
  `followup_date` datetime DEFAULT NULL,
  `worked_date` datetime DEFAULT NULL,
  `tyre_id` bigint(20) DEFAULT '0',
  `user_id` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_crm`
--

INSERT INTO `tb_crm` (`id`, `claim_id`, `comments`, `dispo`, `followup_date`, `worked_date`, `tyre_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 3, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, NULL, NULL),
(2, 6, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, NULL, NULL),
(3, 9, '', '', '2018-08-20 22:23:18', '2018-08-20 22:23:18', 1, 1, NULL, NULL),
(4, 10, '', '', '2018-08-20 22:23:56', '2018-08-20 22:23:56', 1, 1, NULL, NULL),
(5, 11, '', '', '2018-08-20 22:24:43', '2018-08-20 22:24:43', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_deposits`
--

CREATE TABLE `tb_deposits` (
  `id` bigint(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `cheque_num` varchar(50) DEFAULT '',
  `cheque_date` datetime DEFAULT NULL,
  `payor_id` bigint(20) DEFAULT '0',
  `patient_id` bigint(20) DEFAULT '0',
  `payor_type` varchar(10) DEFAULT '',
  `amount` decimal(10,2) DEFAULT '0.00',
  `payment_type` varchar(10) DEFAULT '',
  `notes` text,
  `created_by` bigint(20) DEFAULT '0',
  `practice_id` bigint(20) DEFAULT '0',
  `is_deleted` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_deposits`
--

INSERT INTO `tb_deposits` (`id`, `date`, `cheque_num`, `cheque_date`, `payor_id`, `patient_id`, `payor_type`, `amount`, `payment_type`, `notes`, `created_by`, `practice_id`, `is_deleted`, `created_at`, `updated_at`) VALUES
(2, '2018-10-01 00:00:00', '582', '2018-08-01 00:00:00', 1, 0, '1', '250.00', 'Check', NULL, 0, 1, 0, '2018-08-23 01:04:06', '2018-08-23 01:04:06');

-- --------------------------------------------------------

--
-- Table structure for table `tb_deposit_details`
--

CREATE TABLE `tb_deposit_details` (
  `id` bigint(20) NOT NULL,
  `deposit_id` int(11) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT '0',
  `patient_id` bigint(20) DEFAULT '0',
  `amount_adjusted` decimal(10,2) DEFAULT '0.00',
  `adjustment` decimal(10,2) DEFAULT '0.00',
  `claim_balance_ins` decimal(10,2) DEFAULT '0.00',
  `claim_balance_pat` decimal(10,2) DEFAULT '0.00',
  `is_deleted` int(1) DEFAULT '0',
  `created_by` bigint(20) DEFAULT '0',
  `adjustment_details` varchar(100) DEFAULT '',
  `adjustment_notes` varchar(50) DEFAULT '',
  `adjustment_code` varchar(50) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_deposit_details`
--

INSERT INTO `tb_deposit_details` (`id`, `deposit_id`, `claim_id`, `patient_id`, `amount_adjusted`, `adjustment`, `claim_balance_ins`, `claim_balance_pat`, `is_deleted`, `created_by`, `adjustment_details`, `adjustment_notes`, `adjustment_code`, `created_at`, `updated_at`) VALUES
(1, 2, 1, -1, '20.00', '0.00', '0.00', '5.00', 0, 0, '', '1', 'Contractual Adj', '2018-08-23 05:48:16', '2018-08-23 05:48:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_details`
--

CREATE TABLE `tb_details` (
  `id` bigint(20) NOT NULL,
  `detail` varchar(20) DEFAULT '',
  `detailvalue1` bigint(20) DEFAULT '0',
  `detailvalue2` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_details`
--

INSERT INTO `tb_details` (`id`, `detail`, `detailvalue1`, `detailvalue2`, `created_at`, `updated_at`) VALUES
(1, 'ChartNumber', 29, '', NULL, '2018-08-20 04:44:02'),
(3, 'ClaimNumber', 12, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_disposition`
--

CREATE TABLE `tb_disposition` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_documents`
--

CREATE TABLE `tb_documents` (
  `id` bigint(20) NOT NULL,
  `name` text,
  `type` varchar(10) DEFAULT '',
  `path` text,
  `status` varchar(50) DEFAULT '',
  `notes` text,
  `patient_id` bigint(20) DEFAULT '0',
  `user_id` bigint(20) DEFAULT '0',
  `practice_id` bigint(20) DEFAULT '0',
  `created_on` varchar(30) DEFAULT '',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_edi`
--

CREATE TABLE `tb_edi` (
  `id` bigint(20) NOT NULL,
  `invoice_num` varchar(50) DEFAULT '',
  `location` varchar(100) DEFAULT '',
  `created_on` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_guarantor`
--

CREATE TABLE `tb_guarantor` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT '',
  `dob` datetime DEFAULT NULL,
  `street` text,
  `city` varchar(50) DEFAULT '',
  `state` varchar(50) DEFAULT '',
  `zip` varchar(20) DEFAULT '',
  `phone_home` varchar(15) DEFAULT '',
  `relationship` varchar(100) DEFAULT '',
  `patient_id` bigint(20) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_history`
--

CREATE TABLE `tb_history` (
  `id` bigint(20) NOT NULL,
  `claim_id` bigint(20) DEFAULT '0',
  `disposition_id` bigint(20) DEFAULT '0',
  `comments` text,
  `worked_date` datetime DEFAULT NULL,
  `user_id` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_insurances`
--

CREATE TABLE `tb_insurances` (
  `id` bigint(20) NOT NULL,
  `payer_name` varchar(50) DEFAULT '',
  `enr` bigint(20) DEFAULT '0',
  `typ` varchar(50) DEFAULT '',
  `st` varchar(50) DEFAULT '',
  `lob` varchar(50) DEFAULT '',
  `rte` bigint(20) DEFAULT '0',
  `rts` bigint(20) DEFAULT '0',
  `era` bigint(20) DEFAULT '0',
  `sec` bigint(20) DEFAULT '0',
  `note` text,
  `clearing_house_payor_id` varchar(50) DEFAULT '',
  `addr` text,
  `city` varchar(30) DEFAULT '',
  `state` varchar(10) DEFAULT '',
  `zip` varchar(20) DEFAULT '',
  `phone` varchar(20) DEFAULT '',
  `fax` varchar(20) DEFAULT '',
  `abforbilling` varchar(20) DEFAULT '',
  `practice_id` bigint(20) DEFAULT '0',
  `is_active` int(1) DEFAULT '1',
  `created_by` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_insurances`
--

INSERT INTO `tb_insurances` (`id`, `payer_name`, `enr`, `typ`, `st`, `lob`, `rte`, `rts`, `era`, `sec`, `note`, `clearing_house_payor_id`, `addr`, `city`, `state`, `zip`, `phone`, `fax`, `abforbilling`, `practice_id`, `is_active`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'AETNA', 0, '0', '0', '0', 0, 0, 0, 0, '', '00240', '123, Valley Ave', 'Vienna', 'VA', '06851', '', '', '', 0, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_locations`
--

CREATE TABLE `tb_locations` (
  `id` bigint(20) NOT NULL,
  `patient_id` bigint(20) DEFAULT '0',
  `home` int(2) DEFAULT '0',
  `office` int(2) DEFAULT '0',
  `school` int(2) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_locations`
--

INSERT INTO `tb_locations` (`id`, `patient_id`, `home`, `office`, `school`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 1, '2018-08-20 04:44:02', '2018-08-20 04:44:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_patients`
--

CREATE TABLE `tb_patients` (
  `id` bigint(20) NOT NULL,
  `full_name` varchar(50) DEFAULT '',
  `dob` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT '',
  `relationship` varchar(10) DEFAULT '',
  `ssn` varchar(20) DEFAULT '',
  `martial_status` varchar(20) DEFAULT '',
  `medical_record` varchar(20) DEFAULT '',
  `employment` varchar(20) DEFAULT '',
  `employer` varchar(50) DEFAULT '',
  `reference` varchar(50) DEFAULT '',
  `is_active` int(1) DEFAULT '0',
  `is_deleted` int(1) DEFAULT '0',
  `addr_street1` varchar(50) DEFAULT '',
  `addr_street2` varchar(50) DEFAULT '',
  `addr_city` varchar(20) DEFAULT '',
  `addr_state` varchar(20) DEFAULT '',
  `addr_country` varchar(20) DEFAULT '',
  `addr_zip` varchar(10) DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `phone_home` varchar(50) DEFAULT '',
  `phone_work` varchar(50) DEFAULT '',
  `phone_mobile` varchar(50) DEFAULT '',
  `notifyemail` int(1) DEFAULT '0',
  `notify_phone` int(1) DEFAULT '0',
  `is_financial_resp` int(1) DEFAULT '0',
  `primary_care_phy` bigint(20) DEFAULT '0',
  `referring_phy` bigint(20) DEFAULT '0',
  `default_render_phy` bigint(20) DEFAULT '0',
  `default_service_loc` varchar(20) DEFAULT '',
  `chart_num` varchar(20) DEFAULT '',
  `emergency_contact` varchar(50) DEFAULT '',
  `emergency_phone` varchar(50) DEFAULT '',
  `created_by` bigint(20) DEFAULT '0',
  `last_edited_by` bigint(20) DEFAULT '0',
  `conduct` varchar(10) DEFAULT '',
  `practice_id` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_patients`
--

INSERT INTO `tb_patients` (`id`, `full_name`, `dob`, `gender`, `relationship`, `ssn`, `martial_status`, `medical_record`, `employment`, `employer`, `reference`, `is_active`, `is_deleted`, `addr_street1`, `addr_street2`, `addr_city`, `addr_state`, `addr_country`, `addr_zip`, `email`, `phone_home`, `phone_work`, `phone_mobile`, `notifyemail`, `notify_phone`, `is_financial_resp`, `primary_care_phy`, `referring_phy`, `default_render_phy`, `default_service_loc`, `chart_num`, `emergency_contact`, `emergency_phone`, `created_by`, `last_edited_by`, `conduct`, `practice_id`, `created_at`, `updated_at`) VALUES
(1, 'Ananth B Sriram', '2018-08-01', 'Male', '', '456-65-4654', NULL, NULL, NULL, NULL, NULL, 1, 0, '1024', NULL, NULL, NULL, NULL, '06851', NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, NULL, '3', 'ANASRI00029', '', '', 0, 0, '', 0, '2018-08-20 04:43:57', '2018-08-20 04:44:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_physicians`
--

CREATE TABLE `tb_physicians` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT '',
  `phone_home` varchar(20) DEFAULT '',
  `phone_work` varchar(20) DEFAULT '',
  `phone_mobile` varchar(20) DEFAULT '',
  `phy_email` varchar(100) DEFAULT '',
  `dob` date DEFAULT NULL,
  `individual_npi` varchar(50) DEFAULT '',
  `ssno` varchar(50) DEFAULT '',
  `speciality` varchar(50) DEFAULT '',
  `address` text,
  `fax` varchar(20) DEFAULT '',
  `notes` text,
  `created_by` bigint(20) DEFAULT '0',
  `practice_id` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_physicians`
--

INSERT INTO `tb_physicians` (`id`, `name`, `phone_home`, `phone_work`, `phone_mobile`, `phy_email`, `dob`, `individual_npi`, `ssno`, `speciality`, `address`, `fax`, `notes`, `created_by`, `practice_id`, `created_at`, `updated_at`) VALUES
(1, 'Sriram', '', '', '', '', NULL, '4564613218', '', '', '123 Valley Ave,\r\nVienna MO 06851', '', NULL, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_place_of_services`
--

CREATE TABLE `tb_place_of_services` (
  `id` bigint(20) NOT NULL,
  `code` varchar(20) DEFAULT '',
  `name` varchar(50) DEFAULT '',
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_policies`
--

CREATE TABLE `tb_policies` (
  `id` bigint(20) NOT NULL,
  `case_id` bigint(20) DEFAULT '0',
  `auth_num` varchar(50) DEFAULT '',
  `policy_entity` bigint(20) DEFAULT '0',
  `insurance_id` bigint(20) DEFAULT '0',
  `policy_holder` bigint(20) DEFAULT '0',
  `relationship_to_insured` varchar(20) DEFAULT '',
  `relation_id` bigint(20) DEFAULT '0',
  `policy_num` varchar(50) DEFAULT '',
  `group_num` varchar(50) DEFAULT '',
  `group_name` varchar(50) DEFAULT '',
  `claim_num` varchar(50) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `assigned` bigint(20) DEFAULT '0',
  `crossover_claim` bigint(20) DEFAULT '0',
  `deductible_met` bigint(20) DEFAULT '0',
  `annual_deductible` bigint(20) DEFAULT '0',
  `co_payment` bigint(20) DEFAULT '0',
  `treatment_auth` varchar(50) DEFAULT '',
  `doc_ctrl_num` varchar(50) DEFAULT '',
  `ins_class_a` bigint(20) DEFAULT '0',
  `ins_class_b` bigint(20) DEFAULT '0',
  `ins_class_c` bigint(20) DEFAULT '0',
  `ins_class_d` bigint(20) DEFAULT '0',
  `ins_class_e` bigint(20) DEFAULT '0',
  `ins_class_f` bigint(20) DEFAULT '0',
  `ins_class_g` bigint(20) DEFAULT '0',
  `ins_class_h` bigint(20) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_policies`
--

INSERT INTO `tb_policies` (`id`, `case_id`, `auth_num`, `policy_entity`, `insurance_id`, `policy_holder`, `relationship_to_insured`, `relation_id`, `policy_num`, `group_num`, `group_name`, `claim_num`, `start_date`, `end_date`, `assigned`, `crossover_claim`, `deductible_met`, `annual_deductible`, `co_payment`, `treatment_auth`, `doc_ctrl_num`, `ins_class_a`, `ins_class_b`, `ins_class_c`, `ins_class_d`, `ins_class_e`, `ins_class_f`, `ins_class_g`, `ins_class_h`, `updated_at`, `created_at`) VALUES
(1, 1, '45645646', 1, 1, 1, 'Self', 0, '84656545', NULL, '', NULL, '2018-08-01 00:00:00', '2018-08-31 00:00:00', 0, 0, 0, 0, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '2018-08-20 04:46:46', '2018-08-20 04:46:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_practices`
--

CREATE TABLE `tb_practices` (
  `id` bigint(20) NOT NULL,
  `name` text,
  `speciality` varchar(100) DEFAULT '',
  `dob` varchar(20) DEFAULT '',
  `ssn` varchar(20) DEFAULT '',
  `ein` varchar(15) DEFAULT '',
  `address` text,
  `phone_home` varchar(20) DEFAULT '',
  `phone_work` varchar(20) DEFAULT '',
  `phone_mobile` varchar(20) DEFAULT '',
  `dept` text,
  `fax` varchar(20) DEFAULT '',
  `npi` varchar(20) DEFAULT '',
  `notes` text,
  `is_active` int(1) DEFAULT '1',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_practices`
--

INSERT INTO `tb_practices` (`id`, `name`, `speciality`, `dob`, `ssn`, `ein`, `address`, `phone_home`, `phone_work`, `phone_mobile`, `dept`, `fax`, `npi`, `notes`, `is_active`, `updated_at`, `created_at`) VALUES
(1, 'Forming Friendships LLC', 'ABA', '2018-08-01', '84951654', '54651219', NULL, '', '', '', NULL, '', '115599589', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_relation_ships`
--

CREATE TABLE `tb_relation_ships` (
  `id` bigint(20) NOT NULL,
  `full_name` varchar(50) DEFAULT '',
  `dob` datetime DEFAULT NULL,
  `gender` varchar(10) DEFAULT '',
  `relationship` varchar(10) DEFAULT '',
  `ssn` varchar(20) DEFAULT '',
  `martial_status` varchar(20) DEFAULT '',
  `medical_record` varchar(20) DEFAULT '',
  `employment` varchar(20) NOT NULL,
  `employer` varchar(50) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `is_active` int(1) DEFAULT '1',
  `is_deleted` int(1) DEFAULT '0',
  `addr_street1` varchar(50) DEFAULT '',
  `addr_street2` varchar(50) DEFAULT '',
  `addr_city` varchar(20) DEFAULT '',
  `addr_state` varchar(20) DEFAULT '',
  `addr_country` varchar(20) DEFAULT '',
  `addr_zip` varchar(10) DEFAULT '',
  `email` varchar(125) DEFAULT '',
  `phone_home` varchar(50) DEFAULT '',
  `phone_work` varchar(50) DEFAULT '',
  `phone_mobile` varchar(50) DEFAULT '',
  `notify_email` int(1) DEFAULT '0',
  `notify_phone` int(11) DEFAULT '0',
  `primary_care_phy` bigint(20) DEFAULT '0',
  `referring_phy` bigint(20) DEFAULT '0',
  `default_render_phy` bigint(20) DEFAULT '0',
  `default_service_loc` varchar(20) DEFAULT '',
  `chart_num` varchar(20) DEFAULT '',
  `emergency_contact` varchar(50) DEFAULT '',
  `emergency_phone` varchar(50) DEFAULT '',
  `created_by` bigint(20) DEFAULT '0',
  `last_edited_by` bigint(20) DEFAULT '0',
  `conduct` varchar(10) DEFAULT '',
  `referral_source` varchar(50) DEFAULT '',
  `practice_id` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_service_locations`
--

CREATE TABLE `tb_service_locations` (
  `id` bigint(20) NOT NULL,
  `internal_name` varchar(100) DEFAULT '',
  `npi` varchar(100) DEFAULT '',
  `override_ein` bigint(20) DEFAULT '0',
  `time_zone` varchar(100) DEFAULT '',
  `legacy_num_type` varchar(100) DEFAULT '',
  `legacy_num` varchar(100) DEFAULT '',
  `billing_name` varchar(100) DEFAULT '',
  `address` text,
  `city` varchar(30) DEFAULT '',
  `state` varchar(30) DEFAULT '',
  `zip` varchar(30) DEFAULT '',
  `phone` varchar(100) DEFAULT '',
  `fax` varchar(100) DEFAULT '',
  `place_of_service` varchar(100) DEFAULT '',
  `clia_num` varchar(100) DEFAULT '',
  `type_of_bill` varchar(100) DEFAULT '',
  `pay_to_name` varchar(100) DEFAULT '',
  `inst_address` text,
  `inst_phone` varchar(100) DEFAULT '',
  `inst_fax` varchar(100) DEFAULT '',
  `is_active` int(1) DEFAULT '1',
  `practice_id` bigint(20) DEFAULT '0',
  `created_by` bigint(20) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_statements`
--

CREATE TABLE `tb_statements` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT '',
  `patient_id` bigint(20) DEFAULT '0',
  `location` varchar(50) DEFAULT '',
  `created_on` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_testing_pagination`
--

CREATE TABLE `tb_testing_pagination` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(50) DEFAULT '',
  `last_name` varchar(50) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `role` varchar(128) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email_id` varchar(128) NOT NULL,
  `phone_num` varchar(16) DEFAULT '',
  `is_activated` int(1) DEFAULT '0',
  `is_logged` int(1) DEFAULT '0',
  `token` varchar(128) DEFAULT '',
  `reset_token` varchar(128) DEFAULT '',
  `oauth_token` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `practice_id` bigint(20) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`id`, `first_name`, `last_name`, `username`, `role`, `password`, `email_id`, `phone_num`, `is_activated`, `is_logged`, `token`, `reset_token`, `oauth_token`, `created_at`, `updated_at`, `practice_id`) VALUES
(1, 'Anand', 'B', 'anand', 'Admin', '3cf18c55a93ead38ff48c9ff05d046725986c132', 'anand@curismed.com', '8344435038', 1, 0, '', '', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRiN2E4NTA1YjRjZjdiYzdiYmI3Zjk3MGQzMjIxNjBiMDA3MDI0ZmMzOTViNDhmYWZhMjhmMWFkNjc1YjJlNDJlY2IxZTE4ZmIxNTVhNzhjIn0.eyJhdWQiOiIxIiwianRpIjoiNGI3YTg1MDViNGNmN2JjN2JiYjdmOTcwZDMyMjE2MGIwMDcwMjRmYzM5NWI0OGZhZmEyOGYxYWQ2NzViMmU0MmVjYjFlMThmYjE1NWE3OGMiLCJpYXQiOjE1MzUwMDUxOTAsIm5iZiI6MTUzNTAwNTE5MCwiZXhwIjoxNTY2NTQxMTkwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.c28JEzV4H6vFhC7VShaZM7ofXvck3t1MpbPlBlbWrvEZ3ax6thikmV-2EBPUwGRHnp9pENj5h9Kr9oKj2K_9NYgQ-htQinOZifGEXDRll-yWsmFPrAy4oK66VJ9dG2ibhSSt48T3R70onbdPK24Kpdu1xGg91nubnepWx98rfGXCHMcMYv4VF2jCTWpYATg1HXTecLeYnm1_gfYKFAJhnJ5Mj-DC5M9DlXzPVWIIxiJX5lINNiLuq_IWNgucbuGrxNfFD8XrclVvythwPqd0xqqGQaCOMv1k8J7BrPYulKlIuawlADZf5D239462cWjVdlvFZDKHMRp-iQp14nPcsZQWwr33P3B2DnoqGqv-Li60RONnbhGcZhIbLrMzk80Pp0m9bNpj6CmZI8dDFgc7WWTU3IdBZalXEWx58JqF2Hct1S9Ra3lJ4fkUT7XlgmToUzX9Y8Ns1pROxc_TrVjTz5JFeASPIKOpPmbdq1pZrB6eigUkgN8cWLJV7mHCZqy7PgF9sEmtbDQVh_LQLA3NSwVYq_5AwDXRR3cnk6yXg1-Cjh-io6b7PMyShbJVWkkQiBzFEndU9IzQYA701vciW1kG7QGnywO-iqGKywEWMy1irAtN0QfCH4gukNyf5zX6m_NBOvykEE4UXF2ovZc4QNmJbUPQApB7LbuNIvGbD_c', '2018-08-23 00:49:22', '2018-08-23 00:49:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_users_session`
--

CREATE TABLE `tb_users_session` (
  `id` bigint(20) NOT NULL,
  `session_token` varchar(128) DEFAULT '',
  `user_id` bigint(20) DEFAULT '0',
  `date_login` datetime DEFAULT NULL,
  `date_last_accessed` datetime DEFAULT NULL,
  `is_active` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_users_session`
--

INSERT INTO `tb_users_session` (`id`, `session_token`, `user_id`, `date_login`, `date_last_accessed`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'a456112e-a69c-11e8-8c66-b4b686ec1691', 1, '2018-08-23 06:20:05', '2018-08-23 06:20:05', 0, '2018-08-23 00:50:05', '2018-08-23 01:53:29'),
(2, '7fa07dd4-a6a5-11e8-8c66-b4b686ec1691', 1, '2018-08-23 07:23:29', '2018-08-23 07:23:29', 1, '2018-08-23 01:53:29', '2018-08-23 01:53:29'),
(3, '01621c01-a6a6-11e8-8c66-b4b686ec1691', 1, '2018-08-23 07:27:07', '2018-08-23 07:27:07', 1, '2018-08-23 01:57:07', '2018-08-23 01:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_acl_list`
--

CREATE TABLE `tb_user_acl_list` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT '0',
  `acl_key` varchar(128) DEFAULT '',
  `is_create` int(1) DEFAULT '0',
  `is_edit` int(1) DEFAULT '0',
  `is_delete` int(1) DEFAULT '0',
  `is_view` int(1) DEFAULT '0',
  `is_display` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_zipcodes`
--

CREATE TABLE `tb_zipcodes` (
  `id` bigint(20) NOT NULL,
  `state` varchar(10) DEFAULT '',
  `city` varchar(100) DEFAULT '',
  `zip` varchar(10) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_zipcodes`
--

INSERT INTO `tb_zipcodes` (`id`, `state`, `city`, `zip`, `created_at`, `updated_at`) VALUES
(1, 'MA', 'Mussourie', '06851', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `remember_token` varchar(100) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'anand', 'anand@curismed.com', '$2y$10$1SpgJyteGSQd3t/WSZQqWOyTqBRTb/sDB9G6UZxk5onmMglzdx4ni', '', '2018-08-23 00:49:49', '2018-08-23 00:49:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tb_acl_list`
--
ALTER TABLE `tb_acl_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_activity`
--
ALTER TABLE `tb_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_alerts`
--
ALTER TABLE `tb_alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_appointments`
--
ALTER TABLE `tb_appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_auth`
--
ALTER TABLE `tb_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_billing_code`
--
ALTER TABLE `tb_billing_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_cases`
--
ALTER TABLE `tb_cases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_claims`
--
ALTER TABLE `tb_claims`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_claims_history`
--
ALTER TABLE `tb_claims_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_claims_submission`
--
ALTER TABLE `tb_claims_submission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_code_cpt`
--
ALTER TABLE `tb_code_cpt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_code_icd`
--
ALTER TABLE `tb_code_icd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_comments`
--
ALTER TABLE `tb_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_crm`
--
ALTER TABLE `tb_crm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_deposits`
--
ALTER TABLE `tb_deposits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_deposit_details`
--
ALTER TABLE `tb_deposit_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_details`
--
ALTER TABLE `tb_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_disposition`
--
ALTER TABLE `tb_disposition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_documents`
--
ALTER TABLE `tb_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_edi`
--
ALTER TABLE `tb_edi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_guarantor`
--
ALTER TABLE `tb_guarantor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_history`
--
ALTER TABLE `tb_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_insurances`
--
ALTER TABLE `tb_insurances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_locations`
--
ALTER TABLE `tb_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_patients`
--
ALTER TABLE `tb_patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_physicians`
--
ALTER TABLE `tb_physicians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_place_of_services`
--
ALTER TABLE `tb_place_of_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_policies`
--
ALTER TABLE `tb_policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_practices`
--
ALTER TABLE `tb_practices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_relation_ships`
--
ALTER TABLE `tb_relation_ships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_service_locations`
--
ALTER TABLE `tb_service_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_statements`
--
ALTER TABLE `tb_statements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_testing_pagination`
--
ALTER TABLE `tb_testing_pagination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_users_session`
--
ALTER TABLE `tb_users_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user_acl_list`
--
ALTER TABLE `tb_user_acl_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_zipcodes`
--
ALTER TABLE `tb_zipcodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_acl_list`
--
ALTER TABLE `tb_acl_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_activity`
--
ALTER TABLE `tb_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_alerts`
--
ALTER TABLE `tb_alerts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_appointments`
--
ALTER TABLE `tb_appointments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tb_auth`
--
ALTER TABLE `tb_auth`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_billing_code`
--
ALTER TABLE `tb_billing_code`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_cases`
--
ALTER TABLE `tb_cases`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_claims`
--
ALTER TABLE `tb_claims`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_claims_history`
--
ALTER TABLE `tb_claims_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_claims_submission`
--
ALTER TABLE `tb_claims_submission`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_code_cpt`
--
ALTER TABLE `tb_code_cpt`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_code_icd`
--
ALTER TABLE `tb_code_icd`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_comments`
--
ALTER TABLE `tb_comments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_crm`
--
ALTER TABLE `tb_crm`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_deposits`
--
ALTER TABLE `tb_deposits`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_deposit_details`
--
ALTER TABLE `tb_deposit_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_details`
--
ALTER TABLE `tb_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_disposition`
--
ALTER TABLE `tb_disposition`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_documents`
--
ALTER TABLE `tb_documents`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_edi`
--
ALTER TABLE `tb_edi`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_guarantor`
--
ALTER TABLE `tb_guarantor`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_history`
--
ALTER TABLE `tb_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_insurances`
--
ALTER TABLE `tb_insurances`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_locations`
--
ALTER TABLE `tb_locations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_patients`
--
ALTER TABLE `tb_patients`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_physicians`
--
ALTER TABLE `tb_physicians`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_place_of_services`
--
ALTER TABLE `tb_place_of_services`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_policies`
--
ALTER TABLE `tb_policies`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_practices`
--
ALTER TABLE `tb_practices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_relation_ships`
--
ALTER TABLE `tb_relation_ships`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_service_locations`
--
ALTER TABLE `tb_service_locations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_statements`
--
ALTER TABLE `tb_statements`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_testing_pagination`
--
ALTER TABLE `tb_testing_pagination`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_users_session`
--
ALTER TABLE `tb_users_session`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_user_acl_list`
--
ALTER TABLE `tb_user_acl_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_zipcodes`
--
ALTER TABLE `tb_zipcodes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
