-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 05, 2018 at 07:54 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.7-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_curismed_aba1`
--

-- --------------------------------------------------------

--
-- Table structure for table `disposition`
--

CREATE TABLE `disposition` (
  `dispoID` int(11) NOT NULL,
  `DispoName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `historyID` int(11) NOT NULL,
  `claimID` int(10) NOT NULL,
  `dispo` int(10) NOT NULL,
  `comments` varchar(10000) NOT NULL,
  `workedDate` datetime NOT NULL,
  `user` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mapdispo`
--

CREATE TABLE `mapdispo` (
  `mapID` int(11) NOT NULL,
  `dispoID` int(10) NOT NULL,
  `tyreID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2018_08_02_110108_create_employees_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_activity`
--

CREATE TABLE `m_activity` (
  `activityID` int(11) NOT NULL,
  `caseID` int(10) NOT NULL,
  `authNo` varchar(20) NOT NULL,
  `activityName` varchar(50) NOT NULL,
  `activityCPT` varchar(20) NOT NULL,
  `activityMod1` varchar(10) NOT NULL,
  `activityMod2` varchar(10) NOT NULL,
  `activityMod3` varchar(10) NOT NULL,
  `activityMod4` varchar(10) NOT NULL,
  `activityBilledPer` varchar(10) NOT NULL,
  `activityBilledPerTime` varchar(10) NOT NULL,
  `totalAuth` int(10) NOT NULL,
  `authType` varchar(50) NOT NULL,
  `remainAuth` int(10) NOT NULL,
  `activityRates` decimal(10,2) NOT NULL,
  `activityMax1` varchar(10) NOT NULL,
  `activityMax2` varchar(10) NOT NULL,
  `activityMax3` varchar(10) NOT NULL,
  `activityPer1` varchar(10) NOT NULL,
  `activityPer2` varchar(10) NOT NULL,
  `activityPer3` varchar(10) NOT NULL,
  `activityIs1` varchar(10) NOT NULL,
  `activityIs2` varchar(10) NOT NULL,
  `activityIs3` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_alerts`
--

CREATE TABLE `m_alerts` (
  `alert_ID` int(11) NOT NULL,
  `alert_desc` varchar(1000) NOT NULL,
  `alert_patientID` int(10) NOT NULL,
  `alerts_EditPatient` int(1) NOT NULL,
  `alerts_ScheduleApp` int(1) NOT NULL,
  `alerts_Charge` int(1) NOT NULL,
  `alerts_ViewClaims` int(1) NOT NULL,
  `alerts_Deposits` int(1) NOT NULL,
  `alerts_Statements` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_appointments`
--

CREATE TABLE `m_appointments` (
  `appID` int(11) NOT NULL,
  `appStartDt` datetime NOT NULL,
  `appEndDt` datetime NOT NULL,
  `appPatientName` varchar(50) NOT NULL,
  `appAuth` varchar(20) NOT NULL,
  `appActivity` varchar(50) NOT NULL,
  `appLocation` varchar(20) NOT NULL,
  `appProvider` varchar(50) NOT NULL,
  `appDesc` varchar(200) NOT NULL,
  `appStatus` varchar(20) NOT NULL,
  `appPatientID` varchar(10) NOT NULL,
  `appProviderID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_auth`
--

CREATE TABLE `m_auth` (
  `authID` int(11) NOT NULL,
  `patientID` int(10) NOT NULL,
  `caseID` int(10) NOT NULL,
  `policyEntity` int(5) NOT NULL,
  `tos` varchar(50) NOT NULL,
  `authNo` varchar(100) NOT NULL,
  `startDt` date NOT NULL,
  `endDt` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_billingCode`
--

CREATE TABLE `m_billingCode` (
  `billingCdID` int(11) NOT NULL,
  `billingCd` varchar(10) NOT NULL,
  `billingCdDesc` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_cases`
--

CREATE TABLE `m_cases` (
  `caseID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `isClosed` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `allowPrint` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `caseDt` datetime NOT NULL,
  `caseChartNo` varchar(20) NOT NULL,
  `globalCoverageUntil` varchar(50) NOT NULL,
  `employer` varchar(50) NOT NULL,
  `empstatus` varchar(50) NOT NULL,
  `retirementDate` date NOT NULL,
  `location` varchar(50) NOT NULL,
  `workPhone` varchar(50) NOT NULL,
  `extension` varchar(50) NOT NULL,
  `studStatus` varchar(50) NOT NULL,
  `guarantor` varchar(50) NOT NULL,
  `maritalStatus` varchar(50) NOT NULL,
  `assignedProvider` int(11) NOT NULL,
  `referringProvider` int(11) NOT NULL,
  `supervisingProvider` int(11) NOT NULL,
  `operatingProvider` int(11) NOT NULL,
  `otherProvider` int(11) NOT NULL,
  `referralSource` varchar(50) NOT NULL,
  `attorney` varchar(50) NOT NULL,
  `facility` varchar(50) NOT NULL,
  `caseBillingCode` varchar(50) NOT NULL,
  `priceCode` varchar(50) NOT NULL,
  `otherArrangements` varchar(50) NOT NULL,
  `authorizedThrough` varchar(50) NOT NULL,
  `authNo` varchar(50) NOT NULL,
  `lastVisit` datetime NOT NULL,
  `noOfVisits` int(11) NOT NULL,
  `totalVisits` int(10) NOT NULL,
  `authID` varchar(50) NOT NULL,
  `lastVisitNo` varchar(50) NOT NULL,
  `principalDiag` varchar(100) NOT NULL,
  `defDiag2` varchar(100) NOT NULL,
  `defDiag3` varchar(100) NOT NULL,
  `defDiag4` varchar(100) NOT NULL,
  `poa1` varchar(100) NOT NULL,
  `poa2` varchar(100) NOT NULL,
  `poa3` varchar(100) NOT NULL,
  `poa4` varchar(100) NOT NULL,
  `allergiesNotes` varchar(1000) NOT NULL,
  `ediNotes` varchar(1000) NOT NULL,
  `repTypeCode` varchar(50) NOT NULL,
  `attachCtrlNo` varchar(50) NOT NULL,
  `repTransCode` varchar(50) NOT NULL,
  `injuryDt` date NOT NULL,
  `illnessIndicator` varchar(50) NOT NULL,
  `firstConsultDate` date NOT NULL,
  `similarSymptoms` varchar(100) NOT NULL,
  `sameSymptom` int(11) NOT NULL,
  `empRelated` int(11) NOT NULL,
  `emergency` int(11) NOT NULL,
  `relatedTo` varchar(50) NOT NULL,
  `relatedState` varchar(50) NOT NULL,
  `nature` varchar(50) NOT NULL,
  `lastXRay` date NOT NULL,
  `deathStat` varchar(50) NOT NULL,
  `unableToWorkFrm` date NOT NULL,
  `unableToWorkTo` date NOT NULL,
  `totalDisabFrm` date NOT NULL,
  `totDisabTo` date NOT NULL,
  `partDisabFrm` date NOT NULL,
  `partDisabTo` date NOT NULL,
  `hospFrm` date NOT NULL,
  `hospTo` date NOT NULL,
  `returnWorkIndicator` varchar(50) NOT NULL,
  `disabilityPercent` varchar(50) NOT NULL,
  `lastWorkDate` date NOT NULL,
  `pregnant` int(11) NOT NULL,
  `estimatedDOB` date NOT NULL,
  `dateAssumedCare` date NOT NULL,
  `dateRelinquishedCare` date NOT NULL,
  `outsideLab` int(11) NOT NULL,
  `labCharges` int(11) NOT NULL,
  `localUseA` varchar(20) NOT NULL,
  `localUseB` varchar(20) NOT NULL,
  `indicator` varchar(50) NOT NULL,
  `refDate` date NOT NULL,
  `prescripDate` date NOT NULL,
  `priorAuthNo` varchar(50) NOT NULL,
  `extra1` varchar(50) NOT NULL,
  `extra2` varchar(50) NOT NULL,
  `extra3` varchar(50) NOT NULL,
  `extra4` varchar(50) NOT NULL,
  `outsideProvider` varchar(50) NOT NULL,
  `dateLastSeen` date NOT NULL,
  `epsdt` int(11) NOT NULL,
  `resubmissionDt` date NOT NULL,
  `familyPlanning` int(11) NOT NULL,
  `originalRefNo` varchar(50) NOT NULL,
  `serviceAuth` varchar(50) NOT NULL,
  `nonAvailIndicator` varchar(50) NOT NULL,
  `branchService` varchar(50) NOT NULL,
  `sponsorStat` varchar(50) NOT NULL,
  `splProgram` varchar(50) NOT NULL,
  `sponsorGuide` varchar(50) NOT NULL,
  `effStartDt` date NOT NULL,
  `effEndDt` date NOT NULL,
  `carePlanOversight` varchar(50) NOT NULL,
  `hospiceNo` varchar(50) NOT NULL,
  `CLIANo` varchar(50) NOT NULL,
  `mamoCertification` varchar(50) NOT NULL,
  `refAccessNo` varchar(50) NOT NULL,
  `demoCode` varchar(50) NOT NULL,
  `assignIndicator` varchar(50) NOT NULL,
  `insuranceTypeCode` varchar(50) NOT NULL,
  `timelyFillingIndicator` varchar(50) NOT NULL,
  `EPSDTRefCode1` varchar(50) NOT NULL,
  `EPSDTRefCode2` varchar(50) NOT NULL,
  `EPSDTRefCode3` varchar(50) NOT NULL,
  `homebound` int(11) NOT NULL,
  `IDENo` varchar(50) NOT NULL,
  `conditionIndicator` varchar(50) NOT NULL,
  `certCodeApplies` int(11) NOT NULL,
  `codeCategory` varchar(50) NOT NULL,
  `totalVisitRendered` varchar(50) NOT NULL,
  `totalVisitProjected` varchar(50) NOT NULL,
  `noOfVisitsHomeHealth` int(11) NOT NULL,
  `noOfUnits` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `disciplineTypeCode` varchar(50) NOT NULL,
  `deliveryPatternCode` varchar(50) NOT NULL,
  `deliveryTimeCode` varchar(50) NOT NULL,
  `freqPeriod` int(11) NOT NULL,
  `freqCount` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `isCash` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_claims`
--

CREATE TABLE `m_claims` (
  `claimID` int(11) NOT NULL,
  `caseID` int(11) NOT NULL,
  `appID` int(11) NOT NULL,
  `authNo` varchar(40) NOT NULL,
  `fromDt` date NOT NULL,
  `toDt` date NOT NULL,
  `proced` varchar(50) NOT NULL,
  `mod1` varchar(50) NOT NULL,
  `units` int(11) NOT NULL,
  `charge` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `diag1` varchar(50) NOT NULL,
  `diag2` varchar(50) NOT NULL,
  `diag3` varchar(50) NOT NULL,
  `diag4` varchar(50) NOT NULL,
  `applied` int(11) DEFAULT NULL,
  `notes` varchar(200) DEFAULT NULL,
  `refCode` varchar(15) DEFAULT NULL,
  `placeOfService` varchar(50) NOT NULL,
  `isPosted` int(11) NOT NULL,
  `isSent` int(11) NOT NULL,
  `isSecClaim` int(2) NOT NULL DEFAULT '0',
  `allowed` decimal(10,2) NOT NULL,
  `paid` decimal(10,2) NOT NULL,
  `adjustment` decimal(10,2) NOT NULL,
  `adjustCode` varchar(100) DEFAULT NULL,
  `claimBalance` decimal(10,2) NOT NULL,
  `claimBalancePat` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reason` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `copay` decimal(10,2) DEFAULT NULL,
  `deductible` decimal(10,2) DEFAULT NULL,
  `coins` decimal(10,2) DEFAULT NULL,
  `primaryCarePhy` int(11) DEFAULT NULL,
  `serviceLocID` int(11) DEFAULT NULL,
  `claimStatus` varchar(50) DEFAULT NULL,
  `insuranceID` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `SentBatchNumber` int(11) DEFAULT NULL,
  `ClaimNumber` varchar(20) NOT NULL,
  `created_At` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_claimshistory`
--

CREATE TABLE `m_claimshistory` (
  `historyID` int(11) NOT NULL,
  `claimID` int(10) NOT NULL,
  `pou` varchar(50) NOT NULL,
  `purpose` varchar(50) NOT NULL,
  `notes` varchar(500) NOT NULL,
  `created_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_claimssubmission`
--

CREATE TABLE `m_claimssubmission` (
  `id` int(11) NOT NULL,
  `patientID` int(10) NOT NULL,
  `claimID` int(11) NOT NULL,
  `ClaimNumber` varchar(11) NOT NULL,
  `submitted_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_codecpt`
--

CREATE TABLE `m_codecpt` (
  `id` int(11) NOT NULL,
  `cptCode` varchar(20) NOT NULL,
  `description` varchar(500) NOT NULL,
  `charge` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_codeicd`
--

CREATE TABLE `m_codeicd` (
  `id` int(11) NOT NULL,
  `icdCode` varchar(20) NOT NULL,
  `dxCode` varchar(50) NOT NULL,
  `longDescription` varchar(500) NOT NULL,
  `shortDescription` varchar(300) NOT NULL,
  `codeType` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_comments`
--

CREATE TABLE `m_comments` (
  `commentID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `commentBy` int(11) NOT NULL,
  `commentText` varchar(500) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_crm`
--

CREATE TABLE `m_crm` (
  `id` int(11) NOT NULL,
  `claimID` int(50) NOT NULL,
  `comments` varchar(5000) NOT NULL,
  `dispo` varchar(20) NOT NULL,
  `followupDate` date NOT NULL,
  `workedDate` date NOT NULL,
  `tyreID` int(10) NOT NULL,
  `userID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_depositdetails`
--

CREATE TABLE `m_depositdetails` (
  `depositDetailID` int(11) NOT NULL,
  `depositID` int(11) NOT NULL,
  `claimID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `amountAdjusted` decimal(10,2) NOT NULL,
  `adjustment` decimal(10,2) NOT NULL,
  `claimBalanceIns` decimal(10,2) NOT NULL,
  `claimBalancePat` decimal(10,2) NOT NULL,
  `isdeleted` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `adjustmentDetails` varchar(100) NOT NULL,
  `adjustmentNotes` varchar(50) NOT NULL,
  `adjustmentCode` varchar(50) NOT NULL,
  `created_At` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_deposits`
--

CREATE TABLE `m_deposits` (
  `depositID` int(11) NOT NULL,
  `depositDate` date NOT NULL,
  `chequeNo` varchar(50) NOT NULL,
  `chequeDate` date NOT NULL,
  `payorID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `payorType` varchar(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `paymentType` varchar(10) NOT NULL,
  `notes` varchar(250) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `created_At` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_details`
--

CREATE TABLE `m_details` (
  `detail` varchar(20) NOT NULL,
  `detailvalue1` int(11) NOT NULL,
  `detailvalue2` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_details`
--

INSERT INTO `m_details` (`detail`, `detailvalue1`, `detailvalue2`, `created_at`, `updated_at`) VALUES
('ChartNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00'),
('BatchNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00'),
('ClaimNumber', 1, '', '2018-08-01 00:00:00', '2018-08-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_documents`
--

CREATE TABLE `m_documents` (
  `docID` int(11) NOT NULL,
  `docName` varchar(1000) NOT NULL,
  `docType` varchar(10) NOT NULL,
  `docPath` varchar(1000) NOT NULL,
  `status` varchar(50) NOT NULL,
  `notes` varchar(1000) NOT NULL,
  `patientID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `CreatedOn` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_edi`
--

CREATE TABLE `m_edi` (
  `ediID` int(11) NOT NULL,
  `ediInvoiceNo` varchar(50) NOT NULL,
  `ediLocation` varchar(100) NOT NULL,
  `ediCreatedOn` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_employer`
--

CREATE TABLE `m_employer` (
  `employerID` int(11) NOT NULL,
  `employerName` varchar(100) NOT NULL,
  `employerAddr` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_guarantor`
--

CREATE TABLE `m_guarantor` (
  `guarantorID` int(11) NOT NULL,
  `guarantorName` varchar(100) NOT NULL,
  `guarantorDOB` date NOT NULL,
  `guarantorStreet` varchar(500) NOT NULL,
  `guarantorCity` varchar(50) NOT NULL,
  `guarantorState` varchar(50) NOT NULL,
  `guarantorZip` varchar(20) NOT NULL,
  `guarantorPhoneHome` varchar(15) NOT NULL,
  `guarantorRelationship` varchar(100) NOT NULL,
  `patientID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_insurances`
--

CREATE TABLE `m_insurances` (
  `insuranceID` int(11) NOT NULL,
  `payerName` varchar(50) NOT NULL,
  `enr` int(11) NOT NULL,
  `typ` varchar(50) NOT NULL,
  `st` varchar(50) NOT NULL,
  `lob` varchar(50) NOT NULL,
  `rte` int(11) NOT NULL,
  `rts` int(11) NOT NULL,
  `era` int(11) NOT NULL,
  `sec` int(11) NOT NULL,
  `note` varchar(500) NOT NULL,
  `clearingHousePayorID` varchar(50) NOT NULL,
  `addr` varchar(500) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(10) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `abforbilling` varchar(20) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `isActive` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `created_At` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_insured`
--

CREATE TABLE `m_insured` (
  `insuredID` int(11) NOT NULL,
  `insured_fullName` varchar(50) NOT NULL,
  `insured_dob` date NOT NULL,
  `insured_gender` varchar(1) NOT NULL,
  `insured_relationship` varchar(10) NOT NULL,
  `insured_ssn` varchar(20) NOT NULL,
  `insured_martialStatus` varchar(20) NOT NULL,
  `insured_medicalRecord` varchar(20) NOT NULL,
  `insured_employment` varchar(20) NOT NULL,
  `insured_employer` varchar(50) NOT NULL,
  `insured_reference` varchar(50) NOT NULL,
  `insured_isActive` int(11) NOT NULL,
  `insured_isDeleted` int(11) NOT NULL,
  `insured_addrStreet1` varchar(50) NOT NULL,
  `insured_addrStreet2` varchar(50) NOT NULL,
  `insured_addrCity` varchar(20) NOT NULL,
  `insured_addrState` varchar(20) NOT NULL,
  `insured_addrCountry` varchar(20) NOT NULL,
  `insured_addrZip` varchar(10) NOT NULL,
  `insured_email` varchar(50) NOT NULL,
  `insured_phoneHome` varchar(50) NOT NULL,
  `insured_phoneWork` varchar(50) NOT NULL,
  `insured_phoneMobile` varchar(50) NOT NULL,
  `insured_emergencyContact` varchar(50) NOT NULL,
  `insured_emergencyPhone` varchar(50) NOT NULL,
  `patientID` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_locations`
--

CREATE TABLE `m_locations` (
  `locID` int(11) NOT NULL,
  `patientID` int(3) NOT NULL,
  `home` int(2) NOT NULL,
  `office` int(2) NOT NULL,
  `school` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_logs`
--

CREATE TABLE `m_logs` (
  `logID` int(11) NOT NULL,
  `logModule` varchar(50) NOT NULL,
  `logDetail` varchar(1024) NOT NULL,
  `logAction` varchar(50) NOT NULL,
  `logRefID` varchar(20) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_modifiers`
--

CREATE TABLE `m_modifiers` (
  `ID` int(11) NOT NULL,
  `modifierCode` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_patients`
--

CREATE TABLE `m_patients` (
  `patientID` int(11) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `relationship` varchar(10) NOT NULL,
  `ssn` varchar(20) NOT NULL,
  `martialStatus` varchar(20) NOT NULL,
  `medicalRecord` varchar(20) NOT NULL,
  `employment` varchar(20) NOT NULL,
  `employer` varchar(50) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `isActive` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `addrStreet1` varchar(50) NOT NULL,
  `addrStreet2` varchar(50) NOT NULL,
  `addrCity` varchar(20) NOT NULL,
  `addrState` varchar(20) NOT NULL,
  `addrCountry` varchar(20) NOT NULL,
  `addrZip` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phoneHome` varchar(50) NOT NULL,
  `phoneWork` varchar(50) NOT NULL,
  `phoneMobile` varchar(50) NOT NULL,
  `notifyEmail` int(11) NOT NULL,
  `notifyPhone` int(11) NOT NULL,
  `IsFinancialResp` int(1) NOT NULL,
  `primaryCarePhy` int(11) NOT NULL,
  `referringPhy` int(11) NOT NULL,
  `defaultRenderPhy` int(11) NOT NULL,
  `defaultServiceLoc` varchar(20) NOT NULL,
  `chartNo` varchar(20) NOT NULL,
  `emergencyContact` varchar(50) NOT NULL,
  `emergencyPhone` varchar(50) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `lastEditedBy` int(11) NOT NULL,
  `conduct` varchar(10) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_physicians`
--

CREATE TABLE `m_physicians` (
  `physicianID` int(11) NOT NULL,
  `phyName` varchar(50) NOT NULL,
  `phoneHome` varchar(20) NOT NULL,
  `phoneWork` varchar(20) NOT NULL,
  `phoneMobile` varchar(20) NOT NULL,
  `phyEmail` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `individualNPI` varchar(50) NOT NULL,
  `ssno` varchar(50) NOT NULL,
  `speciality` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `notes` varchar(1000) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_placeofservices`
--

CREATE TABLE `m_placeofservices` (
  `placeID` int(11) NOT NULL,
  `placeCode` varchar(20) NOT NULL,
  `placeName` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_policies`
--

CREATE TABLE `m_policies` (
  `policyID` int(11) NOT NULL,
  `caseID` int(11) NOT NULL,
  `authNo` varchar(50) NOT NULL,
  `policyEntity` int(11) NOT NULL,
  `insuranceID` int(11) NOT NULL,
  `policyHolder` int(11) NOT NULL,
  `relationshipToInsured` varchar(20) NOT NULL,
  `relationID` int(11) NOT NULL,
  `policyNo` varchar(50) NOT NULL,
  `groupNo` varchar(50) NOT NULL,
  `groupName` varchar(50) NOT NULL,
  `claimNo` varchar(50) NOT NULL,
  `startDt` date NOT NULL,
  `endDt` date NOT NULL,
  `assigned` int(11) NOT NULL,
  `crossoverClaim` int(11) NOT NULL,
  `deductibleMet` int(11) NOT NULL,
  `annualDeductible` int(11) NOT NULL,
  `coPayment` int(11) NOT NULL,
  `treatmentAuth` varchar(50) NOT NULL,
  `docCtrlNo` varchar(50) NOT NULL,
  `insClassA` int(11) NOT NULL,
  `insClassB` int(11) NOT NULL,
  `insClassC` int(11) NOT NULL,
  `insClassD` int(11) NOT NULL,
  `insClassE` int(11) NOT NULL,
  `insClassF` int(11) NOT NULL,
  `insClassG` int(11) NOT NULL,
  `insClassH` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_practices`
--

CREATE TABLE `m_practices` (
  `practiceID` int(11) NOT NULL,
  `practiceName` varchar(1000) NOT NULL,
  `speciality` varchar(100) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `ssn` varchar(20) NOT NULL,
  `EIN` varchar(15) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `phoneHome` varchar(20) NOT NULL,
  `phoneWork` varchar(20) NOT NULL,
  `phoneMobile` varchar(20) NOT NULL,
  `dept` varchar(200) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `npi` varchar(20) NOT NULL,
  `notes` varchar(2000) NOT NULL,
  `isActive` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_procedure`
--

CREATE TABLE `m_procedure` (
  `procedureID` int(11) NOT NULL,
  `procedureCd` varchar(50) NOT NULL,
  `procedureDesc` varchar(500) DEFAULT NULL,
  `procedureCharge` float(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_relationships`
--

CREATE TABLE `m_relationships` (
  `relationID` int(11) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `relationship` varchar(10) NOT NULL,
  `ssn` varchar(20) NOT NULL,
  `martialStatus` varchar(20) NOT NULL,
  `medicalRecord` varchar(20) NOT NULL,
  `employment` varchar(20) NOT NULL,
  `employer` varchar(50) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `isActive` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `addrStreet1` varchar(50) NOT NULL,
  `addrStreet2` varchar(50) NOT NULL,
  `addrCity` varchar(20) NOT NULL,
  `addrState` varchar(20) NOT NULL,
  `addrCountry` varchar(20) NOT NULL,
  `addrZip` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phoneHome` varchar(50) NOT NULL,
  `phoneWork` varchar(50) NOT NULL,
  `phoneMobile` varchar(50) NOT NULL,
  `notifyEmail` int(11) NOT NULL,
  `notifyPhone` int(11) NOT NULL,
  `primaryCarePhy` int(11) NOT NULL,
  `referringPhy` int(11) NOT NULL,
  `defaultRenderPhy` int(11) NOT NULL,
  `defaultServiceLoc` varchar(20) NOT NULL,
  `chartNo` varchar(20) NOT NULL,
  `emergencyContact` varchar(50) NOT NULL,
  `emergencyPhone` varchar(50) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `lastEditedBy` int(11) NOT NULL,
  `conduct` varchar(10) NOT NULL,
  `referralSource` varchar(50) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_serviceLocations`
--

CREATE TABLE `m_serviceLocations` (
  `serviceLocID` int(11) NOT NULL,
  `internalName` varchar(100) NOT NULL,
  `NPI` varchar(100) NOT NULL,
  `overrideEIN` int(11) NOT NULL,
  `timeZone` varchar(100) NOT NULL,
  `legacyNoType` varchar(100) NOT NULL,
  `legacyNo` varchar(100) NOT NULL,
  `billingName` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `placeOfService` varchar(100) NOT NULL,
  `CLIANo` varchar(100) NOT NULL,
  `typeOfBill` varchar(100) NOT NULL,
  `payToName` varchar(100) NOT NULL,
  `instAddress` varchar(1000) NOT NULL,
  `instPhone` varchar(100) NOT NULL,
  `instFax` varchar(100) NOT NULL,
  `isActive` int(11) NOT NULL,
  `practiceID` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_statements`
--

CREATE TABLE `m_statements` (
  `statementID` int(11) NOT NULL,
  `statementName` varchar(50) NOT NULL,
  `patientID` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_taxonomycodes`
--

CREATE TABLE `m_taxonomycodes` (
  `ID` int(11) NOT NULL,
  `medicareCode` varchar(50) NOT NULL,
  `medicareProvider` varchar(200) NOT NULL,
  `taxonomy` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_users`
--

CREATE TABLE `m_users` (
  `userID` int(100) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `emailID` varchar(150) NOT NULL,
  `phoneNo` varchar(15) NOT NULL,
  `IsLogged` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_zipcodes`
--

CREATE TABLE `m_zipcodes` (
  `zipID` int(11) NOT NULL,
  `state` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_zones`
--

CREATE TABLE `m_zones` (
  `zoneID` int(11) NOT NULL,
  `zoneName` varchar(100) NOT NULL,
  `zoneDesc` varchar(100) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0359f544699d9e93c3c3c05c67bcf896b27b04f91edb2dc5fa7df7af778605b92995f61fe9f82da7', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:49:00', '2018-08-04 14:49:00', '2019-08-04 20:19:00'),
('10439690d383bf5cd8fd1d650032bf301f6f920cd4da19bb4facee9628923e5070e8744c44d91ff6', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:17:26', '2018-08-05 08:17:26', '2019-08-05 13:47:26'),
('2339dc8f7795ef2998bb4fa43869433293f6f4fd8464b9e041545316a8e01e44b08a8edcb9f94c6c', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:09:26', '2018-08-05 08:09:26', '2019-08-05 13:39:26'),
('31275d788581b04ad1487ef76530e289617ff3d5084de966b2a230e24d67269b1be741b5c8a1c8f5', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:13:46', '2018-08-05 08:13:46', '2019-08-05 13:43:46'),
('3713935e4f2c6054e29ea28d94e556bdeec5b368855c6e410937c1fc4415207ecb561f1938938d8c', 1, 1, 'MyApp', '[]', 0, '2018-08-04 13:23:35', '2018-08-04 13:23:35', '2019-08-04 18:53:35'),
('40adf24890b23c4c0b064552e87f1644ff7c3ef85d3f889519a565e5f5cab6b6becb7755133ab7e0', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:17:48', '2018-08-05 08:17:48', '2019-08-05 13:47:48'),
('511e7aef299e8d4d868c129e4df0d0bfd936497317d5fb2f55df3d0c333c41a2d81ac7cf1b4ae4c6', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:45:51', '2018-08-04 14:45:51', '2019-08-04 20:15:51'),
('5993d7264d65365042017beb02bea4dbad8afdb41455677619793e12593c41d02f5c9f26acd7ffbf', 1, 1, 'MyApp', '[]', 0, '2018-08-04 13:24:16', '2018-08-04 13:24:16', '2019-08-04 18:54:16'),
('655565a6a64df484742f32820264cbb25f3314d7a31e34cfe5f1dd6cc4e4dd994de0091fa79f0392', 2, 1, 'MyApp', '[]', 0, '2018-08-04 13:59:22', '2018-08-04 13:59:22', '2019-08-04 19:29:22'),
('6d6eef2ac97cc9fa89b75fd7c1749bf421e8ddcd4593ebbad441d9f275565480981f576cb33ea1ed', 1, 1, 'MyApp', '[]', 0, '2018-08-04 13:23:29', '2018-08-04 13:23:29', '2019-08-04 18:53:29'),
('6d93bc6ca687c9631c46c0de35b8fc0b8d1e0f3db24f0b4aa9bef53520c3e725f9c287dc2e32b827', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:49:10', '2018-08-04 14:49:10', '2019-08-04 20:19:10'),
('7515695fa003aa78221e93a67f3025a7748e5cb12a51a56b52cb2815a515fcf44a6b702e22c59d94', 4, 1, 'MyApp', '[]', 0, '2018-08-04 16:58:46', '2018-08-04 16:58:46', '2019-08-04 22:28:46'),
('7fb66bc9e7f7d8d41b7c9c085557b1cd77a1ae5b778fd7a09040ebb5c264558a9063665478dc89d1', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:11:46', '2018-08-05 08:11:46', '2019-08-05 13:41:46'),
('8086e06d85e79e77a84aa2a21baa07dd7c4b352bbc0cb5fb09b345c385fbb0db025838fec4caa57e', 1, 1, 'MyApp', '[]', 0, '2018-08-04 12:58:56', '2018-08-04 12:58:56', '2019-08-04 18:28:56'),
('83b4f059e23e828f088622d8ae2a04afa564db67b4330be1038dcbcb077bb81f7aa8333df9da5f51', 1, 1, 'MyApp', '[]', 0, '2018-08-04 13:26:52', '2018-08-04 13:26:52', '2019-08-04 18:56:52'),
('a7efc2f0df524ff4c733bbeca4ca1eacc48252360bf82e2d5e4db84a45dc8d53ae12691d0e77474b', 3, 1, 'MyApp', '[]', 0, '2018-08-05 08:16:55', '2018-08-05 08:16:55', '2019-08-05 13:46:55'),
('ac53db770d1f85151097180c2a060ccbadffe29935fa4128994422c4196aa2b30308f2670284f4f4', 3, 1, 'MyApp', '[]', 0, '2018-08-04 17:12:25', '2018-08-04 17:12:25', '2019-08-04 22:42:25'),
('b715eb16aeda52fa58d7d2ff5f6174c202f4494b6543242ece51e4c2c9d6615f05b88f842cb078f5', 2, 1, 'MyApp', '[]', 0, '2018-08-04 13:56:50', '2018-08-04 13:56:50', '2019-08-04 19:26:50'),
('c3f5687304a347e3f094efa4c336fa73d94e7988c7a1965eb2f242a314c38e78045fe82bce2e7e99', 2, 1, 'MyApp', '[]', 0, '2018-08-04 13:59:16', '2018-08-04 13:59:16', '2019-08-04 19:29:16'),
('d7a10a29542e8134ec6c469cba7de22bcb1be7d71c2b8332ca5c529937ab2a3613d260d62970e5ad', 3, 1, 'MyApp', '[]', 0, '2018-08-04 16:58:21', '2018-08-04 16:58:21', '2019-08-04 22:28:21'),
('db909d37ddc1f6ab18b74d02e85e7b8da93b1f55800dde6d58ca138597d714d7e3b26cd093eff32f', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:48:35', '2018-08-04 14:48:35', '2019-08-04 20:18:35'),
('f79753b6a356650ef4aeba5a80922940eb5e7f4856a9e98f1dda9ef1eefa26185fcb50118123b335', 5, 1, 'MyApp', '[]', 0, '2018-08-05 08:28:07', '2018-08-05 08:28:07', '2019-08-05 13:58:07'),
('f81a0dcb369742208084a40c85b6bb5245c1563c92b22a46699e2071248c15a10394bf7ec830c8c9', 2, 1, 'MyApp', '[]', 0, '2018-08-04 14:45:16', '2018-08-04 14:45:16', '2019-08-04 20:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '7X61XonRmaLjqaue2Y8O6hKLOHEmeuhVygqS6WHk', 'http://localhost', 1, 0, 0, '2018-08-04 11:58:00', '2018-08-04 11:58:00'),
(2, NULL, 'Laravel Password Grant Client', '3PF0nBejsgERULceggQZQuvQQEHCpLA1Gn2vqxTt', 'http://localhost', 0, 1, 0, '2018-08-04 11:58:00', '2018-08-04 11:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-08-04 11:58:00', '2018-08-04 11:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `role` varchar(128) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email_id` varchar(128) NOT NULL,
  `phone_num` varchar(16) NOT NULL,
  `is_logged` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`id`, `first_name`, `last_name`, `username`, `role`, `password`, `email_id`, `phone_num`, `is_logged`, `created_at`, `updated_at`) VALUES
(7, 'a', 'a', 'a', 'Admin', '76af7efae0d034d1e3335ed1b90f24b6cadf2bf1', 'a@a.com', '1234567890', 0, '2018-08-05 08:36:53', '2018-08-05 08:36:53'),
(8, 'b', 'b', 'b', 'Admin', '76af7efae0d034d1e3335ed1b90f24b6cadf2bf1', 'b@b1.com', '1234567890', 0, '2018-08-05 08:37:49', '2018-08-05 08:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'rish', 'rish@avr.com', '$2y$10$1WN0cW0Q0.wxhCkRpOeLR.TKo9HXDHSL5.ZRS1XgCAq6/YEP4SxPe', NULL, '2018-08-04 13:56:50', '2018-08-04 13:56:50'),
(3, 'a', 'a@a.com', '$2y$10$mEXRcBYhi0VAtXNGZgoV2uCF9cY84VRguqzVrXOPX1i4sCOr4FiIG', NULL, '2018-08-04 16:58:21', '2018-08-04 16:58:21'),
(4, 'b', 'b@b.com', '$2y$10$PfbNMARkySTirI/Ezqwe6eKFG3F2lrT9y6xSqSa9g2IegiYaFtzgq', NULL, '2018-08-04 16:58:46', '2018-08-04 16:58:46'),
(5, 'a', 'a.a@a.com', '$2y$10$PyPnpWrJaAoSJJLoiNyPjeZF0pVxJbWvst0N1kQTX5TR3QR2iIYq2', NULL, '2018-08-05 08:28:06', '2018-08-05 08:28:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `disposition`
--
ALTER TABLE `disposition`
  ADD PRIMARY KEY (`dispoID`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`historyID`);

--
-- Indexes for table `mapdispo`
--
ALTER TABLE `mapdispo`
  ADD PRIMARY KEY (`mapID`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_activity`
--
ALTER TABLE `m_activity`
  ADD PRIMARY KEY (`activityID`);

--
-- Indexes for table `m_alerts`
--
ALTER TABLE `m_alerts`
  ADD PRIMARY KEY (`alert_ID`);

--
-- Indexes for table `m_appointments`
--
ALTER TABLE `m_appointments`
  ADD PRIMARY KEY (`appID`);

--
-- Indexes for table `m_auth`
--
ALTER TABLE `m_auth`
  ADD PRIMARY KEY (`authID`),
  ADD UNIQUE KEY `authNo` (`authNo`);

--
-- Indexes for table `m_billingCode`
--
ALTER TABLE `m_billingCode`
  ADD PRIMARY KEY (`billingCdID`);

--
-- Indexes for table `m_cases`
--
ALTER TABLE `m_cases`
  ADD PRIMARY KEY (`caseID`);

--
-- Indexes for table `m_claims`
--
ALTER TABLE `m_claims`
  ADD PRIMARY KEY (`claimID`);

--
-- Indexes for table `m_claimshistory`
--
ALTER TABLE `m_claimshistory`
  ADD PRIMARY KEY (`historyID`);

--
-- Indexes for table `m_claimssubmission`
--
ALTER TABLE `m_claimssubmission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_codecpt`
--
ALTER TABLE `m_codecpt`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `m_codeicd`
--
ALTER TABLE `m_codeicd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `dxCode` (`dxCode`);

--
-- Indexes for table `m_comments`
--
ALTER TABLE `m_comments`
  ADD PRIMARY KEY (`commentID`);

--
-- Indexes for table `m_crm`
--
ALTER TABLE `m_crm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_depositdetails`
--
ALTER TABLE `m_depositdetails`
  ADD PRIMARY KEY (`depositDetailID`);

--
-- Indexes for table `m_deposits`
--
ALTER TABLE `m_deposits`
  ADD PRIMARY KEY (`depositID`);

--
-- Indexes for table `m_details`
--
ALTER TABLE `m_details`
  ADD PRIMARY KEY (`detail`);

--
-- Indexes for table `m_documents`
--
ALTER TABLE `m_documents`
  ADD PRIMARY KEY (`docID`);

--
-- Indexes for table `m_edi`
--
ALTER TABLE `m_edi`
  ADD PRIMARY KEY (`ediID`);

--
-- Indexes for table `m_employer`
--
ALTER TABLE `m_employer`
  ADD PRIMARY KEY (`employerID`);

--
-- Indexes for table `m_guarantor`
--
ALTER TABLE `m_guarantor`
  ADD PRIMARY KEY (`guarantorID`);

--
-- Indexes for table `m_insurances`
--
ALTER TABLE `m_insurances`
  ADD PRIMARY KEY (`insuranceID`);

--
-- Indexes for table `m_insured`
--
ALTER TABLE `m_insured`
  ADD PRIMARY KEY (`insuredID`);

--
-- Indexes for table `m_locations`
--
ALTER TABLE `m_locations`
  ADD PRIMARY KEY (`locID`);

--
-- Indexes for table `m_logs`
--
ALTER TABLE `m_logs`
  ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `m_modifiers`
--
ALTER TABLE `m_modifiers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `m_patients`
--
ALTER TABLE `m_patients`
  ADD PRIMARY KEY (`patientID`);

--
-- Indexes for table `m_physicians`
--
ALTER TABLE `m_physicians`
  ADD PRIMARY KEY (`physicianID`);

--
-- Indexes for table `m_placeofservices`
--
ALTER TABLE `m_placeofservices`
  ADD PRIMARY KEY (`placeID`);

--
-- Indexes for table `m_policies`
--
ALTER TABLE `m_policies`
  ADD PRIMARY KEY (`policyID`);

--
-- Indexes for table `m_practices`
--
ALTER TABLE `m_practices`
  ADD PRIMARY KEY (`practiceID`);

--
-- Indexes for table `m_procedure`
--
ALTER TABLE `m_procedure`
  ADD PRIMARY KEY (`procedureID`);

--
-- Indexes for table `m_relationships`
--
ALTER TABLE `m_relationships`
  ADD PRIMARY KEY (`relationID`);

--
-- Indexes for table `m_serviceLocations`
--
ALTER TABLE `m_serviceLocations`
  ADD PRIMARY KEY (`serviceLocID`);

--
-- Indexes for table `m_statements`
--
ALTER TABLE `m_statements`
  ADD PRIMARY KEY (`statementID`);

--
-- Indexes for table `m_taxonomycodes`
--
ALTER TABLE `m_taxonomycodes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `m_zipcodes`
--
ALTER TABLE `m_zipcodes`
  ADD PRIMARY KEY (`zipID`);

--
-- Indexes for table `m_zones`
--
ALTER TABLE `m_zones`
  ADD PRIMARY KEY (`zoneID`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `disposition`
--
ALTER TABLE `disposition`
  MODIFY `dispoID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `historyID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mapdispo`
--
ALTER TABLE `mapdispo`
  MODIFY `mapID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `m_activity`
--
ALTER TABLE `m_activity`
  MODIFY `activityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `m_alerts`
--
ALTER TABLE `m_alerts`
  MODIFY `alert_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_appointments`
--
ALTER TABLE `m_appointments`
  MODIFY `appID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_auth`
--
ALTER TABLE `m_auth`
  MODIFY `authID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `m_billingCode`
--
ALTER TABLE `m_billingCode`
  MODIFY `billingCdID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_cases`
--
ALTER TABLE `m_cases`
  MODIFY `caseID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `m_claims`
--
ALTER TABLE `m_claims`
  MODIFY `claimID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `m_claimshistory`
--
ALTER TABLE `m_claimshistory`
  MODIFY `historyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `m_claimssubmission`
--
ALTER TABLE `m_claimssubmission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `m_comments`
--
ALTER TABLE `m_comments`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_crm`
--
ALTER TABLE `m_crm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `m_depositdetails`
--
ALTER TABLE `m_depositdetails`
  MODIFY `depositDetailID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_deposits`
--
ALTER TABLE `m_deposits`
  MODIFY `depositID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_documents`
--
ALTER TABLE `m_documents`
  MODIFY `docID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_edi`
--
ALTER TABLE `m_edi`
  MODIFY `ediID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_employer`
--
ALTER TABLE `m_employer`
  MODIFY `employerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_guarantor`
--
ALTER TABLE `m_guarantor`
  MODIFY `guarantorID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_insurances`
--
ALTER TABLE `m_insurances`
  MODIFY `insuranceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT for table `m_insured`
--
ALTER TABLE `m_insured`
  MODIFY `insuredID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_locations`
--
ALTER TABLE `m_locations`
  MODIFY `locID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `m_logs`
--
ALTER TABLE `m_logs`
  MODIFY `logID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `m_modifiers`
--
ALTER TABLE `m_modifiers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `m_patients`
--
ALTER TABLE `m_patients`
  MODIFY `patientID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `m_physicians`
--
ALTER TABLE `m_physicians`
  MODIFY `physicianID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `m_placeofservices`
--
ALTER TABLE `m_placeofservices`
  MODIFY `placeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `m_policies`
--
ALTER TABLE `m_policies`
  MODIFY `policyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `m_practices`
--
ALTER TABLE `m_practices`
  MODIFY `practiceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `m_procedure`
--
ALTER TABLE `m_procedure`
  MODIFY `procedureID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_relationships`
--
ALTER TABLE `m_relationships`
  MODIFY `relationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_serviceLocations`
--
ALTER TABLE `m_serviceLocations`
  MODIFY `serviceLocID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_statements`
--
ALTER TABLE `m_statements`
  MODIFY `statementID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_taxonomycodes`
--
ALTER TABLE `m_taxonomycodes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=486;
--
-- AUTO_INCREMENT for table `m_users`
--
ALTER TABLE `m_users`
  MODIFY `userID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `m_zipcodes`
--
ALTER TABLE `m_zipcodes`
  MODIFY `zipID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29385;
--
-- AUTO_INCREMENT for table `m_zones`
--
ALTER TABLE `m_zones`
  MODIFY `zoneID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
