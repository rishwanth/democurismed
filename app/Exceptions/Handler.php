<?php

namespace App\Exceptions;

//use Response;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->ajax() || $request->wantsJson()){
            $message = $exception->getMessage() . ' => ' . $exception->getTraceAsString();
            $response =  array('status' => 0, 'message' => $message, 'data' => array());
            return response(json_encode($response));      
        }

         if ($exception){
            $response = [
                    'status' => 0,
                    'message' => get_class($exception),
                    'data' => ['errocode'=>$exception->getStatusCode(),
                        'message'=>$exception->getMessage()]
                    ];
            // this goto log or error mail
            return parent::render($request, $exception);
            return response(json_encode($response));      
        }        
        $message = $exception->getMessage() . ' => ' . $exception->getTraceAsString();
        if ($this->isHttpException($exception)) {
            $statusCode = $exception->getStatusCode();
        }

        return parent::render($request, $exception);
    }
}
