<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : User
* Purpose       : Authentication for passport package by framework
* Description   : 
* Created date  : 
* Created time  : 
* Author        : Framework
* 
* History       : 2018-08-02, Rishwanth, Code clean up
********************************************************************************************/
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
//use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable  {

    use HasApiTokens, Notifiable;

     protected $table = 'users';
     protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
