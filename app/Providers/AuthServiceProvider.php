<?php
namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $carbon_date = Carbon::parse(now());
        Passport::routes();
/*        Passport::tokensExpireIn($carbon_date->addHours(0.5));
        Passport::refreshTokensExpireIn($carbon_date->addHours(1));
*/    }
}
