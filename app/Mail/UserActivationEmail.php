<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserActivationEmail extends Mailable {
    use Queueable, SerializesModels;
    public $mUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user) {
        //
        $this->mUser = $user;
    }

    public function build() {
         return $this->from('noreply@curismed.com')
                    ->bcc('rishwanth@curismed.com')
                    ->subject('Greeting from AMROMED LLC - Activate your account')
                    ->view('mails.register_activation')
                    ->text('mails.register_activation')
                    ->with(
                      [
                      ]);
    }
}
