<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(){
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin', 'Display home page');
        return view('welcome');
    }

    public function getCsrfToken(Request $request){
        try{
	    	$this->mStatus = 1;
	    	$this->mMessage = 'Success';
	    	$this->mData = ['_token' => csrf_token()];
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
	}
    
}
