<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ActivityController
* Description   : 
*
* Created date  : 2018-08-01 
* Created time  : 06:00 AM IST
* Author        : Rishwanth
* 
* History       : 2018-04-04 07:30 PM IST, Rishwanth, Code clean up
* History       : 2018-04-04 08:00 PM IST, Rishwanth, renamed the function name
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;
use PDF;
use Elibyy\TCPDF\Facades\TCPDF;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

use App\Users;
use App\Model\ActivityModel;
use App\Model\PatientsModel;
use App\Model\DetailsModel;
use App\Model\ClaimsSubmissioActivityModel;
use App\Model\ClaimsModel;

class HCFA500Controller extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index(Request $request) {
    }



    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function pdf(Request $request, $submitClaim = '') {
        $fileNameOnly = 'HCFA-1500';
        $flagFileIsCreated = 0;
        try{
          $ClaimNo = $request->get('ClaimNumber');
          $chartNumber = $request->get('chartNumber');
          $withBackgroundImage = (int) $request->get('withBackgroundImage');
          $info = array();

          //DB::beginTransaction();

          $sql = "SELECT P.id as patientID, CL.id as claimID, CL.auth_num as authNo, PR.EIN , PR.name as practiceName, PR.address as practiceAddr, PR.npi,PH.individual_npi as individualNPI, PH.name as phyName, SL.billing_name as billingName, SL.address, SL.NPI, P.full_name as fullName, P.chart_num as chartNo, P.dob, P.gender, P.addr_street1 as addrStreet1, P.addr_city as addrCity, P.addr_state as addrState, P.addr_zip as addrZip,P.phone_home as phoneHome, P.primary_care_phy as primaryCarePhy, P.referring_phy as referringPhy, P.default_render_phy as defaultRenderPhy, P.default_service_loc as defaultServiceLoc, I.payer_name as payerName, I.addr, I.city, I.state, I.zip, I.clearing_house_payor_id as clearingHousePayorID, PO.policy_num as policyNo, PO.group_num as groupNo, CL.from_date as fromDt, CL.to_date as toDt, CL.total, CL.place_of_service as placeOfService, CL.units, CL.proced, CL.mod1 from tb_claims CL inner join tb_cases CA on CA.id = CL.case_id inner join tb_patients P on P.id = CA.patient_id inner join tb_policies PO on PO.case_id = CA.id inner join tb_insurances I on I.id = PO.insurance_id inner join tb_physicians PH on PH.id = CL.primary_care_phy inner join tb_practices PR on PR.id = P.practice_id inner join tb_service_locations SL on SL.id = CL.service_loc_id where P.chart_num = '$chartNumber' AND CL.is_sent='0' AND CL.claim_status='Ready'";

            $fileNameOnly = '';
            $fileName = '';

            $rows = DB::select($sql);
            $num_of_row = count($rows);

          if($num_of_row % 6 == 0){
            $num_of_row = $num_of_row / 6;
          } else {
            $num_of_row = $num_of_row / 6;
            $num_of_row = (string) $num_of_row;
            $num_of_row = explode(".", $num_of_row)[0];
            $num_of_row = (int)$num_of_row +1;
          }

          $diag1_temp = '';
          $diag2_temp = '';
          $diag3_temp = '';
          $diag4_temp = '';
          $claimID = 0;
          for ($incr=0; $incr < $num_of_row; $incr++) { 
              $sql1 = $sql . " and CL.ID>$claimID order by CL.id LIMIT 6";

              $individualNPI1 = "";
              $individualNPI2 = "";
              $individualNPI3 = "";
              $individualNPI4 = "";
              $individualNPI5 = "";
              $individualNPI6 = "";

              $Smonth1 = "";
              $Smonth2 = "";
              $Smonth3 = "";
              $Smonth4 = "";
              $Smonth5 = "";
              $Smonth6 = "";

              $Sdate1 = "";
              $Sdate2 = "";
              $Sdate3 = "";
              $Sdate4 = "";
              $Sdate5 = "";
              $Sdate6 = "";

              $Syear1 = "";
              $Syear2 = "";
              $Syear3 = "";
              $Syear4 = "";
              $Syear5 = "";
              $Syear6 = "";

              $Emonth1 = "";
              $Emonth2 = "";
              $Emonth3 = "";
              $Emonth4 = "";
              $Emonth5 = "";
              $Emonth6 = "";

              $Edate1 = "";
              $Edate2 = "";
              $Edate3 = "";
              $Edate4 = "";
              $Edate5 = "";
              $Edate6 = "";

              $Eyear1 = "";
              $Eyear2 = "";
              $Eyear3 = "";
              $Eyear4 = "";
              $Eyear5 = "";
              $Eyear6 = "";

              $placeOfService1 = "";
              $placeOfService2 = "";
              $placeOfService3 = "";
              $placeOfService4 = "";
              $placeOfService5 = "";
              $placeOfService6 = "";

              $proced1 = "";
              $proced2 = "";
              $proced3 = "";
              $proced4 = "";
              $proced5 = "";
              $proced6 = "";

              $mod11 = "";
              $mod12 = "";
              $mod13 = "";
              $mod14 = "";
              $mod15 = "";
              $mod16 = "";

              $pointer1 = "";
              $pointer2 = "";
              $pointer3 = "";
              $pointer4 = "";
              $pointer5 = "";
              $pointer6 = "";

              $charge1 = "";
              $charge2 = "";
              $charge3 = "";
              $charge4 = "";
              $charge5 = "";
              $charge6 = "";

              $units1 = "";
              $units2 = "";
              $units3 = "";
              $units4 = "";
              $units5 = "";
              $units6 = "";

              $insuranceAddr1 = "";
              $practiceAddr = "";
              $facilityAddr = "";

              $chargeTot = 0;
              $count = 0;
              $i = 1;

              $rows1 = DB::select($sql1);
                PDF::SetAutoPageBreak(TRUE, 0);
                //PDF::SetLeftMargin(2);
                PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                PDF::SetTopMargin(0);
                PDF::SetHeaderMargin(0);
                PDF::SetFooterMargin(0);
                PDF::AddPage();
                PDF::setPrintFooter(false);
                PDF::SetFont('times', '', 9);
                PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);
                PDF::SetTitle('HCFA-1500');
                if ($withBackgroundImage == 1) {
                    $img_file = public_path('/img/image_demo.jpg');
                    PDF::Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
                  $path = $img_file;
                  $type = pathinfo($path, PATHINFO_EXTENSION);
                  $data = file_get_contents($path);
                  $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                }

            //$img = file_get_contents($path);
            //PDF::Image('@' . $img);

              foreach ($rows1 as $row1) {
                  $row = (array) $row1;
                  $count = $count + 1;
                  $fullName = $row['fullName'];
                  $claimID = $row['claimID'];
                  $dob = $row['dob'];
                  $gender = $row['gender'];
                  $addrStreet1 = $row['addrStreet1'];
                  $addrCity = $row['addrCity'];
                  $addrState = $row['addrState'];
                  $addrZip = $row['addrZip'];
                  $phoneHome = $row['phoneHome'];
                  $payerName = $row['payerName'];
                  $insuranceAddr1 = $row['addr'];

                  $insuranceCity = $row['city'];
                  $insuranceState = $row['state'];
                  $insuranceZip = $row['zip'];
                  $insurancePayorID = $row['clearingHousePayorID'];

                  $policyNo = $row['policyNo'];
                  $groupNo = $row['groupNo'];
                  $authNo = $row['authNo'];
                  $patientID = $row['patientID'];
                  $facilityName = $row['billingName'];
                  $facilityAddr = $row['address'];
                  $facilityNPI = $row['NPI'];
                  $practiceNPI = $row['npi'];
                  $practiceName = $row['practiceName'];
                  $practiceAddr = $row['practiceAddr'];
                  $providerName = $row['phyName'];
                  $chartNo = $row['chartNo'];
                  $fromDt = $row['fromDt'];
                  $toDt = $row['toDt'];
                  ${"individualNPI".$i} = $row['individualNPI'];
                  ${"placeOfService".$i} = $row['placeOfService'];
                  ${"proced".$i} = $row['proced'];
                  ${"mod1".$i} = $row['mod1'];
                  ${"charge".$i} = number_format(round((float)$row['total'],2),2);
                  ${"units".$i} = $row['units'];
                  $EIN = $row['EIN'];

                  $c = $submitClaim . '-' .($incr+1);
                  $chartNo = $c;

                  $claims = ClaimsModel::find($claimID);
                  $claims->is_sent = 1;
                  $claims->claim_status = "Submit";
                  $claims->updated_at = now();
                  $claims->save();

                  $claims_sub = new ClaimsSubmissioActivityModel();
                  $claims_sub->claim_id = $claimID;
                  $claims_sub->patient_id = $patientID;
                  $claims_sub->claim_number = $chartNo;
                  $claims_sub->submitted_at = now();
                  $claims_sub->save();

                  $sql3 = "SELECT diag1,diag2,diag3,diag4 from tb_claims where claim_number = '$ClaimNo' 
                  GROUP BY claim_number";
                  $row3 = DB::select($sql3);
                  foreach ($row3 as $row4) {
                      $row1 = (array) $row4;
                      $diag1_temp = $row1['diag1'];
                      $diag2_temp = $row1['diag2'];
                      $diag3_temp = $row1['diag3'];
                      $diag4_temp = $row1['diag4'];
                  }

                  $chargeTot = $chargeTot + number_format(round((float)$row['total'],2),2);

                  $tempfromDt = explode("-", $fromDt);
                  if($tempfromDt[1] != ""){
                      ${"Smonth".$i} = $tempfromDt[1];
                  } else {
                      ${"Smonth".$i} = "";
                  }

                  if($tempfromDt[2] != ""){
                      ${"Sdate".$i} = $tempfromDt[2];
                  } else {
                      ${"Sdate".$i} = "";
                  }

                  if($tempfromDt[0] != ""){
                      ${"Syear".$i} = substr($tempfromDt[0],-2);
                  } else {
                      ${"Syear".$i} = "";
                  }

                  $temptoDt = explode("-", $toDt);
                  if($temptoDt[1] != ""){
                      ${"Emonth".$i} = $temptoDt[1];
                  } else {
                      ${"Emonth".$i} = "";
                  }

                  if($temptoDt[2] != ""){
                      ${"Edate".$i} = $temptoDt[2];
                  } else {
                      ${"Edate".$i} = "";
                  }

                  if($temptoDt[0] != ""){
                      ${"Eyear".$i} = substr($temptoDt[0],-2);
                  } else {
                      ${"Eyear".$i} = "";
                  }

                  $temptoDt = explode("-", $toDt);

                  if($temptoDt[1] != ""){
                    ${"Emonth".$i} = $temptoDt[1];
                  } else {
                    ${"Emonth".$i} = "";
                  }

                  if($temptoDt[2] != ""){
                    ${"Edate".$i} = $temptoDt[2];
                  } else {
                    ${"Edate".$i} = "";
                  }

                  if($temptoDt[0] != ""){
                    ${"Eyear".$i} = substr($temptoDt[0],-2);
                  } else {
                    ${"Eyear".$i} = "";
                  }

                  if($diag1_temp != "" && $diag2_temp != "" && $diag3_temp != "" && $diag4_temp != "" ){
                    ${"pointer".$i} = "ABCD";
                  }
                  else if($diag1_temp != "" && $diag2_temp != "" && $diag3_temp != "" && $diag4_temp == "" ){
                    ${"pointer".$i} = "ABC";
                  }
                  else if($diag1_temp != "" && $diag2_temp != "" && $diag3_temp == "" && $diag4_temp == "" ){
                    ${"pointer".$i} = "AB";
                  }
                  else if($diag1_temp != "" && $diag2_temp == "" && $diag3_temp == "" && $diag4_temp == "" ){
                    ${"pointer".$i} = "A";
                  } else if($diag1_temp == "" && $diag2_temp == "" && $diag3_temp == "" && $diag4_temp == "" ){
                    ${"pointer".$i} = "";
                  }
                  $i++;
              }

              $sql9 = "SELECT * from tb_guarantor where patient_id = '$patientID'";
              $rows91 = DB::select($sql9);
              if(count($rows91) > 0){
                foreach ($rows91 as $rows92) {
                    $row9 = (array) $rows92;
                    $guarantorName = $row9['guarantorName'];
                    $guarantorDOB = $row9['guarantorDOB'];
                    $guarantorStreet = $row9['guarantorStreet'];
                    $guarantorCity = $row9['guarantorCity'];
                    $guarantorState = $row9['guarantorState'];
                    $guarantorZip = $row9['guarantorZip'];
                    $guarantorPhoneHome = $row9['guarantorPhoneHome'];
                }
              } else {
                $guarantorName = $fullName;
                $guarantorDOB = $dob;
                $guarantorStreet = $addrStreet1;
                $guarantorCity = $addrCity;
                $guarantorState = $addrState;
                $guarantorZip = $addrZip;
                $guarantorPhoneHome = $phoneHome;
            }

            $insuranceAddr1 = explode(",", $insuranceAddr1);
            $practiceAddr = explode(",", $practiceAddr);
            $facilityAddr = explode(",", $facilityAddr);

            $tempName = explode(" ", $fullName);
            if($tempName[0] != ""){
              $firstName = $tempName[0];
            } else {
              $firstName = "";
            }

            if($tempName[1] != "") {
              $middleName = $tempName[1];
            } else {
              $middleName = "";
            }

            if($tempName[2] != ""){
              $lastName = $tempName[2];
            } else {
              $lastName = "";
            }

            $fullName = $lastName.', '.$firstName.' '.$middleName;
            $tempName1 = explode(" ", $guarantorName);
            if($tempName1[0] != "") {
              $firstName1 = $tempName1[0];
            } else {
              $firstName1 = "";
            }

            if($tempName1[1] != "") {
              $middleName1 = $tempName1[1];
            } else {
              $middleName1 = "";
            }

            if($tempName1[2] != "") {
              $lastName1 = $tempName1[2];
            } else {
              $lastName1 = "";
            }

            $guarantorName = $lastName1.', '.$firstName1.' '.$middleName1;
            $tempDOB = explode("-", $dob);
            if($tempDOB[0] != "") {
              $year = $tempDOB[0];
            } else {
              $year = "";
            }

            if($tempDOB[1] != "") {
              $month = $tempDOB[1];
            } else {
              $month = "";
            }

            if($tempDOB[2] != "") {
              $date = $tempDOB[2];
            } else {
              $date = "";
            }

            if($gender == "M"){
              $Male = 'X';
              $Female = '';
            } else {
              $Male = '';
              $Female = 'X';
            }

            $CurrDate = date('Y-m-d');
            $tempCurrDate = explode("-", $CurrDate);
            if($tempCurrDate[0] != ""){
              $Curryear = $tempCurrDate[0];
            } else {
              $Curryear = "";
            }

            if($tempCurrDate[1] != ""){
              $Currmonth = $tempCurrDate[1];
            } else {
              $Currmonth = "";
            }

            if($tempCurrDate[2] != ""){
              $Currdate = $tempCurrDate[2];
            } else {
              $Currdate = "";
            }
            $dummy  = str_pad('', 39);


            if (!isset($facilityAddr[1])) {
                $facilityAddr[1] = '';
            }
            if (!isset($practiceAddr[1])) {
                $practiceAddr[1] = '';
            }
            if (!isset($insuranceAddr1[1])) {
                $insuranceAddr1[1] = '';
            }


            
            if ($flagFileIsCreated == 0) {
                $flagFileIsCreated = 1;
                $fileNameOnly = $submitClaim.'_'.$Currmonth.''.$Currdate.''.$Curryear;
-                
                PDF::SetTitle($fileNameOnly);
            }

            PDF::Text( 100, 16, $payerName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 100, 22, $insuranceAddr1[0], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 100, 27, $insuranceAddr1[1], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::SetFont('times', '', 10);

            PDF::Text  ( 114.5, 38, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 130, 38, $policyNo, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 130, 47, $fullName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 130, 56, $addrStreet1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 130, 65, $addrCity, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 70, 65, $addrState, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 130, 74, $addrZip, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 130, 89, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 134, 93, $month, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 142, 93, $date, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 150, 93, $year, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 8, 47, $fullName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 78, 47.5, $month, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 87, 47.5, $date, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 95, 47.5, $year, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 107, 47, $Male, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 120, 47, $Female, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 8, 56, $addrStreet1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 85.5, 56, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 8, 65, $addrCity, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 192, 65, $addrState, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 165, 75, $phoneHome, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 8, 74, $addrZip, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 40.5, 74, $phoneHome, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 8, 83, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 8, 92, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 104.5, 92, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 188, 92, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 104.5, 100, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 104.5, 109, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 144, 118.5, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 20, 136, 'SIGNATURE ON FILE', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 95, 136,  $Currmonth.'/'.$Currdate.'/'.$Curryear, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 150, 136, 'SIGNATURE ON FILE', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 107.5, 169, '0', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 10, 172, $diag1_temp, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 10, 182, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 76, 171.5, $diag3_temp, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 107.5, 173, $diag4_temp, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 43, 172, $diag2_temp, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 40, 182, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 144, 164, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 168, 165, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 5, 199, $Smonth1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 13, 199, $Sdate1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 20.5, 199, $Syear1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 28, 199, $Emonth1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 36, 199, $Edate1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 44, 199, $Eyear1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 52, 199, $placeOfService1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 70, 199, $proced1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 86, 199, $mod11, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 116, 199, $pointer1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 134, 199, $charge1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 142, 199, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 151.5, 199, $units1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 176, 199, $individualNPI1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );PDF::Text  ( 5, 208, $Smonth2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 13, 208, $Sdate2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 20.5, 208, $Syear2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 28, 208, $Emonth2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 36, 208, $Edate2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 44, 208, $Eyear2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 52, 208, $placeOfService2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 70, 208, $proced2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 86, 208, $mod12, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 116, 208, $pointer2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 134, 208, $charge2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 142, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 151.5, 208, $units2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 176, 208, $individualNPI2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );PDF::Text  ( 5, 217, $Smonth3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 13, 217, $Sdate3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 20.5, 217, $Syear3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 28, 217, $Emonth3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 36, 217, $Edate3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 44, 217, $Eyear3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 52, 217, $placeOfService3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 70, 217, $proced3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 86, 217, $mod13, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 116, 217, $pointer3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 134, 217, $charge3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 142, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 151.5, 217, $units3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 176, 217, $individualNPI3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );PDF::Text  ( 5, 225, $Smonth4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 13, 225, $Sdate4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 20.5, 225, $Syear4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 28, 225, $Emonth4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 36, 225, $Edate4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 44, 225, $Eyear4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 52, 225, $placeOfService4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 70, 225, $proced4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 86, 225, $mod14, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 116, 225, $pointer4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 134, 225, $charge4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 142, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 151.5, 225, $units4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 176, 225, $individualNPI4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );PDF::Text  ( 5, 233, $Smonth5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 13, 233, $Sdate5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 20.5, 233, $Syear5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 28, 233, $Emonth5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 36, 233, $Edate5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 44, 233, $Eyear5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 52, 233, $placeOfService5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 70, 233, $proced5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 86, 233, $mod15, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 116, 233, $pointer5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 134, 233, $charge5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 142, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 151.5, 233, $units5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 176, 233, $individualNPI5, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );PDF::Text  ( 5, 242, $Smonth6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 13, 242, $Sdate6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 20.5, 242, $Syear6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 28, 242, $Emonth6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 36, 242, $Edate6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 44, 242, $Eyear6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 52, 242, $placeOfService6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 70, 242, $proced6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 86, 242, $mod16, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 116, 242, $pointer6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 134, 242, $charge6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 142, 242, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 151.5, 242, $units6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 176, 242, $individualNPI6, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

            PDF::Text  ( 10, 252.5, $EIN, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 50, 252.5, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 64, 252.5, $chartNo, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 97, 252.5, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 136, 252.5, number_format(round((float)$chargeTot)), false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 147, 252.5, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 164, 252.5, '0', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 172, 252.5, '00', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 188, 252.5, '0', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 194, 252.5, '00', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::SetFont('times', '', 9);
            PDF::Text  ( 5, 269, $providerName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 40, 272, $Currmonth.'/'.$Currdate.'/'.$Curryear, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 61, 262, $facilityName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 61, 266, $facilityAddr[0], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 61, 270, $facilityAddr[1], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 61, 275, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 135, 262, $practiceName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 135, 266, $practiceAddr[0], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 135, 270, $practiceAddr[1], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
            PDF::Text  ( 136, 275, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );

        }

            //DB::commit();
            if (count($rows) < 1){
                $this->mMessage = "Records are empty";
                return $this->sendResponseDefault();
            } else {
                 $folder = $this->getFolderHcfa();
                 $fileNameOnly = $folder . $fileNameOnly . '.pdf'; 
               //PDF::Output($fileNameOnly,'FI');
               PDF::Output($fileNameOnly,'F');
                $this->mMessage = "HFCA created successfully";
               $this->mStatus = 1;
            }
        } catch (Exception $e) {
            //DB::rollback();
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

}
