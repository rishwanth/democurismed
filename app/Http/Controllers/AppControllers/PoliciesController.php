<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PoliciesController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;

//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\PoliciesModel;
use App\Model\CaseModel;

class PoliciesController extends Controller {

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        $data = array();
        $status = 0;
        $message = 'Failed';
        
        $caseChartNo = $request->get('caseChartNo');
        $rows = CaseModel::where('', '=', $caseChartNo)
                    ->select('case_id')
                    ->get();

        if ($objCases->count() < 1){
            $message = "Case chart number $caseChartNo not found ";
            return $this->sendResponse($status, $message, $data);
        }   
        $case_id = $rows[0]->case_id;

        $input[''] = '';
        $input['policy_entity'] = 'policyEntity';
        $input['insurance_id'] = 'insuranceID';
        $input['policy_holder'] = 'policyHolder';
        $input['relationship_to_insured'] = 'relationshipToInsured';
        $input['policy_num'] = 'policyNo';
        $input['group_num'] = 'groupNo';
        $input['claim_num'] = 'claimNo';
        $input['start_date'] = 'startDt';
        $input['end_date'] = 'endDt';
        $input['co_payment'] = 'coPayment';

        $sql = "";
        foreach ($rows as $tableField => $formField) {
            if (strlen($tableField) ==0 || strlen($formField) ==0){
                continue;
            }
            if ($tableField == 'policy_entity'){
                continue;
            }
            if ($tableField == 'policy_entity'){
                continue;
            }

            $$tableField = $request->get($formField);
            if (strlen($sql)){
                $sql .= ", ";
            }
            $sql .= "$tableField='" . $$tableField . "'";
        }
        $sql = "update tb_policies set " . $sql . " WHERE policy_entity='$policy_entity' AND case_id='$case_id'";
        DB::statement($sql);

        $input = [];
        $input[''] = '';
        $input['principal_diag'] = 'principalDiag';
        $input['def_diag_2'] = 'defDiag2';
        $input['def_diag_3'] = 'defDiag3';
        $input['def_diag_4'] = 'defDiag4';
        $sql = "";
        foreach ($rows as $tableField => $formField) {
            if (strlen($tableField) ==0 || strlen($formField) ==0){
                continue;
            }
            $$tableField = $request->get($formField);
            if (strlen($sql)){
                $sql .= ", ";
            }
            $sql .= "$tableField='" . $$tableField . "'";
        }
        $sql = "update tb_cases set " . $sql . " WHERE case_id='$case_id'";
        DB::statement($sql);

        $status = 1;
        $message = 'Success';
        if (!empty($objModel)) {
            $data = (array) $objModel ;
        } else {
            $data = array(0 => array('invoice_num' => '00000') );
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function check(Request $request) {
        $data = array('status' => 'Primary');
        $status = 1;
        $message = 'Success';

        $caseID = (int) $request->get('caseID');

        $sql = "SELECT PO.policy_entity FROM tb_policies PO INNER JOIN tb_claims CL ON CL.case_id = PO.case_id WHERE PO.case_id = '$caseID' GROUP BY PO.policy_entity";
        $rows = DB::select($sql);
        if (count($rows)){
            $data = array('status' => 'Secondary');
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function loadPolicy(Request $request){
        try{
            $caseChartNo = $request->get('caseChartNo');
            $policyEntity = $request->get('policyEntity');
            if (empty($caseChartNo)){
                $this->mMessage = 'Invalid case chart number';
                return $this->sendResponseDefault();
            }
            if (empty($policyEntity)){
                $this->mMessage = 'Invalid policy entity';
                return $this->sendResponseDefault();
            }
            $rows =  CaseModel::where('chart_num', '=', $caseChartNo)
                        ->select('id')
                        ->get();
            if ($rows->count() > 0) {
                $caseID = $rows[0]->id;
                $policies = PoliciesModel::where(['case_id' => $caseID, 'policy_entity' => $policyEntity])
                    ->selectRaw('*, (select payer_name from tb_insurances I where I.id=insurance_id) as insurance_name')
                    ->get();
                    //Common::WriteLog('Policies','Policy Loaded. Case ID:' . $caseid,0);
                if ($policies->count() > 0){
                    $this->mData = $policies;
                    $this->mMessage = "Policy Loaded Success";
                    $this->mStatus = 1;
                } else {
                    $this->mMessage = 'Records not found';
                }
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function createPolicy(Request $request) {
        try{
            $input[''] = '';
            $input['policy_entity'] = 'policyEntity';
            $input['insurance_id'] = 'insuranceID';
            $input['policy_holder'] = 'policyHolder';
            $input['relationship_to_insured'] = 'relationshipToInsured';
            $input['policy_num'] = 'policyNo';
            $input['group_num'] = 'groupNo';
            $input['claim_num'] = 'claimNo';
            $input['start_date'] = 'startDt';
            $input['end_date'] = 'endDt';
            $input['co_payment'] = 'coPayment';
            $input['auth_num'] = 'authNo';


            $policyEntity = $request->get('policyEntity');
            $caseChartNo = $request->get('caseChartNo');
            $insuranceID = $request->get('insuranceID');
            
            //return $request->all();

            $rows = CaseModel::where(['chart_num' => $caseChartNo])
                    ->select('id')
                    ->get();
            if ($rows->count() < 1){
                $this->mMessage = 'Invalid Case Chart Number';
                return $this->sendResponseDefault();
            }

            $caseid = $rows[0]->id;
            $rows = PoliciesModel::where(['case_id' => $caseid, 'policy_entity' => $policyEntity])
                ->select('id')
                ->get();
            $policy_id = 0;
            $policies = new PoliciesModel();
            if ($rows->count() > 1){
                $policy_id = $rows[0]->id;
                $policies = PoliciesModel::find($policy_id);
            }

            if (!empty($policies)) {
                //Common::WriteLog('Policies','Policy Updated. Case ID:' . $caseid . 'Policy Entity:'.Input::get('policyEntity'),0,'Edit',$caseid);
                 foreach ($input as $tableField => $formField) {
                    if (strlen($tableField) ==0 || strlen($formField) ==0){
                        continue;
                    }
                    $policies->$tableField = $request->get($formField);
                }
                $policies->case_id = $caseid;
                if ($policies->save()){
                    $this->mMessage = 'Policy Updated Successfully!';
                    $this->mStatus = 1;
                    $this->mData = array('id'=>$policies->id,'ID'=>$policies->id, 'PolicyID' => $policies->id);
                } else {
                    $this->mMessage = 'Policy create error';
                }
            } else {
                $this->mMessage = 'Policy create error';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }
}