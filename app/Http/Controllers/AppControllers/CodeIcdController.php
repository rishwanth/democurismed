<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AlertsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : Anand
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;

//use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\CodeIcdModel;

class CodeIcdController extends Controller {


    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request) {
        $id = (int) $request->get('id');
        return $this->update($request, $id);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request, $id = 0) {
        try {
            $input[''] = '';
            $input['code_type'] = 'codeType';
            $input['long_description'] = 'longDesc';
            $input['short_description'] = 'shortDesc';
            $input['dx_code'] = 'dxCode';
            $input['icd_code'] = 'icdCode';
            $icdCode = $request->get('icdCode');

            if (empty($icdCode)){
                $this->mMessage = 'ICD Code is required';
                return $this->sendResponseDefault();
            }

            $objModel = array();
            if ($id > 0 ) {
                $objModel = CodeIcdModel::find($id);
            } else {
                $count = CodeIcdModel::where('icd_code',$icdCode)
                        ->select('id')
                        ->get()
                        ->count();
                if ($count > 0){
                    $this->mMessage = 'This ICD Code Already Exists';
                    return $this->sendResponseDefault();
                }
                $objModel = new CodeIcdModel();
            }
            if (empty($objModel)){
                $this->mMessage = 'Invalid ICD Code or ID';
                return $this->sendResponseDefault();
            }

            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0) {
                    continue;
                }
                $$tableField = $request->get($formField);
                $objModel->$tableField = $$tableField;
            }
            if ($objModel->save()){
                if ($objModel->id > 0){
                    $this->mStatus = 1;
                    $this->mMessage = 'ICD Created Successfully';
                    $this->mData = array('id'=>$objModel->id,
                                'ID'=>$objModel->id);
                }
            } else {
                $this->mMessage = 'ICD Created Failed';
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function edit(){
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function view(){
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request){
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list(Request $request){
        $status = 0;
        $message = "Records not found";
        $data = array();

        $searchTerm = $request->get('searchTerm');
        $codeType = $request->get('codeType');
        $sql = "";
        if (empty($codeType)){
            $codeType = 10;
        }
        if (!empty($searchTerm)){
            $c = " icd_code LIKE '%$searchTerm%' ";
            if (strlen($sql) > 0) {
                $sql .= " and "; 
            }
            $sql .= $c;
        }

        if (!empty($codeType)){
            $c = " code_type = '$codeType' ";
            if (strlen($sql) > 0) {
                $sql .= " and "; 
            }
            $sql .= $c;
        }

        $rows = CodeIcdModel::select('*')
                    ->whereRaw($sql)
                    ->get();
        if ($rows->count() > 0) {
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getByTerm(Request $request){
        try{
            $searchTerm = trim(strip_tags($request->get('term')));

            $sql = "select dx_code as value, id from tb_code_icd where icd_code LIKE '%".$searchTerm."%' and code_type='10'";
            
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

     /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function listICD(Request $request){   
        try {
            $codeType = $request->get('codeType');
            $icdCode = $request->get('icdCode');
            $longDescription = $request->get('longDescription');
            $shortDescription = $request->get('shortDescription');
            $sql = ' 1=1 ';

            if (empty($codeType)){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " code_type='$codeType' ";
            }
            if (empty($icdCode)){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " icd_code='$icdCode' ";
            }
            if (empty($longDescription)){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " long_description like '%$longDescription%' ";
            }
            if (empty($shortDescription)){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " short_description like '%$shortDescription%' ";
            }
            $rows = CodeIcdModel::whereRaw($sql)
                    ->select('*')
                    ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Codes ICD Listed Successfully';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

}