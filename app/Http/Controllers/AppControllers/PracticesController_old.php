<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PracticesController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 11:20 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;
use Validator;
use QueryException;
use Exception;

//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\PracticesModel;
use App\Model\ZoneModel;
use App\Model\HolidayModel;
use App\Model\PaycodesModel;
use App\Model\PracticePaycodesModel;
use App\Model\SessionRuleModel;
use App\Model\PracticesInsuranceModel;
use App\Model\PayPeriodsModel;
use App\Users;

class PracticesController_old extends Controller {
    private $mHelper;

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function __construct() {
        parent::__construct();
        $this->mHelper = new HelperController();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request, $id = 0) {
        try {
            $practiceName = $request->get('practiceName');
            $phoneMobile = $request->get('phoneMobile');
            if (empty($practiceName)){
                $this->mMessage = 'Practice Name is required';
                return $this->sendResponseDefault();
            }
            $phoneMobile = $request->get('phoneMobile');
            if (empty($phoneMobile)){
                $this->mMessage = 'Phone Mobile number is required';
                return $this->sendResponseDefault();
            }

            $input[''] = '';
            $input['name'] = 'practiceName';
            $input['dob'] = 'phyDOB';
            $input['npi'] = 'phyNPI';
            $input['ssn'] = 'phySSN';
            $input['ein'] = 'EIN';
            $input['address'] = 'phyAddr';
            $input['phone_home'] = 'phyHomePhone';
            $input['phone_work'] = 'phyWorkPhone';
            $input['phone_mobile'] = 'phyMobilePhone';
            $input['dept'] = 'dept';
            $input['fax'] = 'phyFax';
            $input['notes'] = 'phyNotes';
            $input['speciality'] = 'speciality';

            if ($id < 1 ) {
                $id = (int) $request->get('id');
                $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
                if (!empty($practiceID)){
                    $id = $practiceID;
                }
            }

            $objModel = PracticesModel::find($id);
            if (!$objModel){
                $objModel = new PracticesModel();
            }
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0) {
                    continue;
                }
                $$tableField = '' . $request->get($formField);
                $objModel->$tableField = $$tableField;
            }
            $objModel->save();
            if ($objModel->id > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Saved success';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
     }

     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function listForRegister(Request $request){
        try{
            $rows = PracticesModel::where('is_active',1)
                        ->select('id', 'name')
                        ->get();
            if ($rows->count() > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        try{
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            if ($practiceID < 1){
                $this->mMessage = 'Practice ID is required';
            } else {
                return $this->save($request, $practiceID);
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function edit()
    {
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function view()
    {
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request){
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function list(Request $request){
        try{
            $rows = PracticesModel::where('is_active',1)
                        ->select('*')
                        ->get();
            if ($rows->count() > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request){
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request){
        return $this->save($request, 0);
    }

     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function addZone(Request $request) {
        try{
            $id = (int) $request->get('id');
            $zoneId = (int) $request->get('zoneId');
            if ($id < 1) {
                $id = $zoneId;
            }
            return $this->saveZone($request, $id);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function saveZone(Request $request, $id = 0) {
        try{
            $practice_id = (int) $request->get('practice_id');
            
            if ($practice_id < 1) {
                $this->mMessage = 'Practice ID is required';
                return $this->sendResponseDefault();
            }
            $practice = PracticesModel::find($practice_id);
            if (empty($practice)) {
                $this->mMessage = 'Invalid Practice ID';
                return $this->sendResponseDefault();
            }

            $input['description'] = 'description';
            $input['name'] = 'name';
            $input['is_active'] = 'is_active';

            if ($id > 0) {
                $obj = ZoneModel::find($id);
                if (empty($obj)){
                    $this->mMessage = 'Zone not found';
                    return $this->sendResponseDefault();
                }
            } else {
                $obj = new ZoneModel();
            }
            foreach ($input as $tableField => $formField) {
                $value = '' . $request->get($formField);
                if ($tableField == 'is_active'){
                    $value = (int) $value;
                }
                $obj ->$tableField = $value;
            }
            $obj->practice_id = $practice_id;
            $flagAdded = 1;
            if ($id > 0) {
                $flagAdded = 0;
            }
            if ($obj->save()){
                if ($obj->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = $flagAdded==1 ? "Added Success" : "Updated Success";
                    $id = $obj->id;
                    $this->mData = array('id' => $id, 'zoneID' => $id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function addHoliday(Request $request) {
        try{
            $id = (int) $request->get('id');
            $holidayId = (int) $request->get('holidayId');
            if ($id < 1) {
                $id = $holidayId;
            }
            return $this->saveHoliday($request, $id);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function saveHoliday(Request $request, $id = 0) {
        try{
            $practice_id = (int) $request->get('practice_id');
            
            if ($practice_id < 1) {
                $this->mMessage = 'Practice ID is required';
                return $this->sendResponseDefault();
            }
            $practice = PracticesModel::find($practice_id);
            if (empty($practice)) {
                $this->mMessage = 'Invalid Practice ID';
                return $this->sendResponseDefault();
            }

            $input['date'] = 'date';
            $input['description'] = 'description';
            $input['is_active'] = 'is_active';

            $date = $request->get('date');
            $sql = "select id from tb_holiday where practice_id=$practice_id AND `date`='$date' AND id!=$id";
            $rows = DB::select($sql);
            if (count($rows) > 0 ){
                $this->mMessage = 'Holiday already exists for this date';
                return $this->sendResponseDefault();
            }

            if ($id > 0) {
                $obj = HolidayModel::find($id);
                if (empty($obj)){
                    $this->mMessage = 'Holiday not found';
                    return $this->sendResponseDefault();
                }
            } else {
                $obj = new HolidayModel();
            }
            foreach ($input as $tableField => $formField) {
                $value = '' . $request->get($formField);
                if ($tableField == 'is_active'){
                    $value = (int) $value;
                }
                $obj ->$tableField = $value;
            }
            $obj->practice_id = $practice_id;
            $flagAdded = 1;
            if ($id > 0) {
                $flagAdded = 0;
            }
            if ($obj->save()){
                if ($obj->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = $flagAdded==1 ? "Added Success" : "Updated Success";
                    $id = $obj->id;
                    $this->mData = array('id' => $id, 'holidayID' => $id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function addPaycode(Request $request) {
        try{
            $id = (int) $request->get('id');
            $practicePaycodeId = (int) $request->get('practicePaycodeId');
            if ($id < 1) {
                $id = $practicePaycodeId;
            }

            return $this->savePaycode($request, $id);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function savePaycode(Request $request, $id = 0) {
        try{
            $practice_id = (int) $request->get('practice_id');
            $paycode_id = (int) $request->get('paycode_id');
            
            if ($practice_id < 1) {
                $this->mMessage = 'Practice ID is required';
                return $this->sendResponseDefault();
            }
            if ($paycode_id < 1) {
                $this->mMessage = 'Paycode ID is required';
                return $this->sendResponseDefault();
            }

            $practice = PracticesModel::find($practice_id);
            if (empty($practice)) {
                $this->mMessage = 'Invalid Practice ID';
                return $this->sendResponseDefault();
            }

            $paycode = PaycodesModel::find($paycode_id);
            if (empty($paycode)) {
                $this->mMessage = 'Invalid Paycode ID';
                return $this->sendResponseDefault();
            }

            $input['paycode_id'] = 'paycode_id';
            $input['is_active'] = 'is_active';

            if ($id > 0) {
                $obj = PracticePaycodesModel::find($id);
                if (empty($obj)){
                    $this->mMessage = 'Practice Paycodes not found';
                    return $this->sendResponseDefault();
                }
            } else {
                $obj = new PracticePaycodesModel();
            }
            foreach ($input as $tableField => $formField) {
                $value = '' . $request->get($formField);
                if ($tableField == 'is_active'){
                    $value = (int) $value;
                }
                $obj ->$tableField = $value;
            }
            $obj->practice_id = $practice_id;
            $flagAdded = 1;
            if ($id > 0) {
                $flagAdded = 0;
            }
            if ($obj->save()){
                if ($obj->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = $flagAdded==1 ? "Added Success" : "Updated Success";
                    $id = $obj->id;
                    $this->mData = array('id' => $id, 'PracticePaycodeID' => $id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function listZone(Request $request){
        try{
            $practice_id = (int) $request->get('practice_id');
            $rows = ZoneModel::where('practice_id', $practice_id)
                    ->select('*')
                    ->get();
            if ($rows->count() > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function listHoliday(Request $request){
        try{
            $practice_id = (int) $request->get('practice_id');
            $rows = HolidayModel::where('practice_id', $practice_id)
                    ->select('*')
                    ->orderBy('date')
                    ->get();
            if ($rows->count() > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function listPaycode(Request $request){
        try{
            $practice_id = (int) $request->get('practice_id');
            $rows = PracticePaycodesModel::where('practice_id', $practice_id)
                    ->select('*')
                    ->get();
            if ($rows->count() > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function uploadLogo(Request $request){
        //try{
            $id = (int) $request->get('id');
            $practice_id = (int) $request->get('practice_id');

            if ($id < 1) {
                $id = $practice_id;
            }
            if ($id < 1){
                $this->mMessage = 'Practice ID is required.';
                //return $this->sendResponseDefault();
            }
            $obj = PracticesModel::find($id);
            if (empty($obj)){
                $this->mMessage = 'Practice ID not found';
                //return $this->sendResponseDefault();
            }
            $response = $this->mHelper->saveUploadFile($request, 'logo', 'images', 'IMAGE', 'ARRAY');

            return $response;

            if ($response['status'] == 1) {
                $obj->image_name = $response['data']['image_name'];
                $obj->image_name_saved = $response['data']['image_name_saved'];
                if ($obj->save()){
                    $this->mStatus = 1;
                    $this->mMessage = 'Success';
                } else {
                    $this->mMessage = 'Save logo failed';
                }
            } else {
                $this->mStatus = 0;
                $this->mMessage = $response['message'];
            }
        //} catch (Exception $e) {
            //$this->mMessage = $e->getMessage();
        //}
        return $this->sendResponseDefault();
    }


     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function addSessionRule(Request $request) {
        try{
            $id = (int) $request->get('id');
            $ruleId = (int) $request->get('ruleId');
            if ($id < 1) {
                $id = $ruleId;
            }
            return $this->saveSessionRule($request, $id);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function saveSessionRule(Request $request, $id = 0) {
        try{
            $practice_id = (int) $request->get('practice_id');
            
            if ($practice_id < 1) {
                $this->mMessage = 'Practice ID is required';
                //return $this->sendResponseDefault();
            }
            $practice = PracticesModel::find($practice_id);
            if (empty($practice)) {
                $this->mMessage = 'Invalid Practice ID';
                //return $this->sendResponseDefault();
            }

            $input['description'] = 'description';
            $input['name'] = 'name';
            $input['is_active'] = 'is_active';
            $input['is_run_rule'] = 'is_run_rule';
            $input['is_prevent_session_creation'] = 'is_prevent_session_creation';

            if ($id > 0) {
                $obj = SessionRuleModel::find($id);
                if (empty($obj)){
                    $this->mMessage = 'SessionRule not found';
                    return $this->sendResponseDefault();
                }
            } else {
                $obj = new SessionRuleModel();
            }
            foreach ($input as $tableField => $formField) {
                $value = '' . $request->get($formField);
                if ($tableField == 'is_active'){
                    $value = (int) $value;
                }
                $obj ->$tableField = $value;
            }
            $obj->practice_id = $practice_id;
            $flagAdded = 1;
            if ($id > 0) {
                $flagAdded = 0;
            }
            if ($obj->save()){
                if ($obj->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = $flagAdded==1 ? "Added Success" : "Updated Success";
                    $id = $obj->id;
                    $this->mData = array('id' => $id, 'ruleID' => $id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function addPracticesInsurance(Request $request) {
        try{
            $id = (int) $request->get('id');
            $practices_insurance_id = (int) $request->get('practices_insurance_id');
            if ($id < 1) {
                $id = $practices_insurance_id;
            }
            return $this->savePracticesInsurance($request, $id);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function savePracticesInsurance(Request $request, $id = 0) {
        try{
            $practice_id = (int) $request->get('practice_id');
            $insurance_id = (int) $request->get('insurance_id');

            if ($practice_id < 1) {
                $this->mMessage = 'Practice ID is required';
                //return $this->sendResponseDefault();
            }
            if ($insurance_id < 1) {
                $this->mMessage = 'Insurance ID is required';
                //return $this->sendResponseDefault();
            }
            $practice = PracticesInsuranceModel::find($practice_id);
            if (empty($practice)) {
                $this->mMessage = 'Invalid Practice ID';
                //return $this->sendResponseDefault();
            }


            if ($id > 0) {
                $obj = SessionRuleModel::find($id);
                if (empty($obj)){
                    $this->mMessage = 'PracticesInsuranceModel not found';
                    return $this->sendResponseDefault();
                }
            } else {
                $obj = new PracticesInsuranceModel();
            }
            $obj->practice_id = $practice_id;
            $obj->insurance_id = $insurance_id;

            $flagAdded = 1;
            if ($id > 0) {
                $flagAdded = 0;
            }
            if ($obj->save()){
                if ($obj->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = $flagAdded==1 ? "Added Success" : "Updated Success";
                    $id = $obj->id;
                    $this->mData = array('id' => $id, 'PracticesInsurance ' => $id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function addPayPeriods(Request $request) {
        try{
            $id = (int) $request->get('pay_period_id');
            return $this->savePayPeriods($request, $id);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function savePayPeriods(Request $request, $id = 0) {
        try{
            $practice_id = (int) $request->get('practice_id');
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');

            if ($practice_id < 1) {
                $this->mMessage = 'Practice ID is required';
                //return $this->sendResponseDefault();
            }
            if (empty($start_date)) {
                $this->mMessage = 'start_date is required';
                //return $this->sendResponseDefault();
            }
            if (empty($end_date)) {
                $this->mMessage = 'start_date is required';
                //return $this->sendResponseDefault();
            }

            $practice = PracticesModel::find($practice_id);
            if (empty($practice)) {
                $this->mMessage = 'Invalid Practice ID';
                //return $this->sendResponseDefault();
            }


            if ($id > 0) {
                $obj = PayPeriodsModel::find($id);
                if (empty($obj)){
                    $this->mMessage = 'PayPeriods not found';
                    return $this->sendResponseDefault();
                }
            } else {
                $obj = new PayPeriodsModel();
            }
            $obj->practice_id = $practice_id;
            $obj->start_date = $start_date;
            $obj->end_date = $end_date;

            $flagAdded = 1;
            if ($id > 0) {
                $flagAdded = 0;
            }
            if ($obj->save()){
                if ($obj->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = $flagAdded==1 ? "Added Success" : "Updated Success";
                    $id = $obj->id;
                    $this->mData = array('id' => $id, 'Pay Period  ' => $id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


    public function getUsers(Request $request){
        try{
            $rows = Users::where('is_activated',1)
                        ->select('id', 'first_name', 'last_name', 'username', 'email_id')
                        ->get();
            if ($rows->count() > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    public function _getZones(Request $request){
        try{
            $sql = "select * from tb_zone Z ";
            $rows = DB::select($sql);
           
            if (count($rows) > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    public function getZones(Request $request){
        try{
            $sql = "select * from tb_zone Z ";
            $rows = DB::select($sql);
           
            if (count($rows) > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


}