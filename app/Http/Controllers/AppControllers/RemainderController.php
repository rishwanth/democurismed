<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ReminderController
* Description   : 
* Created date  : 
* Created time  : 
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use QueryException;
use Exception;
use Session;
use DB;
use Carbon;
use File;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Users;
use App\Http\Controllers\Controller;
use App\Model\ReminderModel;

class ReminderController extends Controller {

    public function __construct() {
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function index() {
    }



    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        $reminder_id = (int) $request->get('reminder_id');
        try {
            $input[''] = '';
            $input['user_id'] = 'user_id';
            $input['message'] = 'message';
            $input['from_date_time'] = 'from_date_time';
            $input['snoze_time_in_min'] = 'snoze_time_in_min';
            $input['status'] = 'status';

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $value = '' . $request->get($formField);
                if (!empty($value)){
                    $$tblField = $request->get($formField);
                }
            }
            $obj = new ReminderModel();
            if ($reminder_id > 0){
                $obj = ReminderModel::find($reminder_id);
                if (empty($obj)){
                    $this->mStatus = 0;
                    $this->mMessage = 'Not found';
                    return $this->sendResponseDefault();
                }
            }

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                if (isset($$tblField)){
                    $obj->$tblField = $$tblField;
                }
            }
            if ($obj->save()){
                $this->mStatus = 1;
                $this->mMessage = 'Added Success';
                if ($reminder_id > 0) {
                    $this->mMessage = 'Updated Success';
                }
                $this->mData = ['id'=>$obj->id] ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getByUserID(Request $request){
        try{
            $status = '' . $request->get('status');
            $user_id = (int) $request->get('user_id');
            if (empty($status)){
                $status = 'empty';
            }
            
            //here filter DB::select($sql)

            $rows = ReminderModel::where('user_id', '=', $user_id)
                        ->where('status', $status)
                        ->select('*')
                        ->get();
            if ($rows->count() > 0 ){
                $this->mStatus = 1;
                $this->mMessage = 'success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


}
