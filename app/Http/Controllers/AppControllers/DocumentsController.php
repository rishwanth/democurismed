<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : tb_documents
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 07:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;
use DB;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use App\Model\DocumentsModel;
use App\Users;

class DocumentsController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }



    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request) {
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function get(Request $request) {
        $patient_id = $request->get('patientID');
        $sql = " 1=1 ";
        if (!empty($patient_id)){
            $c = "patient_id = '$patient_id'";
            if (strlen($sql) > 0){
                $sql .= " and ";
            }
            $sql .= $c;
        }

        $objRows = DocumentsModel::WhereRaw($sql)
                    ->select('*')
                    ->get();
        $message = 'Failed';
        $data = array();
        $status = 0;
        if ($objRows->count()){
            $objRows = (array) $objRows;
            $status = 1;
            $message = 'Success';
            $data = $objRows ;
        } else {
            $message = 'Records are empty';
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        try{
            $id = $request->get('docID');
            $status = $request->get('status');
            $notes = $request->get('notes');

            $objDoc = DocumentsModel::find($id);
            if ($objDoc){
                if (!empty($status)){
                    $objDoc->status = $status;
                }
                if (!empty($notes)){
                    $objDoc->notes = $notes;
                }
                $objDoc->save();
                $this->mStatus = 1;
                $this->mMessage = 'Success';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
   }

}
