<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : RegisterController
* Description   : 
*
* Created date  : 2018-08-01 
* Created time  : 06:00 AM IST
* Author        : Rishwanth
* 
* History       : 2018-04-04 07:30 PM IST, Rishwanth, Code clean up
* History       : 2018-04-04 08:00 PM IST, Rishwanth, renamed the function name
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;
use Log;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Users;
use App\Model\CaseModel;
use App\Model\AuthModel;
use App\Model\PatientsModel;
use App\Model\PoliciesModel;

class CaseController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }



    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
    	try {
            $patientID = (int) $request->get('patientID');
            $patient = PatientsModel::find($patientID); 
            if (empty($patient)){
                $this->mMessage = 'Invalid Patient ID';
                return $this->sendResponseDefault();
            }
            $input[''] = '';
            $input['description'] = 'description';
            $input['auth_num'] = 'authNo';
            $input['last_visit'] = 'lastVisit';
            $input['principal_diag'] = 'principalDiag';
            $input['def_diag_2'] = 'defDiag2';
            $input['def_diag_3'] = 'defDiag3';
            $input['def_diag_4'] = 'defDiag4';

            $count = CaseModel::where('patient_id', '=', $patientID)
                        ->select('id')
                        ->get()
                        ->count();

            $totC = $count + 1;
            $prefix = $patient->chart_num;
            $prefix = $prefix . '_' . $totC;
	        $case = new CaseModel();
            $case->chart_num = $prefix;
            $case->is_deleted = 0;
            $case->patient_id = $patientID;
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0){
                    continue;
                }
                $case->$tableField = $request->get($formField);
            }
	        if ($case->save()){
		        if ($case->id > 0) {
					$this->mStatus = 1;
                    $this->mMessage = 'Cases added successfully';
                    $this->mData = ['id' => $case->id, 'caseID' => $case->id ];
                    Log::debug('Cases added success');
				} else {
                    $this->mStatus = 0;
                    $this->mMessage = 'Cases added failed';
                    Log::debug('Cases added failed');
                }
			} else {
                $this->mStatus = 0;
                $this->mMessage = 'Cases added failed';
                Log::debug('Cases added failed');
            }
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
                    Log::debug("case->save() 5");
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request) {
        $patient_id = $request->get('patientID');
        $chart_num = $request->get('caseChartNo');
        $sql = " is_deleted=0 ";
        if (!empty($patient_id)){
            $c = "patient_id = '$patient_id'";
            if (strlen($sql) > 0){
                $sql .= " and ";
            }
            $sql .= $c;
        }
        if (!empty($chart_num)){
            $c = "chart_num = '$chart_num'";
            if (strlen($sql) > 0){
                $sql .= " and ";
            }
            $sql .= $c;
        }

        $rows = CaseModel::orWhereRaw($sql)
                    ->select('id', 'chart_num')
                    ->get();
        $message = 'Failed';
        $data = array();
        $status = 0;
        if ($rows->count()){
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getGroup(Request $request) {
        $message = 'Record not found';
        $data = array();
        $status = 0;
        $case_id = $request->get('caseID');
        $case_id = (int) '0' . $case_id;

        $sql = "select CA.chart_num, A.tos as description, I.payer_name, A.auth_num, PO.policy_num, A.start_date, A.end_date from tb_cases CA inner join tb_policies PO on PO.case_id = CA.id inner join tb_auth A on A.case_id = PO.case_id inner join tb_insurances I on I.id = PO.insurance_id where CA.id = '$case_id' ORDER BY A.id ASC LIMIT 1";
        $rows = DB::select($sql);
        if (count($rows) > 0){
            $status = 1;
            $message = 'Success';
            $data = $rows;
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getAuth1(Request $request) {
        $message = 'Record not found';
        $data = array();
        $status = 0;

        $case_id = $request->get('caseID');
        $case_id = (int) '0' . $case_id;

        $sql = "select A.auth_num, A.tos as description, A.start_date, A.end_date, I.payer_name from tb_auth A inner join tb_cases CA on CA.id = A.case_id  inner join tb_policies PO on PO.case_id = CA.id inner join tb_insurances I on I.id = PO.insurance_id  where A.case_id = '$case_id' AND PO.policy_entity='1' ";

        $rows = DB::select($sql);
        if (count($rows) > 0){
            $status = 1;
            $message = 'Success';
            $data = $rows;
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function loadByChartNum(Request $request) {
        $caseChartNo = $request->get('caseChartNo');
        if (empty($caseChartNo)){
            $this->mMessage = 'Invalid chart number';
            return $this->sendResponseDefault();
        }

        $count = CaseModel::where('chart_num', '=', $caseChartNo)->select('id')->get()->count();
        if ($count < 1){
            $this->mMessage = 'Invalid chart number';
            return $this->sendResponseDefault();
        }
        $sql = "select CA.* from tb_cases CA ";
        $sql .= " where is_deleted = 0 and CA.chart_num = '$caseChartNo' ";

        //Common::WriteLog('Cases','Case Loaded. Case Chart Number:' . Input::get('caseChartNo'),0);
        $rows = DB::select($sql);
        if (count($rows) > 0){
            $this->mData = $rows;
            $this->mMessage = 'Success';
            $this->mStatus = 1;
        } else {
            $this->mMessage = 'Records not found';
        }
        return $this->sendResponseDefault();
    }

    public function getCaseChart(Request $request){
        $caseChartNo = $request->get('caseChartNo');
        if (empty($caseChartNo)){
            $this->mMessage = 'Invalid chart number';
            return $this->sendResponseDefault();
        }

        $sql = "select CA.*, PO.* from tb_cases CA, tb_policies PO
        where PO.case_id = CA.id and CA.chart_num = '$caseChartNo'";

        $rows = DB::select($sql);
        if (count($rows) > 0){
            $this->mData = $rows;
            $this->mMessage = 'Success';
            $this->mStatus = 1;
        } else {
            $this->mMessage = 'Records not found';
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getChartNum(Request $request) {
        try{
            $patientID = (int) $request->get('patientID');
            $sql = "select * from tb_cases CA where CA.patient_id='$patientID' order by CA.chart_num desc limit 1";
            //return $sql;
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $chartNo = "";
                $claimID = 0;
                foreach ($rows as $row) {
                    $chartNo = $row->chart_num;
                    $claimID = $row->id;
                }
                $startPos = strpos($chartNo , "_");
                $strLength = strlen($chartNo);
                $temp = substr($chartNo, $startPos+1, $strLength);
                $val = substr($chartNo,0,$startPos);

                $this->mStatus = 1;
                $this->mData = array('val' => $val.'_'.$temp.','.$claimID);
                $this->mMessage = 'Success';
            } else {
                $this->mMessage = 'Record not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getCaseID(Request $request) {
        try{
            $caseChartNo = $request->get('caseChartNo');
            $rows = CaseModel::where('chart_num', $caseChartNo)
                    ->select('id')
                    ->get();
            if ($rows->count() > 0){
                $this->mMessage = 'Success';
                $this->mStatus = 1;
                $this->mData = array('case_id' => $rows[0]->id,
                 'id' => $rows[0]->id);
            } else {
                $this->mMessage = 'Record not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }
}
