<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PhysiciansController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 06:00 PM IST
* Author        : Rishwanth
* 

********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;
use QueryException;
use DB;
use Session;
use Carbon;
use Excel;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Model\PhysiciansModel;
use App\Model\PhysiciansPaycodesModel;
use App\Model\PhysiciansWorkhistoryModel;
use App\Users;

class PhysiciansController extends Controller {

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        try{
            $common = new HelperController();
            $data['SESSION'] = $this->mSession;
            $data['SCREEN_SCREEN'] = 'Employee';
            $data['specialityList'] = $common->getSpecialityList();
            $data['countryList'] = $common->getCountryList();
            $data['paycodeList'] = $common->getList($request, 'paycodes','ARRAY');
            $data['savedPaycodes'] = $this->loadPaycodes(0);
            $data['savedWorkhistory'] = $this->loadWorkhistory(0);
            $data['fieldsList'] = $this->getFieldsList();
            $data['yesnoList'] = $common->getListYesNo('ARRAY');
            $data['benifitsList'] = $common->getListBenifits('ARRAY');
            return view('physicians_add', compact('data'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getActivity(Request $request) {
        try{
            $employeeId = (int) $request->get('employeeId');
            $billable = '' . $request->get('billable');
            if ($employeeId < 1) {
                $this->mStatus = 0;
                $this->mMessage = 'Employee ID is required';
                return $this->sendResponseDefault();
            }
            $sql = "select PA.paycode_id as id, PC.name  from " . $this->mTables['EMPLOYEE_ACTIVITY'] . " PA, " . $this->mTables['PAYCODES'] . " PC where PC.id=PA.paycode_id AND PA.physician_id=$employeeId";
            if (!empty($billable)){
                $sql .= " and PA.billable='$billable'";
            } 
            $rows = DB::select($sql);
            if (count($rows) < 1) {
                $this->mStatus = 0;
                $this->mMessage = 'Records not found';
                $this->mData = [0 => ''];
            } else {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            }
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function edit(Request $request) {
        try{
            $id = (int) $request->get('id');
            if ($id < 1) {
                return redirect('/physician/list');
            }
            $row = PhysiciansModel::where('id', $id)->select('*')->get();
            if ($row->count() < 1){
                return redirect('/physician/list');
            }
            $common = new HelperController();
            $data['SESSION'] = $this->mSession;
            $data['edit_row'] = $row[0]->toArray();
            $data['SCREEN_SCREEN'] = 'Employee';
            $data['specialityList'] = $common->getSpecialityList();
            $data['countryList'] = $common->getCountryList();
            $data['paycodeList'] = $common->getList($request, 'paycodes','ARRAY');
            $data['savedPaycodes'] = $this->loadPaycodes($id);
            $data['savedWorkhistory'] = $this->loadWorkhistory($id);
            $data['fieldsList'] = $this->getFieldsList();
            $data['yesnoList'] = $common->getListYesNo('ARRAY');
            $data['benifitsList'] = $common->getListBenifits('ARRAY');
            return view('physicians_add', compact('data'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function addNew(Request $request) {
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request, $id = 0) {
        $id1 = (int) $request->get('id');
        if ($id1 > 0){
            $id = $id1;
        }
        try {
            DB::beginTransaction();
            $input['name'] = 'name';
            $input['date_of_birth'] = 'dob';
            $input['individual_npi'] = 'individual_npi';
            $input['ssno'] = 'ssno';
            $input['employee_position'] = 'employee_position';
            $input['address_1'] = 'address_1';
            $input['address_2'] = 'address_2';
            $input['phone_home'] = 'phone_home';
            $input['phone_work'] = 'phone_work';
            $input['phone_mobile'] = 'phone_mobile';
            $input['email'] = 'phy_email';
            $input['fax'] = 'fax';
            $input['notes'] = 'notes';
            $input['speciality'] = 'speciality';
            $input['employee_type'] = 'employee_type';
            $input['date_of_hire'] = 'date_of_hire';
            $input['has_benifits'] = 'has_benifits';
            $input['payroll_company'] = 'payroll_company';
            $input['payroll_employee_num'] = 'payroll_employee_num';
            $input['workers_comp_num'] = 'workers_comp_num';
            $input['department_id'] = 'department_id';
            $input['department_name'] = 'department_name';
            $input['pay_code'] = 'pay_code';
            $input['address_1'] = 'address_1';
            $input['address_2'] = 'address_2';
            $input['city'] = 'city';
            $input['region'] = 'region';
            $input['country_id'] = 'country_id';
            $input['country_name'] = 'country_name';
            $input['zip'] = 'zip';
            $input['driver_license_number'] = 'driver_license_number';
            $input['driver_license_expiration'] = 'driver_license_expiration';
            $input['auto_insurance_number'] = 'auto_insurance_number';
            $input['auto_insurance_expiration'] = 'auto_insurance_expiration';
            $input['educational_credential'] = 'educational_credential';
            $input['tb_clearance_expiration'] = 'tb_clearance_expiration';
            $input['medical_clearance_expiration'] = 'medical_clearance_expiration';
            $input['background_check_completion'] = 'background_check_completion';
            $input['background_check_expiration'] = 'background_check_expiration';
            //$input['paycode_name'] = 'paycode_name';
            //$input['paycode_id'] = 'paycode_id';
            $input['job_title'] = 'job_title';

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $$tblField = $request->get($formField);
            }

            $flagAdded = 1;
            $obj = new PhysiciansModel();
            if ($id > 0) {
                $obj = PhysiciansModel::find($id);
                $flagAdded = 0;
            }
            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $obj->$tblField = $$tblField;
            }
            if($id == 0){
                $obj->practice_id = $this->getPracticeID();
            }
            if ($obj->save()){
                if ($obj->id > 0) {
                    $paycode = $this->savePaycodes($request, $obj->id);
                    if ($paycode['status'] == 0) {
                        DB::rollback();
                        $this->mStatus = 0;
                        $this->mMessage = $paycode['message'];
                        return $this->sendResponseDefault();
                    }
                    $workhistory = $this->saveWorkhistory($request, $obj->id);
                    if ($workhistory['status'] == 0) {
                        DB::rollback();
                        $this->mStatus = 0;
                        $this->mMessage = $workhistory['message'];
                        return $this->sendResponseDefault();
                    }
                    $this->mStatus = 1;
                    $this->mMessage = 'Physicians added successfully';
                    if ($flagAdded == 0) {
                        $this->mMessage = 'Physicians updated successfully';
                    }
                    $this->mData = ['id' => $obj->id];
                    DB::commit();
                } else {
                    DB::rollback();
                    $this->mStatus = 0;
                    $this->mMessage = 'Physicians added failed';
                }
            }
        } catch (Exception $e) {
            DB::rollback();
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        try{
            $id = (int) $request->get('physicianID');
            if ($id < 1){
                $this->mMessage = 'Physician ID is missing';
                return $this->sendResponseDefault();
            }
            return $this->save($request, $id);
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request) {
        try{
            $physicianID = (int) $request->get('physicianID');
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');

            $sql = " 1=1 ";
            if ($physicianID > 0){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " id='$physicianID' ";
            }
            if ($practiceID > 0){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " practice_id='$practiceID' ";
            }
            $rows = PhysiciansModel::whereRaw($sql)
                        ->select('*')
                        ->get();

            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Record not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function get(Request $request){
        $practiceID = $this->getPracticeID();
        $term = $request->get('term');
        $sql = "select concat(id, ' - ' ,name) as `key`, concat(id, ' - ' ,name) as label from tb_physicians where practice_id=$practiceID ";
        if (strlen($term) > 0) {
            $sql .= " and (name like '%" . $term . "%') ";
        }
        $rows = DB::select($sql);
         if (count($rows) > 0){
             $this->mData = json_encode($rows);
             $this->mMessage ='Success';
             $this->mStatus = 1;
         } 
        return json_encode($rows);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function deletePhysicians(Request $request){
        try{
            $id = (int) $request->get('id');
            if ($id < 1){
                $this->mMessage = "Physicians is required";
                return $this->sendResponseDefault();
            }
            $result = PhysiciansModel::where('id', $id)
                        ->delete();
            if ($result){
                $this->mStatus = 1;
                $this->mMessage = "Deleted #id $id successfully";
            } else {
                $this->mMessage = 'Deleted failed';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list1(Request $request) {
        try{
            $id = (int) $request->get('id');
            $physicianID = $request->get('physicianID');
            if (!empty($physicianID)){
                $id = $physicianID;
            }

            $sql = " where 1=1 ";
            if (!empty($id)){
                $c = "PH.id = '$id'";
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= $c;
            }
            $sql = "select PH.name as `label`, concat(PH.id, ' - ' ,PH.name) as `key`, PH.* from tb_physicians PH " . $sql;

            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function listAll(Request $request) {
        try{
            $practiceID = Session::get('LOGGED_USER_PRATICE_ID');
            //here is error
            //$practiceID = 1;
            $rows = PhysiciansModel::where('practice_id', $practiceID)
                        ->select('*')
                        ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getPrimary(Request $request) {
        try{
            $sql = "select concat(id, ' - ', name) as label from tb_physicians";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getInfo(Request $request) {
        try{
            $physicianID = (int) $request->get('physicianID');
            $sql = "select * from tb_physicians where id='$physicianID'";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getFacilityInfo(Request $request) {
        try{
            $sql = "select id as serviceLocID, internal_name as internalName from tb_service_locations";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
   }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list(Request $request) {
        $practiceID = Session::get('LOGGED_USER_PRATICE_ID');
        //here is error
        $practiceID = 1;
        try{
            $common = new HelperController();
            $data['SESSION'] = $this->mSession;
            $data['specialityList'] = $common->getSpecialityList();
            return view('physicians_list', compact('data'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function listold(Request $request) {
        try{
            $data['SESSION'] = $this->mSession;
            return view('physicians_list_old', compact('data'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function savePaycodes(Request $request, $physicianId) {
        try{
            $arr = $request->get('paycodes');
            if (!is_array($arr)){
                $this->mStatus = 1;
                return $this->sendResponseDefault('ARRAY');
            }
            foreach ($arr as $key => $value) {
                $id = (int) $value['id'];
                $paycode_id = $value['paycode_id'];
                $rate = $value['rate'];
                $billable = $value['billable'];
                $mileage = $value['mileage'];
                $delete = (int) $value['delete'];
                if ($delete == 1){
                   if ($id > 0) {
                        $paycode = PhysiciansPaycodesModel::find($id);
                        $paycode->delete();
                   }
                   continue;
                }
                $paycode = new PhysiciansPaycodesModel();
                if ($id > 0) {
                    $paycode = PhysiciansPaycodesModel::find($id);
                }
                $paycode->physician_id = $physicianId;
                $paycode->paycode_id = $paycode_id;
                $paycode->billable = $billable;
                $paycode->mileage = $mileage;
                $paycode->rate = $rate;
                if (!$paycode->save()){
                    $this->mStatus = 0;
                    $this->mMessage = 'Paycode save failed';
                    return $this->sendResponseDefault('ARRAY');
                }
            }
            $this->mStatus = 1;
            $this->mMessage = 'Success';
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function saveWorkhistory(Request $request, $physicianId) {
        try{
            $arr = $request->get('workhistory');
            if (!is_array($arr)){
                $this->mStatus = 1;
                return $this->sendResponseDefault('ARRAY');
            }
            foreach ($arr as $key => $value) {
                $id = (int) $value['id'];
                $start_date = $value['start_date'];
                $end_date = $value['end_date'];
                $notes = $value['notes'];
                $delete = (int) $value['delete'];
                if ($delete == 1){
                   if ($id > 0) {
                        $workhistory = PhysiciansWorkhistoryModel::find($id);
                        $workhistory->delete();
                   }
                   continue;
                }
                $workhistory = new PhysiciansWorkhistoryModel();
                if ($id > 0) {
                    $workhistory = PhysiciansWorkhistoryModel::find($id);
                }
                $workhistory->physician_id = $physicianId;
                $workhistory->start_date = $start_date;
                $workhistory->end_date = $end_date;
                $workhistory->notes = $notes;
                if (!$workhistory->save()){
                    $this->mStatus = 0;
                    $this->mMessage = 'Workhistory save failed';
                    return $this->sendResponseDefault('ARRAY');
                }
            }
            $this->mStatus = 1;
            $this->mMessage = 'Success';
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function loadPaycodes($physicianId) {
        $sql = "select ppc.*, pc.name from tb_physicians_paycodes ppc, tb_paycodes pc where pc.id=ppc.paycode_id and ppc.physician_id=$physicianId";

        $rows = DB::select($sql);
        if (count($rows) > 0) {
            $this->mStatus = 1;
            $this->mMessage = 'Success';
        } else {
            $this->mStatus = 0;
            $this->mMessage = 'Paycodes not found';
        }
        $this->mData = $rows;
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function loadWorkhistory($physicianId) {
        $rows = PhysiciansWorkhistoryModel::where('physician_id', $physicianId)
            ->select('*')
            ->get();
        if ($rows->count() > 0) {
            $this->mStatus = 1;
            $this->mMessage = 'Success';
        } else {
            $this->mStatus = 0;
            $this->mMessage = 'Workhistory not found';
        }
        $this->mData = $rows;
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function report(Request $request) {
        try{
            $employeeId = (int) $request->get('employeeId');
            if ($employeeId < 1) {
                $this->mStatus = 0;
                $this->mMessage = 'Employee ID is required';
                return $this->sendResponseDefault();
            }
            
            $paycode = "(select sum(mileage) from tb_physicians_paycodes PPC where PPC.physician_id=PH.id) as mileage, ";
            $paycode .= "(select sum(rate) from tb_physicians_paycodes PPC where PPC.physician_id=PH.id) as rate ";
            $sql = "select name, region, AP.start_date, AP.end_date, $paycode from tb_physicians PH, tb_appointments AP where PH.id=AP.provider_id AND PH.id=$employeeId";
            $rows = DB::select($sql);
            if (count($rows) < 1){
                $this->mStatus = 0;
                $this->mMessage = 'Records not found';
                $this->mData = [];
                return $this->sendResponseDefault();
            }
            $data = [];
            foreach ($rows as $key => $row1) {
                $row = (array) $row1;
                $sdate = Carbon::parse($row['start_date']);
                $edate = Carbon::parse($row['end_date']);
                $totalDuration = $edate->diffInSeconds($sdate);
                $duration = gmdate('H:i', $totalDuration);
                $data[$key]['Name'] = $row['name'];
                $data[$key]['Region'] = $row['region'];
                $data[$key]['Date'] = $sdate->format('m-d-Y');
                $data[$key]['Duration'] = $duration;
                $data[$key]['Milegae'] = $row['mileage'];
                $data[$key]['Rate'] = $row['rate'];
            }
            $fileName = $this->getFolderDocs() . 'a.xls';
            $list = collect($data);

            $type = 'xlsx';
            return Excel::create('payroll', function($excel) use ($data) {
                $excel->sheet('mySheet', function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getFieldsList() {
        $cols = array();
        try{
            $rows = DB::getSchemaBuilder()->getColumnListing('tb_physicians');
            foreach ($rows as $key => $value) {
                $cols[$value] = $value;    
            }
        } catch (Exception $e) {
        }
        return $cols;
    }


}
