<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ReportController
* Description   : 
*
* Created date  : 2018-09-08 
* Created time  : 06:00 AM IST
* Author        : Rishwanth
* 
name
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;
use DB;
use Carbon;
use Excel;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Pagination\Paginator;

use App\Users;
use App\Model\PortalLogsModel;

use App\User;

class ReportController_old extends Controller {

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function reportTimesheet(Request $request) {
        try{
            $employeeId = (int) $request->get('employeeId');
            
            $sql = "select P.full_name, CL.*, AP.status as ApptStatus,  AP.provider_id, AP.appointment_type,  E.name as employee_name, E.employee_type, AP.desc, E.speciality, PR.name as ClientName, PR.speciality as ServiceLine, E.payroll_employee_num, AP.start_date, AP.end_date  ";
            $sql .= " from  " . $this->mTables['CLAIM'] . " CL, " . $this->mTables['APPOINTMENT']. " AP, ". $this->mTables['EMPLOYEE'] . " E, " . $this->mTables['PRACTICE'] . " PR, " . $this->mTables['PATIENT'] . " P WHERE PR.id=E.practice_id AND CL.app_id=AP.id and AP.provider_id=E.id and AP.provider_id=E.id AND P.id=AP.patient_id  ";
            if ($employeeId > 0){
                $sql .= " AND AP.provider_id=$employeeId ";
            }
            $sql .= " order by E.name, P.full_name, AP.start_date ";
            //$sql .= " LIMIT 10 ";
            $rows = DB::select($sql);
            if (count($rows) < 1){
                $this->mStatus = 0;
                $this->mMessage = 'Records not found';
                $this->mData = [];
                return $this->sendResponseDefault();
            }
            $data = [];
            foreach ($rows as $key => $row1) {
                $row = (array) $row1;
                $sdate = Carbon::parse($row['start_date']);
                $edate = Carbon::parse($row['end_date']);
                $totalDuration = $edate->diffInSeconds($sdate);
                $duration = gmdate('H:i', $totalDuration);
                $data[$key]['ID'] = $row['payroll_employee_num'];
                $data[$key]['ApptStatus'] = $row['ApptStatus'];
                $data[$key]['Employee'] = $row['employee_name'];
                if ($row['employee_type'] == 'Full Time'){
                    $data[$key]['ExemptStatus'] = 'Exempt';
                    $data[$key]['PayType'] = 'Salary';
                } else {
                    $data[$key]['ExemptStatus'] = 'Non-Exempt';
                    $data[$key]['PayType'] = 'Hourly';
                }
                $data[$key]['DateOfService'] = $sdate->format('m-d-Y');
                $data[$key]['StartTime'] = $sdate->format('H:i');
                $data[$key]['EndTime'] = $edate->format('H:i');
                $data[$key]['Duration'] = $duration;
                $data[$key]['Rate'] = number_format($row['charge'],2);
                $data[$key]['Miles'] = '#';
                $totalDuration =  $totalDuration / 60;
                $data[$key]['MileageRate'] = number_format($row['charge'] * $totalDuration,2);
                $data[$key]['ApptNotes'] = $row['desc'];
                $data[$key]['AppointmentType'] = $row['appointment_type'];
                $data[$key]['Paycode'] = 'Paycode';
                $data[$key]['PaycodeName'] = 'PaycodeName';
                $data[$key]['ServiceLine'] = $row['ServiceLine'];
                $data[$key]['ServiceName'] = $row['speciality'];
                $data[$key]['ClientName'] = $row['full_name'];
            }
            $type = 'xlsx';
            $fileName = 'timesheet';
            return Excel::create($fileName, function($excel) use ($data) {
                $sheetName = 'timesheet';
                $excel->sheet($sheetName, function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function reportPayroll(Request $request) {
        try{
            $employeeId = (int) $request->get('employeeId');
            
            $paycode = ", CL.units as mileage ";
            $paycode .= ", CL.total as rate ";

            $sql = "select AP.provider_id as EmployeeId, P.full_name, CL.*, AP.status as ApptStatus,  AP.provider_id, AP.appointment_type,  E.name as employee_name, E.employee_type, AP.desc, E.speciality, PR.name as ClientName, PR.speciality as ServiceLine, E.payroll_employee_num, E.city as Location, AP.start_date, AP.end_date , rand() as rnd " . $paycode;
            $sql .= " from  " . $this->mTables['CLAIM'] . " CL, " . $this->mTables['APPOINTMENT']. " AP, ". $this->mTables['EMPLOYEE'] . " E, " . $this->mTables['PRACTICE'] . " PR, " . $this->mTables['PATIENT'] . " P WHERE PR.id=E.practice_id AND CL.app_id=AP.id and AP.provider_id=E.id and AP.provider_id=E.id AND P.id=AP.patient_id  ";
            if ($employeeId > 0){
                $sql .= " AND AP.provider_id=$employeeId ";
            }
            $sql .= " order by E.name, E.id, P.full_name, P.id, CL.from_date ";
            //$sql .= " LIMIT 100 ";

            $rows = DB::select($sql);
            if (count($rows) < 1){
                $this->mStatus = 0;
                $this->mMessage = 'Records not found';
                $this->mData = [];
                return $this->sendResponseDefault();
            }
            $data = [];
            $previousEmployeeID = 0;
            $sno = -1;
            $key = -1;
            $sheetNameStart = '';
            $sheetNameEnd = '';
            $EmployeeId = 0;

            foreach ($rows as $key1 => $row1) {
                $row = (array) $row1;
                $sdate = Carbon::parse($row['start_date']);
                $edate = Carbon::parse($row['end_date']);
                $totalDuration = $edate->diffInSeconds($sdate);
                $duration = gmdate('H:i', $totalDuration);
                
                $EmployeeId = $row['EmployeeId'];
                if ($previousEmployeeID != $EmployeeId){
                    $sno++;
                    $key = $sno;
                    $data[$key]['ID'] = $row['payroll_employee_num'];
                    $data[$key]['Name'] = $row['employee_name'];
                    if ($row['employee_type'] == 'Full Time'){
                        $data[$key]['ExemptStatus'] = 'Exempt';
                        $data[$key]['PayType'] = 'Salary';
                    } else {
                        $data[$key]['ExemptStatus'] = 'Non-Exempt';
                        $data[$key]['PayType'] = 'Hourly';
                    }
                    $data[$key]['Location'] = $row['Location'];
                    $data[$key]['Date'] = $sdate->format('m-d-y');
                    $data[$key]['DateEnd'] = $sdate->format('m-d-y');
                    $data[$sno]['RegHours'] = 0;
                    $data[$key]['Rate'] = 0;
                    $data[$key]['Milegae'] = 0;
                    $data[$key]['AvgRegRate'] = 0;
                    $data[$key]['RTPay'] = 0;
                    if (empty($sheetNameStart)){
                        $sheetNameStart = $data[$key]['Date'];
                    }
                }

                $totalDuration = $totalDuration / 60;

                $th = $data[$key]['RegHours'] + $totalDuration;
                
                $m = (float) $row['mileage'];
                $m = $m +  ($m * $row['rnd']);
                $tm = $data[$key]['Milegae'] + $m;

                $r = (float)  $row['rate'];
                $r = $r +  ($r * $row['rnd']);
                $tr = $data[$key]['Rate'] + $r;

                $arr =  $tr + $tm;

                $data[$key]['RegHours'] = (float) number_format($th,2);
                $data[$key]['Milegae'] = (float) number_format($tm,2);
                $data[$key]['Rate'] = (float) number_format($tr,2);
                $data[$key]['AvgRegRate'] = (float) $data[$key]['Milegae'] + $data[$key]['Rate'];
                $t = ($data[$key]['AvgRegRate'] * $data[$key]['RegHours']);
                $data[$key]['RTPay'] = number_format(($data[$key]['AvgRegRate'] * $data[$key]['RegHours']),2);
                $sheetNameEnd = $sdate->format('m-d-y');
                $data[$key]['DateEnd'] = $sheetNameEnd;
                $previousEmployeeID = $EmployeeId;
            }
            $fileName = 'payroll';
            $type = 'xlsx';
            $sheetName = 'payroll-' . $sheetNameStart . '-to-' . $sheetNameEnd;
            return Excel::create($fileName, function($excel) use ($data, $sheetName) {
                $excel->sheet($sheetName, function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }




    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function reportDepositDetailsInsurance(Request $request) {
        try{
            $data = $this->getDummyDataDDI();
           
            $fileName='Deposit Details Insurance';

            $type = 'xlsx';
            return Excel::create($fileName, function($excel) use ($data) {
                $sheetName='Deposit Details Insurance';
                $excel->sheet($sheetName, function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }



    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    function getDummyDataALI(){
        $arr = array('Patient', 'DOB', 'Provider', 'SupervisorProvider',  'Location', 'DOS', 'CPT', 'units',  'Datebilled', 'name', 'Insuranceid', 'BilledAmount', 'AllowedAmount',  'Pri Insurance Paid',  'Pri Adjustment',  'SecondaryInsurancePaid',  'Secondary Adjustment', 'GuarantorPaid', 'copay', 'coinsurance', 'deductable',  'Denial Reason Code', 'Reason Desc', 'Claim', 'DepositDate', 'InstrumentNumber', 'DepositNotes', 'Aging Category Name', 'Aging Notes', 'Notes Worked Date',  'Notes Follow up Date');
        $data = [];
        for ($i=1; $i<=10; $i++){
            foreach ($arr as $key => $value) {
                $data[$i-1][$value] = $value . $i;
            }
        }
        return $data;
    }

    function getDummyDataDDI(){
        $arr = array('depositid', 'Date Of Deposit', 'Create Date', 'Payor Type', 'Payor Name', 'Check/EFT Number', 'Deposit Amount', 'PaymentAmount', 'notes', 'unappliedamount', 'facilityid', 'checkdate', 'clientdepositdate', 'PatientName', 'DOS');
        $data = [];
        for ($i=1; $i<=10; $i++){
            foreach ($arr as $key => $value) {
                $data[$i-1][$value] = $value . $i;
            }
        }
        return $data;
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    function getDataForAccountsLedgerInsurance(){
        $sql = "select P.full_name as Patient, E.name as Provider, AP.* from " . $this->mTables['APPOINTMENT'] . " AP, " . $this->mTables['PATIENT'] . " P, " . $this->mTables['EMPLOYEE']. " E where P.id=AP.patient_id and AP.provider=E.id ";
        $rows = DB::select($sql);

        $arr = array('Patient', 'DOB', 'Provider', 'SupervisorProvider',  'Location', 'DOS', 'CPT', 'units',  'Datebilled', 'name', 'Insuranceid', 'BilledAmount', 'AllowedAmount',  'Pri Insurance Paid',  'Pri Adjustment',  'SecondaryInsurancePaid',  'Secondary Adjustment', 'GuarantorPaid', 'copay', 'coinsurance', 'deductable',  'Denial Reason Code', 'Reason Desc', 'Claim', 'DepositDate', 'InstrumentNumber', 'DepositNotes', 'Aging Category Name', 'Aging Notes', 'Notes Worked Date',  'Notes Follow up Date');
        return $rows;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function reportClients(Request $request) {
        try{
            $patientId = (int) $request->get('patientId');
            
            $sql = "select P.*  ";
            $sql .= " from  " . $this->mTables['PATIENT'] . " P WHERE 1=1   ";
            if ($patientId > 0){
                $sql .= " AND P.id=$patientId";
            }
            $sql .= " order by P.full_name ";
            $rows = DB::select($sql);
            if (count($rows) < 1){
                $this->mStatus = 0;
                $this->mMessage = 'Records not found';
                $this->mData = [];
                return $this->sendResponseDefault();
            }
            $data = [];
            foreach ($rows as $key => $row1) {
                $row = (array) $row1;
                $dob = Carbon::parse($row['dob']);
                $dob = $dob->format('m-d-Y');
                $data[$key]['Status'] = $row['is_active'] == 1 ? 'Active' : 'Inactive' ;
                $data[$key]['Full Name'] = $row['full_name'];
                $data[$key]['Gender'] = $row['gender'];
                $data[$key]['DOB'] = $dob;
                $data[$key]['Language'] = '';
                $data[$key]['Garuntor Last Name'] = '';
                $data[$key]['Zone'] = '';
                $data[$key]['Email'] = $row['email'];
                $data[$key]['Location'] = $row['addr_state'];
                $data[$key]['Address1'] = $row['addr_street1'];
                $data[$key]['Address2'] = $row['addr_street2'];
                $data[$key]['City'] = $row['addr_city'];
                $data[$key]['State'] = $row['addr_state'];
                $data[$key]['Zip'] = $row['addr_zip'];
                $data[$key]['Phone Home'] = $row['phone_home'];
                $data[$key]['Phone Work'] = $row['phone_work'];
                $data[$key]['Cell'] = $row['phone_mobile'];
                $data[$key]['Fax'] = 'no field in table';
                $data[$key]['Latest Payor'] = '';
                $data[$key]['Last Scheduled Session'] = '';
                $data[$key]['Last Rendered Session'] = '';
                $data[$key]['Acct#'] = '';

                $sql1 = "select CL.from_date, CL.auth_num, CL.claim_number, I.payer_name FROM " . $this->mTables['CLAIM'] . " CL, " . $this->mTables['INSURANCE'] . " I, " . $this->mTables['APPOINTMENT'] . " AP where CL.app_id=AP.id AND CL.insurance_id = I.id AND AP.patient_id=" . $row['id'] . " order by CL.from_date desc LIMIT 1";
                $rows1 = DB::select($sql1);
                foreach ($rows1 as $key => $row2) {
                    $row2 = (array) $row2;
                    $data[$key]['Latest Payor'] = $row2['payer_name'];
                    $from_date = Carbon::parse($row2['from_date']);
                    $from_date = $from_date->format('m-d-Y H:i');
                    $data[$key]['Last Scheduled Session'] = $from_date;
                    $data[$key]['Last Rendered Session'] = $from_date;
                    $accountNum = $row2['auth_num'] . '/' .$row2['claim_number'];
                    $data[$key]['Acct#'] = $accountNum;
                }
            }
            $type = 'xlsx';
            $fileName = 'Client Details';
            return Excel::create($fileName, function($excel) use ($data) {
                $sheetName = 'Client Details';
                $excel->sheet($sheetName, function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function reportEmployee(Request $request) {
        try{
            $employeeId = (int) $request->get('employeeId');
            
            $sql = "select E.*  ";
            $sql .= " from  " . $this->mTables['EMPLOYEE'] . " E WHERE 1=1   ";
            if ($employeeId > 0){
                $sql .= " AND E.id=$employeeId";
            }
            $sql .= " order by E.name ";
            $rows = DB::select($sql);
            if (count($rows) < 1){
                $this->mStatus = 0;
                $this->mMessage = 'Records not found';
                $this->mData = [];
                return $this->sendResponseDefault();
            }
            $data = [];
            foreach ($rows as $key => $row1) {
                $row = (array) $row1;
                $dob = Carbon::parse($row['dob']);
                $dob = $dob->format('m-d-Y');
                $data[$key]['ID'] = $row['payroll_employee_num'];
                $data[$key]['Full Name'] = $row['name'];
                $data[$key]['DOB'] = $dob;
                $data[$key]['Title'] = 'Nil';
                $data[$key]['Highest Degree'] = 'Nil';
                $data[$key]['License Number'] = $row['driver_license_number'];
                $data[$key]['Email'] = $row['phy_email'];
                $data[$key]['NPI'] = $row['individual_npi'];
                $data[$key]['Phone Home'] = $row['phone_home'];
                $data[$key]['Phone Work'] = $row['phone_work'];
                $data[$key]['Phone Mobile'] = $row['phone_mobile'];
                $data[$key]['Fax'] = $row['fax'];;
                $data[$key]['Address Line 1'] = $row['address_1'];
                $data[$key]['Address Line 2'] = $row['address_2'];
                $data[$key]['Zip'] = $row['zip'];
                $data[$key]['City'] = $row['city'];
                $data[$key]['State'] = 'Nil';
                $data[$key]['Country'] = $row['country_name'];
            }
            $type = 'xlsx';
            $fileName = 'Provider Details';
            return Excel::create($fileName, function($excel) use ($data) {
                $sheetName = 'Provider Details';
                $excel->sheet($sheetName, function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }    

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function reportDeposits(Request $request) {
        try{
            $payorId = (int) $request->get('payorId');
            
            $sql = " (select full_name from " . $this->mTables['PATIENT'] . " P where P.id=D.patient_id) as full_name ";
            $sql = " select $sql, D.*, I.payer_name";
            $sql .= ", (select sum(amount_adjusted) from " .  $this->mTables['DEPOSIT_DETAILS'] . " DD where DD.deposit_id=D.id and DD.is_deleted=0) as Balance ";           
            $sql .= " from  " . $this->mTables['DEPOSIT'] . " D Left Join  ";
            $sql .= $this->mTables['INSURANCE'] . " I on D.payor_id=I.id ";
            if ($payorId > 0){
                $sql .= " AND I.id=$payor_id";
            }
            $sql .= " order by I.payer_name ";
            //return $sql;
            $rows = DB::select($sql);
            if (count($rows) < 1){
                $this->mStatus = 0;
                $this->mMessage = 'Records not found';
                $this->mData = [];
                return $this->sendResponseDefault();
            }
            $data = [];
            foreach ($rows as $key => $row1) {
                $row = (array) $row1;
                $date = Carbon::parse($row['date']);
                $date = $date->format('m-d-Y');
                $created_at = Carbon::parse($row['created_at']);
                $created_at = $created_at->format('m-d-Y');
                $cheque_date = Carbon::parse($row['cheque_date']);
                $cheque_date = $cheque_date->format('m-d-Y');
                $Balance = (float) '0' . $row['Balance'];

                $data[$key]['Deposit ID'] = $row['id'];
                $data[$key]['Date Of Deposit'] = $date;
                $data[$key]['Create Date'] = $created_at;
                $data[$key]['Payor Type'] = $row['payor_type'];
                $data[$key]['Name'] = $row['payer_name'];
                if ($row['payor_type'] == 2) {
                    $data[$key]['Name'] = $row['full_name'];
                }
                $data[$key]['Check/EFT Number'] = $row['cheque_num'];
                $data[$key]['Deposit Amount'] = $row['amount'];
                $data[$key]['Payment Amount'] = $row['amount'];
                $data[$key]['Notes'] = $row['notes'];
                $data[$key]['Un Applied Amount'] = $row['amount'] - $Balance;
                $data[$key]['Check Date'] = $row['cheque_date'];
            }
            $type = 'xlsx';
            $fileName = 'Deposits';
            return Excel::create($fileName, function($excel) use ($data) {
                $sheetName = 'Deposits';
                $excel->sheet($sheetName, function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }    

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function reportAuth(Request $request) {
        try{
            $patientId = (int) $request->get('patientId');
            
            $sql = "select P.*, I.payer_name, CL.from_date, CL.to_date, CL.auth_num, CL.units, CL.from_date as detailStart, CL.to_date as detailEnd, CL.copay,  CL.place_of_service, CL.proced, 
                CL.diag1, CL.diag2, CL.diag3, CL.diag4, CL.deductible ";
            $sql .= ", E.name as employee_name ";
            $sql .= ", AU.start_date as startDate, AU.end_date as endDate, AU.policy_entity "; 
            $sql .= ", A.auth_type, A.per1, A.billed_per_time, A.rates, A.max1, A.max2, A.max3 ";
            $sql .= ", CA.freq_period, CA.freq_count, CA.facility ";
            $sql .= ", '???' as WHICH ";
            $sql .= " from  " . $this->mTables['PATIENT'] . " P, ";
            $sql .= $this->mTables['CASE'] . " CA, ";
            $sql .= $this->mTables['CLAIM'] . " CL, ";
            $sql .= $this->mTables['INSURANCE'] . " I, ";
            $sql .= $this->mTables['ACTIVITY'] . " A, ";
            $sql .= $this->mTables['AUTH'] . " AU, ";
            $sql .= $this->mTables['EMPLOYEE'] . " E, ";
            $sql .= $this->mTables['APPOINTMENT'] . " AP ";
            $sql .= " WHERE 1=1 and CL.case_id=CA.id AND CA.patient_id=P.id  AND CL.insurance_id=I.id AND AU.case_id=CA.id AND A.case_id=CA.id AND AP.provider_id=E.id AND A.id=CL.app_id ";
            if ($patientId > 0){
                $sql .= " AND P.id=$patientId";
            }
            //$sql .= " GROUP by P.id, E.id ";
            $sql .= " order by CL.from_date, P.full_name, E.name ";
            //$sql .= " LIMIT 100";
            //return $sql;
            $rows = DB::select($sql);
            if (count($rows) < 1){
                $this->mStatus = 0;
                $this->mMessage = 'Records not found';
                $this->mData = [];
                return $this->sendResponseDefault();
            }

            $data = [];
            foreach ($rows as $key => $row1) {
                $row = (array) $row1;
                $dob = Carbon::parse($row['dob']);
                $dob = $dob->format('m-d-Y');
                $startDate = Carbon::parse($row['startDate']);
                $startDate = $startDate->format('m-d-Y');
                $endDate = Carbon::parse($row['created_at']);
                $endDate = $endDate->format('m-d-Y');
                $detailStart = Carbon::parse($row['detailStart']);
                $detailStart = $detailStart->format('m-d-Y H:i');
                $detailEnd = Carbon::parse($row['detailStart']);
                $detailEnd = $detailEnd->format('m-d-Y H:i');

                $data[$key]['Patient'] = $row['full_name'];
                $data[$key]['Physician'] = $row['employee_name'];
                $data[$key]['Supervisor'] = "???";
                $data[$key]['Zone'] = "???";
                $data[$key]['UCI Insurance Num'] = "???";
                $data[$key]['Vendor Num'] = "???";
                $data[$key]['Auth Num'] = $row['auth_num'];
                $data[$key]['Start Date'] = $startDate;
                $data[$key]['End Date'] = $endDate;
                $data[$key]['Payor'] = $row['payer_name'];
                $data[$key]['Detail Start'] = $detailStart;
                $data[$key]['Detail End'] = $detailEnd;
                $data[$key]['Activity Type'] = $row['auth_type'];
                $data[$key]['Sub Type'] = $row['WHICH'];
                $data[$key]['CPT code'] = $row['proced'];
                $data[$key]['Billing Modifier'] = $row['proced'];
                $data[$key]['Detail Auth Num'] = $row['auth_num'];
                $data[$key]['Rate Per'] = $row['per1'];
                $data[$key]['Unitin Mins'] = $row['billed_per_time'];
                $data[$key]['Rate'] = $row['rates'];
                $data[$key]['Maxby'] = $row['max1'] . ' / ' . $row['max2'] . '/' . $row['max3'] ;
                $data[$key]['Freq'] = $row['freq_period'] . '/' . $row['freq_count'];
                $data[$key]['Value'] = $row['WHICH'];
                $data[$key]['Gender'] = $row['gender'];
                $data[$key]['DOB'] = $dob;
                $data[$key]['Address1'] = $row['addr_street1'];
                $data[$key]['Address2'] = $row['addr_street2'];
                $data[$key]['City'] = $row['addr_city'];
                $data[$key]['State'] = $row['addr_state'];
                $data[$key]['Zip'] = $row['addr_zip'];
                $data[$key]['Phone 1'] = $row['phone_home'];
                $data[$key]['Phone 2'] = $row['phone_work'];
                $data[$key]['isprimaryauth'] = $row['WHICH'];
                $data[$key]['Auth Details Insurance ID'] = $row['policy_entity'];
                $data[$key]['copay'] = $row['copay'];
                $data[$key]['Deductable'] = $row['deductible'];
                $data[$key]['Place Holder'] = $row['place_of_service'];
                $data[$key]['Diagnosis 1'] = $row['diag1'];
                $data[$key]['Diagnosis 2'] = $row['diag2'];
                $data[$key]['Diagnosis 3'] = $row['diag3'];
                $data[$key]['Diagnosis 4'] = $row['diag4'];
                $data[$key]['Split Type'] = $row['WHICH'];
                $data[$key]['Facility ID'] = $row['facility'];
                $data[$key]['External Acct Number'] = '???';
                $data[$key]['Custom 1'] = $row['WHICH'];
                $data[$key]['Custom 2'] = $row['WHICH'];
            }
            $type = 'xlsx';
            $fileName = 'Auth Details';
            return Excel::create($fileName, function($excel) use ($data) {
                $sheetName = 'Auth Details';
                $excel->sheet($sheetName, function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }    

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function reportAccountLedgerInsurance(Request $request) {
        try{
            $data = $this->getDummyDataALI();
           
            $fileName='Account Ledger Insurance';

            $type = 'xlsx';
            return Excel::create($fileName, function($excel) use ($data) {
                $sheetName='Account Ledger Insurance';
                $excel->sheet($sheetName, function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

}
