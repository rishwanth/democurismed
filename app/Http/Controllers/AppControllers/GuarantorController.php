<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : GuarantorController
* Description   : 
*
* Created date  : 2018-08-01 
* Created time  : 06:00 AM IST
* Author        : Rishwanth
* 
* History       : 2018-04-04 07:30 PM IST, Rishwanth, Code clean up
* History       : 2018-04-04 08:00 PM IST, Rishwanth, renamed the function name
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Users;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Exception;
use App\Model\GuarantorModel;

class GuarantorController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }



    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        $response =  array('status' => 0, 'message' => 'Add Failed', 'data' => array());
        try {

            $input['patient_id'] = 'patientID';
            $input['street'] = 'guarantorAddrStreet';
            $input['dob'] = 'guarantorDOB';
            $input['city'] = 'guarantorAddrCity';
            $input['state'] = 'guarantorAddrState';
            $input['zip'] = 'guarantorAddrZip';
            $input['name'] = 'guarantorName';
            $input['relationship'] = 'guarantorRelationship';
            $input['phone_home'] = 'guarantorPhoneHome';

            $obj = new GuarantorModel();
            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $$tblField = $request->get($formField);
                $obj->$tblField = $$tblField;
            }
            if ($obj->save()){
                if ($auth->id > 0) {
                    $response =  array('status' => 1, 'message' => 'Guarantor added successfully', 'data' => array());
                } else {
                    $response =  array('status' => 0, 'message' => 'Guarantor added failed', 'data' => array());
                }
            }
            $response['message'] = $e->getMessage();
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }
        return json_encode($response);      
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request) {
        $id = $request->get('id');
        if (empty($id)){
            $rows = GuarantorModel::select('*')
                        ->get();
        } else {
            $rows = GuarantorModel::where('id','=', $id)
                        ->select('*')
                        ->get();
        }
        $message = 'failed';
        $data = array();
        $status = 0;
        $message = 'Empty';
        if ($id > 0) {
            $message = " Records not found for #$id.";
        }
        if ($rows->count()){
            $rows =  $rows;
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }

    public function get(Request $request){
        $id = $request->get('id');
        if (empty($id)){
            $rows = GuarantorModel::select('*')
                        ->get();
        } else {
            $rows = GuarantorModel::where('id','=', $id)
                        ->select('*')
                        ->get();
        }
        $message = 'failed';
        $data = array();
        $status = 0;
        $message = 'Empty';
        if ($id > 0) {
            $message = "Records not found for #$id.";
        }
        if ($rows->count()){
            $rows =  $rows;
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }
}
