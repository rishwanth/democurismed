<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PhysiciansController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 06:00 PM IST
* Author        : Rishwanth
* 

********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;
use QueryException;
use DB;
use Session;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\DB;
//use Illuminate\Database\QueryException;
use App\Model\PhysiciansModel;
use App\Users;

class PhysiciansController_1 extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request, $id = 0) {
        $input[''] = '';
        $input['name'] = 'PrimaryPhysiName';
        $input['dob'] = 'phyDOB';
        $input['individual_npi'] = 'phyNPI';
        $input['ssno'] = 'phySSN';
        $input['address'] = 'phyAddr';
        $input['phone_home'] = 'phyHomePhone';
        $input['phone_work'] = 'phyWorkPhone';
        $input['phone_mobile'] = 'phyMobilePhone';
        $input['email'] = 'phyEmail';
        $input['fax'] = 'phyFax';
        $input['notes'] = 'phyNotes';
        $input['speciality'] = 'speciality';
        $input['practice_id'] = 'practiceID';

        foreach ($input as $formField => $tblField) {
            if (empty($formField)){
                continue;
            }
            $$tblField = $request->get($formField);
        }

        $response =  array('status' => 0, 'message' => 'Add Failed', 'data' => array());

        try {
            
            $obj = new PhysiciansModel();
            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $obj->$tblField = $$tblField;
            }
            if($id == 0){
                $obj->practice_id = $this->getPracticeID();
            }
            if ($obj->save()){
                if ($auth->id > 0) {
                    $response =  array('status' => 1, 'message' => 'Physicians added successfully', 'data' => array());
                } else {
                    $response =  array('status' => 0, 'message' => 'Physicians added failed', 'data' => array());
                }
            }
            $response['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();
        }
        return json_encode($response);      
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        try{
            $id = (int) $request->get('physicianID');
            if ($id < 1){
                $this->mMessage = 'Physician ID is missing';
                return $this->sendResponseDefault();
            }
            return $this->save($request, $id);
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request) {
        try{
            $physicianID = (int) $request->get('physicianID');
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');

            $sql = " 1=1 ";
            if ($physicianID > 0){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " id='$physicianID' ";
            }
            if ($practiceID > 0){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " practice_id='$practiceID' ";
            }
            $rows = PhysiciansModel::whereRaw($sql)
                        ->select('*')
                        ->get();

            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Record not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function get(Request $request){
        $practiceID = $this->getPracticeID();
        $term = $request->get('term');
        $sql = "select concat(id, ' - ' ,name) as `key`, concat(id, ' - ' ,name) as label from tb_physicians where practice_id=$practiceID ";
        if (strlen($term) > 0) {
            $sql .= " and (name like '%" . $term . "%') ";
        }
        $rows = DB::select($sql);
         if (count($rows) > 0){
             $this->mData = json_encode($rows);
             $this->mMessage ='Success';
             $this->mStatus = 1;
         } 
        return json_encode($rows);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function deletePhysicians(Request $request){
        try{
            $id = (int) $request->get('id');
            if ($id < 1){
                $this->mMessage = "Physicians is required";
                return $this->sendResponseDefault();
            }
            $result = PhysiciansModel::where('id', $id)
                        ->delete();
            if ($result){
                $this->mStatus = 1;
                $this->mMessage = "Deleted #id $id successfully";
            } else {
                $this->mMessage = 'Deleted failed';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list1(Request $request) {
        try{
            $id = (int) $request->get('id');
            $physicianID = $request->get('physicianID');
            if (!empty($physicianID)){
                $id = $physicianID;
            }

            $sql = " where 1=1 ";
            if (!empty($id)){
                $c = "PH.id = '$id'";
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= $c;
            }
            $sql = "select PH.name as `label`, concat(PH.id, ' - ' ,PH.name) as `key`, PH.* from tb_physicians PH " . $sql;

            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list_all(Request $request) {
        try{
            $practiceID = Session::get('LOGGED_USER_PRATICE_ID');
            $rows = PhysiciansModel::where('practice_id', $practiceID)
                        ->select('*')
                        ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getPrimary(Request $request) {
        try{
            $sql = "select concat(id, ' - ', name) as label from tb_physicians";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getInfo(Request $request) {
        try{
            $physicianID = (int) $request->get('physicianID');
            $sql = "select * from tb_physicians where id='$physicianID'";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getFacilityInfo(Request $request) {
        try{
            $sql = "select id as serviceLocID, internal_name as internalName from tb_service_locations";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
   }
}
