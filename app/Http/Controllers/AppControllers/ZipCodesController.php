<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ZipCodesController
* Description   : 
*
* Created date  : 2018-08-31 
* Created time  : 04:00 AM IST
* Author        : Rishwanth
* 

********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;
use Validator;
use QueryException;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Users;
use App\Model\ZipcodesModel;

class ZipCodesController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        $id = (int) $request->get('zipcodesID');
        if ($id < 1) {
            $this->mMessage = 'Zip codes ID is required';
            return $this->sendResponseDefault();
        }
        return $this->save($request, 0);
    }

   
    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request, $id = 0) {
        try{
            $state = $request->get('state');
            $city = $request->get('city');
            $zip = $request->get('zip');
            if (empty($state)){
                $this->mMessage = "State name is required";
                return $this->sendResponseDefault();
            }
            if (empty($city)){
                $this->mMessage = "City is required";
                return $this->sendResponseDefault();
            }
            if (empty($zip)){
                $this->mMessage = "Zip is required";
                return $this->sendResponseDefault();
            }
            if ($id > 0) {
                $zipcodes = ZipcodesModel::find($id);
            } else {
                $zipcodes = new ServiceLocationsModel();
            }
            $zipcodes->state = $state;
            $zipcodes->city = $city;
            $zipcodes->zip = $zip;
            if ($zipcodes->save()){
                if ($zipcodes->id > 0){
                    $this->mMessage = "zipcodes saved Success";
                    $this->mStatus =1;
                    $this->mData=array('id' => $zipcodes->id, 'zipcodesID' => $zipcodes->id);
                }
            } else {
                $this->mMessage = "zipcodes saved Failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $this->getDBError($e);
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getZipInfo(Request $request){
        try{
            $zip = $request->get('zip');
            if (empty($zip)){
                $this->mMessage = 'Zip is required';
                return $this->sendResponseDefault();
            }

            $rows = ZipcodesModel::where('zip','=', $zip)
                        ->select('*')
                        ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $this->getDBError($e);
        }
        return $this->sendResponseDefault();
    }

}
