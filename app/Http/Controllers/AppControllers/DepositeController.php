<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : DepositeController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 11:00 PM IST
* Author        : Rishwanth
* 

DB::beginTransaction();
DB::commit();
DB::rollback();
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;
use Session;
use DB;
use Validator;
use QueryException;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Users;
use App\Model\DepositDetailsModel;
use App\Model\DepositModel;
use App\Model\PracticesModel;
use App\Model\InsurancesModel;
use App\Model\PatientsModel;
use App\Model\ClaimsModel;
use App\Model\ClaimsSubmissioActivityModel;

class DepositeController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
        return view('deposits');
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function deposits() {
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function post() {
        return view('deposit_post');
    }


    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update() {
        try{
            $deposit_id = (int) $request->get('depositID');
            if ($deposit_id < 1){
                $this->mMessage = 'Deposite id is missing';
                return $this->sendResponseDefault();
            }
            $objModel = DepositModel::find($deposit_id);
            if (empty($objModel) || is_null($objModel)) {
                $this->mMessage = 'Record not found';
                return $this->sendResponseDefault();
            }

            $input[''] = '';
            //$input['id'] = 'depositID';
            $input['cheque_num'] = 'chequeNo';
            $input['cheque_date'] = 'chequeDate';
            $input['payor_type'] = 'payorType';
            $input['payment_type'] = 'paymentType';
            $input['payor_id'] = 'payorID';
            $input['amount'] = 'amount';
            $input['date'] = 'depositDate';
            $input['notes'] = 'notes';

            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) ==0 || strlen($formField) == 0){
                    continue;
                }
                $objModel->$tableField = $request->get($formField);
            }
            if ($objModel->save()){
                $this->mStatus = 1;
                $this->mMessage = "Saved Success";
            } else {
                $this->mMessage = "Saved failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function get(Request $request){
        try{
            $id = (int) $request->get('depositID');
            $sql = '  where 1=1 ';
            if ($id > 0){
                $c = " id = $id";
                if (strlen($sql) > 0) {
                    $sql .= " and ";
                }
                $sql .= $c;
            }
            $sql = "select * from tb_deposits $sql";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function check(Request $request){
        try{
            $id = (int) $request->get('depositID');
            $rows = DepositDetailsModel::where('id', $id);
            $this->mStatus = 1;
            if ($rows->count() > 0) {
                $this->mData = array('status' => 'green');
            } else {
                $this->mData = array('status' => 'red');
            }
            $this->mMessage = 'Success';
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function delete(Request $request){
        try{        
            $id = (int)$request->get('depositID');
            $rows = DepositModel::find($id);
            if ($rows) {
                if ($rows->delete()){
                    $this->mMessage = 'Deleted Success';
                } else {
                    $this->mMessage = 'Deleted failed';
                }
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function total(Request $request){
        try{
            $claimID = (int) $request->get('claimID');
            $paid = (float) $request->get('paid');
            $adjust = (float) $request->get('adjust');

            $tot  = 0;
            $sql = "SELECT sum(amount_adjusted) + sum(adjustment) as final FROM tb_deposit_details WHERE claim_id='$claimID'";
            $rows = DB::select($sql);

            if(count($rows) > 0){
                $rows =  $rows;
                foreach ($rows as $row) {
                    $tot = (float) $row->final;
                }
                $tot = $tot + $paid + $adjust;
            }
            else{
                $tot = $paid + $adjust + 0; 
            }
            $this->mData = array('total' => $tot);
            $this->mStatus = 1;
            $this->mMessage = 'Success';
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function loadDeposits(Request $request){
        try{
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            if ($practiceID < 1) {
                $this->mMessage = 'Invalid Practice ID';
                return $this->sendResponseDefault();
            }

            $practice = PracticesModel::find($practiceID);
            if (empty($practice)) {
                $this->mMessage = 'Practice ID not found';
                return $this->sendResponseDefault();
            }

            $sql = "select D.*, date(D.date) as depositDate, date(D.cheque_date) as chequeDate, I.payer_name, (select full_name from tb_patients where id=D.patient_id) as patient_name ";
            $sql .= ", (D.amount - (select sum(amount_adjusted) from tb_deposit_details DD where DD.deposit_id=D.id and DD.is_deleted=0)) as Balance ";
            $sql .= " from tb_deposits D, tb_insurances I ";
            $sql .= " where I.id=D.payor_id and D.practice_id='$practiceID' and D.is_deleted=0 ";


            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mData = $rows;
                $this->mMessage = "Success";
                $this->mStatus = 1;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function createDeposit(Request $request) {
        try{
            $validator = Validator::make($request->all(), ['amount'=>'required','chequeNo'=>'required','chequeDate'=>'required','payorID'=>'required','payorType'=>'required','paymentType'=>'required']);

            if ($validator->fails()) {
                $this->mMessage = $validator->messages();
                return $this->sendResponseDefault();
            }

            $practiceID = $this->getPracticeID();
            $practice = PracticesModel::find($practiceID);
            if (empty($practice)) {
                $this->mMessage = 'Invalid Practice ID';
                return $this->sendResponseDefault();
            }
            $insurance = "";
            $payorType = $request->get('payorType');
            if($payorType == 1){
                $payorID = $request->get('payorID');
                $insurance = InsurancesModel::where('payer_name', $payorID)
                                ->select('id')
                                ->get();
            }
            else{
                $payorID = $request->get('payorID');
                $insurance = PatientsModel::where('full_name', $payorID)
                                ->select('id')
                                ->get();
            }

            

            $input[''] = '';
            $input['date'] = 'depositDate';
            $input['cheque_num'] = 'chequeNo';
            $input['cheque_date'] = 'chequeDate';
            $input['payor_id'] = 'payorID';
            $input['payor_type'] = 'payorType';
            $input['amount'] = 'amount';
            $input['payment_type'] = 'paymentType';
            $input['notes'] = 'notes';
            //$input['practice_id'] = 'practiceID';

            $deposit = new DepositModel();
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0){
                    continue;
                }
                $deposit->$tableField = $request->get($formField);
            }
            $deposit->practice_id = $practiceID;
            $deposit->created_by = 0;
            $deposit->is_deleted = 0;
            $deposit->payor_id = $insurance[0]->id;
            //return $insurance[0]->id;
            if ($deposit->save()){
                if ($deposit->id > 0){
                    $this->mMessage = "Deposit Created Successfully";
                    $this->mData['DepositID'] =$deposit->id;               
                    $this->mData['id'] =$deposit->id;
                    $this->mStatus = 1;
                }
            } else {
                $this->mMessage = "Deposit Created failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function adjustDeposit(Request $request) {
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
        try {
            $did = (int) $request->get('depositID');
            if ($did <= 0) { 
                $this->mMessage = 'Invalid Deposit ID';
                return $this->sendResponseDefault();
            }
            $deposit = DepositModel::where('id', $did)
                ->where('is_deleted', 0)
                ->select('id')
                ->get();
            if ($deposit->count() < 1){
                $this->mMessage = 'Invalid Deposit ID';
                return $this->sendResponseDefault();
            }

            $claimID = (int)$request->get('claimID');
            $patientID = (int)$request->get('patientID');
            $insuranceID = (int)$request->get('insuranceID');

            if($claimID<=0 && $patientID<=0){
                $this->mMessage = 'ClaimID or PatientID is necessary!';
                return $this->sendResponseDefault();
            }
            if($claimID>0 && $patientID>0){
                $this->mMessage = 'Cannot use both ClaimID and PatientID!';
                return $this->sendResponseDefault();
            }

            if($claimID>0) {
                $count = ClaimsModel::where('id', $claimID)
                        ->where('is_deleted', 0)
                        ->select('id')
                        ->get()
                        ->count();
                if ($count < 1) {
                    $this->mMessage = 'Invalid ClaimID!';
                    return $this->sendResponseDefault();
                }
            }

            if($patientID>0) {
                $count = PatientsModel::where('id', '=', $patientID)
                        ->where('is_deleted', '=', 0)
                        ->select('id')
                        ->get()
                        ->count();
                if ($count < 1) {
                    $this->mMessage = 'Invalid patientID!';
                    return $this->sendResponseDefault();
                }
            }
                       
            DB::beginTransaction();
            $depositdetID = 0;
            $response = $this->addDepositDetails($request);
            if ($response['status'] == 0) {
                DB::rollback();
                $this->mMessage = $response['message'];
                return $this->sendResponseDefault();
            }
            $depositdetID = $response['data'];

            $sql = "select CL.id ";
            $sql .= " from tb_claims CL, tb_cases CA, tb_policies PO ";
            $sql .= " where CL.case_id=CA.id and PO.case_id=CA.id and CL.id='$claimID' ";
            $rows = DB::select($sql);
            $tempFinPaid = count($rows); 

            if ($tempFinPaid >= 2) {
                $sql = "select PO.insurance_id ";
                $sql .= " from tb_claims CL, tb_cases CA, tb_policies PO ";
                $sql .= " where CL.case_id=CA.id and PO.case_id=CA.id and CL.id=$claimID and PO.policy_entity >=2 ";
                $rows = DB::select($sql);
                $tempFinPaid = 0;
                if (count($rows) > 0){
                    $tempFinPaid = $rows[0]->insurance_id;
                }

                $sqlClaimsUpdate = '';
                if ($request->has('insuranceID') && $tempFinPaid > 0){ 
                    if (strlen($sqlClaimsUpdate) > 0){
                        $sqlClaimsUpdate .= ", ";
                    }
                    $sqlClaimsUpdate .= " insurance_id='$tempFinPaid', is_sec_claim='1', status='1' ";
                    if (strlen($sqlClaimsUpdate) > 0){
                        $sqlClaimsUpdate .= "update tb_claims set $sqlClaimsUpdate where id='$claimID' and is_deleted=0";
                        DB::statement($sqlClaimsUpdate);
                    }
                }
            } 
            $sqlClaimsUpdate = " is_posted='1' ";
            if ($request->has('insuranceID')) { 
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " insurance_id='$insuranceID' ";
            }
            if ($request->has('allowed')){ 
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " allowed='" . $request->get('allowed') . "' ";
            }
            if ($request->has('paid')){ 
                $paid = (float) $request->get('paid');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " paid = paid + $paid "; 
            }
            if ($request->has('adjustment')){ 
                $adjustment = (float) $request->get('adjustment');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " adjustment = '$adjustment' "; 
            }
            if ($request->has('claimBalanceIns')){ 
                $claimBalanceIns = (float) $request->get('claimBalanceIns');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " claim_balance = '$claimBalanceIns' ";
            }
            if ($request->has('claimBalancePat')){ 
                $claimBalancePat = (float) $request->get('claimBalancePat');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " claim_balance_pat = '$claimBalancePat' ";
            }

            if ($request->has('reason')){ 
                $reason =  $request->get('reason');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " reason='$reason' ";
            }   
            if ($request->has('status')){ 
                $status = $request->get('status');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " status='$status' ";
            }
            if ($request->has('copay')){ 
                $copay = (float) $request->get('copay');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " copay='$copay' ";
            }
            if ($request->has('deductible')){ 
                $deductible = (float) $request->get('deductible');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " deductible = '$deductible' ";
            }

            if ($request->has('coins')){ 
                $coins = (float) $request->get('coins');
                if (strlen($sqlClaimsUpdate) > 0){
                    $sqlClaimsUpdate .= ", ";
                }
                $sqlClaimsUpdate .= " coins='$coins' ";
            }

            if (strlen($sqlClaimsUpdate) > 0) {
                $sql = "update tb_claims set $sqlClaimsUpdate where id='$claimID' and is_deleted=0 ";
                DB::statement($sql);
            }

            $str = 'Deposits => Deposit Detail Created. Deposit Detail ID:' . $depositdetID . ' Claim ID: ' .$claimID . ' Patient ID:' .$patientID.', DepositDetailsID:'.$depositdetID;

            $this->mMessage = 'Deposit Detail Created Successfully!';
            $this->mStatus = 1;
            $this->mData['DepositDetailID'] = $depositdetID;
            $this->addLogs(__FILE__, __FUNCTION__, $str);
            $this->addLogs(__FILE__, __FUNCTION__, 'End');
            DB::commit();
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
            $this->addLogs(__FILE__, __FUNCTION__, 'ERROR:' . $this->mMessage);
            DB::rollback();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function adjustPatDeposit(Request $request) {
        try {
            $did = (int) $request->get('depositID');
            if ($did < 1) { 
                $this->mMessage = 'Invalid Deposit ID'; 
                return $this->sendResponseDefault();
            }
            $claimID = (int) $request->get('claimID');
            $patientID = (int) $request->get('patientID');
            $insuranceID = (int) $request->get('insuranceID');

            if($claimID<=0 && $patientID<=0){
                $this->mMessage = 'ClaimID or PatientID is necessary!';
                return $this->sendResponseDefault();
            }
            if($claimID>0 && $patientID>0){
                $this->mMessage ='Cannot use both ClaimID and PatientID!';
                return $this->sendResponseDefault();
            }
            $count = DepositModel::where('id', $did)
                        ->where('is_deleted', 0)
                        ->select('id')
                        ->get()->count();
            if ( $count < 1) {
                $this->mMessage = 'Invalid Deposit ID'; 
                return $this->sendResponseDefault();
            }

            if($claimID > 0) {
                $count = ClaimsModel::where('id', $claimID)
                            ->where('is_deleted',0)
                            ->select('id')
                            ->get()
                            ->count();

                if ( $count < 1) {
                    $this->mMessage = 'Invalid ClaimID!';
                    return $this->sendResponseDefault();
                }
            }

            if($patientID > 0) {
                $count = PatientsModel::where('id', $patientID)
                            ->where('is_deleted', 0)
                            ->select('id')
                            ->get()
                            ->count();
                if ( $count < 1) {
                    $this->mMessage = 'Invalid patientID!';
                    return $this->sendResponseDefault();
                }
            }

            DB::beginTransaction();
            $depositdetID = 0;
            $response = $this->addDepositDetails($request);
            if ($response['status'] == 0) {
                DB::rollback();
                $this->mMessage = $response['message'];
                return $this->sendResponseDefault();
            }
            $depositdetID = $response['data'];

            $claims = ClaimsModel::find($claimID);
            if (!empty($claims)) {
                $row = $claims;
                $tempFinPaid = $row->paid;
                $tempBalIns = $row->claim_balance;
                $tempFinBalIns = $row->claim_balance;
                $tempBalPat = $row->claim_balance_pat;
                $tempFinBalPat = $row->claim_balance_pat;

                $claims->is_posted=1;
                if ($request->has('allowed')){ 
                    $claims->allowed = $request->get('allowed');
                }
                if ($request->has('paid')){ 
                    $paid = $request->get('paid') + $tempFinPaid;
                    $claims->paid = $paid;
                }

                if ($request->has('adjustment')){ 
                    $claims->adjustment=$request->get('adjustment');
                }

                if ($request->has('claimBalanceIns')){ 
                   $claims->claim_balance = ($tempFinBalIns - $paid); 
                }

                if ($request->has('claimBalancePat')){ 
                    $claims->claim_balance_pat = ($tempFinBalPat - $paid);
                }

                if ($request->has('reason')){ 
                    $claims->reason = $request->get('reason');
                }

                if ($request->has('status')){ 
                    $claims->status = $request->get('status');
                }

                if ($request->has('copay')){ 
                    $claims->copay = $request->get('copay');
                }

                if ($request->has('deductible')){ 
                    $claims->deductible = $request->get('deductible');
                }
                if ($request->has('coins')){ 
                    $claims->coins = $request->get('coins');
                }

                if ($request->has('insuranceID')){ 
                    $claims->insurance_id = $insuranceID;
                }
                if ($claims->save()) {
                    DB::commit();
                } else {
                    $this->mMessage = 'Claims update failed';
                }
            }
            $str = " Deposit Detail Created. Deposit Detail ID: $depositdetID, Claim ID: $claimID, Patient ID: $patientID, DepositDetailID: $depositdetID";
            $this->mMessage = 'Deposit Detail Created Successfully!';
            $this->mStatus = 1;
            $this->mData['DepositDetailID'] = $depositdetID;
        } catch (Exception $e) {
            DB::rollback();
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function findBalance(Request $request)  {
        try {

            $did = (int) $request->get('depositID');
            if ($did < 1) { 
                $this->mMessage = 'Invalid DepositID';
                return $this->sendResponseDefault();
            }
            $deposit = DepositModel::find($did);
            $count = DepositModel::where(['id' => $did, 'is_deleted' => 0])->select('id')->get()->count();
            if (empty($deposit)){
                $this->mMessage = 'Deposit ID not found';
                return $this->sendResponseDefault();
            } else {
                if ($deposit->is_deleted > 0){
                    $this->mMessage = 'Deposit ID alread deleted';
                    return $this->sendResponseDefault();
                }
            }
            $dt = $deposit->amount;
            $depositTotal = $dt;
            $bal = $dt;
            $bal1 = 0;

            if (DepositDetailsModel::where(['deposit_id' => $did, 'is_deleted' => 0])->select('id')->get()->count() > 0) {
                $sql = "select sum(amount_adjusted) as amount_adjusted from tb_deposit_details
                where  deposit_id=$did and is_deleted = 0";
                $rows = DB::select($sql);
                $bal1 = 0;
                if (count($rows) > 0 ){
                    $bal1 = $rows [0]->amount_adjusted;
                }
                $bal = $bal - $bal1;
            }
            $this->mMessage = 'Balance Calculated Successfully!';
            $this->mStatus = 1;
            $this->mData['bal'] = $bal;
            $this->mData['balance'] = $bal;
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function depositDetails(Request $request){
        try {
            $did = (int) $request->get('depositID');
            if ($did < 1) { 
                $this->mMessage = 'Invalid Deposit ID';
                return $this->sendResponseDefault();
            }
            
            $depositdetail = DepositDetailsModel::find($did);
            if (empty($depositdetail)) { 
                $this->mMessage = 'Deposit ID not found';
                return $this->sendResponseDefault();
            } else {
                if ($depositdetail->is_deleted > 0){
                    $this->mMessage = 'Deposit ID alread deleted';
                    return $this->sendResponseDefault();
                }
            }
            $this->mMessage = 'Deposit Detail Listed Successfully!';
            $this->mStatus = 1;
            $this->mData[] = $depositdets;
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function reverseAdjustDeposit(Request $request) {
        try{
            $did = (int) $request->get('depositID');
            $count = DepositModel::where('id', $did)
                        ->where('is_deleted', 0)
                        ->select('id')
                        ->get()
                        ->count();
            if ($count < 1) { 
                $this->mMessage = 'Deposit ID not found';
                return $this->sendResponseDefault();
            }

            $claimID = (int) $request->get('claimID');
            $patientID = (int)$request->get('patientID');
            if($claimID<=0 && $patientID<=0){
                $this->mMessage = 'ClaimID or PatientID is necessary!';
                return $this->sendResponseDefault();
            }

            if($claimID>0 && $patientID>0){
                $this->mMessage = 'Cannot use both ClaimID and PatientID!';
                return $this->sendResponseDefault();
            }

            if($claimID > 0) {
                $count = ClaimsModel::where('id', $claimID)
                    ->where('is_deleted', 0)
                    ->select('id')
                    ->get()
                    ->count();
                if ($count < 1) {
                    $this->mMessage = 'Invalid ClaimID!';
                    return $this->sendResponseDefault();
                }
            }

            if($patientID > 0) {
                $count = PatientsModel::where('id', '=', $patientID)
                            ->where('is_deleted', '=', 0)
                            ->select('id')
                            ->get()
                            ->count();
                if ($count < 1) {
                    $this->mMessage = 'Invalid patientID!';
                    return $this->sendResponseDefault();
                }
            }

            DB::beginTransaction();
            $rows = DepositDetailsModel::where('patient_id', $patientID)
                    ->where('claim_id', $claimID)
                    ->where('is_deleted', 0)
                    ->where('amount_adjusted', $request->get('amountAdjusted'))
                    ->select('id')
                    ->get();
            if ($rows->count() > 0) {
                $id = $rows[0]->id;
                $depositdetails = DepositDetailsModel::find($id);
                $depositdetails->is_deleted = 1;
                $depositdetails->save();

                $rows = ClaimsModel::where('id', $claimID)
                            ->where('is_deleted', 0)
                            ->select('id')
                            ->get();
                if ($rows->count() > 0) {
                    $id = $rows[0]->id;
                    $claims =ClaimsModel::find($id);
                    $claims->is_posted = 0;
                    $claims->allowed = '';
                    $claims->paid ='';
                    $claims->adjustment='';
                    $claims->claim_balance = 0;
                    $claims->reason = '';
                    $claims->status ='';
                    $claims->copay = '';
                    $claims->deductible = '';
                    $claims->coins =  '';
                    $claims->save();

                    //Common::WriteLog('Deposits','Deposit Detail Reversed. Deposit ID:' . $did . ' Claim ID:' .$claimID . ' Patient ID:' .$patientID,0,'DepositID',$did);
                    $this->mMessage = 'Deposit Detail Removed Successfully!';
                    $this->mStatus = 1;
                    DB::commit();
                }
            } else {
                $this->mMessage = 'No Deposit Detail Found!';
                DB::rollback();
            }
        } catch (Exception $e) {
            DB::rollback();
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function addDepositDetails(Request $request) {
        $status = 0;
        $message = '';
        $data = 0;
        $depositdetID = 0;
        try{
            $input[''] = '';
            //$input['is_posted'] = 'isPosted';
            $input['deposit_id'] = 'depositID';
            $input['claim_id'] = 'claimID';
            $input['patient_id'] = 'patientID';
            $input['amount_adjusted'] = 'amountAdjusted';
            // $input['amount'] = 'copay';
            // $input['payment_type'] = 'deductible';
            // $input['notes'] = 'coins';
            //$input['notes'] = 'allowed';
            $input['adjustment_notes'] = 'adjustmentNotes';
            $input['adjustment_code'] = 'adjustmentCode';
            //$input['notes'] = 'paid';
            $input['adjustment'] = 'adjustment';
            $input['claim_balance_pat'] = 'claimBalancePat';
            $input['claim_balance_ins'] = 'claimBalanceIns';
            //$input['notes'] = 'reason';
            //$input['notes'] = 'status';
            $depositDet = new DepositDetailsModel();
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0){
                    continue;
                }
                $depositDet->$tableField = $request->get($formField);
            }
            $depositDet->created_by = 0;
            $depositDet->is_deleted = 0;
            if ($depositDet->save()){
                if ($depositDet->id > 0){
                    $depositdetID = $depositDet->id;
                    $status = 1;
                }
            } else {
                $message = "Deposit Details Created failed";
                $status = 0;
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = 0;
        }
        return array('status' => $status, 'message' => $message,
             'data' => $depositdetID);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    // public function parse835_old(Request $request) {
    //     try
    //     {
    //         $filename = $this->getFile($request, 'filename');
    //         //$filename = '/var/www/html/curismedlive/temp/era1.835';
    //         //return $filename;
    //         $this->mMessage = "Invalid file format";
    //         if (empty($filename)){
    //             return $this->sendResponseDefault();
    //         } 
    //         $str = file_get_contents($filename);
    //         $infh = fopen($filename, 'r');
    //         if (empty($str)){
    //             return $this->sendResponseDefault();
    //         } 
    //         $delimiter1 = '~';
    //         $delimiter2 = '|';
    //         $delimiter3 = '^';

    //         $out = array();
    //         $out['loopid'] = '';
    //         $out['st_segment_count'] = 0;
    //         $buffer = '';
    //         $segid = '';

    //         $tpos = strpos($str, $delimiter1);
    //         if ($tpos === false) {
    //             return $this->sendResponseDefault();
    //         }
    //         $inline = substr($str, 0, $tpos);
    //         $buffer = substr($str, $tpos + 1);

    //         if (substr($inline, 0, 3) === 'ISA') {
    //               $delimiter2 = substr($inline, 3, 1);
    //               $delimiter3 = substr($inline, -1);
    //         }
    //         $format835 = $delimiter1 . 'ST' . $delimiter2 . '835';
    //         $tpos = strpos($str, $format835);
    //         if ($tpos === false) {
    //             return $this->sendResponseDefault();
    //         }
    //         $lines = explode($delimiter1, $str);
    //         $flagStart = false;
    //         $nextFlag = '';
    //         $arr = array();
    //         $prevTag = '';
    //         $arr['CLAIMS_COUNT'] = 0;
    //         $CLAIMS_COUNT = 0;
    //         $sno =-1;
    //         foreach ($lines as $line) {
    //             $seg = explode($delimiter2, $line);
    //             $segId = $seg[0];

    //             if ($segId == 'ISA') {
    //                 continue;
    //             }
    //             if ($segId == 'ST') {
    //                 $flagStart = true;
    //                 $nextFlag = 'BPR';
    //                 continue;
    //             }
    //             if (!$flagStart){
    //                 continue;
    //             }
    //             if ($segId == 'BPR'){
    //                 $arr['AMOUNT'] = $seg[2];
    //                 $arr['DATE'] = $this->getMDYbyYMD($seg[16]);
    //                 //print_r($seg);
    //             }
    //             if ($segId == 'TRN'){
    //                 $arr['CHECK']=$seg[2];
    //                 //print_r($seg);
    //             }
    //             if ($segId == 'N1' ){
    //                 if ($seg[1] == 'PR'){
    //                     $arr['PAYER'] = $seg[2];
    //                     $prevTag = 'PR';
    //                 }
    //                 if ($seg[1] == 'PE'){
    //                     $arr['PAYEE'] = $seg[2];    
    //                     $prevTag = 'PE';
    //                 }
    //             }
    //             if ($segId == 'N3' ){
    //                 if ($prevTag == 'PR'){
    //                     $arr['PAYER'] .= ', ' . $seg[1];
    //                 }
    //                 if ($prevTag == 'PE'){
    //                     $arr['PAYEE'] .= ', ' . $seg[1];    
    //                 }
    //             }
    //             if ($segId == 'N4' ){
    //                 //print_r($seg);
    //                 if ($prevTag == 'PR'){
    //                     if (isset($seg[1])) {
    //                         $arr['PAYER'] .= ', ' . $seg[1];
    //                     }
    //                     if (isset($seg[2])) {
    //                         $arr['PAYER'] .= ', ' . $seg[2];
    //                     }
    //                     if (isset($seg[3])) {
    //                         $arr['PAYER'] .= ', ' . $seg[3];
    //                     }
    //                 }
    //                 if ($prevTag == 'PE'){
    //                     if (isset($seg[1])) {
    //                         $arr['PAYEE'] .= ', ' . $seg[1];    
    //                     }
    //                     if (isset($seg[2])) {
    //                         $arr['PAYEE'] .= ', ' . $seg[2];    
    //                     }
    //                     if (isset($seg[3])) {
    //                         $arr['PAYEE'] .= ', ' . $seg[3];    
    //                     }
    //                 }
    //             }
    //             if ($segId == 'LX' ){
    //                 $prevTag = $segId;
    //                 continue;
    //             }

    //             if ($segId == 'CLP' ){
    //                 $CLAIMS_COUNT++;
    //                 if ($prevTag == 'LX'){
    //                     $arr['CLAIMS_COUNT'] = $arr['CLAIMS_COUNT'] + 1;
    //                 }
    //                 $prevTag = $segId;
    //                 $arr['CLAIMS_COUNT'] = $CLAIMS_COUNT;
    //                 $i = $CLAIMS_COUNT-1;
    //                 $ACCOUNT_NUM = $seg[1];
    //                 $rows = ClaimsSubmissioActivityModel::where('claim_number', $ACCOUNT_NUM)
    //                             ->select('id')
    //                             ->get();
    //                 $sno = -1;
    //                 if ($rows->count() > 0) {
    //                     $sno = $rows[0]->id; 
    //                 }
    //                 $arr['CLAIMS'][$i]['ACCOUNT_NUM'] = $ACCOUNT_NUM;
    //                 $arr['CLAIMS'][$i]['CHARGE_AMOUNT'] = $seg[3];
    //                 $arr['CLAIMS'][$i]['PAYMENT_AMOUNT'] = $seg[4];
    //                 $arr['CLAIMS'][$i]['CONTROL_NUM'] = $seg[7];
    //                 $arr['CLAIMS'][$i]['LINE_ITEM'] = [];
    //                 continue;
    //             }

    //             if ($segId == 'NM1' ){
    //                 if ($prevTag == 'CLP'){
    //                     if ($seg[1] = 'QC'){
                           
    //                         $i = $CLAIMS_COUNT-1;
    //                         if (isset($seg[9])){
    //                             $arr['CLAIMS'][$i]['PATIENT_ID'] = $seg[9];
    //                             if ($seg[9] == 'MI'){
    //                                 $arr['CLAIMS'][$i]['PATIENT_NAME'] = $seg[3] . ' ' . $seg[4];
    //                             }
    //                         } else {
    //                                 $arr['CLAIMS'][$i]['PATIENT_NAME'] = $seg[3] . ' ' . $seg[4];
    //                         }

    //                     }
    //                 }
    //             }

    //             if ($segId == 'DTM' ){
    //                 if ($prevTag == 'CLP'){
    //                     if ($seg[1] = '232'){
    //                         $i = $CLAIMS_COUNT-1;
    //                         $arr['CLAIMS'][$i]['START_DATE'] = $this->getMDYbyYMD($seg[2]);
    //                     }
    //                     if ($seg[1] = '233'){
    //                         $i = $CLAIMS_COUNT-1;
    //                         $arr['CLAIMS'][$i]['END_DATE'] = $this->getMDYbyYMD($seg[2]);
    //                     }
    //                 }
    //                 if ($prevTag == 'SVC'){
    //                     $j = count($arr['CLAIMS'][$i]['LINE_ITEM'])-1;
    //                     $arr['CLAIMS'][$i]['LINE_ITEM'][$j]['DATE'] = $this->getMDYbyYMD($seg[2]);
    //                 }
    //                 $prevTag = $segId;
    //             }
    //             if ($segId == 'SVC' ){
    //                 $i = $CLAIMS_COUNT-1;
    //                 $c = str_replace('HC', '', $seg[1]);
    //                 $c = str_replace(':', '', $c);
    //                 $c = str_replace('HO', '', $c);
    //                 $tmp=[];
    //                 $tmp['ID'] = $sno;
    //                 if  ($sno> 0) {
    //                     $sno++;
    //                 }
    //                 $tmp['CPT'] = $c;
    //                 $tmp['CHARGE_AMOUNT'] = (float) $seg[2];
    //                 $tmp['PAYMENT_AMOUNT'] = (float) $seg[3];
    //                 $tmp['TOTAL_ADJ_AMOUNT'] = round( $tmp['CHARGE_AMOUNT'] - $tmp['PAYMENT_AMOUNT'],2);
    //                 if (!isset($arr['CLAIMS'][$i]['LINE_ITEM'])){
    //                     $arr['CLAIMS'][$i]['LINE_ITEM'] = [];
    //                 }
    //                 $j = count($arr['CLAIMS'][$i]['LINE_ITEM']);
    //                 $arr['CLAIMS'][$i]['LINE_ITEM'][$j] = $tmp;
    //                 $prevTag = $segId;

    //                 $tmp = [];
    //                 $idx_name = $seg[1];
    //                 $tmp['ADJUSTMENT_GROUP_CODE'] = $seg[1];
    //                 $tmp['ADJUSTMENT_GROUP'] = $idx_name;
    //                 if ($seg[1] == 'PR'){
    //                     $idx_name = 'PATIENT RESPONSIBILITY';
    //                     $tmp['ADJUSTMENT_GROUP'] = $idx_name;
    //                 } else if ($seg[1] == 'OA'){
    //                     $idx_name = 'OTHER ADJUSTMENTS';
    //                     $tmp['ADJUSTMENT_GROUP'] = $idx_name;
    //                 }
    //                 $tmp['ADJ_AMOUNT'] = $seg[3];
    //                 $tmp['TRANSLATED_REASON_CODE'] = 'CO-PAYMENT AMOUNT';
    //                 if ($seg[2] == '3'){
    //                     $tmp['TRANSLATED_REASON_CODE'] = 'CO-PAYMENT AMOUNT';
    //                 } else if ($seg[2] == '18'){
    //                     $tmp['TRANSLATED_REASON_CODE'] = 'DUPLICATE CLAIM/SERVICE';
    //                 } else {
    //                     $tmp['TRANSLATED_REASON_CODE'] = 'NIL';
    //                 }
    //                 $j = count($arr['CLAIMS'][$i]['LINE_ITEM'])-1;
    //                 $arr['CLAIMS'][$i]['LINE_ITEM'][$j]['ADJUSTMENT_GROUP'] = $tmp;

    //             }
    //             if ($segId == 'CAS' ){
    //                 //CO CR OA PI PR
    //                 if ($prevTag == 'DTM'){
    //                     if ($seg[1] == 'OA'){
    //                         //continue;
    //                     }
    //                     $tmp=[];
    //                     $tmp['ADJUSTMENT_GROUP_CODE'] = $seg[1];
    //                     $tmp['ADJUSTMENT_GROUP'] = $seg[1];

    //                     $idx_name = $seg[1];

    //                     $tmp['ADJUSTMENT_GROUP'] = $idx_name;
    //                     if ($seg[1] == 'PR'){
    //                         $idx_name = 'PATIENT RESPONSIBILITY';
    //                         $tmp['ADJUSTMENT_GROUP'] = $idx_name;
    //                     } else if ($seg[1] == 'OA'){
    //                         $idx_name = 'OTHER ADJUSTMENTS';
    //                         $tmp['ADJUSTMENT_GROUP'] = $idx_name;
    //                     }
    //                     $tmp['ADJ_AMOUNT'] = $seg[3];
    //                     $tmp['TRANSLATED_REASON_CODE'] = 'CO-PAYMENT AMOUNT';
    //                     if ($seg[2] == '3'){
    //                         $tmp['TRANSLATED_REASON_CODE'] = 'CO-PAYMENT AMOUNT';
    //                     } else if ($seg[2] == '18'){
    //                         $tmp['TRANSLATED_REASON_CODE'] = 'DUPLICATE CLAIM/SERVICE';
    //                     }
    //                     // if ($seg[1] == 'OA'){
    //                     //     $tmp['TRANSLATED_REASON_CODE'] = 'OA';
    //                     //     $idx_name
    //                     // }
    //                     $j = count($arr['CLAIMS'][$i]['LINE_ITEM'])-1;
    //                     $arr['CLAIMS'][$i]['LINE_ITEM'][$j]['ADJUSTMENT_GROUP'] = $tmp;
    //                     for ($i1=0; $i1<$j; $i1++) {
    //                         $c = '';
    //                         if (isset($arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP_CODE'])){
    //                         $c = $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP_CODE'];
    //                     }

    //                         if (strpos($c, ':') > 0){
    //                             $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP_CODE'] = $tmp['ADJUSTMENT_GROUP_CODE'];
    //                             $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP'] = $tmp['ADJUSTMENT_GROUP'];
    //                             $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP_CODE'] = $tmp['ADJUSTMENT_GROUP_CODE'];
    //                             $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['TRANSLATED_REASON_CODE'] = $tmp['TRANSLATED_REASON_CODE'];
    //                             }
    //                     }
    //                     //print_r($seg);
    //                     //die();
    //                     //$prevTag = $segId;
    //                     //print_r($seg);
    //                 }
    //                 $prevTag = $segId;
    //             }
    //         }
    //         $this->mStatus = 1;
    //         $this->mData = $arr;
    //         $this->mMessage = 'Success';
    //     } 
    //     catch (Exception $e) 
    //     {
    //         $this->mMessage = $e->getMessage();
    //     }
    //     //print_r($arr);
    //     return $this->sendResponseDefault();
    // }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function parse835(Request $request) {
        try
        {
            $filename = $this->getFile($request, 'filename');
            //$filename = '/var/www/html/curismedlive/temp/era1.835';
            //return $filename;
            $this->mMessage = "Invalid file format";
            if (empty($filename)){
                return $this->sendResponseDefault();
            } 
            $str = file_get_contents($filename);
            $infh = fopen($filename, 'r');
            if (empty($str)){
                return $this->sendResponseDefault();
            } 
            $delimiter1 = '~';
            $delimiter2 = '|';
            $delimiter3 = '^';

            $out = array();
            $out['loopid'] = '';
            $out['st_segment_count'] = 0;
            $buffer = '';
            $segid = '';

            $tpos = strpos($str, $delimiter1);
            if ($tpos === false) {
                return $this->sendResponseDefault();
            }
            $inline = substr($str, 0, $tpos);
            $buffer = substr($str, $tpos + 1);

            if (substr($inline, 0, 3) === 'ISA') {
                  $delimiter2 = substr($inline, 3, 1);
                  $delimiter3 = substr($inline, -1);
            }
            $format835 = $delimiter1 . 'ST' . $delimiter2 . '835';
            $tpos = strpos($str, $format835);
            if ($tpos === false) {
                return $this->sendResponseDefault();
            }
            $lines = explode($delimiter1, $str);
            $flagStart = false;
            $nextFlag = '';
            $arr = array();
            $prevTag = '';
            $arr['CLAIMS_COUNT'] = 0;
            $CLAIMS_COUNT = 0;
            $sno =-1;
            $_20170925 = 0;
            foreach ($lines as $line) {
                $seg = explode($delimiter2, $line);
                $segId = $seg[0];

                if ($segId == 'ISA') {
                    if ($readfirstlineonly==1){
                        return $seg;
                    }
                    continue;
                }
                if ($segId == 'ST') {
                    $flagStart = true;
                    $nextFlag = 'BPR';
                    continue;
                }
                if (!$flagStart){
                    continue;
                }
                if ($segId == 'BPR'){
                    $arr['AMOUNT'] = $seg[2];
                    $arr['DATE'] = $this->getMDYbyYMD($seg[16]);
                    $c = $arr['DATE'];
                }
                if ($segId == 'TRN'){
                    $arr['CHECK']=$seg[2];
                    //print_r($seg);
                }
                if ($segId == 'N1' ){
                    if ($seg[1] == 'PR'){
                        $arr['PAYER'] = $seg[2];
                        $prevTag = 'PR';
                    }
                    if ($seg[1] == 'PE'){
                        $arr['PAYEE'] = $seg[2];    
                        $prevTag = 'PE';
                    }
                }
                if ($segId == 'N3' ){
                    if ($prevTag == 'PR'){
                        $arr['PAYER'] .= ', ' . $seg[1];
                    }
                    if ($prevTag == 'PE'){
                        $arr['PAYEE'] .= ', ' . $seg[1];    
                    }
                }
                if ($segId == 'N4' ){
                    //print_r($seg);
                    if ($prevTag == 'PR'){
                        if (isset($seg[1])) {
                            $arr['PAYER'] .= ', ' . $seg[1];
                        }
                        if (isset($seg[2])) {
                            $arr['PAYER'] .= ', ' . $seg[2];
                        }
                        if (isset($seg[3])) {
                            $arr['PAYER'] .= ', ' . $seg[3];
                        }
                    }
                    if ($prevTag == 'PE'){
                        if (isset($seg[1])) {
                            $arr['PAYEE'] .= ', ' . $seg[1];    
                        }
                        if (isset($seg[2])) {
                            $arr['PAYEE'] .= ', ' . $seg[2];    
                        }
                        if (isset($seg[3])) {
                            $arr['PAYEE'] .= ', ' . $seg[3];    
                        }
                    }
                }
                if ($segId == 'LX' ){
                    $prevTag = $segId;
                    continue;
                }

                if ($segId == 'CLP' ){
                    $CLAIMS_COUNT++;
                    if ($prevTag == 'LX'){
                        $arr['CLAIMS_COUNT'] = $arr['CLAIMS_COUNT'] + 1;
                    }
                    $prevTag = $segId;
                    $arr['CLAIMS_COUNT'] = $CLAIMS_COUNT;
                    $i = $CLAIMS_COUNT-1;
                    $ACCOUNT_NUM = $seg[1];
                    $rows = ClaimsSubmissioActivityModel::where('claim_number', $ACCOUNT_NUM)
                                ->select('id')
                                ->get();
                    $sno = -1;
                    if ($rows->count() > 0) {
                        $sno = $rows[0]->id; 
                    }
                    $arr['CLAIMS'][$i]['ACCOUNT_NUM'] = $ACCOUNT_NUM;
                    $arr['CLAIMS'][$i]['CHARGE_AMOUNT'] = $seg[3];
                    $arr['CLAIMS'][$i]['PAYMENT_AMOUNT'] = $seg[4];
                    $arr['CLAIMS'][$i]['CONTROL_NUM'] = $seg[7];
                    $arr['CLAIMS'][$i]['LINE_ITEM'] = [];
                    continue;
                }

                if ($segId == 'NM1' ){
                    if ($prevTag == 'CLP'){
                        if ($seg[1] = 'QC'){
                           
                            $i = $CLAIMS_COUNT-1;
                            if (isset($seg[9])){
                                $arr['CLAIMS'][$i]['PATIENT_ID'] = $seg[9];
                                if ($seg[9] == 'MI'){
                                    $arr['CLAIMS'][$i]['PATIENT_NAME'] = $seg[3] . ' ' . $seg[4];
                                }
                            } else {
                                    $arr['CLAIMS'][$i]['PATIENT_NAME'] = $seg[3] . ' ' . $seg[4];
                            }

                        }
                    }
                }

                if ($segId == 'DTM' ){
                    if ($prevTag == 'CLP'){
                            if ($seg[2] == '20170925') {
                                $_20170925 = 1;
                            }
                        if ($seg[1] = '232'){
                            $i = $CLAIMS_COUNT-1;

                            $arr['CLAIMS'][$i]['START_DATE'] = $this->getMDYbyYMD($seg[2]);
                        }
                        if ($seg[1] = '233'){
                            $i = $CLAIMS_COUNT-1;
                        //$this->ECHO($seg,false);
                            $arr['CLAIMS'][$i]['END_DATE'] = $this->getMDYbyYMD($seg[2]);
                        }
                    }
                    if ($prevTag == 'SVC'){
                        $j = count($arr['CLAIMS'][$i]['LINE_ITEM'])-1;
                        $arr['CLAIMS'][$i]['LINE_ITEM'][$j]['DATE'] = $this->getMDYbyYMD($seg[2]);
                    }
                    $prevTag = $segId;
                }
                if ($segId == 'SVC' ){
                    $i = $CLAIMS_COUNT-1;
                    $c = str_replace('HC', '', $seg[1]);
                    $c = str_replace(':', '', $c);
                    $c = str_replace('HO', '', $c);
                    $tmp=[];
                    $tmp['ID'] = $sno;
                    if  ($sno> 0) {
                        $sno++;
                    }
                                if($_20170925 == 1){
                                    //$this->ECHO($seg,false);
                            }

                    $tmp['CPT'] = $c;
                    $tmp['CHARGE_AMOUNT'] = (float) $seg[2];
                    $tmp['PAYMENT_AMOUNT'] = (float) $seg[3];
                    $tmp['TOTAL_ADJ_AMOUNT'] = round( $tmp['CHARGE_AMOUNT'] - $tmp['PAYMENT_AMOUNT'],2);
                    if (!isset($arr['CLAIMS'][$i]['LINE_ITEM'])){
                        $arr['CLAIMS'][$i]['LINE_ITEM'] = [];
                    }
                    $j = count($arr['CLAIMS'][$i]['LINE_ITEM']);
                    $arr['CLAIMS'][$i]['LINE_ITEM'][$j] = $tmp;
                    $prevTag = $segId;

                    $tmp = [];
                    $idx_name = $seg[1];
                    $tmp['ADJUSTMENT_GROUP_CODE'] = $seg[1];
                    $tmp['ADJUSTMENT_GROUP'] = $idx_name;
                    if ($seg[1] == 'PR'){
                        $idx_name = 'PATIENT RESPONSIBILITY';
                        $tmp['ADJUSTMENT_GROUP'] = $idx_name;
                        //$this->ECHO($seg);
                    } else if ($seg[1] == 'OA'){
                        $idx_name = 'OTHER ADJUSTMENTS';
                        $tmp['ADJUSTMENT_GROUP'] = $idx_name;
                    }  else {
                        //die('a');
                    }

                    $tmp['ADJ_AMOUNT'] = $seg[3];
                    $tmp['TRANSLATED_REASON_CODE'] = 'CO-PAYMENT AMOUNT';
                    $tmp['TRANSLATED_REASON_CODE'] = '****';
                    if ($seg[2] == '3'){
                        $tmp['TRANSLATED_REASON_CODE'] = 'CO-PAYMENT AMOUNT';
                    } else if ($seg[2] == '18'){
                        $tmp['TRANSLATED_REASON_CODE'] = 'DUPLICATE CLAIM/SERVICE';
                    } else {
                        $tmp['TRANSLATED_REASON_CODE'] = 'NIL';

                        //$this->ECHO($seg);
                    }
                    if (isset($seg[5])){
                        if ($seg[5]=='8'){
                            $tmp['ADJUSTMENT_GROUP_CODE'] = '8';
                            $tmp['ADJUSTMENT_GROUP'] = 'CONTRACTUAL OBLIGATIONS';
                        $tmp['TRANSLATED_REASON_CODE'] = 'CHARGES EXCEED YOUR CONTRACTED/LEGISLATED FEE ARRANGEMENT';
                        }
                        if ($seg[5]=='4'){
                            $tmp['ADJUSTMENT_GROUP_CODE'] = '4';
                            $tmp['ADJUSTMENT_GROUP'] = 'PAYOR INITIATED REDUCTIONS';
                        $tmp['TRANSLATED_REASON_CODE'] = 'THE TIME LIMIT FOR FILING HAS EXPIRED.                                              ';
                        }
                    } 

                    $j = count($arr['CLAIMS'][$i]['LINE_ITEM'])-1;
                    $arr['CLAIMS'][$i]['LINE_ITEM'][$j]['ADJUSTMENT_GROUP'] = $tmp;

                }
                if ($segId == 'CAS' ){
                    //CO CR OA PI PR
                    if ($prevTag == 'DTM'){
                        if ($seg[1] == 'OA'){
                            //continue;
                        }
                        $tmp=[];
                        $tmp['ADJUSTMENT_GROUP_CODE'] = $seg[1];
                        $tmp['ADJUSTMENT_GROUP'] = $seg[1];

                        $idx_name = $seg[1];

                        $tmp['ADJUSTMENT_GROUP'] = $idx_name;
                        if ($seg[1] == 'PR'){
                            $idx_name = 'PATIENT RESPONSIBILITY';
                            $tmp['ADJUSTMENT_GROUP'] = $idx_name;
                        } else if ($seg[1] == 'OA'){
                            $idx_name = 'OTHER ADJUSTMENTS';
                            $tmp['ADJUSTMENT_GROUP'] = $idx_name;
                        }
                        $tmp['ADJ_AMOUNT'] = $seg[3];
                        $tmp['TRANSLATED_REASON_CODE'] = 'CO-PAYMENT AMOUNT';
                        if ($seg[2] == '3'){
                            $tmp['TRANSLATED_REASON_CODE'] = 'CO-PAYMENT AMOUNT';
                        } else if ($seg[2] == '18'){
                            $tmp['TRANSLATED_REASON_CODE'] = 'DUPLICATE CLAIM/SERVICE';
                        }
                        // if ($seg[1] == 'OA'){
                        //     $tmp['TRANSLATED_REASON_CODE'] = 'OA';
                        //     $idx_name
                        // }
                        $j = count($arr['CLAIMS'][$i]['LINE_ITEM'])-1;
                        $arr['CLAIMS'][$i]['LINE_ITEM'][$j]['ADJUSTMENT_GROUP'] = $tmp;
                        for ($i1=0; $i1<$j; $i1++) {
                            $c = '';
                            if (isset($arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP_CODE'])){
                            $c = $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP_CODE'];
                        }

                            if (strpos($c, ':') > 0){
                                $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP_CODE'] = $tmp['ADJUSTMENT_GROUP_CODE'];
                                $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP'] = $tmp['ADJUSTMENT_GROUP'];
                                $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['ADJUSTMENT_GROUP_CODE'] = $tmp['ADJUSTMENT_GROUP_CODE'];
                                $arr['CLAIMS'][$i]['LINE_ITEM'][$i1]['ADJUSTMENT_GROUP']['TRANSLATED_REASON_CODE'] = $tmp['TRANSLATED_REASON_CODE'];
                                }
                        }
                    }
                    $prevTag = $segId;
                }
            }
            $this->mStatus = 1;
            $this->mData = $arr;
            $this->mMessage = 'Success';
        } 
        catch (Exception $e) 
        {
            $this->mMessage = $e->getMessage();
        }
        //print_r($arr);
        return $this->sendResponseDefault();
    }    

    public function getFile(Request $request, $fieldName){
        if (!$request->hasFile($fieldName)) {
            return '';
        }
        $file = $request->file($fieldName);
        $ext = $file->getClientOriginalExtension();
        $filename = $request->file($fieldName)->getRealPath();
        return $filename;
    }

    private function getMDYbyYMD($ymd){
        return substr($ymd, 4,2) . '/' . substr($ymd, 6,2) .'/' . substr($ymd, 0,4);        
    }
}
