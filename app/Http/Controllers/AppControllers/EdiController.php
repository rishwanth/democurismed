<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AlertsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : Anand
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;
use Carbon;
use Response;
use File;
use phpseclib\Net\SFTP;
use phpseclib\Crypt\RSA;

//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\EdiModel;
use App\Model\DetailsModel;
use App\Model\ClaimsSubmissioActivityModel;
use App\Model\ClaimsModel;

class EdiController extends Controller {

    private $mSSH2Username = 'amromed';
    private $mSSH2Password = 'yfkkRDX6';
    private $mSSH2IpHostname = 'ftp.officeally.com';


    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getLast(Request $request) {
        $objModel = EdiModel::select('invoice_num')
                ->orderBy('id', 'desc')
                ->first();
        $data = array();
        $status = 1;
        $message = 'Success';
        if (!empty($objModel)) {
            $data = (array) $objModel ;
        } else {
            $data = array(0 => array('invoice_num' => '00000') );
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request) {
        try{
            $practiceID = Session::get('LOGGED_USER_PRATICE_ID');

            $sql = "select * from tb_edi where practice_id='$practiceID' ORDER BY id DESC";
            //return $sql;
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    // public function unset1(Request $request) {
    //     $data = array();
    //     $status = 1;
    //     $message = 'Success';
    //     $date  = Carbon::now('America/Chicago');
    //     $date1 = date('Y-m-d H:i:s', strtotime($date));        
    //     $ClaimNo = '' . Session::get('tempEDINo');
    //     $loc = '' . Session::get('loc');

    //     $obj = new EdiModel();
    //     $obj->invoice_num = $ClaimNo;
    //     $obj->location = $loc;
    //     $obj->created_on = $date1;
    //     $obj->save();

    //     Session::forget('tempEDINo');
    //     Session::forget('loc');
    //     return $this->sendResponse($status, $message, $data);
    // }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list_edi(Request $request) {
        return view('list_edi');
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        try{
          $ClaimNo = $request->get('ClaimNumber');
          $chartNumber = $request->get('chartNumber');
          $info = array();
          //return $request->all();

          DB::beginTransaction();

          $sql = "SELECT P.id as patientID, CL.id as claimID, CL.auth_num as authNo, PR.EIN , PR.name as practiceName, PR.address as practiceAddr, PR.npi,PH.individual_npi as individualNPI, PH.name as phyName, SL.billing_name as billingName, SL.address, SL.NPI, P.full_name as fullName, P.chart_num as chartNo, P.dob, P.gender, P.addr_street1 as addrStreet1, P.addr_city as addrCity, P.addr_state as addrState, P.addr_zip as addrZip,P.phone_home as phoneHome, P.primary_care_phy as primaryCarePhy, P.referring_phy as referringPhy, P.default_render_phy as defaultRenderPhy, P.default_service_loc as defaultServiceLoc, I.payer_name as payerName, I.addr, I.city, I.state, I.zip, I.clearing_house_payor_id as clearingHousePayorID, PO.policy_num as policyNo, PO.group_num as groupNo, CL.from_date as fromDt, CL.to_date as toDt, CL.total, CL.place_of_service as placeOfService, CL.units, CL.proced, CL.mod1 from tb_claims CL inner join tb_cases CA on CA.id = CL.case_id inner join tb_patients P on P.id = CA.patient_id inner join tb_policies PO on PO.case_id = CA.id inner join tb_insurances I on I.id = PO.insurance_id inner join tb_physicians PH on PH.id = CL.primary_care_phy inner join tb_practices PR on PR.id = P.practice_id inner join tb_service_locations SL on SL.id = CL.service_loc_id where P.chart_num = '$chartNumber' AND CL.is_sent='0' AND CL.claim_status='Ready' order by CL.id ";
          //return $sql;
            $fileNameOnly = '';
            $fileName = '';

            $rows = DB::select($sql);
            $num_of_row = count($rows);

          if($num_of_row % 6 == 0){
            $num_of_row = $num_of_row / 6;
          } else {
            $num_of_row = $num_of_row / 6;
            $num_of_row = (string) $num_of_row;
            $num_of_row = explode(".", $num_of_row)[0];
            $num_of_row = (int)$num_of_row +1;
          }

          $diag1_temp = '';
          $diag2_temp = '';
          $diag3_temp = '';
          $diag4_temp = '';

          $folderName = $this->getFolderEDI();
          $flagFileIsCreated = 0;        
          $myfileHandle = 0;

          $number = 'BatchNumber';
          $rows1 = DetailsModel::where('detail', $number)
            ->select('detailvalue1', 'id')
            ->get();

          if ($rows1->count() < 1) {
              $this->mMessage = "$number missing in details";
              return $this->sendResponseDefault();
          }

          $submitClaim = $rows1[0]->detailvalue1 + 1;
          $detail = DetailsModel::find($rows1[0]->id);
          $detail->detailvalue1 = $submitClaim;
          $detail->save();

          $submitClaim = str_pad($submitClaim, 5, '0', STR_PAD_LEFT);
          $claimID = 0;
          for ($incr=0; $incr < $num_of_row; $incr++) { 

              $sql1 = $sql . " and CL.id>$claimID LIMIT 6";

              $individualNPI1 = "";
              $individualNPI2 = "";
              $individualNPI3 = "";
              $individualNPI4 = "";
              $individualNPI5 = "";
              $individualNPI6 = "";

              $Smonth1 = "";
              $Smonth2 = "";
              $Smonth3 = "";
              $Smonth4 = "";
              $Smonth5 = "";
              $Smonth6 = "";

              $Sdate1 = "";
              $Sdate2 = "";
              $Sdate3 = "";
              $Sdate4 = "";
              $Sdate5 = "";
              $Sdate6 = "";

              $Syear1 = "";
              $Syear2 = "";
              $Syear3 = "";
              $Syear4 = "";
              $Syear5 = "";
              $Syear6 = "";

              $Emonth1 = "";
              $Emonth2 = "";
              $Emonth3 = "";
              $Emonth4 = "";
              $Emonth5 = "";
              $Emonth6 = "";

              $Edate1 = "";
              $Edate2 = "";
              $Edate3 = "";
              $Edate4 = "";
              $Edate5 = "";
              $Edate6 = "";

              $Eyear1 = "";
              $Eyear2 = "";
              $Eyear3 = "";
              $Eyear4 = "";
              $Eyear5 = "";
              $Eyear6 = "";

              $placeOfService1 = "";
              $placeOfService2 = "";
              $placeOfService3 = "";
              $placeOfService4 = "";
              $placeOfService5 = "";
              $placeOfService6 = "";

              $proced1 = "";
              $proced2 = "";
              $proced3 = "";
              $proced4 = "";
              $proced5 = "";
              $proced6 = "";

              $mod11 = "";
              $mod12 = "";
              $mod13 = "";
              $mod14 = "";
              $mod15 = "";
              $mod16 = "";

              $pointer1 = "";
              $pointer2 = "";
              $pointer3 = "";
              $pointer4 = "";
              $pointer5 = "";
              $pointer6 = "";

              $charge1 = "";
              $charge2 = "";
              $charge3 = "";
              $charge4 = "";
              $charge5 = "";
              $charge6 = "";

              $units1 = "";
              $units2 = "";
              $units3 = "";
              $units4 = "";
              $units5 = "";
              $units6 = "";

              $insuranceAddr1 = "";
              $practiceAddr = "";
              $facilityAddr = "";

              $chargeTot = 0;
              $count = 0;
              $i = 1;

              $rows1 = DB::select($sql1);

              foreach ($rows1 as $row1) {
                  $row = (array) $row1;
                  $count = $count + 1;
                  $fullName = $row['fullName'];
                  $claimID = $row['claimID'];
                  $dob = $row['dob'];
                  $gender = $row['gender'];
                  $addrStreet1 = $row['addrStreet1'];
                  $addrCity = $row['addrCity'];
                  $addrState = $row['addrState'];
                  $addrZip = $row['addrZip'];
                  $phoneHome = $row['phoneHome'];
                  $payerName = $row['payerName'];
                  $insuranceAddr1 = $row['addr'];

                  $insuranceCity = $row['city'];
                  $insuranceState = $row['state'];
                  $insuranceZip = $row['zip'];
                  $insurancePayorID = $row['clearingHousePayorID'];

                  $policyNo = $row['policyNo'];
                  $groupNo = $row['groupNo'];
                  $authNo = $row['authNo'];
                  $patientID = $row['patientID'];
                  $facilityName = $row['billingName'];
                  $facilityAddr = $row['address'];
                  $facilityNPI = $row['NPI'];
                  $practiceNPI = $row['npi'];
                  $practiceName = $row['practiceName'];
                  $practiceAddr = $row['practiceAddr'];
                  $providerName = $row['phyName'];
                  //$chartNo = $row['chartNo'];
                  $fromDt = $row['fromDt'];
                  $toDt = $row['toDt'];
                  ${"individualNPI".$i} = $row['individualNPI'];
                  ${"placeOfService".$i} = $row['placeOfService'];
                  ${"proced".$i} = $row['proced'];
                  ${"mod1".$i} = $row['mod1'];
                  ${"charge".$i} = number_format(round((float)$row['total'],2),2);
                  ${"units".$i} = $row['units'];
                  $EIN = $row['EIN'];



                  $c = $submitClaim . '-' .($incr+1);
                  $chartNo = $c;

                  $sql3 = "SELECT diag1,diag2,diag3,diag4 from tb_claims where claim_number = '$ClaimNo' 
                  GROUP BY claim_number ";
                  $row3 = DB::select($sql3);

                  foreach ($row3 as $row4) {
                      $row1 = (array) $row4;
                      $diag1_temp = $row1['diag1'];
                      $diag2_temp = $row1['diag2'];
                      $diag3_temp = $row1['diag3'];
                      $diag4_temp = $row1['diag4'];
                  }

                  $chargeTot = $chargeTot + number_format(round((float)$row['total'],2),2);

                  $tempfromDt = explode("-", $fromDt);
                  if($tempfromDt[1] != ""){
                      ${"Smonth".$i} = $tempfromDt[1];
                  } else {
                      ${"Smonth".$i} = "";
                  }

                  if($tempfromDt[2] != ""){
                      ${"Sdate".$i} = $tempfromDt[2];
                  } else {
                      ${"Sdate".$i} = "";
                  }

                  if($tempfromDt[0] != ""){
                      ${"Syear".$i} = substr($tempfromDt[0],-2);
                  } else {
                      ${"Syear".$i} = "";
                  }

                  $temptoDt = explode("-", $toDt);
                  if($temptoDt[1] != ""){
                      ${"Emonth".$i} = $temptoDt[1];
                  } else {
                      ${"Emonth".$i} = "";
                  }

                  if($temptoDt[2] != ""){
                      ${"Edate".$i} = $temptoDt[2];
                  } else {
                      ${"Edate".$i} = "";
                  }

                  if($temptoDt[0] != ""){
                      ${"Eyear".$i} = substr($temptoDt[0],-2);
                  } else {
                      ${"Eyear".$i} = "";
                  }

                  if($diag1_temp != "" && $diag2_temp != "" && $diag3_temp != "" && $diag4_temp != "" ){
                    ${"pointer".$i} = "ABCD";
                  }
                  else if($diag1_temp != "" && $diag2_temp != "" && $diag3_temp != "" && $diag4_temp == "" ){
                    ${"pointer".$i} = "ABC";
                  }
                  else if($diag1_temp != "" && $diag2_temp != "" && $diag3_temp == "" && $diag4_temp == "" ){
                    ${"pointer".$i} = "AB";
                  }
                  else if($diag1_temp != "" && $diag2_temp == "" && $diag3_temp == "" && $diag4_temp == "" ){
                    ${"pointer".$i} = "A";
                  }
                  else if($diag1_temp == "" && $diag2_temp == "" && $diag3_temp == "" && $diag4_temp == "" ){
                    ${"pointer".$i} = "";
                  }

                  $i++;
              }

              $sql9 = "SELECT * from tb_guarantor where patient_id = '$patientID'";
              $rows91 = DB::select($sql9);
              if(count($rows91) > 0){
                foreach ($rows91 as $rows92) {
                    $row9 = (array) $rows92;
                    $guarantorName = $row9['guarantorName'];
                    $guarantorDOB = $row9['guarantorDOB'];
                    $guarantorStreet = $row9['guarantorStreet'];
                    $guarantorCity = $row9['guarantorCity'];
                    $guarantorState = $row9['guarantorState'];
                    $guarantorZip = $row9['guarantorZip'];
                    $guarantorPhoneHome = $row9['guarantorPhoneHome'];
                }
              } else {
                $guarantorName = $fullName;
                $guarantorDOB = $dob;
                $guarantorStreet = $addrStreet1;
                $guarantorCity = $addrCity;
                $guarantorState = $addrState;
                $guarantorZip = $addrZip;
                $guarantorPhoneHome = $phoneHome;
            }

            $insuranceAddr1 = explode(",", $insuranceAddr1);
            $practiceAddr = explode(",", $practiceAddr);
            $facilityAddr = explode(",", $facilityAddr);

            $tempName = explode(" ", $fullName);
            if($tempName[0] != ""){
              $firstName = $tempName[0];
            } else {
              $firstName = "";
            }

            if($tempName[1] != "") {
              $middleName = $tempName[1];
            } else {
              $middleName = "";
            }

            if($tempName[2] != ""){
              $lastName = $tempName[2];
            } else {
              $lastName = "";
            }

            $fullName = $lastName.', '.$firstName.' '.$middleName;
            $tempName1 = explode(" ", $guarantorName);
            if($tempName1[0] != "") {
              $firstName1 = $tempName1[0];
            } else {
              $firstName1 = "";
            }

            if($tempName1[1] != "") {
              $middleName1 = $tempName1[1];
            } else {
              $middleName1 = "";
            }

            if($tempName1[2] != "") {
              $lastName1 = $tempName1[2];
            } else {
              $lastName1 = "";
            }

            $guarantorName = $lastName1.', '.$firstName1.' '.$middleName1;
            $tempDOB = explode("-", $dob);
            if($tempDOB[0] != "") {
              $year = $tempDOB[0];
            } else {
              $year = "";
            }

            if($tempDOB[1] != "") {
              $month = $tempDOB[1];
            } else {
              $month = "";
            }

            if($tempDOB[2] != "") {
              $date = $tempDOB[2];
            } else {
              $date = "";
            }

            if($gender == "M"){
              $Male = 'X';
              $Female = '';
            } else {
              $Male = '';
              $Female = 'X';
            }

            $CurrDate = date('Y-m-d');
            $tempCurrDate = explode("-", $CurrDate);
            if($tempCurrDate[0] != ""){
              $Curryear = $tempCurrDate[0];
            } else {
              $Curryear = "";
            }

            if($tempCurrDate[1] != ""){
              $Currmonth = $tempCurrDate[1];
            } else {
              $Currmonth = "";
            }

            if($tempCurrDate[2] != ""){
              $Currdate = $tempCurrDate[2];
            } else {
              $Currdate = "";
            }
            $dummy  = str_pad('', 39);


            if (!isset($facilityAddr[1])) {
                $facilityAddr[1] = '';
            }
            if (!isset($practiceAddr[1])) {
                $practiceAddr[1] = '';
            }
            
            if ($flagFileIsCreated == 0) {
                $flagFileIsCreated = 1;
                $fileNameOnly = $submitClaim.'_'.$Currmonth.''.$Currdate.''.$Curryear;
                $fileName = $folderName . $fileNameOnly;
                $myfileHandle = fopen($fileName, "w");
            }

fwrite($myfileHandle, '                                       '.$payerName.', '.$insurancePayorID.'
                                         '.$insuranceAddr1[0].' 
                                         '.$insuranceCity.' '.$insuranceState.' '.$insuranceZip.' 



                                              X    '.$policyNo.'                    
                                                                                 
   '.str_pad($fullName, 29).''.$month.' '.$date.' '.$year.'   '.$Male.'  '.$Female.'   '.str_pad($guarantorName,30).'            

   '.str_pad($addrStreet1, 28).'   X                '.str_pad($guarantorStreet, 30).'             

   '.str_pad($addrCity,25).''.str_pad($addrState,4).'                   '.str_pad($guarantorCity,25).''.str_pad($guarantorState,4).'  

   '.str_pad($addrZip,13).' '.str_pad($phoneHome,14).'                    '.str_pad($guarantorZip,13).' '.str_pad($guarantorPhoneHome,14).'  

                                                   '.$groupNo.'

                                          X           '.$month.' '.$date.' '.$year.'     '.$Male.'     '.$Female.'   

                                          X                                     

                                          X                                     

   



         SIGNATURE ON FILE             '.$Currmonth.''.$Currdate.''.$Curryear.'           SIGNATURE ON FILE       





                                                           X                     
    '.$dummy.' 0                                                                            
    '  .str_pad($diag1_temp, 12).''.str_pad($diag2_temp, 12).''.str_pad($diag3_temp, 12).''.str_pad($diag4_temp, 12).'

                                                                      
                                                     '.$authNo.'


   '.$Smonth1.' '.$Sdate1.' '.$Syear1.' '.$Emonth1.' '.$Edate1.' '.$Eyear1.' '.$placeOfService1.'     '.str_pad($proced1, 7).''.str_pad($mod11, 12).''.str_pad($pointer1, 5).''.str_pad($charge1, 9," ",STR_PAD_LEFT).'  '.str_pad($units1, 4).'   '.$individualNPI1.'  

   '.$Smonth2.' '.$Sdate2.' '.$Syear2.' '.$Emonth2.' '.$Edate2.' '.$Eyear2.' '.$placeOfService2.'     '.str_pad($proced2, 7).''.str_pad($mod12, 12).''.str_pad($pointer2, 5).''.str_pad($charge2, 9," ",STR_PAD_LEFT).'  '.str_pad($units2, 4).'   '.$individualNPI2.'  

   '.$Smonth3.' '.$Sdate3.' '.$Syear3.' '.$Emonth3.' '.$Edate3.' '.$Eyear3.' '.$placeOfService3.'     '.str_pad($proced3, 7).''.str_pad($mod13, 12).''.str_pad($pointer3, 5).''.str_pad($charge3, 9," ",STR_PAD_LEFT).'  '.str_pad($units3, 4).'   '.$individualNPI3.'  

   '.$Smonth4.' '.$Sdate4.' '.$Syear4.' '.$Emonth4.' '.$Edate4.' '.$Eyear4.' '.$placeOfService4.'     '.str_pad($proced4, 7).''.str_pad($mod14, 12).''.str_pad($pointer4, 5).''.str_pad($charge4, 9," ",STR_PAD_LEFT).'  '.str_pad($units4, 4).'   '.$individualNPI4.'  

   '.$Smonth5.' '.$Sdate5.' '.$Syear5.' '.$Emonth5.' '.$Edate5.' '.$Eyear5.' '.$placeOfService5.'     '.str_pad($proced5, 7).''.str_pad($mod15, 12).''.str_pad($pointer5, 5).''.str_pad($charge5, 9," ",STR_PAD_LEFT).'  '.str_pad($units5, 4).'   '.$individualNPI5.'  

   '.$Smonth6.' '.$Sdate6.' '.$Syear6.' '.$Emonth6.' '.$Edate6.' '.$Eyear6.' '.$placeOfService6.'     '.str_pad($proced6, 7).''.str_pad($mod16, 12).''.str_pad($pointer6, 5).''.str_pad($charge6, 9," ",STR_PAD_LEFT).'  '.str_pad($units6, 4).'   '.$individualNPI6.'  

   '.str_pad($EIN, 16).' X   '.str_pad($chartNo, 14).' X         '.str_pad(number_format(round((float)$chargeTot,2),2), 12," ",STR_PAD_LEFT).'    0.00        
                                                                                 
                         '.str_pad($facilityName, 27).''.str_pad($practiceName, 30).'
   '    .str_pad($providerName, 21).' '.str_pad($facilityAddr[0], 27).''.str_pad($practiceAddr[0], 30).' 
                '.$Currmonth.''.$Currdate.''.$Curryear.' '.str_pad($facilityAddr[1], 26).' '.str_pad($practiceAddr[1], 30).'
                         '.str_pad($facilityNPI, 12).'                '.str_pad($practiceNPI, 12).'
  




  ');

  }
            if ($flagFileIsCreated) {
              fclose($myfileHandle);
            }
            if ($num_of_row < 1){
                DB::rollback();
                $this->mMessage = "Records are empty " . $num_of_row ;
                return $this->sendResponseDefault();
            } else {
                $this->uploadToSSH2($request, $fileName, $fileNameOnly);
                $hcfa = new HCFA500Controller();
                $response = $hcfa->pdf($request, $submitClaim);
                $response = (array) json_decode($response);
                if ($response['status'] == 1) {
                  $this->mStatus = 1;
                  $this->mMessage = "Files created successfully";

                  $edi = new EdiModel();
                  $edi->invoice_num = $fileNameOnly;
                  $edi->practice_id = $this->getPracticeID();
                  $edi->location = $this->getFolderDocs();
                  $edi->created_on = now();
                  $edi->save();

                  DB::commit();
                  //return $this->textFile($request, $fileName);
                } else {
                  DB::rollback();
                  $this->mMessage = $response['message'];
                }
            }
        } catch (Exception $e) {
            DB::rollback();
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }    

    public function textFile(Request $request, $fileName = ''){
        try {
          if (strlen($fileName) < 1) {
              $fileName = public_path('/') . "edi/a.txt";
          }
          $headers = array(
                'Content-Type: text/plain',
              );
          return Response::download($fileName, 'edi.txt', $headers);      
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    public function uploadToSSH2(Request $request, $localFileName = '', $file = 'test.txt'){
        try {
          if (empty($localFileName)) {
            $localFileName = '/var/www/html/temp/test.txt';
          }

          $sftp = new SFTP($this->mSSH2IpHostname);
          if (!$sftp){
              $this->mMessage = "SSH2 connection IP failed";
              return $this->sendResponseDefault();
          }

          if (!$sftp->login($this->mSSH2Username, 
            $this->mSSH2Password)) {
            $this->mMessage = "SSH2 login failed";
            return $this->sendResponseDefault();
          } else {
            $remote_directory = 'inbound/';
            //$remote_directory = '/';
            $remoteFile = $remote_directory . $file;
            $success = true;
            //$success = $sftp->put($remoteFile, $localFileName);
            if ($success) {
              $targetFileName = $this->getFolderEDI() . 'outbound/' . $file;
              File::move($localFileName, $targetFileName);
              $this->mMessage = "File upload success";
              $this->mStatus = 1;
            } else {
              $this->mMessage = "File upload failed";
            }
          }
      } catch (Exception $e) {
          $this->mMessage = $e->getMessage();
      }
      return $this->sendResponseDefault();
    }

    public function uploadAllFilesToSSH2(Request $request){
       try {
          $folderName = $this->getFolderEDI();
          $files = scandir($folderName);
          foreach($files as $file) {
            if (strlen($file) < 3) {
              continue;
            }
            $localFileName = $folderName . $file;
            $this->uploadToSSH2($request, $localFileName, $file);
          }
      } catch (Exception $e) {
          $this->mMessage = $e->getMessage();
      }
      return $this->sendResponseDefault();
  }
}