<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : HelperController
* Description   : 
*
* Created date  : 2018-09-17 
* Created time  : 12:40 PM IST
* Author        : Rishwanth
* 
name
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use QueryException;
use Exception;
use DB;
use Session;
use Log;
use URL;
use Carbon;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use App\Users;
use App\Model\PaycodesModel;
use App\Model\LookupModel;

class HelperController extends Controller {
    public $mTmpFname;

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request, $moduleName = '', $returnType = ''){
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
        try{
            $module_name = '' . $request->get('module_name');
            $return_type = '' . $request->get('return_type');
            if (!empty($returnType)){
                $return_type = $returnType;
            }

            $this->addLogs(__FILE__, __FUNCTION__, '$module_name=>' . $module_name);

            if (!empty($moduleName)){
                $module_name = $moduleName;
            }
            $rows = [];
            if ($module_name == 'paycodes'){
                $rows = PaycodesModel::select('*')->get();
            } else if ($module_name == 'editype'){
                $rows = LookupModel::where('type', 'EDI_TYPE')
                    ->select('*')
                    ->get();
            }

            if ($rows->count() > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault($return_type);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getListYesNo($returnType=''){
        $rows = [];
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
        try{
            $this->mStatus = 1;
            $this->mMessage = 'Success';
            $rows['Yes'] = 'Yes';
            $rows['No'] = 'No';
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        $this->mData = $rows ;
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault($returnType);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getListBenifits($returnType=''){
        $rows = [];
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
        try{
            $this->mStatus = 1;
            $this->mMessage = 'Success';
            $rows[''] = '--Has Benifits--';
            $rows['Yes'] = 'Yes';
            $rows['No'] = 'No';
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        $this->mData = $rows ;
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault($returnType);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getCountryList() {
        $arr[''] = '--Country--';
        try{
            $arr['USA'] = 'USA';
        } catch (Exception $e) {
        }
        return $arr;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getSpecialityList() {
        $arr[''] = '--Speciality--';
        try{
            $arr['Behaviour Therapy'] = 'Behaviour Therapy';
            $arr['Physical Therapy'] = 'Physical Therapy';
            $arr['Occupational Therapy'] = 'Occupational Therapy';
            $arr['Speech Therapy'] = 'Speech Therapy';
        } catch (Exception $e) {
        }
        return $arr;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getSpecialityListWithId() {
        try{
            $arr[1] = 'Behaviour Therapy';
            $arr[2] = 'Physical Therapy';
            $arr[3] = 'Occupational Therapy';
            $arr[4] = 'Speech Therapy';
        } catch (Exception $e) {
        }
        return $arr;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function left($str, $len) {
        try{
            $str = substr($str, 0, $len);
        } catch (Exception $e) {
        }
        return $str;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function right($str, $len) {
        try{
            $str = substr($str, -$len);
        } catch (Exception $e) {
        }
        return $str;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getRandomDecimal() {
        $value = microtime(true);
        $value = $value - (int) $value;
        $value = str_replace(".", "", $value . "");
        return $this->left($value,3) . "." . $this->right($value,2);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getRandomInt() {
        $value = microtime(true);
        $value = $value - (int) $value;
        return $value;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function doCurlPOST($_token, $cookie, $url, $data = array(), $returnType = 'JSON'){
        try{
            $log = __FILE__ . '::' . __FUNCTION__ . "() =>". " Begin => $url";
            Log::debug($log);

            $postData = http_build_query($data, ' ','&');
            $headers[] = 'Content-type: application/x-www-form-urlencoded'; 

            $options = array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_POST => count($data),
                CURLOPT_POSTFIELDS => $postData,
                CURLOPT_HEADER     => 0,      
                CURLOPT_VERBOSE => 1,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_COOKIEFILE => $this->mTmpFname,
                CURLOPT_COOKIEJAR => $this->mTmpFname,
                CURLOPT_COOKIE => $cookie,
                CURLOPT_HTTPHEADER => $headers ,
            );

            $curl  = curl_init( $url );
            curl_setopt_array( $curl, $options );

            $response = curl_exec( $curl );
            $err     = curl_errno( $curl );
            $errmsg  = curl_error( $curl );
            $header  = curl_getinfo( $curl );            

            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                $this->mStatus = 0;
                $this->mMessage = "cURL Error #:" . $err;
            } else {
                $this->mStatus = 1;
                $this->mMessage = "Success";
                $this->mData = json_decode($response, true)['data'];
            }
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault($returnType);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function doCurlGET($url, $returnType = 'JSON'){
        $log = __FILE__ . '::' . __FUNCTION__ . '() =>'. ' Begin ';
/*        Log::debug($log);
        Log::debug($url);
*/        $this->mTmpFname = tempnam(sys_get_temp_dir(),"COOKIE");

        try{
            $options = array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST  =>"GET",
                CURLOPT_POST         => false,
                CURLOPT_HEADER        => false,      
                CURLOPT_VERBOSE => 0,
                CURLOPT_COOKIEFILE => $this->mTmpFname,
                CURLOPT_COOKIEJAR => $this->mTmpFname,
            );

            $curl      = curl_init( $url );
            curl_setopt_array( $curl, $options );

            $response = curl_exec($curl);
            $err     = curl_errno( $curl );
            $errmsg  = curl_error( $curl );
            $header  = curl_getinfo( $curl );            

            curl_close($curl);
            if ($err) {
                $this->mStatus = 0;
                $this->mMessage = "cURL Error #:$err => $errmsg" . $err;
            } else {
                $this->mStatus = 1;
                $this->mMessage = "Success";
                $this->mData = json_decode($response, true)['data'];
            }
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault($returnType);
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */

    function saveUploadFile(Request $request, $fileName, $location, $fileType = '*', $returnType = ''){
        try{
            if (!$request->hasFile($fileName)) {
                $this->mMessage = 'File is required';
                return $this->sendResponseDefault($returnType);
            }

            if (!$request->file($fileName )->isValid()) {
                $this->mMessage = 'File is invalid';
                return $this->sendResponseDefault($returnType);
            }

            $fileNameOriginal = $request->$fileName->getClientOriginalName();
            $path = $request->$fileName->path();
            $extension = $request->$fileName->extension();

            if ($fileType == "*") {
                $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "GIF", "JPG", "PNG", "doc", "txt", "docx", "pdf", "xls", "xlsx"); 
            } else if ($fileType == "IMAGE") {
                $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "GIF", "JPG", "PNG"); 
            }

            if (!in_array($extension, $valid_formats)){
                $this->mMessage = 'Invalid file format';
                return $this->sendResponseDefault($returnType);
            }

            $targetFilename = $this->getUUID() . '.' . $extension;
            $folder = public_path('/') . 'docs/' . $location . '/';

            $path = $request->$fileName->storeAs($folder, $targetFilename);
            $this->mStatus = 1;
            $this->mMessage = 'Success';
            $this->mData = ['image_name' => $fileNameOriginal, 'image_name_saved' => $targetFilename];
        } catch (Exception $e) {
            $this->mStatus = 0;
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault($returnType);
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getUUID(){
        $rows = DB::select('select uuid() as uid');
        return $rows[0]->uid;
    }

}
