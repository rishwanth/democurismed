<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ServiceLocationsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 06:00 PM IST
* Author        : Rishwanth
* 

********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;
use Session;
use DB;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

use App\Users;
use App\Model\LocationsModel;

class LocationsController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request) {
        $id = (int) $request->get('locationId');
        if ($id < 1) {
            $this->mMessage = 'Invalid location ID';
            return $this->sendResponseDefault();
        }
        $this->update($request, $id);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request, $id = 0) {
        //return $request->all();
        try{
            $input['patient_id'] = 'patientID';
            $input['home'] = 'home';
            $input['office'] = 'office';
            $input['school'] = 'school';

            $data = [];
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0){
                    continue;
                }
                $data[$tableField] = $request->get($formField);
            }
            if ($id < 1) {
                $location = LocationsModel::create($data);
            } else {
                $location = LocationsModel::find($id);
                $location->update($data);
            }
            if ($location->save()){
                if ($location->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = 'Locations added successfully';
                    $this->mData = array('id' => $location->id, 'locationId'=>$location->id);
                } else {
                    $this->mMessage = 'Locations added failed';
                }
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getLocation(Request $request) {
        try{
            $patient_id = (int) $request->get('patientID');
            

            $sql = " 1=1 ";
            if ($patient_id > 0){
                $c = "patient_id = '$patient_id'";
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= $c;
            }
            

            $rows = LocationsModel::whereRaw($sql)
                        ->select('*')
                        ->get();
            if ($rows->count()){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $message = " Records not found";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }
    
    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function updateLocation(Request $request) {
        try{
            $patient_id = (int) $request->get('patientID');
            $home = $request->get('home');
            $office = $request->get('office');
            $school = $request->get('school');

            $rows = LocationsModel::where('patient_id', '=', $patient_id)
                        ->select('*')
                        ->get();
            if ($rows->count() > 0){
                $sql = "UPDATE tb_locations SET home='$home', office='$office', school='$school' where patient_id = '$patient_id'";
                DB::statement($sql);
                $this->mStatus = 1;
                $this->mMessage = 'Update Location Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = "Invalid patient ID";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function updatePatientLocation(Request $request) {
        try{
            $patientID = $request->get('patientID');
            $home = $request->get('home');
            $office = $request->get('office');
            $school = $request->get('school');

            $sql = "UPDATE tb_locations SET home='$home', office='$office', school='$school' where patient_id='$patientID'";
            DB::statement($sql);
            $this->mStatus = 1;
            $this->mMessage = 'Update success';
            $this->mData = array('patient_id'=>$patientID);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getPatName(Request $request) {
        try{
            $patientID = (int) $request->get('patientID');

            $sql = "select id as patientID, full_name as fullName from tb_patients where id='$patientID'";

            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
            //echo json_encode($payorInfo);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }
}
