<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : HandlerController
* Description   : 
* Created date  : 
* Created time  : 
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use QueryException;
use Exception;
use Session;
use DB;
use Carbon;
use File;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Users;
use App\Http\Controllers\Controller;
use App\Model\HandlerInsurancesModel;
use App\Model\HandlerActivityModel;
use App\Model\HandlerSessionRuleModel;
use App\Model\HandlerUsersModel;
use App\Model\HandlerCodesModel;
use App\Model\InsurancesModel;
use App\Model\PracticesModel;

class HandlerController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }


    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function dashboard() {
        return view("handler_dashboard");
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */

    public function add(Request $request) {
        //return $request->All();
        // $payerName = $request->get('payerName');
        // return $payerName;
        $this->save1($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function save1(Request $request) {
        //return $request-All();
        //try{
            $payerName = $request->get('payerName');
            $practiceID = $request->get('practiceID');
            //$practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            //return $payerName;

            if (empty($payerName)) {
                $this->mMessage = 'Invalid payer name';
                return $this->sendResponseDefault();
            }
            $practice = PracticesModel::find($practiceID);
            //return $practiceID;
            if (empty($practice)) {
                $this->mMessage = 'Invalid practice id';
                return $this->sendResponseDefault();
            }

            $input['payer_name'] = 'payerName';
            $input['clearing_house_payor_id'] = 'clearingHousePayorID';
            $input['addr'] = 'addr';
            $input['practice_id'] = 'practiceID';

            // if ($id > 0) {
            //     $insurances = InsurancesModel::find($id);
            // } else {
                $insurances = new InsurancesModel();
            //}
            foreach ($input as $tableField => $formField) {
                $value = '' . $request->get($formField);
                if ($tableField == 'practice_id'){
                    $value = (int) $value;
                }
                $insurances->$tableField = $value;
            }
            if ($insurances->save()){
                if ($insurances->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = "Saved Success";
                    $this->mData = array('id' => $insurances->id, 'insuranceID' => $insurances->id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        // } catch (Exception $e) {
        //     $this->mMessage = $e->getMessage();
        // }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function insurances() {
        return view("handler_insurances");
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function activity() {
        return view("handler_activity");
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function sessions() {
        return view("handler_sessions");
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function users() {
        return view("handler_users");
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function reports() {
        return view("handler_reports");
    }


    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function cpts() {
        return view("handler_cpts");
    }




    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function addInsurance(Request $request) {
        $handler_insurance_id = (int) $request->get('handler_insurance_id');
        try {
            $input[''] = '';
            $input['payer_name'] = 'payer_name';

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $value = '' . $request->get($formField);
                if (!empty($value)){
                    $$tblField = $request->get($formField);
                }
            }
            $obj = new HandlerInsurancesModel();
            if ($handler_insurance_id > 0){
                $obj = HandlerInsurancesModel::find($handler_insurance_id);
                if (empty($obj)){
                    $this->mStatus = 0;
                    $this->mMessage = 'handler_insurance_id Not found';
                    return $this->sendResponseDefault();
                }
            }

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                if (isset($$tblField)){
                    $obj->$tblField = $$tblField;
                }
            }
            if ($obj->save()){
                $this->mStatus = 1;
                $this->mMessage = 'Added handler_insurance Success';
                if ($handler_insurance_id > 0) {
                    $this->mMessage = 'Updated handler_insurance Success';
                }
                $this->mData = ['id'=>$obj->id] ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function addActivity(Request $request) {
        $handler_activity_id = (int) $request->get('handler_activity_id');
        try {
            $input[''] = '';
            $input['name'] = 'name';
            $input['service_type_id'] = 'service_type_id';

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $value = '' . $request->get($formField);
                if (!empty($value)){
                    $$tblField = $request->get($formField);
                }
            }
            $obj = new HandlerActivityModel();
            if ($handler_activity_id > 0){
                $obj = HandlerActivityModel::find($handler_insurance_id);
                if (empty($obj)){
                    $this->mStatus = 0;
                    $this->mMessage = 'handler_activity_id Not found';
                    return $this->sendResponseDefault();
                }
            }

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                if (isset($$tblField)){
                    $obj->$tblField = $$tblField;
                }
            }
            if ($obj->save()){
                $this->mStatus = 1;
                $this->mMessage = 'Added handler_activity Success';
                if ($handler_activity_id > 0) {
                    $this->mMessage = 'Updated handler_activity Success';
                }
                $this->mData = ['id'=>$obj->id] ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function addSessionRule(Request $request) {
        $handler_session_rule_id = (int) $request->get('handler_session_rule_id');
        try {
            $input[''] = '';
            $input['name'] = 'name';
            $input['description'] = 'description';
            $input['is_run_rule'] = 'is_run_rule';
            $input['is_prevent_session_creation'] = 'is_prevent_session_creation';

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $value = '' . $request->get($formField);
                if (!empty($value)){
                    $$tblField = $request->get($formField);
                }
            }
            $obj = new HandlerSessionRuleModel();
            if ($handler_session_rule_id > 0){
                $obj = HandlerSessionRuleModel::find($handler_session_rule_id);
                if (empty($obj)){
                    $this->mStatus = 0;
                    $this->mMessage = 'id Not found';
                    return $this->sendResponseDefault();
                }
            }

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                if (isset($$tblField)){
                    $obj->$tblField = $$tblField;
                }
            }
            if ($obj->save()){
                $this->mStatus = 1;
                $this->mMessage = 'Added handler_session_rule Success';
                if ($handler_session_rule_id > 0) {
                    $this->mMessage = 'Updated handler_session_rule Success';
                }
                $this->mData = ['id'=>$obj->id] ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function addUser(Request $request) {
        $handler_user_id = (int) $request->get('handler_user_id');
        try {
            $input[''] = '';
            $input['first_name'] = 'first_name';
            $input['last_name'] = 'last_name';

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $value = '' . $request->get($formField);
                if (!empty($value)){
                    $$tblField = $request->get($formField);
                }
            }
            $obj = new HandlerUsersModel();
            if ($handler_user_id > 0){
                $obj = HandlerUsersModel::find($handler_user_id);
                if (empty($obj)){
                    $this->mStatus = 0;
                    $this->mMessage = 'handler_user_id Not found';
                    return $this->sendResponseDefault();
                }
            }

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                if (isset($$tblField)){
                    $obj->$tblField = $$tblField;
                }
            }
            if ($obj->save()){
                $this->mStatus = 1;
                $this->mMessage = 'Added handler_user Success';
                if ($handler_user_id > 0) {
                    $this->mMessage = 'Updated handler_user Success';
                }
                $this->mData = ['id'=>$obj->id] ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function addCodes(Request $request) {
        $handler_code_id = (int) $request->get('handler_code_id');
        try {
            $input[''] = '';
            $input['code'] = 'code';
            $input['handler_activity_id'] = 'handler_activity_id';

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                $value = '' . $request->get($formField);
                if (!empty($value)){
                    $$tblField = $request->get($formField);
                }
            }
            $obj = new HandlerCodesModel();
            if ($handler_code_id > 0){
                $obj = HandlerCodesModel::find($handler_code_id);
                if (empty($obj)){
                    $this->mStatus = 0;
                    $this->mMessage = 'handler_code_id Not found';
                    return $this->sendResponseDefault();
                }
            }

            foreach ($input as $formField => $tblField) {
                if (empty($formField)){
                    continue;
                }
                if (isset($$tblField)){
                    $obj->$tblField = $$tblField;
                }
            }
            if ($obj->save()){
                $this->mStatus = 1;
                $this->mMessage = 'Added handler_code Success';
                if ($handler_code_id > 0) {
                    $this->mMessage = 'Updated handler_code Success';
                }
                $this->mData = ['id'=>$obj->id] ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

     /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request){
        try{
            $handler = '' . $request->get('handler');
            $handler_activity_id = '' . $request->get('handler_activity_id');
            if (empty($handler)){
                $status = 'empty';
            }
            
            $table = 'tb_handler_' . $handler;

            if (!empty($handler_activity_id)){
                if($table == "tb_handler_codes"){
                    $sql = "select $table.* from $table where handler_activity_id = $handler_activity_id";
                }
            }

            if($handler == "insurances_right"){
                $sql = "select HI.payer_name, PI.id from tb_practices_insurance PI inner join tb_handler_insurances HI on HI.id = PI.id";
            }

            if($table == "tb_handler_activity"){
                $sql = "select $table.*, tb_service_type.name as service_name from $table inner join tb_service_type on tb_service_type.id = $table.service_type_id";
            }
            // else if($table == "tb_handler_codes"){
            //     $sql = "select $table.* from $table where handler_activity_id = ";
            // }
            else{
                $sql = "Select * from $table ";
            }
            //$sql .= " where is_active=1 ";

            $rows = DB::select($sql);
            if (count($rows) > 0 ){
                $this->mStatus = 1;
                $this->mMessage = 'success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

}
