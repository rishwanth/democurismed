<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AppointmentsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;
use Carbon;
use Rule;
use QueryException;
use Exception;
use Log;

//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\AppointmentsModel;

class AppointmentsController extends Controller {


    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function times(Request $request) {
        $data = array();
        $status = 0;
        $message = 'Not found';

        $availDate = $request->get('availDate');
        $appProvider = $request->get('appProvider');
        if (empty($availDate)) {
            $availDate = date('Y-m-d');
        }

        $sql = "SELECT A.start_date as appStartDt, A.end_date as appEndDt FROM tb_appointments A WHERE start_date between '$availDate 00:00:00' AND '$availDate 23:59:59' AND provider_id = '$appProvider'";
        $rows = DB::select($sql);
        if (count($rows) > 0){
            $data = $rows;
            $status = 1;
            $message = 'Success';
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list_appointments(Request $request){
        $practiceID = 1;
        $sql = "select id, patient_id, start_date, end_date, status, patient_name from tb_appointments ";
        //return $sql;
        $rows = DB::select($sql);
         if (count($rows) > 0){
            $response = [];
            foreach ($rows as $row) {
                $row2 = (array) $row;
                $row_array['id'] = $row2['id'];
                $row_array['start_date'] =str_replace('\'','', date('m/d/Y H:i', strtotime($row2['start_date'])));
                $row_array['end_date'] = str_replace('\'','',date('m/d/Y H:i', strtotime($row2['end_date'])));
                
                // $tempFromDt = explode(" ", $row2['start_date']);
                // $tempDt1 = explode("-",$tempFromDt[0]);
                // $tempTime1 = $tempFromDt[1];

                // $start_1 = $tempDt1[1].'/'.$tempDt1[0].'/'.$tempDt1[0].' '.$tempTime1;

                // $tempEndDt = explode(" ", $row2['end_date']);
                // $tempTime2 = $tempEndDt[1];

                // $end_1 = $tempDt1[1].'/'.$tempDt1[0].'/'.$tempDt1[0].' '.$tempTime2;

                // $row_array['start_date'] =$start_1;
                // $row_array['end_date'] = $end_1;
                $row_array['statusApp'] = $row2['status'];
                $row_array['text'] = $row2['patient_id']." - ".$row2['patient_name'];
                array_push($response, $row_array);
            }
             $this->mData = $response;
             $this->mMessage ='Success';
             $this->mStatus = 1;
         } 
        return $this->sendResponseDefault();
        //return json_encode($rows);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        $data = array();
        $status = 0;
        $message = 'Not found';

        $appID = (int) $request->get('claimID');
        $claimStatus_new = '' . $request->get('status');
        $claimID = "";

        $sql = "UPDATE tb_appointments SET status='$claimStatus_new' WHERE id = '$appID'";
        DB::statement($sql);

/*        $query5 = mysql_query("SELECT * FROM m_claims where appID ='$appID'");
        while($row5 = mysql_fetch_array($query5)){
            $claimID = $row5['claimID'];
        }
*/
        $sql = "UPDATE tb_claims SET claim_status='$claimStatus_new' WHERE app_id = '$appID'";
        DB::statement($sql);
        $status = 1;
        $message = 'Success';

        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function get(Request $request) {
        $data = array();
        $status = 0;
        $message = 'Not found';

        $appID = (int) $request->get('appID');
        $rows = AppointmentsModel::where('id', '=', $appID)
                    ->select('*')
                    ->get();
        if ($rows->count() > 0) {
            $data =  $rows;
            $status = 1;
            $message = 'Success';
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function updateBulk(Request $request) {
        $data = array();
        $status = 0;
        $message = 'Not found';

        $appID = (int) $request->get('appID');
        $claimStatus_new = '' . $request->get('statusApp');
        $claimID = "";

        $sql = "UPDATE tb_appointments SET status='$claimStatus_new' WHERE id = '$appID'";
        DB::statement($sql);

        $sql = "UPDATE tb_claims SET status='$claimStatus_new' WHERE app_id = '$appID'";
        DB::statement($sql);
        $status = 1;
        $message = 'Success';

        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function gets(Request $request) {
        try{
            $clientName = $request->get('clientName');
            $providerName = $request->get('providerName');
            $locationName = $request->get('locationName');
            $statusName = $request->get('statusName');
            $fromDate = $request->get('fromDate');
            $toDate = $request->get('toDate');  
            $POS = "";

            $sql = " where 1=1 ";

            if(!empty($clientName)){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " patient_name = '$clientName' ";
            }

            if(!empty($providerName)){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " provider = '$providerName' ";
            }

            if(!empty($statusName )){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " status = '$statusName' ";
            }

            if(!empty($locationName )){            
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " location = '$locationName'";
            }

            if(!empty($fromDate )){            
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " (start_date between '$fromdate' and date_add('$todate', interval 1 day))";
            }

            $sql = "select AP.id as appID, AP.*, A.* from tb_appointments AP inner join tb_activity A on A.id = AP.activity_id $sql";
            $rows = DB::select($sql);
                   //return $sql;

            $response =[];
            foreach ($rows as $row1) {
                $row = (array) $row1;
                $row_array['appID'] = $row['appID'];
                $row_array['clientName'] = $row['patient_name'];
                $row_array['auth_num'] = $row['auth'];
                $row_array['activity'] = $row['name'];
                $row_array['providerName'] = $row['provider'];
                $schDate = explode(' ',$row['start_date']);
                $row_array['fromDate'] = $schDate[0];
                $row_array['fromTime'] = substr($row['start_date'],11);
                $row_array['toTime'] = substr($row['end_date'],11);
                if($row['location'] == "12"){
                    $row_array['loc'] = "Home";
                } else if($row['location'] == "11"){
                    $row_array['loc'] = "Office";
                } else{
                    $row_array['loc'] = "School";
                }
                if($row['status'] == "Confirm"){
                    $row_array['appStatus'] = "Confirmed";
                } else{
                    $row_array['appStatus'] = $row['status'];
                }
                array_push($response, $row_array);
            }
            if (count($rows) > 0) {
                $this->mData = $response;
                $this->mStatus = 1;
                $this->mMessage = 'Success';
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function recent(Request $request) {
        $data = array();
        $status = 0;
        $message = 'Not found';

        $patientID = $request->get('patientID');
        $authNo = $request->get('authNo');

        $sql = "select AP.* from tb_appointments AP inner join tb_activity A on A.auth_num = AP.auth where AP.patient_id = '$patientID' AND AP.auth ='$authNo' ORDER BY AP.id DESC";

        $rows = DB::select($sql);
        if (count($rows) > 0){
            $data = $rows;
            $status = 1;
            $message = 'Success';
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function add_old(Request $request) {
        $patientName = $request->get('patientName');
        $description = $request->get('description');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $patientID = $request->get('patientID');

        $ts1 = strtotime($start_date);
        $ts2 = strtotime($end_date);
        $schTime = abs($ts1 - $ts2) / 60;
        $providerName = $request->get('providerName');
        $providerID = $request->get('providerID');
        $authNo = $request->get('authNo');
        $activity = $request->get('activity');
        $POS = $request->get('POS');

        $appoID = "";
        $claimIDLt1 = "";
        $claimIDLt2 = "";
        $created_AtLt1 = "";
        $created_AtLt2 = "";
        $claimStatus_new = $request->get('statusApp');

        if($POS == ""){
            $sql = "select POS.code from tb_auth AU inner join tb_cases CA on CA.id = AU.case_id inner join tb_patients P on P.id = CA.patient_id inner join tb_service_locations SL on SL.id = P.default_service_loc inner join tb_place_of_services  POS on POS.id = SL.place_of_service where AU.auth_num = '$authNo'";
            $rows = DB::select($sql);
            foreach ($rows as $row) {
                $POS = $row->code;
            }
        } else {
            $POS = $request->get('POS');
        }

        $aName = "";
        $totalVisits = 0;
        $sql = "select * from tb_activity where id = '$activity'";
        $rows = DB::select($sql);
        foreach ($rows as $row) {
            $totalVisits = (int)$row->remain_auth;
            $aName = $row->name;
        }

        if($totalVisits < 0){
            $this->mMessage = "no-auth";
            $this->mMessage = "not-allow";
            return $this->sendResponseDefault();
        }

        {
            $prev_claimNo = 0;
            $noOfVisits = $totalVisits - 1;
            $sql = "UPDATE tb_activity SET remain_auth='$noOfVisits' where id='$activity'";
            DB::statement($sql);

            $sql = "INSERT INTO tb_appointments(start_date, end_date, patient_name, auth, activity_id, location, provider, `desc`, status, patient_id, provider_id) VALUES ('$start_date','$end_date','$patientName','$authNo','$activity','$POS','$providerName','$description','$claimStatus_new','$patientID','$providerID')";


            DB::statement($sql);

            $sql = "SELECT * FROM tb_appointments ORDER BY id DESC LIMIT 1";
            $rows = DB::select($sql);
            $appoID = 0;
            foreach ($rows as $row) {
                $appoID = $row->id;
            }
            if($appoID < 1){
                $this->mMessage = "Appointment add failed";
                return $this->sendResponseDefault();
            }


            $sql = "SELECT CA.principal_diag, CA.def_diag_2, CA.def_diag_3, CA.def_diag_4, P.primary_care_phy, P.default_service_loc, PO.insurance_id, A.case_id, A.auth_num, AU.start_date, AU.end_date, A.cpt, A.mod1, A.mod2, A.mod3, A.mod4, A.billed_per, A.rates FROM tb_auth AU inner join tb_cases CA on CA.id = AU.case_id inner join tb_patients P on P.id = CA.patient_id inner join tb_policies PO on PO.case_id= CA.id inner join tb_activity A on A.auth_num=AU.auth_num where AU.auth_num='$authNo' AND A.id = '$activity' LIMIT 1";
            $rows = DB::select($sql);

            foreach ($rows as $row) {
                $row2 = (array) $row;
                $caseID_new = $row2['case_id'];
                $authNo_new = $row2['auth_num'];
                $fromDt_new = $row2['start_date'];
                $toDt_new = $row2['end_date'];
                $proced_new = $row2['cpt'];
                $mod1_new = $row2['mod1'].' '.$row2['mod2'].' '.$row2['mod3'].' '.$row2['mod4'];
                $units_new = $row2['billed_per'];
                $charge_new = $row2['rates'];
                $diag1_new = $row2['principal_diag'];
                $diag2_new = $row2['def_diag_2'];
                $diag3_new = $row2['def_diag_3'];
                $diag4_new = $row2['def_diag_4'];
                $primaryCarePhy_new = $row2['primary_care_phy'];
                $serviceLocID_new = $row2['default_service_loc'];
                $insuranceID_new = $row2['insurance_id'];
            }


            if( strpos( $proced_new, '-' ) !== false ){
                        $proced_new = explode('-', $proced_new);
                        $proced_new1 = $proced_new[0];
                        $proced_new2 = $proced_new[1];

                        if($units_new == "Session" || $units_new == "Unit"){
                            $units_new = '1';
                            $total_new = $charge_new;
                        } else if($units_new == "15 mins"){
                            $units_new = ($schTime/15);
                            if($units_new > 1){
                                $units_new1 = 1;
                                $units_new2 = $units_new - 1;
                            } else{
                                $units_new1 = 1;
                            }
                            $total_new1 = $units_new1 * $charge_new;
                            $total_new2 = $units_new2 * $charge_new;
                        } else {
                            $units_new = ($schTime/30);
                            if($units_new > 1){
                                $units_new1 = 1;
                                $units_new2 = $units_new - 1;
                            }
                            else{
                                $units_new1 = 1;
                            }
                            $total_new1 = $units_new1 * $charge_new;
                            $total_new2 = $units_new2 * $charge_new;
                        }

                        $fromDt_new = $start_date;
                        $toDt_new = $start_date;
                        $sql = "select detailvalue1 from tb_details where detail = 'ClaimNumber'";

                        $rows = DB::select($sql);
                        foreach ($rows as $row) {
                            $prev_claimNo = (int)$row->detailvalue1;
                        }
                        $claimNo = $prev_claimNo + 1;

                        $sql = "UPDATE tb_details SET detailvalue1='$claimNo' WHERE detail ='ClaimNumber'";
                        $claimNo = str_pad($claimNo, 5, '0', STR_PAD_LEFT); 

                        $sql = "INSERT INTO tb_claims(case_id, app_id, auth_num, from_date, to_date, proced, mod1, units, charge, total, diag1, diag2, diag3, diag4, place_of_service, is_posted, is_sent, allowed, paid, adjustment, claim_balance, status, copay, deductible, coins, primary_care_phy, service_loc_id, claim_status, insurance_id, claim_number) VALUES ('$caseID_new','$appoID','$authNo_new','$fromDt_new','$toDt_new','$proced_new1','$mod1_new','$units_new1','$charge_new','$total_new1','$diag1_new','$diag2_new','$diag3_new','$diag4_new','$POS','0','0','0.00','0.00','0.00','$total_new1','','0.00','0.00','0.00','$primaryCarePhy_new','3','$claimStatus_new','$insuranceID_new','$claimNo')";

                        DB::statement($sql);

                        $sql = "SELECT * FROM tb_claims ORDER BY id DESC LIMIT 1";
                        $rows = DB::select($sql);
                        foreach ($rows as $row) {
                            $claimIDLt1 = $row->id;
                            $created_AtLt1 = $row->created_at;
                            $sql = "INSERT INTO tb_crm(claim_id, comments, dispo, followup_date, worked_date, tyre_id, user_id) VALUES ('$claimIDLt1','','','0000-00-00','0000-00-00','1','1')";
                            DB::statement($sql);
                        }

                        $notesLt1 = "Claim has been created ";

                        $sql = "INSERT INTO tb_claims_history(claim_id, pou, purpose, notes)  VALUES ('$claimIDLt1','Claims','Created','$notesLt1')";
                        DB::statement($sql);

                        $sql = "INSERT INTO tb_claims(case_id, app_id, auth_num, from_date, to_date, proced, mod1, units, charge, total, diag1, diag2, diag3, diag4, place_of_service, is_posted, is_sent, allowed, paid, adjustment, claim_balance, status, copay, deductible, coins, primary_care_phy, service_loc_id, claim_status, insurance_id, claim_number) VALUES ('$caseID_new', '$appoID', '$authNo_new', '$fromDt_new', '$toDt_new', '$proced_new2', '$mod1_new', '$units_new2','$charge_new', '$total_new2', '$diag1_new', '$diag2_new', '$diag3_new', '$diag4_new', '$POS','0','0','0.00','0.00','0.00','$total_new2','','0.00','0.00','0.00','$primaryCarePhy_new','3','$claimStatus_new','$insuranceID_new','$claimNo')";
                        DB::statement($sql);

                        $sql = "SELECT * FROM tb_claims ORDER BY id DESC LIMIT 1";
                        $rows = DB::select($sql);
                        foreach ($rows as $row) {
                            $rowLt2 = (array) $row;
                            $claimIDLt2 = $rowLt2['id'];
                            $created_AtLt2 = $rowLt2['created_at'];

                            $sql = "INSERT INTO tb_crm(claim_id, comments, dispo, followup_date, worked_date, tyre_id, user_id) VALUES ('$claimIDLt2','','',now(),now(),'1','1')";
                            DB::statement($sql);
                        }
                        $notesLt2 = "Claim has been created";

                        $sql = "INSERT INTO tb_claims_history(claim_id, pou, purpose, notes) VALUES ('$claimIDLt2', 'Claims', 'Created', '$notesLt2')";
                        DB::statement($sql);
                        $this->mStatus=1;
                        $this->mMessage = "Success";
            } else {
                if($units_new == "Session" || $units_new == "Unit"){
                    $units_new = '1';
                    $total_new = $charge_new;
                }
                else if($units_new == "15 mins"){
                    $units_new = ($schTime/15);
                    if($units_new > 1){
                        $units_new = $units_new;
                    } else {
                        $units_new = 1;
                    }
                    $total_new = $units_new * $charge_new;
                } else {
                    $units_new = ($schTime/60);
                    if($units_new > 1){
                        $units_new = $units_new;
                    } else {
                        $units_new = 1;
                    }
                    $total_new = $units_new * $charge_new;  
                }
                $fromDt_new = $start_date;
                $toDt_new = $start_date;
                $sql = "select detailvalue1 from tb_details where detail = 'ClaimNumber'";

                $rows = DB::select($sql);
                foreach ($rows as $row) {
                    $row4 = (array) $row;
                    $prev_claimNo = $row4['detailvalue1'];
                }
                $claimNo = $prev_claimNo + 1;
                $sql = "UPDATE tb_details SET detailvalue1='$claimNo' WHERE detail ='ClaimNumber'";
                DB::statement($sql);

                $claimNo = str_pad($claimNo, 5, '0', STR_PAD_LEFT); 
                $sql = "INSERT INTO tb_claims(case_id, app_id, auth_num, from_date, to_date, proced, mod1, units, charge, total, diag1, diag2, diag3, diag4, place_of_service, is_posted, is_sent, allowed, paid, adjustment, claim_balance, status, copay, deductible, coins, primary_care_phy, service_loc_id, claim_status, insurance_id, claim_number) VALUES ('$caseID_new', '$appoID', '$authNo_new', '$fromDt_new', '$toDt_new', '$proced_new', '$mod1_new', '$units_new', '$charge_new', '$total_new', '$diag1_new', '$diag2_new', '$diag3_new','$diag4_new', '$POS', '0', '0', '0.00', '0.00', '0.00', '$total_new','', '0.00', '0.00', '0.00', '$primaryCarePhy_new', '3', '$claimStatus_new', '$insuranceID_new', '$claimNo')";
                DB::statement($sql);

                $sql = "SELECT * FROM tb_claims ORDER BY id DESC LIMIT 1";
                $rows = DB::select($sql);
                foreach ($rows as $row) {
                    $rowLt1 = (array) $row;  
                    $claimIDLt1 = $rowLt1['id'];
                    $created_AtLt1 = $rowLt1['created_at'];
                    $sql = "INSERT INTO tb_crm(claim_id, comments, dispo, followup_date, worked_date, tyre_id, user_id) VALUES ('$claimIDLt1','','',now(),now(),'1','1')";
                    DB::statement($sql);
                }
                $notesLt1 = "Claim has been created";

                $sql = "INSERT INTO tb_claims_history(claim_id, pou, purpose, notes) VALUES ('$claimIDLt1', 'Claims', 'Created', '$notesLt1')";
                DB::statement($sql);
                $this->mStatus=1;
                $this->mMessage = "Success";
            }
        }
        return $this->sendResponseDefault();
    }

        /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function addScheduler(Request $request) {
        try{
            $activity = (int) $request->get('activity');
            if ($activity < 1){
                $this->mMessage = 'Invalid activity';
                return $this->sendResponseDefault();
            }
            $start_date =  $request->get('start_date');
            $end_date = $request->get('end_date');
            if (empty($start_date)){
                $this->mMessage = 'Invalid start_date';
                return $this->sendResponseDefault();
            }
            if (empty($end_date)){
                $this->mMessage = 'Invalid end_date';
                return $this->sendResponseDefault();
            }
            $response = $this->getPlannerDetails($request);
            if ($response['status'] == 0) {
                $this->mMessage = 'Invalid planner details';
                return $this->sendResponseDefault();
            }
            $dates = $response['data'];
            DB::beginTransaction();
            $appIDs = [];
            foreach ($dates as $key => $date) {
                return $date;
                $response = $this->_add($request, $date);
                if ($response['status'] == 0) {
                    $this->mMessage = $response['message'];
                    DB::rollback();
                    return $this->sendResponseDefault();
                } else {
                    $appIDs[] = $response['data']['appID'];
                }
            }
            $this->mStatus = 1;
            $this->mMessage = 'Appointment added Success';
            $this->mData = $appIDs ;
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    public function getPlannerDetails(Request $request, $retType =  '') {
        try
        {
            $repeat_frequency = 'daily';
            $repeat_sunday = '';
            $repeat_monday = '';
            $repeat_tuesday = '';
            $repeat_wednesday = '';
            $repeat_thursday = '';
            $repeat_friday = '';
            $repeat_saturday = '';
            $repeat_count = 0;
            $repeat_count_occurrance = 0;

            $start_date = ''.$request->get('start_date');
            $end_date = ''.$request->get('end_date');
            $start_time = ''.$request->get('start_time');
            $end_time = ''.$request->get('end_time');
            if (empty($start_date)){
                $start_date = '2018-08-15';
            }
            if (empty($end_date)){
                $end_date = '2018-10-30';
            }
            if (empty($start_time)){
                $start_time = '00:01:01';
            }
            if (empty($end_time)){
                $end_time = '23:11:59';
            }

            $stime = $start_time;
            $etime = $end_time;
            $start_date .= ' ' . $stime;
            $end_date .= ' ' . $end_time;

            $sdate = Carbon::parse($start_date);
            $edate = Carbon::parse($end_date);

            $repeat_frequency = $request->get('repeat_frequency');
            if (empty($repeat_frequency)){
                $repeat_frequency = 'daily';
            }
            $repeat_count = (int) $request->get('repeat_count');
            $repeat_sunday = $request->get('repeat_sunday');
            $repeat_monday = $request->get('repeat_monday');
            $repeat_tuesday = $request->get('repeat_tuesday');
            $repeat_wednesday = $request->get('repeat_wednesday');
            $repeat_thursday = $request->get('repeat_thursday');
            $repeat_friday = $request->get('repeat_friday');
            $repeat_saturday = $request->get('repeat_saturday');
            $repeat_count_occurrance  = $request->get('repeat_count_occurrance');

            $arrByDay = [];
            if (!empty($repeat_sunday)){
                $arrByDay [] = 'SU';
            }
            if (!empty($repeat_monday)){
                $arrByDay [] = $repeat_monday;
            }
            if (!empty($repeat_tuesday)){
                $arrByDay [] = $repeat_tuesday;
            }
            if (!empty($repeat_wednesday)){
                $arrByDay [] = $repeat_wednesday;
            }
            if (!empty($repeat_thursday)){
                $arrByDay [] = $repeat_thursday;
            }
            if (!empty($repeat_friday)){
                $arrByDay [] = $repeat_friday;
            }
            if (!empty($repeat_saturday)){
                $arrByDay [] = $repeat_saturday;
            }

            //$startDate = $sdate . ' ' . $stime . ':00';
            //$endDate = $edate . ' ' . $etime . ':00';
            $startDate = $sdate ;
            $endDate = $edate ;

            $ruleStr = '';
            $ruleStr .= ";FREQ=$repeat_frequency";
            if ($repeat_count_occurrance > 0){
                $ruleStr .= ";COUNT=$repeat_count_occurrance";
            }
            if (!empty($arrByDay)){
                $c = implode(',', $arrByDay);
                $ruleStr .=  ";BYDAY=$c";
            }

            //echo '$ruleStr=>' . $ruleStr; die();

            $startDate   = new \DateTime($startDate);
            $rule        = new \Recurr\Rule($ruleStr, $startDate);
            $transformer = new \Recurr\Transformer\ArrayTransformer();
            $rule->setUntil(new \DateTime($endDate));
            $result =  (array) $transformer->transform($rule);
            $rows = [];
            $i = 0;
            foreach ($result as $key => $value) {
                foreach ($value as $key1 => $value1) {
                        $value1 = (array) ($value1);
                    foreach ($value1 as $key2 => $value2) {
                        $key2 = trim($key2);
                        if (strpos($key2, 'index' ) > -1 ){
                            continue;
                        }
                        if (strpos($key2, 'timezone_type' ) > -1 ){
                            continue;
                        }
                        if (strpos($key2, 'start' ) > -1 ){
                            $key2 = "start";
                        }
                        if (strpos($key2, 'end' ) > -1 ){
                            $key2 = "end";
                        }
                        $value2 = (array)  $value2;
                        foreach ($value2 as $key3 => $value3) {
                            if (strpos($key3, 'timezone_type' ) > -1 ){
                                continue;
                            }
                            if ($key2 == "start" && $key3 == 'date'){
                                $d = date('Y-m-d H:i:s', strtotime($value3));
                                $t = date('H:i:s', strtotime($value3));
                                $rows[$i][$key2]['start_date'] = $d;
                                $rows[$i][$key2]['start_time'] = $t;
                            } else if ($key2 == "end" && $key3 == 'date'){
                                $d = date('Y-m-d', strtotime($value3));
                                $t = date('H:i:s', strtotime($value3));
                                $t = $etime . ':00';
                                $d .= ' ' . $t;
                                $rows[$i][$key2]['end_date'] = $d;
                                $rows[$i][$key2]['end_time'] = $t;
                            } else {
                                $rows[$i][$key2][$key3] = $value3;
                            }
                        }
                    }
                    $i++;
                }
            }
            $this->mData = $rows;
            $this->mStatus = 1;
            $this->mMessage = 'Success';
        } 
        catch (Exception $e) 
        {
            $this->mMessage = $e->getMessage(); 
        }
        return $this->sendResponseDefault($retType);
   }


    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {

$proced_new=''        ;
$units_new1 = 0;
$units_new2 = 0;
$units_new = 0;
$charge_new = 0;
                $diag1_new = '';
                $diag2_new = '';
                $diag3_new = '';
                $diag4_new = '';
                $primaryCarePhy_new = '';
                $serviceLocID_new = '';
                $insuranceID_new = '';


                $caseID_new = '';
                $authNo_new = '';
                $fromDt_new = '';
                $toDt_new = '';
                $proced_new = '';
                $mod1_new = '';
        //print_r( $arr); die();

        $patientName = $request->get('patientName');
        $description = $request->get('description');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $patientID = $request->get('patientID');

        $ts1 = strtotime($start_date);
        $ts2 = strtotime($end_date);
        $schTime = abs($ts1 - $ts2) / 60;
        $providerName = $request->get('providerName');
        $providerID = $request->get('providerID');
        $authNo = $request->get('authNo');
        $activity = $request->get('activity');
        $POS = $request->get('POS');

        $appoID = "";
        $claimIDLt1 = "";
        $claimIDLt2 = "";
        $created_AtLt1 = "";
        $created_AtLt2 = "";
        $claimStatus_new = $request->get('statusApp');

        if($POS == ""){
            $sql = "select POS.code from tb_auth AU inner join tb_cases CA on CA.id = AU.case_id inner join tb_patients P on P.id = CA.patient_id inner join tb_service_locations SL on SL.id = P.default_service_loc inner join tb_place_of_services  POS on POS.id = SL.place_of_service where AU.auth_num = '$authNo'";
            $rows = DB::select($sql);
            foreach ($rows as $row) {
                $POS = $row->code;
            }
        } else {
            $POS = $request->get('POS');
        }

        $aName = "";
        $totalVisits = 0;
        $sql = "select * from tb_activity where id = '$activity'";
        $rows = DB::select($sql);
        foreach ($rows as $row) {
            $totalVisits = (int)$row->remain_auth;
            $aName = $row->name;
        }

        if($totalVisits < 0){
            $this->mMessage = "no-auth";
            $this->mMessage = "not-allow";
            return $this->sendResponseDefault();
        }


        $arr = $this->getPlannerDetails($request, 'ARRAY');
        
            foreach ($arr['data'] as $key => $value) {
                //echo 'loop = 1';
                //print_r($value); die();
                //add add addAppoin();
                # code...
         
                    $start_date = $value['start']['start_date'];
                    $end_date = $value['end']['end_date'];
            $prev_claimNo = 0;
            $noOfVisits = $totalVisits - 1;
            $sql = "UPDATE tb_activity SET remain_auth='$noOfVisits' where id='$activity'";
            DB::statement($sql);

            $sql = "INSERT INTO tb_appointments(start_date, end_date, patient_name, auth, activity_id, location, provider, `desc`, status, patient_id, provider_id) VALUES ('$start_date','$end_date','$patientName','$authNo','$activity','$POS','$providerName','$description','$claimStatus_new','$patientID','$providerID')";


            DB::statement($sql);

            $sql = "SELECT * FROM tb_appointments ORDER BY id DESC LIMIT 1";
            $rows = DB::select($sql);
            $appoID = 0;
            foreach ($rows as $row) {
                $appoID = $row->id;
            }
            if($appoID < 1){
                $this->mMessage = "Appointment add failed";
                return $this->sendResponseDefault();
            }


            $sql = "SELECT CA.principal_diag, CA.def_diag_2, CA.def_diag_3, CA.def_diag_4, P.primary_care_phy, P.default_service_loc, PO.insurance_id, A.case_id, A.auth_num, AU.start_date, AU.end_date, A.cpt, A.mod1, A.mod2, A.mod3, A.mod4, A.billed_per, A.rates FROM tb_auth AU inner join tb_cases CA on CA.id = AU.case_id inner join tb_patients P on P.id = CA.patient_id inner join tb_policies PO on PO.case_id= CA.id inner join tb_activity A on A.auth_num=AU.auth_num where AU.auth_num='$authNo' AND A.id = '$activity' LIMIT 1";
            $rows = DB::select($sql);

            foreach ($rows as $row) {
                $row2 = (array) $row;
                $caseID_new = $row2['case_id'];
                $authNo_new = $row2['auth_num'];
                $fromDt_new = $row2['start_date'];
                $toDt_new = $row2['end_date'];
                $proced_new = $row2['cpt'];
                $mod1_new = $row2['mod1'].' '.$row2['mod2'].' '.$row2['mod3'].' '.$row2['mod4'];
                $units_new = $row2['billed_per'];
                $charge_new = $row2['rates'];
                $diag1_new = $row2['principal_diag'];
                $diag2_new = $row2['def_diag_2'];
                $diag3_new = $row2['def_diag_3'];
                $diag4_new = $row2['def_diag_4'];
                $primaryCarePhy_new = $row2['primary_care_phy'];
                $serviceLocID_new = $row2['default_service_loc'];
                $insuranceID_new = $row2['insurance_id'];
            }


            if( strpos( $proced_new, '-' ) !== false ){
                        $proced_new = explode('-', $proced_new);
                        $proced_new1 = $proced_new[0];
                        $proced_new2 = $proced_new[1];

                        if($units_new == "Session" || $units_new == "Unit"){
                            $units_new1 = '1';
                            $total_new1 = $charge_new;
                        } else if($units_new == "15 mins"){
                            $units_new = ($schTime/15);
                            if($units_new > 1){
                                $units_new1 = 1;
                                $units_new2 = $units_new - 1;
                            } else{
                                $units_new1 = 1;
                            }
                            $total_new1 = $units_new1 * $charge_new;
                            $total_new2 = $units_new2 * $charge_new;
                        } else {
                            $units_new = ($schTime/30);
                            if($units_new > 1){
                                $units_new1 = 1;
                                $units_new2 = $units_new - 1;
                            }
                            else{
                                $units_new1 = 1;
                            }
                            $total_new1 = $units_new1 * $charge_new;
                            $total_new2 = $units_new2 * $charge_new;
                        }

                        $fromDt_new = $start_date;
                        $toDt_new = $start_date;
                        $sql = "select detailvalue1 from tb_details where detail = 'ClaimNumber'";

                        $rows = DB::select($sql);
                        foreach ($rows as $row) {
                            $prev_claimNo = (int)$row->detailvalue1;
                        }
                        $claimNo = $prev_claimNo + 1;

                        $sql = "UPDATE tb_details SET detailvalue1='$claimNo' WHERE detail ='ClaimNumber'";
                        $claimNo = str_pad($claimNo, 5, '0', STR_PAD_LEFT); 

                        $sql = "INSERT INTO tb_claims(case_id, app_id, auth_num, from_date, to_date, proced, mod1, units, charge, total, diag1, diag2, diag3, diag4, place_of_service, is_posted, is_sent, allowed, paid, adjustment, claim_balance, status, copay, deductible, coins, primary_care_phy, service_loc_id, claim_status, insurance_id, claim_number) VALUES ('$caseID_new','$appoID','$authNo_new','$fromDt_new','$toDt_new','$proced_new1','$mod1_new','$units_new1','$charge_new','$total_new1','$diag1_new','$diag2_new','$diag3_new','$diag4_new','$POS','0','0','0.00','0.00','0.00','$total_new1','','0.00','0.00','0.00','$primaryCarePhy_new','3','$claimStatus_new','$insuranceID_new','$claimNo')";

                        DB::statement($sql);

                        $sql = "SELECT * FROM tb_claims ORDER BY id DESC LIMIT 1";
                        $rows = DB::select($sql);
                        foreach ($rows as $row) {
                            $claimIDLt1 = $row->id;
                            $created_AtLt1 = $row->created_at;
                            $sql = "INSERT INTO tb_crm(claim_id, comments, dispo, followup_date, worked_date, tyre_id, user_id) VALUES ('$claimIDLt1','','','0000-00-00','0000-00-00','1','1')";
                            DB::statement($sql);
                        }

                        $notesLt1 = "Claim has been created ";

                        $sql = "INSERT INTO tb_claims_history(claim_id, pou, purpose, notes)  VALUES ('$claimIDLt1','Claims','Created','$notesLt1')";
                        DB::statement($sql);

                        $sql = "INSERT INTO tb_claims(case_id, app_id, auth_num, from_date, to_date, proced, mod1, units, charge, total, diag1, diag2, diag3, diag4, place_of_service, is_posted, is_sent, allowed, paid, adjustment, claim_balance, status, copay, deductible, coins, primary_care_phy, service_loc_id, claim_status, insurance_id, claim_number) VALUES ('$caseID_new', '$appoID', '$authNo_new', '$fromDt_new', '$toDt_new', '$proced_new2', '$mod1_new', '$units_new2','$charge_new', '$total_new2', '$diag1_new', '$diag2_new', '$diag3_new', '$diag4_new', '$POS','0','0','0.00','0.00','0.00','$total_new2','','0.00','0.00','0.00','$primaryCarePhy_new','3','$claimStatus_new','$insuranceID_new','$claimNo')";
                        DB::statement($sql);

                        $sql = "SELECT * FROM tb_claims ORDER BY id DESC LIMIT 1";
                        $rows = DB::select($sql);
                        foreach ($rows as $row) {
                            $rowLt2 = (array) $row;
                            $claimIDLt2 = $rowLt2['id'];
                            $created_AtLt2 = $rowLt2['created_at'];

                            $sql = "INSERT INTO tb_crm(claim_id, comments, dispo, followup_date, worked_date, tyre_id, user_id) VALUES ('$claimIDLt2','','',now(),now(),'1','1')";
                            DB::statement($sql);
                        }
                        $notesLt2 = "Claim has been created";

                        $sql = "INSERT INTO tb_claims_history(claim_id, pou, purpose, notes) VALUES ('$claimIDLt2', 'Claims', 'Created', '$notesLt2')";
                        DB::statement($sql);
                        DB::commit();
                        $this->mStatus=1;
                        $this->mMessage = "Success";
                        $this->mData = ['id' => $appoID, 'appointmentID' => $appoID];
            } else {
                if($units_new == "Session" || $units_new == "Unit"){
                    $units_new = '1';
                    $total_new = $charge_new;
                }
                else if($units_new == "15 mins"){
                    $units_new = ($schTime/15);
                    if($units_new > 1){
                        $units_new = $units_new;
                    } else {
                        $units_new = 1;
                    }
                    $total_new = $units_new * $charge_new;
                } else {
                    $units_new = ($schTime/60);
                    if($units_new > 1){
                        $units_new = $units_new;
                    } else {
                        $units_new = 1;
                    }
                    $total_new = $units_new * $charge_new;  
                }
                $fromDt_new = $start_date;
                $toDt_new = $start_date;
                $sql = "select detailvalue1 from tb_details where detail = 'ClaimNumber'";

                $rows = DB::select($sql);
                foreach ($rows as $row) {
                    $row4 = (array) $row;
                    $prev_claimNo = $row4['detailvalue1'];
                }
                $claimNo = $prev_claimNo + 1;
                $sql = "UPDATE tb_details SET detailvalue1='$claimNo' WHERE detail ='ClaimNumber'";
                DB::statement($sql);

                $claimNo = str_pad($claimNo, 5, '0', STR_PAD_LEFT); 
                $sql = "INSERT INTO tb_claims(case_id, app_id, auth_num, from_date, to_date, proced, mod1, units, charge, total, diag1, diag2, diag3, diag4, place_of_service, is_posted, is_sent, allowed, paid, adjustment, claim_balance, status, copay, deductible, coins, primary_care_phy, service_loc_id, claim_status, insurance_id, claim_number) VALUES ('$caseID_new', '$appoID', '$authNo_new', '$fromDt_new', '$toDt_new', '$proced_new', '$mod1_new', '$units_new', '$charge_new', '$total_new', '$diag1_new', '$diag2_new', '$diag3_new','$diag4_new', '$POS', '0', '0', '0.00', '0.00', '0.00', '$total_new','', '0.00', '0.00', '0.00', '$primaryCarePhy_new', '3', '$claimStatus_new', '$insuranceID_new', '$claimNo')";
                DB::statement($sql);

                $sql = "SELECT * FROM tb_claims ORDER BY id DESC LIMIT 1";
                $rows = DB::select($sql);
                foreach ($rows as $row) {
                    $rowLt1 = (array) $row;  
                    $claimIDLt1 = $rowLt1['id'];
                    $created_AtLt1 = $rowLt1['created_at'];
                    $sql = "INSERT INTO tb_crm(claim_id, comments, dispo, followup_date, worked_date, tyre_id, user_id) VALUES ('$claimIDLt1','','',now(),now(),'1','1')";
                    DB::statement($sql);
                }
                $notesLt1 = "Claim has been created";

                $sql = "INSERT INTO tb_claims_history(claim_id, pou, purpose, notes) VALUES ('$claimIDLt1', 'Claims', 'Created', '$notesLt1')";
                DB::statement($sql);
                $this->mStatus=1;
                $this->mMessage = "Success";
            }

        }
        return $this->sendResponseDefault();
    }


}