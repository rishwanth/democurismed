<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AlertsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : Anand
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;

//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Model\AlertsModel;
use Illuminate\Database\QueryException;

class AlertsController extends Controller {

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request) {
        try {
            $input[''] = '';
            $input['desc'] = 'alert_desc';
            $input['is_edit_patient'] = 'alerts_EditPatient';
            $input['is_statements'] = 'alerts_Statements';
            $input['is_deposits'] = 'alerts_Deposits';
            $input['is_charge'] = 'alerts_Charge';
            $input['is_view_claims'] = 'alerts_ViewClaims';
            $input['is_schedule_app'] = 'alerts_ScheduleApp';
            $patient_id = $request->get('alert_patientID');
            $objModel = AlertsModel::find($patient_id);

            if (!$objModel){
                $objModel = new AlertsModel();
                $objModel->patient_id = $patient_id;
            }
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0) {
                    continue;
                }
                $$tableField = $request->get($formField);
                $objModel->$tableField = $$tableField;
            }
            $objModel->save();

            $status = 0;
            $message = 'Save failed';
            $data = array();          
            if ($objModel->id > 0) {
                $status = 1;
                $message = 'Saved success';
            }
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function edit()
    {
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function view()
    {
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request){
    }
}