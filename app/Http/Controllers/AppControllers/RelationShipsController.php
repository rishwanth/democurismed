<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : RelationShipsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : RISHWANTH
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;

//use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\RelationShipsModel;

class RelationShipsController extends Controller {

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request){
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request){
        $id = (int) $request->get('relationID');
        if ($id < 1){
            $this->message = 'Invalid relationships ID';
            return $this->sendResponseDefault();
        }
        return $this->update($request, $id);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request, $id = 0) {
        try {
            $input[''] = '';
            $input['full_name'] = 'fullName';
            $input['gender'] = 'gender';
            $input['practice_id'] = 'practiceID';

            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            if ($id > 0) {
                $objModel = RelationShipsModel::find($id);
            } else {
                $objModel = new RelationShipsModel();
                $objModel->practice_id = $practiceID;
            }

            if (empty($objModel)){
                $this->message = 'Invalid practice ID';
                return $this->sendResponseDefault();
             }

            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0) {
                    continue;
                }
                $$tableField = $request->get($formField);
                $objModel->$tableField = $$tableField;
            }
            if ($objModel->save()){
                if ($objModel->id > 0){
                    $this->mStatus = 1;
                    $this->mMessage = 'RelationShips Saved successfull';
                    $this->mData = array('id' => $objModel->id, 'ID' => $objModel->id);
                }
            } else {
                $this->mMessage = 'Relationships Save failed.';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function load(Request $request) {
        try {
            $relationID = (int) $request->get('relationID');
            $rows = RelationShipsModel::where('id', $relationID)
                    ->where('is_active', 1)
                    ->select('*')
                    ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Relationships Listed Successfully';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function _f1(){
        try {
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

}