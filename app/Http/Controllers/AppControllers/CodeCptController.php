<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AlertsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : Anand
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;

//use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\CodeCptModel;

class CodeCptController extends Controller {

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request) {
        $id = (int) $request->get('id');
        return $this->update($request, $id);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request, $id = 0) {
        try {
            $input[''] = '';
            $input['cpt_code'] = 'cptCode';
            $input['description'] = 'description';
            $input['charge'] = 'charge';
            $cptCode = $request->get('cptCode') . '';
            
            if (empty($cptCode)){
                $this->mMessage = 'CPT Code is required';
                return $this->sendResponseDefault();
            }

            $objModel = array();
            if ($id > 0 ) {
                $objModel = CodeCptModel::find($id);
            } else {
                $count = CodeCptModel::where('cpt_code',$cptCode)
                        ->select('id')
                        ->get()
                        ->count();
                if ($count > 0){
                    $this->mMessage = 'This CPT Code Already Exists';
                    return $this->sendResponseDefault();
                }
                $objModel = new CodeCptModel();
            }

            if (empty($objModel)){
                $this->mMessage = 'Not found';
                return $this->sendResponseDefault();
            }
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0) {
                    continue;
                }
                $$tableField = $request->get($formField);
                $objModel->$tableField = $$tableField;
            }
            $objModel->save();

            if ($objModel->id > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Saved success';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list(Request $request){
        $status = 0;
        $message = "Records not found";
        $data = array();

        $cptID = $request->get('cptID');
        $sql = " 1=1";
        if (!empty($codeType)){
            $c = " id = '$cptID' ";
            if (strlen($sql) > 0) {
                $sql .= " and "; 
            }
            $sql .= $c;
        }

        $rows = CodeCptModel::select('*')
                    ->whereRaw($sql)
                    ->get();
        if ($rows->count() > 0) {
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }

     /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function listCPT(Request $request){   
        try {
            $description = $request->get('description');
            $cptCode = $request->get('cptCode');
            $sql = ' cpt_code != null ';
            
            if (empty($description)){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " description='$description' ";
            }
            if (empty($cptCode)){
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= " cpt_code='$cptCode' ";
            }
            $rows = CodeCptModel::whereRaw($sql)
                        ->select('*')
                        ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Codes CPT Listed successfully.';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

 }