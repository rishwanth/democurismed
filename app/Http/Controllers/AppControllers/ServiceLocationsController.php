<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ServiceLocationsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 06:00 PM IST
* Author        : Rishwanth
* 

********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;
use Validator;
use QueryException;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Users;
use App\Model\ServiceLocationsModel;

class ServiceLocationsController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request) {
        $id = (int) $request->get('serviceLocID');
        if ($id < 1) {
            $this->mMessage = 'Service Location ID is required';
            return $this->sendResponseDefault();
        }
        return $this->update($request, 0);
    }

   
    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request, $id = 0) {
        try{
            $internalName = $request->get('internalName');
            if (empty($internalName)){
                $this->mMessage = "internal Name name is required";
                return $this->sendResponseDefault();
            }
            $input['internal_name'] = 'internalName';
            $input['npi'] = 'NPI';
            $input['time_zone'] = 'timeZone';
            $input['legacy_num_type'] = 'legacyNoType';
            $input['legacy_num'] = 'legacyNo';
            $input['billing_name'] = 'billingName';
            $input['address'] = 'address';
            $input['phone'] = 'phone';
            $input['fax'] = 'fax';
            $input['place_of_service'] = 'placeOfService';
            $input[''] = '';

            if ($id > 0) {
                $serviceLocation = ServiceLocationsModel::find($id);
            } else {
                $serviceLocation = new ServiceLocationsModel();
            }
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0){
                    continue;
                }
                $serviceLocation->$tableField = $request->get($formField);
            }
            if ($serviceLocation->save()){
                if ($serviceLocation->id > 0){
                    $this->mMessage = "Service Locations Updated Success";
                    $this->mStatus =1;
                    $this->mData=array('id' => $serviceLocID, 'serviceLocID' => $serviceLocID);
                }
            } else {
                $this->mMessage = "Service Locations Updated Failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $this->getDBError($e);
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list(Request $request) {
        try{
            $rows = ServiceLocationsModel::where('is_active',1)
                        ->select('*')
                        ->get();
            if ($rows->count()){
                $this->mStatus = 1;
                $this->mMessage = 'Service Location List Successful.';
                $this->mData = $rows ;
            } else {
                $this->mMessage = "Records not found";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    
    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function addServiceLocation(Request $request) {
        return $this->update($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getFacilityInfo(Request $request) {
        try{
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            $sql = "select concat(address, ',', city, ',', state, zip) as address, SL.* from tb_service_locations SL where practice_id='$practiceID'";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Facility Info are listed success.';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getServiceLocation(Request $request) {
        try{
            $sql = "select concat(id, '-', internal_name) as label from tb_service_locations";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
   }
    
}
