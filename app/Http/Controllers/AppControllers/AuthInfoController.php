<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : RegisterController
* Description   : 
*
* Created date  : 2018-08-01 
* Created time  : 06:00 AM IST
* Author        : Rishwanth
* 
* History       : 2018-04-04 07:30 PM IST, Rishwanth, Code clean up
* History       : 2018-04-04 08:00 PM IST, Rishwanth, renamed the function name
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Users;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Model\AuthInfoModel;

class AuthInfoController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }

    /**
     * 	Purpose 	:
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */
    public function add(Request $request) {
    	try {
            $patient_id = $request->get('patientID');
            $case_id = $request->get('caseID');
			$auth_num = $request->get('authNo');
			$tos = $request->get('tos');
			$start_date = $request->get('startDt');
			$end_date = $request->get('endDt');
			$policy_entity =  $request->get('policyEntity');
            
	        $auth = new AuthInfoModel();
	        $auth->patient_id = $patient_id;
	        $auth->case_id = $case_id;
	        $auth->auth_num = $auth_num;
			$auth->tos = $tos;
			$auth->start_date = $start_date;
			$auth->end_date = $end_date;
			$auth->policy_entity = $policy_entity;

	        if ($auth->save()){
		        if ($auth->id > 0) {
                    $this->mMessage = 'AuthInfo Added success';
                    $this->mStatus = 1;
                    $this->mData = array('id'=>$auth->id, 'ID'=>$auth->id);
				} else {
                    $this->mMessage = 'AuthInfo added failed';
                }
			} else {
                $this->mMessage = 'AuthInfo added failed';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getAuthInfoList(Request $request){
        $case_id = $request->get('caseID');
        $message = 'Not found';
        $data = array();
        $status = 0;

        $sql = "select A.*, I.payer_name from tb_auth A inner join tb_policies P on P.auth_num = A.auth_num inner join tb_insurances I on I.id = P.insurance_id where A.case_id = '$case_id'";

        $rows = DB::select($sql);
        $rows1 =[];
        if (count($rows) > 0){
            $status = 1;
            $message = 'Success';
            foreach ($rows as $row) {
                $rows2 =[];
                $therapy = "ST";
                if($row->description == "Behaviour Therapy"){
                    $therapy = "BT";
                } else if($row->description == "Physical Therapy"){
                    $therapy = "PT";
                } else if($row->description == "Occupational Therapy"){
                    $therapy = "OT";
                }
                $rows2['key'] = $row->auth_num;
                $rows2['label'] = $therapy.' - '.$row->payer_name .' - '.$row->auth_num . ' - Auth From : (' . $row->start_date .' to '.$row->end_date .')';
                array_push($rows1, $rows2);
            }
            $data = $rows1;
        }
        return $this->sendResponse($status, $message, $data);
   }     

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getAuthInfo(Request $request){
        try{
            $case_id = (int) $request->get('caseID');
            $policy_entity = (int) $request->get('policyEntity');
            $sql = "select A.* from tb_auth A inner join tb_cases CA on CA.id = A.case_id where A.case_id = '$case_id' AND A.policy_entity='$policy_entity'";

            $rows = DB::select($sql);

            if (count($rows) == 0){
                $this->mMessage = 'Not found';
            } else {
                $this->mStatus = 0;
                $this->mMessage = 'Success';
                $this->mData =  $rows;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
   }     

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function patient(Request $request){
        try{
            $patientID = (int) $request->get('patientID');

            $sql = "select CA.description, I.payer_name, A.auth_num, PO.policy_num, A.start_date, A.end_date from tb_auth A inner join tb_cases CA on CA.id = A.case_id inner join tb_policies PO on PO.case_id = CA.id inner join tb_auth A1 on A1.case_id = PO.case_id inner join tb_insurances I on I.id = PO.insurance_id  where A.patient_id = '$patientID' ";

            $rows = DB::select($sql);
            $rows1 = [];            
            if (count($rows) > 0){
                $status = 1;
                $message = 'Success';
                foreach ($rows as $row) {
                    $rows2 =[];
                    $therapy = "ST";
                    if($row->description == "Behaviour Therapy"){
                        $therapy = "BT";
                    }
                    else if($row->description == "Physical Therapy"){
                        $therapy = "PT";
                    }
                    else if($row->description == "Occupational Therapy"){
                        $therapy = "OT";
                    }
                    $rows2['key'] = $row->auth_num;
                    $rows2['label'] = $therapy .' - '.$row->payer_name. ' - '.$row->auth_num . ' - Auth From : (' . $row->start_date . ' to ' . $row->end_date . ')';
                    array_push($rows1, $rows2);
                }
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows1;
            } else {
                $this->mMessage = 'Record not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
   }     

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getAuthInfoByAuthNum(Request $request){
        try{
            $authNo = $request->get('authNo');
            $rows = AuthInfoModel::where('auth_num','=', $authNo)
                        ->select('*')
                        ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Records are empty';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getTOS(Request $request){
        try{
            $authNo = $request->get('auth');
            $rows = AuthInfoModel::where('auth_num','=', $authNo)
                    ->select('tos')
                    ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Records are empty';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getAuthInfo1(Request $request){

        try{
            $caseID = (int) $request->get('caseID');
            $sql = "select AU.* from tb_auth AU, tb_cases CA where CA.id = AU.case_id and AU.id = '$caseID' and AU.policy_entity='1'";

            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Records are empty';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getAuthInfo2(Request $request){
        try{
            $caseID = (int) $request->get('caseID');
            $sql = "select AU.* from tb_auth AU, tb_cases CA where CA.id = AU.case_id and AU.id = '$caseID' and AU.policy_entity='2'";

            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            } else {
                $this->mMessage = 'Records are empty';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getAuth1(Request $request) {
        try{
            $case_id = (int) $request->get('caseID');
            $sql = "select A.auth_num, A.tos as description, A.start_date, A.end_date, I.payer_name from tb_auth A inner join tb_cases CA on CA.id = A.case_id  inner join tb_policies PO on PO.case_id = CA.id inner join tb_insurances I on I.id = PO.insurance_id  where A.case_id = '$case_id' AND PO.policy_entity='1' ";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Record not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getAuths(Request $request) {
        try{
            $caseID = (int) $request->get('caseID');
            $sql = "select AU.*, AU.tos as description, I.payer_name from tb_auth AU inner join tb_policies PO on PO.auth_num=AU.auth_num inner join tb_insurances I on I.id=PO.insurance_id where AU.case_id='$caseID'";
            $rows = DB::select($sql);
            $auth = array();
            if (count($rows) > 0){
                foreach ($rows as $row) {
                    $therapy = "ST";
                    if($row->description == "Behaviour Therapy"){
                        $therapy = "BT";
                    }else if($row->description=="Physical Therapy"){
                        $therapy = "PT";
                    }else if($row->description=="Occupational Therapy"){
                        $therapy = "OT";
                    }
                    $row_array['key'] = $row->auth_num;
                    $row_array['label'] = $therapy.' - '.$row->payer_name.' - '.$row->auth_num.' - Auth From : ('.$row->start_date.' to '.$row->end_date.')';

                    array_push($auth, $row_array);
                }
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $auth;
            } else {
                $this->mMessage = 'Record not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

}
