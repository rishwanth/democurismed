<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : DetailsController
* Description   : 
*
* Created date  : 2018-08-31 
* Created time  : 04:00 AM IST
* Author        : Rishwanth
* 

********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;
use Validator;
use QueryException;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Users;
use App\Model\DetailsModel;

class DetailsController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        return $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        $id = (int) $request->get('detailsID');
        if ($id < 1) {
            $this->mMessage = 'Details ID is required';
            return $this->sendResponseDefault();
        }
        return $this->save($request, $id);
    }

   
    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request, $id = 0) {
        try{
            $detail = $request->get('detail');
            if (empty($detail)){
                $this->mMessage = "detail is required";
                return $this->sendResponseDefault();
            }
            $input['detail'] = 'detail';
            $input['detailvalue1'] = 'detailvalue1';
            $input['detailvalue2'] = 'detailvalue2';
            $input[''] = '';

            if ($id > 0) {
                $details = DetailsModel::find($id);
            } else {
                $details = new DetailsModel();
            }
            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0){
                    continue;
                }
                $details->$tableField = $request->get($formField);
            }
            if ($details->save()){
                if ($details->id > 0){
                    $this->mMessage = "Details saved Success";
                    $this->mStatus =1;
                    $this->mData=array('id' => $details->id, 'detailsID' => $details->id);
                }
            } else {
                $this->mMessage = "Details Saved Failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        } catch (QueryException $e) {
            $this->mMessage = $this->getDBError($e);
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list(Request $request) {
        try{
            $rows = DetailsModel::select('*')
                        ->get();
            if ($rows->count()){
                $this->mStatus = 1;
                $this->mMessage = 'Details List Successful.';
                $this->mData = $rows ;
            } else {
                $this->mMessage = "Records not found";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }
    
}
