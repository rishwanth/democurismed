<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : POSController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : Anand
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;

//use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\POSModel;

class POSController extends Controller {

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request) {
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function edit(){
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function view(){
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function list(Request $request){
        $message = 'Records not found';
        $data = array();
        $status = 0;

        $id = $request->get('id');
        $sql = " 1=1 ";
        if (!empty($id)){
            $c = "id = '$id'";
            if (strlen($sql) > 0){
                $sql .= " and ";
            }
            $sql .= $c;
        }
        $rows = POSModel::whereRaw($sql)
                    ->select('*')
                    ->get();
        if ($rows->count()){
            $rows =  $rows;
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getInfo(Request $request){
        $message = 'Records not found';
        $data = array();
        $status = 0;

        $serviceLocID = (int) '0' . $request->get('serviceLocID');
        $sql = "select SL.*, POS.* from tb_service_locations SL inner join tb_place_of_services POS on POS.code = SL.place_of_service where SL.id = '$serviceLocID'";
        $rows = DB::select($sql);

        if (count($rows)){
            $rows =  $rows;
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }

        return $this->sendResponse($status, $message, $data);
    }
}