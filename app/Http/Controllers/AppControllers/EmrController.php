<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ActivityController
* Description   : 
*
* Created date  : 2018-08-01 
* Created time  : 06:00 AM IST
* Author        : Rishwanth
* 
* History       : 2018-04-04 07:30 PM IST, Rishwanth, Code clean up
* History       : 2018-04-04 08:00 PM IST, Rishwanth, renamed the function name
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use QueryException;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Users;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\ActivityModel;

class EmrController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: 
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function emr() {
        return view("emr");
    }


}
