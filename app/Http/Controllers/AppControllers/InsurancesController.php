<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AlertsController
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 09:00 PM IST
* Author        : Anand
* 
********************************************************************************************/
namespace App\Http\Controllers\AppControllers;
use Session;
use DB;
use Exception;
use QueryException;

//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\InsurancesModel;
use App\Model\InsuranceSetupModel;
use App\Model\PracticesModel;

class InsurancesController extends Controller {

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function add(Request $request) {
        //return $request->All();
        // $payerName = $request->get('payerName');
        // return $payerName;
        $this->save1($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function create(Request $request) {
        $this->save($request, 0);
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function save1(Request $request, $id = 0) {
        //return $request-All();
        //try{
            $payerName = $request->get('payerName');
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            return $payerName;

            if (empty($payerName)) {
                $this->mMessage = 'Invalid payer name';
                return $this->sendResponseDefault();
            }
            $practice = PracticesModel::find($practiceID);
            if (empty($practice)) {
                $this->mMessage = 'Invalid practice id';
                return $this->sendResponseDefault();
            }

            $input['payer_name'] = 'payerName';
            $input['clearing_house_payor_id'] = 'clearingHousePayorID';
            $input['addr'] = 'addr';
            $input['practice_id'] = 'practiceID';

            if ($id > 0) {
                $insurances = InsurancesModel::find($id);
            } else {
                $insurances = new InsurancesModel();
            }
            foreach ($input as $tableField => $formField) {
                $value = '' . $request->get($formField);
                if ($tableField == 'practice_id'){
                    $value = (int) $value;
                }
                $insurances->$tableField = $value;
            }
            if ($insurances->save()){
                if ($insurances->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = "Saved Success";
                    $this->mData = array('id' => $insurances->id, 'insuranceID' => $insurances->id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        // } catch (Exception $e) {
        //     $this->mMessage = $e->getMessage();
        // }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function list(Request $request) {
        try{
            $rows = InsurancesModel::select('*')
                        ->get();

            if ($rows->count() > 0){
                $this->mData =  $rows;
                $this->mStatus = 1;
                $this->mMessage = "Success";
            } else {
                $this->mMessage = "Records are not found";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getInsurances(Request $request){
        try{
            $id = $request->get('id');
            $insuranceID = $request->get('insuranceID');
            $payerName = $request->get('payerName');
            $claimID = $request->get('claimID');
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            

            if (!empty($insuranceID)){
                $id = $insuranceID;
            }

            $sql = '';
            if (!empty($id)){
                $c = "id=$id";
                if (strlen($sql) > 0) {
                    $sql .= " and ";
                }
                $sql .= $c;
            }
            if (!empty($practiceID)){
                $c = "practice_id='$practiceID'";
                if (strlen($sql) > 0) {
                    $sql .= " and ";
                }
                $sql .= $c;
            }

            if (!empty($payerName)){
                $c = "payer_name='$payerName'";
                if (strlen($sql) > 0) {
                    $sql .= " and ";
                }
                $sql .= $c;
            }
            if (!empty($claimID)){
                $c = "payer_name='$payerName'";
                if (strlen($sql) > 0) {
                    $sql .= " and ";
                }
                $sql .= $c;
            }

            if (strlen($sql) == 0) {
                $this->mMessage = 'Invalid paramater sent.';
                return $this->sendResponseDefault();
            } 

            $sql = "select I.* from tb_insurances I where " . $sql;
            if (!empty($claimID)){
                $sql = "select I.id from tb_claims CL inner join tb_policies PO on PO.case_id = CL.case_id inner join tb_insurances I on I.id = PO.insurance_id where PO.policy_entity = '1' AND CL.id = '$claimID'";
            }
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus =   1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getName(Request $request){
        try{
            $id = (int) $request->get('insuranceID');
            $rows = InsurancesModel::where('id', $id)
                ->select('*')
                ->get();
            if ($rows->count() > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData =  $rows;
            } else {
                $this->mMessage = 'Record not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request) {
        $insuranceID = (int) $request->get('insuranceID');
        if ($insuranceID < 1){
            $this->mMessage = 'Insurance ID is required';
            return $this->sendResponseDefault();
        }
        return $this->save($request, $insuranceID);
    }

     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getlist(Request $request) {
        $practiceID = $this->getPracticeID();
        $term = $request->get('term');
        $sql = "select tb_insurances.id as id, tb_insurances.payer_name as value from tb_practices_insurance inner join tb_insurances on tb_insurances.id = tb_practices_insurance.insurance_id where tb_practices_insurance.practice_id=$practiceID ";
        if (strlen($term) > 0) {
            $sql .= " and (tb_insurances.payer_name like '%" . $term . "%') ";
        }
        //return $sql;
       $rows = DB::select($sql);
         if (count($rows) > 0){
             $this->mData = $rows;
             $this->mMessage ='Success';
             $this->mStatus = 1;
         } 
        return json_encode($rows);
    }
  
     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getAddress(Request $request) {
        try{
            $insuranceID = $request->get('insuranceID');
            $row = InsurancesModel::find($insuranceID);
            if (!empty($row)){
                $address = $row->addr . ', ' . $row->city . ', ' . $row->state . ', ' . $row->zip;
                //echo $address;
                $this->mMessage = 'Success';
                $this->mStatus = 1;
                $this->mData = array('address' => $address);
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getIdByPayerName(Request $request) {
        try{
            $payor = $request->get('payerName');
            $rows = InsurancesModel::where('payer_name', $payor)
                    ->select('id')
                    ->get();
            if ($rows->count() > 0){
                $insurance = $rows[0]->id;
                $this->mMessage = 'Success';
                $this->mStatus = 1;
                $this->mData = array('id' => $insurance, 
                    'insurance' => $insurance);
                //echo $insurance;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

     /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function getByClaimId(Request $request) {
        try{
            $claimID = (int) $request->get('claimID');
            $sql = "select I.payer_name from tb_claims CL inner join tb_policies PO on PO.case_id=CL.case_id inner join tb_insurances I on I.id=PO.insurance_id where PO.policy_entity='1' and CL.id='$claimID'";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mData = $rows;
                 $this->mMessage ='Success';
                 $this->mStatus = 1;
               //echo $insurance;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getNames(Request $request){
        try{
            $payorID = (int) $request->get('insuranceID');
            $sql = "select addr, payer_name as payerName from tb_insurances where id = '$payorID'";
            $rows = DB::select($sql);

            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getById(Request $request){
        try{
            $insuranceID = $request->get('insuranceID');
            $rows = InsurancesModel::where('id', $insuranceID)
                    ->select('*')
                    ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
           } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getByPracticeId(Request $request){
        try{
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            $rows = InsurancesModel::where('practice_id', $practiceID)
                    ->select('*')
                    ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
           } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList1(Request $request){
        try{
            $sql="select id as `key`, concat(id,'-',payer_name) as label from tb_insurances";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
           } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getListNameId(Request $request){
        try{
            $sql = "select id as insuranceID, payer_name as payerName   from tb_insurances";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList2(Request $request){
        try{
            $sql = "select concat(id, ' - ', payer_name) as label   from tb_insurances";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function listAll(Request $request){
        try{
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            $rows = InsurancesModel::where('practice_id', $practiceID)
                    ->where('is_active', 1)
                    ->select('*')
                    ->get();
            if ($rows->count() > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Invalid Practice ID or Records not found.';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
   }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function load(Request $request) {
        try{
            $insuranceID = (int) $request->get('insuranceID');
            $rows = InsurancesModel::where('id', $insuranceID)
                        ->where('is_active', 1)
                        ->select('*')
                        ->get();
            if ($rows->count() <1) { 
                $this->mMessage = 'Invalid Insurance ID.'; 
            } else {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
   }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function listPatients(Request $request) {
        try{
            $insuranceID = (int) $request->get('insuranceID');
            $insurances = InsurancesModel::find($insuranceID);
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');

            if (empty($insurances)) {
                $this->mMessage = 'Invalid Insurance ID.'; 
                return $this->sendResponseDefault();
            }
            $sql = "select distinct P.id as patientID, P.full_name as fullName  ";
            $sql .= "from tb_patients P, tb_cases CA, tb_policies PO ";
            $sql .= " where CA.patient_id=P.id and PO.case_id=CA.id and PO.insurance_id='$insuranceID' and CA.is_deleted=0 and P.is_deleted=0 and P.is_active=1 and P.practice_id='$practiceID'";
            $rows = DB::select($sql);

            //Common::WriteLog('Insurance','Patients Listed. Insurance ID:' . $insuranceID,0);

            if (count($rows) < 1) { 
                $this->mMessage = 'Record not found'; 
            } else {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function addSetup(Request $request) {
        try{
            $id = (int) $request->get('id');
            $setupId = (int) $request->get('setupId');
            if ($id < 1) {
                $id = $setupId;
            }
            return $this->saveSetup($request, $id);
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function saveSetup(Request $request, $id = 0) {
        try{
            $insurance_id = (int) $request->get('insurance_id');
            $practice_id = (int) $request->get('practice_id');
            
            if ($insurance_id < 1) {
                $this->mMessage = 'Insurance ID is required';
                return $this->sendResponseDefault();
            }
            if ($practice_id < 1) {
                $this->mMessage = 'Practice ID is required';
                return $this->sendResponseDefault();
            }
            $insurance = InsurancesModel::find($insurance_id);
            if (empty($insurance)) {
                $this->mMessage = 'Invalid Insurance ID';
                return $this->sendResponseDefault();
            }
            $practice = PracticesModel::find($practice_id);
            if (empty($practice)) {
                $this->mMessage = 'Invalid Practice ID';
                return $this->sendResponseDefault();
            }

            $input['edi_type'] = 'edi_type';
            $input['is_day_club'] = 'is_day_club';
            $input['is_capitated'] = 'is_capitated';
            $input['cms1500_31'] = 'cms1500_31';
            $input['cms1500_32a'] = 'cms1500_32a';
            $input['cms1500_32b'] = 'cms1500_32b';
            $input['cms1500_33a'] = 'cms1500_33a';
            $input['cms1500_33b'] = 'cms1500_33b';
            $input['is_active'] = 'is_active';

            if ($id > 0) {
                $setup = InsuranceSetupModel::find($id);
                if (empty($setup)){
                    $this->mMessage = 'Insurance setup not found';
                    return $this->sendResponseDefault();
                }
            } else {
                $setup = new InsuranceSetupModel();
            }
            foreach ($input as $tableField => $formField) {
                $value = '' . $request->get($formField);
                if ($tableField == 'is_day_club'){
                    $value = (int) $value;
                } else if ($tableField == 'is_capitated'){
                    $value = (int) $value;
                } else if ($tableField == 'is_active'){
                    $value = (int) $value;
                }
                $setup->$tableField = $value;
            }
            $setup->insurance_id = $insurance_id;
            $setup->practice_id = $practice_id;
            $flagAdded = 1;
            if ($id > 0) {
                $flagAdded = 0;
            }
            if ($setup->save()){
                if ($setup->id > 0) {
                    $this->mStatus = 1;
                    $this->mMessage = $flagAdded==1 ? "Added Success" : "Updated Success";
                    $id = $setup->id;
                    $this->mData = array('id' => $id, 'setupID' => $id);
                } else {
                    $this->mMessage = "Save failed";
                }
            } else {
                $this->mMessage = "Save failed";
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  input       :
     *  Output      :
     *  History     : 
     */
    public function listSetup(Request $request){
        try{
            $practice_id = (int) $request->get('practice_id');
            $sql = "select S.*, I.payer_name ";
            $sql .= " from " . $this->mTables['SETUP'] . " S, ";
            $sql .= $this->mTables['INSURANCE'] . " I ";
            $sql .= " where S.practice_id=$practice_id AND S.insurance_id=I.id ";
            $sql .= " ORDER BY I.payer_name ";
            $rows = DB::select($sql);
            if (count($rows) > 0 ) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }
}