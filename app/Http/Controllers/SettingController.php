<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SettingController extends Controller{
    public function list_insurances()
    {
        return view('list_insurances');
    }
    public function CPTs()
    {
        return view('CPTs');
    }
    public function DXs()
    {
        return view('DXs');
    }
    public function list_physicians()
    {
        return view('list_physicians');
    }
    public function list_facilities()
    {
        return view('list_facilities');
    }
    public function list_practices()
    {
        return view('list_practices');
    }
    public function add_practice()
    {
        return view('add_practice');
    }
    public function view_practice()
    {
        return view('view_practice');
    }
    public function list_serviceLocations()
    {
        return view('list_serviceLocations');
    }
}
