<?php

namespace App\Http\Controllers\API;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;

class PassportController extends Controller{

    public function signin(Request $request){
        $credentials = request(['email', 'password']);
        $flag = Auth::attempt($credentials);
        if($flag){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            Session::put('OAUTH_ACCESS_TOKEN', $success['token']);
            return $this->sendResponse(1, 'Success', $success);
        }
        else{
            $message = '';
             $message .= 'Unauthorised token access for this login credential. Mail to support team to generate access token';
            Session::forget('OAUTH_ACCESS_TOKEN');
            return $this->sendResponse(0, $message, array());
        }
    }

    public function login($email, $password){
        $message = '';
        $status = 0;
        $data = array();

        $credentials = ['email' =>  $email, 'password' => $password];

        $flag = Auth::attempt($credentials);
        if($flag){
            $status = 1;
            $user = Auth::user();
            $message =  $user->createToken('MyApp')->accessToken;
            Session::put('OAUTH_ACCESS_TOKEN', $message);
        }
        else{
             $message .= 'Unauthorised user. Ask support team to generate access token';
            Session::forget('OAUTH_ACCESS_TOKEN');
            return array('status' => $status, 'message' => $message, $data);
        }
        return array('status' => $status, 'message' => $message, $data);
    }


    public function getAccessToken($email, $password, $userId){
        if(Auth::attempt(['email' => $email, 'password' => $password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            session(['ACCESS_TOKEN' => $success['token']]);
            session(['LOGGED_USER_ID' => $userId]);
            return $this->sendResponse(1, 'Success', $success);
        }
        else{
            return $this->sendResponse(0, 'Login success but unauthorised user. Ask support team to generate access token and then continue', array());
        }
    }

    public function signup(Request $request) {
        $status = 1;
        $message = 'Success';
        $data = [];

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $status = 0;
            $message = 'Failed';
            $data = ['error'=>$validator->errors()];            
        }
        $input = $request->all();

        $user = User::where('email', '=', $input['email'])->select('*')->get();
        if ($user->count() > 0 ){
            $user = User::find($user[0]->id);
            $user->delete();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $data['oauth_token'] =  $user->createToken('MyApp')->accessToken;
        } else {
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $data['oauth_token'] =  $user->createToken('MyApp')->accessToken;
        }
        return $this->sendResponse($status, $message, $data);
    }

    public function getDetails() {
        $user = Auth::user();
        return response()->json(['success' => $user], 200);
    }

    public function register($name, $email, $password) {
        $status = 0;
        $message = 'Failed';
        $data = [];
        try{
            $input['name'] = $name;
            $input['email'] = $email;
            $input['password'] = $password;
            $input['c_password'] = $password;

            $user = User::where('email', '=', $input['email'])
                    ->select('*')
                    ->get();
            if ($user->count() > 0 ){
                $user = User::find($user[0]->id);
                $user->delete();
            }
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $user->save();
            $message =  $user->createToken(now())->accessToken;
            $data['oauth_token'] = $message;
            $status = 1;
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
        return array('status' => $status, 'message' => $message, $data);
    }

}