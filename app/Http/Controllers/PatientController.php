<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PatientController
* Description   : 
*
* Created date  : 2018-08-03 
* Created time  : 04:22 PM IST
* Author        : Anand
* 
* History       : 2018-04-04 07:00 PM IST, Rishwanth, Code clean up
* History       : 2018-04-04 08:00 PM IST, Rishwanth, renamed the function name
********************************************************************************************/
namespace App\Http\Controllers;
use Session;
use DB;
use QueryException;
use Exception;
use PDF;
use Elibyy\TCPDF\Facades\TCPDF;

//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Model\PatientsModel;
use App\Model\DetailsModel;
use App\Model\StatementsModel;

class PatientController extends Controller {

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function add()
    {
        //$data['SESSION'] = $this->mSession;
        return view('patient');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function edit()
    {
        return view('patient_edit');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function generate_statements()
    {
        return view('generate_statements');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list_statements()
    {
        return view('list_statements');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list_calendar(Request $request){
        $practiceID = 1;
        $sql = "select concat(id, ' - ' ,full_name) as `key`, full_name as label from tb_patients where practice_id=$practiceID ";
        //return $sql;
        $rows = DB::select($sql);
         if (count($rows) > 0){
             $this->mData = json_encode($rows);
             $this->mMessage ='Success';
             $this->mStatus = 1;
         } 
        //return $this->sendResponseDefault('ARRAY');
        return json_encode($rows);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function view()
    {
        $data['SESSION'] = $this->mSession;
        return view('patient_view_all', compact('data'));
        //TODO this will be move to common controller
        if(Session::has('ACCESS_TOKEN')) {
            $ACCESS_TOKEN = Session::get('ACCESS_TOKEN');
            if (strlen($ACCESS_TOKEN) < 10) {
                die('Autherization failed. Ask support team to provide Autherization token');
            }
        } else {
                die('Autherization failed. Ask support team to provide Autherization token');
        }
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request){
        try{
            $id = $request->get('id');
            $patient_id = $request->get('patientID');
            if (!empty($patient_id)) {
                $id = $patient_id;
            }

            $sql = " where 1=1 ";
            if (!empty($id)){
                $c = "id = '$id'";
                if (strlen($sql) > 0){
                    $sql .= " and ";
                }
                $sql .= $c;
            }
            $sql = "select id as `key`, concat(id, ' - ' ,full_name) as label, P.* from tb_patients P " . $sql;

            $rows = DB::select($sql);

            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function statement_list(Request $request){
        try{
            //$patient_id = $request->get('practiceID');

            $sql = "select * from tb_statements";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getLastPayment(Request $request){
        try{
            $patient_id = $request->get('patientID');

            $sql = "SELECT D.created_At FROM tb_deposit_details D inner join tb_claims CL on CL.id = D.claim_id inner join tb_cases CA on CA.id = CL.case_id inner join tb_patients P on P.id = CA.patient_id where P.id = '$patient_id' ORDER BY D.id DESC LIMIT 1";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getInsurance(Request $request){
        try{
            $patient_id = $request->get('patientID');

            $sql = "SELECT I.payer_name from tb_cases CA inner join tb_patients P on P.id = CA.patient_id  inner join tb_policies PO on PO.case_id  = CA.id inner join tb_insurances I on I.id = PO.insurance_id where P.id = '$patient_id' AND PO.policy_entity ='1'";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getLastCharge(Request $request){
        try{
            $patient_id = (int) $request->get('patientID');

            $sql = "SELECT CL.created_at from tb_claims CL inner join tb_cases CA on CA.id = CL.case_id inner join tb_patients P on P.id = CA.patient_id where P.id='$patient_id' ORDER BY CL.id DESC LIMIT 1";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getLastStatment(Request $request){
        try{
            $patient_id = (int) $request->get('patientID');
            $sql = "select created_on from tb_statements where patient_id='$patient_id' order by id desc limit 1";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getOutstandingInsurance(Request $request){
        try{
            $patient_id = (int) $request->get('patientID');
            $sql = "select ifnull(sum(CL.claim_balance),0) as total from tb_claims CL inner join tb_cases CA on CA.id = CL.case_id inner join tb_patients P on P.id = CA.patient_id where CL.is_posted = '0' and P.id = '$patient_id' and CL.insurance_id <> '-100'";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getOutstanding(Request $request){
        try{
            $patient_id = (int) $request->get('patientID');
            $sql = "select ifnull(sum(CL.claim_balance),0) as total from tb_claims CL inner join tb_cases CA on CA.id = CL.case_id inner join tb_patients P on P.id = CA.patient_id where CL.is_posted = '1' and P.id = '$patient_id' and CL.insurance_id='-100'";
            $rows = DB::select($sql);
            if (count($rows) > 0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

     /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function update(Request $request){
        try {
            $patient_id = $request->get('patientID');
            $patient_id = (int) '0' . $patient_id;
            if ($patient_id < 1) {
                $this->mMessage = "Update failed. Invalid patiend id.";
                return $this->sendResponseDefault();
            }

            $objModel = PatientsModel::find($patient_id);
            if (empty($objModel) || is_null($objModel)){
                $this->mMessage = "Record not found.";
                return $this->sendResponseDefault();
            }

            $input[''] = '';
            $input['full_name'] = 'fullName';
            $input['martial_status'] = 'martialStatus';
            $input['ssn'] = 'ssn';
            $input['employment'] = 'employment';
            $input['employer'] = 'employer';
            $input['gender'] = 'gender';
            $input['reference'] = 'reference';
            $input['medical_record'] = 'medicalRecord';
            $input['addr_street1'] = 'addrStreet1';
            $input['addr_street2'] = 'addrStreet2';
            $input['addr_city'] = 'addrCity';
            $input['addr_state'] = 'addrState';
            $input['addr_country'] = 'addrCountry';
            $input['addr_zip'] = 'addrZip';
            $input['phone_home'] = 'phoneHome';
            $input['phone_work'] = 'phoneWork';
            $input['phone_mobile'] = 'phoneMobile';
            $input['email'] = 'email';
            $input['primary_care_phy'] = 'primaryCarePhy';
            $input['default_render_phy'] = 'defaultRenderPhy';
            $input['referring_phy'] = 'referringPhy';
            $input['default_service_loc'] = 'defaultServiceLoc';

            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0){
                    continue;
                }
                $objModel->$tableField = $request->get($formField);
            }
            if ($objModel->save()) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }
 
     /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getClaimsList(Request $request){
        try{
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
        
            $sql = "SELECT P.chart_num, CL.claim_number from tb_claims CL inner join tb_cases CA on CA.id = CL.case_id inner join tb_patients P on P.id = CA.patient_id where CL.is_sent = '0' AND P.practice_id='$practiceID' AND CL.claim_status='Ready' GROUP by P.chart_num";

            //return $sql;

            $rows = DB::select($sql);
            if (count($rows) >0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function list1(Request $request){
        $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
        $term = $request->get('term');
        $sql = "select full_name as `key`, full_name as label from tb_patients where practice_id=$practiceID ";
        if (strlen($term) > 0) {
            $sql .= " and (full_name like '%" . $term . "%') ";
        }
        //return $sql;
        $rows = DB::select($sql);
         if (count($rows) > 0){
             $this->mData = json_encode($rows);
             $this->mMessage ='Success';
             $this->mStatus = 1;
         } 
        //return $this->sendResponseDefault('ARRAY');
        return json_encode($rows);
    }

    public function list2(Request $request) {
        try{
            //check here patientID or id
            $pid = (int)$request->get('id');
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            $pid = $practiceID;

            if ($pid <= 0) { 
                $this->mMessage = 'Invalid Practice ID.'; 
                return $this->sendResponseDefault();
            }
            $sql = "select P.*, PH1.name as primarycarephysician, PH2.name as referringphysician, PH3.name as defaultphysician from tb_patients P left join tb_physicians PH1 on PH1.id=primary_care_phy left join tb_physicians PH2 on PH2.id=P.referring_phy left join tb_physicians PH3 on PH3.id=default_render_phy where P.practice_id = '$pid' and P.is_active = 1";

            //return $sql;
            $rows = DB::select($sql);
            if (count($rows) >0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
   }

    public function list3(Request $request) {
        try{
            $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
            $sql = "select P.*, PH1.name as primarycarephysician, PH2.name as referringphysician, PH3.name as defaultphysician from tb_patients P 
                left join tb_physicians PH1 on PH1.id=primary_care_phy left join tb_physicians PH2 on PH2.id=P.referring_phy left join tb_physicians PH3 on PH3.id=default_render_phy where P.is_active = 1 and P.practice_id='$practiceID'";
            $rows = DB::select($sql);
            if (count($rows) >0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    public function list5(Request $request) {
        $practiceID = (int) Session::get('LOGGED_USER_PRATICE_ID');
        $term = $request->get('term');
        $sql = "select concat(id, ' - ' ,full_name) as `key`, concat(id, ' - ' ,full_name) as label from tb_patients where practice_id=$practiceID ";
        if (strlen($term) > 0) {
            $sql .= " and (full_name like '%" . $term . "%') ";
        }
        //return $sql;
        $rows = DB::select($sql);
         if (count($rows) > 0){
             $this->mData = json_encode($rows);
             $this->mMessage ='Success';
             $this->mStatus = 1;
         } 
        //return $this->sendResponseDefault('ARRAY');
        return json_encode($rows);
    }

    public function listBypatientID(Request $request) {
        try{
            $id = (int)$request->get('patientID');

            if ($id <= 0) { 
                $this->mMessage = 'Invalid Patient ID'; 
                return $this->sendResponseDefault();
            }
            $sql = "select P.*, PH1.name as primarycarephysician, PH2.name as referringphysician, PH3.name as defaultphysician from tb_patients P 
                left join tb_physicians PH1 on PH1.id=primary_care_phy left join tb_physicians PH2 on PH2.id=P.referring_phy left join tb_physicians PH3 on PH3.id=default_render_phy where P.id = '$id' and P.is_active = 1";
            $rows = DB::select($sql);
            if (count($rows) >0) {
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows ;
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
   }

    public function create(Request $request){
        //return $this->sendResponseDefault();
        try{
            $input[''] = '';
            $input['full_name'] = 'fullName';
            $input['martial_status'] = 'martialStatus';
            $input['ssn'] = 'ssn';
            $input['dob'] = 'dob';
            $input['employment'] = 'employment';
            $input['employer'] = 'employer';
            $input['gender'] = 'gender';
            $input['reference'] = 'reference';
            $input['medical_record'] = 'medicalRecord';
            $input['addr_street1'] = 'addrStreet1';
            $input['addr_street2'] = 'addrStreet2';
            $input['addr_city'] = 'addrCity';
            $input['addr_state'] = 'addrState';
            $input['addr_country'] = 'addrCountry';
            $input['addr_zip'] = 'addrZip';
            $input['phone_home'] = 'phoneHome';
            $input['phone_work'] = 'phoneWork';
            $input['phone_mobile'] = 'phoneMobile';
            $input['email'] = 'email';
            $input['practice_id'] = 'practiceID';
            $input['primary_care_phy'] = 'primaryCarePhy';
            $input['default_render_phy'] = 'defaultRenderPhy';
            $input['referring_phy'] = 'referringPhy';
            $input['default_service_loc'] = 'defaultServiceLoc';

            foreach ($input as $tableField => $formField) {
                if (strlen($tableField) == 0 || strlen($formField) == 0){
                    continue;
                }
                $$tableField = $request->get($formField);
            }

            $count = PatientsModel::where('full_name', '=', $full_name)
                            ->where('email', '=', $email)
                            ->where('addr_zip', '=', $addr_zip)
                            ->where('dob', '=', $dob)
                            ->select('id')
                            ->get()->count();

            if($count < 1 ) {
                $input['is_active'] = 'is_active';
                $is_active = 1;
                $patient = new PatientsModel();
                foreach ($input as $tableField => $formField) {
                    if (strlen($tableField) == 0 || strlen($formField) == 0){
                        continue;
                    }
                    $patient->$tableField = $$tableField;
                }
                $patient->practice_id = Session::get('LOGGED_USER_PRATICE_ID');
                $patient->save();
                if ($patient->id < 1) {
                    $this->mMessage = 'Create patient failed';
                } else {
                    $this->mMessage = 'Success`';
                    $this->mStatus = 1;
                    $this->mData = ['patientID' => $patient->id, 'id' => $patient->id ];
                }
            }
         } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    } 

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function updateChartNum(Request $request){
        try{
            $patientID = (int) $request->get('patientID');
            if ($patientID < 1) {
                $this->mMessage = 'Invalid patient id';
                return $this->sendResponseDefault();
            }
            $patient = PatientsModel::find($patientID);
            //$chartNo = DetailsModel::find("1");
            if (empty($patient)){
                $this->mMessage = 'Invalid patient id';
                return $this->sendResponseDefault();
            }

            if (strlen($patient->chart_num) > 0){
                $this->mMessage = 'Chart Number already set';
                return $this->sendResponseDefault();
            }

            $rows = DetailsModel::where(['detail'=>'ChartNumber'])
                    ->select('*')
                    ->get();
            if ($rows->count() > 0){
                $row = $rows[0];
                $detailvalue1 = $row->detailvalue1 + 1;
                DetailsModel::where('id', $row->id)
                    ->update(['detailvalue1'=>$detailvalue1]);

                $prefix = $request->get('prefix');
                $id1 = $detailvalue1;
                $idstring = sprintf('%05d', $id1);
                $patient->chart_num = $prefix . $idstring;
                $patient->save();

                    //Common::WriteLog('Patients','ChartNumber Created. Patient ID:' . $pid . ' Chart Number:' . Input::get('prefix') . $idstring, 0,'Chart',$pid);
                $this->mStatus = 1;
                $this->mMessage = 'ChartNumber updated success';
                $this->mData = ['ChartNumber' => $patient->chart_num];
            } else {
                $this->mMessage = 'ChartNumber update failed';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();

    }
    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList4(Request $request) {
        try{
            $sql = "select id as `key`, concat(id, ' - ', full_name) as label  from tb_patients";

            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getPatName(Request $request) {
        try{
            $patientID = $request->get('patientID');
            $sql = "select id as patientID, full_name as fullName  from tb_patients where id='$patientID'";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList5(Request $request) {
        try{
            $sql = "select concat(id, ' - ', full_name) as label from tb_patients P";
            $rows = DB::select($sql);
            if (count($rows) > 0){
                $this->mStatus = 1;
                $this->mMessage = 'Success';
                $this->mData = $rows;
            } else {
                $this->mMessage = 'Records not found';
            }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

       /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getStatementAsPDF(Request $request) {
        $fileNameOnly = 'a.pdf';
        $temp1 = '';
        try{
            $patientID = (int) $request->get('patientID');
            $filterFrom = $request->get('filterFrom');
            $filterTo = $request->get('filterTo');
            $i = 1;

            if ($patientID < 1){
                $this->mMessage = 'Invalid patient ID';
                return $this->sendResponseDefault();
            }
            if (empty($filterFrom) or empty($filterTo)){
                $this->mMessage = 'Invalid filter date';
                return $this->sendResponseDefault();
            }

            $temp = '';
            $fullName = 'fullName';
            $sql = "SELECT A.name as activityName, SL.billing_name as billingName, SL.address, P.full_name as fullName, P.id as patientID, P.addr_street1 as addrStreet1, P.addr_city as addrCity, P.addr_state as addrState, P.addr_zip as addrZip, P.chart_num as chartNo, CL.from_date as fromDt, SUM(CL.copay) as copay, SUM(CL.coins) as coins, SUM(CL.deductible) as deductible, CL.total, CL.claim_balance as claimBalance, CL.paid, CL.adjustment  FROM tb_claims CL inner join tb_cases CA on CA.id = CL.case_id inner join tb_patients P on P.id = CA.patient_id inner join tb_appointments AP on AP.id = CL.app_id inner join tb_activity A on A.id = AP.activity_id inner join tb_service_locations SL on SL.id = P.default_service_loc where P.id = '$patientID' AND CL.insurance_id = '-100' AND (CL.from_date between '$filterFrom' AND '$filterTo') GROUP by CL.claim_number";

                $rows = DB::select($sql);

              $totCopay = 0;
              $totCoins = 0;
              $totDeduc = 0;
              $totalFin = 0;
            if(count($rows) > 0){
                foreach ($rows as $row){
                    $row1 = (array) $row;
                    $fromDt = $row1['fromDt'];
                    $fullName = $row1['fullName'];
                    $total = $row1['total'];
                    $activityName = $row1['activityName'];
                    $copay = $row1['copay'];
                    $totCopay = $totCopay + $copay;
                    $coins = $row1['coins'];
                    $totCoins = $totCoins + $coins;
                    $deduc = $row1['deductible'];
                    $totDeduc = $totDeduc + $deduc;

                 $temp1 .='<tr>
                  <td style="text-align:center;border:1px solid #000">'.$fromDt.'</td>
                  <td style="text-align:center;border:1px solid #000">'.$activityName.'</td>
                  <td style="text-align:center;border:1px solid #000">'.$copay.'</td>
                  <td style="text-align:center;border:1px solid #000">'.$coins.'</td>
                  <td style="text-align:center;border:1px solid #000">'.$deduc.'</td>
                    </tr>';
                }
        }

        $sql1 = $sql . " LIMIT 1";
        $rows1 = DB::select($sql1);
        if(count($rows1) > 0){
            foreach ($rows1 as $row1) {
                $row = (array) $row1;
              $billingName = $row['billingName'];
              $patientName = $row['fullName'];
              $address = explode(',', $row['address']);
              $billingAddr = $address[0]; // piece1
              $billingTotAddr = $address[1];
              $addrStreet1 = $row['addrStreet1'];
              $patientID = $row['patientID'];
              $totAddr = $row['addrCity'].' '.$row['addrState'].' '.$row['addrZip'];
              $chartNo = $row['chartNo'];

              $date=strtotime(date('Y-m-d'));  // if today :2013-05-23
              $today1 = date('m-d-Y');
              $dueDate = date('m-d-Y',strtotime('+15 days',$date));

              $totalFin = $totCopay + $totCoins + $totDeduc;

              $image_file = public_path('/') . 'FormingFriendships.png';

              $temp .='<style>.td{vertical-align:middle}</style>
            <table style="table-layout: fixed; width: 898px;">

              <tr>
                <th style="font-size:12px;">'.$billingName.' </th>
                <th ></th>
                <th colspan="1" rowspan="4"><img src="' . $image_file . '"/></th>
                <th ></th>
              </tr>
              <tr>
                <td style="font-size:12px;">'.$billingAddr.'</td>
                <td ></td>
                <td ></td>
              </tr>
              <tr>
                <td style="font-size:12px;">'.$billingTotAddr.'</td>
                <td ></td>
                <td ></td>
              </tr>
              <tr>
                <td style="font-size:12px;">Phone:</td>
                <td ></td>
                <td ></td>
              </tr>
              </table>
              <table style="table-layout: fixed; width: 898px">
              <tr>
                <td ></td>
                <td ></td>
                <td></td>
                <td colspan="1" rowspan="5"></td>
              </tr>
              <tr>
                <td ></td>
                <td ></td>
                <td >Invoice # : '.$chartNo.'_'.$fromDt.'</td>
              </tr>
              <tr>
                <td ></td>
                <td ></td>
                <td >Client Account # : '.$chartNo.'</td>
              </tr>
              <tr>
                <td style="font-size:12px;" >Bill To</td>
                <td ></td>
                <td >Date : '.$today1.'</td>
              </tr>
              <tr>
                <td style="font-size:12px;" >'.$patientName.'</td>
                <td ></td>
                <td >Due Date : '.$dueDate.'</td>
              </tr>
              <tr>
                <td style="font-size:12px;" >'.$totAddr.'</td>
                <td ></td>
                <td >Last Payment : $ '.$totalFin.'</td>
              </tr>
              <tr>
                <td ></td>
                <td ></td>
                <td ></td>
              </tr>
              <tr>
                <td ></td>
                <td ></td>
                <td ></td>
              </tr>
              </table>
            <table>
              <tr>
                <th rowspan="2" style="text-align:center;border:1px solid #000; background-color:#ccc; color:#000; width:100px">Service Date</th>
                <th rowspan="2" style="text-align:center;border:1px solid #000; background-color:#ccc; color:#000; width:250px">Description</th>
                <th colspan="3" style="text-align:center;border:1px solid #000;vertical-align:top; background-color:#ccc; color:#000; width:300px">Balance</th>
              </tr>
              <tr>
                <td style="text-align:center;border:1px solid #000; background-color:#ccc; color:#000; width:100px">Copay</td>
                <td style="text-align:center;border:1px solid #000; background-color:#ccc; color:#000; width:100px">Coins</td>
                <td style="text-align:center;border:1px solid #000; background-color:#ccc; color:#000; width:100px">Deductible</td>
              </tr>';

            }
        }

        $temp = $temp.$temp1;
          $temp .='<tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
        <table style="table-layout: fixed; width: 700px">
          <tr>
            <td>PREVIOUS BALANCE</td>
            <td ></td>
            <td ></td>
            <td ></td>
          </tr>
          <tr>
            <td>TOTAL BALANCE DUE FROM</td>
            <td>From '.$filterFrom.' to '.$filterTo.'</td>
            <td ></td>
            <td>$ '.$totalFin.'</td>
          </tr>
          <tr>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          </table>
          <table style="table-layout: fixed; width: 700px">
          <tr>
            <td>Payment Coupon </td>
            <td colspan="2" rowspan="1" style="text-align:center">Credit Card Authorization</td>
            <td></td>
          </tr>
          <tr>
            <td><br>Please return this coupon with your payment.</td>
            <td colspan="3" rowspan="1" style="text-align:center; margin-top:5px;">|____|____|____|____|____|____|____|____|____|____|____|____|____|____|____|____|</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Make your checks payable to :</td>
            <td>Type of card :</td>
            <td></td>
            <td>CVV :</td>
          </tr>
          <tr>
            <td>Progressive Behavioural Science </td>
            <td>Exp. Date </td>
            <td></td>
            <td>Amount : $</td>
          </tr>
          <tr>
            <td>Invoice # : '.$chartNo.'_'.$fromDt.'</td>
            <td>Account Holder Name :</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7/13/2018</td>
            <td>Bill Address : </td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
          </tr>
          <tr>
            <td ></td>
            <td >Signature :</td>
            <td >Date : '.$today1.'</td>
            <td ></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>';
    
          $tbl = $temp;

            //PDF::SetCreator(PDF_CREATOR);
            PDF::SetAuthor('Curismed');
            PDF::SetTitle('INVOICE # :'.$fullName);
            PDF::SetSubject('Curismed INVOICE # :'.$fullName);
            PDF::SetKeywords('TCPDF, PDF, example, test, guide');

            PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
            PDF::SetFooterMargin(PDF_MARGIN_FOOTER);

            PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

            PDF::AddPage('P', 'A4');
            PDF::SetFont('helvetica', '', 8);
            $CurrPage = PDF::getAliasNumPage();
            PDF::writeHTML($tbl, true, false, false, false, '');

            $folder = $this->getFolderStatements();
            $fileNameOnly = $chartNo.'_'.$fromDt.'.pdf';
            //PDF::Output($fileNameOnly,'FI');

            $statementName = $chartNo.'_'.$fromDt . '.pdf';
            $location = $this->getFolderStatementsUrl() . $chartNo.'_'.$fromDt;
            $location = $this->getFolderStatementsUrl();

            $statement = new StatementsModel();
            $statement->name = $statementName;
            $statement->patient_id = $patientID;
            $statement->location = $location;
            $statement->created_on = now();
            $statement->save();

            $fileNameOnly = $folder . '/'.$chartNo.'_'.explode(' ',$fromDt)[0].'.pdf';
            //$fileNameOnly = "c:\a" . $chartNo.'_'.$fromDt.'.pdf';
            PDF::Output($fileNameOnly,'FI');
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        } catch (QueryException $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }


}