<?php
/*********************************************************
* Product       : CURISMED
* Module        : UserController
* Description   :
*
* Created  Date : 2018-08-01
* Author        : Rishwanth
* 
* History       : 2018-04-04 Login, Logout Implemented
**********************************************************/
namespace App\Http\Controllers;
use PDF;
use Validator;
use Session;
use Log;
use DB;
use Carbon;
use QueryException;
use Exception;

use Illuminate\Http\Request;
//use App\Http\Requests\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\PassportController;
use App\User;
use App\Users;
use Illuminate\Support\Facades\Auth;
use App\Model\UsersSessionModel;
use App\Model\PatientsModel;

class UserController extends Controller {
    /**
     *
     * 
     */
    public function __construct() {

    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function login(Request $request){
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin', '');
        DB::beginTransaction();
        try{
            $username = $request->get('username');
            $password = ($request->get('password'));
            $email = '';
            $users = DB::table('tb_users')
                    ->join('tb_practices','tb_practices.id','=','tb_users.practice_id') 
                    ->where('username', '=', $username)
                    ->where('password', '=', sha1($password))
                    ->select('*')
                    ->get();

            if (false) {

                if ($users->count() < 1) {
                    $this->mMessage = 'Invalid credential.';
                    $this->addLogs(__FILE__, __FUNCTION__, 'End', $message);
                    DB::rollback();
                    return $this->sendResponseDefault();
                }
                $users = $users[0];
                $email = $users->email_id;

                $passport = new PassportController();
                $tokenResponse = $passport->login($email, $password);
                //return $tokenResponse;
                $response = $tokenResponse;
                if ($response['status'] == 0) {
                    $this->mMessage = 'Autherize login and token generation failed.';
                    $this->addLogs(__FILE__, __FUNCTION__, 'End', $message);
                    DB::rollback();
                    return $this->sendResponseDefault();
                }
                if ($users->is_activated < 1){
                    $this->addLogs(__FILE__, __FUNCTION__, 'End', $message);
                    $this->mMessage = 'Login success. Activation not completed yet.';
                    DB::rollback();
                    return $this->sendResponseDefault();
                }
                $session = $this->checkSession($users->id);
                if (empty($session['session_token']) ){
                    $this->mMessage = 'Already login by other system for past duration ' . $session['duration'] . $session['session_token'] . ' minutes. Please wait for session logout.';
                    $this->addLogs(__FILE__, __FUNCTION__, 'End', $this->mMessage);
                    DB::rollback();
                    return $this->sendResponseDefault();
                }
                $this->mStatus = 1;
                $data['oauth_token'] = $response['message'];
                $data['session_token'] = $session['session_token'];
                $this->mMessage = 'Success ';
                Session::put('LOGGED_USER_PRATICE_NAME', $users->name);
            Session::put('LOGGED_USER_PRATICE_ID', $users->practice_id);

            } else {
                $sql = "select * from tb_users where username='peter' ";
                $users = DB::select($sql);
                $users = $users[0];
                $data['oauth_token'] = 'oauth_token';
                $data['session_token'] = 'session_token';
                $this->mStatus = 1;
                $this->mMessage = 'Success ';
                Session::put('LOGGED_USER_PRATICE_NAME', '$users->name');
            Session::put('LOGGED_USER_PRATICE_ID', 1);
            }
           // print_r($users);
            $data['base_url'] = url('/');        
            $this->mData = $data;
            Session::put('LOGGED_USER_ID', $users->id);
            Session::put('LOGGED_USER_NAME', $users->first_name);
            $this->addLogs(__FILE__, __FUNCTION__, 'End', $this->mMessage);
            DB::commit();
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function logout(Request $request){
        try{
            $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
            $session_token = $this->getSessionToken();
            UsersSessionModel::where('session_token','=', $session_token)->update(['is_active' => 0]);

            $session_token = $request->get('session_token');
            UsersSessionModel::where('session_token','=', $session_token)->update(['is_active' => 0]);

            Auth::logout();
            Session::flush();
            $this->addLogs(__FILE__, __FUNCTION__, 'End');
            return redirect('/');
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function activateAccount(Request $request){
        try{
            $token = $request->get('token');
            if (empty($token) || strlen($token) < 2){
                $this->mMessage = 'Invalid token';
                return $this->sendResponseDefault();
            }
            $rows = Users::where('token','=', $token)
                    ->select('*')
                    ->get();
            if ($rows->count() < 1){
                $this->mMessage = 'Invalid token';
                return $this->sendResponseDefault();
            }
            $row = $rows[0];
            if ($row->is_activated > 0){
                $this->mMessage = 'Account already activate';
                return $this->sendResponseDefault();
            }
            $data['token'] = $row->token;
            return view('users.password_get_after_activation', compact('data'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function savePassword(Request $request){
        DB::beginTransaction();
        try{
            $token = $request->get('token');
            $user = Users::where('token', '=', $token)
                        ->select('*')
                        ->get();

            $password1 = trim($request->get('password1'));
            $password2 = trim($request->get('password2'));
            $data = array();
            if ($user->count() < 1) {
                $data['message'] = 'Activation token expired.';
            } else if (strlen($password1) == 0) {
                $data['message'] = 'Passoword is empty';
            } else if (strlen($password2) == 0) {
                $data['message'] = 'Comfirm password is empty';
            } else if ($password1 != $password2) {
                $data['message'] = 'Passoword does not matcch';
            }
            if (!empty($data)){
                DB::rollback();
                $data['token'] = $request->get('password2');
                return view('users.password_get_after_activation', compact('data'));
            }
            $user = $user[0];
            $userId = $user->id;
            $email = $user->email_id;
            $name = $user->username;
            $passport = new PassportController();
            $response = $passport->register($name, $email, $password1);
            if ($response['status'] == 0) {
                DB::rollback();
                $data['token'] = $token;
                $data['message'] = 'Save password failed. Autherization token generation failed';
                return view('users.password_get_after_activation', compact('data'));
            }
            $userObj = Users::find($userId);
            $userObj->password = sha1($password1);
            $userObj->token = '';
            $userObj->reset_token = '';
            $userObj->oauth_token = $response['message'];
            $userObj->is_activated = 1;
            $userObj->save();

            $data['message'] = 'Password set successfully. Please check your mail and login to continue';
            $objMail = new MailController();
            $objMail->sendPasswordSuccess($user->id);
            DB::commit();
            return view('welcome', compact('data'));
        } catch (Exception $e) {
            DB::rollback();
            $this->mMessage = $e->getMessage();
        }
        DB::commit();
        return $this->sendResponseDefault();
    }


    public function forgotpasswordShowScreen(Request $request){
        return view('users.forgotpassword');
    }

    public function forgotpassword(Request $request){
        try{
            $email_id = $request->get('email_id');
            if (empty($email_id)){
                $this->mMessage = 'Invalid email id';   
                return $this->sendResponseDefault();
            } else {
                 $rows = Users::where('email_id','=', $email_id)
                        ->select('*')
                        ->get();
                if ($rows->count() < 1){
                    $this->mMessage = 'Email id not registed yet.';   
                    return $this->sendResponseDefault();
                } else {
                    $id = $rows[0]->id;
                    $reset_token = sha1(now());
                    Users::find($id)->update(['reset_token' => $reset_token]);
                    $this->mMessage = $reset_token;
                    $objMail = new MailController();
                    $flag = $objMail->sendResetPasswordLink($id);
                    if ($flag){
                    } else {
                        $this->mMessage ='Mail sent failed => ' . $flag;
                    }
                    $this->mStatus  = 1;
                    $this->mMessage ='Reset password link has been sent. Please check your mail id.';
                }
           }
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    public function resetpasswordShowScreen(Request $request){
        try{
            $reset_token = $request->get('reset_token');
            $users = Users::where('reset_token','=', $reset_token)
                        ->select('*')
                        ->get();
            if ($users->count() < 1){
                $this->mMessage = 'Invalid reset token or token expired.';   
                return $this->sendResponse($this->mStatus, $this->mMessage, $this->mData);
            }
            $users = $users[0];
            return view('users.resetpassword', compact('users'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    public function resetpassword(Request $request){
        DB::beginTransaction();
        try{
            $password1 = $request->get('password1');
            $password2 = $request->get('password2');
            $reset_token = $request->get('reset_token');
            if (empty($password1) || strlen($password1) < 1){
                $this->mMessage = 'Invalid password';   
                return $this->sendResponseDefault();
            }
            if ($password1 != $password2){
                $this->mMessage = 'Password does not match';   
                return $this->sendResponseDefault();
            }
            $users = Users::where('reset_token','=', $reset_token)
                        ->select('*')
                        ->get();
            if ($users->count() < 1){
                $this->mMessage = 'Invalid reset token';   
            }
            $rows = User::where('email', '=', $users[0]->email_id)
                    ->select('*')
                    ->get();
            if ($rows->count() < 1){
                $this->mMessage = 'Users not found in oauth Autherization';   
                return $this->sendResponseDefault();
            }
            $id = $users[0]->id;
            Users::find($id)->update(['password' => sha1($password1)]);
            $id = $rows[0]->id;
            User::find($id)->update(['password' => bcrypt($password1)]);
            DB::commit();

            $objMail = new MailController();
            $flag = $objMail->sendResetPasswordSuccess($users[0]->id);

            $this->mStatus = 1;
            $this->mMessage = 'success';
        } catch (Exception $e) {
            DB::rollback();
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */
     public function checkSession($userId){
        try{
            $sql = "select * from tb_users_session where user_id='$userId'
            and is_active=1 order by id desc limit 1";
            $rows = DB::select($sql);
            if (false && count($rows) > 0){
                $row = $rows[0];
                $lastAccessedTime = Carbon::parse($row->date_last_accessed);
                $now = Carbon::parse(now());                
                $totalDuration = $now->diffInSeconds($lastAccessedTime);
                $min= round($totalDuration / 60,0);
                if ($min < 5){
                    return ['duration'=>$min, 'session_token' => ''];
                } else {
                    $session = UsersSessionModel::find($row->id);
                    $session->is_active = 0;
                    $session->save();
                }
            } 
            $session_token = $this->addSession($userId);
            return ['duration'=>0, 'session_token' => $session_token];
        } catch (Exception $e) {
            return ['duration'=>0, 'session_token' => ''];
        }
        return ['duration'=>0, 'session_token' => ''];
    }

    /**
     *  Purpose     : 
     *  Input       :
     *  Output      :
     *  History     : 
     */
     public function addSession($userId) {
        $token = $this->getUUID();
        $input = array();
        $input['user_id'] = $userId;
        $input['date_login'] = now();
        $input['date_last_accessed'] = now();
        $input['is_active'] =1;
        $input['session_token'] = $token;
        $session = UsersSessionModel::create($input);
        $session->save();
        Session::put('SESSION_TOKEN', $token);
        return $token;
     }


}