<?php
/**
 * Created by PhpStorm.
 * User: admir
 * Date: 8/3/2018
 * Time: 4:22 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SchedulerController extends Controller{

    /**
     * 	Purpose 	:
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */
    public function __construct() {

    }
	
    /**
     * 	Purpose 	:
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */
    public function scheduler()
    {
        return view('scheduler');
    }

    public function scheduler_calendar()
    {
        return view('scheduler_calendar');
    }

}