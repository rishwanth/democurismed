<?php
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Routing\UrlGenerator;
use App\Mail\DemoEmail;
use App\Mail\UserActivationEmail;
use App\Mail\UserResetpasswordEmail;
use App\Mail\UserPasswordSetSuccessEmail;
use App\Mail\UserPasswordResetSuccessEmail;
use App\Users;
 
class MailController extends Controller {
    public $mUserId;
    /**
     *
     * 
     */
    public function __construct($mUserId = 0) {
        parent::__construct();
        //$this->mUserId == $mUserId;
    }
	
    public function send($mailId = "noreply@curismed.com") {
    	$this->sendActionLink(8);
    	return ;
        $objDemo = new \stdClass();
        $objDemo->demo_one = 'Mail activation';
        $objDemo->demo_two = 'Password Reser';
        $objDemo->sender = 'Sender';
        $objDemo->receiver = 'Received';
        Mail::to($mailId)->send(new DemoEmail($objDemo));
    }

    public function sendActionLink($userId) {
        $rows = Users::where('id','=', $userId)
            ->select('*')
            ->get();

        if ($rows->count() < 1 ){
            return 0;
        }
        $row = $rows[0];
   	    $mailId = $row->email_id;
        $objDemo = new \stdClass();
        $objDemo->first_name = $row->first_name;
        $objDemo->last_name = $row->last_name;
        $objDemo->sender = 'Curismed Customer Support';
        $objDemo->receiver = $row->first_name;;
        $objDemo->link = url('/') . '/user/activation?token=' . $row->token;
        Mail::to($mailId)
        	->send(new UserActivationEmail($objDemo))
        	;
    }

    public function sendResetPasswordLink($userId) {
        $rows = Users::where('id','=', $userId)
            ->select('*')
            ->get();

        if ($rows->count() < 1 ){
            return 0;
        }
        $row = $rows[0];
        $mailId = $row->email_id;
        $objDemo = new \stdClass();
        $objDemo->first_name = $row->first_name;
        $objDemo->last_name = $row->last_name;
        $objDemo->sender = 'Curismed Customer Support';
        $objDemo->receiver = $row->first_name;;
        $objDemo->link = url('/') . '/user/resetpassword?reset_token=' . $row->reset_token;
        return Mail::to($mailId)
            ->send(new UserResetpasswordEmail($objDemo))
            ;
    }

    public function sendPasswordSuccess($userId) {
        $rows = Users::where('id','=', $userId)
            ->select('*')
            ->get();

        if ($rows->count() < 1 ){
            return 0;
        }
        $row = $rows[0];
        $mailId = $row->email_id;
        $objDemo = new \stdClass();
        $objDemo->first_name = $row->first_name;
        $objDemo->last_name = $row->last_name;
        $objDemo->sender = 'Curismed Customer Support';
        $objDemo->receiver = $row->first_name;
        $objDemo->link = url('/');
        return Mail::to($mailId)
            ->send(new UserPasswordSetSuccessEmail($objDemo))
            ;
    }

    public function sendResetPasswordSuccess($userId) {
        $rows = Users::where('id','=', $userId)
            ->select('*')
            ->get();

        if ($rows->count() < 1 ){
            return 0;
        }
        $row = $rows[0];
        $mailId = $row->email_id;
        $objDemo = new \stdClass();
        $objDemo->first_name = $row->first_name;
        $objDemo->last_name = $row->last_name;
        $objDemo->sender = 'Curismed Customer Support';
        $objDemo->receiver = $row->first_name;
        $objDemo->link = url('/');
        return Mail::to($mailId)
            ->send(new UserPasswordResetSuccessEmail($objDemo))
            ;
    }

}