<?php

namespace App\Http\Controllers;
use Session;
use DB;
use Exception;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Model\PortalLogsModel;


class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $RESPONSE_SUCCESS_CODE = 200;
    public $mUserId = 0;
    public $mStatus = 0;
    public $mMessage = 'Record not found';
    public $mData = [];
    public $mSession = [];
    public $mTables = [];
 
    public function __construct() {
        $this->mUserId = $this->getUserId();
        $this->middleware(function ($request, $next) {
            $this->mSession = $this->getSessionValues();
                return $next($request);
        });

        $this->initialize();
        if (gethostname() == 'admir'){
            file_put_contents(storage_path('logs/laravel.log'),'');
        }
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function initialize(){
        $arr['ACTIVITY'] = 'tb_activity';
        $arr['APPOINTMENT'] = 'tb_appointments';
        $arr['EMPLOYEE'] = 'tb_physicians';
        $arr['EMPLOYEE_PAYCODE'] = 'tb_physicians_paycodes';
        $arr['EMPLOYEE_ACTIVITY'] = 'tb_physicians_paycodes';
        $arr['EMPLOYEE_WORKHISTORY'] = 'tb_physicians_workhistory';
        $arr['PAYCODES'] = 'tb_paycodes';
        $arr['PATIENT'] = 'tb_patients';
        $arr['CASE'] = 'tb_cases';
        $arr['CLAIM'] = 'tb_claims';
        $arr['PLACE_OF_SERVICE'] = 'tb_place_of_services';
        $arr['SERVICE_LOCATION'] = 'tb_service_locations';
        $arr['PAYROLL'] = 'tb_payroll';
        $arr['AUTH'] = 'tb_auth';
        $arr['PRACTICE'] = 'tb_practices';
        $arr['INSURANCE'] = 'tb_insurances';
        $arr['DEPOSIT'] = 'tb_deposits';
        $arr['DEPOSIT_DETAILS'] = 'tb_deposit_details';
        $arr['SETUP'] = 'tb_insurance_setup';

        $arr['ACL_LIST'] = 'tb_acl_list';
        $arr['ACL_LIST_USER'] = 'tb_user_acl_list';
        $arr['ACL_LIST_ROLE'] = 'tb_role_user_acl_list';

        $arr['PRACTICES_INSURANCE'] = 'tb_practices_insurance';
        $arr['HANDLER_INSURANCES'] = 'tb_handler_insurances';
        $arr['INSURANCE_SETUP'] = 'tb_insurance_setup';
        $arr['USERS'] = 'tb_users';
        $arr['ROLE'] = 'tb_role';

        $this->mTables = $arr;
    }
  
  	public function sendResponse($status, $message, $data, $type = 'JSON_STRING'){
  		$respone['status'] = $status;
  		$respone['message'] = $message;
  		$respone['data'] = $data;

  		if ($type == 'JSON_OBJECT') {
            return $respone;
        } else if ($type == 'ARRAY') {
            return $respone;
  		} else {
  			return json_encode($respone);
  		}
  	}

    public function sendResponseDefault($type = 'JSON_STRING'){
      $respone['status'] = $this->mStatus;
      $respone['message'] = $this->mMessage;
      $respone['data'] = $this->mData;

      if ($type == 'JSON_OBJECT') {
            return $respone;
      } else if ($type == 'ARRAY') {
            return $respone;
      } else {
        return json_encode($respone);
      }
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function getUserID(){
        $user_id = 0;
        if(Session::has('LOGGED_USER_ID') && Session::has('ACCESS_TOKEN')) {
            $user_id = Session::get('LOGGED_USER_ID');
        }
        return $user_id;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function isValidUser()
    {
        return 0;
        $user_id = 0;
        if(Session::has('LOGGED_USER_ID') && Session::has('ACCESS_TOKEN')) {
            $user_id = Session::get('LOGGED_USER_ID');
            $access_token = Session::get('ACCESS_TOKEN');
            if (strlen($access_token) < 10 || $user_id < 1) {
                //echo ('Autherization failed. Ask support team to provide Autherization token');
                $user_id = 0;
            }
        } else {
            //echo ('Autherization failed. Ask support team to provide Autherization token');
            $user_id = 0;
        }
        return $user_id;
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getUUID(){
        $rows = DB::select('select uuid() as uid');
        return $rows[0]->uid;
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getSessionToken(){
      if (Session::has('SESSION_TOKEN')){
        return Session::get('SESSION_TOKEN');  
      }
      return 'session_token_not_created_yet';
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getDBError(Exception $e){
        $error = $e->getMessage();
        $pos = strpos($error, '(SQL:');
        if ($pos > 0) {
            return substr($error, 0, $pos-1)  . ' => ' .  get_class($e);
        } 
        return $error . ' => ' . get_class($e) ;
    }


    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function addLogs($file, $function, $action, $comments= ''){
        try{
            $session_token = $this->getSessionToken();
            $logs = new PortalLogsModel();
            $logs->session_token = $this->getSessionToken();
            $logs->date = now();
            $logs->action = $action;
            $logs->comments = $comments;
            $logs->file = $file;
            $logs->function = $function;
            $logs->ip =$_SERVER["REMOTE_ADDR"];
            $logs->save();
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        return $this->sendResponseDefault('ARRAY');
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getPracticeID(){
      if (Session::has('LOGGED_USER_PRATICE_ID')){
        return Session::get('LOGGED_USER_PRATICE_ID');  
      }
      return 0;
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getFolderEDI(){
        $folderName = public_path('/') . "docs/edi/";
        return $folderName;
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getFolderDocs(){
        $folderName = public_path('/') . "docs/";
        return $folderName;
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getFolderStatementsUrl(){
        $folderName = url('/') . "/public/docs/statements/";
        return $folderName;
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getFolderStatements(){
        $folderName = public_path('/') . "/docs/statements/";
        return $folderName;
    }

    /**
     *  Purpose   :
     *  Input     :
     *  Output    :
     *  History   : 
     */
    public function getFolderHcfa(){
        $folderName = public_path('/') . "/docs/hcfa/";
        return $folderName;
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */

    public function getSessionValues(){
        $arr=[];
        foreach(Session::all() as $key => $obj){
            $arr[$key] = $obj;
        }
        return $arr;
    }
}
