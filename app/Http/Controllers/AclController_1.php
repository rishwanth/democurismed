<?php
/*********************************************************
* Product       : CURISMED
* Module        : AclController
* Description   :
*
* Created  Date : 2018-08-01
* Author        : Rishwanth
* 
* History       : 2018-04-04 Login, Logout Implemented
**********************************************************/
namespace App\Http\Controllers;
use Session;
use DB;
use QueryException;
use Exception;
use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Users;
use App\UserAclModel;
use Illuminate\Support\Facades\Auth;

class AclController_1 extends Controller {
    /**
     *
     * 
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     *
     * 
     */
    public function create() {

    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function index(Request $request){
        try{
            $user_id = $this->isValidUser();
            if ($user_id < 1) {
                    $message1 = 'Autherization failed. Ask support team to provide Autherization token';
                    $message = array( 'message' => $message1);
                 return abort(404, $message1);
            }
            //TOBEREMOVED
            //$user_id = 7;
            //TOBEREMOVED

            $sql = "select acl.acl_key as main_acl_key, acl.acl_name, uacl.* from tb_acl_list acl
            left join tb_user_acl_list uacl on acl.acl_key = uacl.acl_key and uacl.user_id=$user_id";
            $rows = DB::select($sql);
            return view('user_acl', compact('rows'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request){
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
        try{
            $user_id = $this->isValidUser();
            //TOBEREMOVED
            //$user_id = 7;
            //TOBEREMOVED
            $acl_keys = $request->get('acl_key');
            $user_acl_ids = $request->get('user_acl_id');
            foreach ($acl_keys as $key => $value) {
                $id = (int) $user_acl_ids[$key];
                $is_display = 'IS_DISPLAY_' . $value;
                $is_create = 'IS_CREATE_' . $value;
                $is_edit = 'IS_EDIT_' . $value;
                $is_view = 'IS_VIEW_' . $value;
                $is_delete = 'IS_DELETE_' . $value;

                $is_display = (int) $request->get($is_display);
                $is_create = (int) $request->get($is_create);
                $is_edit = (int) $request->get($is_edit);
                $is_view = (int) $request->get($is_view);
                $is_delete = (int) $request->get($is_delete);


                $userAcl = UserAclModel::find($id);
                if(is_null($userAcl)){
                    $userAcl = new UserAclModel();
                }
                $userAcl->user_id = $user_id;
                $userAcl->acl_key = $value;
                $userAcl->is_display = $is_display;
                $userAcl->is_create = $is_create;
                $userAcl->is_edit = $is_edit;
                $userAcl->is_view = $is_view;
                $userAcl->is_delete = $is_delete;
                $userAcl->save();
            }
            return redirect('acl');
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

}