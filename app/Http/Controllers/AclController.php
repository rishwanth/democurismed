<?php
/*********************************************************
* Product       : CURISMED
* Module        : AclController
* Description   :
* Created  Date : 2018-08-01
* Author        : Rishwanth
**********************************************************/
namespace App\Http\Controllers;
use Session;
use DB;
use QueryException;
use Exception;
use Validator;
use Log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Users;
use App\Model\AclRoleUserModel;
use App\Model\AclModel;
use App\Model\AclUserModel;
use Illuminate\Support\Facades\Auth;

class AclController extends Controller {
    /**
     *
     * 
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     *
     * 
     */
    public function create() {

    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function index(Request $request){
        try{
            $user_id = (int) $request->get('user_id');
            $role_id = (int) $request->get('role_id');
            if ($role_id > 0){
                $this->indexRole($request);
                return;
            }
            if ($user_id < 1){
                $user_id = 1;
            }
            if ($user_id < 1) {
                    $message1 = 'Autherization failed. Ask support team to provide Autherization token';
                    $message = array( 'message' => $message1);
                 return abort(404, $message1);
            }
            $sql = '';
            $sql .= "select acl.acl_key as main_acl_key, acl.acl_name, uacl.* from tb_acl_list acl
            left join tb_user_acl_list uacl on acl.acl_key = uacl.acl_key and uacl.user_id=$user_id ";
            //$sql .= " where acl.acl_key='ACL_CLIENTS' ";
            //$sql .= ' order by acl.order_by ';
            //print_r($sql); die();
            $rows = DB::select($sql);
            $data['rows'] = $rows;
            $data['user_id'] = $user_id;
            $data['role_id'] = 0;
            return view('user_acl', compact('data'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function indexRole(Request $request){
        try{
            $role_id = (int) $request->get('role_id');
            if ($role_id < 1){
                $role_id = 1;
            }
            $sql = '';
            $sql .= "select acl.acl_key as main_acl_key, acl.acl_name, racl.* from tb_acl_list acl
            left join tb_role_user_acl_list racl on acl.acl_key = racl.acl_key and racl.role_id=$role_id ";
            $sql .= ' order by acl.order_by ';
/*            echo $sql;
            die();
*/            $rows = DB::select($sql);
            $data['rows'] = $rows;
            $data['user_id'] = 0;
            $data['role_id'] = $role_id;
            return view('user_acl', compact('data'));
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function save(Request $request){
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
        DB::beginTransaction();
        try{
            $user_id = $this->isValidUser();
            $role_id = (int) $request->get('role_id');
            if ($role_id > 0) {
                return $this->saveRole($request);
            }
            if ($user_id < 1){
                $user_id = 1;
            }
            //$acl_keys = $request->get('acl_key');
            //$user_acl_ids = $request->get('user_acl_id');
            $tbl = $this->mTables['ACL_LIST_USER'];
            $sql = "delete from $tbl where user_id=" . $user_id;
            DB::statement($sql);

            $user_id = $request->get('user_id');
            $ids = $request->get('ids');
            $arr = explode(",", $ids);
            $rowsAcl = AclModel::select('*')->get();
            $arrMenu = [];

            foreach ($rowsAcl as $key => $value) {
                foreach ($rowsAcl as $key1 => $value1) {
                    $menu = $value1->acl_key;
                    if (!isset($arrMenu[$menu]['is_display'])){
                        $arrMenu[$menu]['is_display'] = 0;
                    }
                    if (!isset($arrMenu[$menu]['is_create'])){
                        $arrMenu[$menu]['is_create'] = 0;
                    }
                    if (!isset($arrMenu[$menu]['is_edit'])){
                        $arrMenu[$menu]['is_edit'] = 0;
                    }
                    if (!isset($arrMenu[$menu]['is_view'])){
                        $arrMenu[$menu]['is_view'] = 0;
                    }
                    if (!isset($arrMenu[$menu]['is_delete'])){
                        $arrMenu[$menu]['is_delete'] = 0;
                    }
                }
                $menu = $value->acl_key;


                $is_display =  'IS_DISPLAY_' . $menu ;
                $is_create = 'IS_CREATE_' . $menu;
                $is_add = 'IS_ADD_' . $menu;
                $is_edit = 'IS_EDIT_' . $menu;
                $is_view = 'IS_VIEW_' . $menu;
                $is_delete = 'IS_DELETE_' . $menu;
                foreach ($arr as $key1 => $value1) {
                    $value1 = trim($value1);
                    if ($is_display  == $value1){
                        $arrMenu[$menu]['is_display'] = 1;
                    }
                    if ($is_create == $value1){
                        $arrMenu[$menu]['is_create'] = 1;
                    }
                    if ($is_edit == $value1 ){
                        $arrMenu[$menu]['is_edit'] = 1;
                    }
                    if ($is_delete == $value1){
                        $arrMenu[$menu]['is_delete'] = 1;
                    }
                    if ($is_view == $value1 ){
                        $arrMenu[$menu]['is_view'] = 1;
                    }
                }
            }
            //print_r($arrMenu ); die();
            foreach ($arrMenu as $key => $value) {
                $rows = AclUserModel::where('user_id', $user_id)->where('acl_key', $key)->select('id')->get();
                $userAcl = new AclUserModel();
                if ($rows->count() > 0){
                    $id = $rows[0]['id'];
                    $userAcl = AclUserModel::find($id);
                }
                //print_r($arrMenu[$key]); 
                $userAcl->user_id = $user_id;
                $userAcl->acl_key = $key;
                $userAcl->is_display = $arrMenu[$key]['is_display'];
                $userAcl->is_create = $arrMenu[$key]['is_create'];
                $userAcl->is_edit = $arrMenu[$key]['is_edit'];
                $userAcl->is_view = $arrMenu[$key]['is_view'];
                $userAcl->is_delete = $arrMenu[$key]['is_delete'];
                $userAcl->save();
            }
            //die();
            $this->mMessage = 'Saved Success';
            DB::commit();
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
            DB::rollback();
        }
        //$this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function saveRole(Request $request){
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
        DB::beginTransaction();
        try{
            $role_id = (int) $request->get('role_id');
            if ($role_id < 2) {
                $role_id = 4;
            }
            $tbl = $this->mTables['ACL_LIST_ROLE'];
            $sql = "delete from $tbl where role_id=" . $role_id;
            DB::statement($sql);

            $ids = $request->get('ids');
            $arr = explode(",", $ids);
            $rowsAcl = AclModel::select('*')->get();
            $arrMenu = [];

            foreach ($rowsAcl as $key => $value) {
                foreach ($rowsAcl as $key1 => $value1) {
                    $menu = $value1->acl_key;
                    if (!isset($arrMenu[$menu]['is_display'])){
                        $arrMenu[$menu]['is_display'] = 0;
                    }
                    if (!isset($arrMenu[$menu]['is_create'])){
                        $arrMenu[$menu]['is_create'] = 0;
                    }
                    if (!isset($arrMenu[$menu]['is_edit'])){
                        $arrMenu[$menu]['is_edit'] = 0;
                    }
                    if (!isset($arrMenu[$menu]['is_view'])){
                        $arrMenu[$menu]['is_view'] = 0;
                    }
                    if (!isset($arrMenu[$menu]['is_delete'])){
                        $arrMenu[$menu]['is_delete'] = 0;
                    }
                }
                $menu = $value->acl_key;

                $is_display = 'IS_DISPLAY_' . $value;
                $is_create = 'IS_CREATE_' . $value;
                $is_edit = 'IS_EDIT_' . $value;
                $is_view = 'IS_VIEW_' . $value;
                $is_delete = 'IS_DELETE_' . $value;
                foreach ($arr as $key1 => $value1) {
                    $value1 = trim($value1);
                    {
                        if ($is_display  == $value1){
                            $arrMenu[$menu]['is_display'] = 1;
                        }
                        if ($is_create == $value1){
                            $arrMenu[$menu]['is_create'] = 1;
                        }
                        if ($is_edit == $value1){
                            $arrMenu[$menu]['is_edit'] = 1;
                        }
                        if ($is_delete == $value1){
                            $arrMenu[$menu]['is_delete'] = 1;
                        }
                        if ($is_view == $value1){
                            $arrMenu[$menu]['is_view'] = 1;
                        }
                    }
                }
            }

            foreach ($arrMenu as $key => $value) {
                $userAcl = new AclRoleUserModel();
                $rows = AclRoleUserModel::where('role_id', $role_id)
                            ->where('acl_key', $key)
                            ->select('id')->get();
                $id = 0;
                if ($rows->count() > 0){
                    $id = $rows[0]['id'];
                    $userAcl = AclRoleUserModel::find($id);
                }
                $userAcl->role_id = $role_id;
                $userAcl->acl_key = $key;
                $userAcl->is_display = $arrMenu[$key]['is_display'];
                $userAcl->is_create = $arrMenu[$key]['is_create'];
                $userAcl->is_edit = $arrMenu[$key]['is_edit'];
                $userAcl->is_view = $arrMenu[$key]['is_view'];
                $userAcl->is_delete = $arrMenu[$key]['is_delete'];
                $userAcl->save();
                //print_r($userAcl); die();
                $id = $userAcl->id;
                $sql = "select * from tb_role_user_acl_list where id=$id";
                $rows = DB::select($sql);
                //print_r($rows); die();
            }
            $this->mStatus = 1;
            $this->mMessage = 'Saved Success';
            DB::commit();
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
            DB::rollback();
        }
        //$this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getList(Request $request){
        try{
            $user_id = (int) $request->get('user_id');
            $role_id = (int) $request->get('role_id');
            if ($role_id > 0){
                return $this->getListRole($request);
            }
            if ($user_id < 1){
                $user_id = 1;
            }
/*            if ($user_id < 1) {
                    $message1 = 'Autherization failed. Ask support team to provide Autherization token';
                    $message = array( 'message' => $message1);
                 return abort(404, $message1);
            }
*/            //TOBEREMOVED
            //$user_id = 7;
            //TOBEREMOVED
            $sql = '';
            $sql .= "select acl.acl_key as main_acl_key, acl.acl_name, uacl.* from tb_acl_list acl
            left join tb_user_acl_list uacl on acl.acl_key = uacl.acl_key and uacl.user_id=$user_id ";
            $sql .= ' order by acl.order_by ';
            $rows = DB::select($sql);
            $this->mStatus = 1;
            $this->mMessage = 'success';
            $this->mData = $rows ;
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getListRole(Request $request){
        try{
            $role_id = (int) $request->get('role_id');
            if ($role_id < 1){
                $role_id = 4;
            }
            $sql = '';
            $sql .= "select acl.acl_key as main_acl_key, acl.acl_name, racl.* from tb_acl_list acl
            left join tb_role_user_acl_list racl on acl.acl_key = racl.acl_key and racl.role_id=$role_id ";
            $sql .= ' order by acl.order_by ';
            //print_r( $sql);             die();
            $rows = DB::select($sql);
            $this->mStatus = 1;
            $this->mMessage = 'success';
            $this->mData = $rows ;
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getListFields(Request $request){
        try{
            $user_id = 1;
            $screen = (int) $request->get('screen');
            $sql = '';
            $sql .= "select acl.acl_key as main_acl_key, acl.acl_name, racl.* from tb_acl_fields_list acl
            left join tb_user_acl_fields_list racl on acl.acl_key = racl.menu_key_name and racl.user_id=$user_id ";
            //$sql .= ' order by acl.order_by ';
            $rows = DB::select($sql);
            $this->mStatus = 1;
            $this->mMessage = 'success';
            $this->mData = $rows ;
        } catch (Exception $e) {
            $this->mMessage = $e->getMessage();
        }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function backup(Request $request){
         $log = __FILE__ . '::' . __FUNCTION__ . '() =>'. ' Begin';
        Log::debug($log);
        return 'Success';
        //return $this->sendResponseDefault();
    }

}