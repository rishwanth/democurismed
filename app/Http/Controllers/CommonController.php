<?php
/*********************************************************
* Product       : CURISMED
* Module        : UserController
* Description   :
*
* Created  Date : 2018-08-01
* Author        : Rishwanth
* 
* History       : 2018-04-04 Login, Logout Implemented
**********************************************************/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
//use App\Http\Requests\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Users;
use App\Model\AuthInfoModel;
use App\Model\PhysicianModel;
use App\Model\InsurancesModel;
use App\Model\ActivityModel;
use App\Model\ZipcodesModel;


use Validator;
use Session;
use Log;

class CommonController extends Controller
{
    private $successStatus = 200;

    /**
     *
     * 
     */
    public function __construct() {

    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getPhysician(Request $request){
        $filter = $request->get('filter');
        if (empty($authNo)){
            $rows = PhysicianModel::select('*')
                        ->get();
        } else {
            $rows = PhysicianModel::where('id','=', $filter)
                        ->select('*')
                        ->get();
        }
        $message = 'failed';
        $data = array();
        $status = 0;
        $message = 'Empty';
        if ($rows->count()){
            $rows =  $rows;
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }

    public function upload()
    {
        return view('upload');
    }

    function saveUploadFile(Request $request, $fileName = ''){
        if (strlen($fileName) < 2) {
            $fileName = 'myfile';
        }

        if (!$request->hasFile($fileName )) {
            return ;
        }

        if (!$request->file($fileName )->isValid()) {
            return ;
        }

        $path = $request->$fileName->path();

        $extension = $request->$fileName->extension();
        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "GIF", "JPG", "PNG", "doc", "txt", "docx", "pdf", "xls", "xlsx"); //add the formats 
        //check valid extension


        $targetFilename = time() . '.' . $extension;
        $folder = public_path('/') . 'docs/';

        $path = $request->$fileName->storeAs($folder, $targetFilename);
    }



    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getActivity(Request $request){
        $id = $request->get('id');
        if (empty($id)){
            $rows = ActivityModel::select('*')
                        ->get();
        } else {
            $rows = ActivityModel::where('id','=', $id)
                        ->select('*')
                        ->get();
        }
        $message = 'failed';
        $data = array();
        $status = 0;
        $message = 'Empty';
        if ($id > 0) {
            $message = " Records not found for #$id.";
        }
        if ($rows->count()){
            $rows =  $rows;
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function deleteActivity(Request $request){
        $id = $request->get('id');
        $status = 0;
        $message = 'Failed';
        $data = array();
        if (empty($id)){
            $message = "Acitivity is empty";
        } else {
            $result = ActivityModel::where('id','=', $id)
                        ->delete();
            if ($result){
                $status = 1;
                $message = 'Deleted successfully';
            }
        }
        return $this->sendResponse($status, $message, $data);
    }


    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function deleteAuthInfo(Request $request){
        $id = $request->get('id');
        $status = 0;
        $message = 'Failed';
        $data = array();
        if (empty($id)){
            $message = "AuthInfo id is empty";
        } else {
            $result = AuthInfoModel::where('id','=', $id)
                        ->delete();
            if ($result){
                $status = 1;
                $message = 'Deleted successfully';
            } else {
                $message = 'Deleted Failed';
            }
        }
        return $this->sendResponse($status, $message, $data);
    }

    /**
     *  Purpose     :
     *  Input       :
     *  Output      :
     *  History     : 
     */
    public function getZipInfo(Request $request){
        $zip = $request->get('zip');
        if (empty($zip)){
            $this->mMessage = 'Zip is required';
                return $this->sendResponseDefault();
        } else {
            $rows = ZipcodesModel::where('zip','=', $zip)
                        ->select('*')
                        ->get();
        }
        $message = 'failed';
        $data = array();
        $status = 0;
        $message = 'Empty';
        if ($zip != "") {
            $message = "Records not found for zip $zip";
        }
        if ($rows->count()){
            $rows =  $rows;
            $status = 1;
            $message = 'Success';
            $data = $rows ;
        }
        return $this->sendResponse($status, $message, $data);
    }
}