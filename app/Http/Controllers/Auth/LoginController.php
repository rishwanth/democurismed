<?php

namespace App\Http\Controllers\Auth;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Support\Facades\Session;
use App\Http\Controllers\UserController;
use App\Http\Controllers\API\PassportController;
use App\User;
use App\Users;
//use App\Http\Requests\Request;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','login');
        //$this->middleware('guest');
    }

    public function login(Request $request) {
        $message = '****login' . print_r($request->all(), true);
        $response =  array('status' => 0, 'message' => $message, 'data' => array());
            return (json_encode($response));      
        
        

        $username = $request->get('username');
        $password = $request->get('password');
        $passport = new PassportController($username, $password);
        $tokenResponse = $passport->login();
        return $tokenResponse;
    }
    
    public function validateLogin($request){
        $response =  array('status' => 0, 'message' => '****validateLogin', 'data' => array());
            return (json_encode($response));      
        //$userController = new UserController();
        //return $userController->login($request);
    }

    public function sendFailedLoginResponse($request){
        //$response =  array('status' => 0, 'message' => '***', 'data' => array());
        //return (json_encode($response));      

        if (!isset($request->username) || !isset($request->password)){
            $response =  array('status' => 0, 'message' => 'Invalid username or password', 'data' => array());
            return (json_encode($response));      
        }

        $row = Users::where('username', $request->username)->select('*')->get();
        if ( $row->count() == 0 ) {
            if ($request->ajax() || $request->wantsJson()){
                $response =  array('status' => 0, 'message' => 'Invalid username', 'data' => array());
                return (json_encode($response));      
            } else {
                return redirect()->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors([
                        $this->username() => 'a',
                    ]);
            }
        }

        $password = '*';
        if (isset($request->password)) {
            $password = $request->password;
        }

        $row1 = Users::where('username', $request->username)
                    ->where('password', sha1($password))->select('*')->get();
        if ( $row1->count() == 0 ) {
            if ($request->ajax() || $request->wantsJson()){
                $response =  array('status' => 0, 'message' => 'Invalid password', 'data' => array());
                return json_encode($response);      
            } else {
                return redirect()->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors([
                        'password' => 'p',
                    ]);
            }
        }   

        if ($row1->count()>0) {
            $passport = new PassportController();
            $tokenResponse = $passport->getAccessToken($row1[0]->email_id, $password, $row1[0]->id);

        if ($request->ajax() || $request->wantsJson()){
                return view('patient');
                //return $tokenResponse;
            } else {
                return view('patient');
            }
        }

        if ($request->ajax() || $request->wantsJson()){
            $response =  array('status' => 0, 'message' => 'Access token failed. Ask support team to generate access token', 'data' => array());
            return json_encode($response);      
        } else {
            $response =  array('status' => 0, 'message' => 'Access token failed. Ask support team to generate access token', 'data' => array());
            return json_encode($response);      
        }
    }
    

}
