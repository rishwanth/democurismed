<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : RegisterController
* Description   : 
*
* Created date  : 2018-08-01 
* Created time  : 06:00 AM IST
* Author        : Rishwanth
* 
* History       : 2018-04-04 07:30 PM IST, Rishwanth, Code clean up
* History       : 2018-04-04 08:00 PM IST, Rishwanth, renamed the function name
********************************************************************************************/
namespace App\Http\Controllers;
use DB;
use QueryException;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Users;

class RegisterController extends Controller {

    public function __construct() {
    }

    /**
     * 	Purpose 	: Display a register page
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */

    public function index() {
        $this->addLogs(__FILE__, __FUNCTION__, 'Begin');
        return view('register');    
    }

    /**
     * 	Purpose 	:
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */
    public function update(Request $request) {
		$response =  array('status' => 0, 'message' => 'Register Failed', 'data' => array());
        $flagMailSent = 0;
    	try {
	    	$firstName = $request->get('firstName');
			$lastName = $request->get('lastName');
			$userName = $request->get('userName');
			$role = $request->get('role');
			$emailID = $request->get('emailID');
			$phoneNo =  $request->get('mobileNo');
			$practiceID =  $request->get('practiceID');
            
            $user = Users::where('email_id', '=', "'$emailID'")
                    ->orWhere('username','=', "'$userName'");
            if ($user->count() > 0){
                $this->mStatus = 0;
                $this->mMessage =  'Username or email already registerd';
                $this->mData = array();          
                return $this->sendResponseDefault();
            }

            //return $request->all();
	        $register = new Users();
	        $register->first_name = $firstName;
	        $register->last_name = $lastName;
	        $register->username = $userName;
			$register->role = $role;
			$register->password = $this->getUUID();
			$register->password = sha1('a123');
			$register->email_id = $emailID;
			$register->phone_num = $phoneNo;
            $register->practice_id = $practiceID;
            $register->token = $this->getUUID();
	        if ($register->save()){
		        if ($register->id > 0) {
                    $this->mMessage = 'Register success. Please check your mail.';
                    $this->mStatus = 1;
					$this->mData = array('id' => $register->id);	        
                    $objMail = new MailController();
                    $objMail->sendActionLink($register->id);
				} 
			}
	    } catch (QueryException $e) {
            $this->mMessage = $e->getMessage();
	    } catch (\Exception $e) {
            $this->mMessage = $e->getMessage();
	    }
        $this->addLogs(__FILE__, __FUNCTION__, 'End');
        return $this->sendResponseDefault();
    }

    /**
     * 	Purpose 	:
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */
    function generatePassword(){
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
		    $n = rand(0, $alphaLength);
		    $pass[] = $alphabet[$n];
		}
		$password = implode($pass); //turn the array into a string
		return $password;
    }


    // will be removed
    /**
     * 	Purpose 	:
     * 	Input 		:
     *  Output 		:
     * 	History 	: 
     */
    public function savePassword(Request $request){
        dd('a');
    }
}
