Route::get('login', array(
	'uses' => 'UserController@login'
));
 
// route to process the form
 
Route::post('login', array(
	'uses' => 'UserController@login'
));
Route::get('logout', array(
	'uses' => 'UserController@logout'
));
Route::get('/',
function ()
	{
	return view('welcome');
	});