<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : HandlerUsersModel 
* Description   : 
*
* Created date  : 
* Created time  :
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HandlerUsersModel extends Model {
    //
    protected $table = 'tb_handler_users';
}

