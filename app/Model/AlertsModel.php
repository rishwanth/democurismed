<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AlertsModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 08:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AlertsModel extends Model {
    //
    protected $table = 'tb_alerts';
}

