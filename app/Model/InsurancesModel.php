<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : InsurancesModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InsurancesModel extends Model {
    //
    protected $table = 'tb_insurances';

    protected $fillable = [
        'payer_name',
    ];
}

