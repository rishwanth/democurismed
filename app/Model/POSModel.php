<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : POSModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class POSModel extends Model {
    //
    protected $table = 'tb_place_of_services';
}

