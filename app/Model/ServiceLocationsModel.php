<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ClaimsModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 10:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceLocationsModel extends Model {
    //
    protected $table = 'tb_service_locations';

    protected $fillable = ['internal_name', 'npi', 'time_zone',
    'legacy_num', 'billing_name', 'address', 'phone', 'fax', 'place_of_service', 'practice_id'
	];
}

