<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : DepositDetailsModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 11:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DepositDetailsModel extends Model {
    //
    protected $table = 'tb_deposit_details';
}

