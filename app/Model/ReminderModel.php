<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ReminderModel 
* Description   : 
*
* Created date  : 
* Created time  : 
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReminderModel extends Model {
    //
    protected $table = 'tb_reminder';
}

