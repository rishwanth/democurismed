<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PracticesInsuranceModel
* Description   : 
*
* Created date  : 
* Created time  : 
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PracticesInsuranceModel extends Model {
    //
    protected $table = 'tb_practices_insurance';
}

