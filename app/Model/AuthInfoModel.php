<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : Users 
* Description   : 
*
* Created date  : 2018-08-04 
* Created time  : 06:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AuthInfoModel extends Model
{
    //
    protected $table = 'tb_auth';
}

