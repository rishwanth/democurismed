<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ZipcodesModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ZipcodesModel extends Model {
    //
    protected $table = 'tb_zipcodes';
}

