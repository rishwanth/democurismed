<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AclRoleUserModel 
* Description   : 
* Created date  : 2018-11-19 08:00:00 IST
* Author        : Rishwanth
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AclRoleUserModel extends Model {
    protected $table = 'tb_role_user_acl_list';
}

