<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : BaseModel 
* Description   : 
*
* Created at  	: 2018-11-14 08:00:00
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
	private $mTable;

    public function _getTableName(){
		return $this->mTable;    	
    }
	
}

