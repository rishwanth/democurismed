<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PracticePaycodesModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PracticePaycodesModel extends Model {
    //
    protected $table = 'tb_practice_paycodes';

    protected $fillable = [
		'practice_id', 
		'paycode_id', 
		'is_active', 
    ];
}

