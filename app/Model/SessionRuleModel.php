<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : SessionRuleModel
* Description   : 
*
* Created date  : 
* Created time  : 
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SessionRuleModel extends Model {
    //
    protected $table = 'tb_session_rule';

}

