<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PortalLogsModel 
* Description   : 
*
* Created date  : 2018-08-20 
* Created time  : 03:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PortalLogsModel extends Model {
    //
    protected $table = 'tb_portal_logs';
}

