<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PayPeriodsModel 
* Description   : 
*
* Created date  :  
* Created time  : 
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PayPeriodsModel extends Model {
    //
    protected $table = 'tb_pay_periods';
}

