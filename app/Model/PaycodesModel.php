<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PaycodesModel
* Description   : 
*
* Created date  : 2018-09-17 
* Created time  : 12:40 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaycodesModel extends Model {
    //
    protected $table = 'tb_paycodes';
}

