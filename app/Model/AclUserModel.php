<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AclUserModel 
* Description   : 
*
* Created date  : 2018-08-05 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AclUserModel extends Model
{
    //
    protected $table = 'tb_user_acl_list';
}
