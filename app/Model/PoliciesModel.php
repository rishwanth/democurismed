<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PoliciesModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 11:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PoliciesModel extends Model {
    //
    protected $table = 'tb_policies';
}

