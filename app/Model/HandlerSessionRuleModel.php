<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : HandlerSessionRuleModel 
* Description   : 
*
* Created date  : 
* Created time  :
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HandlerSessionRuleModel extends Model {
    //
    protected $table = 'tb_handler_session_rule';
}

