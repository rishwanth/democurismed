<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PhysicianModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ActivityModel extends Model {
    //
    protected $table = 'tb_activity';
}

