<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PhysiciansPaycodesModel 
* Description   : 
*
* Created date  : 2018-09-17 
* Created time  : 03:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PhysiciansPaycodesModel extends Model {
    //
    protected $table = 'tb_physicians_paycodes';
}

