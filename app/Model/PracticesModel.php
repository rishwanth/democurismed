<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PracticesModel
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 11:15 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PracticesModel extends Model {
    //
    protected $table = 'tb_practices';
}

