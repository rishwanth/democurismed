<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class Event extends Model
{
    //
    protected $table = 'template_calendar_events';
    protected $fillable = ['title','start_date','end_date'];
}