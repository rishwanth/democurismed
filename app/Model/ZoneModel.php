<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : InsurancesModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ZoneModel extends Model {
    //
    protected $table = 'tb_zone';

    protected $fillable = [
        'name',
        'description',
		'practice_id', 
		'is_active', 
    ];
}

