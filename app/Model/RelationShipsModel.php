<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : RelationShipsModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RelationShipsModel extends Model {
    //
    protected $table = 'tb_relation_ships';
}

