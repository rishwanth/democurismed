<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : AppointmentsModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 12:00 AM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AppointmentsModel extends Model {
    //
    protected $table = 'tb_appointments';
}

