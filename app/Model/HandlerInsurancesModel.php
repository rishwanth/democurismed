<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : HandlerInsurancesModel 
* Description   : 
*
* Created date  : 
* Created time  :
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HandlerInsurancesModel extends Model {
    //
    protected $table = 'tb_handler_insurances';
}

