<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : InsurancesModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InsuranceSetupModel extends Model {
    //
    protected $table = 'tb_insurance_setup';

    protected $fillable = [
        'payer_name',
		'insurance_id', 
		'practice_id', 
		'edi_type', 
		'is_day_club' , 
		'is_capitated', 
		'cms1500_31',
		'cms1500_32a', 
		'cms1500_32b', 
		'cms1500_33a', 
		'cms1500_33b', 
		'is_active', 
    ];
}

