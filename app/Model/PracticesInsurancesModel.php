<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PracticesInsuranceModel
* Description   : 
*
* Created date  : 
* Author        : Rishwanth
*
* Modified 		
* Thu Nov 15 10:51:46 IST 2018 : added based class
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\BaseModel;

class PracticesInsurancesModel extends BaseModel {
    
    protected $table = 'tb_practices_insurance';
}
