<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : TestingPaginationModel 
* Description   : 
*
* Created date  : 2018-08-12 
* Created time  : 06:00 AM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TestingPaginationModel extends Model {
    //
    protected $table = 'tb_testing_pagination';
}

