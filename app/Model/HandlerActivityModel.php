<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : HandlerActivityModel 
* Description   : 
*
* Created date  : 
* Created time  :
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HandlerActivityModel extends Model {
    //
    protected $table = 'tb_handler_activity';
}

