<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : HandlerCodesModel 
* Description   : 
*
* Created date  : 
* Created time  :
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HandlerCodesModel extends Model {
    //
    protected $table = 'tb_handler_codes';
}

