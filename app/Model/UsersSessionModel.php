<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : UsersSessionModel 
* Description   : 
*
* Created date  : 2018-08-17 
* Created time  : 11:00 AM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsersSessionModel extends Model {
    //
    protected $table = 'tb_users_session';

    protected $fillable = [
        'session_token', 'user_id', 'date_login', 
        'date_last_accessed', 'is_active'
   ];

}

