<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PhysiciansModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PhysiciansModel extends Model {
    //
    protected $table = 'tb_physicians';
}

