<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : StatementsModel 
* Description   : 
*
* Created date  : 2018-08-29 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatementsModel extends Model {
    //
    protected $table = 'tb_statements';
}

