<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : PhysiciansWorkhistoryModel 
* Description   : 
*
* Created date  : 2018-09-17 
* Created time  : 03:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PhysiciansWorkhistoryModel extends Model {
    //
    protected $table = 'tb_physicians_workhistory';
}

