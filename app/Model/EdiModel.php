<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : EdiModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 10:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EdiModel extends Model {
    //
    protected $table = 'tb_edi';
}

