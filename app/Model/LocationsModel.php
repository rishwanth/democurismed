<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : ClaimsModel 
* Description   : 
*
* Created date  : 2018-08-07 
* Created time  : 10:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LocationsModel extends Model {
    //
    protected $table = 'tb_locations';
    protected $fillable = [
        'patient_id', 'home', 'office', 'school'
    ];
}

