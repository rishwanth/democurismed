<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : RoleModel 
* Description   : 
* Created date  : 2018-11-19 10:00:00 AM IST
* Author        : Rishwanth 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleModel extends Model {
    protected $table = 'tb_role';
   
}

