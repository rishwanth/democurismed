<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : TestpurposeModel 
* Description   : 
*
* Created date  : 2018-08-01 
* Created time  : 12:00 AM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TestpurposeModel extends Model {
    //
    protected $table = 'tb_users';
}

