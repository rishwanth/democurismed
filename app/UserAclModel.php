<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : Users 
* Description   : 
*
* Created date  : 2018-08-05 
* Created time  : 04:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAclModel extends Model
{
    //
    protected $table = 'tb_user_acl_list';
}
