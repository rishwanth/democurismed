<?php
/***************************************************************************************
* Product       : CURISMED
* Module        : Users 
* Description   : 
*
* Created date  : 2018-08-04 
* Created time  : 06:00 PM IST
* Author        : Rishwanth
* 
********************************************************************************************/
namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    protected $table = 'tb_users';

    protected $fillable = [
        'first_name', 'last_name', 'username', 
        'role', 'password', 'email_id', 'phone_num',  
        'is_activated', 'is_logged', 'token', 'reset_token', 'oauth_token'
   ];
}
