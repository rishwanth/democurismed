<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::any('/', function () {
    return view('welcome');
});
*/
Route::any('/', 'HomeController@index');
Route::any('csrftoken/get', 'HomeController@getCsrfToken');
Route::any('lock_screen', 'HomeController@lock_screen');
Route::any('list_insurances', 'SettingController@list_insurances');
Route::any('CPTs', 'SettingController@CPTs');
Route::any('DXs', 'SettingController@DXs');
Route::any('list_physicians', 'SettingController@list_physicians');
Route::any('list_facilities', 'SettingController@list_facilities');
Route::any('list_practices', 'SettingController@list_practices');
Route::any('view_practice', 'SettingController@view_practice');
Route::any('add_practice', 'SettingController@add_practice');
Route::any('list_serviceLocations', 'SettingController@list_serviceLocations');

Route::any('list_edi', 'AppControllers\EdiController@list_edi');
Route::any('generate_edi', 'AppControllers\EdiController@generate_edi');
Route::any('generate_statements', 'AppControllers\EdiController@generate_statements');
Route::any('list_statements', 'AppControllers\EdiController@list_statements');
Route::any('login','UserController@login');
Route::any('logout','UserController@logout');

Route::any('generate_statements','PatientController@generate_statements');
Route::any('list_statements','PatientController@list_statements');
Route::any('statement_list','PatientController@statement_list');

Route::any('patient/add', 'PatientController@add');
Route::any('patient/edit', 'PatientController@edit');
Route::any('patient/view', 'PatientController@view');
Route::any('patient/update', 'PatientController@update');
Route::any('patient/list', 'PatientController@getList');
Route::any('patient/list1', 'PatientController@list1');
Route::any('patient/list_calendar', 'PatientController@list_calendar');
Route::any('patient/list2', 'PatientController@list2');
Route::any('patient/list3', 'PatientController@list3');
Route::any('patient/list5', 'PatientController@list5');
Route::any('patient/listbypatientid', 'PatientController@listBypatientID');
Route::any('patient/create', 'PatientController@create');
Route::any('patient/updatechartnum', 'PatientController@updateChartNum');
Route::any('patient/getpatname', 'PatientController@getPatName');

Route::any('patient/getstatement', 'PatientController@getStatementAsPDF');



Route::any('scheduler', 'SchedulerController@scheduler');
Route::any('scheduler_calendar', 'SchedulerController@scheduler_calendar');
Route::any('appointments/list', 'AppControllers\AppointmentsController@list_appointments');
Route::any('appointments/recurrence', 'AppControllers\AppointmentsController@getPlannerDetails');

//User
Route::any('register', 'RegisterController@index');
Route::any('forgotpassword', 'UserController@forgotpasswordShowScreen');
Route::any('user/forgotpassword', 'UserController@forgotpassword');
Route::any('user/activation', 'UserController@activateAccount');
Route::any('user/resetpassword', 'UserController@resetpasswordShowScreen');
Route::any('user/resetpasswordsave', 'UserController@resetpassword');
Route::any('user/password/save', 'UserController@savePassword');
Route::any('register/update', 'RegisterController@update');

Route::any('acl', 'AclController@index');
Route::any('acl/create', 'AclController@create');
Route::any('acl/save', 'AclController@save');
Route::any('acl/getlist', 'AclController@getList');

Route::any('acl/role', 'AclController@indexRole');
Route::any('acl/rolesave', 'AclController@save');
Route::any('acl/rolegetlist', 'AclController@getList');


// Email related routes
Route::any('mail/send', 'MailController@send');

Route::any('document/upload', 'CommonController@upload');
Route::any('document/uploadfile', 'CommonController@saveUploadFile');


//Common
Route::any('authinfo/add', 'AppControllers\AuthInfoController@add');
Route::any('authinfo/list', 'AppControllers\AuthInfoController@getAuthInfoList');
Route::any('authinfo/patient', 'AppControllers\AuthInfoController@patient');
Route::any('authinfo/getinfobyauthnum', 'AppControllers\AuthInfoController@getAuthInfoByAuthNum');
Route::any('authinfo/delete', 'CommonController@deleteAuthInfo');
Route::any('authinfo/gettos', 'AppControllers\AuthInfoController@getTOS');
Route::any('authinfo/getauthoinfo1', 'AppControllers\AuthInfoController@getAuthInfo1');
Route::any('authinfo/getauthoinfo2', 'AppControllers\AuthInfoController@getAuthInfo2');
Route::any('authinfo/getauth1', 'AppControllers\AuthInfoController@getAuth1');
Route::any('authinfo/getauths', 'AppControllers\AuthInfoController@getAuths');


//Route::any('physician/get', 'AppControllers\PhysiciansController@getPhysician');
Route::any('physician/listall', 'AppControllers\PhysiciansController@listAll');
Route::any('physician/listold', 'AppControllers\PhysiciansController@listold');
Route::any('physician/list', 'AppControllers\PhysiciansController@list');
Route::any('physician/addnew', 'AppControllers\PhysiciansController@addNew');
Route::any('physician/list1', 'AppControllers\PhysiciansController@list1');
Route::any('physician/add', 'AppControllers\PhysiciansController@add');
Route::any('physician/update', 'AppControllers\PhysiciansController@update');
Route::any('physician/get', 'AppControllers\PhysiciansController@get');
Route::any('physician/create', 'AppControllers\PhysiciansController@add');
//Route::any('physician/load', 'AppControllers\PhysiciansController@getListAll');

Route::any('activity/add', 'AppControllers\ActivityController@add');
//Route::post('activity/add', 'AppControllers\ActivityController@add');
Route::post('activity/getbyauth', 'AppControllers\ActivityController@getActivityByAuth');
Route::any('activity/list', 'AppControllers\ActivityController@getActivity');
Route::any('activity/delete', 'AppControllers\ActivityController@deleteActivity');
Route::any('activity/auth', 'AppControllers\ActivityController@auth');


Route::any('codeicd/get', 'AppControllers\CodeicdController@list');
Route::any('codeicd/list', 'AppControllers\CodeicdController@list');
Route::any('codeicd/add', 'AppControllers\CodeicdController@add');
//Route::any('codeicd/get', 'CommonController@getCodeIcd');
//Route::any('codeicd/list', 'CommonController@getCodeIcd');
Route::any('codecpt/update', 'AppControllers\CodeCptController@update');
Route::any('codecpt/list', 'AppControllers\CodeCptController@list');


Route::any('zipinfo/get', 'CommonController@getZipInfo');
Route::any('zipinfo/list', 'CommonController@getZipInfo');

Route::any('cases/get', 'AppControllers\CaseController@getList');
Route::any('cases/add', 'AppControllers\CaseController@add');
Route::any('cases/getgroup', 'AppControllers\CaseController@getGroup');
Route::any('cases/list', 'AppControllers\CaseController@getList');
Route::any('cases/loadbychartnum', 'AppControllers\CaseController@loadByChartNum');
Route::any('cases/getbychartnum', 'AppControllers\CaseController@getCaseChart');
Route::any('cases/getchartnum', 'AppControllers\CaseController@getChartNum');
Route::any('cases/getcaseid', 'AppControllers\CaseController@getCaseID');



Route::any('guarantor/add', 'AppControllers\GuarantorController@add');
Route::any('guarantor/get', 'AppControllers\GuarantorController@get');
Route::any('guarantor/list', 'AppControllers\GuarantorController@list');

Route::any('authinfo/get', 'CommonController@getAuthInfo');
Route::any('activity/list', 'CommonController@getActivity');

Route::any('patient/list', 'PatientController@getList');
Route::any('patient/lastpayment', 'PatientController@getLastPayment');
Route::any('patient/insurance', 'PatientController@getInsurance');
Route::any('patient/lastcharge', 'PatientController@getLastCharge');
Route::any('patient/laststatement', 'PatientController@getLastStatment');
Route::any('patient/outstandinginsurance', 'PatientController@getOutstandingInsurance');
Route::any('patient/outstanding', 'PatientController@getOutstanding');
Route::any('patient/claimslist', 'PatientController@getClaimsList');


//Route::any('codeicd/get', 'CommonController@getCodeIcd');
//Route::any('codeicd/list', 'CommonController@getCodeIcd');


Route::any('document/update', 'AppControllers\DocumentsController@update');
Route::any('document/get', 'AppControllers\DocumentsController@get');

Route::any('alerts/save', 'AppControllers\DocumentsController@save');

Route::any('edi/getlast', 'AppControllers\EdiController@getLast');
Route::any('edi/unset', 'AppControllers\EdiController@unset1');
Route::any('edi/list', 'AppControllers\EdiController@getList');


Route::any('ledger', 'AppControllers\ClaimsController@ledger');
Route::any('claims/list', 'AppControllers\ClaimsController@list');
Route::any('claims/history', 'AppControllers\ClaimsController@history');
Route::any('claims/updatetransfer', 'AppControllers\ClaimsController@updateTransfer');
Route::any('claims/updateadjustment', 'AppControllers\ClaimsController@updateAdjustment');
Route::any('claims/updatesettle', 'AppControllers\ClaimsController@updateSettle');
Route::any('claims/delete', 'AppControllers\ClaimsController@delete');
Route::any('claims/updatehistory', 'AppControllers\ClaimsController@updateHistory');
Route::any('claims/getaccount', 'AppControllers\ClaimsController@getAccount');
Route::any('claims/update', 'AppControllers\ClaimsController@update');
Route::any('claims/refill', 'AppControllers\ClaimsController@refill');
Route::any('claims/status', 'AppControllers\ClaimsController@status');
Route::any('claims/isposted', 'AppControllers\ClaimsController@isPosted');
Route::any('claims/bulkupdateledger', 'AppControllers\ClaimsController@bulkUpdateLedger');
Route::any('claims/updatestatus', 'AppControllers\ClaimsController@updateStatus');
Route::any('claims/updateledger', 'AppControllers\ClaimsController@updateLedger');
Route::any('claims/getledgerinfo', 'AppControllers\ClaimsController@getLedgerInfo');
Route::any('claims/getpatientname', 'AppControllers\ClaimsController@getPatientName');
Route::any('claims/getall', 'AppControllers\ClaimsController@getAll');
Route::any('claims/getconfirmed', 'AppControllers\ClaimsController@getConfirmed');
Route::any('claims/getsubmit', 'AppControllers\ClaimsController@getSubmit');
Route::any('claims/getready', 'AppControllers\ClaimsController@getReady');
Route::any('claims/get', 'AppControllers\ClaimsController@get');
Route::any('claims/loadbycaseid', 'AppControllers\ClaimsController@loadClaimByCaseId');
Route::any('claims/add', 'AppControllers\ClaimsController@add');
Route::any('claims/save', 'AppControllers\ClaimsController@saveClaim');
Route::any('claims/listclaim', 'AppControllers\ClaimsController@listClaim');
Route::any('claims/listdeposit', 'AppControllers\ClaimsController@listDepositClaim');
Route::any('claims/listdepositERA', 'AppControllers\ClaimsController@listDepositERA');
Route::any('claims/listpatientclaim', 'AppControllers\ClaimsController@listPatientClaim');
Route::any('claims/listall', 'AppControllers\ClaimsController@listAllClaims');
Route::any('claims/increment', 'AppControllers\ClaimsController@increment');
Route::any('claims/remove', 'AppControllers\ClaimsController@removeClaim');
Route::any('claims/undoremove', 'AppControllers\ClaimsController@undoRemoveClaim');
Route::any('claims/listbydeposit', 'AppControllers\ClaimsController@listByDeposit');
Route::any('claims/ledgerpatientonly', 'AppControllers\ClaimsController@ledgerPatientOnly');
Route::any('claims/ledgerinsuranceonly', 'AppControllers\ClaimsController@ledgerInsuranceOnly');
Route::any('claims/listclaimbynumber', 'AppControllers\ClaimsController@listClaimByNumber');
Route::any('claims/generate', 'AppControllers\ClaimsController@generate');
Route::any('claims/generateall', 'AppControllers\ClaimsController@generateAll');
Route::any('claims/generatepaper', 'AppControllers\ClaimsController@generatePaper');
Route::any('claims/loadlocations', 'AppControllers\ClaimsController@loadLocations');
Route::any('claims/transaction', 'AppControllers\ClaimsController@getTransations');


Route::any('reminder/add', 'AppControllers\ReminderController@add');
Route::any('reminder/update', 'AppControllers\ReminderController@add');
Route::any('reminder/get', 'AppControllers\ReminderController@getByUserID');


Route::any('servicelocation/list', 'AppControllers\ServiceLocationsController@list');
Route::any('location/list', 'AppControllers\LocationsController@getLocation');
Route::any('location/add', 'AppControllers\LocationsController@add');
Route::any('location/update', 'AppControllers\LocationsController@save');
Route::any('location/save', 'AppControllers\LocationsController@save');

Route::any('cases/getauth1', 'AppControllers\CaseController@getAuth1');
Route::any('policies/update', 'AppControllers\PoliciesController@update');
Route::any('policies/check', 'AppControllers\PoliciesController@check');
Route::any('policies/loadpolicy', 'AppControllers\PoliciesController@loadPolicy');
Route::any('policies/createpolicy', 'AppControllers\PoliciesController@createPolicy');



Route::any('deposits', 'AppControllers\DepositeController@index');
Route::any('deposits/post', 'AppControllers\DepositeController@post');
Route::any('deposits/update', 'AppControllers\DepositeController@update');
Route::any('deposits/any', 'AppControllers\DepositeController@any');
Route::any('deposits/get', 'AppControllers\DepositeController@get');
Route::any('deposits/check', 'AppControllers\DepositeController@check');
Route::any('deposits/delete', 'AppControllers\DepositeController@delete');
Route::any('deposits/total', 'AppControllers\DepositeController@total');
Route::any('deposits/loaddeposits', 'AppControllers\DepositeController@loadDeposits');
Route::any('deposits/create', 'AppControllers\DepositeController@createDeposit');
Route::any('deposits/adjustdeposit', 'AppControllers\DepositeController@adjustDeposit');
Route::any('deposits/adjustpatdeposit', 'AppControllers\DepositeController@adjustPatDeposit');
Route::any('deposits/findbalance', 'AppControllers\DepositeController@findBalance');
Route::any('deposits/depositdetails', 'AppControllers\DepositeController@depositDetails');
Route::any('deposits/reverseadjust', 'AppControllers\DepositeController@reverseAdjustDeposit');



Route::any('insurances/add', 'AppControllers\InsurancesController@add');
Route::any('insurances/update', 'AppControllers\InsurancesController@update');
Route::any('insurances/get', 'AppControllers\InsurancesController@getList');
Route::any('insurances/getAddress', 'AppControllers\InsurancesController@getAddress');
Route::any('insurances/list', 'AppControllers\InsurancesController@list');
Route::any('insurances/getname', 'AppControllers\InsurancesController@getName');
Route::any('insurances/getByClaimId', 'AppControllers\InsurancesController@getByClaimId');
Route::any('insurances/getidbypayername', 'AppControllers\InsurancesController@getIdByPayerName');
Route::any('insurances/listpatients', 'AppControllers\InsurancesController@listPatients');
Route::any('insurances/getByClaimId', 'AppControllers\InsurancesController@getByClaimId');




Route::any('appointments/times', 'AppControllers\AppointmentsController@times');
Route::any('appointments/update', 'AppControllers\AppointmentsController@update');
Route::any('appointments/get', 'AppControllers\AppointmentsController@get');
Route::any('appointments/updatebulk', 'AppControllers\AppointmentsController@update');
Route::any('appointments/gets', 'AppControllers\AppointmentsController@gets');
Route::any('appointments/recent', 'AppControllers\AppointmentsController@recent');
Route::any('appointments/add', 'AppControllers\AppointmentsController@add');

Route::any('practices/add', 'AppControllers\PracticesController@save');
Route::any('practices/update', 'AppControllers\PracticesController@update');
Route::any('practices/list', 'AppControllers\PracticesController@list');
Route::any('practices/listforregister', 'AppControllers\PracticesController@listForRegister');
Route::any('practices/create', 'AppControllers\PracticesController@create');

Route::any('pos/list', 'AppControllers\POSController@list');
Route::any('pos/getinfo', 'AppControllers\POSController@getInfo');
Route::any('test/test', 'AppControllers\TestpurposeController@index');

Route::any('addfakename', 'AppControllers\TestpurposeController@addRecordsForPagination');
Route::any('pagination', 'AppControllers\TestpurposeController@page1');
Route::any('_t1', 'AppControllers\TestpurposeController@page2');
Route::any('mysql', 'AppControllers\TestpurposeController@getMysql');
Route::any('logs', 'AppControllers\TestpurposeController@logs1');
Route::any('hcfa', 'AppControllers\HCFA500Controller@pdf');
Route::any('edi/create', 'AppControllers\EdiController@create');
Route::any('edi/download1', 'AppControllers\EdiController@textFile');
Route::any('edi/uploadtossh2', 'AppControllers\EdiController@uploadToSSH2');
Route::any('edi/uploadallfilestossh2', 'AppControllers\EdiController@uploadAllFilesToSSH2');

Route::any('deposits/geterainfofromfile', 'AppControllers\DepositeController@parse835');

Route::get('events', 'AppControllers\EventController@index');
Route::get('events/add', 'AppControllers\EventController@add');
Route::get('events/deleteall', 'AppControllers\EventController@deleteAll');


Route::any('physician/list1', 'AppControllers\PhysiciansController@list1');
Route::any('physician/save', 'AppControllers\PhysiciansController@add');
Route::any('physician/update', 'AppControllers\PhysiciansController@update');
Route::any('physician/get', 'AppControllers\PhysiciansController@get');
Route::any('physician/create', 'AppControllers\PhysiciansController@add');
//Route::any('physician/load', 'AppControllers\PhysiciansController@getListAll');
Route::any('physician/list', 'AppControllers\PhysiciansController@list');
Route::any('physician/add', 'AppControllers\PhysiciansController@add');
Route::any('physician/listall', 'AppControllers\PhysiciansController@listAll');
Route::any('physician/listold', 'AppControllers\PhysiciansController@listold');
Route::any('physician/addnew', 'AppControllers\PhysiciansController@addNew');
Route::any('physician/edit', 'AppControllers\PhysiciansController@edit');

Route::any('physician/report', 'AppControllers\PhysiciansController@report');

Route::any('physician/activity', 'AppControllers\PhysiciansController@getActivity');

Route::any('helper/get', 'AppControllers\HelperController@getList');
Route::any('helper/add', 'AppControllers\TestpurposeController@add');

//Route::any('report/schedule', 'AppControllers\ReportController@reportTimesheet');
Route::any('report/timesheet', 'AppControllers\ReportController@reportTimesheet');
Route::any('report/payroll', 'AppControllers\ReportController@reportPayroll');
Route::any('report/clients', 'AppControllers\ReportController@reportClients');
Route::any('report/employee', 'AppControllers\ReportController@reportEmployee');
Route::any('report/provider', 'AppControllers\ReportController@reportEmployee');
Route::any('report/deposits', 'AppControllers\ReportController@reportDeposits');
Route::any('report/auth', 'AppControllers\ReportController@reportAuth');
Route::any('report/ledgerinsurance', 'AppControllers\ReportController@reportAccountLedgerInsurance');
Route::any('report/ledgertotalclient', 'AppControllers\ReportController@reportLedgerTotalClient');
Route::any('report/ledgertotalinsurance', 'AppControllers\ReportController@reportLedgerTotalInsurance');
Route::any('report/schedule', 'AppControllers\ReportController@reportSchedule');


Route::any('insurances/addsetup', 'AppControllers\InsurancesController@addSetup');
Route::any('practices/addzone', 'AppControllers\PracticesController@addZone');
Route::any('practices/addholiday', 'AppControllers\PracticesController@addHoliday');
Route::any('practices/addpaycode', 'AppControllers\PracticesController@addPaycode');
Route::any('insurances/listsetup', 'AppControllers\InsurancesController@listSetup');
Route::any('practices/listzone', 'AppControllers\PracticesController@listZone');
Route::any('practices/listholiday', 'AppControllers\PracticesController@listHoliday');
Route::any('practices/listpaycode', 'AppControllers\PracticesController@listPaycode');
Route::any('practices/uploadlogo', 'AppControllers\PracticesController@uploadLogo');

//Practices

Route::any('practices/add', 'AppControllers\PracticesController@save');
Route::any('practices/update', 'AppControllers\PracticesController@update');
Route::any('practices/list', 'AppControllers\PracticesController@list');
Route::any('practices/listforregister', 'AppControllers\PracticesController@listForRegister');
Route::any('practices/create', 'AppControllers\PracticesController@create');
Route::any('practices/addzone', 'AppControllers\PracticesController@addZone');
Route::any('practices/addholiday', 'AppControllers\PracticesController@addHoliday');
Route::any('practices/addpaycode', 'AppControllers\PracticesController@addPaycode');
Route::any('practices/listzone', 'AppControllers\PracticesController@listZone');
Route::any('practices/listholiday', 'AppControllers\PracticesController@listHoliday');
Route::any('practices/listpaycode', 'AppControllers\PracticesController@listPaycode');
Route::any('practices/uploadlogo', 'AppControllers\PracticesController@uploadLogo');

Route::any('practices/getusers', 'AppControllers\PracticesController@getUsers');
Route::any('practices/getzones', 'AppControllers\PracticesController@getZones');

Route::any('practices/setup', 'AppControllers\PracticesController@listPractices');
Route::any('practices/save', 'AppControllers\PracticesController@save');
Route::any('practices/addpracticesinsurance', 'AppControllers\PracticesController@addPracticesInsurance');
Route::any('practices/saveaddpayors', 'AppControllers\PracticesController@saveAddPayors');
Route::any('practices/listpayors', 'AppControllers\PracticesController@listPayors');
Route::any('practices/savepayors', 'AppControllers\PracticesController@savePayors');
Route::any('practices/listsessionrule', 'AppControllers\PracticesController@listSessionRule');
Route::any('practices/sessionrule', 'AppControllers\PracticesController@addSessionRule');
Route::any('employee/save', 'AppControllers\PhysiciansController@save');
Route::any('practices/saveusers', 'AppControllers\PracticesController@saveUsers');
Route::any('practices/updateuser', 'RegisterController@updateUser');
Route::any('practices/listrole', 'AppControllers\RoleController@list');
Route::any('practices/saverole', 'AppControllers\RoleController@save');


//Handler

Route::any('handler/dashboard', 'AppControllers\HandlerController@dashboard');
Route::any('handler/insurances', 'AppControllers\HandlerController@insurances');
Route::any('handler/activity', 'AppControllers\HandlerController@activity');
Route::any('handler/sessions', 'AppControllers\HandlerController@sessions');
Route::any('handler/cpts', 'AppControllers\HandlerController@cpts');
Route::any('handler/users', 'AppControllers\HandlerController@users');
Route::any('handler/reports', 'AppControllers\HandlerController@reports');

Route::any('handler/insurances/add', 'AppControllers\HandlerController@addInsurance');
Route::any('handler/activity/add', 'AppControllers\HandlerController@addActivity');
Route::any('handler/sessionrule/add', 'AppControllers\HandlerController@addSessionRule');
Route::any('handler/user/add', 'AppControllers\HandlerController@addUser');
Route::any('handler/codes/add', 'AppControllers\HandlerController@addCodes');
Route::any('handler/get', 'AppControllers\HandlerController@getList');

Route::any('practices/addpayperiods', 'AppControllers\PracticesController@addPayPeriods');

Route::any('emr', 'AppControllers\EmrController@emr');

Route::any('handler/insurances/add', 'AppControllers\HandlerController@save1');

Route::any('appointments/recurrence', 'AppControllers\AppointmentsController@getPlannerDetails');
