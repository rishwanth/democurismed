@extends('layout.app')
@section('content')
<?php
//echo '<pre>';print_r($data['benifitsList']);die();

$countSavedPaycodes = count($data['savedPaycodes']['data']);
$countSavedWorkhistory = count($data['savedWorkhistory']['data']);
$id = isset($data['edit_row']) ? $data['edit_row']['id'] : 0;
$row = isset($data['edit_row']) ? $data['edit_row'] : '';

foreach ($data['fieldsList'] as $key => $value) {
    if ($value == 'id') {
        continue;
    }
    $$value = '';
    if ($id > 0) {
        $$value = $row[$value];
        if ($value == 'dob'){
            $$value = date('m-d-Y', strtotime($row[$value]));
        } else if ($value == 'date_of_hire'){
            $$value = date('m-d-Y', strtotime($row[$value]));
        } else if ($value == 'driver_license_expiration'){
            $$value = date('m-d-Y', strtotime($row[$value]));
        } else if ($value == 'auto_insurance_expiration'){
            $$value = date('m-d-Y', strtotime($row[$value]));
        } else if ($value == 'background_check_expiration'){
            $$value = date('m-d-Y', strtotime($row[$value]));
        }
    }
}
?>
<div class="col-12">
    <div class="card ">
        
        <div class="card-body">
            <div class="stepwizard">
                <div class="stepwizard-row setup-card">
                    <div class="stepwizard-step">
                        <a href="#step-1" id="up1" class="btn btn-primary btn-block">Payroll Setup</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" id="up2" class="btn btn-default btn-block">Employee Address</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" id="up3" class="btn btn-default btn-block">Other Info</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-4" id="up4" class="btn btn-default btn-block">Work History</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-5" id="up5" class="btn btn-default btn-block">Pay Codes</a>
                    </div>

                </div>
            </div>

<form role="form">
<input type="hidden" id="_token" name="_token"
       value="<?php echo csrf_token(); ?>">
<input type="hidden" id="id" name="id"
       value="<?php echo $id; ?>">
<input type="hidden" id="countSavedPaycodes" 
        name="countSavedPaycodes"
       value="<?php echo $countSavedPaycodes; ?>">

<input type="hidden" id="countSavedWorkhistory" 
    name="countSavedWorkhistory"
       value="<?php echo $countSavedWorkhistory; ?>">

<div class="row setup-content" id="step-1">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnChkPat" />
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="name" name="name" placeholder="Employee Name" 
                                            value="{{$name}}"/>
                                        </div>

                                        <div class="col-md-4">
                                            <select class="form-control" id="speciality" name="speciality">
                                        @foreach($data['specialityList'] as $key => $value)
                            <option value="{{$key}}" <?php if ($key==$speciality) echo ' selected'; ?>>{{$value}}</option>
                                        @endforeach
                                            </select>
                                       </div>

                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="employee_type" name="employee_type">
                                                <option value="">--Employee Type--</option>
                                                <option value="Full Time">Full Time</option>
                                                <option value="Part Time">Part Time</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="date form-control validateGeneral" id="date_of_hire" name="date_of_hire" 
                                            placeholder="Date Of Hire"
                                            value="{{$date_of_hire}}" />
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="date form-control validateGeneral" id="date_of_birth" name="date_of_birth" 
                                            value="{{$dob}}"
                                            placeholder="Date Of Birth" />
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row" >
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral"  id="employee_position" 
                                            name="employee_position" 
                                            value="{{$employee_position}}"

                                            placeholder="Employee Position" 
                                             />
                                        </div>

                                        <div class="col-md-4">
                                                <input type="text" class="form-control validateGeneral"  id="individual_npi" name="individual_npi" placeholder="Individual NPI of the Primary Employee" 
                                                value="{{$individual_npi}}" />
                                            </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="ssno" name="ssno"
                                            value="{{$ssno}}"
                                            data-inputmask1='"mask": "(999) 999-9999"' placeholder="Social Security #" data-mask1 />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="has_benifits" name="has_benifits">
                                    @foreach($data['benifitsList']['data'] as $key => $value)
                            <option value="{{$key}}" 
                            <?php if ($key==$has_benifits) echo ' selected'; ?>>{{$value}}</option>
                                        @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="payroll_company" 
                                            name="payroll_company"
                                            value="{{$payroll_company}}"
                                            placeholder="Payroll Company"  />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="payroll_employee_num" 
                                            name="payroll_employee_num"
                                            placeholder="Payroll Employee #" 
                                    value="{{$payroll_employee_num}}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="workers_comp_num"
                                            name="workers_comp_num"
                                            placeholder="Workers Comp #" 
                                    value="{{$workers_comp_num}}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="department_name" name="department_name" placeholder="Department" 
                            value="{{$department_name}}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="pay_code" name="pay_code"
                                            placeholder="Pay Code" 
                                    value="{{$pay_code}}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                        <textarea class="form-control validateGeneral" rows="5"
                                        cols="80" name="notes"
                                        id="notes" 
                                placeholder="Notes">{{$notes}}</textarea>
                                        </div>
                                    </div>
                                </div>

                        <div class="col-sm-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-md-8 float-right">
                                    <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNext1"/>
                            </div>
                        </div>
                    </div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- step-2 -->

<div class="row setup-content" id="step-2">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="address_1" name="address_1" placeholder="Address Line 1"
                                            value="{{$address_1}}" />
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="address_2" name="address_2" placeholder="Address Line 2"
                                    value="{{$address_2}}" />
                                            <br/>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="city" name="city" 
                                            placeholder="City" 
                                        value="{{$city}}" />
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral"  id="region" name="region" placeholder="Region" 
                                            value="{{$region}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="country_id" name="country_id">
                                        @foreach($data['countryList'] as $key => $value)
            <option value="{{$key}}"
            <?php if ($value==$country_name) echo ' selected'; ?>>{{$value}}</option>
                                        @endforeach
                                            </select>
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="zip" name="zip" placeholder="Zip / Postal Code" value="{{$zip}}" />
                                            <br/>
                                        </div>

                                    </div>
                                </div>                                


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="phone_home" name="phone_home" placeholder="Phone Home" 
                                            value="{{$phone_home}}" />
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="phone_work" name="phone_work" placeholder="Phone Work" 
                                            value="{{$phone_work}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="phone_mobile" name="phone_mobile" placeholder="Mobile Number" 
                                            value="{{$phone_mobile}}" />
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="fax" name="fax" placeholder="Fax" 
                                            value="{{$fax}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                            <input type="email" class="form-control required" 
                                            id="email" name="email" placeholder="Email" 
                                            value="{{$phy_email}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>


                        <div class="col-sm-12 col-md-12">
                        <div class="form-group row">
                        <div class="col-md-8">

                                        <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNext21" name="btnNext21" />

                        </div>
                        </div>
                        </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- step-3 -->

<div class="row setup-content" id="step-3">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnChkPat" />
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="driver_license_number" 
                                            name="driver_license_number" placeholder="Driver License Number" 
                                    value="{{$driver_license_number}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control date validateGeneral" 
                                            id="driver_license_expiration" 
                                            name="driver_license_expiration" 
                                            placeholder="Driver License Expiration" 
                                    value="{{$driver_license_expiration}}"/>
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row" >
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral"  
                                            id="auto_insurance_number" 
                                            name="auto_insurance_number" 
                                            placeholder="Auto Insurance Number" 
                                    value="{{$auto_insurance_number}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class=" date form-control required"
                                            id="auto_insurance_expiration" 
                                            name="auto_insurance_expiration" 
                                            placeholder="Auto Insurance Expiration" 
                                        value="{{$auto_insurance_expiration}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" 
                                            id="educational_credential" 
                                            name="educational_credential" 
                                            placeholder="Educational Credential" 
                                        value="{{$educational_credential}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class=" date form-control validateGeneral " 
                                            id="tb_clearance_expiration" 
                                            name="tb_clearance_expiration" 
                                            placeholder="TB Clearance Expiration" 
                                        value="{{@tb_clearance_expiration}}" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="text" class="date form-control validateGeneral " 
                                id="medical_clearance_expiration" 
                                name="medical_clearance_expiration" 
                                placeholder="Medical Clearance Expiration"
                        value="{{$medical_clearance_expiration}}" />
                                <br/>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="text" class="form-control validateGeneral" 
                                id="background_check_completion" 
                                name="background_check_completion" 
                                placeholder="Background Check Completion" 
                        value="{{$background_check_completion}}" />
                                <br/>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="text" class="date form-control validateGeneral" 
                                id="background_check_expiration" 
                                name="background_check_expiration" 
                                placeholder="Background Check Expiration" 
                        value="{{$background_check_expiration}}" />
                                <br/>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                    <div class="form-group row">
                    <div class="col-md-4">
                                           
                        <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNext3"/>
                    </div>
                    </div>
                    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<!-- step-4 -->
<div class="row setup-content" id="step-4">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnChkPat" />
                            <div class="row">


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-primary" id="addworkhistory">+ New Entry</button>
                                        </div>

                                    </div>
                                </div>

                <!-- workhistory list -->
                <div class="col-sm-12 col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <table class="table table-border" id="tblworkshistory" width="100%">
                                <tr>
                                    <th width="20%">Start Date</th>
                                    <th width="20%">End Date</th>
                                    <th width="40%">Notes</th>
                                    <th width="10%">Delete</th>
                                </tr>
                                <?php
                                loadSavedWorkhistory($data['savedWorkhistory']['data']);
                                ?>
                            </table>
                </div>
                </div>
                </div>




                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">

                                        <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNext4"/>
                                </div>
                                </div>
                                </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- end of step-4 -->

<!-- step-5 -->
<div class="row setup-content" id="step-5">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnChkPat" />
                            <div class="row">

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select class="form-control" 
                                            id="paycode_id" 
                                            name="paycode_id">
                                        @foreach($data['paycodeList']['data'] as $key => $value)
                            <option value="{{$value['id']}}">{{$value['name']}}</option>
                                        @endforeach

                                            </select>
                                            <br/>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-primary" id="addpaycode">Add</button>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="job_title" 
                                            name="job_title" 
                                            placeholder="Job title" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                <!-- paycode added list -->
                <div class="col-sm-12 col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <table class="table" id="tblpaycode" width="100%">
                                <tr>
                                    <th width="40%">Paycode Name</th>
                                    <th width="15%">Billable</th>
                                    <th width="15%">Mileage Rate</th>
                                    <th width="15%">Hourly Rate</th>
                                    <th width="15%">Delete</th>
                                </tr>
                                <?php
                                loadSavedPaycodes($data['savedPaycodes']['data']);
                                ?>
                            </table>
                </div>
                </div>
                </div>


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">

                                            <br/>

                                        <input class="btn btn-primary nextBtn float-right" type="button" value="Save" id="btnNext5"/>
                                </div>
                                </div>
                                </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- end of step-5 -->


</form>
        
        </div>
    </div>
</div>

<?php
function loadSavedPaycodes($rows){
    $i = 0;
    foreach ($rows as $row1) {
        $row = (array)$row1;
        $i++;
        echo '<tr>';
            echo '<td width="60%">';
            $id = 'paycode_tb_id_' . $i ;
            $c = '';
            $c .= '<input type="hidden" name="' . $id . '" id="' . $id . '" ';
            $c .= ' value="' . $row['id'] . '" />';
            echo $c;

            $paycode_id = $row['paycode_id'];
            $id = 'paycode_id_' . $i ;
            $c = '<input type="hidden" name="' . $id . '" id="' . $id . '" ';
            $c .= ' value="' . $paycode_id . '" />';
            echo $c;

            $paycode_name = $row['name'];
            echo $paycode_name;
            echo '</td>';

            echo '<td width="15%">';
            $id = 'paycode_billable_' . $i ;
            $c = '<select class="form-control" id="' . $id . '" ';
            $c .= ' name="' . $id . '">';
            $checked = '';
            if ($row['billable'] == 'Yes'){
                $checked = ' selected ';
            }
            $c .= '<option value="Yes"' . $checked . '>Yes</option>'; 
            $checked = '';
            if ($row['billable'] == 'No'){
                $checked = ' selected ';
            }
            $c .= '<option value="No"' . $checked . '>No</option>'; 
            $c .= '</select>';

            echo $c;
            echo '</td>';

            $id = 'paycode_mileage_' . $i ;
            echo '<td width="15%">';
            $c = '<input type="text" class="form-control" name="' . $id .'" id="' . $id . '" value="' . $row['mileage'] . '" />';
            echo $c;
            echo '</td>';

            echo  '<td width="15%">';
            $id = 'paycode_rate_' . $i ;
            $c = '<input type="text" class="form-control" name="' . $id .'" id="' . $id . '" value="' . $row['rate'] . '" />';
            echo $c;
            echo '</td>';

            $id = 'paycode_delete_' . $i ;
            $c = '<input type="checkbox" class="form-control"  id="' . $id . '" ' . ' name="' . $id . '" value="1" />';
            echo '<td width="15%">';
            echo $c;
            echo '</td>';

        echo '</tr>';
    }
}

function loadSavedWorkhistory($rows){
    $i = 0;
    foreach ($rows as $row) {
        $i++;
        echo '<tr>';

        $id = 'workhistory_tb_id_' . $i ;
        $c = '<input type="hidden" name="' . $id . '" id="' . $id . '" ';
        $c .= ' value="' . $row['id'] . '" />';

        $id = 'workhistory_start_date_' . $i ;
        $d = date('m-d-Y', strtotime($row['start_date']));
        $c .= '<input type="text" class="form-control date" name="' . $id . '" id="' . $id . '" value="' . $d . '"  />';

        //start date
        echo '<td width="20%" >';
        echo $c;
        echo '</td>';

        //end date
        $id = 'workhistory_end_date_' . $i ;
        $d = date('m-d-Y', strtotime($row['end_date']));
        $c =  '<input type="text" class="form-control date" name="' . $id . '" id="' . $id . '" value="' . $d . '" />';

        echo '<td width="20%" >';
        echo $c;
        echo '</td>';

        //notes
        $id = 'workhistory_notes_' . $i ;
        $c =  '<input type="text" class="form-control" name="' . $id . '" id="' . $id . '" value="' . $row['notes'] . '"  />';
        echo '<td width="50%">';
        echo $c;
        echo '</td>';

        //delete
        $id = 'workhistory_delete_' . $i ;
        $c = '<input type="checkbox" class="form-control"  id="' . $id . '" name="' . $id . '" value="1" />';
        echo '<td width="10%">';
        echo $c;
        echo '</td>';
        echo '</tr>';
    }
   
}

?>

<style type="text/css">
#step-1 .form-group{
    margin-bottom: 1rem;
}
</style>

@stop
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script type="text/javascript">
    var countPaycodes = 0;
    var countWorkhistory = 0;
$(document).ready(function(){
    countPaycodes = parseInt($('#countSavedPaycodes').val());
    countWorkhistory = parseInt($('#countSavedWorkhistory').val());

    $('#btnNext1').click(function(){
        displayStep(2);
    });
    $('#btnNext21').click(function(){
        displayStep(3);
    });
    $('#btnNext3').click(function(){
        displayStep(4);
    });
    $('#btnNext4').click(function(){
        displayStep(5);
    });
    $('#btnNext5').click(function(){
        save();
    });

    $('#addpaycode').click(function(){
        addRowPayCode();
    });

    $('#addworkhistory').click(function(){
        addRowWorkHistory();
    });

    initialize();
});

function initialize(){
    $('.date').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        format: 'm-d-Y',
        timepicker: false,
        //minDate: '-2013/01/02',
        //maxDate: '+2014/12/31',
        formatDate: 'm-d-Y',
        closeOnDateSelect: true
    });
}

function getSelectedPaycodes(){
    var i;
    var t;
    var c;
    var val;
    var id;

    var data = [];
    for (i=1; i<=countPaycodes; i++){
        var obj = {};
        c = 'paycode_tb_id_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.id = val;

        c = 'paycode_id_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.paycode_id = val;

        c = 'paycode_billable_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.billable = val;

        c = 'paycode_mileage_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.mileage = val;

        c = 'paycode_rate_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.rate = val;

        c = 'paycode_delete_' + i;
        id = '#'+c;
        val = 0;
        if ($(id).prop('checked')==true){ 
            val = 1;
        }        
        obj.delete = val;
        data[i-1] = obj;
    }
    return data;
}

function getAddedWorkhistory(){
    var i;
    var t;
    var c;
    var val;
    var id;

    var data = [];
    for (i=1; i<=countWorkhistory; i++){
        var obj = {};
        c = 'workhistory_tb_id_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.id = val;

        c = 'workhistory_start_date_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.start_date = convertDate(val);

        c = 'workhistory_end_date_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.end_date = convertDate(val);

        c = 'workhistory_notes_' + i;
        id = '#'+c;
        val = $(id).val();
        obj.notes = val;

        c = 'workhistory_delete_' + i;
        id = '#'+c;
        val = 0;
        if ($(id).prop('checked')==true){ 
            val = 1;
        }        
        obj.delete = val;
        data[i-1] = obj;
    }
    return data;
}


function save(){
    var root_url = $("#hidden_root_url").val();
    $('#loader').show();
    var id = $('#id').val();
    var name = $('#name').val();
    if (name.length < 2) {
        alert('Invalid Employee Name')
        return;
    }
    var paycodes = getSelectedPaycodes();
    var workhistory = getAddedWorkhistory();

    var speciality = $("#speciality").val();
    var employee_type = $("#employee_type").val();
    var employee_position = $("#employee_position").val();
    var date_of_birth = convertDate($('#date_of_birth').val());
    var date_of_hire = convertDate($('#date_of_hire').val());
    var individual_npi = $('#individual_npi').val();
    var ssno = $('#ssno').val();
    var has_benifits = $('#has_benifits').val();
    var payroll_company = $('#payroll_company').val();
    var payroll_employee_num = $('#payroll_employee_num').val();
    var workers_comp_num = $('#workers_comp_num').val();
    var department_name = $('#department_name').val();
    var pay_code = $('#pay_code').val();
    var notes = $('#notes').val();
    var address_1 = $('#address_1').val();
    var address_2 = $('#address_2').val();
    var city = $('#city').val();
    var region = $('#region').val();
    var zip = $('#zip').val();
    var phone_home = $('#phone_home').val();
    var phone_work = $('#phone_work').val();
    var phone_mobile = $('#phone_mobile').val();
    var email = $('#email').val();
    var fax = $('#fax').val();
    var driver_license_number = $('#driver_license_number').val();
    var driver_license_expiration = convertDate($('#driver_license_expiration').val());
    var auto_insurance_number = $('#auto_insurance_number').val();
    var auto_insurance_expiration = convertDate($('#auto_insurance_expiration').val());
    var educational_credential = $('#educational_credential').val();
    var tb_clearance_expiration = convertDate($('#tb_clearance_expiration').val());
    var medical_clearance_expiration = convertDate($('medical_clearance_expiration').val());
    var background_check_completion = $('#background_check_completion').val();
    var background_check_expiration = convertDate($('background_check_expiration').val());

    var country_id = $('#country_id').val();
    var country_name = $('#country_id').val();
    var _token = $('#_token').val();

    $.ajax({
        type: "POST",
        url:root_url + "physician/addnew",
        data:{
            "_token" : _token,
            "id" : id,
            "physicianID" : id,
            "name" : name,
            "speciality" : speciality,
            "employee_type" : employee_type,
            "employee_position" : employee_position,
            "date_of_birth" : date_of_birth,
            "date_of_hire" : date_of_hire,
            "individual_npi" : individual_npi,
            "ssno" : ssno,
            "has_benifits": has_benifits,
            "payroll_company": payroll_company,
            "payroll_employee_num": payroll_employee_num,
            "workers_comp_num" : workers_comp_num,
            "department_name" : department_name,
            "pay_code" : pay_code,
            "notes" : notes,
            "address_1" : address_1,
            "address_2" : address_2,
            "city" : city,
            "region" : region,
            "zip" : zip,
            "phone_home" : phone_home,
            "phone_work" : phone_work,
            "phone_mobile" : phone_mobile,
            "email" : email,
            "fax" : fax,
            "driver_license_number" : driver_license_number,
            "driver_license_expiration" : driver_license_expiration,
            "auto_insurance_number" : auto_insurance_number,
            "auto_insurance_expiration" : auto_insurance_expiration,
            "educational_credential" : educational_credential,
            "tb_clearance_expiration" : tb_clearance_expiration,
            "medical_clearance_expiration" : medical_clearance_expiration,
            "background_check_completion" : background_check_completion,
            "country_id" : country_id,
            "country_name" : country_name,
            "paycodes" : paycodes,
            "workhistory": workhistory,
        },success:function(result) {
            $('#loader').hide();
            console.log(result);
            var res = JSON.parse(result);
            if(res.status == 0){
                alert(res.message);
                return;
            }  else {
                alert(res.message);
                window.location.href = root_url + "physician/list"; 
            }
        }
    });

}

function displayStep(stepNum){
    for (i=1; i<=5; i++){
        step = '#step-' + i;
        up = '#up' + i;
        if (stepNum == i){
            $(step).css({"display":"flex"});
            $(up).addClass("btn-primary");
        } else {
            $(step).css({"display":"none"});
            $(up).removeClass("btn-primary");
        }
    }
}

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

var convertDate = function(usDate) {
    if(usDate != "" && usDate != null && usDate != undefined){
        var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
        return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }
}

function addRowPayCode(){
    var paycode_id = parseInt($('#paycode_id').val());
    if (paycode_id < 1){
        alert('Please select the valid pay code');
        return;
    }
    var paycode_name = $("#paycode_id").find("option:selected").text();
    var newRow = $("<tr>");
    var cols = "";
    var c = '';
    var id = '';
    countPaycodes = countPaycodes + 1;
    
    id = 'paycode_tb_id_' + countPaycodes ;
    c = '';
    c = c + '<input type="hidden" name="' + id + '" id="' + id + '" ';
    c = c + ' value="0" />';

    id = 'paycode_id_' + countPaycodes ;
    c = c + '<input type="hidden" name="' + id + '" id="' + id + '" ';
    c = c + ' value="' + paycode_id + '" />';

    cols += '<td width="60%">' + c + paycode_name + '</td>';

    id = 'paycode_billable_' + countPaycodes ;
    cols += '<td width="15%">';
    //<input type="text" class="form-control" name="' + id +'" id="' + id + '" />';
    c = '<select class="form-control" id="' + id + '" name="' + id +'">';
    c = c + '<option value="Yes">Yes</option>'; 
    c = c + '<option value="No">No</option>'; 
    c = c + '</select>';
    cols += c;
    cols += '</td>';

    id = 'paycode_mileage_' + countPaycodes ;
    cols += '<td width="15%"><input type="text" class="form-control" name="' + id +'" id="' + id + '" /></td>';

    id = 'paycode_rate_' + countPaycodes ;
    cols += '<td width="15%"><input type="text" class="form-control" name="' + id +'" id="' + id + '" /></td>';

    id = 'paycode_delete_' + countPaycodes ;
    c = '<input type="checkbox" class="form-control"  id="' + id + '" ';
    c = c + ' name="' + id + '" value="1" />';

    cols += '<td width="15%">' + c + '</td>';

    newRow.append(cols);
    $("#tblpaycode").append(newRow);
    $('#paycode_id').val(-1);
}

function addRowWorkHistory(){
    var newRow = $("<tr class='col-md-12'>");
    var cols = "";
    var c = '';
    var c1 = '';
    var id = '';
    countWorkhistory = countWorkhistory + 1;
    
    id = 'workhistory_tb_id_' + countWorkhistory ;
    c = '<input type="hidden" name="' + id + '" id="' + id + '" ';
    c = c + ' value="0" />';

    id = 'workhistory_start_date_' + countWorkhistory ;
    c = c + '<input type="text" class="form-control date" name="' + id + '" id="' + id + '"  />';

    //start date
    cols += '<td width="20%" >' + c + '</td>';

    //end date
    id = 'workhistory_end_date_' + countWorkhistory ;
    c =  '<input type="text" class="form-control date" name="' + id + '" id="' + id + '"  />';
    cols += '<td width="20%" >' + c + '</td>';

    //notes
    id = 'workhistory_notes_' + countWorkhistory ;
    c =  '<input type="text" class="form-control" name="' + id + '" id="' + id + '"  />';
    cols += '<td width="50%">' + c + '</td>';

    //delete
    id = 'workhistory_delete_' + countWorkhistory ;
    c = '<input type="checkbox" class="form-control"  id="' + id + '" name="' + id + '" value="1" />';
    cols += '<td width="10%">' + c + '</td>';

    newRow.append(cols);
    $("#tblworkshistory").append(newRow);
    initialize();
}

</script>