@extends('layout.app')
@section('content')


<div class="row">
    <h2 style="color: #251367; margin-left:20px;">Insurances List</h2>
    <div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">
            <a href="javascript:void(0)" class="btn btn-primary" id="linkNewIns"><i class="icon icon-plus"></i>&nbsp;Add New Insurance</a><br/><br/>
                <table id="insuranceList" class="table display" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
                </table>
        </div>
    </div>
</div>


<div class="modal fade" id="AddInsurance" tabindex="-1">

    <div class="modal-dialog" style="width:60%">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title" id="lblTitle">Add New Insurance</h4>

            </div>

            <div class="modal-body" style="min-height:250px;">

                <input type="hidden" id="hdnFlag"/>
                <div class="card-body">
                    <div class="row">

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Payer Name </label>

                            <div class="col-md-12">

                                <input type="text" id="payerName" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Clearing House ID </label>

                            <div class="col-md-12">

                                <input type="text" id="clearingHousePayorID" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Address Street 1 </label>

                            <div class="col-md-12">

                                <input type="text" id="addrStrt1" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Address Street 2 </label>

                            <div class="col-md-12">

                                <input type="text" id="addrStrt2" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">City </label>

                            <div class="col-md-12">

                                <input type="text" id="addrCity" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">State </label>

                            <div class="col-md-12">

                                <input type="text" id="addrState" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label class="control-label col-md-12">Zipcode </label>

                            <div class="col-md-12">

                                <input type="text" id="addrZip" class="form-control" />

                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>

            <div class="modal-footer">

                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />

                <input type="button" id="btnSaveInsurance" class="btn btn-primary" data-dismiss="modal" value="Save" />

            </div>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div>

@stop

    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/alertify.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function(){

            var practiceId = sessionStorage.getItem('practiceId');

            $('#linkNewIns').click(function(){

                $('#AddInsurance').modal('show');

                document.getElementById("hdnFlag").value = 0;

                document.getElementById('btnSaveInsurance').value = "Save Insurance";

                document.getElementById('lblTitle').innerHTML = "Add Insurance";

                document.getElementById('payerName').value = "";

                document.getElementById('clearingHousePayorID').value = "";

                document.getElementById('addrStrt1').value = "";

                document.getElementById('addrCity').value = "";

                document.getElementById('addrState').value = "";

                document.getElementById('addrZip').value = "";

            })

            $(document).on('click','.editServLoc',function() {

                var temp = $(this).attr('class').split(' ')[0];

                var insuranceID = temp.replace('edit','');

                document.getElementById("hdnFlag").value = insuranceID;

                $.ajax({

                    type: "POST",

                    url: root_url+"insurances/get",

                    async : false,

                    data:{

                        insuranceID : insuranceID

                    },success:function(result){

                        $('#AddInsurance').modal('show');

                        var res = JSON.parse(result);

                        document.getElementById('btnSaveInsurance').value = "Update Insurance";

                        document.getElementById('lblTitle').innerHTML = "Edit Insurance";

                        document.getElementById('payerName').value = res[0].payerName;

                        document.getElementById('clearingHousePayorID').value = res[0].clearingHousePayorID;

                        document.getElementById('addrStrt1').value = res[0].addr;

                        document.getElementById('addrCity').value = res[0].city;

                        document.getElementById('addrState').value = res[0].state;

                        document.getElementById('addrZip').value = res[0].zip;

                    }

                });

            });

            var ins_url = rool_url + "insurances/get";
            $.post(ins_url,

                {

                    practiceID: practiceId

                },

                function(data1, status){

                    var dt = [];

                    data1 = JSON.parse(data1);

                    $.each(data1,function(i,v) {

                        dt.push([data1[i].insuranceID,data1[i].payerName,data1[i].clearingHousePayorID,data1[i].addr,data1[i].insuranceID]);

                    });

                    //alert(dt);

                    var table = $('#insuranceList').DataTable({

                        "data": dt,

                        "bProcessing": true,

                        "aoColumns": [

                            {"mdata": "insuranceID","title":"ID", "visible": false},

                            {"title":"Payer Name","mdata": "payerName"},

                            {"title":"Clearing House ID","mdata": "clearingHousePayorID", "width":"15%"},

                            {"title":"Address","mdata": "addr"},

                            {"title":"Action",

                                "render": function ( data, type, full, meta ) {

                                    return '<a href="javascript:void(0);" class="edit'+data+' editServLoc"><span class="ti-pencil"></span></a>';

                                }

                            }

                        ]

                    });


                });

            $('#btnSaveInsurance').click(function(){

                document.getElementById('btnSaveInsurance').value = "Add Insurance";

                document.getElementById('lblTitle').innerHTML = "Add New Insurance";

                var hdnFlag = document.getElementById("hdnFlag").value;

                if(hdnFlag == 0){

                    var payerName = $('#payerName').val();

                    var clearingHousePayorID = $('#clearingHousePayorID').val();

                    var addrStrt1 = $('#addrStrt1').val()+ ",";

                    if($('#addrStrt2').val() == ""){

                        var addrStrt2 = "";

                    }

                    else{

                        var addrStrt2 = $('#addrStrt2').val()+ ",";

                    }



                    var addrCity = $('#addrCity').val()+ " ";

                    var addrState = $('#addrState').val()+ " ";

                    var addrZip = $('#addrZip').val();

                    var addr = addrStrt1+addrStrt2+addrCity+addrState+addrZip;

                    var practiceID = sessionStorage.getItem("practiceId");

                    $.ajax({

                        type: "POST",

                        url: root_url+"insurances/add",

                        data:{

                            "payerName" : payerName,

                            "clearingHousePayorID" : clearingHousePayorID,

                            "addr" : addr,

                            "practiceID" : practiceID,

                        },success:function(result){
                            var res = JSON.parse(result);
                            new PNotify({
                                type:'success',
                                text: json.message,
                                after_init: function(notice){
                                    notice.attention('rubberBand');
                                }
                            })

                        }

                    });

                }

                else{

                    var payerName = $('#payerName').val();

                    var clearingHousePayorID = $('#clearingHousePayorID').val();

                    var addrStrt1 = $('#addrStrt1').val()+ ",";

                    if($('#addrStrt2').val() == ""){

                        var addrStrt2 = "";

                    }

                    else{

                        var addrStrt2 = $('#addrStrt2').val()+ ",";

                    }



                    var addrCity = $('#addrCity').val()+ " ";

                    var addrState = $('#addrState').val()+ " ";

                    var addrZip = $('#addrZip').val();

                    var addr = addrStrt1+addrStrt2+addrCity+addrState+addrZip;

                    var practiceID = sessionStorage.getItem("practiceId");

                    var insuranceID = document.getElementById("hdnFlag").value;

                    $.ajax({

                        type: "POST",

                        url: root_url+"insurances/update",

                        data:{

                            "insuranceID" : insuranceID,

                            "payerName" : payerName,

                            "clearingHousePayorID" : clearingHousePayorID,

                            "addr" : addr,

                            "practiceID" : practiceID,

                        },success:function(result){

                            var res = JSON.parse(result);
                            new PNotify({
                                type:'success',
                                text: json.message,
                                after_init: function(notice){
                                    notice.attention('rubberBand');
                                }
                            })

                        }

                    });

                }

            });

        });

    </script>