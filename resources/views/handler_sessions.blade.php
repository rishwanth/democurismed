@extends('layout.handler')
@section('content')
<?php
    $base_url = URL::to('/');
?>
<div class="row" style="margin: 5%;">
    <!-- <div class="card" style="width: 100%;">
        <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                   Add Sessions to the Facility
                </a>
            </h5>
        </div>
        <div id="collapseThree" class="collapse show" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion" style="padding:20px;">
            <div class="card-body">
                <select multiple="multiple" size="10" name="duallistbox_demo1[]">
                  <option value="option1">AETNA</option>
                  <option value="option2">MEDICARE</option>
                  <option value="option3" selected="selected">MEDICAID</option>
                  <option value="option4">CIGNA</option>
                  <option value="option5">TRICARE</option>
                  <option value="option6" selected="selected">UHC</option>
                  <option value="option7">Optum</option>
                  <option value="option8">Healthnet</option>
                  <option value="option9">Anthem</option>
                  <option value="option0">BCBS</option>
                </select>
            </div>
        </div>
    </div> -->


    <div class="card" style="width: 100%;">
        <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                   Add Sessions to the Facility
                </a>
            </h5>
        </div>
        <div id="collapseThree" class="collapse show" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion" style="padding:20px;">
            <div class="card-body">
                <div class="col-sm-12">
                    <div class="form-group row" style="margin-bottom: 1%">
                        <div class="col-4">
                            <label for="input-text" class="control-label float-right txt_media1">Session Rule Name</label>
                        </div>
                        <div class="col-8">
                            <input type="text" class="col-md-6 form-control" id="txtSessionName"/>
                        </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: 1%">
                        <div class="col-4">
                            <label for="input-text" class="control-label float-right txt_media1">Session Rule Desc</label>
                        </div>
                        <div class="col-8">
                            <input type="text" class="col-md-6 form-control" id="txtSessionDesc"/>
                        </div>
                    </div>
                   
                </div>
                <div class="col-sm-12">
                    <table id="getSessions" class="table table-bordered"></table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){
        //document.getElementById("lblUser").innerHTML = {{Session::get('LOGGED_USER_NAME')}};
        var root_url = $("#hidden_root_url").val();
        //alert('i am here layout=>app.blade.php' + root_url);
        
       

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })
    });
</script>
