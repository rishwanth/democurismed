<!DOCTYPE html>
<html>
<head>
    <title>Login | AMROMED LLC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ URL::to('/') }}/public/img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="{{ URL::to('/') }}/public/css/bootstrap.css" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="{{ URL::to('/') }}/public/vendors/themify/css/themify-icons.css" rel="stylesheet"/>
    <link href="{{ URL::to('/') }}/public/vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet"/>
    <link href="{{ URL::to('/') }}/public/css/login.css" rel="stylesheet">
    <style type="text/css">
        body{
            color:#000;
        }
    </style>
    <!--end page level css-->
</head>

<body id="sign-in">
<div class="preloader">
    <div class="loader_img"><img src="{{ URL::to('/') }}/public/img/loader.gif" alt="loading..." height="64" width="64"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-10 col-sm-8 m-auto login-form">

                <h2 class="text-center logo_h2">
                    <img src="{{ URL::to('/') }}/public/img/pages/clear_black.png" alt="Logo">
                </h2>

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form id="authentication" method="post" class="login_validator">
                            <div class="form-group">
                                <input type="hidden" id="_token" name="_token"
                                  value="{!! csrf_token() !!}" />
                                <input type="hidden" id="reset_token" name="reset_token"
                                  value="{{ $users->reset_token }}" />
                                <input type="hidden" id="root_url" name="root_url"
                                  value="{{ URL::to('/') }}" />

                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="password1" name="password1"
                                       placeholder="Enter the new password" required=>
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="password2" name="password2"
                                       placeholder="Enter the new password" required=>
                            </div>

                            <div class="form-group">
                                <img src="{{ URL::to('/') }}/public/img/pages/arrow-right.png" id="btnSubmit" style="margin:0 50%; background-color: chocolate;border-radius: 15px;cursor: pointer;" alt="Go" width="30" height="30"> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="{{ URL::to('/') }}/public/js/jquery.min.js" type="text/javascript"></script>

<script src="{{ URL::to('/') }}/public/js/popper.min.js" type="text/javascript"></script>
<script src="{{ URL::to('/') }}/public/js/bootstrap.min.js" type="text/javascript"></script>

<!-- end of global js -->
<!-- page level js -->

<script src="{{ URL::to('/') }}/public/vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/public/js/custom_js/login.js"></script>

<script>

$(document).ready(function(){

	$('#btnSubmit').click(function(){
        var _token = $('#_token').val();
        var reset_token = $('#reset_token').val();
        var password1 = $('#password1').val();
        var password2 = $('#password2').val();
		$.ajax({
            type: "POST",
            url:"{{ URL::to('user/resetpasswordsave') }}",
            data:{ 
                "_token" : _token,
                "reset_token" : reset_token,
                "password1" : password1,
                "password2" : password2,
                },success:function(result){
                    var json =JSON.parse(result);
                    var root_url = $('#root_url').val();
                    if (json.status == 0){
                        alert(json.message);
                    } else {
                        alert(json.message);
                        window.location.href = root_url;
                    }
                 }
        });
	});
});
</script>
</body>
</html>