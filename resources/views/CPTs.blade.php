@extends('layout.app')
@section('content')

	<div class="row">
		<h2 style="color: #251367; margin-left:20px;">CPTs LIST</h2>
		<div class="col-md-12" style="margin-top:20px;">
        <div class="widget box" >
            <div class="widget-content">
                <a href="#" id="linkAddCPT" class="btn btn-primary" style="text-decoration:none"><i class="icon icon-plus"></i>&nbsp;&nbsp; New Procedure Code</a><br/><br/>
					<table id="CPTList" class="table display" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
					</table>
            </div>
        </div>

	</div>

				
    <div class="modal fade" id="CPTModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Procedure Code</h4>
                </div>
                <div class="modal-body" style="min-height:150px;">
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">CPT Code </label>
                        <div class="col-md-12">
                            <input type="text" id="cptCode" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Description </label>
                        <div class="col-md-12">
                            <input type="text" id="desc" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Charges </label>
                        <div class="col-md-12">
                            <input type="text" id="charges" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveCPT" class="btn btn-primary" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
	<div class="modal fade" id="EditCPT" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit CPT</h4>
				</div>
				<div class="modal-body" style="min-height:150px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label col-md-12">CPT Code </label>
                                <div class="col-md-12">
                                    <input type="text" id="cptCd" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label col-md-12">Description </label>
                                <div class="col-md-12">
                                    <input type="text" id="desc1" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label col-md-12">Charges </label>
                                <div class="col-md-12">
                                    <input type="text" id="charge" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnEditCPT" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
@stop

        <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready(function(){
                document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
                $.post(root_url + "/codes/cpt", function(data1, status){
                    var dt = [];
                    $.each(data1,function(i,v) {
                        dt.push([data1[i].id,data1[i].cptCode,data1[i].description,data1[i].charge]);
                    });
                    //alert(dt);
                    var table = $('#CPTList').DataTable({
                        "data": dt,
                        "bProcessing": true,
                        "aoColumns": [
                            {"mdata": "id","title":"CPT Cd", visible:false},
                            {"title":"CPT Code","mdata": "cptCode", "width":"15%"},
                            {"title":"Description","mdata": "description"},
                            {
                                "title":"Charges",
                                "mdata": "charge",
                                mRender: function (data, type, row) { return '$'+row[3]; }
                            },
                            {
                                "title":"Actions",
                                "mdata": "Actions",
                                mRender: function (data, type, row) { return '<a href="javascript:void(0);" onclick="OpenEdit('+row[0]+')"><span class="ti-pencil"></span></a>'; }
                            },
                        ]
                    });
                });
                $('#btnEditCPT').click(function(){
                    var description = document.getElementById('desc1').value;
                    var cptCode = document.getElementById('cptCd').value;
                    var charge = document.getElementById('charge').value;

                    $.ajax({
                        type: "POST",
                        async : false,
                        url:"updateCharges.php",
                        data:{
                            "id" : ID,
                            "description" : description,
                            "cptCode" : cptCode,
                            "charge" : charge,
                        },success:function(result){
                            $('#EditCPT').modal('hide');
                            window.location.reload();
                        }
                    });
                });
                $('#btnSaveCPT').click(function(){
                    var description = document.getElementById('desc').value;
                    var cptCode = document.getElementById('cptCode').value;
                    var charge = document.getElementById('charges').value;

                    $.ajax({
                        type: "POST",
                        async : false,
                        url:root_url + "/codes/cptcreate",
                        data:{
                            "description" : description,
                            "cptCode" : cptCode,
                            "charge" : charge,
                        },success:function(result){
                            $('#CPTModal').modal('hide');
                            window.location.reload();
                        }
                    });
                })
                $('#linkAddCPT').click(function(){
                    $('#CPTModal').modal("show");
                })
            });

            function OpenEdit(id){
                $.ajax({
                    type: "POST",
                    url:"getCPTinfo.php",
                    async : false,
                    data:{
                        cptID : id
                    },success:function(result){
                        $('#EditCPT').modal('show');
                        var res = JSON.parse(result);
                        document.getElementById('desc1').value = res[0].description;
                        document.getElementById('cptCd').value = res[0].cptCode;
                        document.getElementById('charge').value = res[0].charge;
                    }
                });
            }
        </script>