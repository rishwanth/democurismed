<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>EMR </title>
<link type="text/css" rel="stylesheet" href="https://cdn.tinymce.com/4/skins/lightgray/content.min.css" />
<link type="text/css" rel="stylesheet" href="upload/plugin.css" />
<!-- <script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
 -->
 <script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>


<!--script src="../tinymce/tinymce.min.js"></script-->
<script src="upload/plugin.js"></script>
<script type="text/javascript">
tinymce.init({
	selector: "textarea",
	toolbar: "bold italic strikethrough link unlink numlist bullist blockquote upload image fullscreen",
	width: 600,
	height: 200,
	upload_action: 'upload.php',//required
	upload_file_name: 'userfile',//required
	plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste upload"
	]
});
</script>
</head>
<body>
<div style="width: 600px; margin: 20px auto;">
  <h1>EMR </h1>

	<form method="post" action="upload.php">
		<textarea name="content"></textarea>
		<input type="submit" value="upload" />
	</form>
</div>
</body>
</html>