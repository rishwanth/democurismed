<br/>
Dear <b>{{ $mUser->first_name }} {{ $mUser->last_name }} </b>,

<br/><br/>We are glad to have you a part with Curismed,<br/><br/> 

Here is the activation link to activate your account.

<a href="{{ $mUser->link }}">{{ $mUser->link }}</a>

<br/><br/>

<br/><br/>Thanks,

<br/>{{ $mUser->sender }}
