Hello {{ $demo->receiver }},
This is a text email for testing purposes
 
Lines are :
 
#1 : {{ $demo->demo_one }}
#2 : {{ $demo->demo_two }}
 
Values passed by With method:
 
#3 : {{ $testVarOne }}
#4 : {{ $testVarOne }}
 
Thank You,
{{ $demo->sender }}