Hello <i>{{ $demo->receiver }}</i>,
<p>This is a test email noreply@curismed.com</p>
 
<p><u>Mail details:</u></p>
 
<div>
<p><b>Line 1:</b>&nbsp;{{ $demo->demo_one }}</p>
<p><b>Line 1:</b>&nbsp;{{ $demo->demo_two }}</p>
</div>
 
<p><u>Test mail</u></p>
 
<div>
<p><b>Line 1:</b>&nbsp;{{ $testVarOne }}</p>
<p><b>Line 1:</b>&nbsp;{{ $testVarTwo }}</p>
</div>
 
Thank You,
<br/>
<i>{{ $demo->sender }}</i>