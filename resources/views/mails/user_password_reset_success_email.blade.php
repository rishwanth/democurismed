<br/>
Dear <b>{{ $mUser->first_name }} {{ $mUser->last_name }} </b>,

<br/><br/>We are glad to have you a part with Curismed,<br/><br/> 

Your password has been reset successfull. 

<br/><br/>

Please click the below link to login

<br/><br/>

<a href="{{ $mUser->link }}">{{ $mUser->link }}</a>

<br/><br/>

<br/><br/>Thanks,

<br/>{{ $mUser->sender }}
