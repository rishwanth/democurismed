@extends('layout.app')
@section('content')
    <div id="content">
      <h3 style="margin:20px 20px 20px 20px;">Generate Statement</h3>
      <div class="card-body">
        <div class="row">
          <div class="form-group col-md-4">
              <label class="control-label col-md-5">Filter From</label>
              <div class="col-md-7">
                  <input type="text" class="form-control" id="filterFrom" />
              </div>
          </div>
          <div class="form-group col-md-4">
              <label class="control-label col-md-5">Filter To</label>
              <div class="col-md-7">
                  <input type="text" class="form-control" id="filterTo" />
              </div>
          </div>
          <div class="form-group col-md-4" style="margin-bottom:30px;">
              <label class="control-label col-md-5">Patient</label>
              <div class="col-md-7">
                  <select id="patient" class="form-control">
                  </select>
              </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="form-group col-md-12" style="margin-bottom:50px;">
          <input type="button" class="btn btn-primary" value="Generate" id="btnGenerate"/>
      </div>
    </div>
  </div>
@stop
<style>
    .icon:hover{
        text-decoration: none;
    }
    a:hover{
        text-decoration: none;
    }
</style>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/js/alertify.js"></script>
<script>
    $(document).ready(function() {
      var root_url = $("#hidden_root_url").val();
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        $('#loader').show();
        $('#filterFrom').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#filterTo').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $.post(root_url + "/patient/list3",
            {
            },
            function(data, status){
                var res = JSON.parse(data);
                    if(res.status == 0){
                        return;
                    }
                    var data = res.data;
                    var arrDep = [];
                    $('#patient').html('');
                    for(var x in data){
                        arrDep.push(data[x]);
                    }
                    //alert(arrCase);
                    $.each(arrDep,function(i,v) {
                        $('#patient').html('');
                        $('#patient').append('<option value=""></option>');
                        arrDep.forEach(function(t) {
                            $('#patient').append('<option value="'+t.id+'">'+t.full_name+'</option>');
                        });
                    });
            });

        var inv = "";
        $.ajax({
            type: "GET",
            url: root_url+"edi/getlast",
            async : false,
            data:{
            },success:function(result){
                inv = parseInt(result)+1;
                inv = pad(inv, 5);
            }
        });


        $('#btnGenerate').click(function(){
            var root_url = $("#hidden_root_url").val();
            var value1 = $('#patient').val();
            var filterFrom = convertDate($("#filterFrom").val());
            var filterTo = convertDate($("#filterTo").val());
            $.ajax({
                type: "POST",
                url:root_url+"patient/getstatement",
                data:{
                    "patientID" : value1,
                    "filterFrom" : filterFrom,
                    "filterTo" : filterTo
                },success:function(result){
                    //var json = JSON.parse(result);
                    alert("generated");
                    // if(result == "no"){
                    //     alertify.error("This Patient doesn't have any pending statment");
                    // }
                    // else{
                    //     //window.open('Documents/Statements/','_blank');
                    //     alertify.success("Patient Staement generated successfully");
                    // }
                }
            });
        });
    });
    function pad(number, length) {

        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }

        return str;

    }
    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2];

        return month + '-' + day + '-' + year;
    }
    var convertDate = function(usDate) {
        if(usDate != "" && usDate != null && usDate != undefined){
            var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
            return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
        }
    }
</script>
