<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Logs Data</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="{{ asset('public/css/app.css') }}" rel="stylesheet" />
</head>
<body>
   <div class="container">
      <table class="table table-striped">
         <thead>
         <tr>
            <th>Sno</th>
            <th>User Name</th>
            <th>Date</th>
            <th>IP</th>
            <th>Action</th>
            <th>Function</th>
            <th>Comments</th>
            <th>File</th>
         </tr>
         </thead>
         <tbody>
            @foreach($logs as $key => $log)
            <tr>
               <td>{{ $key+1 }}</td>
               <td>{{ $log->user_name }}</td>
               <td>{{ $log->date }}</td>
               <td>{{ $log->ip }}</td>
               <td>{{ $log->action }}</td>
               <td>{{ $log->function }}</td>
               <td>{{ $log->comments }}</td>
               <td>{{ $log->file }}</td>
            </tr>
            @endforeach
         </tbody>
      </table>
      {{ $logs->links() }}
   </div>
</body>
</html>