<!DOCTYPE html>
<html>
<head>
    <title>AMROMED LLC | Query Editor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="public/img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="public/css/bootstrap.css" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="public/vendors/themify/css/themify-icons.css" rel="stylesheet"/>
    <link href="public/vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet"/>
    <link href="public/css/login.css" rel="stylesheet">
    <style type="text/css">
        body{
            color:#000;
            background: #fff;
        }
    </style>
    <!--end page level css-->
</head>

<body id="sign-in1">
<div class="preloader">
    <div class="loader_img"><img src="public/img/loader.gif" alt="loading..." height="64" width="64"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 m-auto login-form1">

                <h2 class="text-center logo_h2">
                    <img src="public/img/pages/clear_black.png" alt="Logo">
                </h2>

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form id="formSQL" name="formSQL" method="post" class="login_validator" action="{{ URL::to('/') }}/mysql">
                            <div class="form-group">
                                <input type="hidden" id="_token" name="_token"
                                  value="{!! csrf_token() !!}" />
                                <input type="hidden" id="root_url" name="v"
                                  value="{{ URL::to('/') }}" />
                                  

                            </div>

<div class="form-group">
<textarea class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="query" name="query"
       placeholder="Enter your DB Query" 
       rows=6>{{$data['query']}}</textarea> 
</div>


<div class="form-group">
<img src="public/img/pages/arrow-right.png" id="btnSubmit" style="margin:0 50%; background-color: chocolate;border-radius: 15px;cursor: pointer;" alt="Go" width="30" height="30"> 
</div>

<div class="form-group">
    JSON Results
</div>

<div class="form-group">
<textarea class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="result" name="result`"
placeholder="Your query results is here" 
rows=3 readonly>{{$data['result']}}</textarea> 
</div>

<div class="form-group" style="overflow: auto;
overflow-y: hidden1;height: 300px">
<?php
    if (count($data['rows']) >0){
?>
<div class="form-group">
    Query Results
</div>

<table class="table table-striped table-bordered">

    <tr>
    <th><strong>Sno</strong></th>
    @foreach($data['rows'][0] as $key => $value)
        <th><strong>{{ $key }}</strong></th>
    @endforeach
    </tr>           

@foreach($data['rows'] as $key => $array)
    <tr>
        <td>{{ $key+1 }}</td>
    @foreach($array as $column_name => $value)
        <td>{{ $value }}</td>
    @endforeach
    </tr>
@endforeach
</table>

<?php
    }
?>
</div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="public/js/jquery.min.js" type="text/javascript"></script>

<script src="public/js/popper.min.js" type="text/javascript"></script>
<script src="public/js/bootstrap.min.js" type="text/javascript"></script>

<!-- end of global js -->
<!-- page level js -->

<script src="public/vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<script type="text/javascript" src="public/js/custom_js/login.js"></script>

<script>
$(document).ready(function(){
	$('#btnSubmit').click(function(){
        $('#formSQL').submit();
	});
});
</script>
</body>
</html>