@extends('layout.app')
@section('content')
<div class="row">
	<h2 style="color: #251367; margin-left:20px;">Find Patient</h2>
	<div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">
        	<form action="EditPatient.php" method="post">
            <table id="test" class="table table-bordered display" style="cursor:pointer" cellspacing="0" width="90%">
			</table>
			</form>
            <a href="AddPatient.php" class="btn btn-primary">Add New Client</a>
        </div>
    </div>

</div>
@stop

    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script>


        $(document).ready(function(){
            $('#loader').show();
            var practiceId = sessionStorage.getItem('practiceId');
            //$("#test tr").css('cursor', 'pointer');
            $.post(root_url + "/patients",
                {
                    practiceID: practiceId
                },
                function(data1, status){
                    var dt = [];
                    $.each(data1,function(i,v) {
                        var DOB = changeDateFormat(data1[i].dob);

                        dt.push([data1[i].patientID,data1[i].fullName,DOB,data1[i].gender,data1[i].ssn,data1[i].chartNo]);
                    });
                    //alert(dt);
                    var table = $('#test').DataTable({
                        "data": dt,
                        columns: [
                            {"title": "patientID", visible:false},
                            {"title": "Client Name"},
                            {"title": "DOB"},
                            {"title": "Gender"},
                            {"title": "SSN"},
                            {"title": "Account #"}
                        ]
                    });
                    $('#loader').hide();
                    $('#test tbody').on('click', 'tr', function () {
                        $('#MedCoding').modal('show');
                        var data = table.row( this ).data();
                        var id = data[0];
                        sessionStorage.setItem("patientId", id);
                        window.location.href ="EditPatient.php";
                    });
                });
        });
        function changeDateFormat(inputDate){  // expects Y-m-d
            if(inputDate != "" && inputDate != undefined && inputDate != null){
                var splitDate = inputDate.split('-');
                if(splitDate.count == 0){
                    return null;
                }

                var year = splitDate[0];
                var month = splitDate[1];
                var day = splitDate[2];

                return month + '-' + day + '-' + year;
            }
        }
    </script>