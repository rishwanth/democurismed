<?php

/*echo '<pre>';
print_r( $data);
die();
*/
/*$data['specialityList'] = $specialityList;
$data['fieldsList'] = 
*/
$countSavedPaycodes = 0; //count($data['savedPaycodes']['data']);
$countSavedWorkhistory = 0; //count($data['savedWorkhistory']['data']);
$id = isset($data['edit_row']) ? $data['edit_row']['id'] : 0;
$row = isset($data['edit_row']) ? $data['edit_row'] : '';


?>
<div class="modal fade" id="AddEmployee" tabindex="-1">
    <div class="modal-dialog modal-lg" style="width:100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="lblTitle">Add New Employee</h4>
            </div>

           <div class="modal-body" style="min-height:250px;">

                <div class="col-12">
    <div class="card ">
        
        <div class="card-body">
            <div class="stepwizard">
                <div class="stepwizard-row setup-card">
                    <div class="stepwizard-step">
                        <a href="#step-employee-1" id="up1" class="btn btn-primary btn-block">Payroll Setup</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-employee-2" id="up2" class="btn btn-default btn-block">Employee Address</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-employee-3" id="up3" class="btn btn-default btn-block">Other Info</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-employee-4" id="up4" class="btn btn-default btn-block">Work History</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-employee-5" id="up5" class="btn btn-default btn-block">Activities</a>
                    </div>

                </div>
            </div>

<form role="form">

<input type="hidden" id="employee_id" 
name="employee_id" value="0" >

<input type="hidden" id="countSavedPaycodes" 
        name="countSavedPaycodes"
       value="<?php echo $countSavedPaycodes; ?>">

<input type="hidden" id="countSavedWorkhistory" 
    name="countSavedWorkhistory"
       value="<?php echo $countSavedWorkhistory; ?>">

<div class="row setup-content" id="step-employee-1">
        <div class="col-12">
            <div class="card">

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnChkPat" />
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="name" name="name" placeholder="Employee Name" 
                                            value="name"/>
                                        </div>

                                        <div class="col-md-4">
                                            <select class="form-control" id="speciality" name="speciality">
                                        
                                            </select>
                                       </div>

                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="employee_type" name="employee_type">
                                                <option value="">--Employee Type--</option>
                                                <option value="Full Time">Full Time</option>
                                                <option value="Part Time">Part Time</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="date form-control validateGeneral" id="date_of_hire" name="date_of_hire" 
                                            placeholder="Date Of Hire"
                                            value="" />
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="date form-control validateGeneral" id="date_of_birth" name="date_of_birth" 
                                            value=""
                                            placeholder="Date Of Birth" />
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row" >
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral"  id="employee_position" 
                                            name="employee_position" 
                                            value="employee_position"

                                            placeholder="Employee Position" 
                                             />
                                        </div>

                                        <div class="col-md-4">
                                                <input type="text" class="form-control validateGeneral"  id="individual_npi" name="individual_npi" placeholder="Individual NPI of the Primary Employee" 
                                                value="individual_npi" />
                                            </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="ssno" name="ssno"
                                            value="ssno"
                                            data-inputmask1='"mask": "(999) 999-9999"' placeholder="Social Security #" data-mask1 />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="has_benifits" name="has_benifits">
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="payroll_company" 
                                            name="payroll_company"
                                            value="payroll_company"
                                            placeholder="Payroll Company"  />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="payroll_employee_num" 
                                            name="payroll_employee_num"
                                            placeholder="Payroll Employee #" 
                                    value="payroll_employee_num" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="workers_comp_num"
                                            name="workers_comp_num"
                                            placeholder="Workers Comp #" 
                                    value="workers_comp_num" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="department_name" name="department_name" placeholder="Department" 
                            value="department_name" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="pay_code" name="pay_code"
                                            placeholder="Pay Code" 
                                    value="pay_code" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                        <textarea class="form-control validateGeneral" rows="5"
                                        cols="80" name="notes"
                                        id="notes" 
                                placeholder="Notes">notes</textarea>
                                        </div>
                                    </div>
                                </div>

                        <div class="col-sm-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-md-8 float-right">
                                    <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNextEmployee1"/>
                            </div>
                        </div>
                    </div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- step-employee-2 -->

<div class="row setup-content" id="step-employee-2">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="address_1" name="address_1" placeholder="Address Line 1"
                                            value="address_1" />
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="address_2" name="address_2" placeholder="Address Line 2"
                                    value="address_2" />
                                            <br/>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="city" name="city" 
                                            placeholder="City" 
                                        value="city" />
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral"  id="region" name="region" placeholder="Region" 
                                            value="region" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="country_id" name="country_id">
                                            </select>
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="zip" name="zip" placeholder="Zip / Postal Code" value="zip" />
                                            <br/>
                                        </div>

                                    </div>
                                </div>                                


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="phone_home" name="phone_home" placeholder="Phone Home" 
                                            value="phone_home" />
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="phone_work" name="phone_work" placeholder="Phone Work" 
                                            value="phone_work" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="phone_mobile" name="phone_mobile" placeholder="Mobile Number" 
                                            value="phone_mobile" />
                                            <br/>
                                        </div>

                                        <div class="col-md-4">
                                            <input type="text" class="form-control required" 
                                            id="fax" name="fax" placeholder="Fax" 
                                            value="fax" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                            <input type="email" class="form-control required" 
                                            id="email" name="email" placeholder="Email" 
                                            value="phy_email" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>


                        <div class="col-sm-12 col-md-12">
                        <div class="form-group row">
                        <div class="col-md-8">

                                        <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNextEmployee2" name="btnNextEmployee2" />

                        </div>
                        </div>
                        </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- step-employee-3 -->

<div class="row setup-content" id="step-employee-3">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnChkPat" />
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" id="driver_license_number" 
                                            name="driver_license_number" placeholder="Driver License Number" 
                                    value="driver_license_number" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control date validateGeneral" 
                                            id="driver_license_expiration" 
                                            name="driver_license_expiration" 
                                            placeholder="Driver License Expiration"                                     value=""/>
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row" >
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral"  
                                            id="auto_insurance_number" 
                                            name="auto_insurance_number" 
                                            placeholder="Auto Insurance Number" 
                                    value="" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class=" date form-control required"
                                            id="auto_insurance_expiration" 
                                            name="auto_insurance_expiration" 
                                            placeholder="Auto Insurance Expiration" 
                                        value="" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control validateGeneral" 
                                            id="educational_credential" 
                                            name="educational_credential" 
                                            placeholder="Educational Credential" 
                                        value="" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" class=" date form-control validateGeneral " 
                                            id="tb_clearance_expiration" 
                                            name="tb_clearance_expiration" 
                                            placeholder="TB Clearance Expiration" 
                                        value="" />
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="text" class="date form-control validateGeneral " 
                                id="medical_clearance_expiration" 
                                name="medical_clearance_expiration" 
                                placeholder="Medical Clearance Expiration"
                        value="" />
                                <br/>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="text" class="form-control validateGeneral" 
                                id="background_check_completion" 
                                name="background_check_completion" 
                                placeholder="Background Check Completion" 
                        value="" />
                                <br/>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="text" class="date form-control validateGeneral" 
                                id="background_check_expiration" 
                                name="background_check_expiration" 
                                placeholder="Background Check Expiration" 
                        value="" />
                                <br/>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                    <div class="form-group row">
                    <div class="col-md-4">
                                           
                        <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNextEmployee3"/>
                    </div>
                    </div>
                    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<!-- step-employee-4 -->
<div class="row setup-content" id="step-employee-4">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnChkPat" />
                            <div class="row">

                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            Start Date : 

<input type="hidden" class="form-control date" name="employee_workhistory_id" id="employee_workhistory_id" value="0" />
<input type="text" class="form-control date" name="start_date" id="start_date"  />

                                        </div>

                                        <div class="col-md-3">
                                            End Date : 
<input type="text" class="form-control date" name="end_date" id="end_date"  />
                                        </div>

                                        <div class="col-md-3">
                                            Notes : 
<input type="text" class="form-control " name="notes" id="notes"  />
                                        </div>

                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-primary" id="addworkhistory">+ AddNew / Save </button>
                                        </div>

                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <br/>
                                        </div>


                                    </div>
                                </div>

                <!-- workhistory list -->
                <div class="col-sm-12 col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <table class="table table-border" id="workhistoryTable" width="100%">
                            </table>
                </div>
                </div>
                </div>




                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">

                                        <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNextEmployee4"/>
                                </div>
                                </div>
                                </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- end of step-employee-4 -->

<!-- step-employee-5 -->
<div class="row setup-content" id="step-employee-5">
        <div class="col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnChkPat" />
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select class="form-control" 
                                            id="paycode_id" 
                                            name="paycode_id">
                                            <option value="1">from hander 1</option>
                                            <option value="2">from hander 2</option>
                                            <option value="3">from hander 3</option>

                                            </select>
                                            <br/>
                                        </div>

                                        <div class="col-md-2">
                                            <input type="hidden" class="form-control validateGeneral" id="paycode_tb_id" 
                                            name="paycode_tb_id" 
                                            placeholder="paycode_tb_id" />
                                        </div>

                                        <div class="col-md-2">
                                            Billable :
                                            <select class="form-control" 
                                                id="billable" 
                                                name="billable">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>

                                        </div>

                                        <div class="col-md-2">
                                            Mileage :
                                            <input type="text" class="form-control validateGeneral" id="mileage" 
                                            name="mileage" 
                                            placeholder="mileage" />
                                        </div>

                                        <div class="col-md-2">Rate :
                                            <input type="text" class="form-control validateGeneral" id="rate" 
                                            name="rate" 
                                            placeholder="rate" />

                                        </div>

                                        <div class="col-md-1">
                                            <button type="button" class="btn btn-primary" id="addpaycode">Save </button>
                                        </div>

                                        <div class="col-md-1" style="display: none;">
                                            <button type="button" class="btn btn-primary" id="loadpaycode">Refresh</button>
                                        </div>

                                    </div>
                                </div>

                <!-- paycode added list -->
                <div class="col-sm-12 col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <table class="table table-border" id="paycodeTable" width="100%">
                            </table>

                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="form-group row">
                        <div class="col-md-4">

                            <br/>
                            <input class="btn btn-primary nextBtn float-right" type="button" value="Save" id="btnNextEmployee5"/>
                        </div>
                    </div>
                </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- end of step-employee-5 -->


</form>
        
        </div>
    </div>
</div>


            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="_btnSave" class="btn btn-primary _btnsave saveemployee" data-dismiss1="modal" value="Save" data-btnname="employee" data-btnurl="employee" />
            </div>
            
        </div>
    </div>
</div>

<script type="text/javascript">
    var countPaycodes = 0;
    var countWorkhistory = 0;
$(document).ready(function(){
    countPaycodes = parseInt($('#countSavedPaycodes').val());
    countWorkhistory = parseInt($('#countSavedWorkhistory').val());



    $('.saveemployee').click(function(){
        save('');
    });

    $(document).on('click','.workhistory-edit',function(){
        var id = $(this).data('id');
        addRowWorkHistory(id, 'edit');
    })

    $(document).on('click','.workhistory-delete',function(){
        var id = $(this).data('id');
        addRowWorkHistory(id, 'delete');
    })

    $(document).on('click','.paycode-edit',function(){
        var id = $(this).data('id');
        addRowPayCode(id, 'edit');
    })
    $(document).on('click','.paycode-delete',function(){
        var id = $(this).data('id');
        var c = id + ' => delete';
        alert(c)
        addRowPayCode(id, 'delete');
    })


    $('.employee-edit').click(function(){
        var id = $(this).data('id');
        $('#lblTitle').text('Edit => ID : ' + id).show();
        $('#employee_id').val(id);
        $('#AddEmployee').modal('show');
        fillEmployeeEdit(id);
    });

    

    $('#btnNextEmployee1').click(function(){
        displayStep(2);
    });
    
    $('#btnNextEmployee2').click(function(){
        displayStep(3);
    });
    $('#btnNextEmployee3').click(function(){
        displayStep(4);
    });
    $('#btnNextEmployee4').click(function(){
        displayStep(5);
    });
    $('#btnNextEmployee5').click(function(){
        displayStep(6);
    });

    $('#addpaycode').click(function(){
        addRowPayCode(0,'add');
    });

    $('#loadpaycode').click(function(){
        //addRowPayCode(5,'edit');
    });

    $('#addworkhistory').click(function(){
        addRowWorkHistory(0, 'add');
    });

    initialize();
});

function initialize(){
    $('.date').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        format: 'm-d-Y',
        timepicker: false,
        //minDate: '-2013/01/02',
        //maxDate: '+2014/12/31',
        formatDate: 'm-d-Y',
        closeOnDateSelect: true
    });
}


function save(step){
    var root_url = $("#hidden_root_url").val();
    $('#loader').show();
    var id = $('#employee_id').val();
    var name = $('#name').val();
    if (name.length < 2) {
        alert('Invalid Employee Name')
        return;
    }
    var paycodes = '';
    var workhistory = '';
    if (step =='' || step == 'paycode'){
        //paycodes = getSelectedPaycodes();
    }
    if (step =='' || step == 'workhistory'){
        //workhistory = getAddedWorkhistory();
    }

    var speciality = $("#speciality").val();
    var employee_type = $("#employee_type").val();
    var employee_position = $("#employee_position").val();
    var date_of_birth = convertDate($('#date_of_birth').val());
    var date_of_hire = convertDate($('#date_of_hire').val());
    var individual_npi = $('#individual_npi').val();
    var ssno = $('#ssno').val();
    var has_benifits = $('#has_benifits').val();
    var payroll_company = $('#payroll_company').val();
    var payroll_employee_num = $('#payroll_employee_num').val();
    var workers_comp_num = $('#workers_comp_num').val();
    var department_name = $('#department_name').val();
    var pay_code = $('#pay_code').val();
    var notes = $('#notes').val();
    var address_1 = $('#address_1').val();
    var address_2 = $('#address_2').val();
    var city = $('#city').val();
    var region = $('#region').val();
    var zip = $('#zip').val();
    var phone_home = $('#phone_home').val();
    var phone_work = $('#phone_work').val();
    var phone_mobile = $('#phone_mobile').val();
    var email = $('#email').val();
    var fax = $('#fax').val();
    var driver_license_number = $('#driver_license_number').val();
    var driver_license_expiration = convertDate($('#driver_license_expiration').val());
    var auto_insurance_number = $('#auto_insurance_number').val();
    var auto_insurance_expiration = convertDate($('#auto_insurance_expiration').val());
    var educational_credential = $('#educational_credential').val();
    var tb_clearance_expiration = convertDate($('#tb_clearance_expiration').val());
    var medical_clearance_expiration = convertDate($('medical_clearance_expiration').val());
    var background_check_completion = $('#background_check_completion').val();
    var background_check_expiration = convertDate($('background_check_expiration').val());

    var country_id = $('#country_id').val();
    var country_name = $('#country_id').val();
    var _token = $('#_token').val();
    var data = {};
    if (step == '1') {
        data = {
                "_token" : _token,
                "step" : step,
                "id" : id,
                "physicianID" : id,
                "name" : name,
                "speciality" : speciality,
                "employee_type" : employee_type,
                "employee_position" : employee_position,
                "date_of_birth" : date_of_birth,
                "date_of_hire" : date_of_hire,
                "individual_npi" : individual_npi,
                "ssno" : ssno,
                "has_benifits": has_benifits,
                "payroll_company": payroll_company,
                "payroll_employee_num": payroll_employee_num,
                "workers_comp_num" : workers_comp_num,
                "department_name" : department_name,
                "pay_code" : pay_code,
                "notes" : notes,
        }
    };

    if (step == '2') {
        data = {
                "_token" : _token,
                "step" : step,
                "id" : id,
                "physicianID" : id,
                "address_1" : address_1,
                "address_2" : address_2,
                "city" : city,
                "region" : region,
                "zip" : zip,
                "phone_home" : phone_home,
                "phone_work" : phone_work,
                "phone_mobile" : phone_mobile,
                "email" : email,
                "fax" : fax,
            }
    };

    if (step == '3') {
        data = {
                "_token" : _token,
                "step" : step,
                "id" : id,
                "physicianID" : id,
                "driver_license_number" : driver_license_number,
                "driver_license_expiration" : driver_license_expiration,
                "auto_insurance_number" : auto_insurance_number,
                "auto_insurance_expiration" : auto_insurance_expiration,
                "educational_credential" : educational_credential,
                "tb_clearance_expiration" : tb_clearance_expiration,
                "medical_clearance_expiration" : medical_clearance_expiration,
                "background_check_completion" : background_check_completion,
                "country_id" : country_id,
                "country_name" : country_name,
        }
    };

    if (step == 'paycode') {
        data = {
                "_token" : _token,
                "step" : step,
                "id" : id,
                "physicianID" : id,
                "paycodes" : paycodes,
        }
    };

    if (step == 'paycode') {
        data = {
                "_token" : _token,
                "step" : step,
                "id" : id,
                "physicianID" : id,
                "workhistory": workhistory,
        }
    };

    $.ajax({
        type: "POST",
        url:root_url + "physician/addnew",
        data: data,
        success:function(result) {
            $('#loader').hide();
            console.log(result);
            var res = JSON.parse(result);
            if(res.status == 0){
                alert(res.message);
                fillUser();
                return false;
            }  else {
                id = res.data.id;
                alert(res.message + ' => ' + id);
                $('#employee_id').val(id);
                if (step == '1'){
                    alert('fill employee table has to be done');
                }
                return true;
                //window.location.href = root_url + "physician/list"; 
            }
        }
    });
}

function displayStep(stepNum){
    var step = stepNum-1;
    if (stepNum == 5){
        step = 'paycodes';
    }
    if (stepNum == 6){
        step = 'activities';
    }

    if( save(step) == false){
        return;
    }

    for (i=1; i<=5; i++){
        step = '#step-employee-' + i;
        up = '#up' + i;
        if (stepNum == i){
            $(step).css({"display":"flex"});
            $(up).addClass("btn-primary");
        } else {
            $(step).css({"display":"none"});
            $(up).removeClass("btn-primary");
        }
    }
}

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

var convertDate = function(usDate) {
    if(usDate != "" && usDate != null && usDate != undefined){
        var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
        return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }
}


function fillEmployeeEdit(id){
    var root_url = $("#hidden_root_url").val();
    $('#loader').show();
    var _token = $('#_token').val();

    $.ajax({
        type: "POST",
        url:root_url + "physician/list1",
        data:{
            "_token" : _token,
            "id" : id,
            "workhistory" : 1,
            "activities" : 1,
        },success:function(result) {
            $('#loader').hide();
            console.log(result);
            var data1 = JSON.parse(result);
            if(data1.status == 0){
                alert(data1.message);
                return;
            }  else {
                var res = data1.data['rows'][0];
                $('#name').val(res.name);
                $('#phone_home').val(res.phone_home);
                $('#phone_work').val(res.phone_work);
                $('#phone_mobile').val(res.phone_mobile);
                $('#phy_email').val(res.phy_email);
                $('#dob').val(res.dob);
                $('#individual_npi').val(res.individual_npi);
                $('#ssno').val(res.ssno);
                $('#speciality').val(res.speciality);
                $('#address').val(res.address);
                $('#fax').val(res.fax);
                $('#notes').val(res.notes);
                $('#employee_position').val(res.employee_position);
                $('#employee_type').val(res.employee_type);
                $('#date_of_hire').val(res.date_of_hire);
                $('#has_benifits').val(res.has_benifits);
                $('#payroll_company').val(res.payroll_company);
                $('#payroll_employee_num').val(res.payroll_employee_num);
                $('#workers_comp_num').val(res.workers_comp_num);
                $('#department_id').val(res.department_id);
                $('#department_name').val(res.department_name);
                $('#pay_code').val(res.pay_code);
                $('#address_1').val(res.address_1);
                $('#address_2').val(res.address_2);
                $('#city').val(res.city);
                $('#region').val(res.region);
                $('#country_id').val(res.country_id);
                $('#region').val(res.region);
                $('#country_name').val(res.country_name);
                $('#zip').val(res.zip);
                $('#driver_license_number').val(res.driver_license_number);
                $('#driver_license_expiration').val(res.driver_license_expiration);
                $('#educational_credential').val(res.educational_credential);
                $('#tb_clearance_expiration').val(res.tb_clearance_expiration);
                $('#medical_clearance_expiration').val(res.medical_clearance_expiration);
                $('#auto_insurance_number').val(res.auto_insurance_number);
                $('#background_check_completion').val(res.background_check_completion);
                $('#background_check_expiration').val(res.background_check_expiration);

                var c = '';
                res = data1.data['savedPaycodes'].data;
                c += 'count savedPaycodes => ' + res.length;
                //alert(c);
                fillPaycodes(res);

                res = data1.data['savedWorkhistory'].data;
                c += '\ncount savedWorkhistory => ' + res.length;
                fillWorkhistory(res);
            }
        }
    });

}

function addRowWorkHistory(id, stepAction){
    var root_url = $("#hidden_root_url").val();
    $('#loader').show();
    var step = 'workhistory';
    var step_action = stepAction;

    var id1 = $('#employee_workhistory_id').val();
    var tb_workhistory_id = id1;
    if (id1 > 0){
        id = id1;
        step_action = 'update';
    } else {
        if (id > 0){
            step_action = 'edit';
        }
    }
    if (stepAction == 'delete' || stepAction == 'edit' || stepAction == 'load'){
        step_action = stepAction;
    }
    var employee_id = $('#employee_id').val();
    var start_date = convertDate($('#start_date').val());
    var end_date = convertDate($('#end_date').val());
    var notes = $('#notes').val();
    var _token = $('#_token').val();
    var data = {};


    data = {
        "_token" : _token,
        "step" : step,
        "step_action" : step_action,
        "id" : id,
        "tb_workhistory_id" : tb_workhistory_id,
        "physician_id" : employee_id,
        "start_date" : start_date,
        "end_date" : end_date,
        "notes" : notes,
    };

    $.ajax({
        type: "POST",
        url:root_url + "physician/addnew",
        data: data,
        success:function(result) {
            $('#loader').hide();
            console.log(result);
            var res = JSON.parse(result);
            if(res.status == 0){
                alert(res.message);
                fillWorkhistory('');
                return false;
            }  else {
                if (step_action == 'edit'){
                    var data1 = res.data[0];
                    $('#start_date').val(data1.start_date);
                    $('#end_date').val(data1.end_date)
                    $('#employee_workhistory_id').val(data1.id);
                    //initialize(data1);
                    alert(res.message + ' => to save click update');
                } else if (step_action == 'delete'){
                    //alert(res.message);
                    $('#employee_workhistory_id').val(0);
                    fillWorkhistory(res.data.savedWorkhistory.data);
                } else {
                    $('#start_date').val('');
                    $('#end_date').val('')
                    $('#notes').val('');
                    $('#employee_workhistory_id').val(0);
                    id = res.data.id;
                    alert(res.message);
                    fillWorkhistory(res.data.savedWorkhistory.data);
                }
                return true;
            }
        }
    });
}


function fillWorkhistory(data1){
    var dt = [];

    if (data1 == ''){
    }  else {
        $.each(data1,function(i,  v) {
            dt.push([data1[i].id,
                data1[i].start_date,
                data1[i].end_date,
                data1[i].notes,
                data1[i].id]);
        });
    }

    $('#workhistoryTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Start Date", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "End Date", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Notes", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a data-id="'+data+'" class="workhistory-edit" ><span class="ti-pencil "></span></a><a data-id="'+data+'" class="workhistory-delete" > | <span class=" ti-trash "></span></a>';
                }
            },
        ]
    });
}

function fillPaycodes(data1){
    var dt = [];
    if (data1 == ''){
        dt = [];
    } else {
        $.each(data1,function(i,  v) {
            dt.push([data1[i].id,
                data1[i].name,
                data1[i].billable,
                data1[i].mileage,
                data1[i].rate,
                data1[i].id]);
        });
    }

    $('#paycodeTable').DataTable({
        "data": dt,
        "autoWidth": false,
        "bDestroy":true,
        "columnDefs": [
            { "width": "10px", "targets": 0 },
            { "width": "200px", "targets": 1 },
            { "width": "100px", "targets": 2 },
        ],
        columns: [
            {"title": "docID","visible" : false},
            {"title": "Paycode", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Billable", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Milage", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Rate", "width": "60px",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {"title": "Actions",
                "render": function ( data, type, full, meta ) {
                    return '<a data-id="'+data+'" class="paycode-edit" ><span class="ti-pencil "></span></a><a data-id="'+data+'" class="paycode-delete" > | <span class=" ti-trash "></span></a>';
                }
            },
        ]
    });
}

function addRowPayCode(id, stepAction){
    var root_url = $("#hidden_root_url").val();
    $('#loader').show();
    var step = 'paycode';
    var step_action = stepAction;
    var paycode_tb_id = id;
    var id1 = $('#employee_paycode_id').val();
    if (id1 > 0){
        id = id1;
        step_action = 'add';
    } else {
        if (id > 0){
            step_action = 'edit';
        }
    }
    if (stepAction == 'delete'){
        step_action = stepAction;
    } else if (stepAction == 'load'){
        step_action = stepAction;
    } else if (stepAction == 'edit'){
        step_action = stepAction;
    }
    paycode_tb_id = $('#paycode_tb_id').val();
    if (id > 0 && stepAction == 'edit') {
        paycode_tb_id = id;
    }
    var employee_id = $('#employee_id').val();
    var paycode_id = $('#paycode_id').val();
    var billable = $('#billable').val();
    var mileage = $('#mileage').val();
    var rate = $('#rate').val();

    var _token = $('#_token').val();
    var data = {};


    data = {
        "_token" : _token,
        "step" : step,
        "step_action" : step_action,
        "id" : id,
        "paycode_tb_id" : paycode_tb_id,
        "physician_id" : employee_id,
        "paycode_id" : paycode_id,
        "billable" : billable,
        "mileage" : mileage,
        "rate" : rate,
    };

    $.ajax({
        type: "POST",
        url:root_url + "physician/addnew",
        data: data,
        success:function(result) {
            $('#loader').hide();
            console.log(result);
            var res = JSON.parse(result);
            if(res.status == 0){
                alert(res.message);
                fillPaycodes('');
                return false;
            }  else {
                if (step_action == 'edit'){
                    alert(res.message + ' => to save click update');
                    var data1 = res.data[0];
                    $('#paycode_tb_id').val(data1.id);
                    $('#paycode_id').val(data1.paycode_id);
                    $('#billable').val(data1.billable)
                    $('#mileage').val(data1.mileage)
                    $('#rate').val(data1.rate);
                    //fillPaycodes(res.data.savedPaycodes.data);
                } else if (step_action == 'delete'){
                    //alert(res.message);
                    fillPaycodes(res.data.savedPaycodes.data);
                } else {
                    $('#paycode_tb_id').val(0);
                    $('#paycode_id').val(1);
                    $('#billable').val('Yes')
                    $('#mileage').val('0')
                    $('#rate').val('0');
                    id = res.data.id;
                    //alert(res.message);
                    fillPaycodes(res.data.savedPaycodes.data);
                }
                return true;
            }
        }
    });
}


var convertDate = function(usDate) {
    if (usDate == '') {
        return '0000-00-00';
    }
    if (usDate == undefined) {
        return '0000-00-00';
    }
    var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
    return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2] + ' 00:00:00';
}

</script>