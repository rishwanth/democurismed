@extends('layout.app')
@section('content')
 
    <div class="row">
        <h2 style="color: #251367; margin-left:20px;">Deposit</h2>
        <div class="col-md-12" style="margin-top:20px;">
        <div class="widget box" >
            <div class="widget-content">
                <a href="javascript:void(0)" id="linkAddDep" class="btn btn-primary" style="text-decoration:none"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Deposit</a>
                <a href="javascript:void(0)" id="uploadERA" class="btn btn-primary" style="text-decoration:none"><i class="fa fa-plus"></i>&nbsp;&nbsp;Upload ERA</a>
                <form action="EditPatient.php" method="post">
                    <table id="deposit" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
                    </table>
                </form>
                <table id="depositDetails" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
                </table>
                <table id="depERA" class="table table-bordered table table-hover table-striped" style="margin-top:70px;">
                    <thead id="depHead">
                    </thead>
                    <tbody id="depBody">
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="modal fade" id="AddDeposit" tabindex="-1">
        <div class="modal-dialog modal-lg" style="width:75%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Deposit</h4>
                </div>
                <div class="modal-body" style="min-height:380px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Select Payor Type </label>
                                <div class="col-md-12">
                                    <select class="form-control" id="payorType">
                                        <option value="0" selected></option>
                                        <option value="1">Payor</option>
                                        <option value="2">Client</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6" id="divPayorType">
                                <label class="control-label col-md-12" id="lblType">Payor </label>
                                <div class="col-md-12">
                                    <input type="text" id="payor" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Deposit Date </label>
                                <div class="col-md-12">
                                    <input type="text" id="depositDate" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12" id="lblType1">Payment Method </label>
                                <div class="col-md-12">
                                    <select class="form-control" id="paymentType">
                                        <option>Check</option>
                                        <option>EFT</option>
                                        <option>Credit Card</option>
                                        <option>Cash</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label col-md-12">Check # </label>
                                <div class="col-md-12">
                                    <input type="text" id="instrumentNo" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label col-md-12">Check Date </label>
                                <div class="col-md-12">
                                    <input type="text" id="instrumentDate" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label col-md-12" id="lblType">Amount </label>
                                <div class="col-md-12">
                                    <input type="text" id="amount" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-md-8">
                                <label class="control-label col-md-12" id="lblType">Notes </label>
                                <div class="col-md-12">
                                    <textarea class="form-control" id="notes" cols="5" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveDep" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalERA" tabindex="-1">
        <div class="modal-dialog" style="width:30%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Upload 835 File</h4>
                </div>
                <div class="modal-body" style="min-height:180px;">
                    <form id="upload" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-12">
                            <label class="control-label col-md-12">835 File :</label>
                            <div class="col-md-10">
                                <input id="filename" type="file" name="filename" />
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <input class="btn btn-success float-right" type="submit" id="upload" value="Upload">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="EditDeposit" tabindex="-1">
        <div class="modal-dialog" style="width:75%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Deposit</h4>
                </div>
                <div class="modal-body" style="min-height:280px;">
                    <input type="hidden" id="_hdnDepID"/>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Select Payor Type </label>
                        <div class="col-md-6">
                            <select class="form-control" id="EditpayorType">
                                <option value="0"></option>
                                <option value="1">Payor</option>
                                <option value="2">Client</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6" id="divPayorType">
                        <label class="control-label col-md-12" id="lblType">Payor </label>
                        <div class="col-md-6">
                            <select class="form-control" id="Editpayor">
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Deposit Date </label>
                        <div class="col-md-6">
                            <input type="text" id="EditdepositDate" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12" id="lblType">Payment Method </label>
                        <div class="col-md-6">
                            <select class="form-control" id="EditpaymentType">
                                <option>Check</option>
                                <option>EFT</option>
                                <option>Credit Card</option>
                                <option>Cash</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12">Check # </label>
                        <div class="col-md-6">
                            <input type="text" id="EditinstrumentNo" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12">Check Date </label>
                        <div class="col-md-6">
                            <input type="text" id="EditinstrumentDate" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12" id="lblType">Amount </label>
                        <div class="col-md-6">
                            <input type="text" id="Editamount" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label class="control-label col-md-12" id="lblType">Notes </label>
                        <div class="col-md-10">
                            <textarea class="form-control" id="Editnotes" col="5" row="6"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnEditDep" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@stop
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script>


    $(document).ready(function(){
        //alert("hello");
        var root_url = $("#hidden_root_url").val();
        $(".ti-menu-alt").click();
        $('#divPayorType').hide();
        $("#depERA").hide();
        // //var practiceId = sessionStorage.getItem('practiceId');
        // //var temp = sessionStorage.getItem("patientId");
        $(document).on('focusout','#amount',function() {
            var amount = $(this).val();
            amount = amount;
            document.getElementById("amount").value = amount;
        });
        $("#uploadERA").click(function(){
            $("#modalERA").modal("show");
        })

        $("#upload").submit(function (event) {
            event.preventDefault();
            $.ajax({
                 type: "post",
                 url: root_url+"/deposits/geterainfofromfile",
                 data: new FormData($(this)[0]),
                 processData: false,
                 contentType: false,
                 cache: false,
                 success: function (res) {
                    //console.log(res);
                    var data = JSON.parse(res).data;
                    $("#modalERA").modal("hide");
                    $("#depERA").show();
                    var arrDep = [];
                    // var claims_list = [];
                    // var claims = [];
                    $('#depBody').html('');
                    //for(var x in data){
                        arrDep.push(data);
                    //}

                    $('#depHead').html('<th style="background-color:#2E576B; color:#fff; width:3%;">ID</th><th style="background-color:#2E576B; color:#fff; width:7%;">DOS</th><th style="background-color:#2E576B; color:#fff; width:6%;">Code</th><th style="background-color:#2E576B; color:#fff; width:6%;">Modifier</th><th style="background-color:#2E576B; color:#fff; width:5%;">Billed</th><th style="background-color:#2E576B; color:#fff; width:7%;">Allowed</th><th style="background-color:#2E576B; color:#fff; width:7%;">Paid</th><th style="background-color:#2E576B; color:#fff; width:7%;">Adjustment</th><th style="background-color:#2E576B; color:#fff; width:10%;">Insurance Balance</th><th style="background-color:#2E576B; color:#fff; width:10%;">Patient Balance</th><th style="background-color:#2E576B; color:#fff; width:9%;">Reason</th><th style="background-color:#2E576B; color:#fff; width:15%;">Status</th>');
                    arrDep.forEach(function(t) {
                        //for(var x in t.CLAIMS){
                            //claims_list.push(t.CLAIMS);
                        //}
                        console.log(t.CLAIMS);
                        // for(var x in claims_list){
                        //     claims.push(claims_list);
                        // }
                        // console.log(claims);
                        var i=0;
                        t.CLAIMS.forEach(function(t){
                            var clsName = "";
                            var charge_amount = t.CHARGE_AMOUNT;
                            var payment_amount = t.PAYMENT_AMOUNT;
                            //var account_num = "00018-1";
                            var account_num = t.ACCOUNT_NUM;
                            var claims = t.LINE_ITEM;
                            claims.forEach(function(t2){
                                var claim_dos = t2.DATE;
                                var claim_id = t2.ID;
                                var claim_charge = t2.CHARGE_AMOUNT;
                                var claim_paid = t2.PAYMENT_AMOUNT;
                                var claim_adj = t2.TOTAL_ADJ_AMOUNT;
                                var isDuplicate = t2.ADJUSTMENT_GROUP.TRANSLATED_REASON_CODE;
                                if(isDuplicate != "DUPLICATE CLAIM/SERVICE"){
                                    if(claim_id!=null && claim_id != "" && claim_id != "-1"){
                                        $.ajax({
                                            type: "POST",
                                            url: root_url + "claims/listdepositERA",
                                            async:false,
                                            data: {
                                                claim_number : claim_id,
                                            },
                                            success: function (result) {
                                                var res = JSON.parse(result);
                                                if(res.status == 0){
                                                    alert("Error on parsing this ERA!!");
                                                }
                                                else{
                                                    t3 = res.data[0];
                                                    if(t3.status != '3'){
                                                        var clsName = "";
                                                        if(t3.isPosted == '1'){
                                                            clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t3.claimID+'" /></td>';
                                                        }
                                                        else{
                                                            clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t3.claimID+'" /></td>';
                                                        }
                                                        if(t3.claimBalance == "0"){
                                                            balanceValue = t3.claimBalance;
                                                        }
                                                        else{
                                                            balanceValue = t3.claimBalance;
                                                        }
                                                        if(t3.claimBalancePat == "0.00"){
                                                            balanceValuePat = t3.claimBalancePat;
                                                        }
                                                        else{
                                                            balanceValuePat = t3.claimBalancePat;
                                                        }

                                                        var billed = t3.total;


                                                        if((t3.allowed == "" && t3.adjustment == "" && t3.paid == "") ||( t3.allowed == "0.00" && t3.adjustment == "0.00" && t3.paid == "0.00")){
                                                            $('#depBody').append('<tr>'+clsName+'<td>'+(t3.fromDt)+'</td><td>'+t3.proced+'</td><td>'+t3.mod1+'</td><td id="txtTot'+t3.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2"  id="txtDepAllowed'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+billed+'"/></td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+claim_paid+'"/></td><td><input type="text" class="form-control textbox3"  id="txtDepAdjust'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+claim_adj+'"/></td><td><select id="ddlreason'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t3.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                                            if(claim_adj == 0){
                                                                var id_ddl = "#ddlStatus"+t3.claimID;
                                                                $(id_ddl).val("3");
                                                            }
                                                            else{
                                                                var id_ddl = "#ddlStatus"+t3.claimID;
                                                                $(id_ddl).val("1");
                                                            }
                                                        }
                                                        else{
                                                            if(balanceValue <=0){
                                                                $('#depBody').append('<tr>'+clsName+'<td>'+(t3.fromDt)+'</td><td>'+t3.proced+'</td><td>'+t3.mod1+'</td><td id="txtTot'+t3.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+billed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t3.claimID+'" style="float:left; width:70px; border-radius:3px;" value="'+claim_paid+'"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t3.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                                                if(claim_adj == 0){
                                                                    var id_ddl = "#ddlStatus"+t3.claimID;
                                                                    $(id_ddl).val("3");
                                                                }
                                                                else{
                                                                    var id_ddl = "#ddlStatus"+t3.claimID;
                                                                    $(id_ddl).val("1");
                                                                }
                                                            }
                                                            else{
                                                                $('#depBody').append('<tr>'+clsName+'<td>'+(t3.fromDt)+'</td><td>'+t3.proced+'</td><td>'+t3.mod1+'</td><td id="txtTot'+t3.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+billed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t3.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t3.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                                                            }
                                                        }

                                                        // var status = "ddlStatus"+t3.claimID;
                                                        // if(t3.status != ""){
                                                        //     document.getElementById(status).value = status_new;
                                                        // }
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                             // $.ajax({
                             //        type: "POST",
                             //        url: root_url + "claims/listdepositERA",
                             //        async:false,
                             //        data: {
                             //            claim_number : account_num,
                             //        },
                             //        success: function (result) {
                             //            var res = JSON.parse(result);
                             //            if(res.status == 0){
                             //                alert("Error on parsing this ERA!!");
                             //            }
                             //            else{
                             //                res = res.data;
                             //                var arrDep1 = [];
                             //                //arrDep1.push(res);
                             //                arrDep1 = res;
                             //                //$.each(arrDep,function(i,v) {
                             //                arrDep1.forEach(function(t3) {
                             //                        if(t3.status != '3'){
                             //                            var clsName = "";
                             //                            if(t3.isPosted == '1'){
                             //                                clsName = '<td style="background-color:red"><input type="checkbox" class="chkBulk chkBulk'+t3.claimID+'" /></td>';
                             //                            }
                             //                            else{
                             //                                clsName = '<td style="background-color:white"><input type="checkbox" class="chkBulk chkBulk'+t3.claimID+'" /></td>';
                             //                            }
                             //                            if(t3.claimBalance == "0"){
                             //                                balanceValue = t3.claimBalance;
                             //                            }
                             //                            else{
                             //                                balanceValue = t3.claimBalance;
                             //                            }
                             //                            if(t3.claimBalancePat == "0.00"){
                             //                                balanceValuePat = t3.claimBalancePat;
                             //                            }
                             //                            else{
                             //                                balanceValuePat = t3.claimBalancePat;
                             //                            }

                             //                            var billed = t3.total;


                             //                            if((t3.allowed == "" && t3.adjustment == "" && t3.paid == "") ||( t3.allowed == "0.00" && t3.adjustment == "0.00" && t3.paid == "0.00")){
                             //                                $('#depBody').append('<tr>'+clsName+'<td>'+(t3.fromDt)+'</td><td>'+t3.proced+'</td><td>'+t3.mod1+'</td><td id="txtTot'+t3.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2"  id="txtDepAllowed'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+t3.allowed+'"/></td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3"  id="txtDepAdjust'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><select id="ddlreason'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t3.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                             //                            }
                             //                            else{
                             //                                if(balanceValue <=0){
                             //                                    $('#depBody').append('<tr>'+clsName+'<td>'+(t3.fromDt)+'</td><td>'+t3.proced+'</td><td>'+t3.mod1+'</td><td id="txtTot'+t3.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+t3.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t3.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3" selected>Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t3.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                             //                                }
                             //                                else{
                             //                                    $('#depBody').append('<tr>'+clsName+'<td>'+(t3.fromDt)+'</td><td>'+t3.proced+'</td><td>'+t3.mod1+'</td><td id="txtTot'+t3.claimID+'">'+billed+'</td><td><input type="text" class="form-control textbox2" id="txtDepAllowed'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+t3.allowed+'"/></td><td><input type="text" class="form-control textbox1" id="txtDepPayment'+t3.claimID+'" style="float:left; width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox3" id="txtDepAdjust'+t3.claimID+'" style="width:70px; border-radius:3px;" value="0.00"/></td><td><input type="text" class="form-control textbox7"  id="txtBalanceIns'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValue+'"/></td><td><input type="text" class="form-control textbox8"  id="txtBalancePat'+t3.claimID+'" style="width:70px; border-radius:3px;" value="'+balanceValuePat+'"/></td><td><select id="ddlreason'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t3.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option><option value="4">Denied</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t3.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a></td></tr>');
                             //                                }
                             //                            }

                             //                            var status = "ddlStatus"+t3.claimID;
                             //                            if(t3.status != ""){
                             //                                document3.getElementById(status).value = t3.status;
                             //                            }
                             //                        }
                             //                });
                             //            }
                             //        }
                             //    });
                        });
                    });
                }
            });
        });

       
        $(document).on('focusin','.textbox5',function() {
            var isTer = sessionStorage.getItem("txtboxPatPaid")
            if(isTer != "" && isTer != null){
                var tempValue1 = $(this).val();
                var tempID = $(this).attr('id');
                var resStr = tempID.replace('txtDepPatPayment','');
                var tempid = 'txtBalancePat'+resStr;
                var tempID1 = 'txtBalanceIns'+resStr;
                var tempTot = 'txtTot'+resStr;
                if(tempValue1 != ""){
                    var tempBal = document.getElementById(tempid).value;
                    var tempTotal = document.getElementById(tempTot).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    // var interApplied = AmtApplied - tempValue1;
                    // var interRemain = AmtRemain + tempValue1;
                    var FinalBal = tempTotal ;
                    var FinalApp = AmtApplied - parseFloat(tempValue1);
                    var FinalRem = AmtRemain + parseFloat(tempValue1);
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                        document.getElementById(tempid).value = "";
                    }
                }
            }
        });
        
        $(document).on('focus','#payor',function() {
            var ptype = $("#payorType").val();
            if(ptype=="2"){
                var patientList1 = "";
                patientList1 = root_url + "patient/list1";
                $("#payor").autocomplete({
                    source: patientList1,
                    autoFocus:true,
                    select: function( event, ui ) {
                        document.getElementById("payor").value = ui.item.id;
                      }
                });
            }
            else{
                var insurances = "";
                insurances = root_url + "insurances/get";
                $("#payor").autocomplete({
                    source: insurances,
                    autoFocus:true,
                    focus: function (event, ui) {
                       var val = ui.item.id;
                       event.preventDefault(); // Prevent the default focus behavior.
                    },
                    select: function( event, ui ) {
                        var label = ui.item.id;
                        var value = ui.item.value;
                      }
                });
            }

         });
        $('#depositDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#depositDate').mask('00-00-0000');

        $('#instrumentDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#instrumentDate').mask('00-00-0000');

        $('#loader').show();
        // //var pracID = sessionStorage.getItem('practiceId');
         loadDep();
    });


    function floorFigure(figure, decimals){
        if (!decimals) decimals = 2;
        var d = Math.pow(10,decimals);
        return (parseInt(figure*d)/d).toFixed(decimals);
    };

    function toFixed(value, n) {
        const f = Math.pow(10, n);
        return (Math.trunc(value*f)/f).toFixed(n);
    }

    function loadDep(){
        var root_url = $("#hidden_root_url").val();
        $.ajax({
            type: "POST",
            url:root_url + "deposits/loaddeposits",
            async: false,
            data:{
            },
            success:function(data){
                var res = JSON.parse(data);
                if(res.status == 0){
                    var dt = [];
                    var table = $('#deposit').DataTable({
                    "data": dt,
                    "bPaginate": false,
                    "bDestroy": true,
                    "bProcessing": true,
                    "aoColumns": [
                        {"mdata": "depositID","title":"Deposit ID", visible:false},
                        {"title":"Deposit Date","mdata": "Date",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title":"Cheque No","mdata": "Cheque No"},
                        {"title":"Cheque Date","mdata": "Cheque Date",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title":"Payor Name","mdata": "payorName"},
                        {"title":"Payor Type","mdata": "Payor Type", visible:false},
                        {"title":"Payment","mdata": "Payment",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"UnApplied","mdata": "Balance",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"Payment Method","mdata": "Payment Method"},
                        {"title":"Description","mdata": "Description"},
                        {
                            "title":"Actions",
                            "mdata": "Actions",
                            //mRender: function (data, type, row) { return '<a href="javascript:void(0);" onclick="apply('+row[0]+','+row[11]+','+row[5]+','+row[6]+')">Apply</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="viewDepDetails('+row[0]+')">View Details</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="editDep('+row[0]+')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="deleteDep('+row[0]+')"><i class="fa fa-trash"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="uploadDoc('+row[0]+')">Upload</a>'; }
                            mRender: function (data, type, row) { return '<a href="javascript:void(0);" onclick="apply('+row[0]+','+row[11]+','+row[5]+','+row[6]+')">Apply</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="viewDepDetails('+row[0]+')">View Details</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="editDep('+row[0]+')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="deleteDep('+row[0]+')"><i class="fa fa-trash"></i></a>'; }
                        },
                        {"title":"payorID","mdata": "payorID", "visible": false}
                    ]
                });
                    return;
                }
                var data1 = res.data;
                //alert(data);
                var dt = [];
                $.each(data1,function(i,v) {
                    var payerName = "";
                    if(data1[i].Balance == null){
                        data1[i].Balance = data1[i].amount;
                    }
                    if(data1[i].payor_type == '1'){
                        payerName = data1[i].payer_name;
                    }
                    else{
                        payerName = data1[i].patient_name;
                    }

                    dt.push([data1[i].id,data1[i].depositDate,data1[i].cheque_num,data1[i].chequeDate,payerName,data1[i].payor_type,data1[i].amount,data1[i].Balance,data1[i].payment_type,data1[i].notes,data1[i].notes,data1[i].payor_id]);
                });
                var table = $('#deposit').DataTable({
                    "data": dt,
                    "bPaginate": false,
                    "bDestroy": true,
                    "bProcessing": true,
                    "aoColumns": [
                        {"mdata": "depositID","title":"Deposit ID", visible:false},
                        {"title":"Deposit Date","mdata": "Date",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title":"Cheque No","mdata": "Cheque No"},
                        {"title":"Cheque Date","mdata": "Cheque Date",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title":"Payor Name","mdata": "payorName"},
                        {"title":"Payor Type","mdata": "Payor Type", visible:false},
                        {"title":"Payment","mdata": "Payment",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"UnApplied","mdata": "Balance",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"Payment Method","mdata": "Payment Method"},
                        {"title":"Description","mdata": "Description"},
                        {
                            "title":"Actions",
                            "mdata": "Actions",
                            //mRender: function (data, type, row) { return '<a href="javascript:void(0);" onclick="apply('+row[0]+','+row[11]+','+row[5]+','+row[6]+')">Apply</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="viewDepDetails('+row[0]+')">View Details</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="editDep('+row[0]+')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="deleteDep('+row[0]+')"><i class="fa fa-trash"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="uploadDoc('+row[0]+')">Upload</a>'; }
                            mRender: function (data, type, row) { return '<a href="javascript:void(0);" onclick="apply('+row[0]+','+row[11]+','+row[5]+','+row[6]+')">Apply</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="viewDepDetails('+row[0]+')">View Details</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="editDep('+row[0]+')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="deleteDep('+row[0]+')"><i class="fa fa-trash"></i></a>'; }
                        },
                        {"title":"payorID","mdata": "payorID", "visible": false}
                    ]
                });
                $('#loader').hide();
            }
        });
    }

    function uploadDoc(id){
        var depositID = id;
        sessionStorage.setItem("deposID",depositID);
        window.location.href="depUpload.php";
    }

    function viewDepDetails(id){
        var root_url = $("#hidden_root_url").val();
        $.ajax({
            type: "POST",
            url:root_url + "/claims/listbydeposit",
            data:{
                depositID : id
            },success:function(result){
                $('#depositDetails').show();
                var data1 = JSON.parse(result).data;
                var dt = [];
                $.each(data1,function(i,v) {
                    var currPayer = "";
                    if(data1[i].adjustment_notes == "1"){
                        data1[i].adjustment_notes = "Patient Responsibility";
                    }
                    else if(data1[i].adjustment_notes == "2"){
                        data1[i].adjustment_notes = "Bill to Next Responsibility";
                    }
                    else{
                        data1[i].adjustment_notes = "Closed";
                    }
                    if(data1[i].adjustment_code == "" || data1[i].adjustment_code==null || data1[i].adjustment_code == undefined){
                        data1[i].adjustment_code == "-";
                    }
                    $.ajax({
                        type: "POST",
                        url: root_url+"insurances/getByClaimId",
                        async :false,
                        data:{
                            claimID : data1[i].claimID
                        },success:function(result){
                            var result = JSON.parse(result).data;
                            currPayer = result.payer_name;
                        }
                    });
                    dt.push([id,data1[i].claimID,data1[i].fullName,data1[i].to_date,data1[i].proced,data1[i].total,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claim_balance_ins,data1[i].claim_balance_pat,data1[i].adjustment_code,data1[i].adjustment_notes,currPayer]);
                });
                var table = $('#depositDetails').DataTable({
                    "data": dt,
                    "bPaginate": false,
                    "bDestroy": true,
                    "bProcessing": true,
                    "aoColumns": [
                        {"mdata": "depositID","title":"Deposit ID", visible:false},
                        {"mdata": "claimID","title":"Claim ID", visible:false},
                        {"title":"Client","mdata": "fromDt",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title":"DOS","mdata": "toDt",
                            "render": function ( data, type, full, meta ) {
                                return changeDateFormat(data);
                            }
                        },
                        {"title":"CPT","mdata": "proced",
                            "render": function ( data, type, full, meta ) {
                                return data;
                            }
                        },
                        {"title":"Total","mdata": "total",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"Allowed","mdata": "allowed",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"Paid","mdata": "paid",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"Adjustment","mdata": "adjustment",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"Ins. Balance","mdata": "claimBalance",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"Pat. Balance","mdata": "claimBalance",
                            "render": function ( data, type, full, meta ) {
                                return '$'+data;
                            }
                        },
                        {"title":"Reason","mdata": "reason"},
                        {"title":"Status","mdata": "status"},
                        {"title":"Insurance","mdata": "insuranceID"}
                    ]
                });
            }
        });
    }

    function editDep(id){
        var root_url = $("#hidden_root_url").val();
        $.ajax({
            type: "POST",
            url: root_url+"deposits/get",
            async : false,
            data:{
                depositID : id
            },success:function(result){
                $('#EditDeposit').modal('show');
                populateEditPayor();
                var res = JSON.parse(result);
                if(res[0].payorType == '1'){
                    var ID = res[0].payorID;
                    $("#EditpayorType").val(res[0].payorType);
                    $.ajax({
                        type: "POST",
                        url: root_url+"insurances/get",
                        async: false,
                        data:{
                            "insuranceID" : res[0].payorID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#Editpayor").val(ID);
                        }
                    });
                }
                else{
                    $.ajax({
                        type: "POST",
                        url: root_url+"patient/list",
                        async: false,
                        data:{
                            "patientID" : res[0].payorID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#Editpayor").val(res[0].patientID+" - "+res[0].fullName);
                        }
                    });
                    $("#EditpayorType").val("2");
                    document.getElementById('lblType').innerHTML = "Client";
                }
                document.getElementById('EditpaymentType').value = res[0].paymentType;
                document.getElementById('EditinstrumentNo').value = res[0].chequeNo;
                document.getElementById('EditinstrumentDate').value = changeDateFormat(res[0].chequeDate);
                document.getElementById('Editamount').value = res[0].amount;
                document.getElementById('Editnotes').value = res[0].notes;
                document.getElementById('EditdepositDate').value = changeDateFormat(res[0].depositDate);
                document.getElementById('_hdnDepID').value = res[0].depositID;
            }
        });
    }

    function deleteDep(id){
        var root_url = $("#hidden_root_url").val();
        $.ajax({
            type: "POST",
            url: root_url+"deposits/check",
            async : false,
            data:{
                depositID : id
            },success:function(result){
                if(result=="red"){
                    var x = confirm("Are you sure you want to delete?");
                    if (x){
                        $.ajax({
                            type: "POST",
                            url: root_url+"deposits/delete",
                            async : false,
                            data:{
                                depositID : id
                            },success:function(result){
                                alert(result);
                                window.location.reload();
                            }
                        });
                    }
                    else{
                        return false;
                    }
                }
                else{
                    alert("Deposit utilized, So you can't delete it");
                }
            }
        });
    }

    function apply(depositId,payorId,payorType,depAmt){
        var root_url = $("#hidden_root_url").val()+"deposits/post";
        sessionStorage.setItem("depID",depositId);
        sessionStorage.setItem("depPayor",payorId);
        sessionStorage.setItem("depPayorType",payorType);
        sessionStorage.setItem("depAmt",depAmt);
        window.location.href = root_url;
    }
    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2];

        return month + '-' + day + '-' + year;
    }
    function changeDate(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[2];
        var month = splitDate[0];
        var day = splitDate[1];

        return year+ '-' + month + '-' + day;
    }
</script>