@extends('layout.app')
@section('content')

<div class="row">
	<h2 style="color: #251367; margin-left:20px;">Service Location list</h2>
	<div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">
        	<a href="javascript:void(0)" class="btn btn-primary" id="linkNewLocation"><i class="icon icon-plus"></i>&nbsp;Add New Service Location</a><br/><br/>
				<table id="serviceLoc" class="table display" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
				</table>
        </div>
    </div>

</div>

<div class="modal fade" id="AddServLoc" tabindex="-1">
	<div class="modal-dialog modal-lg" style="width:60%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="lblTitle">Add New Service Location</h4>
			</div>
			<div class="modal-body" style="min-height:150px;">
                <div class="card-body">
                    <div class="row">
                        <input type="hidden" id="hdnFlag"/>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Internal Name </label>
                            <div class="col-md-12">
                                <input type="text" id="internalName" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Time Zone </label>
                            <div class="col-md-12">
                                <input type="text" id="timeZone" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">NPI </label>
                            <div class="col-md-12">
                                <input type="text" id="npi" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Legacy Number Type </label>
                            <div class="col-md-12">
                                <input type="text" id="legacyType" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Legacy Number </label>
                            <div class="col-md-12">
                                <input type="text" id="legacyNo" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Place Of Services </label>
                            <div class="col-md-12">
                                <input type="text" id="POS" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="checkbox-inline col-md-5">
                                <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="EIN"> &nbsp;&nbsp;Override EIN ?
                            </label>
                        </div>
                        <div class="clearfix"></div>
                                                        
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Billing Name &amp; Address</span></label><br/>
                        </div>
                        <div class="clearfix"></div>
                        <hr style="margin-top:0;margin-bottom:0;">
                        <span style="margin:0px 0px 15px 20px;">The name and Address of the service location for the provider submitting the bill</span>
                        <br/>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Billing Name </label>
                            <div class="col-md-12">
                                <input type="text" id="billingName" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Phone Number </label>
                            <div class="col-md-12">
                                <input type="text" id="phone" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Fax </label>
                            <div class="col-md-12">
                                <input type="text" id="fax" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label col-md-12">Address </label>
                            <div class="col-md-12">
                                <textarea class="form-control" id="address"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
				<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
				<input type="button" id="btnSaveServLoc" class="btn btn-primary" data-dismiss="modal" value="Save" />
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
@stop
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script>


        $(document).ready(function(){
            var practiceId = sessionStorage.getItem('practiceId');
            $('#linkNewLocation').click(function(){
                $('#AddServLoc').modal('show');
                document.getElementById("hdnFlag").value = 0;
            })
            var Places = "";
            $.ajax({
                type: "POST",
                url: root_url + "pos/list",
                data: {
                    practiceID: practiceID
                },
                success: function (result) {
                    Places = result;
                }
            });
            $("#POS").autocomplete({
                source: Places,
                autoFocus:true
            });
            $(document).on('click','.editServLoc',function() {
                var temp = $(this).attr('class').split(' ')[0];
                var servID = temp.replace('edit','');
                document.getElementById("hdnFlag").value = servID;
                $.ajax({
                    type: "POST",
                    url: root_url+"pos/getinfo",
                    async : false,
                    data:{
                        serviceLocID : servID
                    },success:function(result){
                        $('#AddServLoc').modal('show');
                        var res = JSON.parse(result);
                        document.getElementById('lblTitle').value = "Edit Service Location";
                        document.getElementById('btnSaveServLoc').value = "Update Location";
                        document.getElementById('internalName').value = res[0].internalName;
                        document.getElementById('npi').value = res[0].NPI;
                        document.getElementById('timeZone').value = res[0].timeZone;
                        document.getElementById('legacyType').value = res[0].legacyNoType;
                        document.getElementById('legacyNo').value = res[0].legacyNo;
                        document.getElementById('billingName').value = res[0].billingName;
                        document.getElementById('address').value = res[0].address;
                        document.getElementById('phone').value = res[0].phone;
                        document.getElementById('fax').value = res[0].fax;
                        document.getElementById('POS').value = res[0].placeOfService;
                    }
                });
            });
            var serv_url = root_url + 'location/list';
            $.post(serv_url,
                {
                    practiceID: practiceId
                },
                function(data1, status){
                    var dt = [];
                    data1 = JSON.parse(data1);
                    $.each(data1,function(i,v) {
                        dt.push([data1[i].serviceLocID,data1[i].billingName,data1[i].NPI,data1[i].placeOfService,data1[i].address,data1[i].serviceLocID]);
                    });
                    //alert(dt);
                    var table = $('#serviceLoc').DataTable({
                        "data": dt,
                        "bProcessing": true,
                        "aoColumns": [
                            {"mdata": "serviceLocID","title":"ID", "visible": false},
                            {"title":"Location Name","mdata": "billingName"},
                            {"title":"NPI","mdata": "NPI", "width":"25%"},
                            {"title":"Place Of Service","mdata": "placeOfService"},
                            {"title":"Address","mdata": "address"},
                            {"title":"Action",
                                "render": function ( data, type, full, meta ) {
                                    return '<a href="javascript:void(0);" class="edit'+data+' editServLoc"><span class="ti-pencil"></span></a>';
                                }
                            }
                        ]
                    });
                });
            $('#btnSaveServLoc').click(function(){
                var hdnFlag = document.getElementById("hdnFlag").value;
                if(hdnFlag == 0){
                    var internalName = $('#internalName').val();
                    var placeServ = $('#POS').val();
                    var n=placeServ.indexOf("-");
                    var POS = placeServ.substr(0,n-1);
                    var timeZone = $('#timeZone').val();
                    var npi = $('#npi').val();
                    var legacyNo = $('#legacyNo').val();
                    var legacyType = $('#legacyType').val();
                    var billingName = $('#billingName').val();
                    var phone = $('#phone').val();
                    var fax = $('#fax').val();
                    var address = $('#address').val();
                    var practiceID = sessionStorage.getItem("practiceId");
                    $.ajax({
                        type: "POST",
                        url: root_url+"location/add",
                        data:{
                            "internalName" : internalName,
                            "NPI" : npi,
                            "timeZone" : timeZone,
                            "legacyNoType" : legacyType,
                            "legacyNo" : legacyNo,
                            "billingName" : billingName,
                            "address" : address,
                            "phone" : phone,
                            "fax" : fax,
                            "placeOfService" : POS,
                            "practiceID" : practiceID,
                        },success:function(result){
                            alertify.success("New Service Location is successfully added");
                        }
                    });
                }
                else{
                    var internalName = $('#internalName').val();
                    var placeServ = $('#POS').val();
                    var n=placeServ.indexOf("-");
                    var POS = placeServ.substr(0,n-1);
                    var timeZone = $('#timeZone').val();
                    var npi = $('#npi').val();
                    var legacyNo = $('#legacyNo').val();
                    var legacyType = $('#legacyType').val();
                    var billingName = $('#billingName').val();
                    var phone = $('#phone').val();
                    var fax = $('#fax').val();
                    var address = $('#address').val();
                    var practiceID = sessionStorage.getItem("practiceId");
                    var serviceLocID = document.getElementById("hdnFlag").value;
                    $.ajax({
                        type: "POST",
                        url: root_url+"location/update",
                        data:{
                            "serviceLocID" : serviceLocID,
                            "internalName" : internalName,
                            "NPI" : npi,
                            "timeZone" : timeZone,
                            "legacyNoType" : legacyType,
                            "legacyNo" : legacyNo,
                            "billingName" : billingName,
                            "address" : address,
                            "phone" : phone,
                            "fax" : fax,
                            "placeOfService" : POS,
                            "practiceID" : practiceID,
                        },success:function(result){
                            alertify.success("Service Location updated successfully.");
                        }
                    });
                }
            });
        });
    </script>