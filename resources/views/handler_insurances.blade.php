@extends('layout.handler')
@section('content')

<?php
    $base_url = URL::to('/');
?>

<div class="row" style="margin: 5%; ">
    <a href="javascript:void(0)" class="btn btn-primary" id="linkNewIns"><i class="icon icon-plus"></i>&nbsp;Add New Insurance</a>
    <table id="insuranceList" class="table display" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
    </table>
</div>

@stop
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){

        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        //document.getElementById('practiceName1').innerHTML = sessionStorage.getItem('practiceName');
        //document.getElementById("lblUser").innerHTML = {{Session::get('LOGGED_USER_NAME')}};
        var root_url = $("#hidden_root_url").val();
        //alert('i am here layout=>app.blade.php' + root_url);
        $("#linkLogout").click(function(){
            sessionStorage.removeItem('practiceName');
            sessionStorage.removeItem('userID');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('loginName');
        })

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })

        var practiceId = "1";

        
        $('#btnSaveInsurance').click(function(){

            document.getElementById('btnSaveInsurance').value = "Add Insurance";

            document.getElementById('lblTitle').innerHTML = "Add New Insurance";

            var hdnFlag = document.getElementById("hdnFlag").value;
            alert(root_url);

            if(hdnFlag == 0){

                var payerName = $('#payerName').val();

                var clearingHousePayorID = $('#clearingHousePayorID').val();

                var addrStrt1 = $('#addrStrt1').val()+ ",";

                if($('#addrStrt2').val() == ""){

                    var addrStrt2 = "";

                }

                else{

                    var addrStrt2 = $('#addrStrt2').val()+ ",";

                }



                var addrCity = $('#addrCity').val()+ " ";

                var addrState = $('#addrState').val()+ " ";

                var addrZip = $('#addrZip').val();

                var addr = addrStrt1+addrStrt2+addrCity+addrState+addrZip;

                //var practiceID = sessionStorage.getItem("practiceId");

                $.ajax({

                    type: "POST",

                    url: root_url+"handler/insurances/add",

                    data:{

                        "payerName" : payerName,

                        "clearingHousePayorID" : clearingHousePayorID,

                        "addr" : addr,

                        "practiceID" : practiceId,

                    },success:function(result){
                        var res = JSON.parse(result);
                        // new PNotify({
                        //     type:'success',
                        //     text: json.message,
                        //     after_init: function(notice){
                        //         notice.attention('rubberBand');
                        //     }
                        // })
                        alert(res.data);

                    }

                });

            }

            else{

                var practiceID = sessionStorage.getItem("practiceId");

                var insuranceID = document.getElementById("hdnFlag").value;

                $.ajax({

                    type: "POST",

                    url: root_url+"insurances/update",

                    data:{

                        "insuranceID" : insuranceID,

                        "payerName" : payerName,

                        "clearingHousePayorID" : clearingHousePayorID,

                        "addr" : addr,

                        "practiceID" : practiceID,

                    },success:function(result){

                        var res = JSON.parse(result);
                        new PNotify({
                            type:'success',
                            text: json.message,
                            after_init: function(notice){
                                notice.attention('rubberBand');
                            }
                        })

                    }

                });

            }

        });

    });

</script>

</body>

</html>