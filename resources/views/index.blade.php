<!-- index.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>List Employee</title>
    <link rel="stylesheet" href="{{asset('public/css/app.css')}}">
  </head>
  <body>
    <div class="container">

<nav class="navbar">
  <div class="navbar-header">
  </div>
  <ul class="nav navbar-nav">
    <li>
      <a  href="{{ URL::to('users/create') }}">Add new</a> | 
      <a  href="{{ URL::to('users') }}">List</a> |
      <a  href="{{ URL::to('users/create') }}">Login</a> | 
      <a  href="{{ URL::to('users/create') }}">Register</a> | 
    <li>
  </ul>
</nav>

    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-border" >
    <thead>
      <tr>
        <th align="center">User ID</th>
        <th align="center">User Name</th>
        <th align="center">Actions</th>
      </tr>
    </thead>
    <tbody>

      @foreach($employees as $employee)
      <tr>
        <td>{{$employee['id']}}</td>
        <td>{{$employee['name']}}</td>

        <td align="right">
          <a  class="btn btn-success" href="{{ URL::to('users/create') }}">Add New</a>

          <a href="{{action('EmployeeController@edit', $employee['id'])}}" class="btn btn-warning">Edit</a>
        </td>

        <td align="left">
          <form action="{{action('EmployeeController@destroy', $employee['id'])}}" 
          method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
            <a  class="btn btn-success" href="{{ URL::to('permission') }}">Permission {{ URL::to('permission') }}</a>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>