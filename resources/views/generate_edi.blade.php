@extends('layout.app')
@section('content')
 <h3 style="margin:20px 20px 20px 20px;">Generate EDI</h3>
  <div class="clearfix"></div>
  <div class="form-group col-md-12" style="margin-bottom:50px;">
      <input type="button" class="btn btn-primary" value="Submit EDI" id="btnGenerate"/>
  </div>
 @stop
<style>
    .icon:hover{
        text-decoration: none;
    }
    a:hover{
        text-decoration: none;
    }
</style>

<script>
    $(window).load(function() {
        "use strict";

        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        $(".navigation li").parent().find('li').removeClass("active");

        $("#navv3").addClass("active");
        $('#loader').show();
        $.post(root_url + "/patients",
            {
                practiceID: '1',
            },
            function(data, status){
                $('#patient').html('');
                $('#loader').hide();
                data.forEach(function(t) {
                    $('#patient').append('<option value="'+t.patientID+'">'+t.fullName+'</option>');
                });
            });
        var inv = "";
        $.ajax({
            type: "GET",
            url: root_url+"edi/getlast",
            async : false,
            data:{
            },success:function(result){
                inv = parseInt(result)+1;
                inv = pad(inv, 5);
            }
        });
        $('#btnGenerate').click(function(){
            $.ajax({
                type: "POST",
                url: root_url+"patient/claimslist",
                data:{
                    practiceID : "1"
                },success:function(result){
                    var n = result.length;
                    //result = result.substr(0,n-1);
                    var res = JSON.parse(result);
                    if(res.length != 0){
                        for(var i=0; i< res.length;i++){
                            var chNo = res[i].chartNumber;
                            var ClNo = res[i].ClaimNumber;
                            $.ajax({
                                type: "POST",
                                url:"EDI_new.php",
                                async:false,
                                data:{
                                    ClaimNumber : ClNo,
                                    chartNumber : chNo
                                },success:function(result){
                                    alertify.success("EDI successfully generated");
                                }
                            });
                        }
                        $.ajax({
                            type: "POST",
                            url: root_url+"edi/unset",
                            data:{
                            },success:function(result){
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:"test.php",
                            data:{
                            },success:function(result){
                            }
                        });
                    }
                    else{
                        alert("No Claims are to be generated");
                    }
                }
            });
        });
    });

    function pad(number, length) {

        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }

        return str;

    }
    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2];

        return month + '-' + day + '-' + year;
    }
</script>