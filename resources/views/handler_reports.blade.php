<?php
    $base_url = URL::to('/');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>AMROMED LLC </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="<?php echo $base_url; ?>/public/img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>/public/css/app.css"/>
    <!-- end of global css -->
    <!--page level css -->
    <link href="<?php echo $base_url; ?>/public/vendors/swiper/css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/nvd3/css/nv.d3.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $base_url; ?>/public/vendors/lcswitch/css/lc_switch.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/select2/css/select2.min.css" type="text/css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/select2/css/select2-bootstrap.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/public/vendors/animate/animate.min.css">
    <link href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.brighttheme.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.buttons.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.mobile.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/vendors/pnotify/css/pnotify.history.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/vendors/laddabootstrap/css/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/vendors/weathericon/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/vendors/metrojs/css/MetroJs.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/css/custom.css" rel="stylesheet" type="text/css" >
    <link href="<?php echo $base_url; ?>/public/css/custom_css/wizard.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/css/advbuttons.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/public/css/custom_css/dashboard2.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/css/custom_css/dashboard1.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $base_url; ?>/public/css/custom_css/dashboard1_timeline.css" rel="stylesheet"/>
    <link href="<?php echo $base_url; ?>/public/css/jquery.datetimepicker.css" rel="stylesheet" />
    <link href="<?php echo $base_url; ?>/public/css/alertify.css" rel='stylesheet' type='text/css'>
    <link href="<?php echo $base_url; ?>/public/css/default.css" rel='stylesheet' type='text/css'>
    <link href="<?php echo $base_url; ?>/public/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $base_url; ?>/public/css/mini_sidebar.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        table.datatable{
            font-size:12px;
        }
        ::-webkit-input-placeholder {
            font-size: 11px;
        }
        :-moz-placeholder { /* Firefox 18- */
            font-size: 11px;
        }
        ::-moz-placeholder {  /* Firefox 19+ */
            font-size: 11px;
        }
        /* Overriding styles */
        ::-webkit-input-placeholder {
            font-size: 12px!important;
        }
        :-moz-placeholder { /* Firefox 18- */
            font-size: 12px!important;
        }
        ::-moz-placeholder {  /* Firefox 19+ */
            font-size: 12px!important;
        }
        .ui-autocomplete-input {
            border: none;
            font-size: 11px;
            z-index: 1000;
            height: 24px;
            margin-bottom: 5px;
            padding-top: 2px;
            border: 1px solid #DDD !important;
            padding-top: 0px !important;
            z-index: 1511;
            position: relative;
        }
        .ui-menu .ui-menu-item a {
            font-size: 11px;
            color:#fff;
        }
        .ui-autocomplete {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 99999999 !important;
            float: left;
            display: none;
            min-width: 160px;
            width: 160px;
            padding: 4px 0;
            margin: 2px 0 0 0;
            list-style: none;
            background-color: #ffffff;
            border-color: #ccc;
            border-color: rgba(0, 0, 0, 0.2);
            border-style: solid;
            border-width: 1px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;
            -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            -webkit-background-clip: padding-box;
            -moz-background-clip: padding;
            background-clip: padding-box;
            *border-right-width: 2px;
            *border-bottom-width: 2px;
        }
        .ui-menu-item > a.ui-corner-all {
            display: block;
            padding: 3px 15px;
            clear: both;
            font-weight: normal;
            line-height: 18px;
            color:#000;
            white-space: nowrap;
            text-decoration: none;
        }
        .ui-state-hover {
            color: #fff;
            text-decoration: none;
            background-color: #0088cc;
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            background-image: none;
        }
        .ui-state-active {
            color: #fff;
            text-decoration: none;
            background-color: #0088cc;
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            background-image: none;
        }
        .icon:hover{
            text-decoration: none;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
    <!--end of page level css-->
</head>
<body class="skin-default mini_sidebar">
<div class="preloader">
    <div class="loader_img"><img src="<?php echo $base_url; ?>/public/img/loader.gif" alt="loading..." height="64" width="64"></div>
</div>
<!-- header logo: style can be found in header-->
<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="{{ URL::to('index')    }}" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the marginin -->
            <img src="<?php echo $base_url; ?>/public/img/logo_white.png" alt="logo"/>
        </a>
        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <div class="mr-auto">
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <i class="fa fa-fw ti-menu-alt"></i>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw ti-email black"></i>
                        <span class="badge badge-pill badge-success">2</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages table-striped">
                        <li class="dropdown-title">New Messages</li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar7.jpg" alt="avatar-image">
                                <div class="message-body"><strong>Ernest Kerry</strong>
                                    <br>
                                    Can we Meet?
                                    <br>
                                    <small>Just Now</small>
                                    <span class="badge badge-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar6.jpg" alt="avatar-image">
                                <div class="message-body"><strong>John</strong>
                                    <br>
                                    Dont forgot to call...
                                    <br>
                                    <small>5 minutes ago</small>
                                    <span class="badge badge-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar5.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Wilton Zeph</strong>
                                    <br>
                                    If there is anything else &hellip;
                                    <br>
                                    <small>14/10/2014 1:31 pm</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar1.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Jenny Kerry</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="<?php echo $base_url; ?>/public/img/authors/avatar.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Tony</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-footer"><a href="#"> View All messages</a></li>
                    </ul>
                </li>
                <!--rightside toggle-->
                <li>
                    <a href="#" class="dropdown-toggle toggle-right" data-toggle="dropdown">
                        <i class="fa fa-fw ti-view-list black"></i>
                        <span class="badge badge-pill badge-danger">9</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown-->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle padding-user d-block" data-toggle="dropdown">
                        <img src="<?php echo $base_url; ?>/public/img/authors/avatar1.jpg" width="25" class="rounded-circle img-fluid float-left"
                             height="25" alt="User Image">
                        <div class="riot">
                            <div id="practiceName"><?php if(!empty(Session::get('LOGGED_USER_PRATICE_NAME'))) { echo Session::get('LOGGED_USER_PRATICE_NAME'); } ?>
                                <span><i class="fa fa-sort-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo $base_url; ?>/public/img/authors/avatar1.jpg" class="rounded-circle" alt="User Image">
                            
                            <p id="practiceName1"><?php if(!empty(Session::get('LOGGED_USER_PRATICE_NAME'))) { echo Session::get('LOGGED_USER_PRATICE_NAME'); } ?></p>
                        </li>
                        <!-- Menu Body -->
                        <li class="p-t-3"><a href="javascript:void(0)" class="dropdown-item"> <i class="fa fa-fw ti-user"></i> My Profile </a>
                        </li>
                        <li role="presentation"></li>
                        <li><a href="javascript:void(0)" class="dropdown-item"><i class="fa fa-fw ti-settings"></i> Account Settings </a></li>
                        <li role="presentation" class="dropdown-divider"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="float-left">
                                <a href="{{ URL::to('lockscreen')   }}">
                                    <i class="fa fa-fw ti-lock"></i>
                                    Lock
                                </a>
                            </div>
                            <div class="float-right">
                                <a href="{{ URL::to('logout') }}?session_token=token" id="linkLogout">
                                    <i class="fa fa-fw ti-shift-right"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <input type="hidden" id="hidden_root_url" name="hidden_root_url"
           value="{{ URL::to('/') }}/">
    <div class="row" style="margin: 5%; ">
        <div class="col-sm-2">
            <a href=""><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/insurance.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Insurance</span>
            </div></a>
        </div>
        <div class="col-sm-2">
            <a href=""><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/activity.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Activity</span>
            </div></a>
        </div>
        <div class="col-sm-2">
            <a href=""><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/session.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Session Rule</span>
            </div></a>
        </div>
        <div class="col-sm-2">
            <a href=""><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/cpt.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>CPTs</span>
            </div></a>
        </div>
        <div class="col-sm-2">
            <a href=""><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/users.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Users</span>
            </div></a>
        </div>
        <div class="col-sm-2">
            <a href=""><div style="width:70%; border:1px solid #ccc; padding: 10px;">
                <img src="<?php echo $base_url; ?>/public/img/handler/reports.png" style="width:100%; margin:auto;">
            </div>
            <div style="width:70%;text-align: center;">
                <span>Reports</span>
            </div></a>
        </div>
    </div>
    <div class="row" style="margin: 5%; ">    

    </div>
    
<div id="qn"></div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="<?php echo $base_url; ?>/public/js/app.js" type="text/javascript"></script>
<!-- end of global js -->

<!-- begining of page level js -->

<!--swiper-->
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/alertify.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/swiper/js/swiper.min.js"></script>
<link href="public/css/bootstrap-duallistbox.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="public/js/jquery.bootstrap-duallistbox.js"></script>
<!-- <script src="<?php echo $base_url; ?>/public/js/dashboard2.js" type="text/javascript"></script> -->

<!--chartjs-->
<script src="<?php echo $base_url; ?>/public/vendors/chartjs/js/Chart.js"></script>
<!--nvd3 chart-->
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/nvd3/d3.v3.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/nvd3/js/nv.d3.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/moment/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/advanced_newsTicker/js/newsTicker.js"></script>
<script src="<?php echo $base_url; ?>/public/vendors/iCheck/js/icheck.js"></script>
<script src="<?php echo $base_url; ?>/public/vendors/moment/js/moment.min.js"></script>
<script src="<?php echo $base_url; ?>/public/vendors/select2/js/select2.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/js/custom_js/form_wizards.js" type="text/javascript"></script>
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end of page level js -->
<script src="<?php echo $base_url; ?>/public/vendors/moment/js/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/vendors/timedropper/js/timedropper.js" type="text/javascript"></script>
<!-- date-range-picker -->
<script src="<?php echo $base_url; ?>/public/vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/jquery.mask.min.js"></script>

<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/Buttons/js/buttons.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/laddabootstrap/js/spin.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/laddabootstrap/js/ladda.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/custom_js/button_main.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.animate.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.buttons.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.confirm.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.nonblock.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.mobile.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.desktop.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.history.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/vendors/pnotify/js/pnotify.callbacks.js"></script>
<script src="<?php echo $base_url; ?>/public/js/custom_js/notifications.js"></script>
<link rel="stylesheet" href="<?php echo $base_url; ?>/public/Scheduler/dhtmlxscheduler.css" type="text/css"  title="no title" charset="utf-8">
<script src="<?php echo $base_url; ?>/public/Scheduler/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>

<script>
    $(document).ready(function(){
        //alert(sessionStorage.getItem("patientId"));
        //document.getElementById('hdntoggleDX').value = "false";
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        //document.getElementById('practiceName1').innerHTML = sessionStorage.getItem('practiceName');
        //document.getElementById("lblUser").innerHTML = {{Session::get('LOGGED_USER_NAME')}};
        var root_url = $("#hidden_root_url").val();
        //alert('i am here layout=>app.blade.php' + root_url);
        $("#linkLogout").click(function(){
            sessionStorage.removeItem('practiceName');
            sessionStorage.removeItem('userID');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('loginName');
        })

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })
    });
</script>

</body>

</html>