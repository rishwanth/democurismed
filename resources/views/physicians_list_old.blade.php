@extends('layout.app')
@section('content')
 
<div class="row">
	<h2 style="color: #251367; margin-left:20px;">Find Employees</h2>
	<div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">
        	<a href="#" id="linkPrimaryPhy" class="btn btn-primary" style="text-decoration:none"><i class="icon icon-plus"></i>&nbsp;&nbsp;Add Employee</a><br/><br/>
				<table id="physicianList" class="table display table-bordered" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
				</table>
        </div>
    </div>

</div>

				
	<div class="modal fade" id="PrimaryPhysi" tab-index="-1">
		<div class="modal-dialog modal-lg" style="width:70%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Primary Employee</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:10px;">
					<input type="hidden" id="hdnFlag"/>
					<div class="card-body">
						<div class="row">
			              	<div class="form-group col-md-6">
			                    <label class="control-label col-md-4">Full Name </label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control required" id="PrimaryPhysiName" name="PrimaryPhysiName" placeholder="Name of the Primary Employee" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6">
			                	<label class="control-label col-md-4">Speciality </label>
			                    <div class="col-md-8">
			                        <select class="form-control" id="phySpeciality">
			                        	<option value=""></option>
			                        	<option value="Behaviour Therapy">Behaviour Therapy</option>
		                                <option value="Physical Therapy">Physical Therapy</option>
		                                <option value="Occupational Therapy">Occupational Therapy</option>
		                                <option value="Speech Therapy">Speech Therapy</option>
			                        </select>
			                    </div>
			            	</div>
			            	<div class="clearfix"></div>

			            	<div class="form-group col-md-6">
			                    <label class="control-label col-md-4">Date Of Birth </label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control required" id="phyDOB" name="txtPTNumber" placeholder="Date Of Birth of the Primary Employee" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-4">Individual NPI </label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control required" id="phyNPI" name="txtPTNumber" placeholder="Individual NPI of the Primary Employee" value="" />
			                    </div>
			                </div>

			            	<div class="form-group col-md-6">
			                    <label class="control-label col-md-4">Social Security # </label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control required" id="phySSN" name="txtPTNumber" placeholder="SSN of the Primary Employee" value="" />
			                    </div>
			                </div>
			            </div>
			        </div>
	                <div class="clearfix"></div>
	                <hr/>
	                <div class="card-body">
						<div class="row">
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-4">Address </label>
			                    <div class="col-md-8">
			                    	<textarea class="form-control" cols="5" rows="4" id="phyAddr"></textarea>
			                    </div>
			                </div>

			                <div class="form-group col-md-6" >
			                    <label class="control-label col-md-4">Home</label>
			                    <div class="col-md-8">
			                         <input type="text" class="form-control required" id="phyHomePhone" name="txtPTNumber" placeholder="Enter the Home Phone" value="" />
			                    </div>
			                </div>

			                 <div class="form-group col-md-6">
			                    <label class="control-label col-md-4">Work </label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control required" id="phyWorkPhone" name="txtPTNumber" placeholder="Enter the Work Phone" value="" />
			                    </div>
			                </div>

			                <div class="form-group col-md-6" >
			                    <label class="control-label col-md-4">Mobile </label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control required" id="phyMobilePhone" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="" />
			                    </div>
			                </div>
			                <div class="clearfix"></div>
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-4">Email </label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control required" id="phyEmail" name="txtPTNumber" placeholder="Enter the Email" value="" />
			                    </div>
			                </div>
			                <div class="form-group col-md-6">
			                    <label class="control-label col-md-4">Fax </label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control required" id="phyFax" name="txtPTNumber" placeholder="Enter the Fax" value="" />
			                    </div>
			                </div>
			            </div>
			        </div>
	                <div class="clearfix"></div>
	                <hr/>
	                <div class="card-body">
						<div class="row">
			                <div class="form-group col-md-12">
			                    <label class="control-label col-md-2">Notes </label>
			                    <div class="col-md-8">
			                    	<textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Add Primary Employee" class="btn btn-primary" id="btnAddPriPhy"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
@stop
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script>


	$(document).ready(function(){
		var root_url = $("#hidden_root_url").val();
		$('#linkPrimaryPhy').click(function(){
			document.getElementById("hdnFlag").value = 0;
			$('#PrimaryPhysi').modal('show');
		});

		$(document).on('click','.editPhy',function() {
			var temp = $(this).attr('class').split(' ')[0];
			var phyID = temp.replace('edit','');
			document.getElementById("hdnFlag").value = phyID;
			$.ajax({
				type: "POST",
				url: root_url+"physician/get",
				async : false,
				data:{
					physicianID : phyID
				},success:function(result){
					$('#PrimaryPhysi').modal('show');
					var res = JSON.parse(result);
					document.getElementById("PrimaryPhysiName").value = res[0].name;
					document.getElementById("phySpeciality").value = res[0].speciality;
					document.getElementById("phyDOB").value = changeDateFormat(res[0].dob);
					document.getElementById("phyNPI").value = res[0].individual_npi;
					document.getElementById("phySSN").value = res[0].ssno;
					document.getElementById("phyAddr").value = res[0].address;
					document.getElementById("phyHomePhone").value = res[0].phone_home;
					document.getElementById("phyWorkPhone").value = res[0].phone_work;
					document.getElementById("phyMobilePhone").value = res[0].phone_mobile;
					document.getElementById("phyEmail").value = res[0].phy_email;
					document.getElementById("phyFax").value = res[0].fax;
					document.getElementById("phyNotes").value = res[0].notes;
					document.getElementById("hdnFlag").value = res[0].id;
					document.getElementById("btnAddPriPhy").value = "Update Employee";
				}
			});
		});

		var practiceId = sessionStorage.getItem('practiceId');
		$.get(root_url + "/physician/listall",
			function(data1, status){
				data1 = JSON.parse(data1).data;
				var dt = [];
				$.each(data1,function(i,v) {
					dt.push([data1[i].id,data1[i].name,data1[i].individual_npi,data1[i].phone_work,data1[i].address,data1[i].speciality,data1[i].id]);
				});
				//alert(dt);
				var table = $('#physicianList').DataTable({
					"data": dt,
					"aoColumns": [
						{"mdata": "physicianID","title":"Employee ID",visible:false},
						{"title":"Employee Name","mdata": "phyName"},
						{"title":"NPI","mdata": "individualNPI"},
						{"title":"Phone #","mdata": "phoneWork"},
						{"title":"Address","mdata": "address"},
						{"title":"Speciality","mdata": "speciality"},
						{"title":"Action",
							"render": function ( data, type, full, meta ) {
								return '<a href="javascript:void(0);" class="edit'+data+' editPhy"><span class="ti-pencil"></span></a>';
							}
						},
					]
				});
			});
		$("#btnAddPriPhy").click(function() {
			var hdnFlag = document.getElementById("hdnFlag").value;
			if(hdnFlag == 0){
				var phyName = document.getElementById("PrimaryPhysiName").value;
				var speciality = document.getElementById("phySpeciality").value;
				var dob = document.getElementById("phyDOB").value;
				var individualNPI = document.getElementById("phyNPI").value;
				var ssno = document.getElementById("phySSN").value;
				var address = document.getElementById("phyAddr").value;
				var phoneHome = document.getElementById("phyHomePhone").value;
				var phoneWork = document.getElementById("phyWorkPhone").value;
				var phoneMobile = document.getElementById("phyMobilePhone").value;
				var phyEmail = document.getElementById("phyEmail").value;
				var fax = document.getElementById("phyFax").value;
				var notes = document.getElementById("phyNotes").value;
				var practiceID = sessionStorage.getItem('practiceId');

				$.ajax({
					type: "POST",
					url: root_url+"physician/add",
					data:{
						"PrimaryPhysiName" : phyName,
						"phyDOB" : dob,
						"phyNPI" : individualNPI,
						"phySSN" : ssno,
						"phyAddr" : address,
						"phyHomePhone" : phoneHome,
						"phyWorkPhone" : phoneWork,
						"phyMobilePhone" : phoneMobile,
						"phyEmail" : phyEmail,
						"phyFax" : fax,
						"phyNotes" : notes,
						"speciality" : speciality,
						"practiceID" : practiceID
					},success:function(result){
						var res = JSON.parse(result);
                        new PNotify({
                            type:'success',
                            text: json.message,
                            after_init: function(notice){
                                notice.attention('rubberBand');
                            }
                        })
						$('#PrimaryPhysi').modal('hide');
						window.location.reload();
					}
				});
			}
			else
			{
				var phyName = document.getElementById("PrimaryPhysiName").value;
				var physicianID = document.getElementById("hdnFlag").value;
				var speciality = document.getElementById("phySpeciality").value;
				var dob = convertDate(document.getElementById("phyDOB").value);
				var individualNPI = document.getElementById("phyNPI").value;
				var ssno = document.getElementById("phySSN").value;
				var address = document.getElementById("phyAddr").value;
				var phoneHome = document.getElementById("phyHomePhone").value;
				var phoneWork = document.getElementById("phyWorkPhone").value;
				var phoneMobile = document.getElementById("phyMobilePhone").value;
				var phyEmail = document.getElementById("phyEmail").value;
				var fax = document.getElementById("phyFax").value;
				var notes = document.getElementById("phyNotes").value;

				$.ajax({
					type: "POST",
					url: root_url+"physician/update",
					data:{
						"physicianID" : physicianID,
						"PrimaryPhysiName" : phyName,
						"phyDOB" : dob,
						"phyNPI" : individualNPI,
						"phySSN" : ssno,
						"phyAddr" : address,
						"phyHomePhone" : phoneHome,
						"phyWorkPhone" : phoneWork,
						"phyMobilePhone" : phoneMobile,
						"phyEmail" : phyEmail,
						"phyFax" : fax,
						"phyNotes" : notes,
						"speciality" : speciality
					},success:function(result){
						var res = JSON.parse(result);
                        new PNotify({
                            type:'success',
                            text: json.message,
                            after_init: function(notice){
                                notice.attention('rubberBand');
                            }
                        })
						$('#PrimaryPhysi').modal('hide');
						window.location.reload();
					}
				});
			}
		});
	});
	function changeDateFormat(inputDate){  // expects Y-m-d
		var splitDate = inputDate.split('-');
		if(splitDate.count == 0){
			return null;
		}

		var year = splitDate[0];
		var month = splitDate[1];
		var day = splitDate[2].substr(0,2);

		return month + '-' + day + '-' + year;
	}
	var convertDate = function(usDate) {
		var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
		return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
	}
</script>