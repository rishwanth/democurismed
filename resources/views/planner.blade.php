<?php
    $base_url = URL::to('/');
?>

<!DOCTYPE html>
<html>
<head>
    <title>AMROMED LLC | Query Editor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo $base_url; ?>/public/img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="<?php echo $base_url; ?>/public/css/bootstrap.css" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="<?php echo $base_url; ?>/public/vendors/themify/css/themify-icons.css" rel="stylesheet"/>
    <link href="<?php echo $base_url; ?>/public/vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet"/>
    <link href="<?php echo $base_url; ?>/public/css/login.css" rel="stylesheet">
    <style type="text/css">
        body{
            color:#000;
            background: #fff;
        }
    </style>
    <!--end page level css-->
</head>

<body id="sign-in1">

<div class="preloader">
    <div class="loader_img"><img src="<?php echo $base_url; ?>/public/img/loader.gif" alt="loading..." height="64" width="64"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 m-auto login-form1">

                <h2 class="text-center logo_h2">
                    <img src="<?php echo $base_url; ?>/public/img/pages/clear_black.png" alt="Logo">
                </h2>

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form id="formSQL" name="formSQL" method="post" class="login_validator1" action="{{ URL::to('/') }}/addplanner1">
                            <div class="form-group">
                                <input type="hidden" id="_token" name="_token"
                                  value="{!! csrf_token() !!}" />
                                <input type="hidden" id="root_url" name="v"
                                  value="{{ URL::to('/') }}" />
                                  

                            </div>

<div class="form-group">
    Reccurring Inputs
</div>

<div class="row">
    <div class="col-md-3 col-sd-3">
        Start date : yyyy-mm-dd<br/>
        <input id="start_date" name="start_date" value="{{  $response['start_date'] }}">
    </div>
    <div class="col-md-3 col-sd-2">
        Start Time : hh:mm:ss<br/>
        <input id="start_time" name="start_time" value="{{  $response['start_time'] }}">
    </div>
    <div class="col-md-3 col-sd-3">
        End date : yyyy-mm-dd <br/>
        <input id="end_date" name="end_date" value="{{  $response['end_date'] }}">
    </div>
    <div class="col-md-3 col-sd-2">
        End Time : hh:mm:ss <br/>
        <input id="end_time" name="end_time" value="{{  $response['end_time'] }}">
    </div>
</div>

<div class="row">
    <br/>
</div>

<div class="row">
    <div class="col-md-12 col-sd-12">
        Recurrance : Every
  <input type="text" name="repeat_count" id="repeat_count" 
  value="{{ $response['repeat_count'] }}" class="col-md-1"> 
  <input type="radio" name="repeat_frequency" value="daily" 
  <?php if ($response['repeat_frequency']=="daily") echo 'checked'; ?> > Daily
  <input type="radio" name="repeat_frequency" value="weekly" 
  <?php if ($response['repeat_frequency']=="weekly") echo 'checked'; ?> > Weekly
  <input type="radio" name="repeat_frequency" value="monthly" 
  <?php if ($response['repeat_frequency']=="monthly") echo 'checked'; ?> > Monthly
  > Monthly
  <input type="radio" name="repeat_frequency" value="yearly" 
<?php if ($response['repeat_frequency']=="yearly") echo 'checked'; ?>  
  > Yearly

    </div>
</div>

<div class="row">
    <br/>
</div>

<div class="row">
    <div class="col-md-12 col-sd-12">
        Repeats on : Every
        <input type="checkbox" name="repeat_sunday" id="repeat_sunday" 
        value="SU" <?php if ($response['repeat_sunday']=="SU") echo 'checked'; ?> > Sunday
        <input type="checkbox" name="repeat_monday" id="repeat_monday" 
        value="MO" <?php if ($response['repeat_monday']=="MO") echo 'checked'; ?>> Monday
        <input type="checkbox" name="repeat_tuesday" id="repeat_tuesday"
        value="TU" <?php if ($response['repeat_tuesday']=="TU") echo 'checked'; ?>> Tuesday
        <input type="checkbox" name="repeat_wednesday"  id="repeat_wednesday"
        value="WE" <?php if ($response['repeat_wednesday']=="WE") echo 'checked'; ?>> Wednesday
        <input type="checkbox" name="repeat_thursday" id="repeat_thursday"
        value="TH" <?php if ($response['repeat_thursday']=="TH") echo 'checked'; ?>> Thursday
        <input type="checkbox" name="repeat_friday" id="repeat_friday"
        value="FR" <?php if ($response['repeat_friday']=="FR") echo 'checked'; ?>> Friday
        <input type="checkbox" name="repeat_saturday" id="repeat_saturday"
        value="SA" <?php if ($response['repeat_saturday']=="SA") echo 'checked'; ?> > Saturday
    </div>
</div>

<div class="form-group">
</div>

<div class="row">
    <div class="col-md-12 col-sd-12">
        Occurrance : #
  <input type="text" name="repeat_count_occurrance" id="repeat_count_occurrance" value="{{ $response['repeat_count_occurrance'] }}" class="col-md-1"> 
</div>
</div>

<div class="form-group">
    <div class="col-md-12 col-sd-12">
    <img src="<?php echo $base_url; ?>/public/img/pages/arrow-right.png" id="btnSubmit" style="margin:0 50%; background-color: chocolate;border-radius: 15px;cursor: pointer;" alt="Go" width="30" height="30"> <button name="submit" id="submit" class="btn btn-success" type="submit" value="submit">Generate Records</button> 
</div>
</div>

<div class="form-group" style="overflow: auto;
overflow-y: hidden1;height: 500px">

<div class="form-group">
    <hr/>
    Date records
    <hr/>
</div>

<table class="table table-striped table-bordered">
    <tr>
        <th>Sno</td>
        <th>Start Date</td>
        <th>Start Time</td>
        <th>Timezone type</td>
        <th>End Date</td>
        <th>End Time</td>
        <th>Timezone type</td>
    </tr>
@foreach($response['rows'] as $key => $value)
    <tr>
        <td>{{ 1 }}</td>
        @foreach($value as $key1 => $value1)
            @foreach($value1 as $key2 => $value2)
            <td>{{ $value2 }}</td>
            @endforeach
        @endforeach
    </tr>
@endforeach
</table>

</div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="<?php echo $base_url; ?>/public/js/jquery.min.js" type="text/javascript"></script>

<script src="<?php echo $base_url; ?>/public/js/popper.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>/public/js/bootstrap.min.js" type="text/javascript"></script>

<!-- end of global js -->
<!-- page level js -->

<script src="<?php echo $base_url; ?>/public/vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/public/js/custom_js/login.js"></script>

<script>
$(document).ready(function(){
});
</script>
</body>
</html>