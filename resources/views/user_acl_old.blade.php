@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <h4>User Permission</h4>
        </div>
        <div class="col-lg-6 col-md-6 pull-right">
            <a class="btn btn-success" href="{{ URL::to('acl/create') }}"> Create New Permission</a>
        </div>
    </div>


<?php $i=0; ?>
    <form action="{{ URL::to('acl/save') }}" method="POST">
        <input type="hidden" id="_token" name="_token"
               value="<?php echo csrf_token(); ?>">
        <input type="hidden" id="root_url" name="root_url"
               value="{{ URL::to('/') }}/">
    <table class="table table-bordered dataTables_scrollHead">
        <tr style="background: lightblue;">
            <th>No</th>
            <th>Screen / Module Name</th>
            <th align="center">Display</th>
            <th align="center">Create</th>
            <th align="center">Edit</th>
            <th align="center">Delete</th>
            <th align="center">View</th>
        </tr>
	    @foreach ($rows as $row)
	    <tr>
            <?php 
                $id_create = 'IS_CREATE_' . $row->main_acl_key . '';
                $id_edit = 'IS_EDIT_' . $row->main_acl_key . '';
                $id_delete = 'IS_DELETE_' . $row->main_acl_key . '';
                $id_view = 'IS_VIEW_' . $row->main_acl_key . '';
                $id_display = 'IS_DISPLAY_' . $row->main_acl_key . '';
            ?>
	        <td>{{ ++$i }}</td>
	        <td>{{ $row->acl_name}}
                    {!! Form::hidden('acl_key[]', $row->main_acl_key, array('id' => 'acl_key[]' )) !!}
                    {!! Form::hidden('user_acl_id[]', $row->id, array('id' => 'user_acl_id[]', 'value' => $row->id )) !!}
            </td>
            <td>{{ Form::checkbox($id_display, '1', $row->is_display, array('id' => $id_display )) }}</td>
            <td>{{ Form::checkbox($id_create, '1', $row->is_create, array('id' => $id_create )) }}</td>
            <td>{{ Form::checkbox($id_edit, '1', $row->is_edit, array('id' => $id_edit )) }}</td>
            <td>{{ Form::checkbox($id_delete, '1', $row->is_delete, array('id' => $id_delete )) }}</td>
            <td>{{ Form::checkbox($id_view, '1', $row->is_view, array('id' => $id_view )) }}</td>
 	        <td>


	        </td>
	    </tr>
	    @endforeach

    </table>
    <button type="submit" class="btn btn-success">Update</button>
</form>

@endsection