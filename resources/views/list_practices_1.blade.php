@extends('layout.app')
@section('content')
    <div class="row">
        <div class="col-6" >
            <h2>Practices</h2>
        </div>
        <div class="col-6" >
            <a href="{{ URL::to('add_practice')   }}" class="btn btn-primary float-right">Add New Practice</a>
        </div>
        <div class="col-md-12" style="margin-top:20px;">
            <div class="widget box" >
                <div class="widget-content">
                    
                        <div class="table-responsive">
                            <table id="test" class="table table-bordered" style="cursor:pointer; border-radius:15px; border-color:#000;" cellspacing="0" width="100%">
                            </table>
                        </div>
                </div>
            </div>

        </div>
@stop
        <style type="text/css">
            .dataTables_wrapper .dataTables_scroll {
                border: 1px solid #000;
                margin: 1.5rem 0;
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
                border-bottom-left-radius: 4px;
                border-bottom-right-radius: 4px;
            }




            div.dataTables_scrollHead table.dataTable {
                margin-bottom: 0 !important;
                margin-top: 0 !important;
                border: 0 !important;

            }

            .dataTables_scrollBody {
                overflow-x: hidden !important;
                border-top: 1px solid #000;
            }


            .table {
                border-left:0px !important;
                border-right:0px !important;

            }

            .table th {
                border-bottom: 0 !important;
            }
        </style>
        <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
        <script
                src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script>


            $(document).ready(function(){
                $(".preloader").show();

                var root_url = $("#hidden_root_url").val();
                //document.getElementById("lblUser").innerHTML = sessionStorage.getItem('userName');

                $(".navigation li").parent().find('li').removeClass("active");

                $("#navv1").addClass("active");
                $(".left-side").css({"max-height":"800px"});
                $.ajax({
                    type: "POST",
                    url: root_url+"/practices/list",
                    data: {
                    }, success: function (result) {
                        var data = JSON.parse(result);
                        if (data.status != 0) {
                            var data1 = data.data;
                            //alert(data1.length);
                            var dt = [];
                            $.each(data1, function (i, v) {

                                dt.push([data1[i].id, data1[i].name, data1[i].ein, data1[i].address, data1[i].npi]);
                            });
                            //alert(dt);
                            var table = $('#test').DataTable({
                                "scrollY": "300px",
                                "scrollCollapse": true,
                                "data": dt,
                                columns: [
                                    {"title": "practiceID", visible: false},
                                    {"title": "Practice Name"},
                                    {"title": "EIN"},
                                    {"title": "Address"},
                                    {"title": "NPI"}
                                ]
                            });
                            $(".dataTables_filter").find("input").css({"border-radius": "15px"});
                            $(".dataTables_scrollBody table").css({"border": "none"});
                            // $("table.dataTable thead .sorting").css({"text-align":"center"})
                            // $("table.dataTable td ").css({"text-align":"center"})
                            $('.preloader').hide();
                            $('#test tbody').on('click', 'tr', function () {
                                $('#MedCoding').modal('show');
                                var data = table.row(this).data();
                                var id = data[0];
                                sessionStorage.setItem("patientId", id);
                                window.location.href = "{{ URL::to('view_practice')}}";
                            });
                            $("th:first").css({"border-top-left-radius": "5px"});
                        }
                        else{
                            var dt = [];
                            var table = $('#test').DataTable({
                                "scrollY": "300px",
                                "scrollCollapse": true,
                                "data": dt,
                                columns: [
                                    {"title": "patientID", visible: false},
                                    {"title": "Client Name"},
                                    {"title": "DOB"},
                                    {"title": "SSN"},
                                    {"title": "Account #"},
                                    {"title": "Cell #"},
                                    {"title": "Rendering Provider"},
                                    {"title": "Locations"}
                                ]
                            });
                            $(".dataTables_filter").find("input").css({"border-radius": "15px"});
                            $(".dataTables_scrollBody table").css({"border": "none"});
                            // $("table.dataTable thead .sorting").css({"text-align":"center"})
                            // $("table.dataTable td ").css({"text-align":"center"})
                            $('.preloader').hide();
                            $('#test tbody').on('click', 'tr', function () {
                                $('#MedCoding').modal('show');
                                var data = table.row(this).data();
                                var id = data[0];
                                sessionStorage.setItem("patientId", id);
                                window.location.href = "edit";
                            });
                            $("th:first").css({"border-top-left-radius": "5px"});
                        }
                    }
                });
                
                $("th:first").css({"border-top-left-radius":"5px"});
            });
            function changeDateFormat(inputDate){  // expects Y-m-d
                if(inputDate != "" && inputDate != undefined && inputDate != null){
                    var splitDate = inputDate.split('-');
                    if(splitDate.count == 0){
                        return null;
                    }

                    var year = splitDate[0];
                    var month = splitDate[1];
                    var day = splitDate[2];

                    return month + '-' + day + '-' + year;
                }
            }
</script>