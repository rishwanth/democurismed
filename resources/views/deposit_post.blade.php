@extends('layout.app')
@section('content')

        <div class="alert alert-warning fade in" style="margin-top:10px;" id="divAlert">
            <i class="icon-remove close" data-dismiss="alert"></i>
            <strong>Warning!</strong> <span id="alertTop">Your Payment Due is on 05/22/2016</span>
        </div>
        <!-- /Breadcrumbs line -->

        <div id="content">
            
            <h3 style="margin:0px 20px 20px 20px;">Payment</h3>
            <div class="card-body">
                <div class="row">
                    <input type="hidden" id="_hdnRetAllow"/>
                    <input type="hidden" id="_hdnRetPaid"/>
                    <input type="hidden" id="_hdnRetAdjust"/>
                    <input type="hidden" id="_hdnRetBal"/>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-12">Select Client</label>
                        <div class="col-md-10">
                            <select id="patient" class="form-control">
                                <option value="0"></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-12">From Date</label>
                        <div class="col-md-10">
                            <input type="text" id="fromDepDt" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-12">To Date</label>
                        <div class="col-md-10">
                            <input type="text" id="toDepDt" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-md-4" style="margin-bottom:20px;">
                        <label class="control-label col-md-2"></label>
                        <div class="col-md-6">
                            <input type="checkbox" class="checkbox"/> All Clients
                        </div>
                    </div>
                    <div class="form-group col-md-4" style="margin-bottom:20px;">
                        <label class="control-label col-md-2"></label>
                        <div class="col-md-6">
                            <input type="checkbox" class="checkbox" id="chkClosed"/> Include Closed
                        </div>
                    </div>
                    <div class="form-group col-md-4" style="margin-bottom:20px;margin-top:20px;">
                        <div class="col-md-12">
                            <input type="button" class="btn btn-primary" value="Get Claims" id="btnGetDeposit">
                            <input type="button" class="btn btn-primary" value="Import ERA" id="btnGetERA">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="form-group col-md-4">
                        <span class="col-md-6"><strong>Total Amount</strong></span>
                        <span class="col-md-6" id="spanTotAmt"></span>
                    </div>

                    <div class="form-group col-md-4">
                        <span class="col-md-6"><strong>Amount Applied</strong></span>
                        <span class="col-md-6" id="spanAmtApp"></span>
                    </div>

                    <div class="form-group col-md-4">
                        <span class="col-md-6"><strong>Amount Remaining</strong></span>
                        <span class="col-md-6" id="spanAmtRem"></span>
                    </div>
                </div>
            </div>
            
            <table class="table table-bordered table table-hover table-striped" style="margin-top:70px;">
                <thead id="depHead">
                </thead>
                <tbody id="depBody">
                </tbody>
            </table>
            <a href="javascript:void(0)" id="bulkDiv"><input type="button" value="Save all" id="hitBulk" class="btn btn-primary" />
            <a href="{{ URL::to('/deposits')}}"><button class="btn btn-primary pull-right">Back to Deposit</button></a>
        </div>
    </div>
<div class="modal fade" id="Status" tabindex="-1">
    <div class="modal-dialog" style="width:30%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Deposit</h4>
            </div>
            <div class="modal-body" style="min-height:180px;">
                <div class="form-group col-md-12">
                    <label class="control-label col-md-12">Copay :</label>
                    <div class="col-md-10" id="copay">
                        <input type="text" class="form-control copay" />
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="control-label col-md-12">Co-Ins :</label>
                    <div class="col-md-10" id="coins">
                        <input type="text" class="form-control coins" />
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="control-label col-md-12">Deductible :</label>
                    <div class="col-md-10" id="deduc">
                        <input type="text" class="form-control deduc" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" id="btnSaveStatus" class="btn btn-primary" data-dismiss="modal" value="Save" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalERA" tabindex="-1">
    <div class="modal-dialog" style="width:30%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Upload 835 File</h4>
            </div>
            <div class="modal-body" style="min-height:180px;">
                <form id="upload" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">835 File :</label>
                        <div class="col-md-10">
                            <input id="filename" type="file" name="filename" />
                        </div>
                    </div>
                    <input class="btn btn-success" type="submit" id="upload" value="Upload">
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" id="btnSaveStatus" class="btn btn-primary" data-dismiss="modal" value="Save" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@stop
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function(){
        var root_url = $("#hidden_root_url").val();
        $(".ti-menu-alt").click();
        var depID = sessionStorage.getItem("depID");
        var depAmt = sessionStorage.getItem("depAmt");
        var depPayor = sessionStorage.getItem("depPayor");
        var depPayorType = sessionStorage.getItem("depPayorType");
        //$("#bulkDiv").hide();
        $('#divAlert').hide();

        $('#fromDepDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#fromDepDt').mask('00-00-0000');

        $('#toDepDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#toDepDt').mask('00-00-0000');

       

        
        
        // $('#Status').on('keypress', function (event) {
        //   //alert("BEFORE ENTER clicked");
        //   var keycode = (event.keyCode ? event.keyCode : event.which);
        //   if(keycode == '13'){
        //     //alert("AFTER ENTER clicked");
        //     $('#btnSaveStatus').click();
        //   }
        //   // var tempID = $(".textbox7").attr("id");
        //   // var tempChk = tempID.replace('txtBalanceIns','');
        //   var tempID = sessionStorage.getItem("currClaim");
        //   var temp = "#txtBalanceIns"+tempID;
        //   $(temp).focus();
        //   sessionStorage.removeItem("currClaim");
        // });
        $(document).on('focusout','.textbox5',function() {
            if($(this).val()!="0.00" && $(this).val()!=""){
                var tempValue1 = $(this).val();
                var newVal = tempValue1;
                $(this).val(newVal);
                var tempID = $(this).attr('id');
                var tempChk = tempID.replace('txtDepPatPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",true);
                sessionStorage.setItem("txtboxPatPaid",tempValue1)
                FocOutPat(tempValue1,tempID);
                // if ($("input:checkbox:checked").length > 1) {
                //   $("#bulkDiv").show();
                // }
                // else{
                //   $("#bulkDiv").hide();
                // }
            }
            else{
                var tempID = $(this).attr('id');
                var tempChk = tempID.replace('txtDepPatPayment','');
                tempChk = ".chkBulk"+tempChk;
                $(tempChk).prop("checked",false);
                // if ($("input:checkbox:checked").length > 1) {
                //   $("#bulkDiv").show();
                // }
                // else{
                //   $("#bulkDiv").hide();
                // }
            }
        });
        $(document).on('focus','.textbox3',function() {
            var tempValue1 = $(this).val();
            var newVal = tempValue1;
            //var t = $(this).val(newVal);
            sessionStorage.setItem("txtboxAdjust",newVal);
        });
        $(document).on('focusout','.textbox3',function() {
            var tempValue1 = $(this).val();
            var newVal = parseFloat(tempValue1);
            $(this).val(newVal);
            var tempID = $(this).attr('id');
            var resStr = tempID.replace('txtDepAdjust','');
            tempID = 'txtBalancePat'+resStr;
            var tempID1 = 'txtBalanceIns'+resStr;
            var tempPaid = 'txtDepPayment'+resStr;
            var tempPaidVal = document.getElementById(tempPaid).value;
            if(tempPaidVal !=""){
                var tempValue1 = $(this).val();
                var newVal = parseFloat(tempValue1);
                $(this).val(newVal);
                var tempID = $(this).attr('id');
                var resStr = tempID.replace('txtDepAdjust','');
                tempID = 'txtBalancePat'+resStr;
                var tempID1 = 'txtBalanceIns'+resStr;
                var tempPaid = 'txtDepPayment'+resStr;
                var tempTot =  'txtTot'+resStr;
                var tempCopay =  'txtcopay'+resStr;
                var tempDeduc =  'txtdeduc'+resStr;
                var tempCoIns =  'txtcoins'+resStr;
                var tempBal = document.getElementById(tempTot).innerHTML;
                var tempPaidVal = document.getElementById(tempPaid).value;

                $.ajax({
                    type: "POST",
                    url: root_url+"deposits/total",
                    async : false,
                    data:{
                        claimID : resStr,
                        paid : tempPaidVal,
                        adjust : newVal
                    },success:function(result){
                        var res = JSON.parse(result);
                        if(res.status == 0){
                            return;
                        }
                        var Balance = res.data.total;
                        Balance = tempBal - Balance;
                        document.getElementById(tempID).value = Balance;
                        document.getElementById(tempID1).value = "0.00"
                        sessionStorage.setItem("patientBal",Balance);
                        $("#Status").modal('show');
                        StatusModal(resStr);
                        document.getElementById(tempCopay).value = Balance.toFixed(2);
                        var sesCopay = "txtcopay"+resStr;
                        var sesCoins = "txtcoins"+resStr;
                        var sesDeduc = "txtdeduc"+resStr;
                        sessionStorage.setItem(sesCopay,Balance);
                        document.getElementById(tempDeduc).value = "0.00"; 
                        document.getElementById(tempCoIns).value = "0.00"; 
                        var copayvalue = document.getElementById(tempCopay).value;
                        var coinsvalue = document.getElementById(tempCoIns).value;
                        var deducvalue = document.getElementById(tempDeduc).value;
                        sessionStorage.setItem("tempCopay",Balance); 
                        sessionStorage.setItem("tempDeduc",deducvalue); 
                        sessionStorage.setItem("tempCoins",coinsvalue); 
                        sessionStorage.setItem(sesCoins,coinsvalue);
                        sessionStorage.setItem(sesDeduc,deducvalue);
                    }
                });
            }
        });
        $(document).on('focusout','.copay',function() {
            var isTer = sessionStorage.getItem("txtboxPaid");
            var copayvalue = 0;
            var coinsvalue = 0;
            var deducvalue = 0;
            if(isTer != "" && isTer != null){
                var temp = $(this).attr('id');
                var tempValue1 = $(this).val();
                var resStr = temp.replace('txtcopay','');
                var tempID = "txtBalancePat"+resStr;
                var tempID1 = "txtBalanceIns"+resStr;
                var tempCopay =  'txtcopay'+resStr;
                var tempDeduc =  'txtdeduc'+resStr;
                var tempCoIns =  'txtcoins'+resStr;
                //document.getElementById(tempCopay).value = document.getElementById(tempCopay).value;
                copayvalue = document.getElementById(tempCopay).value;
                coinsvalue = document.getElementById(tempCoIns).value;
                deducvalue = document.getElementById(tempDeduc).value;
                tempValue1 = eval(copayvalue) + eval(coinsvalue) + eval(deducvalue);
                document.getElementById(tempID).value = tempValue1.toFixed(2);
                sessionStorage.setItem("tempCopay",copayvalue);
                sessionStorage.setItem("tempDeduc",deducvalue);
                sessionStorage.setItem("tempCoins",coinsvalue);
                var balFinal = sessionStorage.getItem(tempCopay);
                balFinal = balFinal - tempValue1;
                document.getElementById(tempID1).value = toFixed(balFinal,2);
            }
        });
        $(document).on('focusout','.coins',function() {
            var isTer = sessionStorage.getItem("txtboxPaid")
            if(isTer != "" && isTer != null){
                var temp = $(this).attr('id');
                var tempValue1 = $(this).val();
                var resStr = temp.replace('txtcoins','');
                var tempID = "txtBalancePat"+resStr;
                var tempID1 = "txtBalanceIns"+resStr;
                var tempCopay =  'txtcopay'+resStr;
                var tempDeduc =  'txtdeduc'+resStr;
                var tempCoIns =  'txtcoins'+resStr;
                //document.getElementById(tempCoIns).value = document.getElementById(tempCoIns).value;
                var copayvalue = document.getElementById(tempCopay).value;
                var coinsvalue = document.getElementById(tempCoIns).value;
                var deducvalue = document.getElementById(tempDeduc).value;
                tempValue1 = eval(copayvalue) + eval(coinsvalue) + eval(deducvalue);
                document.getElementById(tempID).value = tempValue1.toFixed(2);
                sessionStorage.setItem("tempCopay",copayvalue);
                sessionStorage.setItem("tempDeduc",deducvalue);
                sessionStorage.setItem("tempCoins",coinsvalue);
                var balFinal = sessionStorage.getItem(tempCopay);
                balFinal = balFinal - tempValue1;
                document.getElementById(tempID1).value = toFixed(balFinal,2);
            }
        });
        $(document).on('focusout','.deduc',function() {
            var isTer = sessionStorage.getItem("txtboxPaid")
            if(isTer != "" && isTer != null){
                var temp = $(this).attr('id');
                var tempValue1 = $(this).val();
                var resStr = temp.replace('txtdeduc','');
                var tempID = "txtBalancePat"+resStr;
                var tempID1 = "txtBalanceIns"+resStr;
                var tempCopay =  'txtcopay'+resStr;
                var tempDeduc =  'txtdeduc'+resStr;
                var tempCoIns =  'txtcoins'+resStr;
                //document.getElementById(tempDeduc).value = document.getElementById(tempDeduc).value;
                var copayvalue = document.getElementById(tempCopay).value;
                var coinsvalue = document.getElementById(tempCoIns).value;
                var deducvalue = document.getElementById(tempDeduc).value;

                tempValue1 = eval(copayvalue) + eval(coinsvalue) + eval(deducvalue);
                document.getElementById(tempID).value = tempValue1.toFixed(2);
                sessionStorage.setItem("tempCopay",copayvalue);
                sessionStorage.setItem("tempDeduc",deducvalue);
                sessionStorage.setItem("tempCoins",coinsvalue);
                var balFinal = sessionStorage.getItem(tempCopay);
                balFinal = balFinal - tempValue1;
                document.getElementById(tempID1).value = toFixed(balFinal,2);
            }
        });
        // $("#Status").on("shown.bs.modal", function () {
        //     $(this).find('input[type="text"]').focus();
        // });
        $(document).on('focusout','.textbox2',function() {
            var tempValue1 = $(this).val();
            var newVal = tempValue1;
            $(this).val(newVal);
        });
        $(document).on('focusin','.textbox1',function() {
            var isTer = sessionStorage.getItem("txtboxPaid")
            if(isTer != "" && isTer != null){
                var tempValue1 = $(this).val();
                var tempID = $(this).attr('id');
                var resStr = tempID.replace('txtDepPayment','');
                var tempid = 'txtBalancePat'+resStr;
                var tempID1 = 'txtBalanceIns'+resStr;
                var tempTot = 'txtTot'+resStr;
                if(tempValue1 != ""){
                    var tempBal = document.getElementById(tempid).value;
                    var tempTotal = document.getElementById(tempTot).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    // var interApplied = AmtApplied - tempValue1;
                    // var interRemain = AmtRemain + tempValue1;
                    var FinalBal = tempTotal ;
                    var FinalApp = AmtApplied - parseFloat(tempValue1);
                    var FinalRem = AmtRemain + parseFloat(tempValue1);
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                        document.getElementById(tempid).value = "";
                    }
                }
            }
        });
        $(document).on('focusin','.textbox5',function() {
            var isTer = sessionStorage.getItem("txtboxPatPaid")
            if(isTer != "" && isTer != null){
                var tempValue1 = $(this).val();
                var tempID = $(this).attr('id');
                var resStr = tempID.replace('txtDepPatPayment','');
                var tempid = 'txtBalancePat'+resStr;
                var tempID1 = 'txtBalanceIns'+resStr;
                var tempTot = 'txtTot'+resStr;
                if(tempValue1 != ""){
                    var tempBal = document.getElementById(tempid).value;
                    var tempTotal = document.getElementById(tempTot).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    // var interApplied = AmtApplied - tempValue1;
                    // var interRemain = AmtRemain + tempValue1;
                    var FinalBal = tempTotal ;
                    var FinalApp = AmtApplied - parseFloat(tempValue1);
                    var FinalRem = AmtRemain + parseFloat(tempValue1);
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = "";
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = "";
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                        document.getElementById(tempid).value = "";
                    }
                }
            }
        });
    });



    function StatusModal(ID){
        var root_url = $("#hidden_root_url").val();
        var fin = ID.replace("statusID","");
        $("#copay").html('<input type="text" id="txtcopay'+fin+'" class="form-control copay" value="" />');
        $("#coins").html('<input type="text" id="txtcoins'+fin+'" class="form-control coins" value="" />');
        $("#deduc").html('<input type="text" id="txtdeduc'+fin+'" class="form-control deduc" value="" />');
        $.ajax({
            type: "POST",
            url: root_url+"claims/status",
            async : false,
            data:{
                claimID : fin
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                    return;
                }
                var t = res.data[0];

                sessionStorage.setItem("currClaim",fin);
                var copayVal = "#txtcopay"+t.claimID;
                var deducVal = "#txtdeduc"+t.claimID;
                var coinsVal = "#txtcoins"+t.claimID;
                if(t.copay != ""){
                    $(copayVal).val(t.copay);
                    var sesCopay = "txtcopay"+fin;
                    var sesCopay1 = "txtcopay"+fin;
                    sessionStorage.setItem(sesCopay,t.copay);
                    document.getElementById(sesCopay1).focus();
                }
                else
                {
                    $(copayVal).val(t.copay);
                    var sesCopay = "txtcopay"+fin;
                    var sesCopay1 = "txtcopay"+fin;
                    sessionStorage.setItem(sesCopay,t.copay);
                    document.getElementById(sesCopay1).focus();
                }
                if(t.deductible != ""){
                    $(deducVal).val(t.deductible);
                    var sesDeduc = "txtdeduc"+fin;
                    sessionStorage.setItem(sesDeduc,t.deductible);
                }
                else{
                    $(deducVal).val(t.deductible);
                    var sesDeduc = "txtdeduc"+fin;
                    sessionStorage.setItem(sesDeduc,t.deductible);
                }
                if(t.coins != ""){
                    $(coinsVal).val(t.coins);
                    var sesCoins = "txtcoins"+fin;
                    sessionStorage.setItem(sesCoins,t.coins);
                }
                else{
                    $(coinsVal).val(t.coins);
                    var sesCoins = "txtcoins"+fin;
                    sessionStorage.setItem(sesCoins,t.coins);
                }
            }
        });
    }
    function FocOut(a,b){
        var root_url = $("#hidden_root_url").val();
        var tempVal = a;
        var resStr = b.replace('txtDepPayment','');
        var tempid = 'txtBalancePat'+resStr;
        var tempID1 = 'txtBalanceIns'+resStr;
        var tempStatus = '#ddlStatus'+resStr;
        var tempTot = 'txtTot'+resStr;
        var tempAll = 'txtDepAllowed'+resStr;
        var tempAdj = 'txtDepAdjust'+resStr;
        var tempCopay = 'txtcopay'+resStr;
        var tempCoins = 'txtcoins'+resStr;
        var tempCaseID = "";
        var temp = "";
        var Ispost = "";
        var tempSessiom = sessionStorage.getItem("txtboxPaid");
        $.ajax({
            type: "POST",
            url: root_url+"claims/isposted",
            async : false,
            data:{
                claimID : resStr
            },success:function(result){
                var res = JSON.parse(result);
                if(res.status == 0){
                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                    return;
                }
                result = res.data;
                //result = JSON.parse(result);
                tempCaseID = result[0].caseID;
                Ispost = result[0].isPosted;
            }
        });
        if(Ispost == "0"){
            if(a != tempSessiom){
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var tempAll = document.getElementById(tempAll).value;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var interApplied = AmtApplied - a;
                        var interRemain = AmtRemain - a;
                        var FinalApp = interApplied + parseFloat(decVal);
                        var FinalRem = interRemain - parseFloat(decVal);
                        //alert(tempBal);
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                    }
                    else{
                        $(this).val(tempVal);
                    }
                }
            }
            else{
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(decVal);
                        var FinalRem = AmtRemain - parseFloat(decVal);
                        //alert(Balance);
                        if(Balance <= 0){
                            $(tempStatus).val("3");
                            $("#Status").modal('hide');
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"policies/check",
                                async : false,
                                data:{
                                    caseID : tempCaseID
                                },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        $("#Status").modal('show');
                                        //StatusModal(resStr);
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                        $("#Status").modal('show');
                                        //StatusModal(resStr);
                                    }
                                }
                            });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        // $("#Status").modal('show');
                        // StatusModal(resStr);
                        // if(Balance % 1 != 0){
                        //     document.getElementById(tempCoins).value = Balance;
                        // }
                        // else{
                        //     console.log(tempCopay+".value = "+Balance+"");
                        //     document.getElementById(tempCopay).value = parseFloat(Balance);
                        // }
                    }
                    else{
                        $(this).val(tempVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(tempVal);
                        var FinalRem = AmtRemain - parseFloat(tempVal);
                        //alert(Balance);
                        if(Balance <= 0){
                            $(tempStatus).val("3");
                            $("#Status").modal('hide');
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"policies/check",
                                async : false,
                                data:{
                                    caseID : tempCaseID
                                },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        // $.ajax({
                                        //       type: "POST",
                                        //       url:"updateInsurance.php",
                                        //       async : false,
                                        //       data:{
                                        //         caseID : tempCaseID
                                        //       },success:function(result){
                                        //       }
                                        //   });
                                        $("#Status").modal('show');
                                        //StatusModal(resStr);
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                        $("#Status").modal('show');
                                        //StatusModal(resStr);
                                    }
                                }
                            });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        // $("#Status").modal('show');
                        // StatusModal(resStr);
                        // if(Balance % 1 != 0){
                        //     document.getElementById(tempCoins).value = Balance;
                        // }
                        // else{
                        //     console.log(tempCopay+".value = "+Balance+"");
                        //     document.getElementById(tempCopay).value = Balance;
                        // }
                    }
                }
            }
        }
        else{
            if(a != tempSessiom){
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var tempAll = document.getElementById(tempAll).value;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var interApplied = AmtApplied - a;
                        var interRemain = AmtRemain - a;
                        var FinalApp = interApplied + parseFloat(decVal);
                        var FinalRem = interRemain - parseFloat(decVal);
                        //alert(tempBal);
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                    }
                    else{
                        $(this).val(tempVal);
                    }
                }
            }
            else{
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = 0;
                        var Balance = "";
                        $.ajax({
                            type: "POST",
                            url: root_url+"deposits/total",
                            async : false,
                            data:{
                                claimID : resStr,
                                paid : a,
                                adjust : Adjust
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                                    return;
                                }
                                //var data1 = res.data;
                                var Balance = res.data.total;
                                Balance = tempBal - Balance;
                            }
                        });
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(decVal);
                        var FinalRem = AmtRemain - parseFloat(decVal);
                        //alert(Balance);
                        if(Balance <= 0){
                            $(tempStatus).val("3");
                            $("#Status").modal('hide');
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"policies/check",
                                async : false,
                                data:{
                                    caseID : tempCaseID
                                },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        // $("#Status").modal('show');
                                        // StatusModal(resStr);
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                        // $("#Status").modal('show');
                                        // StatusModal(resStr);
                                    }
                                }
                            });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        // $("#Status").modal('show');
                        // StatusModal(resStr);
                        // if(Balance % 1 != 0){
                        //     document.getElementById(tempCoins).value = Balance;
                        // }
                        // else{
                        //     console.log(tempCopay+".value = "+Balance+"");
                        //     document.getElementById(tempCopay).value = parseFloat(Balance);
                        // }
                    }
                    else{
                        $(this).val(tempVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = 0;
                        var Balance = "";
                        $.ajax({
                            type: "POST",
                            url: root_url+"deposits/total",
                            async : false,
                            data:{
                                claimID : resStr,
                                paid : a,
                                adjust : Adjust
                            },success:function(result){
                                var res = JSON.parse(result);
                                if(res.status == 0){
                                    new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                                    return;
                                }
                                //var data1 = res.data;
                                var Balance = res.data.total;
                                Balance = tempBal - Balance;
                            }
                        });
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(tempVal);
                        var FinalRem = AmtRemain - parseFloat(tempVal);
                        //alert(Balance);
                        if(Balance <= 0){
                            $(tempStatus).val("3");
                            $("#Status").modal('hide');
                        }
                        else{
                            $.ajax({
                                type: "POST",
                                url: root_url+"policies/check",
                                async : false,
                                data:{
                                    caseID : tempCaseID
                                },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        // $.ajax({
                                        //       type: "POST",
                                        //       url:"updateInsurance.php",
                                        //       async : false,
                                        //       data:{
                                        //         caseID : tempCaseID
                                        //       },success:function(result){
                                        //       }
                                        //   });
                                        // $("#Status").modal('show');
                                        // StatusModal(resStr);
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                        // $("#Status").modal('show');
                                        // StatusModal(resStr);
                                    }
                                }
                            });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                            document.getElementById(tempid).value = Balance;
                            document.getElementById(tempAdj).value = Adjust;
                        }
                        // $("#Status").modal('show');
                        // StatusModal(resStr);
                        // if(Balance % 1 != 0){
                        //     document.getElementById(tempCoins).value = Balance;
                        // }
                        // else{
                        //     console.log(tempCopay+".value = "+Balance+"");
                        //     document.getElementById(tempCopay).value = Balance;
                        // }
                    }
                }
            }
        }

    }
    function FocOutPat(a,b){
        var root_url = $("#hidden_root_url").val();
        var tempVal = a;
        var resStr = b.replace('txtDepPatPayment','');
        var tempid = 'txtBalancePat'+resStr;
        var tempID1 = 'txtBalanceIns'+resStr;
        var tempStatus = '#ddlStatus'+resStr;
        var tempTot = 'txtTot'+resStr;
        var tempAll = 'txtDepAllowed'+resStr;
        var tempAdj = 'txtDepAdjust'+resStr;
        var tempCopay = 'txtcopay'+resStr;
        var tempCoins = 'txtcoins'+resStr;
        var tempCaseID = "";
        var tempSessiom = sessionStorage.getItem("txtboxPatPaid");
        $.ajax({
            type: "POST",
            url: root_url+"claims/get",
            async : false,
            data:{
                claimID : resStr
            },success:function(result){
                tempCaseID = result;
            }
        });
        if(a != tempSessiom){
            var IsDec = tempVal.indexOf(".");
            if(tempVal != ""){
                if(IsDec == '0' || IsDec == '-1'){
                    var decVal = tempVal+'.00';
                    $(this).val(decVal);
                    var tempBal = document.getElementById(tempTot).innerHTML;
                    var tempAll = document.getElementById(tempAll).value;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtBal = parseFloat(tempBal);
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    var Adjust = AmtBal - tempAll;
                    var Balance = tempAll - tempVal;
                    var interApplied = AmtApplied - a;
                    var interRemain = AmtRemain - a;
                    var FinalApp = interApplied + parseFloat(decVal);
                    var FinalRem = interRemain - parseFloat(decVal);
                    //alert(tempBal);
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                        document.getElementById(tempAdj).value = Adjust;
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                        document.getElementById(tempAdj).value = Adjust;
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = Balance;
                        document.getElementById(tempAdj).value = Adjust;
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2)+'.00';
                        document.getElementById(tempid).value = Balance;
                        document.getElementById(tempAdj).value = Adjust;
                    }
                }
                else{
                    $(this).val(tempVal);
                }
            }
        }
        else{
            var IsDec = tempVal.indexOf(".");
            if(tempVal != ""){
                if(IsDec == '0' || IsDec == '-1'){
                    var decVal = tempVal+'.00';
                    $(this).val(decVal);
                    var tempBal = document.getElementById(tempTot).innerHTML;
                    var patBal = document.getElementById(tempid).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtBal = parseFloat(tempBal);
                    var Balance = patBal - tempVal;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    var FinalApp = AmtApplied + parseFloat(decVal);
                    var FinalRem = AmtRemain - parseFloat(decVal);
                    //alert(Balance);
                    if(Balance == 0 || Balance <= 0){
                        $(tempStatus).val("3");
                        $("#Status").modal('hide');
                    }
                    else{
                        $.ajax({
                            type: "POST",
                            url: root_url+"policies/check",
                            async : false,
                            data:{
                                caseID : tempCaseID
                            },success:function(result){
                                if(result == "Secondary"){
                                    $(tempStatus).val("2");
                                    //$("#Status").modal('show');
                                    //StatusModal(resStr);
                                }
                                else
                                {
                                    $(tempStatus).val("1");
                                    //$("#Status").modal('show');
                                    //StatusModal(resStr);
                                }
                            }
                        });
                    }
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                        //document.getElementById(tempAdj).value = Adjust;
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                        //document.getElementById(tempAdj).value = Adjust;
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = Balance;
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                        document.getElementById(tempid).value = Balance;
                    }
                    // $("#Status").modal('show');
                    // StatusModal(resStr);
                }
                else{
                    var decVal = tempVal;
                    $(this).val(tempVal);
                    var tempBal = document.getElementById(tempTot).innerHTML;
                    var patBal = document.getElementById(tempid).innerHTML;
                    var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                    var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                    var AmtBal = parseFloat(tempBal);
                    var Balance = patBal - tempVal;
                    var AmtApplied = parseFloat(befAmtApp);
                    var AmtRemain = parseFloat(befAmtRem);
                    var FinalApp = AmtApplied + parseFloat(decVal);
                    var FinalRem = AmtRemain - parseFloat(decVal);
                    //alert(Balance);
                    if(Balance == 0 || Balance <= 0){
                        $(tempStatus).val("3");
                        $("#Status").modal('hide');
                    }
                    else{
                        $.ajax({
                            type: "POST",
                            url: root_url+"policies/check",
                            async : false,
                            data:{
                                caseID : tempCaseID
                            },success:function(result){
                                if(result == "Secondary"){
                                    $(tempStatus).val("2");
                                    // $.ajax({
                                    //       type: "POST",
                                    //       url:"updateInsurance.php",
                                    //       async : false,
                                    //       data:{
                                    //         caseID : tempCaseID
                                    //       },success:function(result){
                                    //       }
                                    //   });
                                    //$("#Status").modal('show');
                                    //StatusModal(resStr);
                                }
                                else
                                {
                                    $(tempStatus).val("1");
                                    //$("#Status").modal('show');
                                    //StatusModal(resStr);
                                }
                            }
                        });
                    }
                    if(FinalApp % 1 != 0){
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                    }
                    else{
                        document.getElementById('spanAmtApp').innerHTML = toFixed(FinalApp,2);
                        document.getElementById(tempid).value = Balance;
                    }
                    if(FinalRem % 1 != 0){
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2);
                        document.getElementById(tempid).value = Balance;
                    }
                    else{
                        document.getElementById('spanAmtRem').innerHTML = toFixed(FinalRem,2) +'.00';
                        document.getElementById(tempid).value = Balance;
                    }
                    //$("#Status").modal('show');
                    //StatusModal(resStr);
                }
            }
        }

    }

    function floorFigure(figure, decimals){
        if (!decimals) decimals = 2;
        var d = Math.pow(10,decimals);
        return (parseInt(figure*d)/d).toFixed(decimals);
    };

    function toFixed(value, n) {
        const f = Math.pow(10, n);
        return (Math.trunc(value*f)/f).toFixed(n);
    }


    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2];

        return month + '-' + day + '-' + year;
    }
    function changeDate(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var month = splitDate[0];
        var date1 = splitDate[1];
        var year = splitDate[2];

        return year + '-' + month + '-' + date1;
    }
</script>