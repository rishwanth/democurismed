@extends('layout.app')
@section('content')
    <div class="card-body">
        <div class="row">
            <input type="hidden" id="_hdnAppID"/>
            <input type="hidden" id="_hdnAppID1"/>

            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <a href="scheduler_new1.php"><input type="button" value="Calender View" class="btn btn-primary" /></a>
                    </div>
                    <div class="col-7">
                        <input type="button" id="triggerAddApp" value="Add Appointment" class="btn btn-primary" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-12">
                        <h3 class="text-right">Manage Sessions</h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1">Client Name</label>
                    </div>
                    <div class="col-7">
                        <input type="text" class="form-control required" id="clientName" placeholder="Client Name" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1">Provider Name</label>
                    </div>
                    <div class="col-7">
                        <input type="text" class="form-control required" id="providerName" placeholder="Provider Name" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1">Location Name</label>
                    </div>
                    <div class="col-7">
                        <select class="form-control" id="locationName">
                            <option value=""></option>
                            <option value="12">Home</option>
                            <option value="11">Office</option>
                            <option value="03">School</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1">Status</label>
                    </div>
                    <div class="col-7">
                        <select class="form-control" id="statusName">
                            <option value=""></option>
                            <option value="Confirm">Confirmed</option>
                            <option value="Ready">Ready</option>
                            <option value="Noshow">No Show</option>
                            <option value="Hold">Hold</option>
                            <option value="Cancelled by Client">Cancelled by Client</option>
                            <option value="Cancelled by Provider">Cancelled by Provider</option>
                            <option value="Rendered">Rendered</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1">From Date</label>
                    </div>
                    <div class="col-7">
                        <input type="text" class="form-control required" id="fromDate" placeholder="From Date" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1">To Date</label>
                    </div>
                    <div class="col-7">
                        <input type="text" class="form-control required" id="toDate" placeholder="To Date" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1"></label>
                    </div>
                    <div class="col-7">
                        <input type="button" class="btn btn-primary" id="btnGo" value="Get Details" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1"></label>
                    </div>
                    <div class="col-7">
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group row">
                    <div class="col-5">
                        <label for="input-text" class="control-label float-right txt_media1"></label>
                    </div>
                    <div class="col-7">
                        <form method="post" action="UploadFileNew_ff.php" enctype="multipart/form-data">
                            <input type="file" name="file"/><br/><br/>
                            <input type="submit" name="submit_file" value="Submit"/>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12" id="rightDiv">
                <div id="schedulerTable" class="table-responsive">
                    <table id="tblSch" class="table-bordered" >
                    </table>
                    <div class="card-body" id="bulkDiv">
                        <div class="row">
                            <select class="form-control col-md-2" style="height:30px"  id="ddlSta">
                                <option value=""></option>
                                <option value="Confirm">Confirmed</option>
                                <option value="Noshow">No Show</option>
                                <option value="Hold">Hold</option>
                                <option value="Cancelled by Client">Cancelled by Client</option>
                                <option value="Cancelled by Provider">Cancelled by Provider</option>
                                <option value="Rendered">Rendered</option>
                            </select>
                            <button class="ml-sm-2 button-circle btn btn-green" id="btnFin">
                                <span class="ti-arrow-right"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalAvail" tabindex="-1" style="z-index:99999">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Check Availability for Provider</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">Select Date</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="availDate" placeholder="Select Date" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                    </div>
                    <div class="col-sm-12" id="divAvail">
                        <div class="form-group row">
                            <!--  <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">12:00 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">12:15 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">12:30 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">12:45 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">01:00 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">01:15 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">01:30 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">01:45 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">02:00 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">02:15 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">02:30 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">02:45 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">03:00 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">03:15 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">03:30 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">03:45 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">04:00 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">04:15 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">04:30 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">04:45 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">05:00 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">05:15 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">05:30 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">05:45 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">06:00 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">06:15 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">06:30 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">06:45 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">07:00 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">07:15 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">07:30 AM</span></a> &nbsp;
                             <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">07:45 AM</span></a> &nbsp; -->
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">08:00 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">08:15 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">08:30 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">08:45 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">09:00 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">09:15 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">09:30 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">09:45 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">10:00 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">10:15 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">10:30 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">10:45 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">11:00 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">11:15 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">11:30 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">11:45 AM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">12:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">12:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">12:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">12:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">01:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">01:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">01:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">01:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">02:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">02:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">02:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">02:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">03:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">03:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">03:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">03:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">04:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">04:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">04:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">04:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">05:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">05:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">05:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">05:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">06:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">06:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">06:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">06:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">07:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">07:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">07:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">07:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">08:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">08:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">08:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">08:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">09:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">09:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">09:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">09:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">10:00 PM</span></a> &nbsp;
                            <!-- <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">10:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">10:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">10:45 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">11:00 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">11:15 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">11:30 PM</span></a> &nbsp;
                            <a href="javascript:void(0)"><span class="badge badge-success" style="margin-bottom:1em">11:45 PM</span></a> &nbsp;
         -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modalAppoint" tabindex="-1" style="z-index:9999">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Appoinments</h4>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Client Name </label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control required" id="AppClientName" placeholder="Client Name" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Auth # </label>
                                    </div>
                                    <div class="col-7">
                                        <select class="form-control" id="AppAuth"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Activity </label>
                                    </div>
                                    <div class="col-7">
                                        <select class="form-control" id="AppActivity"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Provider Name  </label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control required" id="AppProvider" placeholder="Provider Name" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Time Duration </label>
                                    </div>
                                    <div class="col-7">
                                        <select class="form-control" id="AppStepTime">
                                            <option value="0">-- Select Duration --</option>
                                            <option value="15">15 mins</option>
                                            <option value="30">30 mins</option>
                                            <option value="45">45 mins</option>
                                            <option value="60">1 Hour</option>

                                            <option value="75">1 Hour 15 mins</option>
                                            <option value="90">1 Hour 30 mins</option>
                                            <option value="105">1 Hour 45 mins</option>
                                            <option value="120">2 Hour</option>

                                            <option value="135">2 Hour 15 mins</option>
                                            <option value="150">2 Hour 30 mins</option>
                                            <option value="165">2 Hour 45 mins</option>
                                            <option value="180">3 Hour</option>

                                            <option value="195">3 Hour 15 mins</option>
                                            <option value="210">3 Hour 30 mins</option>
                                            <option value="225">3 Hour 45 mins</option>
                                            <option value="240">4 Hour</option>

                                            <option value="255">4 Hour 15 mins</option>
                                            <option value="270">4 Hour 30 mins</option>
                                            <option value="285">4 Hour 45 mins</option>
                                            <option value="300">5 Hour</option>

                                            <option value="315">5 Hour 15 mins</option>
                                            <option value="330">5 Hour 30 mins</option>
                                            <option value="345">5 Hour 45 mins</option>
                                            <option value="360">6 Hour</option>

                                            <option value="375">6 Hour 15 mins</option>
                                            <option value="390">6 Hour 30 mins</option>
                                            <option value="405">6 Hour 45 mins</option>
                                            <option value="420">7 Hour</option>

                                            <option value="435">7 Hour 15 mins</option>
                                            <option value="450">7 Hour 30 mins</option>
                                            <option value="465">7 Hour 45 mins</option>
                                            <option value="480">8 Hour</option>

                                            <option value="495">8 Hour 15 mins</option>
                                            <option value="510">8 Hour 30 mins</option>
                                            <option value="525">8 Hour 45 mins</option>
                                            <option value="540">9 Hour</option>

                                            <option value="555">9 Hour 15 mins</option>
                                            <option value="570">9 Hour 30 mins</option>
                                            <option value="585">9 Hour 45 mins</option>
                                            <option value="600">10 Hour</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">From Time </label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control required" id="AppFromTime" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">To Time  </label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control required" id="AppToTime" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Location </label>
                                    </div>
                                    <div class="col-7">
                                        <select class="form-control" id="ddlLoca">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Description </label>
                                    </div>
                                    <div class="col-7">
                                        <textarea class="form-control" rows="5" cols="3" id="AppDesc"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1"></label>
                                    </div>
                                    <div class="col-7">
                                        <a href="javascript:void(0)" id="linkCheckAvail">Check Availability</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnAddApp" class="btn btn-primary" value="Add Appointment" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="EditAppoint" tabindex="-1" style="z-index:9999">
        <div class="modal-dialog modal-lg" style="width:80%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Appointment</h4>
                </div>
                <div class="modal-body" style="min-height:420px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Client Name </label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control required" id="EditAppClientName" placeholder="Client Name" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Auth # </label>
                                    </div>
                                    <div class="col-7">
                                        <select class="form-control" id="EditAppAuth"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Activity </label>
                                    </div>
                                    <div class="col-7">
                                        <select class="form-control" id="EditAppActivity"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Provider Name  </label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control required" id="EditAppProvider" placeholder="Provider Name" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">From Time </label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control required" id="EditAppFromTime" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">To Time  </label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control required" id="EditAppToTime" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Status  </label>
                                    </div>
                                    <div class="col-7">
                                        <select class="form-control" id="EditAppStatus">
                                            <option value=""></option>
                                            <option value="Confirm">Confirmed</option>
                                            <option value="Noshow">No Show</option>
                                            <option value="Hold">Hold</option>
                                            <option value="Cancelled by Client">Cancelled by Client</option>
                                            <option value="Cancelled by Provider">Cancelled by Provider</option>
                                            <option value="Rendered">Rendered</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Location </label>
                                    </div>
                                    <div class="col-7">
                                        <select class="form-control" id="EditddlLoca">
                                            <option value="11">Office</option>
                                            <option value="12">Home</option>
                                            <option value="03">School</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1">Description </label>
                                    </div>
                                    <div class="col-7">
                                        <textarea class="form-control" rows="5" cols="3" id="EditAppDesc"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-5">
                                        <label for="input-text" class="control-label float-right txt_media1"></label>
                                    </div>
                                    <div class="col-7">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <table id="tblSch1" class="table-bordered" ></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" class="btn btn-primary" id="btnUpdateApp" value="Update Appointment"/>
                </div>
            </div>
        </div>
    </div>





@stop
<style>
    .form-group{
        margin-bottom:0;
    }
    .ui-autocomplete-input {
        border: none;
        font-size: 11px;

        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
        color:#fff;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;

        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color:#000;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover {
        color: #fff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
    .ui-state-active {
        color: #fff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }

    .icon:hover{
        text-decoration: none;
    }
    a:hover{
        text-decoration: none;
    }
</style>

<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf-8">
    var convertDate = function(usDate) {
        if(usDate!=""){
            var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
            return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
        }
    }
    function getFormattedString(d){
        return d.getFullYear() + "-"+(d.getMonth()+1) +"-"+d.getDate() + ' '+d.toString().split(' ')[4];
        // for time part you may wish to refer http://stackoverflow.com/questions/6312993/javascript-seconds-to-time-string-with-format-hhmmss

    }
    $(document).ready(function(){
        var root_url = $("#hidden_root_url").val();
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        //$("#rightDiv").css("border-left","1px solid #000");
        $(".navigation li").parent().find('li').removeClass("active");
        $("#bulkDiv").hide();
        $("#divAvail").hide();
        $("#linkCheckAvail").hide();
        $("#navv2").addClass("active");
        // $('#AppFromTime').datetimepicker({
        //           // dayOfWeekStart: 1,
        //           // lang: 'en',
        //           format: 'm-d-Y H:i',
        //           timepicker: true,
        //           //minDate: '-2013/01/02',
        //           //maxDate: '+2014/12/31',
        //           formatDate: 'm-d-Y HH:mm',
        //           hours12:false,
        //           closeOnTimeSelect: true,

        //       });
        $("#divLoc").hide();
        $('#EditAppFromTime').datetimepicker({
            // dayOfWeekStart: 1,
            // lang: 'en',
            format: 'm-d-Y H:i',
            timepicker: true,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y HH:mm',
            hours12:false,
            closeOnTimeSelect: true,

        });
        $(document).on('click','.chk',function() {
            if ($("input:checkbox:checked").length > 1) {
                $("#bulkDiv").show();
            }
            else{
                $("#bulkDiv").hide();
            }
        });
        $(document).on('click','.chkall',function() {
            if($('.chkall').is(':checked') == true){
                $(".chk").prop("checked",true);
                $("#bulkDiv").show();
            }
            else{
                $(".chk").prop("checked",false);
                $("#bulkDiv").hide();
            }
        });
        $(document).on('click','#linkCheckAvail',function() {
            $("#modalAvail").modal("show");
            $("#availDate").val("");
            var len = $("#divAvail span").length;
            for(var j=0;j < len;j++){
                $("#divAvail span").eq(j).removeClass( "badge-danger" ).addClass( "badge-success" );
            }
        });
        $(document).on('click','.badge',function() {
            var res = $(this).html();
            var fin = "";
            res = res.split(" ");
            var time = res[0];
            var ampm = res[1];
            if(ampm == "PM"){
                time = time.split(":");
                if(time[0] == "12"){
                    var time1 = parseInt(time[0]);
                    var time2 = parseInt(time[1]);
                    fin = time1+":"+time2;
                }
                else{
                    var time1 = parseInt(time[0]);
                    var time2 = parseInt(time[1]);
                    time1 = 12+time1;
                    fin = time1+":"+time2;
                }
            }
            else{
                fin = time;
            }
            var availVal = $("#availDate").val();
            $("#modalAvail").modal("hide");
            var valFin = $("#AppFromTime").val(availVal+" "+fin);
            if(valFin != ""){
                var sVal = $("#AppFromTime").val();
                var fromDt = new Date(sVal);
                var mins = fromDt.getMinutes();
                var step = document.getElementById("AppStepTime").value;
                var toDt = new Date(fromDt);
                toDt.setMinutes(parseInt(toDt.getMinutes()) + parseInt(step));
                document.getElementById("AppToTime").value = nonConvertDt(toDt);
            }

        });
        $(document).on('focusout','#AppStepTime',function() {
            if ($(this).val() != "") {
                if($("#AppProvider").val() != ""){
                    $("#linkCheckAvail").show();
                }
            }
            else{
                $("#linkCheckAvail").hide();
            }
        });
        // flatpickr("#fromDate", {
        //   // disable: ["2018-01-30", "2018-01-15", "2018-03-08", new Date(2025, 4, 9) ],
        //   enableTime: true,
        //   dateFormat: "m-d-Y H:i",
        // });

        $('#fromDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#toDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        flatpickr("#availDate", {
            // disable: ["2018-01-30", "2018-01-15", "2018-03-08", new Date(2025, 4, 9) ],
            enableTime: true,
            dateFormat: "m-d-Y",
            //"static":true,
            onClose: function(selectedDates, dateStr, instance) {
                var d = new Date(selectedDates);
                var availDate = getFormattedString(d).split(" ")[0];
                var provider = $("#AppProvider").val();
                provider = provider.split(" - ")[0];
                //alert(availDate);
                //$("#divAvail").show();
                $.ajax({
                    type: "POST",
                    url: root_url+"appointments/times",
                    data:{
                        availDate : availDate,
                        appProvider : provider
                    },success:function(data){
                        $("#divAvail").show();
                        var res = JSON.parse(data);
                        if(res.status==0){
                            var len = $("#divAvail span").length;
                            var $MySpans = $("#divAvail span");
                            $MySpans.removeClass( "badge-danger" ).addClass( "badge-success" );
                            $("#divAvail span").css('pointer-events','auto');
                            return;
                        }
                        res = res.data;
                        for(var i=0; i< res.length;i++){
                            var t1 = res[i].appStartDt.split(" ")[1];
                            var t1mins = parseInt(t1.split(":")[0])*60 + parseInt(t1.split(":")[1]);
                            var t2 = res[i].appEndDt.split(" ")[1];
                            var t2mins = parseInt(t2.split(":")[0])*60 + parseInt(t2.split(":")[1]);
                            var diffmins = (t2mins - t1mins)/15;
                            t1mins = "";
                            for(var k=0;k<diffmins;k++){
                                if(t1mins == ""){
                                    t1mins = parseInt(t1.split(":")[0])*60 + parseInt(t1.split(":")[1]);
                                }
                                else{
                                    t1mins = t1mins + 15;
                                }

                                var hours = Math.floor( t1mins / 60);
                                var minutes = t1mins % 60;
                                if(hours <10){
                                    t1 = "0"+hours+":"+minutes;
                                }
                                else{
                                    t1 = hours+":"+minutes;
                                }
                                if(t1.split(":")[0] >= 12){
                                    if(t1.split(":")[0] == "12"){
                                        if(t1.split(":")[1] == "0"){
                                            var cal = parseInt(t1.split(":")[0]);
                                            t1 = cal+":0"+t1.split(":")[1]+" PM";
                                        }
                                        else{
                                            var cal = parseInt(t1.split(":")[0]);
                                            t1 = cal+":"+t1.split(":")[1]+" PM";
                                        }
                                    }
                                    else{
                                        if(t1.split(":")[1] == "0"){
                                            var cal = parseInt(t1.split(":")[0]);
                                            cal = cal - 12;
                                            if(cal <10){
                                                t1 = "0"+cal+":0"+t1.split(":")[1]+" PM";
                                            }
                                            else{
                                                t1 = cal+":0"+t1.split(":")[1]+" PM";
                                            }
                                        }
                                        else{
                                            var cal = parseInt(t1.split(":")[0]);
                                            cal = cal - 12;
                                            if(cal <10){
                                                t1 = "0"+cal+":"+t1.split(":")[1]+" PM";
                                            }
                                            else{
                                                t1 = cal+":"+t1.split(":")[1]+" PM";
                                            }
                                        }
                                    }
                                    var len = $("#divAvail span").length;
                                    for(var j=0;j < len;j++){
                                        if(t1 == $("#divAvail span")[j].innerHTML){
                                            $("#divAvail span").eq(j).removeClass( "badge-success" ).addClass( "badge-danger" );
                                            $("#divAvail span").eq(j).parent().css('pointer-events','none');
                                        }
                                    }

                                }
                                else{
                                    if(t1.split(":")[1] == "0"){
                                        t1 = t1.split(":")[0]+":0"+t1.split(":")[1]+" AM";
                                    }
                                    else{
                                        t1 = t1.split(":")[0]+":"+t1.split(":")[1]+" AM";
                                    }
                                    var len = $("#divAvail span").length;
                                    for(var j=0;j < len;j++){
                                        if(t1 == $("#divAvail span")[j].innerHTML){
                                            $("#divAvail span").eq(j).removeClass( "badge-success" ).addClass( "badge-danger" );
                                            $("#divAvail span").eq(j).parent().css('pointer-events','none');
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            },
        });

        //      $(document).on('change','#chkLoc',function() {
        //       var remember = document.getElementById('chkLoc');
        // if (remember.checked){
        //  $("#divLoc").show();
        // }else{
        //  $("#divLoc").hide();
        // }
        //      });

        $(document).on('focusout','#AppFromTime',function() {
            if($(this).val()!=""){
                var sVal = $(this).val();
                var fromDt = new Date(sVal);
                var mins = fromDt.getMinutes();
                var step = document.getElementById("AppStepTime").value;
                var toDt = new Date(fromDt);
                toDt.setMinutes(parseInt(toDt.getMinutes()) + parseInt(step));
                document.getElementById("AppToTime").value = nonConvertDt(toDt);
            }
        })
        $(document).on('focusout','#EditAppFromTime',function() {
            if($(this).val()!=""){
                var sVal = $(this).val();
                var fromDt = new Date(sVal);
                var mins = fromDt.getMinutes();
                var step = document.getElementById("EditAppStepTime").value;
                var toDt = new Date(fromDt);
                toDt.setMinutes(parseInt(toDt.getMinutes()) + parseInt(step));
                document.getElementById("EditAppToTime").value = nonConvertDt(toDt);
            }
        })
        $(document).on('focusout','#AppClientName',function() {
            var sVal = $(this).val();
            var n=sVal.indexOf("- ");
            var patientID = sVal.substr(0,n-1);
            $.ajax({
                type: "POST",
                url: root_url+"location/list",
                data:{
                    patientID : patientID
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                        return;
                    }
                    else{
                        $('#ddlLoca').html('');
                        $('#ddlLoca').append( '<option value="">-- Select Locations --</option>' );
                        //var res = JSON.parse(data);
                        if(res.data[0].home=="1"){
                            $('#ddlLoca').append( '<option value="12">Home</option>' );
                        }
                        if(res.data[0].office=="1"){
                            $('#ddlLoca').append( '<option value="11">Office</option>' );
                        }
                        if(res.data[0].school=="1"){
                            $('#ddlLoca').append( '<option value="03">School</option>' );
                        }
                    }
                }
            });
            $.ajax({
                type: "POST",
                url: root_url+"authinfo/patient",
                data:{
                    patientID : patientID
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                        return;
                    }
                    else{
                        var data = res.data;
                        $("#AppProvider").val("");
                        $("#AppFromTime").val("");
                        $("#AppToTime").val("");
                        $("#AppDesc").val("");
                        //document.getElementById("chkLoc").checked = false;
                        //$("#divLoc").hide();
                        $('#AppActivity').prop('selectedIndex',0);
                        $('#AppStatus').prop('selectedIndex',0);
                        $('#AppStepTime').prop('selectedIndex',0);
                        var arrCase = [];
                        $('#AppAuth').html('');
                        for(var x in data){
                            arrCase.push(data[x]);
                        }
                        //alert(arrCase);
                        $.each(arrCase,function(i,v) {
                            $('#AppAuth').html('<option value="">-- Select Auth --</option>');
                            arrCase.forEach(function(t) {
                                $('#AppAuth').append('<option value="'+t.key+'">'+t.label+'</option>');
                            });
                        });
                    }
                }
            });
        });
        $(document).on('focusout','#EditAppClientName',function() {
            var sVal = $(this).val();
            var n=sVal.indexOf("- ");
            var patientID = sVal.substr(0,n-1);
            $.ajax({
                type: "POST",
                url: root_url+"authinfo/patient",
                data:{
                    patientID : patientID
                },success:function(data){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                        return;
                    }
                    else{
                        var data = res.data;
                        var arrCase = [];
                        $('#EditAppAuth').html('');
                        for(var x in data){
                            arrCase.push(data[x]);
                        }
                        //alert(arrCase);
                        $.each(arrCase,function(i,v) {
                            $('#EditAppAuth').html('<option value="">-- Select Auth --</option>');
                            arrCase.forEach(function(t) {
                                $('#EditAppAuth').append('<option value="'+t.key+'">'+t.label+'</option>');
                            });
                        });
                    }
                }
            });
        });
        $('#AppAuth').change(function(){
            var authNo = $(this).val();
            $.ajax({
                type: "POST",
                url: root_url+"activity/auth",
                data:{
                    authNo : authNo
                },success:function(result){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                        return;
                    }
                    else{
                        var data = res.data;
                        var arrCase = [];
                        $('#AppActivity').html('');
                        for(var x in data){
                            arrCase.push(data[x]);
                        }
                        //alert(arrCase);
                        $.each(arrCase,function(i,v) {
                            $('#AppActivity').html('<option value="">-- Select Activity --</option>');
                            arrCase.forEach(function(t) {
                                $('#AppActivity').append('<option value="'+t.key+'">'+t.label+'</option>');
                            });
                        });
                    }
                }
            });
        })
        $('#EditAppAuth').change(function(){
            var authNo = $(this).val();
            $.ajax({
                type: "POST",
                url: root_url+"activity/auth",
                data:{
                    authNo : authNo
                },success:function(data){
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        new PNotify({
                            //title: 'Login Failed',
                            type:'error',
                            text: res.message,
                            after_init: function(notice){

                                    notice.attention('rubberBand');
                            }
                        })
                        return;
                    }
                    else{
                        data = res.data;
                        var arrCase = [];
                        $('#EditAppActivity').html('');
                        for(var x in data){
                            arrCase.push(data[x]);
                        }
                        //alert(arrCase);
                        $.each(arrCase,function(i,v) {
                            $('#EditAppActivity').html('<option value="">-- Select Activity --</option>');
                            arrCase.forEach(function(t) {
                                $('#EditAppActivity').append('<option value="'+t.key+'">'+t.label+'</option>');
                            });
                        });
                    }
                }
            });
        })
        $("#AppStatus option[value='Rendered']").hide();
        $("#AppStatus option[value='Noshow']").hide();
        $("#AppStatus option[value='Hold']").hide();
        $("#AppStatus option[value='Cancelled']").hide();
        $("#AppStatus option[value='Cancelled by Client']").hide();
        $("#AppStatus option[value='Cancelled by Provider']").hide();
        var patientList = "";
        patientList = root_url + "patient/list1";
        //alert(patientList);
        var patientList1 = "";
        patientList1 = root_url + "patient/list5";
        $("#clientName").autocomplete({
            source: patientList,
            autoFocus:true
        });
        $("#AppClientName").autocomplete({
            source: patientList1,
            autoFocus:true
        });
        $("#EditAppClientName").autocomplete({
            source: patientList1,
            autoFocus:true
        });
        $("#btnGo").click(function(){
            $("#schedulerTable").show();
            $("#DivAddApp").hide();
        })
        $("#schedulerTable").hide();
        
        var PrimaryPhy = "";
        PrimaryPhy = root_url + "physician/get";

        $("#providerName").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#AppProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#EditAppProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#btnAddApp").click(function(){
            //          var appID = sessionStorage.getItem("appID");
            // if(appID == "" || appID == null){
            var truncID = "";
            var truncID1 = "";
            var patientName = $("#AppClientName").val();
            var n1=patientName.indexOf(" -");
            var remStr = patientName.substr(0,n1+3);
            var patName = patientName.replace(remStr,"");
            var patID = patientName;

            var providerName = $("#AppProvider").val();
            var n11=providerName.indexOf(" - ");
            var remStr1 = providerName.substr(0,n11+3);
            var patName1 = providerName.replace(remStr1,"");
            var patID1 = providerName;
            var authNo = $("#AppAuth").val();
            var activity = $("#AppActivity").val();
            var POS = $("#ddlLoca").val();

            if(patID != ""){
                var n=patID.indexOf(" -");
                truncID = patID.substr(0,n);
            }
            if(patID1 != ""){
                var n=patID1.indexOf(" -");
                truncID1 = patID1.substr(0,n);
            }
            var description = $("#AppDesc").val();
            //var statusApp = $("#AppStatus").val();
            var start_date = convertDt($("#AppFromTime").val());
            var end_date = convertDt($("#AppToTime").val());
            $.ajax({
                type: "POST",
                url:root_url+"appointments/add",
                data:{
                    patientName : patName,
                    description : description,
                    statusApp : "Confirm",
                    start_date : start_date,
                    end_date : end_date,
                    patientID : truncID,
                    providerName : patName1,
                    providerID : truncID1,
                    authNo : authNo,
                    activity : activity,
                    POS : POS
                },success:function(result){
                    //alert(result);
                    var res = JSON.parse(result);
                    if(res.status == 0){
                        return;
                    }
                    else{
                        if(result == "not-allow"|| result == "no-auth"){
                            alertify.error("Auth Expired or You have exceeded the Total Auth Limit");
                            sessionStorage.removeItem("appID");
                        }else{
                            alertify.success("Appointment Added successfully");
                            sessionStorage.removeItem("appID");
                            //window.location.reload();
                            $("#AppProvider").val("");
                            $("#AppFromTime").val("");
                            $("#AppToTime").val("");
                            $("#AppDesc").val("");
                            //document.getElementById("chkLoc").checked = false;
                            //$("#divLoc").hide();
                            $('#AppActivity').prop('selectedIndex',0);
                            $('#AppStatus').prop('selectedIndex',0);
                            $('#AppStepTime').prop('selectedIndex',0);
                            getAppointData();
                        }
                    }
                }
            });
            //}
        })
        $("#btnUpdateApp").click(function(){
            var appID = document.getElementById("_hdnAppID").value;
            if(appID != "" && appID != null){
                var statusApp = $("#EditAppStatus").val();
                $.ajax({
                    type: "POST",
                    url: root_url+"appointments/update",
                    data:{
                        appID : appID,
                        statusApp : statusApp
                    },success:function(result){
                        alert("Appointment Updated successfully");
                        sessionStorage.removeItem("appID");
                        $("#EditAppoint").modal("hide");
                        $("#btnGo").click();
                        //window.location.reload();
                    }
                });
            }
            else{
                alert("Error on selecting Appointment. Contact with Administrator");
            }
        })
        $("#btnGo").click(function(){
            getAppointData1();
        })
        $("#triggerAddApp").click(function(){
            $("#modalAppoint").modal("show");
        })
        $(document).on('click','.editSch',function() {
            var temp = $(this).attr('class').split(' ')[1];
            var newTemp = temp.replace('editSch','');
            var appID = newTemp;
            document.getElementById("_hdnAppID").value = appID;
            sessionStorage.setItem("appID",appID);
            $("#EditAppoint").modal("show");
            $("#EditAppClientName").attr("disabled",true);
            $("#EditAppProvider").attr("disabled",true);
            $("#EditAppAuth").attr("disabled",true);
            $("#EditAppActivity").attr("disabled",true);
            $("#AppStatus option[value='Rendered']").show();
            $("#AppStatus option[value='Noshow']").show();
            $("#AppStatus option[value='Hold']").show();
            $("#AppStatus option[value='Cancelled']").show();
            $("#AppStatus option[value='Cancelled by Client']").show();
            $("#AppStatus option[value='Cancelled by Provider']").show();
            $("#EditAppDesc").attr("disabled",true);
            $("#divDuration").hide();
            $("#EditAppFromTime").attr("disabled",true);
            $("#EditAppToTime").attr("disabled",true);
            $.ajax({
                type: "POST",
                url: root_url+"appointments/get",
                data:{
                    appID : appID
                },success:function(data){
                    data = JSON.parse(data);
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: root_url+"authinfo/patient",
                        data:{
                            patientID : data[0].AppPatientID
                        },success:function(res){
                            res = JSON.parse(res);
                            var arrCase = [];
                            $('#EditAppAuth').html('');
                            for(var x in res){
                                arrCase.push(res[x]);
                            }
                            //alert(arrCase);
                            $.each(arrCase,function(i,v) {
                                $('#EditAppAuth').html('-- Select Auth --');
                                arrCase.forEach(function(t) {
                                    $('#EditAppAuth').append('<option value="'+t.key+'">'+t.label+'</option>');
                                });
                            });
                        }
                    });
                    document.getElementById("EditAppAuth").value = data[0].authNo;
                    $.ajax({
                        type: "POST",
                        url: root_url+"activity/auth",
                        async:false,
                        data:{
                            authNo : data[0].authNo
                        },success:function(data){
                            data = JSON.parse(data);
                            var arrCase = [];
                            $('#EditAppActivity').html('');
                            for(var x in data){
                                arrCase.push(data[x]);
                            }
                            //alert(arrCase);
                            $.each(arrCase,function(i,v) {
                                $('#EditAppActivity').html('-- Select Activity --');
                                arrCase.forEach(function(t) {
                                    $('#EditAppActivity').append('<option value="'+t.key+'">'+t.label+'</option>');
                                });
                            });
                        }
                    });
                    document.getElementById("EditAppClientName").value = data[0].clientName;
                    document.getElementById("EditAppActivity").value = data[0].activity;
                    document.getElementById("EditAppProvider").value = data[0].providerName;
                    document.getElementById("EditAppStatus").value = data[0].appStatus;
                    document.getElementById("EditddlLoca").value = data[0].loc;
                    //document.getElementById("EditAppStepTime").value = data[0].;
                    document.getElementById("EditAppFromTime").value = data[0].fromTime;
                    document.getElementById("EditAppToTime").value = data[0].toTime;
                    $("#EditAppStatus option[value='Rendered']").show();
                    document.getElementById("EditAppDesc").value = data[0].appDesc;

                }
            });
        });
        // $('#AppFromTime').datetimepicker({
        //     // dayOfWeekStart: 1,
        //     // lang: 'en',
        //     format: 'm-d-Y H:i',
        //     timepicker: true,
        //     //minDate: '-2013/01/02',
        //     //maxDate: '+2014/12/31',
        //     formatDate: 'm-d-Y HH:mm',
        //     hours12:false,
        //     closeOnTimeSelect: true,

        // });
        $('#EditAppFromTime').datetimepicker({
            // dayOfWeekStart: 1,
            // lang: 'en',
            format: 'm-d-Y H:i',
            timepicker: true,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y HH:mm',
            hours12:false,
            closeOnTimeSelect: true,

        });
        $("#btnFin").click(function(){
            var searchIDs = $('input:checked').map(function(){
                return $(this).attr('id');
            });
            for(var i=0;i<searchIDs.length;i++){
                var temp = searchIDs[i];
                var newTemp = temp.replace('chk','');
                document.getElementById('_hdnAppID1').value = newTemp;
                var tempStatus = document.getElementById("ddlSta").value;

                $.ajax({
                    type: "POST",
                    url: root_url+"appointments/updatebulk",
                    asyn:false,
                    data:{
                        claimID : newTemp,
                        status : tempStatus,
                    },success:function(result){
                    }
                });
            }
            getAppointData1();
            alertify.success("Updated successfully");
        });
    });

    function convertDt(i){
        i = new Date(i);
        var res = i.toLocaleDateString();
        var yy = i.getFullYear();
        var mm = pad(i.getMonth()+1);
        var dd = pad(i.getDate());
        var hours = pad(i.getHours());
        var min = pad(i.getMinutes());
        return yy+"-"+mm+"-"+dd+" "+hours+":"+min;
    }
    function nonConvertDt(i){
        var res = i.toLocaleDateString();
        var yy = i.getFullYear();
        var mm = pad(i.getMonth()+1);
        var dd = pad(i.getDate());
        var hours = pad(i.getHours());
        var min = pad(i.getMinutes());
        return mm+"-"+dd+"-"+yy+" "+hours+":"+min;
    }
    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2].substr(0,2);

        return month + '-' + day + '-' + year;
    }
    function getAppointData1(){
        var root_url = $("#hidden_root_url").val();
        var clientName = $("#clientName").val();
        var providerNa = $("#providerName").val();
        var len = providerNa.length;
        var n=providerNa.indexOf("-");
        var providerName = providerNa.substr(n+2,len);
        var statusName = $("#statusName").val();
        var locationName = $("#locationName").val();
        var fromDate = convertDate($("#fromDate").val());
        var toDate = convertDate($("#toDate").val());
        $.ajax({
            type: "POST",
            url: root_url+"appointments/gets",
            data:{
                clientName : clientName,
                providerName : providerName,
                statusName : statusName,
                locationName : locationName,
                fromDate : fromDate,
                toDate : toDate,
            },success:function(result){
                var res = JSON.parse(result);
                 if(res.status == 0){
                    var dt = [];
                     $('#tblSch').DataTable({
                        destroy: true,
                        "pageLength": 5,
                        "data": dt,
                        columns: [
                            {"title": "<input type='checkbox' class='chkall' />",
                                "render": function ( data, type, full, meta ) {
                                    return '<input type="checkbox" class="chk chk'+data+'" id="chk'+data+'"/>';
                                }
                            },
                            {"title": "Client"},
                            // {"title": "Auth #"},
                            {"title": "Activity"},
                            {"title": "Provider"},
                            {"title": "Location"},
                            {"title": "Scheduled Date"},
                            {"title": "From"},
                            {"title": "To"},
                            {"title": "Status"},
                            {"title": "Action",
                                "render": function ( data, type, full, meta ) {
                                    return '<a href="javascript:void(0);" class="editSch editSch'+data+'"><span class="ti-pencil"></span></a>';
                                }
                            }
                        ]
                    });
                    return;
                }
                var data1 = res.data;

                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].appID,data1[i].clientName,data1[i].activity,data1[i].providerName,data1[i].loc,data1[i].fromDate,data1[i].fromTime,data1[i].toTime,data1[i].appStatus,data1[i].appID]);
                    //dt.push([data1[i].appID,data1[i].client_name,data1[i].activity,data1[i].provider_name,data1[i].loc,changeDateFormat(data1[i].from_date),data1[i].from_time,data1[i].to_time,data1[i].appStatus,data1[i].appID]);
                });
                $('#tblSch').DataTable({
                    destroy: true,
                    "pageLength": 5,
                    "data": dt,
                    columns: [
                        {"title": "<input type='checkbox' class='chkall' />",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="checkbox" class="chk chk'+data+'" id="chk'+data+'"/>';
                            }
                        },
                        {"title": "Client"},
                        // {"title": "Auth #"},
                        {"title": "Activity"},
                        {"title": "Provider"},
                        {"title": "Location"},
                        {"title": "Scheduled Date"},
                        {"title": "From"},
                        {"title": "To"},
                        {"title": "Status"},
                        {"title": "Action",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0);" class="editSch editSch'+data+'"><span class="ti-pencil"></span></a>';
                            }
                        }
                    ]
                });
            }
        });
    }
    function getAppointData(){
        var patientName = $("#AppClientName").val();
        var n1=patientName.indexOf(" -");
        var remStr = patientName.substr(0,n1+3);
        var patName = patientName.replace(remStr,"");
        var root_url = $("#hidden_root_url").val();
        var patID = patientName;
        var truncID = "";
        if(patID != ""){
            var n=patID.indexOf(" -");
            truncID = patID.substr(0,n);
        }
        var authNo = $("#AppAuth").val();

        $.ajax({
            type: "POST",
            url: root_url + "appointments/recent",
            data: {
                patientID: truncID,
                authNo : authNo
            }, success: function (result) {
                $("#tblSch").show();
                var res = JSON.parse(result);
                 if(res.status == 0){
                    return;
                }
                var data1 = res.data;
                var dt = [];
                $.each(data1,function(i,v) {
                    //dt.push([data1[i].appID,data1[i].clientName,data1[i].authNo,data1[i].activity,data1[i].fromDate,data1[i].fromTime,data1[i].toTime,data1[i].loc,data1[i].appStatus,data1[i].providerName,data1[i].appID]);
                    dt.push([data1[i].appID,data1[i].clientName,data1[i].activity,data1[i].providerName,data1[i].loc,changeDateFormat(data1[i].fromDate),data1[i].fromTime,data1[i].toTime,data1[i].appStatus,data1[i].appID]);
                });
                $('#tblSch').DataTable({
                    destroy: true,
                    "pageLength": 5,
                    "data": dt,
                    columns: [
                        {"title": "",
                            "render": function ( data, type, full, meta ) {
                                return '<input type="checkbox" class="chk chk'+data+'" id="chk'+data+'"/>';
                            }
                        },
                        {"title": "Client"},
                        // {"title": "Auth #"},
                        {"title": "Activity"},
                        {"title": "Provider"},
                        {"title": "Location"},
                        {"title": "Scheduled Date"},
                        {"title": "From"},
                        {"title": "To"},
                        {"title": "Status"},
                        {"title": "Action",
                            "render": function ( data, type, full, meta ) {
                                return '<a href="javascript:void(0);" class="editSch editSch'+data+'"><span class="ti-pencil"></span></a>';
                            }
                        }
                    ]
                });
            }
        });
    }
    function pad(n){return n<10 ? '0'+n : n}
</script>