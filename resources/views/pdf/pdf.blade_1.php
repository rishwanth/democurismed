<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table class="table table-bordered" border="1" cellpadding="0" cellspacing="0">

      <tr>
        <th>
          first_name
        </th>
        <th>
          last_name
        </th>
        <th>
          username
        </th>
      </tr>

      @foreach($users as $user)
        <tr>
          <td>
            {{ $user->first_name }}
          </td>
          <td>
            {{ $user->last_name }}
          </td>
          <td>
            {{ $user->username }}
          </td>
        </tr>
    @endforeach
    </table>
  </body>
</html>