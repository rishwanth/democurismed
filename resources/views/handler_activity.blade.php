@extends('layout.handler')
@section('content')
<?php
    $base_url = URL::to('/');
?>
<div class="row" style="margin: 5%; ">
    <div class="bs-example" style="width: 60%; ">
        <ul class="nav nav-tabs" style="margin-bottom: 15px;">
            <li class="nav-item">
                <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Add Activity</a>
            </li>
            <li class="nav-item">
                <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Add CPT codes</a>
            </li>
            <!-- <li class="nav-item">
        <a href="#tab_3_7" style="color:#000;" data-toggle="tab" class="nav-link">Log</a>
    </li> -->
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane active" id="tab_3_1">
                <div class="col-12">
                    <div class="card-body">
                        <div class="col-sm-12">
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Type of Service</label>
                                </div>
                                <div class="col-8">
                                    <select id="ddlService" class="col-md-10 form-control">
                                        <option value="">-- Select Service Type --</option>
                                        <option value="1">Behaviour Therapy</option>
                                        <option value="2">Physical Therapy</option>
                                        <option value="3">Occupational Therapy</option>
                                        <option value="4">Speech Therapy</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Activity Name</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="col-md-10 form-control" id="txtActivity"/>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-4">
                                </div>
                                <div class="col-8">
                                    <input type="button" class="btn btn-primary" id="btnAddActivity" value="Add Activity"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <table id="getActivities" class="table table-bordered"></table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab_3_2">
                <div class="col-12">
                    <div class="card-body">
                        <div class="col-sm-12">
                            <div class="form-group row" style="margin-bottom: 1%">
                                <div class="col-2">
                                    <label for="input-text" class="control-label float-right txt_media1">Activity Name</label>
                                </div>
                                <div class="col-8">
                                    <select id="ddlActivity" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <div class="col-2">
                                    <label for='dup' class="m-t-b-15">CPT Codes: </label>
                                </div>
                                <div class="col-8">
                                    <div class='input'>
                                        <input id='dup' value='' placeholder="type & hit space to create codes" style="min-width: 200px;">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <input type="button" class="btn btn-primary" value="Add Values" id="btnGetValues"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="codesList"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
<link rel="stylesheet" href="<?php echo $base_url; ?>/public/vendors/insignia/css/insignia.css">
<link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>/public/css/custom_css/insignia_custom.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="<?php echo $base_url; ?>/public/vendors/insignia/js/insignia.js"></script>

<script>
    $(document).ready(function(){
        insignia(dup, {
            validate: function () {
                return true;
            }
        });
        //alert(sessionStorage.getItem("patientId"));
        //document.getElementById('hdntoggleDX').value = "false";
       

       

        $(".ti-menu-alt").click(function(){
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        })
    });
</script>
